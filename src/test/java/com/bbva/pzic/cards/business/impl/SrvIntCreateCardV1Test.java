package com.bbva.pzic.cards.business.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.dao.ICardsDAOV1;
import com.bbva.pzic.routine.validator.Validator;
import com.bbva.pzic.routine.validator.impl.DefaultValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * Created on 06/04/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class SrvIntCreateCardV1Test {

    @InjectMocks
    private SrvIntCardsV1 srvInt;
    @Mock
    private ICardsDAOV1 cardsDAO;
    @Mock
    private Validator validator;

    @Before
    public void setUp() {
        validator = new DefaultValidator();
        srvInt.setValidator(validator);
    }

    @Test(expected = BusinessServiceException.class)
    public void mapInWithoutCardRelatedContracts0ContractIdCreateCard() throws IOException {
        DTOIntCard dtoIntCard = EntityMock.getInstance().buildDTOIntCard();
        dtoIntCard.getRelatedContracts().get(0).setContractId(null);

        InputCreateCard input = new InputCreateCard();
        input.setCard(dtoIntCard);

        srvInt.createCard(input);

        verify(cardsDAO, never()).createCard(any(InputCreateCard.class));
    }

}
