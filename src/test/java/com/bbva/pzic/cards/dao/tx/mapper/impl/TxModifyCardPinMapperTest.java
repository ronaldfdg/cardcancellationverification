package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.dao.model.mpwp.FormatoMPMENWP;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardPinMapper;
import org.junit.Before;
import org.junit.Test;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TxModifyCardPinMapperTest {

    private ITxModifyCardPinMapper txModifyCardPinMapper;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void init() {
        txModifyCardPinMapper = new TxModifyCardPinMapper();
    }

    @Test
    public void mapInFullTest() {
        final DTOIntPin dtoIn = entityMock.buildDTOIntPin();

        FormatoMPMENWP formatoMPMENWP = txModifyCardPinMapper.mapInput(dtoIn);
        assertNotNull(formatoMPMENWP.getNumtarj());
        assertNotNull(formatoMPMENWP.getDtcif01());
        assertNotNull(formatoMPMENWP.getDtcif02());
        assertNotNull(formatoMPMENWP.getDtcif03());
        assertNotNull(formatoMPMENWP.getDtcif04());
        assertNotNull(formatoMPMENWP.getDtcif05());
        assertNotNull(formatoMPMENWP.getDtcif06());
        assertNotNull(formatoMPMENWP.getDtcif07());
        assertNotNull(formatoMPMENWP.getDtcif08());
        assertNotNull(formatoMPMENWP.getDtcif09());
        assertNotNull(formatoMPMENWP.getDtcif10());
        assertNotNull(formatoMPMENWP.getDtcif11());
        assertNotNull(formatoMPMENWP.getDtcif12());
        assertNotNull(formatoMPMENWP.getDtcif13());
        assertNotNull(formatoMPMENWP.getDtcif14());

        assertEquals(dtoIn.getCardId(), formatoMPMENWP.getNumtarj());
        assertEquals(CIF_01, formatoMPMENWP.getDtcif01());
        assertEquals(CIF_02, formatoMPMENWP.getDtcif02());
        assertEquals(CIF_03, formatoMPMENWP.getDtcif03());
        assertEquals(CIF_04, formatoMPMENWP.getDtcif04());
        assertEquals(CIF_05, formatoMPMENWP.getDtcif05());
        assertEquals(CIF_06, formatoMPMENWP.getDtcif06());
        assertEquals(CIF_07, formatoMPMENWP.getDtcif07());
        assertEquals(CIF_08, formatoMPMENWP.getDtcif08());
        assertEquals(CIF_09, formatoMPMENWP.getDtcif09());
        assertEquals(CIF_10, formatoMPMENWP.getDtcif10());
        assertEquals(CIF_11, formatoMPMENWP.getDtcif11());
        assertEquals(CIF_12, formatoMPMENWP.getDtcif12());
        assertEquals(CIF_13, formatoMPMENWP.getDtcif13());
        assertEquals(CIF_14, formatoMPMENWP.getDtcif14());
    }

    @Test
    public void mapInPartialTest() {
        final DTOIntPin dtoIn = entityMock.buildDTOIntPin();
        dtoIn.setPin(CIF_01 + CIF_02 + CIF_03 + CIF_04 + CIF_05 + CIF_06 + CIF_07 + CIF_08 + CIF_09
                + CIF_10 + CIF_11 + CIF_12 + CIF_13 + "A");

        FormatoMPMENWP formatoMPMENWP = txModifyCardPinMapper.mapInput(dtoIn);
        assertNotNull(formatoMPMENWP.getNumtarj());
        assertNotNull(formatoMPMENWP.getDtcif01());
        assertNotNull(formatoMPMENWP.getDtcif02());
        assertNotNull(formatoMPMENWP.getDtcif03());
        assertNotNull(formatoMPMENWP.getDtcif04());
        assertNotNull(formatoMPMENWP.getDtcif05());
        assertNotNull(formatoMPMENWP.getDtcif06());
        assertNotNull(formatoMPMENWP.getDtcif07());
        assertNotNull(formatoMPMENWP.getDtcif08());
        assertNotNull(formatoMPMENWP.getDtcif09());
        assertNotNull(formatoMPMENWP.getDtcif10());
        assertNotNull(formatoMPMENWP.getDtcif11());
        assertNotNull(formatoMPMENWP.getDtcif12());
        assertNotNull(formatoMPMENWP.getDtcif13());
        assertNotNull(formatoMPMENWP.getDtcif14());

        assertEquals(dtoIn.getCardId(), formatoMPMENWP.getNumtarj());
        assertEquals(CIF_01, formatoMPMENWP.getDtcif01());
        assertEquals(CIF_02, formatoMPMENWP.getDtcif02());
        assertEquals(CIF_03, formatoMPMENWP.getDtcif03());
        assertEquals(CIF_04, formatoMPMENWP.getDtcif04());
        assertEquals(CIF_05, formatoMPMENWP.getDtcif05());
        assertEquals(CIF_06, formatoMPMENWP.getDtcif06());
        assertEquals(CIF_07, formatoMPMENWP.getDtcif07());
        assertEquals(CIF_08, formatoMPMENWP.getDtcif08());
        assertEquals(CIF_09, formatoMPMENWP.getDtcif09());
        assertEquals(CIF_10, formatoMPMENWP.getDtcif10());
        assertEquals(CIF_11, formatoMPMENWP.getDtcif11());
        assertEquals(CIF_12, formatoMPMENWP.getDtcif12());
        assertEquals(CIF_13, formatoMPMENWP.getDtcif13());
        assertEquals("A", formatoMPMENWP.getDtcif14());
    }
}