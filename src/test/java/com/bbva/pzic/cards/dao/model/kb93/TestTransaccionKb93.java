package com.bbva.pzic.cards.dao.model.kb93;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>KB93</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionKb93 {

    @InjectMocks
    private TransaccionKb93 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionKb93 rq = new PeticionTransaccionKb93();
        RespuestaTransaccionKb93 rs = transaccion.invocar(rq);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionKb93.class, RespuestaTransaccionKb93.class, rq))
                .thenReturn(rs);

        RespuestaTransaccionKb93 result = transaccion.invocar(rq);

        Assert.assertEquals(result, rs);
    }
}
