package com.bbva.pzic.cards.dao.model.mpg5;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPG5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpg5 {

    @InjectMocks
    private TransaccionMpg5 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);


    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpg5 peticion = new PeticionTransaccionMpg5();

        RespuestaTransaccionMpg5 respuesta = transaccion.invocar(peticion);
        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpg5.class, RespuestaTransaccionMpg5.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionMpg5 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);


    }
}