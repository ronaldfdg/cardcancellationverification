package com.bbva.pzic.cards.dao.model.ppcut003_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>PPCUT003</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPpcut003_1 {

    @InjectMocks
    private TransaccionPpcut003_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPpcut003_1 rq = new PeticionTransaccionPpcut003_1();
        RespuestaTransaccionPpcut003_1 rs = new RespuestaTransaccionPpcut003_1();

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionPpcut003_1.class, RespuestaTransaccionPpcut003_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPpcut003_1 result = transaccion.invocar(rq);

        Assert.assertEquals(result, rs);
    }
}