package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardReports;
import com.bbva.pzic.cards.dao.model.cardreports.ModelCreateCardReportsRequest;
import com.bbva.pzic.cards.util.Converter;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class RestCreateCardReportsV0MapperTest {

    private RestCreateCardReportsV0Mapper mapper = new RestCreateCardReportsV0Mapper();

    private EntityMock mock = EntityMock.getInstance();

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateCardReports input = mock.getInputCreateCardReportsMock();

        ModelCreateCardReportsRequest result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData().getId());
        assertNotNull(result.getSecurity());
        assertNotNull(result.getSecurity().getUsuario());
        assertNotNull(result.getSecurity().getPassword());
        assertNotNull(result.getData().getCanal());
        assertNotNull(result.getData().getTipoTarjeta());
        assertNotNull(result.getData().getTipoTarjeta().getId());
        assertNotNull(result.getData().getNumeroProducto());
        assertNotNull(result.getData().getTipoNumeroTarjeta());
        assertNotNull(result.getData().getTipoNumeroTarjeta().getId());
        assertNotNull(result.getData().getMarca());
        assertNotNull(result.getData().getMarca().getId());
        assertNotNull(result.getData().getProducto());
        assertNotNull(result.getData().getProducto().getId());
        assertNotNull(result.getData().getProducto().getNombre());
        assertNotNull(result.getData().getProducto().getSubproducto());
        assertNotNull(result.getData().getProducto().getSubproducto().getId());
        assertNotNull(result.getData().getSubproducto());
        assertNotNull(result.getData().getSubproducto().getNombre());
        assertNotNull(result.getData().getSubproducto().getDescripcion());

        assertNotNull(result.getData().getFlagMonedaPrincipal());
        assertNotNull(result.getData().getFlagInnominada());
        assertNotNull(result.getData().getCliente());
        assertNotNull(result.getData().getCliente().getCodigoCentral());
        assertNotNull(result.getData().getCliente().getPrimerNombre());
        assertNotNull(result.getData().getCliente().getSegundoNombre());
        assertNotNull(result.getData().getCliente().getApellidoPaterno());
        assertNotNull(result.getData().getCliente().getApellidoMaterno());
        assertNotNull(result.getData().getCliente().getDocumentoIdentidad());
        assertNotNull(result.getData().getCliente().getDocumentoIdentidad().getId());
        assertNotNull(result.getData().getCliente().getDocumentoIdentidad().getNumero());
        assertNotNull(result.getData().getCliente().getDatosContacto());
        // EMAIL
        assertNotNull(result.getData().getCliente().getDatosContacto().get(0));
        assertNotNull(result.getData().getCliente().getDatosContacto().get(0).getTipo());
        assertNotNull(result.getData().getCliente().getDatosContacto().get(0).getTipo().getId());
        assertNotNull(result.getData().getCliente().getDatosContacto().get(0).getValor());
        // MOBILE
        assertNotNull(result.getData().getCliente().getDatosContacto().get(1));
        assertNotNull(result.getData().getCliente().getDatosContacto().get(1).getTipo());
        assertNotNull(result.getData().getCliente().getDatosContacto().get(1).getTipo().getId());
        assertNotNull(result.getData().getCliente().getDatosContacto().get(1).getValor());

        assertNotNull(result.getData().getDatosPago());
        assertNotNull(result.getData().getDatosPago().getId());
        assertNotNull(result.getData().getDatosPago().getFrecuencia());
        assertNotNull(result.getData().getDatosPago().getDia());
        assertNotNull(result.getData().getImporte());
        assertNotNull(result.getData().getImporte().getMonto());
        assertNotNull(result.getData().getImporte().getMoneda());
        assertNotNull(result.getData().getCuentaCargo());
        assertNotNull(result.getData().getTipoCuentaCargo());
        assertNotNull(result.getData().getTipoCuentaCargo().getId());
        assertNotNull(result.getData().getDatosEntrega());
        // STORED
        assertNotNull(result.getData().getDatosEntrega().get(0));
        assertNotNull(result.getData().getDatosEntrega().get(0).getId());
        assertNotNull(result.getData().getDatosEntrega().get(0).getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(0).getTipo().getId());
        assertNotNull(result.getData().getDatosEntrega().get(0).getContacto());
        assertNotNull(result.getData().getDatosEntrega().get(0).getContacto().getTipoContacto());
        assertNotNull(result.getData().getDatosEntrega().get(0).getContacto().getTipoContacto().getId());
        assertNotNull(result.getData().getDatosEntrega().get(0).getContacto().getId());
        assertNull(result.getData().getDatosEntrega().get(0).getContacto().getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(0).getContacto().getValor());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDireccion());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDireccion().getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDireccion().getTipo().getId());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDireccion().getId());
        assertNull(result.getData().getDatosEntrega().get(0).getDireccion().getDireccionDetallada());
        assertNull(result.getData().getDatosEntrega().get(0).getDireccion().getDireccionCompleta());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDestino());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDestino().getId());
        assertNotNull(result.getData().getDatosEntrega().get(0).getDestino().getNombre());
        assertNotNull(result.getData().getDatosEntrega().get(0).getOficina());
        assertNotNull(result.getData().getDatosEntrega().get(0).getOficina().getId());
        assertNotNull(result.getData().getDatosEntrega().get(0).getOficina().getNombre());

        // SPECIFIC
        assertNotNull(result.getData().getDatosEntrega().get(1));
        assertNotNull(result.getData().getDatosEntrega().get(1).getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(1).getTipo().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto().getTipoContacto());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto().getTipoContacto().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto().getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto().getTipo().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getContacto().getValor());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getTipo().getId());
        assertNull(result.getData().getDatosEntrega().get(1).getDireccion().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0));
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getTipo());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getTipo().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getCodigo());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getNombre());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDireccion().getDireccionCompleta());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDestino());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDestino().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getDestino().getNombre());
        assertNotNull(result.getData().getDatosEntrega().get(1).getOficina());
        assertNotNull(result.getData().getDatosEntrega().get(1).getOficina().getId());
        assertNotNull(result.getData().getDatosEntrega().get(1).getOficina().getId());

        assertNotNull(result.getData().getOficina());
        assertNotNull(result.getData().getOficina().getId());
        assertNotNull(result.getData().getOficina().getNombre());
        assertNotNull(result.getData().getProgramaBeneficios());
        assertNotNull(result.getData().getProgramaBeneficios().getId());
        assertNotNull(result.getData().getProgramaBeneficios().getNombre());
        assertNotNull(result.getData().getEstado());
        assertNotNull(result.getData().getOferta());
        assertNotNull(result.getData().getOferta().getId());


        assertEquals(input.getCardId(), result.getData().getId());
        assertEquals(input.getUser(), result.getSecurity().getUsuario());
        assertEquals(input.getPassword(), result.getSecurity().getPassword());
        assertEquals(input.getChannel(), result.getData().getCanal());
        assertEquals(input.getCardType().getId(), result.getData().getTipoTarjeta().getId());
        assertEquals(input.getNumber(), result.getData().getNumeroProducto());
        assertEquals(input.getNumberType().getId(), result.getData().getTipoNumeroTarjeta().getId());
        assertEquals(input.getBrandAssociation().getId(), result.getData().getMarca().getId());
        assertEquals(input.getProduct().getId(), result.getData().getProducto().getId());
        assertEquals(input.getProduct().getName(), result.getData().getProducto().getNombre());
        assertEquals(input.getProduct().getSubproduct().getId(), result.getData().getProducto().getSubproducto().getId());
        assertEquals(input.getProduct().getSubproduct().getName(), result.getData().getSubproducto().getNombre());
        assertEquals(input.getProduct().getSubproduct().getDescription(), result.getData().getSubproducto().getDescripcion());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getData().getImporte().getMoneda());
        assertEquals(Converter.booleanToInteger(input.getCurrencies().get(0).getIsMajor()), result.getData().getFlagMonedaPrincipal());
        assertEquals(Converter.booleanToInteger(input.getHasPrintedHolderName()), result.getData().getFlagInnominada());
        assertEquals(input.getParticipants().get(0).getId(), result.getData().getCliente().getCodigoCentral());
        assertEquals(input.getParticipants().get(0).getFirstName(), result.getData().getCliente().getPrimerNombre());
        assertEquals(input.getParticipants().get(0).getMiddleName(), result.getData().getCliente().getSegundoNombre());
        assertEquals(input.getParticipants().get(0).getLastName(), result.getData().getCliente().getApellidoPaterno());
        assertEquals(input.getParticipants().get(0).getSecondLastName(), result.getData().getCliente().getApellidoMaterno());
        assertEquals(input.getParticipants().get(0).getIdentityDocuments().get(0).getNumber(), result.getData().getCliente().getDocumentoIdentidad().getNumero());
        assertEquals(input.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType().getId(), result.getData().getCliente().getDocumentoIdentidad().getId());

        // EMAIL
        assertEquals(input.getParticipants().get(0).getContactDetails().get(0).getEmailContact().getContactType(), result.getData().getCliente().getDatosContacto().get(0).getTipo().getId());
        assertEquals(input.getParticipants().get(0).getContactDetails().get(0).getEmailContact().getAddress(), result.getData().getCliente().getDatosContacto().get(0).getValor());

        // MOBILE
        assertEquals(input.getParticipants().get(0).getContactDetails().get(1).getMobileContact().getContactType(), result.getData().getCliente().getDatosContacto().get(1).getTipo().getId());
        assertEquals(input.getParticipants().get(0).getContactDetails().get(1).getMobileContact().getNumber(), result.getData().getCliente().getDatosContacto().get(1).getValor());

        assertEquals(input.getPaymentMethod().getId(), result.getData().getDatosPago().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getId(), result.getData().getDatosPago().getFrecuencia());
        assertEquals(Integer.valueOf(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay()), result.getData().getDatosPago().getDia());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getData().getImporte().getMonto());
        assertEquals(input.getRelatedContracts().get(0).getNumber(), result.getData().getCuentaCargo());
        assertEquals(input.getRelatedContracts().get(0).getNumberType().getId(), result.getData().getTipoCuentaCargo().getId());

        //STORED
        assertEquals(input.getDeliveries().get(0).getId(), result.getData().getDatosEntrega().get(0).getId());
        assertEquals(input.getDeliveries().get(0).getServiceType().getId(), result.getData().getDatosEntrega().get(0).getTipo().getId());
        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getData().getDatosEntrega().get(0).getContacto().getTipoContacto().getId());
        assertEquals(input.getDeliveries().get(0).getContact().getId(), result.getData().getDatosEntrega().get(0).getContacto().getId());
        assertEquals(input.getDeliveries().get(0).getContact().getContact().getAddress(), result.getData().getDatosEntrega().get(0).getContacto().getValor());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getData().getDatosEntrega().get(0).getDireccion().getTipo().getId());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getData().getDatosEntrega().get(0).getDireccion().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getData().getDatosEntrega().get(0).getDestino().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getName(), result.getData().getDatosEntrega().get(0).getDestino().getNombre());
        assertEquals(input.getDeliveries().get(0).getBranch().getId(), result.getData().getDatosEntrega().get(0).getOficina().getId());
        assertEquals(input.getDeliveries().get(0).getBranch().getName(), result.getData().getDatosEntrega().get(0).getOficina().getNombre());

        //SPECIFIC
        assertEquals(input.getDeliveries().get(1).getId(), result.getData().getDatosEntrega().get(1).getId());
        assertEquals(input.getDeliveries().get(1).getServiceType().getId(), result.getData().getDatosEntrega().get(1).getTipo().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContactType(), result.getData().getDatosEntrega().get(1).getContacto().getTipoContacto().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getId(), result.getData().getDatosEntrega().get(1).getContacto().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContact().getContactDetailType(), result.getData().getDatosEntrega().get(1).getContacto().getTipo().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContact().getAddress(), result.getData().getDatosEntrega().get(1).getContacto().getValor());
        assertEquals(input.getDeliveries().get(1).getAddress().getAddressType(), result.getData().getDatosEntrega().get(1).getDireccion().getTipo().getId());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0), result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getTipo().getId());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getCode(), result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getCodigo());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getName(), result.getData().getDatosEntrega().get(1).getDireccion().getDireccionDetallada().get(0).getNombre());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getFormattedAddress(), result.getData().getDatosEntrega().get(1).getDireccion().getDireccionCompleta());
        assertEquals(input.getDeliveries().get(1).getDestination().getId(), result.getData().getDatosEntrega().get(1).getDestino().getId());
        assertEquals(input.getDeliveries().get(1).getDestination().getName(), result.getData().getDatosEntrega().get(1).getDestino().getNombre());
        assertEquals(input.getDeliveries().get(1).getBranch().getId(), result.getData().getDatosEntrega().get(1).getOficina().getId());
        assertEquals(input.getDeliveries().get(1).getBranch().getName(), result.getData().getDatosEntrega().get(1).getOficina().getNombre());

        assertEquals(input.getContractingBranch().getId(), result.getData().getOficina().getId());
        assertEquals(input.getContractingBranch().getName(), result.getData().getOficina().getNombre());
        assertEquals(input.getLoyaltyProgram().getId(), result.getData().getProgramaBeneficios().getId());
        assertEquals(input.getLoyaltyProgram().getDescription(), result.getData().getProgramaBeneficios().getNombre());
        assertEquals(Integer.valueOf(input.getProposalStatus()), result.getData().getEstado());
        assertEquals(input.getOfferId(), result.getData().getOferta().getId());
    }

    @Test
    public void mapInWithoutNonMandatoryParametersTest() throws IOException {
        InputCreateCardReports input = mock.getInputCreateCardReportsMock();

        input.setBrandAssociation(null);
        input.getProduct().setName(null);
        input.getProduct().setSubproduct(null);
        input.setHasPrintedHolderName(null);
        input.getParticipants().get(0).setMiddleName(null);
        input.getParticipants().get(0).setSecondLastName(null);
        input.getParticipants().get(0).setContactDetails(null);
        input.setPaymentMethod(null);
        input.setGrantedCredits(null);
        input.setRelatedContracts(null);
        input.setDeliveries(null);
        input.setContractingBranch(null);
        input.setLoyaltyProgram(null);
        input.setOfferId(null);

        ModelCreateCardReportsRequest result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData().getId());
        assertNotNull(result.getSecurity());
        assertNotNull(result.getSecurity().getUsuario());
        assertNotNull(result.getSecurity().getPassword());
        assertNotNull(result.getData().getCanal());
        assertNotNull(result.getData().getTipoTarjeta());
        assertNotNull(result.getData().getTipoTarjeta().getId());
        assertNotNull(result.getData().getNumeroProducto());
        assertNotNull(result.getData().getTipoNumeroTarjeta());
        assertNotNull(result.getData().getTipoNumeroTarjeta().getId());
        assertNull(result.getData().getMarca());
        assertNotNull(result.getData().getProducto());
        assertNotNull(result.getData().getProducto().getId());
        assertNull(result.getData().getProducto().getNombre());
        assertNull(result.getData().getProducto().getSubproducto());
        assertNull(result.getData().getSubproducto());
        assertNotNull(result.getData().getFlagMonedaPrincipal());
        assertNull(result.getData().getFlagInnominada());
        assertNotNull(result.getData().getCliente());
        assertNotNull(result.getData().getCliente().getCodigoCentral());
        assertNotNull(result.getData().getCliente().getPrimerNombre());
        assertNull(result.getData().getCliente().getSegundoNombre());
        assertNotNull(result.getData().getCliente().getApellidoPaterno());
        assertNull(result.getData().getCliente().getApellidoMaterno());
        assertNotNull(result.getData().getCliente().getDocumentoIdentidad());
        assertNotNull(result.getData().getCliente().getDocumentoIdentidad().getId());
        assertNotNull(result.getData().getCliente().getDocumentoIdentidad().getNumero());
        assertNull(result.getData().getCliente().getDatosContacto());
        assertNull(result.getData().getDatosPago());
        assertNotNull(result.getData().getImporte());
        assertNotNull(result.getData().getImporte().getMoneda());
        assertNull(result.getData().getImporte().getMonto());
        assertNull(result.getData().getCuentaCargo());
        assertNull(result.getData().getTipoCuentaCargo());
        assertNull(result.getData().getDatosEntrega());
        assertNull(result.getData().getOficina());
        assertNull(result.getData().getProgramaBeneficios());
        assertNotNull(result.getData().getEstado());
        assertNull(result.getData().getOferta());

        assertEquals(input.getCardId(), result.getData().getId());
        assertEquals(input.getUser(), result.getSecurity().getUsuario());
        assertEquals(input.getPassword(), result.getSecurity().getPassword());
        assertEquals(input.getChannel(), result.getData().getCanal());
        assertEquals(input.getCardType().getId(), result.getData().getTipoTarjeta().getId());
        assertEquals(input.getNumber(), result.getData().getNumeroProducto());
        assertEquals(input.getNumberType().getId(), result.getData().getTipoNumeroTarjeta().getId());
        assertEquals(input.getProduct().getId(), result.getData().getProducto().getId());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getData().getImporte().getMoneda());
        assertEquals(Converter.booleanToInteger(input.getCurrencies().get(0).getIsMajor()), result.getData().getFlagMonedaPrincipal());
        assertEquals(input.getParticipants().get(0).getId(), result.getData().getCliente().getCodigoCentral());
        assertEquals(input.getParticipants().get(0).getFirstName(), result.getData().getCliente().getPrimerNombre());
        assertEquals(input.getParticipants().get(0).getLastName(), result.getData().getCliente().getApellidoPaterno());
        assertEquals(input.getParticipants().get(0).getIdentityDocuments().get(0).getNumber(), result.getData().getCliente().getDocumentoIdentidad().getNumero());
        assertEquals(input.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType().getId(), result.getData().getCliente().getDocumentoIdentidad().getId());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getData().getImporte().getMoneda());
        assertEquals(Integer.valueOf(input.getProposalStatus()), result.getData().getEstado());
    }
}
