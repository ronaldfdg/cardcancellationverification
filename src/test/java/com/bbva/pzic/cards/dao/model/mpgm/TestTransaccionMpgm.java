package com.bbva.pzic.cards.dao.model.mpgm;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPGM</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpgm {

    @InjectMocks
    private TransaccionMpgm transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpgm peticion = new PeticionTransaccionMpgm();
        RespuestaTransaccionMpgm respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpgm.class, RespuestaTransaccionMpgm.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionMpgm result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}