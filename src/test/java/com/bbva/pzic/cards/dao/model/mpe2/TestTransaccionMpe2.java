package com.bbva.pzic.cards.dao.model.mpe2;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPE2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpe2 {

    @InjectMocks
    private TransaccionMpe2 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);


    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpe2 peticion = new PeticionTransaccionMpe2();
        RespuestaTransaccionMpe2 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpe2.class, RespuestaTransaccionMpe2.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpe2 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}