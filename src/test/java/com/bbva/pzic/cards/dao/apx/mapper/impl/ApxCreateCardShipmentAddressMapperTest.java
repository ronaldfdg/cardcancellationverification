package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntSpecificAddressType;
import com.bbva.pzic.cards.business.dto.DTOIntStoredAddressType;
import com.bbva.pzic.cards.business.dto.InputCreateCardShipmentAddress;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardShipmentAddressMapper;
import com.bbva.pzic.cards.dao.model.pecpt002_1.PeticionTransaccionPecpt002_1;
import com.bbva.pzic.cards.dao.model.pecpt002_1.RespuestaTransaccionPecpt002_1;
import com.bbva.pzic.cards.dao.model.pecpt002_1.mock.Pecpt002_1Stubs;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.util.Constants.SPECIFIC;
import static com.bbva.pzic.cards.util.Constants.STORED;
import static org.junit.Assert.*;

public class ApxCreateCardShipmentAddressMapperTest {

    private IApxCreateCardShipmentAddressMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxCreateCardShipmentAddressMapper();
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressFullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());

        assertEquals(input.getShipmentId(), result.getShipmentid());
        assertEquals(input.getAddressType(), result.getEntityin().getAddresstype());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getFormattedAddress(), result.getEntityin().getLocation().getFormattedaddress());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getLocationTypes().get(0), result.getEntityin().getLocation().getLocationtypes().get(0).getLocationtype());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().get(0), result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().get(0).getComponenttype());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode(), result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName(), result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().get(0), result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().get(0).getComponenttype());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode(), result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName(), result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getGeolocation().getLatitude(), result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertEquals(((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getGeolocation().getLongitude(), result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).setLocation(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getLocation());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationFormattedAddressTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().setFormattedAddress(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationLocationTypesTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().setLocationTypes(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNull(result.getEntityin().getLocation().getLocationtypes());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().setAddressComponents(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNull(result.getEntityin().getLocation().getAddresscomponents());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsComponentTypesTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(0).setComponentTypes(null);
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(1).setComponentTypes(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsCodeTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(0).setCode(null);
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(1).setCode(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsNameTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(0).setName(null);
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getAddressComponents().get(1).setName(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressLocationGeolocationNullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().setGeolocation(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNull(result.getEntityin().getLocation().getGeolocation());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressLocationGeolocationLongitudeNullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getGeolocation().setLongitude(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInCreateCardSpecificShipmentAddressLocationGeolocationLatitudeNullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(SPECIFIC);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardSpecificShipmentAddress());
        ((DTOIntSpecificAddressType) input.getShipmentAddress()).getLocation().getGeolocation().setLatitude(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getLocation().getFormattedaddress());
        assertNotNull(result.getEntityin().getLocation().getLocationtypes());
        assertEquals(1, result.getEntityin().getLocation().getLocationtypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents());
        assertEquals(2, result.getEntityin().getLocation().getAddresscomponents().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes());
        assertEquals(1, result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().size());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode());
        assertNotNull(result.getEntityin().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName());
        assertNotNull(result.getEntityin().getLocation().getGeolocation());
        assertNull(result.getEntityin().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInStoredShipmentAddressFullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(STORED);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardStoredShipmentAddress());
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getId());
        assertNotNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getDestination().getId());

        assertEquals(input.getShipmentId(), result.getShipmentid());
        assertEquals(input.getAddressType(), result.getEntityin().getAddresstype());
        assertEquals(input.getShipmentAddress().getId(), result.getEntityin().getId());
        assertEquals(((DTOIntStoredAddressType) input.getShipmentAddress()).getDestination().getId(), result.getEntityin().getDestination().getId());
    }

    @Test
    public void mapInStoredShipmentAddressIdNullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(STORED);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardStoredShipmentAddress());
        input.getShipmentAddress().setId(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getLocation());
        assertNull(result.getEntityin().getId());
        assertNotNull(result.getEntityin().getDestination());
        assertNotNull(result.getEntityin().getDestination().getId());
    }

    @Test
    public void mapInStoredShipmentAddressDestinationNullTest() throws IOException {
        InputCreateCardShipmentAddress input = EntityMock.getInstance().buildInputCreateCardShipmentAddress(STORED);
        input.setShipmentAddress(EntityMock.getInstance().buildInputCreateCardStoredShipmentAddress());
        ((DTOIntStoredAddressType) input.getShipmentAddress()).setDestination(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getId());
        assertNull(result.getEntityin().getDestination());
    }

    @Test
    public void mapInStoredShipmentAddressDestinationIdNullTest() throws IOException {
        InputCreateCardShipmentAddress inputShipmentAddress = EntityMock.getInstance().buildInputCreateCardShipmentAddress(STORED);
        DTOIntStoredAddressType input = EntityMock.getInstance().buildInputCreateCardStoredShipmentAddress();
        inputShipmentAddress.setShipmentAddress(input);

        input.getDestination().setId(null);
        PeticionTransaccionPecpt002_1 result = mapper.mapIn(inputShipmentAddress);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getAddresstype());
        assertNull(result.getEntityin().getLocation());
        assertNotNull(result.getEntityin().getId());
        assertNotNull(result.getEntityin().getDestination());
        assertNull(result.getEntityin().getDestination().getId());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressFullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());

        assertEquals(input.getEntityout().getId(), result.getId());
        assertEquals(input.getEntityout().getAddresstype(), result.getAddressType());
        assertEquals(input.getEntityout().getLocation().getFormattedaddress(), result.getLocation().getFormattedAddress());
        assertEquals(input.getEntityout().getLocation().getLocationtypes().get(0).getLocationtype(), result.getLocation().getLocationTypes().get(0));
        assertEquals(input.getEntityout().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().get(0).getComponenttype(), result.getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getEntityout().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode(), result.getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getEntityout().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName(), result.getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getEntityout().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().get(0).getComponenttype(), result.getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(input.getEntityout().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode(), result.getLocation().getAddressComponents().get(1).getCode());
        assertEquals(input.getEntityout().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName(), result.getLocation().getAddressComponents().get(1).getName());
        assertEquals(input.getEntityout().getLocation().getGeolocation().getLatitude(), result.getLocation().getGeolocation().getLatitude());
        assertEquals(input.getEntityout().getLocation().getGeolocation().getLongitude(), result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().setLocation(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNull(result.getLocation());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationFormattedAddressTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().setFormattedaddress(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationLocationTypesTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().setLocationtypes(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNull(result.getLocation().getLocationTypes());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().setAddresscomponents(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNull(result.getLocation().getAddressComponents());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsComponentTypesTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().getAddresscomponents().get(0).getAddresscomponent().setComponenttypes(null);
        input.getEntityout().getLocation().getAddresscomponents().get(1).getAddresscomponent().setComponenttypes(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsCodeTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().getAddresscomponents().get(0).getAddresscomponent().setCode(null);
        input.getEntityout().getLocation().getAddresscomponents().get(1).getAddresscomponent().setCode(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressWithNullLocationAddressComponentsNameTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().getAddresscomponents().get(0).getAddresscomponent().setName(null);
        input.getEntityout().getLocation().getAddresscomponents().get(1).getAddresscomponent().setName(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressLocationGeolocationNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().setGeolocation(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNull(result.getLocation().getGeolocation());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressLocationGeolocationLongitudeNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().getGeolocation().setLongitude(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNotNull(result.getLocation().getGeolocation().getLatitude());
        assertNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardSpecificShipmentAddressLocationGeolocationLatitudeNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        input.getEntityout().getLocation().getGeolocation().setLatitude(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getLocation());
        assertNotNull(result.getLocation().getFormattedAddress());
        assertNotNull(result.getLocation().getLocationTypes());
        assertEquals(1, result.getLocation().getLocationTypes().size());
        assertNotNull(result.getLocation().getAddressComponents());
        assertEquals(2, result.getLocation().getAddressComponents().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getLocation().getGeolocation());
        assertNull(result.getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapOutCreateCardStoredShipmentAddressFullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildStoredShipmentAddressResponse();
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());

        assertEquals(input.getEntityout().getId(), result.getId());
        assertEquals(input.getEntityout().getAddresstype(), result.getAddressType());
        assertEquals(input.getEntityout().getDestination().getId(), result.getDestination().getId());
        assertEquals(input.getEntityout().getDestination().getName(), result.getDestination().getName());
    }

    @Test
    public void mapOutCreateCardStoredShipmentAddressDestinationNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildStoredShipmentAddressResponse();
        input.getEntityout().setDestination(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNull(result.getDestination());
    }

    @Test
    public void mapOutCreateCardStoredShipmentAddressDestinationIdNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildStoredShipmentAddressResponse();
        input.getEntityout().getDestination().setId(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getDestination());
        assertNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());
    }

    @Test
    public void mapOutCreateCardStoredShipmentAddressDestinationNameNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildStoredShipmentAddressResponse();
        input.getEntityout().getDestination().setName(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNull(result.getDestination().getName());
    }

    @Test
    public void mapOutCreateCardStoredShipmentAddressAddressTypeNullTest() throws IOException {
        RespuestaTransaccionPecpt002_1 input = Pecpt002_1Stubs.getInstance().buildStoredShipmentAddressResponse();
        input.getEntityout().setAddresstype(null);
        ShipmentAddress result = mapper.mapOut(input);

        assertNull(result);
    }
}
