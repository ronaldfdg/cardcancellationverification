package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.model.mpw1.mock.FormatsMpw1Mock;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardMapperV0Test {

    private final EntityMock entityMock = EntityMock.getInstance();
    private final FormatsMpw1Mock formatsMpw1Mock = new FormatsMpw1Mock();
    @InjectMocks
    private TxCreateCardMapperV0 mapper;
    @Mock
    private Translator translator;
    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.numberType.id", "1")).thenReturn(CARD_TYPE_ID_KEY_TESTED);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.cardType.id", "D")).thenReturn(CARD_CARDTYPE_ID_KEY_TESTED);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", "V")).thenReturn(CARD_BRAND_ASSOCIATION_ID_KEY_TESTED);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.physicalSupport.id", "S")).thenReturn(PHYSICAL_SUPPORT_ID);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.status.id", "I")).thenReturn(CARD_STATUS_KEY_TESTED);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.relatedContracts.relatedContract.numberType.id", "1")).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.deliveriesManagement.id", "001")).thenReturn(CARD_DELIVERIES_MANAGEMENT_STATEMENT);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.deliveriesManagement.id", "002")).thenReturn(CARD_DELIVERIES_MANAGEMENT_CARD);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.deliveriesManagement.id", "003")).thenReturn(CARD_DELIVERIES_MANAGEMENT_STICKER);

        Mockito.when(cypherTool.encrypt("ES90 0182 1642 0302 0153 5555", AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0)).thenReturn(CARD_ENCRYPT_NUCOREL_KEY_TESTED);
        Mockito.when(cypherTool.encrypt("4940 1970 8307 0361", AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0)).thenReturn(CARD_ENCRYPT_NUMTARJ_KEY_TESTED);
    }

    @Test
    public void mapInTest() throws IOException {
        InputCreateCard inputCreateCard = entityMock.getInputCreateCardFull();
        FormatoMPME0W1 result = mapper.mapIn(inputCreateCard);

        assertNotNull(result);

        assertEquals(inputCreateCard.getCard().getParticipantId(), result.getNumclie());
        assertEquals(inputCreateCard.getCard().getCardTypeId(), result.getTiprodf());
        assertEquals(inputCreateCard.getCard().getPhysicalSupportId(), result.getSopfisi());
        assertEquals(inputCreateCard.getCard().getRelatedContractIdLinkedWith(), result.getCtacarg());
        assertEquals(inputCreateCard.getCard().getTitleId(), result.getMctarje());
        assertEquals(inputCreateCard.getCard().getCutOffDay(), result.getFcierre());
        assertEquals(inputCreateCard.getCard().getPaymentMethodEndDay(), result.getFcargo());
        assertEquals(inputCreateCard.getCard().getStatementType(), result.getIndeecc());
        assertEquals(inputCreateCard.getCard().getGrantedCreditsAmount(), result.getLincred());
        assertEquals(inputCreateCard.getCard().getGrantedCreditsCurrency(), result.getDivisa());
        assertEquals(inputCreateCard.getCard().getDestinationIdHome(), result.getNumdom2());
        assertEquals(inputCreateCard.getCard().getDeliveryAddressConstant(), result.getCodser2());
        assertEquals(inputCreateCard.getCard().getMembershipsNumber(), result.getCodpafr());
        assertEquals(inputCreateCard.getCard().getSupportContractType(), result.getTcontra());
        assertEquals(inputCreateCard.getCard().getDeliveriesManagementServiceTypeIdStatement(), result.getCodseri());
        assertEquals(inputCreateCard.getCard().getDeliveriesManagementContactDetailIdStatement(), result.getCorafil());
        assertEquals(inputCreateCard.getCard().getDeliveriesManagementAddressIdStatement(), result.getNumdomi());
        assertEquals(inputCreateCard.getCard().getDeliveryManagementConstant(), result.getIndcsms());
        assertEquals(inputCreateCard.getCard().getContractingBusinessAgentId(), result.getRegisco());
        assertEquals(inputCreateCard.getCard().getContractingBusinessAgentOriginId(), result.getIndorig());
        assertEquals(inputCreateCard.getCard().getMarketBusinessAgentId(), result.getRegisve());
        assertEquals(inputCreateCard.getCard().getImages().get(0).getId(), result.getDistarj());
        assertEquals(inputCreateCard.getCard().getOfferId(), result.getIdoffer());
        assertEquals(inputCreateCard.getCard().getManagementBranchId(), result.getCodgest());
        assertEquals(inputCreateCard.getCard().getContractingBranchId(), result.getCodofig());
    }

    @Test
    public void mapInWithoutSupportContractType() throws IOException {
        InputCreateCard inputCreateCard = entityMock.getInputCreateCardFull();
        inputCreateCard.getCard().setSupportContractType(null);
        FormatoMPME0W1 formatoMPME0W1 = mapper.mapIn(inputCreateCard);

        assertNotNull(formatoMPME0W1);
        assertNull(formatoMPME0W1.getTcontra());
    }

    @Test
    public void mapOutTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        Card card = mapper.mapOut(formatoMPMS1W1);

        assertNotNull(card);
        assertEquals(formatoMPMS1W1.getNumtarj(), card.getNumber());
        assertEquals(CARD_ENCRYPT_NUMTARJ_KEY_TESTED, card.getCardId());
        assertEquals(CARD_TYPE_ID_KEY_TESTED, card.getNumberType().getId());
        assertEquals(formatoMPMS1W1.getDstiptj(), card.getNumberType().getName());
        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, card.getCardType().getId());
        assertEquals(formatoMPMS1W1.getDsprodf(), card.getCardType().getName());
        assertEquals(CARD_BRAND_ASSOCIATION_ID_KEY_TESTED, card.getBrandAssociation().getId());
        assertEquals(formatoMPMS1W1.getNomatar(), card.getBrandAssociation().getName());
        assertEquals(PHYSICAL_SUPPORT_ID, card.getPhysicalSupport().getId());
        assertEquals(formatoMPMS1W1.getDsopfis(), card.getPhysicalSupport().getName());
        assertEquals(formatoMPMS1W1.getFecvenc(), card.getExpirationDate());
        assertEquals(formatoMPMS1W1.getNombcli(), card.getHolderName());
        assertEquals(formatoMPMS1W1.getMonpri(), card.getCurrencies().get(0).getCurrency());
        assertTrue(card.getCurrencies().get(0).getIsMajor());
        assertEquals(formatoMPMS1W1.getMonsec(), card.getCurrencies().get(1).getCurrency());
        assertFalse(card.getCurrencies().get(1).getIsMajor());
        assertEquals(formatoMPMS1W1.getIlimcre(), card.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1W1.getDlimcre(), card.getGrantedCredits().get(0).getCurrency());
        assertEquals(CARD_STATUS_KEY_TESTED, card.getStatus().getId());
        assertEquals(formatoMPMS1W1.getDestarj(), card.getStatus().getName());
        assertEquals(formatoMPMS1W1.getFecalta(), card.getOpeningDate());
        assertEquals(formatoMPMS1W1.getIdcorel(), card.getRelatedContracts().get(0).getRelatedContractId());
        assertEquals(formatoMPMS1W1.getNucorel(), card.getRelatedContracts().get(0).getNumber());
        assertEquals(CARD_ENCRYPT_NUCOREL_KEY_TESTED, card.getRelatedContracts().get(0).getContractId());
        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, card.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(formatoMPMS1W1.getDetcore(), card.getRelatedContracts().get(0).getNumberType().getName());
        assertEquals(CARD_DELIVERIES_MANAGEMENT_STATEMENT, card.getDeliveriesManagement().get(0).getServiceType().getId());
        assertEquals(formatoMPMS1W1.getDesseri(), card.getDeliveriesManagement().get(0).getServiceType().getName());
        assertEquals(formatoMPMS1W1.getNumdomi(), card.getDeliveriesManagement().get(0).getAddress().getId());
        assertEquals(formatoMPMS1W1.getDesdomi(), card.getDeliveriesManagement().get(0).getAddress().getAddressName());
        assertEquals(CARD_DELIVERIES_MANAGEMENT_CARD, card.getDeliveriesManagement().get(1).getServiceType().getId());
        assertEquals(formatoMPMS1W1.getDesser2(), card.getDeliveriesManagement().get(1).getServiceType().getName());
        assertEquals(formatoMPMS1W1.getNumdom2(), card.getDeliveriesManagement().get(1).getAddress().getId());
        assertEquals(formatoMPMS1W1.getDesdom2(), card.getDeliveriesManagement().get(1).getAddress().getAddressName());
        assertEquals(CARD_DELIVERIES_MANAGEMENT_STICKER, card.getDeliveriesManagement().get(2).getServiceType().getId());
        assertEquals(formatoMPMS1W1.getDesser3(), card.getDeliveriesManagement().get(2).getServiceType().getName());
        assertEquals(formatoMPMS1W1.getNumdom3(), card.getDeliveriesManagement().get(2).getAddress().getId());
        assertEquals(formatoMPMS1W1.getDesdom3(), card.getDeliveriesManagement().get(2).getAddress().getAddressName());

        assertNotNull(card.getDelivery());
        assertNotNull(card.getDelivery().getAddress());
        assertEquals(formatoMPMS1W1.getCodofen(), card.getDelivery().getAddress().getId());
        assertEquals(formatoMPMS1W1.getDesofen(), card.getDelivery().getAddress().getAddressName());
    }

    @Test
    public void mapOut_EmptyTest() {
        Card card = mapper.mapOut(new FormatoMPMS1W1());
        assertNotNull(card);
        assertTrue(card.getCurrencies().get(0).getIsMajor());
        assertFalse(card.getCurrencies().get(1).getIsMajor());
    }

    @Test
    public void mapOut_WithoutNumberTypeNameTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setDstiptj(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(formatoMPMS1W1.getDstiptj(), card.getNumberType().getName());
    }

    @Test
    public void mapOut_WithoutCardTypeNameTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setDsprodf(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(formatoMPMS1W1.getDsprodf(), card.getCardType().getName());
    }

    @Test
    public void mapOut_WithoutBrandAssociationNameTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setNomatar(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(formatoMPMS1W1.getNomatar(), card.getBrandAssociation().getName());
    }

    @Test
    public void mapOut_WithoutPhysicalSupportNameTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setDsopfis(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(formatoMPMS1W1.getDsopfis(), card.getPhysicalSupport().getName());
    }

    @Test
    public void mapOut_WithoutGrantedCreditsTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setIlimcre(null);
        formatoMPMS1W1.setDlimcre(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(card.getGrantedCredits());
    }

    @Test
    public void mapOut_WithoutStatusNameTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setDestarj(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(card.getStatus().getName());
    }

    @Test
    public void mapOut_WithoutRelatedContractNumberTypeNameTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setIdcorel(null);
        formatoMPMS1W1.setDetcore(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNull(card.getRelatedContracts().get(0).getNumberType().getName());
    }

    @Test
    public void mapOut_WithoutGrantedCreditsLimcreNullTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setIlimcre(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNotNull(card.getGrantedCredits().get(0).getCurrency());
    }

    @Test
    public void mapOut_WithoutGrantedCreditsDkincreNullTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = formatsMpw1Mock.getFormatoMPMS1W1();
        formatoMPMS1W1.setDlimcre(null);

        Card card = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(card);
        assertNotNull(card.getGrantedCredits().get(0).getAmount());
    }

    @Test
    public void mapOut_WithoutDeliveriesManagementTest() {
        FormatoMPMS1W1 formatoMPMS1W1 = new FormatoMPMS1W1();
        Card result = mapper.mapOut(formatoMPMS1W1);
        assertNotNull(result);
        assertNull(result.getDeliveriesManagement());
    }
}
