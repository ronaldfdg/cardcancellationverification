package com.bbva.pzic.cards.dao.model.mpl1;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPGM</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpl1 {

    @InjectMocks
    private TransaccionMpl1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpl1 peticion = new PeticionTransaccionMpl1();
        RespuestaTransaccionMpl1 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpl1.class, RespuestaTransaccionMpl1.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionMpl1 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}
