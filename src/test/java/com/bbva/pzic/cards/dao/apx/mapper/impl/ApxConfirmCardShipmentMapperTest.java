package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxConfirmCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt005_1.PeticionTransaccionPecpt005_1;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ApxConfirmCardShipmentMapperTest {

    private IApxConfirmCardShipmentMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxConfirmCardShipmentMapper();
    }

    @Test
    public void mapInFullTest() {
        InputConfirmCardShipment input = EntityMock.getInstance().buildInputConfirmCardShipment();
        PeticionTransaccionPecpt005_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getBiometricid());
        assertNotNull(result.getEntityin().getCurrentaddress());
        assertNotNull(result.getEntityin().getCurrentaddress().getLocation());
        assertNotNull(result.getEntityin().getCurrentaddress().getLocation().getGeolocation());
        assertNotNull(result.getEntityin().getCurrentaddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getEntityin().getCurrentaddress().getLocation().getGeolocation().getLongitude());
        assertNotNull(result.getEntityin().getShipmentaddress());
        assertNotNull(result.getEntityin().getShipmentaddress().getId());

        assertEquals(input.getShipmentId(), result.getShipmentid());
        assertEquals(input.getBiometricId(), result.getEntityin().getBiometricid());
        assertEquals(input.getCurrentAddressLocationGeolocationLatitude(), result.getEntityin().getCurrentaddress().getLocation().getGeolocation().getLatitude());
        assertEquals(input.getCurrentAddressLocationGeolocationLongitude(), result.getEntityin().getCurrentaddress().getLocation().getGeolocation().getLongitude());
        assertEquals(input.getShipmentAddressId(), result.getEntityin().getShipmentaddress().getId());
    }
}
