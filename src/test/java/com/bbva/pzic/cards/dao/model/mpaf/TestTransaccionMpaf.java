package com.bbva.pzic.cards.dao.model.mpaf;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Test de la transacci&oacute;n <code>MPAF</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpaf {

    private static final Log LOG = LogFactory.getLog(TestTransaccionMpaf.class);

    @InjectMocks
    private TransaccionMpaf transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {

        PeticionTransaccionMpaf peticion = new PeticionTransaccionMpaf();

        /*
         * TODO: poblar la peticion con valores adecuados
         */

        try {
            LOG.info("Invocando transaccion, peticion: " + peticion);
            RespuestaTransaccionMpaf respuesta = transaccion.invocar(peticion);
            LOG.info("Recibida respuesta: " + respuesta);

            Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpaf.class, RespuestaTransaccionMpaf.class, peticion)).thenReturn(respuesta);
            RespuestaTransaccionMpaf response = transaccion.invocar(peticion);

            assertEquals(respuesta, response);

        } catch (ExcepcionRespuestaHost e) {
            LOG.error("Error recibido desde host, codigoError: " + e.getCodigoError() + ", descripcion: " + e.getMessage());
        } catch (ExcepcionTransporte e) {
            LOG.error("Error de transporte", e);
        }
    }
}