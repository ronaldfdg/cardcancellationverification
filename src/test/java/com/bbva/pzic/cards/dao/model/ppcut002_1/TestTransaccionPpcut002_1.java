package com.bbva.pzic.cards.dao.model.ppcut002_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PPCUT002</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPpcut002_1 {

    @InjectMocks
    private TransaccionPpcut002_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPpcut002_1 rq = new PeticionTransaccionPpcut002_1();
        RespuestaTransaccionPpcut002_1 rs = new RespuestaTransaccionPpcut002_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPpcut002_1.class, RespuestaTransaccionPpcut002_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPpcut002_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}