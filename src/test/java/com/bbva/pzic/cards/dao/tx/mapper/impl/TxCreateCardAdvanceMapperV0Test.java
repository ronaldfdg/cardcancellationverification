package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.dao.model.mprt.FormatoMPRMRT0;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardAdvanceMapperV0;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class TxCreateCardAdvanceMapperV0Test {

    private ITxCreateCardAdvanceMapperV0 mapper;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void setUp() {
        mapper = new TxCreateCardAdvanceMapperV0();
    }

    @Test
    public void mapInTest() throws IOException {
        InputCreateCard inputCreateCard = entityMock.getInputCreateCardAdvanceFull();
        FormatoMPRMRT0 result = mapper.mapIn(inputCreateCard);

        assertNotNull(result);
        assertNotNull(result.getCtacarg());
        assertNotNull(result.getNucorel());
        assertNotNull(result.getTarinom());
        assertNotNull(result.getNumclie());
        assertNotNull(result.getIdtesms());
        assertNotNull(result.getIndcsms());
    }

    @Test
    public void mapInTestEmpty() {
        InputCreateCard inputCreateCard = new InputCreateCard();
        FormatoMPRMRT0 result = mapper.mapIn(inputCreateCard);

        assertNotNull(result);
        assertNull(result.getCtacarg());
        assertNull(result.getNucorel());
        assertNull(result.getTarinom());
        assertNull(result.getNumclie());
        assertNull(result.getIdtesms());
        assertNull(result.getIndcsms());
    }
}
