package com.bbva.pzic.cards.dao.model.mpgg;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPGG</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpgg {

    @InjectMocks
    private TransaccionMpgg transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpgg peticion = new PeticionTransaccionMpgg();
        RespuestaTransaccionMpgg respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpgg.class, RespuestaTransaccionMpgg.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionMpgg result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);

    }
}