package com.bbva.pzic.cards.dao.model.pecpt004_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PECPT004</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPecpt004_1 {

    @InjectMocks
    private TransaccionPecpt004_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPecpt004_1 rq = new PeticionTransaccionPecpt004_1();
        RespuestaTransaccionPecpt004_1 rs = new RespuestaTransaccionPecpt004_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPecpt004_1.class, RespuestaTransaccionPecpt004_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPecpt004_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}
