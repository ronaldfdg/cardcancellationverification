package com.bbva.pzic.cards.dao.model.mp6i;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MP6I</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMp6i {

    @InjectMocks
    private TransaccionMp6i transaccion;
    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionMp6i peticion = new PeticionTransaccionMp6i();
        RespuestaTransaccionMp6i respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMp6i.class, RespuestaTransaccionMp6i.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMp6i result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}