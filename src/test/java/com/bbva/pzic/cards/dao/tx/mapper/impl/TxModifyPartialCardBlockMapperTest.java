package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.dao.model.mpb4.FormatoMPM0B4E;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyPartialCardBlockMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TxModifyPartialCardBlockMapperTest {

    private ITxModifyPartialCardBlockMapper txModifyPartialCardBlockMapper;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void init() {
        txModifyPartialCardBlockMapper = new TxModifyPartialCardBlockMapper();
    }

    @Test
    public void mapInFullTest() {
        final DTOIntBlock dtoIntBlock = entityMock.buildDTOIntBlock();
        FormatoMPM0B4E formatoMPM0B4E = txModifyPartialCardBlockMapper.mapIn(dtoIntBlock);

        assertNotNull(formatoMPM0B4E.getIdetarj());
        assertNotNull(formatoMPM0B4E.getIdebloq());
        assertNotNull(formatoMPM0B4E.getIderazo());
        assertNotNull(formatoMPM0B4E.getIdactbl());

        assertEquals(dtoIntBlock.getCard().getCardId(), formatoMPM0B4E.getIdetarj());
        assertEquals(dtoIntBlock.getBlockId(), formatoMPM0B4E.getIdebloq());
        assertEquals(dtoIntBlock.getReasonId(), formatoMPM0B4E.getIderazo());
        assertEquals(dtoIntBlock.getIsActive(), formatoMPM0B4E.getIdactbl());
    }

    @Test
    public void mapInWithoutisActiveTest() {
        final DTOIntBlock dtoIntBlock = entityMock.buildDTOIntBlock();
        dtoIntBlock.setReasonId(null);
        FormatoMPM0B4E formatoMPM0B4E = txModifyPartialCardBlockMapper.mapIn(dtoIntBlock);

        assertNotNull(formatoMPM0B4E.getIdetarj());
        assertNotNull(formatoMPM0B4E.getIdebloq());
        assertNull(formatoMPM0B4E.getIderazo());
        assertNotNull(formatoMPM0B4E.getIdactbl());

        assertEquals(dtoIntBlock.getCard().getCardId(), formatoMPM0B4E.getIdetarj());
        assertEquals(dtoIntBlock.getBlockId(), formatoMPM0B4E.getIdebloq());
        assertEquals(dtoIntBlock.getIsActive(), formatoMPM0B4E.getIdactbl());
    }

    @Test
    public void mapInWithoutReasonIdTest() {
        final DTOIntBlock dtoIntBlock = entityMock.buildDTOIntBlock();
        FormatoMPM0B4E formatoMPM0B4E = txModifyPartialCardBlockMapper.mapIn(dtoIntBlock);

        assertNotNull(formatoMPM0B4E.getIdetarj());
        assertNotNull(formatoMPM0B4E.getIdebloq());
        assertNotNull(formatoMPM0B4E.getIderazo());
        assertNotNull(formatoMPM0B4E.getIdactbl());

        assertEquals(dtoIntBlock.getCard().getCardId(), formatoMPM0B4E.getIdetarj());
        assertEquals(dtoIntBlock.getBlockId(), formatoMPM0B4E.getIdebloq());
        assertEquals(dtoIntBlock.getReasonId(), formatoMPM0B4E.getIderazo());
        assertEquals(dtoIntBlock.getIsActive(), formatoMPM0B4E.getIdactbl());
    }
}