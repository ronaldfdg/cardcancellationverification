package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Limits;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMENGL;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMS1GL;
import com.bbva.pzic.cards.dao.model.mpgl.mock.FormatoMPMS1GLMock;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListCardLimitsMapperV0Test {

    @InjectMocks
    private TxListCardLimitsMapperV0 mapper;

    @Mock
    private Translator translator;

    private void setEnumOut() {
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.limits.id", "1")).thenReturn("PREPAID_LIMIT_MONTHLY");
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.limits.id", "2")).thenReturn("PREPAID_LIMIT_WEEKLY");
    }

    @Test
    public void mapInFullTest() {
        DTOIntCard input = EntityMock.getInstance().getDTOIntCard();
        FormatoMPMENGL result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertEquals(input.getCardId(), result.getIdetarj());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPMENGL result = mapper.mapIn(new DTOIntCard());
        assertNotNull(result);
        assertNull(result.getIdetarj());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        List<FormatoMPMS1GL> formatoMPMS1GLs = FormatoMPMS1GLMock.getInstance().getFormatoMPMS1GL();
        FormatoMPMS1GL formatoMPMS1GL = formatoMPMS1GLs.get(0);
        setEnumOut();
        List<Limits> result = mapper.mapOut(formatoMPMS1GL, new ArrayList<>());

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getAmountLimits());
        assertEquals(1, result.get(0).getAmountLimits().size());
        assertNotNull(result.get(0).getAmountLimits().get(0).getAmount());
        assertNotNull(result.get(0).getAmountLimits().get(0).getCurrency());
        assertNotNull(result.get(0).getAllowedInterval());
        assertNotNull(result.get(0).getAllowedInterval().getMinimumAmount());
        assertNotNull(result.get(0).getAllowedInterval().getMinimumAmount().getAmount());
        assertNotNull(result.get(0).getAllowedInterval().getMinimumAmount().getCurrency());
        assertNotNull(result.get(0).getAllowedInterval().getMaximumAmount());
        assertNotNull(result.get(0).getAllowedInterval().getMaximumAmount().getAmount());
        assertNotNull(result.get(0).getAllowedInterval().getMaximumAmount().getCurrency());

        assertEquals("PREPAID_LIMIT_MONTHLY", result.get(0).getId());
        assertEquals(formatoMPMS1GL.getMonactt(), result.get(0).getAmountLimits().get(0).getAmount());
        assertEquals(formatoMPMS1GL.getDivactt(), result.get(0).getAmountLimits().get(0).getCurrency());
        assertEquals(formatoMPMS1GL.getMonmint(), result.get(0).getAllowedInterval().getMinimumAmount().getAmount());
        assertEquals(formatoMPMS1GL.getDivmint(), result.get(0).getAllowedInterval().getMinimumAmount().getCurrency());
        assertEquals(formatoMPMS1GL.getMonmaxt(), result.get(0).getAllowedInterval().getMaximumAmount().getAmount());
        assertEquals(formatoMPMS1GL.getDivmaxt(), result.get(0).getAllowedInterval().getMaximumAmount().getCurrency());
    }

    @Test
    public void mapOutWithoutAmountLimitsTest() throws IOException {
        List<FormatoMPMS1GL> formatoMPMS1GLs = FormatoMPMS1GLMock.getInstance().getFormatoMPMS1GL();
        FormatoMPMS1GL formatoMPMS1GL = formatoMPMS1GLs.get(0);
        formatoMPMS1GL.setMonactt(null);
        formatoMPMS1GL.setDivactt(null);
        setEnumOut();
        List<Limits> result = mapper.mapOut(formatoMPMS1GL, new ArrayList<>());
        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNull(result.get(0).getAmountLimits());
        assertNotNull(result.get(0).getAllowedInterval());

        assertEquals("PREPAID_LIMIT_MONTHLY", result.get(0).getId());
        assertEquals(formatoMPMS1GL.getMonmint(), result.get(0).getAllowedInterval().getMinimumAmount().getAmount());
        assertEquals(formatoMPMS1GL.getDivmint(), result.get(0).getAllowedInterval().getMinimumAmount().getCurrency());
        assertEquals(formatoMPMS1GL.getMonmaxt(), result.get(0).getAllowedInterval().getMaximumAmount().getAmount());
        assertEquals(formatoMPMS1GL.getDivmaxt(), result.get(0).getAllowedInterval().getMaximumAmount().getCurrency());
    }

    @Test
    public void mapOutWithoutAllowedIntervalTest() throws IOException {
        List<FormatoMPMS1GL> formatoMPMS1GLs = FormatoMPMS1GLMock.getInstance().getFormatoMPMS1GL();
        FormatoMPMS1GL formatoMPMS1GL = formatoMPMS1GLs.get(0);
        setEnumOut();
        List<Limits> result = mapper.mapOut(formatoMPMS1GL, new ArrayList());
        assertNotNull(result);
        assertEquals(1, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getAmountLimits());
        assertEquals(1, result.get(0).getAmountLimits().size());
        assertNotNull(result.get(0).getAmountLimits().get(0).getAmount());
        assertNotNull(result.get(0).getAmountLimits().get(0).getCurrency());
        assertNotNull(result.get(0).getAllowedInterval());

        assertEquals("PREPAID_LIMIT_MONTHLY", result.get(0).getId());
        assertEquals(formatoMPMS1GL.getMonactt(), result.get(0).getAmountLimits().get(0).getAmount());
        assertEquals(formatoMPMS1GL.getDivactt(), result.get(0).getAmountLimits().get(0).getCurrency());

        assertEquals(formatoMPMS1GL.getMonmint(), result.get(0).getAllowedInterval().getMinimumAmount().getAmount());
        assertEquals(formatoMPMS1GL.getDivmint(), result.get(0).getAllowedInterval().getMinimumAmount().getCurrency());
        assertEquals(formatoMPMS1GL.getMonmaxt(), result.get(0).getAllowedInterval().getMaximumAmount().getAmount());
        assertEquals(formatoMPMS1GL.getDivmaxt(), result.get(0).getAllowedInterval().getMaximumAmount().getCurrency());


        formatoMPMS1GL = formatoMPMS1GLs.get(1);
        formatoMPMS1GL.setMonmint(null);
        formatoMPMS1GL.setDivmint(null);
        formatoMPMS1GL.setMonmaxt(null);
        formatoMPMS1GL.setDivmaxt(null);
        setEnumOut();
        result = mapper.mapOut(formatoMPMS1GL, result);
        assertNotNull(result);
        assertNotNull(result.get(1).getId());
        assertNotNull(result.get(1).getAmountLimits());
        assertEquals(2, result.size());

        assertEquals(1, result.get(1).getAmountLimits().size());
        assertNotNull(result.get(1).getAmountLimits().get(0).getAmount());
        assertNotNull(result.get(1).getAmountLimits().get(0).getCurrency());
        assertNull(result.get(1).getAllowedInterval());

        assertEquals("PREPAID_LIMIT_WEEKLY", result.get(1).getId());
        assertEquals(formatoMPMS1GL.getMonactt(), result.get(1).getAmountLimits().get(0).getAmount());
        assertEquals(formatoMPMS1GL.getDivactt(), result.get(1).getAmountLimits().get(0).getCurrency());
    }

    @Test
    public void mapOutEmptyTest() {
        List<Limits> result = mapper.mapOut(new FormatoMPMS1GL(), new ArrayList<>());
        assertNotNull(result);
        assertNull(result.get(0).getId());
        assertNull(result.get(0).getAmountLimits());
        assertNull(result.get(0).getAllowedInterval());
    }
}
