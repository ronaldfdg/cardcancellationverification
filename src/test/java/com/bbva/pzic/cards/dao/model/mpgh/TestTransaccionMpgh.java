package com.bbva.pzic.cards.dao.model.mpgh;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPGH</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpgh {

    @InjectMocks
    private TransaccionMpgh transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionMpgh peticion = new PeticionTransaccionMpgh();
        RespuestaTransaccionMpgh respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpgh.class, RespuestaTransaccionMpgh.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionMpgh result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);

    }
}