package com.bbva.pzic.cards.dao.model.mpwp;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPWP</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpwp {

    @InjectMocks
    private TransaccionMpwp transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpwp peticion = new PeticionTransaccionMpwp();
        RespuestaTransaccionMpwp respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpwp.class, RespuestaTransaccionMpwp.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpwp result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }
}