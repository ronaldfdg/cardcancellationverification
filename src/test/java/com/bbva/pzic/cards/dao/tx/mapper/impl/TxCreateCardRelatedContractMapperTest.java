package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1E;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1S;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.NUMBER_TYPE_ID_PAN;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardRelatedContractMapperTest {

    @InjectMocks
    private TxCreateCardRelatedContractMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    private EntityMock entityMock = EntityMock.getInstance();

    private void mapOutEnum() {
        Mockito.when(enumMapper.getEnumValue("relatedContracts.numberType.id", "1")).thenReturn(NUMBER_TYPE_ID_PAN);
    }

    @Test
    public void mapInTest() {
        DTOInputCreateCardRelatedContract dtoIn = entityMock.getDtoInputCreateRelatedContract();
        FormatoMPM0V1E result = mapper.mapIn(dtoIn);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertEquals(result.getNumtarj(), dtoIn.getCardId());
        Assert.assertNotNull(result.getNucorel());
        Assert.assertEquals(result.getNucorel(), dtoIn.getContractId());
    }

    @Test
    public void mapInWithoutCardIdTest() {
        DTOInputCreateCardRelatedContract dtoIn = entityMock.getDtoInputCreateRelatedContract();
        dtoIn.setCardId(null);
        FormatoMPM0V1E result = mapper.mapIn(dtoIn);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getNumtarj());
        Assert.assertNotNull(result.getNucorel());
        Assert.assertEquals(result.getNucorel(), dtoIn.getContractId());
    }

    @Test
    public void mapInWithoutContractIdTest() {
        DTOInputCreateCardRelatedContract dtoIn = entityMock.getDtoInputCreateRelatedContract();
        dtoIn.setContractId(null);
        FormatoMPM0V1E result = mapper.mapIn(dtoIn);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertEquals(result.getNumtarj(), dtoIn.getCardId());
        Assert.assertNull(result.getNucorel());

    }

    @Test
    public void mapOutTest() {
        mapOutEnum();
        FormatoMPM0V1S formatOut = entityMock.geFormatoMPM0V1S();
        DTOOutCreateCardRelatedContract result = mapper.mapOut(formatOut, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getContractId());
        Assert.assertEquals(result.getContractId(), formatOut.getNucorel());
        Assert.assertNotNull(result.getRelatedContractId());
        Assert.assertEquals(result.getRelatedContractId(), formatOut.getIdcorel());
        Assert.assertNotNull(result.getNumberTypeId());
        Assert.assertEquals(result.getNumberTypeId(), NUMBER_TYPE_ID_PAN);
        Assert.assertNotNull(result.getNumberTypeName());
        Assert.assertEquals(result.getNumberTypeName(), formatOut.getDetcore());

    }

    @Test
    public void mapOutWithoutIdcorelTest() {
        mapOutEnum();
        FormatoMPM0V1S formatOut = entityMock.geFormatoMPM0V1S();
        formatOut.setIdcorel(null);
        DTOOutCreateCardRelatedContract result = mapper.mapOut(formatOut, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getContractId());
        Assert.assertEquals(result.getContractId(), formatOut.getNucorel());
        Assert.assertNull(result.getRelatedContractId());
        Assert.assertNotNull(result.getNumberTypeId());
        Assert.assertEquals(result.getNumberTypeId(), NUMBER_TYPE_ID_PAN);
        Assert.assertNotNull(result.getNumberTypeName());
        Assert.assertEquals(result.getNumberTypeName(), formatOut.getDetcore());

    }

    @Test
    public void mapOutWithoutDetCoreTest() {
        mapOutEnum();
        FormatoMPM0V1S formatOut = entityMock.geFormatoMPM0V1S();
        formatOut.setDetcore(null);
        DTOOutCreateCardRelatedContract result = mapper.mapOut(formatOut, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getContractId());
        Assert.assertEquals(result.getContractId(), formatOut.getNucorel());
        Assert.assertNotNull(result.getRelatedContractId());
        Assert.assertEquals(result.getRelatedContractId(), formatOut.getIdcorel());
        Assert.assertNotNull(result.getNumberTypeId());
        Assert.assertEquals(result.getNumberTypeId(), NUMBER_TYPE_ID_PAN);
        Assert.assertNull(result.getNumberTypeName());
    }

    @Test
    public void mapOutWithoutNucorelTest() {
        mapOutEnum();
        FormatoMPM0V1S formatOut = entityMock.geFormatoMPM0V1S();
        formatOut.setNucorel(null);
        DTOOutCreateCardRelatedContract result = mapper.mapOut(formatOut, null);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getContractId());
        Assert.assertNotNull(result.getRelatedContractId());
        Assert.assertEquals(result.getRelatedContractId(), formatOut.getIdcorel());
        Assert.assertNotNull(result.getNumberTypeId());
        Assert.assertEquals(result.getNumberTypeId(), NUMBER_TYPE_ID_PAN);
        Assert.assertNotNull(result.getNumberTypeName());
        Assert.assertEquals(result.getNumberTypeName(), formatOut.getDetcore());
    }

    @Test
    public void mapOutWithoutIdtcoreTest() {
        mapOutEnum();
        FormatoMPM0V1S formatOut = entityMock.geFormatoMPM0V1S();
        formatOut.setIdtcore(null);
        DTOOutCreateCardRelatedContract result = mapper.mapOut(formatOut, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getContractId());
        Assert.assertEquals(result.getContractId(), formatOut.getNucorel());
        Assert.assertNotNull(result.getRelatedContractId());
        Assert.assertEquals(result.getRelatedContractId(), formatOut.getIdcorel());
        Assert.assertNull(result.getNumberTypeId());
        Assert.assertNotNull(result.getNumberTypeName());
        Assert.assertEquals(result.getNumberTypeName(), formatOut.getDetcore());

    }

    @Test
    public void mapOutWithFormatNullTest() {
        DTOOutCreateCardRelatedContract result = mapper.mapOut(null, null);
        Assert.assertNull(result);
    }

    @Test
    public void mapOutWithFormatInicializedTest() {
        DTOOutCreateCardRelatedContract result = mapper.mapOut(new FormatoMPM0V1S(), null);
        Assert.assertNull(result);
    }

}
