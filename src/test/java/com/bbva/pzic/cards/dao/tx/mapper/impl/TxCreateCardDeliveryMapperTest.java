package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntDelivery;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTECKB94;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTSCKB94;
import com.bbva.pzic.cards.dao.model.kb94.mock.FormatsKb94Stubs;
import com.bbva.pzic.cards.facade.v1.dto.Delivery;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM_FRONTEND;
import static com.bbva.pzic.cards.util.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardDeliveryMapperTest {

    @InjectMocks
    private TxCreateCardDeliveryMapper mapper;

    @Mock
    private Translator translator;

    // -------------------------------------------------------- MAP-IN --------------------------------------------------------
    @Test
    public void testMapInFullDeliverySpecificContactDetailTypeEmail() throws IOException {
        DTOIntDelivery input = EntityMock.getInstance().buildDTOIntDeliverySpecificEmail();

        FormatoKTECKB94 result = mapper.mapIn(input);

        assertNotNull(result.getNumtar());
        assertNotNull(result.getTipent());
        assertNotNull(result.getNatcont());
        assertNotNull(result.getTipcont());
        assertNotNull(result.getCorreo());
        assertNotNull(result.getTipdest());

        assertNull(result.getNumfijo());
        assertNull(result.getTiponum());
        assertNull(result.getCodpais());
        assertNull(result.getCodreg());
        assertNull(result.getNumcel());
        assertNull(result.getCompcel());
        assertNull(result.getUsuari());
        assertNull(result.getRedsoc());
        assertNull(result.getIdcont());

        assertEquals(input.getCardId(), result.getNumtar());
        assertEquals(input.getServiceTypeId(), result.getTipent());
        assertEquals(input.getContact().getContactType(), result.getNatcont());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getTipcont());
        assertEquals(input.getContact().getContact().getAddress(), result.getCorreo());
        assertEquals(input.getDestination().getId(), result.getTipdest());
    }

    @Test
    public void testMapInFullDeliverySpecificContactDetailTypeLandline() throws IOException {
        DTOIntDelivery input = EntityMock.getInstance().buildDTOIntDeliverySpecificLandline();

        FormatoKTECKB94 result = mapper.mapIn(input);

        assertNotNull(result.getNumtar());
        assertNotNull(result.getTipent());
        assertNotNull(result.getNatcont());
        assertNotNull(result.getTipcont());
        assertNotNull(result.getNumfijo());
        assertNotNull(result.getTiponum());
        assertNotNull(result.getCodpais());
        assertNotNull(result.getCodreg());
        assertNotNull(result.getTipdest());

        assertNull(result.getCorreo());
        assertNull(result.getNumcel());
        assertNull(result.getCompcel());
        assertNull(result.getUsuari());
        assertNull(result.getRedsoc());
        assertNull(result.getIdcont());

        assertEquals(input.getCardId(), result.getNumtar());
        assertEquals(input.getServiceTypeId(), result.getTipent());
        assertEquals(input.getContact().getContactType(), result.getNatcont());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getTipcont());
        assertEquals(input.getContact().getContact().getNumber(), result.getNumfijo());
        assertEquals(input.getContact().getContact().getPhoneType(), result.getTiponum());
        assertEquals(input.getContact().getContact().getCountryId(), result.getCodpais());
        assertEquals(input.getContact().getContact().getRegionalCode(), result.getCodreg());
        assertEquals(input.getDestination().getId(), result.getTipdest());
    }

    @Test
    public void testMapInFullDeliverySpecificContactDetailTypeMobile() throws IOException {
        DTOIntDelivery input = EntityMock.getInstance().buildDTOIntDeliverySpecificMobile();

        FormatoKTECKB94 result = mapper.mapIn(input);

        assertNotNull(result.getNumtar());
        assertNotNull(result.getTipent());
        assertNotNull(result.getNatcont());
        assertNotNull(result.getTipcont());
        assertNotNull(result.getNumcel());
        assertNotNull(result.getCompcel());
        assertNotNull(result.getTipdest());

        assertNull(result.getCorreo());
        assertNull(result.getNumfijo());
        assertNull(result.getTiponum());
        assertNull(result.getCodpais());
        assertNull(result.getCodreg());
        assertNull(result.getUsuari());
        assertNull(result.getRedsoc());
        assertNull(result.getIdcont());

        assertEquals(input.getCardId(), result.getNumtar());
        assertEquals(input.getServiceTypeId(), result.getTipent());
        assertEquals(input.getContact().getContactType(), result.getNatcont());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getTipcont());
        assertEquals(input.getContact().getContact().getNumber(), result.getNumcel());
        assertEquals(input.getContact().getContact().getPhoneCompany().getId(), result.getCompcel());
        assertEquals(input.getDestination().getId(), result.getTipdest());
    }

    @Test
    public void testMapInFullDeliverySpecificContactDetailTypeSocialMedia() throws IOException {
        DTOIntDelivery input = EntityMock.getInstance().buildDTOIntDeliverySpecificSocialMedia();

        FormatoKTECKB94 result = mapper.mapIn(input);

        assertNotNull(result.getNumtar());
        assertNotNull(result.getTipent());
        assertNotNull(result.getNatcont());
        assertNotNull(result.getTipcont());
        assertNotNull(result.getUsuari());
        assertNotNull(result.getRedsoc());
        assertNotNull(result.getTipdest());

        assertNull(result.getCorreo());
        assertNull(result.getNumfijo());
        assertNull(result.getTiponum());
        assertNull(result.getCodpais());
        assertNull(result.getCodreg());
        assertNull(result.getNumcel());
        assertNull(result.getCompcel());
        assertNull(result.getIdcont());

        assertEquals(input.getCardId(), result.getNumtar());
        assertEquals(input.getServiceTypeId(), result.getTipent());
        assertEquals(input.getContact().getContactType(), result.getNatcont());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getTipcont());
        assertEquals(input.getContact().getContact().getUsername(), result.getUsuari());
        assertEquals(input.getContact().getContact().getSocialNetwork(), result.getRedsoc());
        assertEquals(input.getDestination().getId(), result.getTipdest());
    }

    @Test
    public void testMapInFullDeliverySpecificContactDetailTypeUnrecognized() throws IOException {
        DTOIntDelivery input = EntityMock.getInstance().buildDTOIntDeliverySpecificSocialMedia();
        input.getContact().getContact().setContactDetailTypeOriginal("UNRECOGNIZED");

        FormatoKTECKB94 result = mapper.mapIn(input);

        assertNotNull(result.getNumtar());
        assertNotNull(result.getTipdest());
    }

    @Test
    public void testMapInFullDeliveryStored() throws IOException {
        DTOIntDelivery input = EntityMock.getInstance().buildDTOIntDeliveryStored();

        FormatoKTECKB94 result = mapper.mapIn(input);

        assertNotNull(result.getNumtar());
        assertNotNull(result.getTipent());
        assertNotNull(result.getNatcont());
        assertNotNull(result.getIdcont());
        assertNotNull(result.getTipdest());

        assertNull(result.getTipcont());
        assertNull(result.getCorreo());
        assertNull(result.getNumfijo());
        assertNull(result.getTiponum());
        assertNull(result.getCodpais());
        assertNull(result.getCodreg());
        assertNull(result.getNumcel());
        assertNull(result.getCompcel());
        assertNull(result.getUsuari());
        assertNull(result.getRedsoc());

        assertEquals(input.getCardId(), result.getNumtar());
        assertEquals(input.getServiceTypeId(), result.getTipent());
        assertEquals(input.getContact().getContactType(), result.getNatcont());
        assertEquals(input.getContact().getContact().getId(), result.getIdcont().toString());
        assertEquals(input.getDestination().getId(), result.getTipdest());
    }

    // -------------------------------------------------------- MAP-OUT --------------------------------------------------------
    @Test
    public void testMapOutFullDeliverySpecificContactDetailTypeEmail() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn(SPECIFIC);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getTipcont())).thenReturn(EMAIL);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.destination.id", input.getTipdest())).thenReturn(CUSTOM);

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getAddress());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());

        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountry());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());
        assertNull(result.getContact().getContact().getId());

        assertEquals(input.getIdafil(), result.getId());
        assertEquals(input.getTipent(), result.getServiceType().getId());
        assertEquals(SPECIFIC, result.getContact().getContactType());
        assertEquals(EMAIL, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getCorreo(), result.getContact().getContact().getAddress());
        assertEquals(CUSTOM, result.getDestination().getId());
        assertEquals(input.getDesctd(), result.getDestination().getName());
    }

    @Test
    public void testMapOutFullDeliverySpecificContactDetailTypeLandline() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn(SPECIFIC);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getTipcont())).thenReturn(LANDLINE);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contact.phoneType", input.getTiponum())).thenReturn(HOME);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.destination.id", input.getTipdest())).thenReturn(CUSTOM);

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneType());
        assertNotNull(result.getContact().getContact().getCountry().getId());
        assertNotNull(result.getContact().getContact().getCountry().getName());
        assertNotNull(result.getContact().getContact().getRegionalCode());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());

        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());
        assertNull(result.getContact().getContact().getId());

        assertEquals(input.getIdafil(), result.getId());
        assertEquals(input.getTipent(), result.getServiceType().getId());
        assertEquals(SPECIFIC, result.getContact().getContactType());
        assertEquals(LANDLINE, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getNumfijo(), result.getContact().getContact().getNumber());
        assertEquals(HOME, result.getContact().getContact().getPhoneType());
        assertEquals(input.getCodpais(), result.getContact().getContact().getCountry().getId());
        assertEquals(input.getDespais(), result.getContact().getContact().getCountry().getName());
        assertEquals(input.getCodreg(), result.getContact().getContact().getRegionalCode());
        assertEquals(CUSTOM, result.getDestination().getId());
        assertEquals(input.getDesctd(), result.getDestination().getName());
    }

    @Test
    public void testMapOutFullDeliverySpecificContactDetailTypeMobile() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn(SPECIFIC);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getTipcont())).thenReturn(MOBILE);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.destination.id", input.getTipdest())).thenReturn(CUSTOM);

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());

        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountry());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());
        assertNull(result.getContact().getContact().getId());

        assertEquals(input.getIdafil(), result.getId());
        assertEquals(input.getTipent(), result.getServiceType().getId());
        assertEquals(SPECIFIC, result.getContact().getContactType());
        assertEquals(MOBILE, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getNumcel(), result.getContact().getContact().getNumber());
        assertEquals(input.getCompcel(), result.getContact().getContact().getPhoneCompany().getId());
        assertEquals(input.getCompmov(), result.getContact().getContact().getPhoneCompany().getName());
        assertEquals(CUSTOM, result.getDestination().getId());
        assertEquals(input.getDesctd(), result.getDestination().getName());
    }

    @Test
    public void testMapOutFullDeliverySpecificContactDetailTypeSocialMedia() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn(SPECIFIC);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getTipcont())).thenReturn(SOCIAL_MEDIA);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contact.socialNetwork", input.getRedsoc())).thenReturn(SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.destination.id", input.getTipdest())).thenReturn(CUSTOM);

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getUsername());
        assertNotNull(result.getContact().getContact().getSocialNetwork());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());

        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountry());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getId());

        assertEquals(input.getIdafil(), result.getId());
        assertEquals(input.getTipent(), result.getServiceType().getId());
        assertEquals(SPECIFIC, result.getContact().getContactType());
        assertEquals(SOCIAL_MEDIA, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getUsuari(), result.getContact().getContact().getUsername());
        assertEquals(SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM_FRONTEND, result.getContact().getContact().getSocialNetwork());
        assertEquals(CUSTOM, result.getDestination().getId());
        assertEquals(input.getDesctd(), result.getDestination().getName());
    }

    @Test
    public void testMapOutFullDeliverySpecificContactDetailTypeUnrecognized() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn(SPECIFIC);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getTipcont())).thenReturn("UNRECOGNIZED");

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getDestination());
    }

    @Test
    public void testMapOutFullDeliveryStored() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn(STORED);
        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.destination.id", input.getTipdest())).thenReturn(CUSTOM);

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact().getId());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNotNull(result.getDestination().getName());

        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountry());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());

        assertEquals(input.getIdafil(), result.getId());
        assertEquals(input.getTipent(), result.getServiceType().getId());
        assertEquals(STORED, result.getContact().getContactType());
        assertEquals(input.getIdcont().toString(), result.getContact().getContact().getId());
        assertEquals(CUSTOM, result.getDestination().getId());
        assertEquals(input.getDesctd(), result.getDestination().getName());
    }

    @Test
    public void testMapOutFullDeliveryUnrecognized() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94();

        when(translator.translateBackendEnumValueStrictly(
                "cards.delivery.contactType", input.getNatcont())).thenReturn("UNRECOGNIZED");

        Delivery result = mapper.mapOut(input);

        assertNotNull(result.getId());
        assertNotNull(result.getServiceType());
        assertNotNull(result.getServiceType().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getDestination());
    }

    @Test
    public void testMapOutEmpty() throws IOException {
        FormatoKTSCKB94 input = FormatsKb94Stubs.getInstance().buildFormatoKTSCKB94Empty();

        Delivery result = mapper.mapOut(input);

        assertNull(result.getId());
        assertNull(result.getServiceType());
        assertNull(result.getContact());
        assertNull(result.getDestination());
    }
}
