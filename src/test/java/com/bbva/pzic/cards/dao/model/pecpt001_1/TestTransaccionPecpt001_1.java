package com.bbva.pzic.cards.dao.model.pecpt001_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PECPT001</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPecpt001_1 {

    @InjectMocks
    private TransaccionPecpt001_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPecpt001_1 rq = new PeticionTransaccionPecpt001_1();
        RespuestaTransaccionPecpt001_1 rs = new RespuestaTransaccionPecpt001_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPecpt001_1.class, RespuestaTransaccionPecpt001_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPecpt001_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}