package com.bbva.pzic.cards.dao.model.mpw2;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>MPW2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpw2 {

    @InjectMocks
    private TransaccionMpw2 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionMpw2 rq = new PeticionTransaccionMpw2();
        RespuestaTransaccionMpw2 rs = new RespuestaTransaccionMpw2();

        when(servicioTransacciones.invocar(PeticionTransaccionMpw2.class, RespuestaTransaccionMpw2.class, rq))
                .thenReturn(rs);

        RespuestaTransaccionMpw2 result = transaccion.invocar(rq);

        assertEquals(result, rs);
    }
}
