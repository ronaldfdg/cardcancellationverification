package com.bbva.pzic.cards.dao.model.mpb5;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPB5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpb5 {

    @InjectMocks
    private TransaccionMpb5 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpb5 peticion = new PeticionTransaccionMpb5();
        RespuestaTransaccionMpb5 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpb5.class, RespuestaTransaccionMpb5.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpb5 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}