package com.bbva.pzic.cards.dao.model.mpg3;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>MPG3</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpg3 {

    @InjectMocks
    private TransaccionMpg3 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionMpg3 rq = new PeticionTransaccionMpg3();
        RespuestaTransaccionMpg3 rs = transaccion.invocar(rq);

        when(servicioTransacciones.invocar(PeticionTransaccionMpg3.class, RespuestaTransaccionMpg3.class, rq))
                .thenReturn(rs);

        RespuestaTransaccionMpg3 result = transaccion.invocar(rq);

        assertEquals(result, rs);
    }
}
