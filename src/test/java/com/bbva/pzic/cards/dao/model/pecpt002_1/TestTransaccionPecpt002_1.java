package com.bbva.pzic.cards.dao.model.pecpt002_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PECPT002</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPecpt002_1 {

    @InjectMocks
    private TransaccionPecpt002_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPecpt002_1 rq = new PeticionTransaccionPecpt002_1();
        RespuestaTransaccionPecpt002_1 rs = new RespuestaTransaccionPecpt002_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPecpt002_1.class, RespuestaTransaccionPecpt002_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPecpt002_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}