package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpaf.FormatoMPM0AFE;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TxModifyCardsActivationsMapperTest {

    private TxModifyCardsActivationsMapper mapper = new TxModifyCardsActivationsMapper();

    private EntityMock entityMock;

    @Before
    public void setUp() {
        entityMock = EntityMock.getInstance();
    }

    @Test
    public void mapInAllTest() {
        DTOIntActivation input = entityMock.buildDTOIntModifyCardsActivations();
        FormatoMPM0AFE result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCodclie());
        assertNotNull(result.getCodserv());
        assertNotNull(result.getIndesta());

        assertEquals(input.getCustomerId(), result.getCodclie());
        assertEquals(input.getActivationId(), result.getCodserv());
        assertEquals("1", result.getIndesta());
    }

    @Test
    public void mapInIsActiveFalseTest() {
        DTOIntActivation input = entityMock.buildDTOIntModifyCardsActivations();
        input.setIsActive(false);
        FormatoMPM0AFE result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCodclie());
        assertNotNull(result.getCodserv());
        assertNotNull(result.getIndesta());

        assertEquals(input.getCustomerId(), result.getCodclie());
        assertEquals(input.getActivationId(), result.getCodserv());
        assertEquals("0", result.getIndesta());
    }

    @Test
    public void mapInIsActiveNullTest() {
        DTOIntActivation input = entityMock.buildDTOIntModifyCardsActivations();
        input.setIsActive(null);
        FormatoMPM0AFE result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCodclie());
        assertNotNull(result.getCodserv());
        assertNull(result.getIndesta());

        assertEquals(input.getCustomerId(), result.getCodclie());
        assertEquals(input.getActivationId(), result.getCodserv());
    }

    @Test
    public void mapInWithoutActivationIdTest() {
        DTOIntActivation input = entityMock.buildDTOIntModifyCardsActivations();
        input.setActivationId(null);
        FormatoMPM0AFE result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCodclie());
        assertNull(result.getCodserv());
        assertNotNull(result.getIndesta());

        assertEquals(input.getCustomerId(), result.getCodclie());
        assertEquals("1", result.getIndesta());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPM0AFE result = mapper.mapIn(new DTOIntActivation());
        assertNotNull(result);
        assertNull(result.getCodclie());
        assertNull(result.getCodserv());
        assertNull(result.getIndesta());
    }
}