package com.bbva.pzic.cards.dao.model.mpb4;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPB4</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpb4 {

    @InjectMocks
    private TransaccionMpb4 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpb4 peticion = new PeticionTransaccionMpb4();
        RespuestaTransaccionMpb4 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpb4.class, RespuestaTransaccionMpb4.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpb4 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}