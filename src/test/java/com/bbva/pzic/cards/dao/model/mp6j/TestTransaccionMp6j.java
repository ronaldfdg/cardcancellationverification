package com.bbva.pzic.cards.dao.model.mp6j;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MP6J</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMp6j {

	private static final Log LOG = LogFactory.getLog(TestTransaccionMp6j.class);

	@InjectMocks
	private TransaccionMp6j transaccion;
	@Mock
	private ServicioTransacciones servicioTransacciones;

	@Test
	public void test() throws ExcepcionTransaccion {
		PeticionTransaccionMp6j peticion = new PeticionTransaccionMp6j();
		RespuestaTransaccionMp6j respuesta = transaccion.invocar(peticion);

		Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMp6j.class, RespuestaTransaccionMp6j.class,
				peticion)).thenReturn(respuesta);

		RespuestaTransaccionMp6j result = transaccion.invocar(peticion);

		Assert.assertEquals(result, respuesta);
	}

}

