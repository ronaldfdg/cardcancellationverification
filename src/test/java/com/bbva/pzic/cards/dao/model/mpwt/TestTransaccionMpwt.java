package com.bbva.pzic.cards.dao.model.mpwt;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPWT</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpwt {

    @InjectMocks
    private TransaccionMpwt transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionMpwt peticion = new PeticionTransaccionMpwt();
        RespuestaTransaccionMpwt respuesta = new RespuestaTransaccionMpwt();

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpwt.class, RespuestaTransaccionMpwt.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpwt result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }
}
