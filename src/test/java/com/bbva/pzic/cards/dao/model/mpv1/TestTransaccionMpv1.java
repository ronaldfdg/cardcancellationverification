package com.bbva.pzic.cards.dao.model.mpv1;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPV1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpv1 {

    @InjectMocks
    private TransaccionMpv1 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpv1 peticion = new PeticionTransaccionMpv1();
        RespuestaTransaccionMpv1 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpv1.class, RespuestaTransaccionMpv1.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpv1 result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }
}