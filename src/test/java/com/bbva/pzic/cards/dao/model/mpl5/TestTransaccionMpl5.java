package com.bbva.pzic.cards.dao.model.mpl5;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPL5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpl5 {

    @InjectMocks
    private TransaccionMpl5 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionMpl5 peticion = new PeticionTransaccionMpl5();
        RespuestaTransaccionMpl5 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpl5.class, RespuestaTransaccionMpl5.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpl5 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}