package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PPCUTC01</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPpcutc01_1 {

    @InjectMocks
    private TransaccionPpcutc01_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPpcutc01_1 rq = new PeticionTransaccionPpcutc01_1();
        RespuestaTransaccionPpcutc01_1 rs = new RespuestaTransaccionPpcutc01_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPpcutc01_1.class, RespuestaTransaccionPpcutc01_1.class, rq))
                .thenReturn(rs);

        RespuestaTransaccionPpcutc01_1 result = transaccion.invocar(rq);

        assertEquals(result, rs);
    }
}
