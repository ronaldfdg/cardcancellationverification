package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.dao.apx.mapper.IApxListCardShipmentsMapper;
import com.bbva.pzic.cards.dao.model.pecpt001_1.PeticionTransaccionPecpt001_1;
import com.bbva.pzic.cards.dao.model.pecpt001_1.RespuestaTransaccionPecpt001_1;
import com.bbva.pzic.cards.dao.model.pecpt001_1.mock.Pecpt001_1Stubs;
import com.bbva.pzic.cards.facade.v0.dto.Shipments;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class ApxListCardShipmentsMapperTest {

    private IApxListCardShipmentsMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxListCardShipmentsMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListCardShipments input = EntityMock.getInstance().buildInputListCardShipments();
        PeticionTransaccionPecpt001_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCardagreement());
        assertNotNull(result.getExternalcode());
        assertNotNull(result.getReferencenumber());
        assertNotNull(result.getShippingcompanyid());

        assertEquals(input.getShippingCompanyId(), result.getShippingcompanyid());
        assertEquals(input.getReferenceNumber(), result.getReferencenumber());
        assertEquals(input.getExternalCode(), result.getExternalcode());
        assertEquals(input.getCardAgreement(), result.getCardagreement());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPecpt001_1 input = Pecpt001_1Stubs.getInstance().getShipments();
        List<Shipments> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getDescription());
        assertNotNull(result.get(0).getParticipant());
        assertNotNull(result.get(0).getParticipant().getFullName());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.get(0).getParticipant().getCards());
        assertEquals(2, result.get(0).getParticipant().getCards().size());
        assertNotNull(result.get(0).getParticipant().getCards().get(0));
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.get(0).getParticipant().getCards().get(1));
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.get(0).getShippingCompany());
        assertNotNull(result.get(0).getShippingCompany().getId());
        assertNotNull(result.get(0).getShippingCompany().getName());
        assertNotNull(result.get(1).getId());
        assertNotNull(result.get(1).getStatus());
        assertNotNull(result.get(1).getStatus().getId());
        assertNotNull(result.get(1).getStatus().getDescription());
        assertNotNull(result.get(1).getParticipant());
        assertNotNull(result.get(1).getParticipant().getFullName());
        assertNotNull(result.get(1).getParticipant().getIdentityDocument());
        assertNotNull(result.get(1).getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.get(1).getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.get(1).getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.get(1).getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.get(1).getParticipant().getCards());
        assertEquals(2, result.get(1).getParticipant().getCards().size());
        assertNotNull(result.get(1).getParticipant().getCards().get(0));
        assertNotNull(result.get(1).getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.get(1).getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.get(1).getParticipant().getCards().get(1));
        assertNotNull(result.get(1).getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.get(1).getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.get(1).getShippingCompany());
        assertNotNull(result.get(1).getShippingCompany().getId());
        assertNotNull(result.get(1).getShippingCompany().getName());

        //FIRST INDEX
        assertEquals(input.getEntityout().get(0).getData().getId(), result.get(0).getId());
        assertEquals(input.getEntityout().get(0).getData().getStatus().getId(), result.get(0).getStatus().getId());
        assertEquals(input.getEntityout().get(0).getData().getStatus().getDescription(), result.get(0).getStatus().getDescription());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getFullname(), result.get(0).getParticipant().getFullName());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getIdentitydocument().getDocumentnumber(), result.get(0).getParticipant().getIdentityDocument().getDocumentNumber());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getIdentitydocument().getDocumenttype().getId(), result.get(0).getParticipant().getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getIdentitydocument().getDocumenttype().getDescription(), result.get(0).getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getCards().get(0).getCard().getCardagreement(), result.get(0).getParticipant().getCards().get(0).getCardAgreement());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getCards().get(0).getCard().getReferencenumber(), result.get(0).getParticipant().getCards().get(0).getReferenceNumber());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getCards().get(1).getCard().getCardagreement(), result.get(0).getParticipant().getCards().get(1).getCardAgreement());
        assertEquals(input.getEntityout().get(0).getData().getParticipant().getCards().get(1).getCard().getReferencenumber(), result.get(0).getParticipant().getCards().get(1).getReferenceNumber());
        assertEquals(input.getEntityout().get(0).getData().getShippingcompany().getId(), result.get(0).getShippingCompany().getId());
        assertEquals(input.getEntityout().get(0).getData().getShippingcompany().getName(), result.get(0).getShippingCompany().getName());

        //SECOND INDEX
        assertEquals(input.getEntityout().get(1).getData().getId(), result.get(1).getId());
        assertEquals(input.getEntityout().get(1).getData().getStatus().getId(), result.get(1).getStatus().getId());
        assertEquals(input.getEntityout().get(1).getData().getStatus().getDescription(), result.get(1).getStatus().getDescription());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getFullname(), result.get(1).getParticipant().getFullName());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getIdentitydocument().getDocumentnumber(), result.get(1).getParticipant().getIdentityDocument().getDocumentNumber());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getIdentitydocument().getDocumenttype().getId(), result.get(1).getParticipant().getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getIdentitydocument().getDocumenttype().getDescription(), result.get(1).getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getCards().get(0).getCard().getCardagreement(), result.get(1).getParticipant().getCards().get(0).getCardAgreement());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getCards().get(0).getCard().getReferencenumber(), result.get(1).getParticipant().getCards().get(0).getReferenceNumber());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getCards().get(1).getCard().getCardagreement(), result.get(1).getParticipant().getCards().get(1).getCardAgreement());
        assertEquals(input.getEntityout().get(1).getData().getParticipant().getCards().get(1).getCard().getReferencenumber(), result.get(1).getParticipant().getCards().get(1).getReferenceNumber());
        assertEquals(input.getEntityout().get(1).getData().getShippingcompany().getId(), result.get(1).getShippingCompany().getId());
        assertEquals(input.getEntityout().get(1).getData().getShippingcompany().getName(), result.get(1).getShippingCompany().getName());
    }

    @Test
    public void mapOutStatusNullTest() throws IOException {
        RespuestaTransaccionPecpt001_1 input = Pecpt001_1Stubs.getInstance().getShipments();
        input.getEntityout().get(0).getData().setStatus(null);
        List<Shipments> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getParticipant());
        assertNotNull(result.get(0).getParticipant().getFullName());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.get(0).getParticipant().getCards());
        assertEquals(2, result.get(0).getParticipant().getCards().size());
        assertNotNull(result.get(0).getParticipant().getCards().get(0));
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.get(0).getParticipant().getCards().get(1));
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.get(0).getShippingCompany());
        assertNotNull(result.get(0).getShippingCompany().getId());
        assertNotNull(result.get(0).getShippingCompany().getName());
    }

    @Test
    public void mapOutParticipantNullTest() throws IOException {
        RespuestaTransaccionPecpt001_1 input = Pecpt001_1Stubs.getInstance().getShipments();
        input.getEntityout().get(0).getData().setParticipant(null);
        List<Shipments> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getDescription());
        assertNull(result.get(0).getParticipant());
        assertNotNull(result.get(0).getShippingCompany());
        assertNotNull(result.get(0).getShippingCompany().getId());
        assertNotNull(result.get(0).getShippingCompany().getName());
    }

    @Test
    public void mapOutShipppingCompanyNullTest() throws IOException {
        RespuestaTransaccionPecpt001_1 input = Pecpt001_1Stubs.getInstance().getShipments();
        input.getEntityout().get(0).getData().setShippingcompany(null);
        List<Shipments> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getDescription());
        assertNotNull(result.get(0).getParticipant());
        assertNotNull(result.get(0).getParticipant().getFullName());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.get(0).getParticipant().getCards());
        assertEquals(2, result.get(0).getParticipant().getCards().size());
        assertNotNull(result.get(0).getParticipant().getCards().get(0));
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.get(0).getParticipant().getCards().get(1));
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getReferenceNumber());
        assertNull(result.get(0).getShippingCompany());
    }

    @Test
    public void mapOutParticipantIdentityDocumentNullTest() throws IOException {
        RespuestaTransaccionPecpt001_1 input = Pecpt001_1Stubs.getInstance().getShipments();
        input.getEntityout().get(0).getData().getParticipant().setIdentitydocument(null);
        List<Shipments> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getDescription());
        assertNotNull(result.get(0).getParticipant());
        assertNotNull(result.get(0).getParticipant().getFullName());
        assertNull(result.get(0).getParticipant().getIdentityDocument());
        assertNotNull(result.get(0).getParticipant().getCards());
        assertEquals(2, result.get(0).getParticipant().getCards().size());
        assertNotNull(result.get(0).getParticipant().getCards().get(0));
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.get(0).getParticipant().getCards().get(1));
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.get(0).getShippingCompany());
        assertNotNull(result.get(0).getShippingCompany().getId());
        assertNotNull(result.get(0).getShippingCompany().getName());
    }

    @Test
    public void mapOutParticipantIdentityDocumentDocumentTypeNullTest() throws IOException {
        RespuestaTransaccionPecpt001_1 input = Pecpt001_1Stubs.getInstance().getShipments();
        input.getEntityout().get(0).getData().getParticipant().getIdentitydocument().setDocumenttype(null);
        List<Shipments> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getDescription());
        assertNotNull(result.get(0).getParticipant());
        assertNotNull(result.get(0).getParticipant().getFullName());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument());
        assertNotNull(result.get(0).getParticipant().getIdentityDocument().getDocumentNumber());
        assertNull(result.get(0).getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.get(0).getParticipant().getCards());
        assertEquals(2, result.get(0).getParticipant().getCards().size());
        assertNotNull(result.get(0).getParticipant().getCards().get(0));
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.get(0).getParticipant().getCards().get(1));
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.get(0).getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.get(0).getShippingCompany());
        assertNotNull(result.get(0).getShippingCompany().getId());
        assertNotNull(result.get(0).getShippingCompany().getName());
    }

    @Test
    public void mapOutNullTest() {
        List<Shipments> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        List<Shipments> result = mapper.mapOut(new RespuestaTransaccionPecpt001_1());

        assertNull(result);
    }
}
