package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementDetail;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPME1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS2GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS3GH;
import com.bbva.pzic.cards.dao.model.mpgh.mock.FormatsMPGHMock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 28/07/2018.
 *
 * @author Entelgy
 */
public class TxGetCardFinancialStatementMapperTest {
    private ITxGetCardFinancialStatementMapper mapper;

    @Before
    public void setUp() {
        mapper = new TxGetCardFinancialStatementMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetCardFinancialStatement dtoIn = EntityMock.getInstance().getCardFinancialStatement();
        final FormatoMPME1GH result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getIddocta());
        assertEquals(dtoIn.getCardId(), result.getIdetarj());
        assertEquals(dtoIn.getFinancialStatementId(), result.getIddocta());
    }

    @Test
    public void testMapResponseFormatToDtoOut() throws IOException {
        final FormatoMPMS1GH formato = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHNaturalLifeMilles();

        final DTOIntCardStatementDetail result = mapper.mapOut(formato).getDetail();

        assertNotNull(result.getFormatsPan());
        assertNotNull(result.getProductName());
        assertNotNull(result.getProductCode());
        assertNotNull(result.getBranchName());

        assertNotNull(result.getParticipantNameFirst());
        assertNotNull(result.getParticipantNameSecond());
        assertNotNull(result.getParticipantType());
        assertNotNull(result.getAccountNumberPEN());
        assertNotNull(result.getAccountNumberUSD());

        assertNotNull(result.getProgram().getLifeMilesNumber());
        assertNotNull(result.getProgram().getMonthPoints());
        assertNotNull(result.getProgram().getTotalPoints());
        assertNotNull(result.getProgram().getBonus());

        assertNotNull(result.getCardId());
        assertNotNull(result.getCurrency());
        assertNotNull(result.getEndDate());
        assertNotNull(result.getPayType());

        assertNotNull(result.getAddress().getName());
        assertNotNull(result.getAddress().getStreetType());
        assertNotNull(result.getAddress().getState());
        assertNotNull(result.getAddress().getUbigeo());

        assertNotNull(result.getCreditLimit());
        assertNotNull(result.getLimits().getDisposedBalance());
        assertNotNull(result.getLimits().getAvailableBalance());

        assertNotNull(result.getPaymentDate());

        assertNotNull(result.getInterest().getPurchasePEN());
        assertNotNull(result.getInterest().getPurchaseUSD());
        assertNotNull(result.getInterest().getAdvancedPEN());
        assertNotNull(result.getInterest().getAdvancedUSD());
        assertNotNull(result.getInterest().getCountervailingPEN());
        assertNotNull(result.getInterest().getCountervailingUSD());
        assertNotNull(result.getInterest().getArrearsPEN());
        assertNotNull(result.getInterest().getArrearsUSD());

        assertNotNull(result.getDisposedBalanceLocalCurrency().getAmount());
        assertNotNull(result.getDisposedBalanceLocalCurrency().getCurrency());
        assertNotNull(result.getDisposedBalance().getAmount());
        assertNotNull(result.getDisposedBalance().getCurrency());

        assertNotNull(result.getTotalTransactions());

        assertNotNull(result.getLimits().getFinancingDisposedBalance());

        assertNotNull(result.getExtraPayments().getOutstandingBalanceLocalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getOutstandingBalanceLocalCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getMinimumCapitalLocalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getMinimumCapitalLocalCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getInterestsBalanceLocalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getInterestsBalanceLocalCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getFeesBalanceLocalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getFeesBalanceLocalCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getFinancingTransactionLocalBalance().getAmount());
        assertNotNull(result.getExtraPayments().getFinancingTransactionLocalBalance().getCurrency());
        assertNotNull(result.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getInvoiceAmountLocalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getInvoiceAmountLocalCurrency().getCurrency());

        assertNotNull(result.getExtraPayments().getOutstandingBalance().getAmount());
        assertNotNull(result.getExtraPayments().getOutstandingBalance().getCurrency());
        assertNotNull(result.getExtraPayments().getMinimumCapitalCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getMinimumCapitalCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getInterestsBalance().getAmount());
        assertNotNull(result.getExtraPayments().getInterestsBalance().getCurrency());
        assertNotNull(result.getExtraPayments().getFeesBalance().getAmount());
        assertNotNull(result.getExtraPayments().getFeesBalance().getCurrency());
        assertNotNull(result.getExtraPayments().getFinancingTransactionBalance().getAmount());
        assertNotNull(result.getExtraPayments().getFinancingTransactionBalance().getCurrency());
        assertNotNull(result.getExtraPayments().getInvoiceMinimumAmountCurrency().getAmount());
        assertNotNull(result.getExtraPayments().getInvoiceMinimumAmountCurrency().getCurrency());
        assertNotNull(result.getExtraPayments().getInvoiceAmount().getAmount());
        assertNotNull(result.getExtraPayments().getInvoiceAmount().getCurrency());

        assertNotNull(result.getTotalDebtLocalCurrency().getAmount());
        assertNotNull(result.getTotalDebtLocalCurrency().getCurrency());
        assertNotNull(result.getTotalDebt().getAmount());
        assertNotNull(result.getTotalDebt().getCurrency());

        assertNotNull(result.getTotalMonthsAmortizationMinimum());
        assertNotNull(result.getTotalMonthsAmortizationMinimumFull());

        assertEquals(formato.getNumtarj(), result.getFormatsPan());
        assertEquals(formato.getTiptarj(), result.getProductName());
        assertEquals(formato.getCtiptar(), result.getProductCode());
        assertEquals(formato.getOficina(), result.getBranchName());

        assertEquals(formato.getTitptar(), result.getParticipantNameFirst());
        assertEquals(formato.getTitstar(), result.getParticipantNameSecond());
        assertEquals(formato.getIndtper(), result.getParticipantType());
        assertEquals(formato.getNumcuep(), result.getAccountNumberPEN());
        assertEquals(formato.getNumcueu(), result.getAccountNumberUSD());

        assertEquals(formato.getNumlife(), result.getProgram().getLifeMilesNumber());
        assertEquals(formato.getPuntmes(), result.getProgram().getMonthPoints());
        assertEquals(formato.getPunacum(), result.getProgram().getTotalPoints());
        assertEquals(formato.getBonomes(), result.getProgram().getBonus());

        assertEquals(formato.getNumcont(), result.getCardId());
        assertEquals(formato.getDivcont(), result.getCurrency());
        assertEquals(formato.getFeccier(), result.getEndDate());
        assertEquals(formato.getPagcarg(), result.getPayType());

        assertEquals(formato.getDirptit(), result.getAddress().getName());
        assertEquals(formato.getDirctit(), result.getAddress().getStreetType());
        assertEquals(formato.getDirppro(), result.getAddress().getState());
        assertEquals(formato.getDirpubi(), result.getAddress().getUbigeo());

        assertEquals(formato.getLincred(), result.getCreditLimit());
        assertEquals(formato.getCreutil(), result.getLimits().getDisposedBalance());
        assertEquals(formato.getCredisp(), result.getLimits().getAvailableBalance());

        assertEquals(formato.getFeculpa(), result.getPaymentDate());

        assertEquals(formato.getTeacomp(), result.getInterest().getPurchasePEN());
        assertEquals(formato.getTeacomu(), result.getInterest().getPurchaseUSD());
        assertEquals(formato.getTeaavap(), result.getInterest().getAdvancedPEN());
        assertEquals(formato.getTeaavau(), result.getInterest().getAdvancedUSD());
        assertEquals(formato.getTeacmpp(), result.getInterest().getCountervailingPEN());
        assertEquals(formato.getTeacmpu(), result.getInterest().getCountervailingUSD());
        assertEquals(formato.getTeamorp(), result.getInterest().getArrearsPEN());
        assertEquals(formato.getTeamoru(), result.getInterest().getArrearsUSD());

        assertEquals(formato.getCreutip(), result.getDisposedBalanceLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getDisposedBalanceLocalCurrency().getCurrency());
        assertEquals(formato.getCreutiu(), result.getDisposedBalance().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getDisposedBalance().getCurrency());

        assertEquals(formato.getNummovi(), result.getTotalTransactions());

        assertEquals(formato.getImpcuot(), result.getLimits().getFinancingDisposedBalance());

        assertEquals(formato.getImpatpe(), result.getExtraPayments().getOutstandingBalanceLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getOutstandingBalanceLocalCurrency().getCurrency());
        assertEquals(formato.getMonmipe(), result.getExtraPayments().getMinimumCapitalLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getMinimumCapitalLocalCurrency().getCurrency());
        assertEquals(formato.getImpinpe(), result.getExtraPayments().getInterestsBalanceLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getInterestsBalanceLocalCurrency().getCurrency());
        assertEquals(formato.getImpcope(), result.getExtraPayments().getFeesBalanceLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getFeesBalanceLocalCurrency().getCurrency());
        assertEquals(formato.getCuopape(), result.getExtraPayments().getFinancingTransactionLocalBalance().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getFinancingTransactionLocalBalance().getCurrency());
        assertEquals(formato.getPagmipe(), result.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getCurrency());
        assertEquals(formato.getPagtope(), result.getExtraPayments().getInvoiceAmountLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getInvoiceAmountLocalCurrency().getCurrency());

        assertEquals(formato.getImpatus(), result.getExtraPayments().getOutstandingBalance().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getOutstandingBalance().getCurrency());
        assertEquals(formato.getMonmius(), result.getExtraPayments().getMinimumCapitalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getMinimumCapitalCurrency().getCurrency());
        assertEquals(formato.getImpinus(), result.getExtraPayments().getInterestsBalance().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getInterestsBalance().getCurrency());
        assertEquals(formato.getImpcous(), result.getExtraPayments().getFeesBalance().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getFeesBalance().getCurrency());
        assertEquals(formato.getCuopaus(), result.getExtraPayments().getFinancingTransactionBalance().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getFinancingTransactionBalance().getCurrency());
        assertEquals(formato.getPagmius(), result.getExtraPayments().getInvoiceMinimumAmountCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getInvoiceMinimumAmountCurrency().getCurrency());
        assertEquals(formato.getPagtous(), result.getExtraPayments().getInvoiceAmount().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getInvoiceAmount().getCurrency());

        assertEquals(formato.getImpdetp(), result.getTotalDebtLocalCurrency().getAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getTotalDebtLocalCurrency().getCurrency());
        assertEquals(formato.getImpdetu(), result.getTotalDebt().getAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getTotalDebt().getCurrency());

        assertEquals(formato.getMapmini(), result.getTotalMonthsAmortizationMinimum());
        assertEquals(formato.getMapminf(), result.getTotalMonthsAmortizationMinimumFull());
    }

    @Test
    public void testMapResponseFormatToDtoOut2() throws IOException {
        final List<FormatoMPMS2GH> formatos = FormatsMPGHMock.INSTANCE.getFormatoMPMS2GH();

        DTOIntCardStatement result = mapper.mapOut2(formatos.get(0), null);
        assertEquals(1, result.getRowCardStatement().size());
        assertEquals(formatos.get(0).getLinescu(), result.getRowCardStatement().get(0));

        result = mapper.mapOut2(formatos.get(1), result);
        assertEquals(2, result.getRowCardStatement().size());
        assertEquals(formatos.get(1).getLinescu(), result.getRowCardStatement().get(1));

        result = mapper.mapOut2(formatos.get(2), result);
        assertEquals(3, result.getRowCardStatement().size());
        assertEquals(formatos.get(1).getLinescu(), result.getRowCardStatement().get(1));
    }

    @Test
    public void testMapResponseFormatToDtoOut3() throws IOException {
        final List<FormatoMPMS3GH> formatos = FormatsMPGHMock.INSTANCE.getFormatoMPMS3GH();

        DTOIntCardStatement result = mapper.mapOut3(formatos.get(0), null);
        assertEquals(1, result.getMessages().size());

        result = mapper.mapOut3(formatos.get(1), result);
        assertEquals(2, result.getMessages().size());

        result = mapper.mapOut3(formatos.get(2), result);
        assertEquals(3, result.getMessages().size());
    }
}
