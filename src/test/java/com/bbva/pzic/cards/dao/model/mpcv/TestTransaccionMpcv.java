package com.bbva.pzic.cards.dao.model.mpcv;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPCV</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpcv {

    @InjectMocks
    private TransaccionMpcv transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpcv peticion = new PeticionTransaccionMpcv();
        RespuestaTransaccionMpcv respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpcv.class, RespuestaTransaccionMpcv.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpcv result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}