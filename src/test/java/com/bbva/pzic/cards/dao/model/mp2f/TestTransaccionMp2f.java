package com.bbva.pzic.cards.dao.model.mp2f;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MP2F</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMp2f {

    @InjectMocks
    private TransaccionMp2f transaccion;
    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionMp2f peticion = new PeticionTransaccionMp2f();
        RespuestaTransaccionMp2f respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMp2f.class, RespuestaTransaccionMp2f.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMp2f result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}