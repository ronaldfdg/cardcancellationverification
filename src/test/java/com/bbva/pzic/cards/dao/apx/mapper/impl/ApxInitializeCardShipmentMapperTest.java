package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputInitializeCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxInitializeCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt003_1.PeticionTransaccionPecpt003_1;
import com.bbva.pzic.cards.dao.model.pecpt003_1.RespuestaTransaccionPecpt003_1;
import com.bbva.pzic.cards.dao.model.pecpt003_1.mock.Pecpt003_1Stubs;
import com.bbva.pzic.cards.facade.v0.dto.InitializeShipment;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ApxInitializeCardShipmentMapperTest {

    private IApxInitializeCardShipmentMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxInitializeCardShipmentMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputInitializeCardShipment input = EntityMock.getInstance().buildInputInitializeCardShipment();
        PeticionTransaccionPecpt003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getExternalcode());
        assertNotNull(result.getEntityin().getParticipant());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumentnumber());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype().getId());
        assertNotNull(result.getEntityin().getParticipant().getCard());
        assertNotNull(result.getEntityin().getParticipant().getCard().getCardagreement());
        assertNotNull(result.getEntityin().getParticipant().getCard().getReferencenumber());
        assertNotNull(result.getEntityin().getShippingcompany());
        assertNotNull(result.getEntityin().getShippingcompany().getId());
        assertNotNull(result.getEntityin().getShippingdevice());
        assertNotNull(result.getEntityin().getShippingdevice().getId());
        assertNotNull(result.getEntityin().getShippingdevice().getMacaddress());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication().getName());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication().getVersion());

        assertEquals(input.getExternalCode(), result.getEntityin().getExternalcode());
        assertEquals(input.getParticipant().getIdentityDocument().getNumber(), result.getEntityin().getParticipant().getIdentitydocument().getDocumentnumber());
        assertEquals(input.getParticipant().getIdentityDocument().getDocumentType().getId(), result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype().getId());
        assertEquals(input.getParticipant().getCard().getCardAgreement(), result.getEntityin().getParticipant().getCard().getCardagreement());
        assertEquals(input.getParticipant().getCard().getReferenceNumber(), result.getEntityin().getParticipant().getCard().getReferencenumber());
        assertEquals(input.getShippingCompany().getId(), result.getEntityin().getShippingcompany().getId());
        assertEquals(input.getShippingDevice().getId(), result.getEntityin().getShippingdevice().getId());
        assertEquals(input.getShippingDevice().getMacAddress(), result.getEntityin().getShippingdevice().getMacaddress());
        assertEquals(input.getShippingDevice().getApplication().getName(), result.getEntityin().getShippingdevice().getApplication().getName());
        assertEquals(input.getShippingDevice().getApplication().getVersion(), result.getEntityin().getShippingdevice().getApplication().getVersion());
    }

    @Test
    public void mapInShippingDeviceMacAddressNullTest() throws IOException {
        InputInitializeCardShipment input = EntityMock.getInstance().buildInputInitializeCardShipment();
        input.getShippingDevice().setMacAddress(null);
        PeticionTransaccionPecpt003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getExternalcode());
        assertNotNull(result.getEntityin().getParticipant());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumentnumber());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype().getId());
        assertNotNull(result.getEntityin().getParticipant().getCard());
        assertNotNull(result.getEntityin().getParticipant().getCard().getCardagreement());
        assertNotNull(result.getEntityin().getParticipant().getCard().getReferencenumber());
        assertNotNull(result.getEntityin().getShippingcompany());
        assertNotNull(result.getEntityin().getShippingcompany().getId());
        assertNotNull(result.getEntityin().getShippingdevice());
        assertNotNull(result.getEntityin().getShippingdevice().getId());
        assertNull(result.getEntityin().getShippingdevice().getMacaddress());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication().getName());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication().getVersion());
    }

    @Test
    public void mapInExternalCodeNullTest() throws IOException {
        InputInitializeCardShipment input = EntityMock.getInstance().buildInputInitializeCardShipment();
        input.setExternalCode(null);
        PeticionTransaccionPecpt003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getEntityin());
        assertNull(result.getEntityin().getExternalcode());
        assertNotNull(result.getEntityin().getParticipant());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumentnumber());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype());
        assertNotNull(result.getEntityin().getParticipant().getIdentitydocument().getDocumenttype().getId());
        assertNotNull(result.getEntityin().getParticipant().getCard());
        assertNotNull(result.getEntityin().getParticipant().getCard().getCardagreement());
        assertNotNull(result.getEntityin().getParticipant().getCard().getReferencenumber());
        assertNotNull(result.getEntityin().getShippingcompany());
        assertNotNull(result.getEntityin().getShippingcompany().getId());
        assertNotNull(result.getEntityin().getShippingdevice());
        assertNotNull(result.getEntityin().getShippingdevice().getId());
        assertNotNull(result.getEntityin().getShippingdevice().getMacaddress());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication().getName());
        assertNotNull(result.getEntityin().getShippingdevice().getApplication().getVersion());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPecpt003_1 input = Pecpt003_1Stubs.getInstance().buildRespuestaTransaccionPecpt003_1();
        InitializeShipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
    }

    @Test
    public void mapOutNullTest() {
        InitializeShipment result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        InitializeShipment result = mapper.mapOut(new RespuestaTransaccionPecpt003_1());

        assertNull(result);
    }
}
