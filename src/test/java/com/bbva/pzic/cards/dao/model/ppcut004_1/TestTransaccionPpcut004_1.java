package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>PPCUT004</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPpcut004_1 {

    @InjectMocks
    private TransaccionPpcut004_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPpcut004_1 rq = new PeticionTransaccionPpcut004_1();
        RespuestaTransaccionPpcut004_1 rs = new RespuestaTransaccionPpcut004_1();

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionPpcut004_1.class, RespuestaTransaccionPpcut004_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPpcut004_1 result = transaccion.invocar(rq);

        Assert.assertEquals(result, rs);
    }
}