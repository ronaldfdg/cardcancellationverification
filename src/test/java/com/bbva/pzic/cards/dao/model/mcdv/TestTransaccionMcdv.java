package com.bbva.pzic.cards.dao.model.mcdv;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MCDV</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMcdv {

    @InjectMocks
    private TransaccionMcdv transaccion;
    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionMcdv peticion = new PeticionTransaccionMcdv();
        RespuestaTransaccionMcdv respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMcdv.class, RespuestaTransaccionMcdv.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMcdv result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}