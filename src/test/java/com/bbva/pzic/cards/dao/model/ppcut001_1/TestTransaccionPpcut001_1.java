package com.bbva.pzic.cards.dao.model.ppcut001_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PPCUT001</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPpcut001_1 {

    @InjectMocks
    private TransaccionPpcut001_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPpcut001_1 rq = new PeticionTransaccionPpcut001_1();
        RespuestaTransaccionPpcut001_1 rs = new RespuestaTransaccionPpcut001_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPpcut001_1.class, RespuestaTransaccionPpcut001_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPpcut001_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}
