package com.bbva.pzic.cards.dao.model.mpb2;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPB2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpb2 {

    @InjectMocks
    private TransaccionMpb2 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpb2 peticion = new PeticionTransaccionMpb2();
        RespuestaTransaccionMpb2 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpb2.class, RespuestaTransaccionMpb2.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpb2 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}