package com.bbva.pzic.cards.dao.model.pecpt005_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PECPT005</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPecpt005_1 {

    @InjectMocks
    private TransaccionPecpt005_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPecpt005_1 rq = new PeticionTransaccionPecpt005_1();
        RespuestaTransaccionPecpt005_1 rs = new RespuestaTransaccionPecpt005_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPecpt005_1.class, RespuestaTransaccionPecpt005_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPecpt005_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}