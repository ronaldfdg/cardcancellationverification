package com.bbva.pzic.cards.dao.model.mpde;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPDE</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpde {

    @InjectMocks
    private TransaccionMpde transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {

        PeticionTransaccionMpde peticion = new PeticionTransaccionMpde();
        RespuestaTransaccionMpde respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpde.class, RespuestaTransaccionMpde.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpde result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }


}