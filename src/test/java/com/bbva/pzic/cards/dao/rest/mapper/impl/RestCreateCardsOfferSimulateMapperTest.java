package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationQueryParams;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationResponse;
import com.bbva.pzic.cards.dao.rest.mock.stubs.CardsOfferSimulateMock;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Constants.HEADER_USER_AGENT;
import static com.bbva.pzic.cards.util.Enums.*;
import static org.junit.Assert.*;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestCreateCardsOfferSimulateMapperTest {

    @Mock
    private Translator translator;

    @Mock
    private InputHeaderManager inputHeaderManager;

    @InjectMocks
    private RestCreateCardsOfferSimulateMapper mapper;

    private void enumMapOut(final CardHolderSimulationResponse output) {
        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_CARD_TYPE_ID,
                output.getData().getIdClasificacion())).thenReturn(ENUM_CARDS_CARD_TYPE_ID);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_ITEMIZE_FEES_FEE_TYPE,
                output.getData().getIdComisionMemb())).thenReturn(ENUM_CARDS_ITEMIZE_FEES_FEE_TYPE0);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_ITEMIZE_FEES_MODE,
                output.getData().getIdModAplicMemb())).thenReturn(ENUM_CARDS_ITEMIZE_FEES_MODE0);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_ITEMIZE_FEES_FEE_TYPE,
                output.getData().getIdComisionEstadoCuenta())).thenReturn(ENUM_CARDS_ITEMIZE_FEES_FEE_TYPE1);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_ITEMIZE_FEES_MODE,
                output.getData().getIdModAplicMembEstadoCuenta())).thenReturn(ENUM_CARDS_ITEMIZE_FEES_MODE1);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_OFFERS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE,
                output.getData().getTipoSeguro())).thenReturn(ENUM_CARDS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_PERIOD_ID,
                output.getData().getIdPeriodicidad())).thenReturn(ENUM_CARDS_PERIOD_ID);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_PAYMENT_METHOD_ID,
                output.getData().getIdTipoDeuda())).thenReturn(ENUM_CARDS_PAYMENT_METHOD_ID);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_RATES_MODE, output.getData().getTasa().get(0).getModalidad()))
                .thenReturn(PERCENTAGE_VAL);

        Mockito.when(translator.translateBackendEnumValueStrictly(CARDS_RATES_MODE, output.getData().getTasa().get(1).getModalidad()))
                .thenReturn(AMOUNT_VAL);

        Mockito.when(inputHeaderManager.getHeader(HEADER_USER_AGENT)).thenReturn(HEADER_USER_AGENT_STUB);
    }

    @Test
    public void mapInFullTest() throws IOException {
        DTOIntDetailSimulation input = EntityMock.getInstance().getDTOIntCardOfferSimulate();
        input.setClientId(CLIENT_ID);
        HashMap<String, String> result = mapper.mapInQueryParams(input);
        assertNotNull(result);

        assertEquals(input.getOfferId(), result.get(CardHolderSimulationQueryParams.ID_PROPUESTA));
        assertEquals(input.getProduct().getId(), result.get(CardHolderSimulationQueryParams.BIN));
        assertEquals(input.getProduct().getSubproduct().getId(), result.get(CardHolderSimulationQueryParams.ID_SUB_PRODUCTO));
        assertEquals(CLIENT_ID, result.get(CardHolderSimulationQueryParams.CUSTOMER_ID));
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntDetailSimulation input = new DTOIntDetailSimulation();
        input.setOfferId(null);
        input.setProduct(null);
        HashMap<String, String> result = mapper.mapInQueryParams(input);
        assertNotNull(result);
        assertNull(result.get(CardHolderSimulationQueryParams.ID_PROPUESTA));
        assertNull(result.get(CardHolderSimulationQueryParams.CUSTOMER_ID));
        assertNull(result.get(CardHolderSimulationQueryParams.BIN));
        assertNull(result.get(CardHolderSimulationQueryParams.ID_SUB_PRODUCTO));
    }

    @Test
    public void mapOutFullTest() throws IOException {
        CardHolderSimulationResponse output = CardsOfferSimulateMock.getInstance().buildCarHolderSimulationResponse();
        enumMapOut(output);

        HolderSimulation result = mapper.mapOut(output);

        assertNotNull(result);
        assertNotNull(result.getDetails());
        assertNotNull(result.getDetails().getAdditionalProducts());
        assertEquals(1, result.getDetails().getAdditionalProducts().size());
        assertNotNull(result.getDetails().getAdditionalProducts().get(0));
        assertNotNull(result.getDetails().getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getDetails().getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getDetails().getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getDetails().getBenefits());
        assertEquals(1, result.getDetails().getBenefits().size());
        assertNotNull(result.getDetails().getBenefits().get(0));
        assertNotNull(result.getDetails().getBenefits().get(0).getId());
        assertNotNull(result.getDetails().getBenefits().get(0).getDescription());
        assertNotNull(result.getDetails().getCardType());
        assertNotNull(result.getDetails().getCardType().getId());
        assertNotNull(result.getDetails().getCardType().getName());
        assertNotNull(result.getDetails().getFees());
        assertNotNull(result.getDetails().getFees().getItemizeFees());
        assertEquals(2, result.getDetails().getFees().getItemizeFees().size());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0));
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getName());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getMode());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getMode().getId());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getMode().getName());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1));
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getName());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getMode());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getMode().getId());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getMode().getName());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getItemizeFeeUnit());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getDetails().getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getDetails().getImage());
        assertNotNull(result.getDetails().getImage().getId());
        assertNotNull(result.getDetails().getImage().getName());
        assertNotNull(result.getDetails().getImage().getUrl());
        assertNotNull(result.getDetails().getMaximumAmount());
        assertNotNull(result.getDetails().getMaximumAmount().getAmount());
        assertNotNull(result.getDetails().getMaximumAmount().getCurrency());
        assertNotNull(result.getDetails().getMembership());
        assertNotNull(result.getDetails().getMembership().getId());
        assertNotNull(result.getDetails().getMembership().getDescription());
        assertNotNull(result.getDetails().getMinimumAmount());
        assertNotNull(result.getDetails().getMinimumAmount().getAmount());
        assertNotNull(result.getDetails().getMinimumAmount().getCurrency());
        assertNotNull(result.getDetails().getPaymentMethod());
        assertNotNull(result.getDetails().getPaymentMethod().getId());
        assertNotNull(result.getDetails().getPaymentMethod().getName());
        assertNotNull(result.getDetails().getPaymentMethod().getPeriod());
        assertNotNull(result.getDetails().getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getDetails().getPaymentMethod().getPeriod().getName());
        assertNotNull(result.getDetails().getPaymentPeriod());
        assertNotNull(result.getDetails().getPaymentPeriod().getId());
        assertNotNull(result.getDetails().getPaymentPeriod().getName());
        assertNotNull(result.getDetails().getPaymentPeriod().getEndDays());
        assertEquals(2, result.getDetails().getPaymentPeriod().getEndDays().size());
        assertNotNull(result.getDetails().getPaymentPeriod().getEndDays().get(0));
        assertNotNull(result.getDetails().getPaymentPeriod().getEndDays().get(0).getDay());
        assertNotNull(result.getDetails().getPaymentPeriod().getEndDays().get(1));
        assertNotNull(result.getDetails().getPaymentPeriod().getEndDays().get(1).getDay());
        assertNotNull(result.getDetails().getProduct());
        assertNotNull(result.getDetails().getProduct().getId());
        assertNotNull(result.getDetails().getProduct().getName());
        assertNotNull(result.getDetails().getProduct().getSubproduct().getId());
        assertNotNull(result.getDetails().getProduct().getSubproduct().getName());
        assertNotNull(result.getDetails().getProduct().getSubproduct().getDescription());
        assertNotNull(result.getDetails().getRates());
        assertEquals(2, result.getDetails().getRates().size());
        assertNotNull(result.getDetails().getRates().get(0));
        assertNotNull(result.getDetails().getRates().get(0).getRateType());
        assertNotNull(result.getDetails().getRates().get(0).getRateType().getId());
        assertNotNull(result.getDetails().getRates().get(0).getRateType().getName());
        assertNotNull(result.getDetails().getRates().get(0).getCalculationDate());
        assertNotNull(result.getDetails().getRates().get(0).getMode());
        assertNotNull(result.getDetails().getRates().get(0).getMode().getId());
        assertNotNull(result.getDetails().getRates().get(0).getMode().getName());
        assertNotNull(result.getDetails().getRates().get(0).getUnit());
        assertNotNull(result.getDetails().getRates().get(0).getUnit().getPercentage());

        assertNotNull(result.getDetails().getRates().get(1));
        assertNotNull(result.getDetails().getRates().get(1).getRateType());
        assertNotNull(result.getDetails().getRates().get(1).getRateType().getId());
        assertNotNull(result.getDetails().getRates().get(1).getRateType().getName());
        assertNotNull(result.getDetails().getRates().get(1).getCalculationDate());
        assertNotNull(result.getDetails().getRates().get(1).getMode());
        assertNotNull(result.getDetails().getRates().get(1).getMode().getId());
        assertNotNull(result.getDetails().getRates().get(1).getMode().getName());
        assertNotNull(result.getDetails().getRates().get(1).getUnit());
        assertNotNull(result.getDetails().getRates().get(1).getUnit().getPercentage());
        assertNotNull(result.getDetails().getSuggestedAmount());
        assertNotNull(result.getDetails().getSuggestedAmount().getAmount());
        assertNotNull(result.getDetails().getSuggestedAmount().getCurrency());
        assertNull(result.getOfferType());

        assertEquals(output.getData().getMontoSeguro(), result.getDetails().getAdditionalProducts().get(0).getAmount());
        assertEquals(output.getData().getDivisaMontoSeguro(), result.getDetails().getAdditionalProducts().get(0).getCurrency());
        assertEquals(ENUM_CARDS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE, result.getDetails().getAdditionalProducts().get(0).getProductType());
        assertEquals(output.getData().getBeneficio().get(0).getId(), result.getDetails().getBenefits().get(0).getId());
        assertEquals(output.getData().getBeneficio().get(0).getDescripcion(), result.getDetails().getBenefits().get(0).getDescription());
        assertEquals(ENUM_CARDS_CARD_TYPE_ID, result.getDetails().getCardType().getId());
        assertEquals(output.getData().getNombreClasificacion(), result.getDetails().getCardType().getName());
        assertEquals(ENUM_CARDS_ITEMIZE_FEES_FEE_TYPE0, result.getDetails().getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(output.getData().getNombreMembresia(), result.getDetails().getFees().getItemizeFees().get(0).getName());
        assertEquals(ENUM_CARDS_ITEMIZE_FEES_MODE0, result.getDetails().getFees().getItemizeFees().get(0).getMode().getId());
        assertEquals(output.getData().getModAplicacionMemb(), result.getDetails().getFees().getItemizeFees().get(0).getMode().getName());
        assertEquals(output.getData().getMontoMembr(), result.getDetails().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(output.getData().getMonedaMembr(), result.getDetails().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(ENUM_CARDS_ITEMIZE_FEES_FEE_TYPE1, result.getDetails().getFees().getItemizeFees().get(1).getFeeType());
        assertEquals(output.getData().getNombreMembresiaEstadoCuenta(), result.getDetails().getFees().getItemizeFees().get(1).getName());
        assertEquals(ENUM_CARDS_ITEMIZE_FEES_MODE1, result.getDetails().getFees().getItemizeFees().get(1).getMode().getId());
        assertEquals(output.getData().getModAplicacionMembEstadoCuenta(), result.getDetails().getFees().getItemizeFees().get(1).getMode().getName());
        assertEquals(output.getData().getMontoMembrEstadoCuenta(), result.getDetails().getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertEquals(output.getData().getMonedaMembrEstadoCuenta(), result.getDetails().getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertEquals(output.getData().getIdImagen(), result.getDetails().getImage().getId());
        assertEquals(output.getData().getNombreImagen(), result.getDetails().getImage().getName());
        assertEquals(output.getData().getUrlImagen(), result.getDetails().getImage().getUrl());
        assertEquals(output.getData().getLimiteMaxLinea(), result.getDetails().getMaximumAmount().getAmount());
        assertEquals(output.getData().getMonedaLimiteMax(), result.getDetails().getMaximumAmount().getCurrency());
        assertEquals(output.getData().getIdProgBenef(), result.getDetails().getMembership().getId());
        assertEquals(output.getData().getNombreProgBenef(), result.getDetails().getMembership().getDescription());
        assertEquals(output.getData().getLimiteMinLinea(), result.getDetails().getMinimumAmount().getAmount());
        assertEquals(output.getData().getMonedaLimiteMin(), result.getDetails().getMinimumAmount().getCurrency());
        assertEquals(ENUM_CARDS_PAYMENT_METHOD_ID, result.getDetails().getPaymentMethod().getId());
        assertEquals(output.getData().getNombreTipoDeuda(), result.getDetails().getPaymentMethod().getName());
        assertEquals(ENUM_CARDS_PERIOD_ID, result.getDetails().getPaymentMethod().getPeriod().getId());
        assertEquals(output.getData().getNombrePeriodicidad(), result.getDetails().getPaymentMethod().getPeriod().getName());
        assertEquals(ENUM_CARDS_PERIOD_ID, result.getDetails().getPaymentPeriod().getId());
        assertEquals(output.getData().getNombrePeriodicidad(), result.getDetails().getPaymentPeriod().getName());
        assertEquals(output.getData().getDiapago().get(0).getValor(), result.getDetails().getPaymentPeriod().getEndDays().get(0).getDay());
        assertEquals(output.getData().getDiapago().get(1).getValor(), result.getDetails().getPaymentPeriod().getEndDays().get(1).getDay());
        assertEquals(output.getData().getBin(), result.getDetails().getProduct().getId());
        assertEquals(output.getData().getNombreMarca(), result.getDetails().getProduct().getName());
        assertEquals(output.getData().getIdSubproducto(), result.getDetails().getProduct().getSubproduct().getId());
        assertEquals(output.getData().getNombreSubproducto(), result.getDetails().getProduct().getSubproduct().getName());
        assertEquals(output.getData().getDescSubproducto(), result.getDetails().getProduct().getSubproduct().getDescription());
        assertEquals(output.getData().getTasa().get(0).getId(), result.getDetails().getRates().get(0).getRateType().getId());
        assertEquals(output.getData().getTasa().get(0).getNombre(), result.getDetails().getRates().get(0).getRateType().getName());
        assertEquals(output.getData().getTasa().get(0).getFecha(), result.getDetails().getRates().get(0).getCalculationDate());
        assertEquals(PERCENTAGE_VAL, result.getDetails().getRates().get(0).getMode().getId());
        assertEquals(AMOUNT_VAL, result.getDetails().getRates().get(1).getMode().getId());
        assertEquals(output.getData().getTasa().get(0).getDescModalidad(), result.getDetails().getRates().get(0).getMode().getName());
        assertEquals(output.getData().getTasa().get(0).getValor(), result.getDetails().getRates().get(0).getUnit().getPercentage());
        assertEquals(output.getData().getLineaSugerida(), result.getDetails().getSuggestedAmount().getAmount());
        assertEquals(output.getData().getMonedaLineaSug(), result.getDetails().getSuggestedAmount().getCurrency());
    }

    @Test
    public void mapOutEmptyTest() {
        CardHolderSimulationResponse output = new CardHolderSimulationResponse();
        HolderSimulation result = mapper.mapOut(output);

        assertNull(result);
    }

    @Test
    public void mapOutNullTest() {
        HolderSimulation result = mapper.mapOut(null);

        assertNull(result);
    }
}
