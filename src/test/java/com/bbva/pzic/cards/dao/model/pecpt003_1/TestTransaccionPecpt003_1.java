package com.bbva.pzic.cards.dao.model.pecpt003_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PECPT003</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPecpt003_1 {

    @InjectMocks
    private TransaccionPecpt003_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPecpt003_1 rq = new PeticionTransaccionPecpt003_1();
        RespuestaTransaccionPecpt003_1 rs = new RespuestaTransaccionPecpt003_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPecpt003_1.class, RespuestaTransaccionPecpt003_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionPecpt003_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}
