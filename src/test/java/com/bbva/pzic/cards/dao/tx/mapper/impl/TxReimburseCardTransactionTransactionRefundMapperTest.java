package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMEN6J;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMS16J;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TxReimburseCardTransactionTransactionRefundMapperTest {

    @InjectMocks
    private TxReimburseCardTransactionTransactionRefundMapper mapper;

    @Mock
    private Translator translator;

    private EntityMock mock = EntityMock.getInstance();

    @Test
    public void mapInFullTest() {
        InputReimburseCardTransactionTransactionRefund dtoInt = mock.getInputReimburseCardTransactionTransactionRefundMock();
        FormatoMPMEN6J result = mapper.mapIn(dtoInt);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getIdentif());
        assertNotNull(result.getMcindop());

        assertEquals(dtoInt.getCardId(), result.getIdetarj());
        assertEquals(dtoInt.getTransactionId(), result.getIdentif());
        assertEquals(dtoInt.getRefundType().getId(), result.getMcindop());
    }

    @Test
    public void mapInWithoutMcindop() {
        InputReimburseCardTransactionTransactionRefund input = mock.getInputReimburseCardWithoutRefundType();
        FormatoMPMEN6J result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getIdentif());
        assertNull(result.getMcindop());

        assertEquals(input.getCardId(), result.getIdetarj());
        assertEquals(input.getTransactionId(), result.getIdentif());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPMS16J response = mock.getFormatoMPMS16JMock();
        when(translator.translateBackendEnumValueStrictly(Enums.TRANSACTIONREFUND_REFUNDTYPE_ID, response.getMcindop())).thenReturn(Constants.REFUND);
        when(translator.translateBackendEnumValueStrictly(Enums.TRANSACTIONREFUND_STATUS_ID, response.getCodope())).thenReturn(Constants.APPROVED);
        ReimburseCardTransactionTransactionRefund result = mapper.mapOut(response);

        assertNotNull(result);
        assertNotNull(result.getRefundType());
        assertNotNull(result.getRefundType().getId());
        assertNotNull(result.getRefundType().getName());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getId());
        assertNotNull(result.getTransaction());
        assertNotNull(result.getTransaction().getId());

        assertEquals(Constants.REFUND, result.getRefundType().getId());
        assertEquals(response.getMcindes(), result.getRefundType().getName());
        assertEquals(response.getIdetarj(), result.getCard().getId());
        assertEquals(response.getIdentif(), result.getTransaction().getId());
        assertEquals(Constants.APPROVED, result.getStatus().getId());
        assertEquals(response.getDesoper(), result.getStatus().getDescription());
    }

    @Test
    public void mapOutEmptyTest() {
        ReimburseCardTransactionTransactionRefund result = mapper.mapOut(null);

        assertNull(result);
    }

}
