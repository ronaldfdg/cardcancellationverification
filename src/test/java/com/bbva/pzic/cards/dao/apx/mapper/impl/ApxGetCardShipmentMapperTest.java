package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxGetCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt004_1.PeticionTransaccionPecpt004_1;
import com.bbva.pzic.cards.dao.model.pecpt004_1.RespuestaTransaccionPecpt004_1;
import com.bbva.pzic.cards.dao.model.pecpt004_1.mock.Pecpt004_1Stubs;
import com.bbva.pzic.cards.facade.v0.dto.Shipment;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ApxGetCardShipmentMapperTest {

    private IApxGetCardShipmentMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxGetCardShipmentMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetCardShipment input = EntityMock.getInstance().buildInputGetCardShipment();
        PeticionTransaccionPecpt004_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getShipmentid());
        assertNotNull(result.getExpand());

        assertEquals(input.getShipmentId(), result.getShipmentid());
        assertEquals(input.getExpand(), result.getExpand());
    }

    @Test
    public void mapIEmptyTest() {
        InputGetCardShipment input = new InputGetCardShipment();
        PeticionTransaccionPecpt004_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNull(result.getShipmentid());
        assertNull(result.getExpand());
    }

    @Test
    public void mapOutGetCardShipmentFullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());

        assertEquals(input.getEntityout().getId(), result.getId());
        assertEquals(input.getEntityout().getStatus().getId(), result.getStatus().getId());
        assertEquals(input.getEntityout().getStatus().getDescription(), result.getStatus().getDescription());
        assertEquals(input.getEntityout().getParticipant().getFullname(), result.getParticipant().getFullName());
        assertEquals(input.getEntityout().getParticipant().getIdentitydocument().getDocumentnumber(), result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertEquals(input.getEntityout().getParticipant().getIdentitydocument().getDocumenttype().getId(), result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getEntityout().getParticipant().getIdentitydocument().getDocumenttype().getDescription(), result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertEquals(input.getEntityout().getParticipant().getCards().get(0).getCard().getCardagreement(), result.getParticipant().getCards().get(0).getCardAgreement());
        assertEquals(input.getEntityout().getParticipant().getCards().get(0).getCard().getReferencenumber(), result.getParticipant().getCards().get(0).getReferenceNumber());
        assertEquals(input.getEntityout().getParticipant().getCards().get(1).getCard().getCardagreement(), result.getParticipant().getCards().get(1).getCardAgreement());
        assertEquals(input.getEntityout().getParticipant().getCards().get(1).getCard().getReferencenumber(), result.getParticipant().getCards().get(1).getReferenceNumber());
        assertEquals(input.getEntityout().getShippingcompany().getId(), result.getShippingCompany().getId());
        assertEquals(input.getEntityout().getShippingcompany().getName(), result.getShippingCompany().getName());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getId(), result.getAddresses().get(0).getShipmentAddress().getId());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getAddresstype(), result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getFormattedaddress(), result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getLocationtypes().get(0).getLocationtype(), result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().get(0));
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getAddresscomponents().get(0).getAddresscomponent().getComponenttypes().get(0).getComponenttype(), result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getAddresscomponents().get(0).getAddresscomponent().getCode(), result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getAddresscomponents().get(0).getAddresscomponent().getName(), result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getAddresscomponents().get(1).getAddresscomponent().getComponenttypes().get(0).getComponenttype(), result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getAddresscomponents().get(1).getAddresscomponent().getCode(), result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getAddresscomponents().get(1).getAddresscomponent().getName(), result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getGeolocation().getLatitude(), result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude().toString());
        assertEquals(input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().getLocation().getGeolocation().getLongitude(), result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude().toString());
        assertEquals(input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().getId(), result.getAddresses().get(1).getShipmentAddress().getId());
        assertEquals(input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().getAddresstype(), result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertEquals(input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().getDestination().getId(), result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertEquals(input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().getDestination().getName(), result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
    }

    @Test
    public void mapOutGetCardShipmentStatusNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().setStatus(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNull(result.getStatus());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentTermNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().setTerm(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentParticipantNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().setParticipant(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNull(result.getParticipant());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentShipppingCompanyNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().setShippingcompany(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNull(result.getShippingCompany());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentParticipantIdentityDocumentNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getParticipant().setIdentitydocument(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentParticipantIdentityDocumentDocumentTypeNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getParticipant().getIdentitydocument().setDocumenttype(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentParticipantCardsNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getParticipant().setCards(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNull(result.getParticipant().getCards());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentAddressesNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().setAddresses(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNull(result.getAddresses());
    }

    @Test
    public void mapOutGetCardShipmentAddressesAddressTypeSpecificNullFullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().setAddresstype(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(1, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentSpecificAddressIdNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().setId(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentSpecificAddressLocationNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getAddresses().get(0).getAddress().getShipmentaddress().setLocation(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentStoredAddressIdNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().setId(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentStoredAddressDestinationNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().setDestination(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutGetCardShipmentStoredAddressDestinationIdAndNameNullTest() throws IOException {
        RespuestaTransaccionPecpt004_1 input = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
        input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().getDestination().setId(null);
        input.getEntityout().getAddresses().get(1).getAddress().getShipmentaddress().getDestination().setName(null);
        Shipment result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getTerm());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getFullName());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getDescription());
        assertNotNull(result.getParticipant().getCards());
        assertEquals(2, result.getParticipant().getCards().size());
        assertNotNull(result.getParticipant().getCards().get(0));
        assertNotNull(result.getParticipant().getCards().get(0).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(0).getReferenceNumber());
        assertNotNull(result.getParticipant().getCards().get(1));
        assertNotNull(result.getParticipant().getCards().get(1).getCardAgreement());
        assertNotNull(result.getParticipant().getCards().get(1).getReferenceNumber());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingCompany().getName());
        assertNotNull(result.getAddresses());
        assertEquals(2, result.getAddresses().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getLocationTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents());
        assertEquals(2, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLatitude());
        assertNotNull(result.getAddresses().get(0).getShipmentAddress().getLocation().getGeolocation().getLongitude());
        assertNull(result.getAddresses().get(0).getShipmentAddress().getDestination());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getId());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getAddressType());
        assertNotNull(result.getAddresses().get(1).getShipmentAddress().getDestination());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getId());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getDestination().getName());
        assertNull(result.getAddresses().get(1).getShipmentAddress().getLocation());
    }

    @Test
    public void mapOutNullTest() {
        Shipment result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        Shipment result = mapper.mapOut(new RespuestaTransaccionPecpt004_1());

        assertNull(result);
    }
}
