package com.bbva.pzic.cards.dao.model.mpw1;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPW1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpw1 {

    @InjectMocks
    private TransaccionMpw1 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpw1 peticion = new PeticionTransaccionMpw1();
        RespuestaTransaccionMpw1 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpw1.class, RespuestaTransaccionMpw1.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpw1 result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }
}