package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpb1.FormatoMPM0BX;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxModifyCardMapperActivationMapperTest {

    private static final String CASH_WITHDRAWAL_ACTIVATION_BACKEND_VALUE = "02";
    private final EntityMock entityMock = EntityMock.getInstance();
    @InjectMocks
    private TxModifyCardActivationMapper txModifyCardActivationMapper;
    @Mock
    private EnumMapper enumMapper;

    @Before
    public void init() {
        Mockito.when(enumMapper.getBackendValue("cards.activation.activationId", "CASHWITHDRAWAL_ACTIVATION"))
                .thenReturn(CASH_WITHDRAWAL_ACTIVATION_BACKEND_VALUE);
    }

    @Test
    public void testMapInput() {
        final DTOIntActivation dtoIntActivation = entityMock.buildDTOIntActivation();

        FormatoMPM0BX formatoMPM0BX = txModifyCardActivationMapper.mapInput(dtoIntActivation);

        assertEquals(dtoIntActivation.getCard().getCardId(), formatoMPM0BX.getIdetarj());
        assertEquals(CASH_WITHDRAWAL_ACTIVATION_BACKEND_VALUE, formatoMPM0BX.getCodactv());
        assertNotNull(formatoMPM0BX.getIndactv());
        assertTrue(formatoMPM0BX.getIndactv().equalsIgnoreCase("S"));


        dtoIntActivation.setIsActive(false);
        formatoMPM0BX = txModifyCardActivationMapper.mapInput(dtoIntActivation);

        assertNotNull(formatoMPM0BX.getIndactv());
        assertTrue(formatoMPM0BX.getIndactv().equalsIgnoreCase("N"));

        dtoIntActivation.setIsActive(null);
        formatoMPM0BX = txModifyCardActivationMapper.mapInput(dtoIntActivation);
        assertNull(formatoMPM0BX.getIndactv());
    }
}
