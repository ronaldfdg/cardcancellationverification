package com.bbva.pzic.cards.dao.model.mpg1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPG1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpg1 {

    @InjectMocks
    private TransaccionMpg1 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpg1 peticion = new PeticionTransaccionMpg1();
        RespuestaTransaccionMpg1 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpg1.class, RespuestaTransaccionMpg1.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpg1 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}