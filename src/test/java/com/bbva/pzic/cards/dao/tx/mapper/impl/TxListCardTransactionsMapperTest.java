package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.canonic.TransactionsData;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMENL2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS1L2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS2L2;
import com.bbva.pzic.cards.dao.model.mpl2.mock.FormatsMpl2Mock;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import com.bbva.pzic.utilTest.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListCardTransactionsMapperTest {

    private static final String SETTLED = "SETTLED";
    private static final String PAN = "PAN";
    private static final String PURCHASED = "C";
    private static final String FINANCING_AVAILABLE = "FINANCING_AVAILABLE";
    private static final String DECRYPTED_VALUE = "1";

    @InjectMocks
    private TxListCardTransactionsMapper txListTransactionsMapper;

    @Mock
    private Translator translator;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock entityMock = EntityMock.getInstance();
    private FormatsMpl2Mock formatsMpl2Mock = FormatsMpl2Mock.getInstance();

    @Before
    public void init() {
        Mockito.when(translator.translateBackendEnumValueStrictly("transactions.status.id", "C")).thenReturn(SETTLED);
        Mockito.when(translator.translateBackendEnumValueStrictly("contracts.numberType.id", "1")).thenReturn(PAN);
        Mockito.when(translator.translateBackendEnumValueStrictly("transactions.transactionType.id", "CASH_WITHDRAWAL")).thenReturn(PURCHASED);
        Mockito.when(translator.translateBackendEnumValueStrictly("transactions.financingType.id", "0")).thenReturn(FINANCING_AVAILABLE);
        Mockito.when(cypherTool.encrypt("45001232134949198127", AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS)).thenReturn(DECRYPTED_VALUE);
    }

    @Test
    public void testMapInput() {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMENL2 formatoMPMENL2 = txListTransactionsMapper.mapInput(dtoInputListCardTransactions);
        assertNotNull(formatoMPMENL2);
        assertNotNull(formatoMPMENL2.getNumtarj());
        assertEquals(dtoInputListCardTransactions.getCardId(), formatoMPMENL2.getNumtarj());
        assertNotNull(formatoMPMENL2.getFechini());
        assertEquals(DateUtils.getFormatDefaultHost(formatoMPMENL2.getFechini()), dtoInputListCardTransactions.getFromOperationDate());
        assertNotNull(formatoMPMENL2.getFechfin());
        assertEquals(DateUtils.getFormatDefaultHost(formatoMPMENL2.getFechfin()), dtoInputListCardTransactions.getToOperationDate());
        assertNotNull(formatoMPMENL2.getIdpagin());
        assertEquals(dtoInputListCardTransactions.getPaginationKey(), formatoMPMENL2.getIdpagin());
        assertNotNull(formatoMPMENL2.getTampagi());
        assertEquals(dtoInputListCardTransactions.getPageSize().toString(), formatoMPMENL2.getTampagi().toString());
    }

    @Test
    public void testMapInputWithNullFromOperationDate() {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        dtoInputListCardTransactions.setFromOperationDate(null);
        final FormatoMPMENL2 formatoMPMENL2 = txListTransactionsMapper.mapInput(dtoInputListCardTransactions);
        assertNotNull(formatoMPMENL2);
        assertNotNull(formatoMPMENL2.getNumtarj());
        assertEquals(dtoInputListCardTransactions.getCardId(), formatoMPMENL2.getNumtarj());
        assertNull(formatoMPMENL2.getFechini());
        assertNotNull(formatoMPMENL2.getFechfin());
        assertNotNull(formatoMPMENL2.getIdpagin());
        assertEquals(dtoInputListCardTransactions.getPaginationKey(), formatoMPMENL2.getIdpagin());
        assertNotNull(formatoMPMENL2.getTampagi());
        assertEquals(dtoInputListCardTransactions.getPageSize().toString(), formatoMPMENL2.getTampagi().toString());
    }

    @Test
    public void testMapInputWithNullToOperationDate() {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        dtoInputListCardTransactions.setToOperationDate(null);
        final FormatoMPMENL2 formatoMPMENL2 = txListTransactionsMapper.mapInput(dtoInputListCardTransactions);
        assertNotNull(formatoMPMENL2);
        assertNotNull(formatoMPMENL2.getNumtarj());
        assertEquals(dtoInputListCardTransactions.getCardId(), formatoMPMENL2.getNumtarj());
        assertNotNull(formatoMPMENL2.getFechini());
        assertNull(formatoMPMENL2.getFechfin());
        assertNotNull(formatoMPMENL2.getIdpagin());
        assertEquals(dtoInputListCardTransactions.getPaginationKey(), formatoMPMENL2.getIdpagin());
        assertNotNull(formatoMPMENL2.getTampagi());
        assertEquals(dtoInputListCardTransactions.getPageSize().toString(), formatoMPMENL2.getTampagi().toString());
    }

    @Test
    public void testMapInputWithNullPaginationKey() {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        dtoInputListCardTransactions.setPaginationKey(null);
        final FormatoMPMENL2 formatoMPMENL2 = txListTransactionsMapper.mapInput(dtoInputListCardTransactions);
        assertNotNull(formatoMPMENL2);
        assertNotNull(formatoMPMENL2.getNumtarj());
        assertEquals(dtoInputListCardTransactions.getCardId(), formatoMPMENL2.getNumtarj());
        assertNotNull(formatoMPMENL2.getFechini());
        assertNotNull(formatoMPMENL2.getFechfin());
        assertNull(formatoMPMENL2.getIdpagin());
        assertNotNull(formatoMPMENL2.getTampagi());
        assertEquals(dtoInputListCardTransactions.getPageSize().toString(), formatoMPMENL2.getTampagi().toString());
    }

    @Test
    public void testMapInputWithNullPageSize() {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        dtoInputListCardTransactions.setPageSize(null);
        final FormatoMPMENL2 formatoMPMENL2 = txListTransactionsMapper.mapInput(dtoInputListCardTransactions);
        assertNotNull(formatoMPMENL2);
        assertNotNull(formatoMPMENL2.getNumtarj());
        assertEquals(dtoInputListCardTransactions.getCardId(), formatoMPMENL2.getNumtarj());
        assertNotNull(formatoMPMENL2.getFechini());
        assertNotNull(formatoMPMENL2.getFechfin());
        assertNotNull(formatoMPMENL2.getIdpagin());
        assertEquals(dtoInputListCardTransactions.getPaginationKey(), formatoMPMENL2.getIdpagin());
        assertNull(formatoMPMENL2.getTampagi());
    }

    @Test
    public void testMapOutputList() throws IOException {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMS1L2 formatoMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2().get(0);
        final TransactionsData transactionsData =
                txListTransactionsMapper.mapOutputList(formatoMPMS1L2, new TransactionsData(), dtoInputListCardTransactions);
        assertNotNull(transactionsData);
        assertNotNull(transactionsData.getData());
        assertFalse(transactionsData.getData().isEmpty());
        final Transaction firstTransaction = transactionsData.getData().get(0);
        assertNotNull(firstTransaction);

        assertNotNull(firstTransaction.getId());
        assertEquals(formatoMPMS1L2.getNumoper(), firstTransaction.getId());

        assertNotNull(firstTransaction.getConcept());
        assertEquals(formatoMPMS1L2.getDesoper(), firstTransaction.getConcept());

        assertNotNull(firstTransaction.getOriginAmount());

        assertNotNull(firstTransaction.getOriginAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmori(), firstTransaction.getOriginAmount().getAmount());

        assertNotNull(firstTransaction.getOriginAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMonmori(), firstTransaction.getOriginAmount().getCurrency());

        assertNotNull(firstTransaction.getLocalAmount());

        assertNotNull(firstTransaction.getLocalAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmtit(), firstTransaction.getLocalAmount().getAmount());

        assertNotNull(firstTransaction.getLocalAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMopmtit(), firstTransaction.getLocalAmount().getCurrency());

        assertNotNull(firstTransaction.getOperationDate());

        assertNotNull(firstTransaction.getAccountedDate());
        assertEquals(formatoMPMS1L2.getFechcon(), firstTransaction.getAccountedDate().getTime());

        assertNotNull(firstTransaction.getValuationDate());
        assertEquals(formatoMPMS1L2.getFechcon(), firstTransaction.getValuationDate().getTime());

        assertNotNull(firstTransaction.getStatus());

        assertNotNull(firstTransaction.getStatus().getId());
        assertEquals(SETTLED, firstTransaction.getStatus().getId());

        assertNotNull(firstTransaction.getStatus().getName());
        assertEquals(formatoMPMS1L2.getDestope(), firstTransaction.getStatus().getName());

        assertNotNull(firstTransaction.getContract());

        assertNotNull(firstTransaction.getContract().getId());
        assertEquals(DECRYPTED_VALUE, firstTransaction.getContract().getId());

        assertNotNull(firstTransaction.getContract().getNumber());
        assertEquals(formatoMPMS1L2.getNumconv(), firstTransaction.getContract().getNumber());

        assertNotNull(firstTransaction.getContract().getNumberType());

        assertNotNull(firstTransaction.getContract().getNumberType().getId());
        assertEquals(PAN, firstTransaction.getContract().getNumberType().getId());

        assertNotNull(firstTransaction.getContract().getNumberType().getName());
        assertEquals(formatoMPMS1L2.getDetcore(), firstTransaction.getContract().getNumberType().getName());

        assertNotNull(firstTransaction.getTransactionType());

        assertNotNull(firstTransaction.getTransactionType().getId());
        assertEquals(PURCHASED, firstTransaction.getTransactionType().getId());

        assertNotNull(firstTransaction.getTransactionType().getName());
        assertEquals(formatoMPMS1L2.getNotipop(), firstTransaction.getTransactionType().getName());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getId());
        assertEquals(formatoMPMS1L2.getCodoper(), firstTransaction.getTransactionType().getInternalCode().getId());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getName());
        assertEquals(formatoMPMS1L2.getNomcod(), firstTransaction.getTransactionType().getInternalCode().getName());

        assertNotNull(firstTransaction.getFinancingType().getId());
        assertEquals(FINANCING_AVAILABLE, firstTransaction.getFinancingType().getId());

        assertNotNull(firstTransaction.getFinancingType().getName());
        assertEquals(formatoMPMS1L2.getDetipfn(), firstTransaction.getFinancingType().getName());


    }

    @Test
    public void testMapOutputListWithoutFechconNomCode() throws IOException {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMS1L2 formatoMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2().get(0);
        formatoMPMS1L2.setFechcon(null);
        formatoMPMS1L2.setNomcod(null);
        final TransactionsData transactionsData =
                txListTransactionsMapper.mapOutputList(formatoMPMS1L2, new TransactionsData(), dtoInputListCardTransactions);
        assertNotNull(transactionsData);
        assertNotNull(transactionsData.getData());
        assertFalse(transactionsData.getData().isEmpty());
        final Transaction firstTransaction = transactionsData.getData().get(0);
        assertNotNull(firstTransaction);

        assertNotNull(firstTransaction.getId());
        assertEquals(formatoMPMS1L2.getNumoper(), firstTransaction.getId());

        assertNotNull(firstTransaction.getConcept());
        assertEquals(formatoMPMS1L2.getDesoper(), firstTransaction.getConcept());

        assertNotNull(firstTransaction.getOriginAmount());

        assertNotNull(firstTransaction.getOriginAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmori(), firstTransaction.getOriginAmount().getAmount());

        assertNotNull(firstTransaction.getOriginAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMonmori(), firstTransaction.getOriginAmount().getCurrency());

        assertNotNull(firstTransaction.getLocalAmount());

        assertNotNull(firstTransaction.getLocalAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmtit(), firstTransaction.getLocalAmount().getAmount());

        assertNotNull(firstTransaction.getLocalAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMopmtit(), firstTransaction.getLocalAmount().getCurrency());

        assertNotNull(firstTransaction.getOperationDate());

        assertNull(firstTransaction.getAccountedDate());
        assertNull(firstTransaction.getValuationDate());

        assertNotNull(firstTransaction.getStatus());

        assertNotNull(firstTransaction.getStatus().getId());
        assertEquals(SETTLED, firstTransaction.getStatus().getId());

        assertNotNull(firstTransaction.getStatus().getName());
        assertEquals(formatoMPMS1L2.getDestope(), firstTransaction.getStatus().getName());

        assertNotNull(firstTransaction.getContract());

        assertNotNull(firstTransaction.getContract().getId());
        assertEquals(DECRYPTED_VALUE, firstTransaction.getContract().getId());

        assertNotNull(firstTransaction.getContract().getNumber());
        assertEquals(formatoMPMS1L2.getNumconv(), firstTransaction.getContract().getNumber());

        assertNotNull(firstTransaction.getContract().getNumberType());

        assertNotNull(firstTransaction.getContract().getNumberType().getId());
        assertEquals(PAN, firstTransaction.getContract().getNumberType().getId());

        assertNotNull(firstTransaction.getContract().getNumberType().getName());
        assertEquals(formatoMPMS1L2.getDetcore(), firstTransaction.getContract().getNumberType().getName());

        assertNotNull(firstTransaction.getTransactionType());

        assertNotNull(firstTransaction.getTransactionType().getId());
        assertEquals(PURCHASED, firstTransaction.getTransactionType().getId());

        assertNotNull(firstTransaction.getTransactionType().getName());
        assertEquals(formatoMPMS1L2.getNotipop(), firstTransaction.getTransactionType().getName());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getId());
        assertEquals(formatoMPMS1L2.getCodoper(), firstTransaction.getTransactionType().getInternalCode().getId());

        assertNull(firstTransaction.getTransactionType().getInternalCode().getName());

        assertNotNull(firstTransaction.getFinancingType().getId());
        assertEquals(FINANCING_AVAILABLE, firstTransaction.getFinancingType().getId());

        assertNotNull(firstTransaction.getFinancingType().getName());
        assertEquals(formatoMPMS1L2.getDetipfn(), firstTransaction.getFinancingType().getName());
    }

    @Test
    public void testMapOutputListWithContractNull() throws IOException {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMS1L2 formatoMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2().get(0);
        formatoMPMS1L2.setNumconv(null);
        formatoMPMS1L2.setIdtcore(null);
        formatoMPMS1L2.setDetcore(null);
        final TransactionsData transactionsData =
                txListTransactionsMapper.mapOutputList(formatoMPMS1L2, new TransactionsData(), dtoInputListCardTransactions);
        assertNotNull(transactionsData);
        assertNotNull(transactionsData.getData());
        assertFalse(transactionsData.getData().isEmpty());
        final Transaction firstTransaction = transactionsData.getData().get(0);
        assertNotNull(firstTransaction);

        assertNotNull(firstTransaction.getId());
        assertEquals(formatoMPMS1L2.getNumoper(), firstTransaction.getId());

        assertNotNull(firstTransaction.getConcept());
        assertEquals(formatoMPMS1L2.getDesoper(), firstTransaction.getConcept());

        assertNotNull(firstTransaction.getOriginAmount());

        assertNotNull(firstTransaction.getOriginAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmori(), firstTransaction.getOriginAmount().getAmount());

        assertNotNull(firstTransaction.getOriginAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMonmori(), firstTransaction.getOriginAmount().getCurrency());

        assertNotNull(firstTransaction.getLocalAmount());

        assertNotNull(firstTransaction.getLocalAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmtit(), firstTransaction.getLocalAmount().getAmount());

        assertNotNull(firstTransaction.getLocalAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMopmtit(), firstTransaction.getLocalAmount().getCurrency());

        assertNotNull(firstTransaction.getOperationDate());

        assertNotNull(firstTransaction.getAccountedDate());

        assertEquals(formatoMPMS1L2.getFechcon(), firstTransaction.getAccountedDate().getTime());

        assertNotNull(firstTransaction.getValuationDate());
        assertEquals(formatoMPMS1L2.getFechcon(), firstTransaction.getValuationDate().getTime());

        assertNotNull(firstTransaction.getStatus());

        assertNotNull(firstTransaction.getStatus().getId());
        assertEquals(SETTLED, firstTransaction.getStatus().getId());

        assertNotNull(firstTransaction.getStatus().getName());
        assertEquals(formatoMPMS1L2.getDestope(), firstTransaction.getStatus().getName());

        assertNull(firstTransaction.getContract());

        assertNotNull(firstTransaction.getTransactionType());

        assertNotNull(firstTransaction.getTransactionType().getId());
        assertEquals(PURCHASED, firstTransaction.getTransactionType().getId());

        assertNotNull(firstTransaction.getTransactionType().getName());
        assertEquals(formatoMPMS1L2.getNotipop(), firstTransaction.getTransactionType().getName());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getId());
        assertEquals(formatoMPMS1L2.getCodoper(), firstTransaction.getTransactionType().getInternalCode().getId());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getName());
        assertEquals(formatoMPMS1L2.getNomcod(), firstTransaction.getTransactionType().getInternalCode().getName());
    }

    @Test
    public void testMapOutputListNullFieldNumconv() throws IOException {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMS1L2 formatoMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2().get(0);
        formatoMPMS1L2.setNumconv(null);
        final TransactionsData transactionsData =
                txListTransactionsMapper.mapOutputList(formatoMPMS1L2, new TransactionsData(), dtoInputListCardTransactions);
        assertNotNull(transactionsData);
        assertNotNull(transactionsData.getData());
        assertFalse(transactionsData.getData().isEmpty());
        final Transaction firstTransaction = transactionsData.getData().get(0);
        assertNotNull(firstTransaction);

        assertNotNull(firstTransaction.getId());
        assertEquals(formatoMPMS1L2.getNumoper(), firstTransaction.getId());

        assertNotNull(firstTransaction.getConcept());
        assertEquals(formatoMPMS1L2.getDesoper(), firstTransaction.getConcept());

        assertNotNull(firstTransaction.getOriginAmount());

        assertNotNull(firstTransaction.getOriginAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmori(), firstTransaction.getOriginAmount().getAmount());

        assertNotNull(firstTransaction.getOriginAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMonmori(), firstTransaction.getOriginAmount().getCurrency());

        assertNotNull(firstTransaction.getLocalAmount());

        assertNotNull(firstTransaction.getLocalAmount().getAmount());
        assertEquals(formatoMPMS1L2.getImpmtit(), firstTransaction.getLocalAmount().getAmount());

        assertNotNull(firstTransaction.getLocalAmount().getCurrency());
        assertEquals(formatoMPMS1L2.getMopmtit(), firstTransaction.getLocalAmount().getCurrency());

        assertNotNull(firstTransaction.getOperationDate());

        assertNotNull(firstTransaction.getAccountedDate());

        assertEquals(formatoMPMS1L2.getFechcon(), firstTransaction.getAccountedDate().getTime());

        assertNotNull(firstTransaction.getValuationDate());
        assertEquals(formatoMPMS1L2.getFechcon(), firstTransaction.getValuationDate().getTime());

        assertNotNull(firstTransaction.getStatus());

        assertNotNull(firstTransaction.getStatus().getId());
        assertEquals(SETTLED, firstTransaction.getStatus().getId());

        assertNotNull(firstTransaction.getStatus().getName());
        assertEquals(formatoMPMS1L2.getDestope(), firstTransaction.getStatus().getName());

        assertNotNull(firstTransaction.getContract());

        assertNull(firstTransaction.getContract().getId());

        assertNull(firstTransaction.getContract().getNumber());

        assertNotNull(firstTransaction.getContract().getNumberType());

        assertNotNull(firstTransaction.getContract().getNumberType().getId());
        assertEquals(PAN, firstTransaction.getContract().getNumberType().getId());

        assertNotNull(firstTransaction.getContract().getNumberType().getName());
        assertEquals(formatoMPMS1L2.getDetcore(), firstTransaction.getContract().getNumberType().getName());

        assertNotNull(firstTransaction.getTransactionType());

        assertNotNull(firstTransaction.getTransactionType().getId());
        assertEquals(PURCHASED, firstTransaction.getTransactionType().getId());

        assertNotNull(firstTransaction.getTransactionType().getName());
        assertEquals(formatoMPMS1L2.getNotipop(), firstTransaction.getTransactionType().getName());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getId());
        assertEquals(formatoMPMS1L2.getCodoper(), firstTransaction.getTransactionType().getInternalCode().getId());

        assertNotNull(firstTransaction.getTransactionType().getInternalCode().getName());
        assertEquals(formatoMPMS1L2.getNomcod(), firstTransaction.getTransactionType().getInternalCode().getName());
    }

    @Test
    public void testMapOutputListWithoutOperationDate() throws IOException {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMS1L2 formatoMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2().get(0);
        formatoMPMS1L2.setFechope(null);
        final TransactionsData transactionsData =
                txListTransactionsMapper.mapOutputList(formatoMPMS1L2, new TransactionsData(), dtoInputListCardTransactions);
        assertNotNull(transactionsData);
        assertNull(transactionsData.getData().get(0).getOperationDate());
    }

    @Test
    public void testMapOutputListWithoutOperationDatetime() throws IOException {
        final DTOInputListCardTransactions dtoInputListCardTransactions = entityMock.buildDTOInputListTransactions();
        final FormatoMPMS1L2 formatoMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2().get(0);
        formatoMPMS1L2.setHoraope(null);
        final TransactionsData transactionsData =
                txListTransactionsMapper.mapOutputList(formatoMPMS1L2, new TransactionsData(), dtoInputListCardTransactions);
        assertNotNull(transactionsData);
        assertEquals(formatoMPMS1L2.getFechope(), transactionsData.getData().get(0).getOperationDate().getTime());
    }

    @Test
    public void testMapOutputPagination() throws IOException {
        final FormatoMPMS2L2 formatoMPMS2L2 = formatsMpl2Mock.buildFormatoMPMS2L2();
        final TransactionsData transactionsData = txListTransactionsMapper.mapOutputPagination(formatoMPMS2L2, new TransactionsData());
        assertNotNull(transactionsData);
        assertNotNull(transactionsData.getPagination());
        assertNotNull(transactionsData.getPagination().getNextPage());
        assertEquals(formatoMPMS2L2.getIdpagin(), transactionsData.getPagination().getNextPage());
        assertNotNull(transactionsData.getPagination().getPageSize());
        assertEquals(formatoMPMS2L2.getTampagi().toString(), transactionsData.getPagination().getPageSize().toString());
    }

}
