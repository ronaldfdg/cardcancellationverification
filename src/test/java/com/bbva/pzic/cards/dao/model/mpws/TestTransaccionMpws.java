package com.bbva.pzic.cards.dao.model.mpws;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPWS</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpws {

    @InjectMocks
    private TransaccionMpws transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpws peticion = new PeticionTransaccionMpws();
        RespuestaTransaccionMpws respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpws.class, RespuestaTransaccionMpws.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpws result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }
}