package com.bbva.pzic.cards.dao.model.mpg2;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPG2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpg2 {

    @InjectMocks
    private TransaccionMpg2 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpg2 peticion = new PeticionTransaccionMpg2();
        RespuestaTransaccionMpg2 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpg2.class, RespuestaTransaccionMpg2.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpg2 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}