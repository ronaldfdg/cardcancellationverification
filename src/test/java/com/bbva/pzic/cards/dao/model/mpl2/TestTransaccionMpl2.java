package com.bbva.pzic.cards.dao.model.mpl2;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPL2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpl2 {

    @InjectMocks
    private TransaccionMpl2 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {
        PeticionTransaccionMpl2 peticion = new PeticionTransaccionMpl2();
        RespuestaTransaccionMpl2 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpl2.class, RespuestaTransaccionMpl2.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpl2 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}