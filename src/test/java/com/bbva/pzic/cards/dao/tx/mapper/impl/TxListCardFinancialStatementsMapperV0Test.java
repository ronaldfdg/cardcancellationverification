package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPME1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS2E2;
import com.bbva.pzic.cards.dao.model.mpe2.mock.FormatsMpe2Mock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardFinancialStatementsMapperV0;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
public class TxListCardFinancialStatementsMapperV0Test {

    private static final String TRUE = "1";
    private static final String FALSE = "0";
    private ITxListCardFinancialStatementsMapperV0 mapper;
    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void setUp() {
        mapper = new TxListCardFinancialStatementsMapperV0();
    }

    @Test
    public void mapInFullTest() {
        InputListCardFinancialStatements input = entityMock.getInputListCardFinancialStatements();
        FormatoMPME1E2 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getIdpagin());
        Assert.assertNotNull(result.getTampagi());
        Assert.assertNotNull(result.getIndacti());
        Assert.assertEquals(input.getCardId(), result.getNumtarj());
        Assert.assertEquals(input.getPaginationKey(), result.getIdpagin());
        Assert.assertEquals(input.getPageSize(), result.getTampagi());
        Assert.assertEquals(input.getIsActive() ? TRUE : FALSE, result.getIndacti());
    }

    @Test
    public void mapInWithoutCardIdTest() {
        InputListCardFinancialStatements input = entityMock.getInputListCardFinancialStatements();
        input.setCardId(null);
        FormatoMPME1E2 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getNumtarj());
        Assert.assertNotNull(result.getIdpagin());
        Assert.assertNotNull(result.getTampagi());
        Assert.assertNotNull(result.getIndacti());
        Assert.assertEquals(input.getPaginationKey(), result.getIdpagin());
        Assert.assertEquals(input.getPageSize(), result.getTampagi());
        Assert.assertEquals(input.getIsActive() ? TRUE : FALSE, result.getIndacti());
    }

    @Test
    public void mapInWithputPaginationKeyTest() {
        InputListCardFinancialStatements input = entityMock.getInputListCardFinancialStatements();
        input.setPaginationKey(null);
        FormatoMPME1E2 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNull(result.getIdpagin());
        Assert.assertNotNull(result.getTampagi());
        Assert.assertNotNull(result.getIndacti());
        Assert.assertEquals(input.getCardId(), result.getNumtarj());
        Assert.assertEquals(input.getPageSize(), result.getTampagi());
        Assert.assertEquals(input.getIsActive() ? TRUE : FALSE, result.getIndacti());
    }

    @Test
    public void mapInWithputPageSizeTest() {
        InputListCardFinancialStatements input = entityMock.getInputListCardFinancialStatements();
        input.setPageSize(null);
        FormatoMPME1E2 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getIdpagin());
        Assert.assertNull(result.getTampagi());
        Assert.assertNotNull(result.getIndacti());
        Assert.assertEquals(input.getCardId(), result.getNumtarj());
        Assert.assertEquals(input.getPaginationKey(), result.getIdpagin());
        Assert.assertEquals(input.getIsActive() ? TRUE : FALSE, result.getIndacti());
    }

    @Test
    public void mapInWithIsActiveFalseTest() {
        InputListCardFinancialStatements input = entityMock.getInputListCardFinancialStatements();
        input.setIsActive(Boolean.FALSE);
        FormatoMPME1E2 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getIdpagin());
        Assert.assertNotNull(result.getTampagi());
        Assert.assertNotNull(result.getIndacti());
        Assert.assertEquals(input.getCardId(), result.getNumtarj());
        Assert.assertEquals(input.getPaginationKey(), result.getIdpagin());
        Assert.assertEquals(input.getPageSize(), result.getTampagi());
        Assert.assertEquals(input.getIsActive() ? TRUE : FALSE, result.getIndacti());
    }

    @Test
    public void mapInWithoutIsActiveFalseTest() {
        InputListCardFinancialStatements input = entityMock.getInputListCardFinancialStatements();
        input.setIsActive(null);
        FormatoMPME1E2 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getIdpagin());
        Assert.assertNotNull(result.getTampagi());
        Assert.assertNull(result.getIndacti());
        Assert.assertEquals(input.getCardId(), result.getNumtarj());
        Assert.assertEquals(input.getPaginationKey(), result.getIdpagin());
        Assert.assertEquals(input.getPageSize(), result.getTampagi());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPME1E2 result = mapper.mapIn(new InputListCardFinancialStatements());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getNumtarj());
        Assert.assertNull(result.getIdpagin());
        Assert.assertNull(result.getTampagi());
        Assert.assertNull(result.getIndacti());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPMS1E2 format = FormatsMpe2Mock.getInstance().getFormatoMPMS1E2().get(0);
        DTOIntStatementList result = mapper.mapOut(format, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertEquals(1, result.getData().size());
        Assert.assertNotNull(result.getData().get(0));
        Assert.assertNotNull(result.getData().get(0).getId());
        Assert.assertNotNull(result.getData().get(0).getCutOffDate());
        Assert.assertNotNull(result.getData().get(0).getIsActive());
        Assert.assertEquals(format.getIddocta(), result.getData().get(0).getId());
        Assert.assertEquals(format.getFecorte(), result.getData().get(0).getCutOffDate());
        Assert.assertEquals(format.getIndacti(), result.getData().get(0).getIsActive() ? TRUE : FALSE);
    }

    @Test
    public void mapOutEmptyTest() {
        DTOIntStatementList result = new DTOIntStatementList();
        result = mapper.mapOut(new FormatoMPMS1E2(), result);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertEquals(1, result.getData().size());
        Assert.assertNotNull(result.getData().get(0));
        Assert.assertNull(result.getData().get(0).getId());
        Assert.assertNull(result.getData().get(0).getCutOffDate());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoMPMS2E2 format = FormatsMpe2Mock.getInstance()
                .getFormatoMPMS2E2("123");
        DTOIntStatementList result = mapper.mapOut2(format, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getPagination());
        Assert.assertNotNull(result.getPagination().getPaginationKey());
        Assert.assertNotNull(result.getPagination().getPageSize());
        Assert.assertEquals(format.getIdpagin(), result.getPagination()
                .getPaginationKey());
        Assert.assertEquals(format.getTampagi().toString(), result.getPagination()
                .getPageSize().toString());
    }

    @Test
    public void mapOut2EmptyTest() {
        DTOIntStatementList result = new DTOIntStatementList();
        result = mapper.mapOut2(new FormatoMPMS2E2(), result);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getPagination());
    }

    @Test
    public void mapOut2WitnFormatNullTest() {
        DTOIntStatementList result = new DTOIntStatementList();
        result = mapper.mapOut2(null, result);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getPagination());
    }
}