package com.bbva.pzic.cards.dao.model.mpb1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPB1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpb1 {

    @InjectMocks
    private TransaccionMpb1 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpb1 peticion = new PeticionTransaccionMpb1();
        RespuestaTransaccionMpb1 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpb1.class, RespuestaTransaccionMpb1.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpb1 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}