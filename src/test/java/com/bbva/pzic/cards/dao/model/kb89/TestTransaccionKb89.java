package com.bbva.pzic.cards.dao.model.kb89;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>KB89</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionKb89 {

    private static final Log LOG = LogFactory.getLog(TestTransaccionKb89.class);

    @InjectMocks
    private TransaccionKb89 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionKb89 peticion = new PeticionTransaccionKb89();

        RespuestaTransaccionKb89 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionKb89.class, RespuestaTransaccionKb89.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionKb89 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}