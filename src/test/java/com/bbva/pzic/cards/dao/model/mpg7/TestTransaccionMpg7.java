package com.bbva.pzic.cards.dao.model.mpg7;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPG7</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpg7 {

    @InjectMocks
    private TransaccionMpg7 transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);


    @Test
    public void test() throws ExcepcionTransaccion {

        PeticionTransaccionMpg7 peticion = new PeticionTransaccionMpg7();
        RespuestaTransaccionMpg7 respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpg7.class, RespuestaTransaccionMpg7.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionMpg7 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}