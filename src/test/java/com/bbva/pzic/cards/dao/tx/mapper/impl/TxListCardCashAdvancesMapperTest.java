package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntListCashAdvances;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMENDE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS1DE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS2DE;
import com.bbva.pzic.cards.dao.model.mpde.mock.FormatsMpdeMock;
import com.bbva.pzic.cards.facade.v0.dto.CashAdvances;
import com.bbva.pzic.cards.facade.v0.dto.Rate;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TxListCardCashAdvancesMapperTest {

    @InjectMocks
    private TxListCardCashAdvancesMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    private DummyMock dummyMock;

    @Before
    public void setUp() {
        dummyMock = new DummyMock();
    }

    @Test
    public void mapInFullTest() {
        DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria = EntityMock.getInstance().getDTOIntCashAdvancesSearchCriteria();
        FormatoMPMENDE result = mapper.mapInput(dtoIntCashAdvancesSearchCriteria);
        assertNotNull(result);
        assertNotNull(result.getNrotarj());
        assertEquals(dtoIntCashAdvancesSearchCriteria.getCardId(), result.getNrotarj());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPMENDE result = mapper.mapInput(new DTOIntCashAdvancesSearchCriteria());
        assertNotNull(result);
        assertNull(result.getNrotarj());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPMS1DE format = FormatsMpdeMock.getInstance().getFormatoMPMS1DE().get(0);

        Mockito.when(enumMapper.getEnumValue("cards.terms.frequency", format.getFreccuo())).thenReturn(FRECCUO_VAL);

        DTOIntListCashAdvances result = new DTOIntListCashAdvances();
        result = mapper.mapOutput(format, result);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(1, result.getData().size());

        assertNotNull(result.getData().get(0));
        assertNotNull(result.getData().get(0).getId());
        assertNotNull(result.getData().get(0).getDescription());
        assertNotNull(result.getData().get(0).getApplyDate());
        assertNotNull(result.getData().get(0).getAccountingDate());
        assertNotNull(result.getData().get(0).getMaturityDate());
        assertNotNull(result.getData().get(0).getRequestedAmount());
        assertNotNull(result.getData().get(0).getRequestedAmount().getAmount());
        assertNotNull(result.getData().get(0).getRequestedAmount().getCurrency());
        assertNotNull(result.getData().get(0).getInstallmentAmount());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getAmount());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getCurrency());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown().getPrincipal());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown().getPrincipal().getAmount());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown().getPrincipal().getCurrency());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown().getRates());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown().getRates().getAmount());
        assertNotNull(result.getData().get(0).getInstallmentAmount().getBreakDown().getRates().getCurrency());
        assertNotNull(result.getData().get(0).getCurrentInstallment());
        assertNotNull(result.getData().get(0).getCurrentInstallment().getPaymentDate());
        assertNotNull(result.getData().get(0).getTotal());
        assertNotNull(result.getData().get(0).getTotal().getAmount());
        assertNotNull(result.getData().get(0).getTotal().getCurrency());
        assertNotNull(result.getData().get(0).getPaidAmount());
        assertNotNull(result.getData().get(0).getPaidAmount().getTotal());
        assertNotNull(result.getData().get(0).getPaidAmount().getTotal().getAmount());
        assertNotNull(result.getData().get(0).getPaidAmount().getTotal().getCurrency());
        assertNotNull(result.getData().get(0).getPaidAmount().getPrincipal());
        assertNotNull(result.getData().get(0).getPaidAmount().getPrincipal().getAmount());
        assertNotNull(result.getData().get(0).getPaidAmount().getPrincipal().getCurrency());
        assertNotNull(result.getData().get(0).getPaidAmount().getRates());
        assertNotNull(result.getData().get(0).getPaidAmount().getRates().getAmount());
        assertNotNull(result.getData().get(0).getPaidAmount().getRates().getCurrency());
        assertNotNull(result.getData().get(0).getPendingAmount());
        assertNotNull(result.getData().get(0).getPendingAmount().getTotal());
        assertNotNull(result.getData().get(0).getPendingAmount().getTotal().getAmount());
        assertNotNull(result.getData().get(0).getPendingAmount().getTotal().getCurrency());
        assertNotNull(result.getData().get(0).getPendingAmount().getPrincipal());
        assertNotNull(result.getData().get(0).getPendingAmount().getPrincipal().getAmount());
        assertNotNull(result.getData().get(0).getPendingAmount().getPrincipal().getCurrency());
        assertNotNull(result.getData().get(0).getPendingAmount().getRates());
        assertNotNull(result.getData().get(0).getPendingAmount().getRates().getAmount());
        assertNotNull(result.getData().get(0).getPendingAmount().getRates().getCurrency());
        assertNotNull(result.getData().get(0).getTerms());
        assertNotNull(result.getData().get(0).getTerms().getNumber());
        assertNotNull(result.getData().get(0).getTerms().getPending());
        assertNotNull(result.getData().get(0).getTerms().getPaid());
        assertNotNull(result.getData().get(0).getTerms().getUnpaid());
        assertNotNull(result.getData().get(0).getTerms().getFrequency());
        assertNotNull(result.getData().get(0).getOfferId());

        assertEquals(format.getIdendep(), result.getData().get(0).getId());
        assertEquals(format.getDescdep(), result.getData().get(0).getDescription());
        assertEquals(dummyMock.buildDate(format.getFecinic(), format.getHorinic(), "yyyy-MM-ddHH:mm:ss"), result.getData().get(0).getApplyDate().getTime());
        assertEquals(dummyMock.buildDate(format.getFeccont(), format.getHorcont(), "yyyy-MM-ddHH:mm:ss"), result.getData().get(0).getAccountingDate().getTime());
        assertEquals(format.getFechfin(), result.getData().get(0).getMaturityDate());
        assertEquals(format.getMtosoli(), result.getData().get(0).getRequestedAmount().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getRequestedAmount().getCurrency());
        assertEquals(format.getDeudmes(), result.getData().get(0).getInstallmentAmount().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getInstallmentAmount().getCurrency());
        assertEquals(format.getCapcuom(), result.getData().get(0).getInstallmentAmount().getBreakDown().getPrincipal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getInstallmentAmount().getBreakDown().getPrincipal().getCurrency());
        assertEquals(format.getIntcuom(), result.getData().get(0).getInstallmentAmount().getBreakDown().getRates().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getInstallmentAmount().getBreakDown().getRates().getCurrency());
        assertEquals(format.getFecvenc(), result.getData().get(0).getCurrentInstallment().getPaymentDate());
        assertEquals(format.getTotdeud(), result.getData().get(0).getTotal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getTotal().getCurrency());
        assertEquals(format.getMontotf(), result.getData().get(0).getPaidAmount().getTotal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getPaidAmount().getTotal().getCurrency());
        assertEquals(format.getCapabon(), result.getData().get(0).getPaidAmount().getPrincipal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getPaidAmount().getPrincipal().getCurrency());
        assertEquals(format.getIntabon(), result.getData().get(0).getPaidAmount().getRates().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getPaidAmount().getRates().getCurrency());
        assertEquals(format.getTotcanc(), result.getData().get(0).getPendingAmount().getTotal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getPendingAmount().getTotal().getCurrency());
        assertEquals(format.getTcuopen(), result.getData().get(0).getPendingAmount().getPrincipal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getPendingAmount().getPrincipal().getCurrency());
        assertEquals(format.getInteres(), result.getData().get(0).getPendingAmount().getRates().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(0).getPendingAmount().getRates().getCurrency());
        assertEquals(format.getNumcuot(), result.getData().get(0).getTerms().getNumber());
        assertEquals(format.getCuopdte(), result.getData().get(0).getTerms().getPending());
        assertEquals(format.getCuopaga(), result.getData().get(0).getTerms().getPaid());
        assertEquals(format.getCuovnpa(), result.getData().get(0).getTerms().getUnpaid());
        assertEquals(FRECCUO_VAL, result.getData().get(0).getTerms().getFrequency());
        assertEquals(format.getIdeofea(), result.getData().get(0).getOfferId());

        format = FormatsMpdeMock.getInstance().getFormatoMPMS1DE().get(1);

        Mockito.when(enumMapper.getEnumValue("cards.terms.frequency", format.getFreccuo())).thenReturn(FRECCUO_VAL);

        result = mapper.mapOutput(format, result);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(2, result.getData().size());

        assertNotNull(result.getData().get(1));
        assertNotNull(result.getData().get(1).getId());
        assertNotNull(result.getData().get(1).getDescription());
        assertNotNull(result.getData().get(1).getApplyDate());
        assertNotNull(result.getData().get(1).getAccountingDate());
        assertNotNull(result.getData().get(1).getMaturityDate());
        assertNotNull(result.getData().get(1).getRequestedAmount());
        assertNotNull(result.getData().get(1).getRequestedAmount().getAmount());
        assertNotNull(result.getData().get(1).getRequestedAmount().getCurrency());
        assertNotNull(result.getData().get(1).getInstallmentAmount());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getAmount());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getCurrency());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown().getPrincipal());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown().getPrincipal().getAmount());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown().getPrincipal().getCurrency());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown().getRates());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown().getRates().getAmount());
        assertNotNull(result.getData().get(1).getInstallmentAmount().getBreakDown().getRates().getCurrency());
        assertNotNull(result.getData().get(1).getCurrentInstallment());
        assertNotNull(result.getData().get(1).getCurrentInstallment().getPaymentDate());
        assertNotNull(result.getData().get(1).getTotal());
        assertNotNull(result.getData().get(1).getTotal().getAmount());
        assertNotNull(result.getData().get(1).getTotal().getCurrency());
        assertNotNull(result.getData().get(1).getPaidAmount());
        assertNotNull(result.getData().get(1).getPaidAmount().getTotal());
        assertNotNull(result.getData().get(1).getPaidAmount().getTotal().getAmount());
        assertNotNull(result.getData().get(1).getPaidAmount().getTotal().getCurrency());
        assertNotNull(result.getData().get(1).getPaidAmount().getPrincipal());
        assertNotNull(result.getData().get(1).getPaidAmount().getPrincipal().getAmount());
        assertNotNull(result.getData().get(1).getPaidAmount().getPrincipal().getCurrency());
        assertNotNull(result.getData().get(1).getPaidAmount().getRates());
        assertNotNull(result.getData().get(1).getPaidAmount().getRates().getAmount());
        assertNotNull(result.getData().get(1).getPaidAmount().getRates().getCurrency());
        assertNotNull(result.getData().get(1).getPendingAmount());
        assertNotNull(result.getData().get(1).getPendingAmount().getTotal());
        assertNotNull(result.getData().get(1).getPendingAmount().getTotal().getAmount());
        assertNotNull(result.getData().get(1).getPendingAmount().getTotal().getCurrency());
        assertNotNull(result.getData().get(1).getPendingAmount().getPrincipal());
        assertNotNull(result.getData().get(1).getPendingAmount().getPrincipal().getAmount());
        assertNotNull(result.getData().get(1).getPendingAmount().getPrincipal().getCurrency());
        assertNotNull(result.getData().get(1).getPendingAmount().getRates());
        assertNotNull(result.getData().get(1).getPendingAmount().getRates().getAmount());
        assertNotNull(result.getData().get(1).getPendingAmount().getRates().getCurrency());
        assertNotNull(result.getData().get(1).getTerms());
        assertNotNull(result.getData().get(1).getTerms().getNumber());
        assertNotNull(result.getData().get(1).getTerms().getPending());
        assertNotNull(result.getData().get(1).getTerms().getPaid());
        assertNotNull(result.getData().get(1).getTerms().getUnpaid());
        assertNotNull(result.getData().get(1).getTerms().getFrequency());
        assertNotNull(result.getData().get(1).getOfferId());

        assertEquals(format.getIdendep(), result.getData().get(1).getId());
        assertEquals(format.getDescdep(), result.getData().get(1).getDescription());
        assertEquals(dummyMock.buildDate(format.getFecinic(), format.getHorinic(), "yyyy-MM-ddHH:mm:ss"), result.getData().get(1).getApplyDate().getTime());
        assertEquals(dummyMock.buildDate(format.getFeccont(), format.getHorcont(), "yyyy-MM-ddHH:mm:ss"), result.getData().get(1).getAccountingDate().getTime());
        assertEquals(format.getFechfin(), result.getData().get(1).getMaturityDate());
        assertEquals(format.getMtosoli(), result.getData().get(1).getRequestedAmount().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getRequestedAmount().getCurrency());
        assertEquals(format.getDeudmes(), result.getData().get(1).getInstallmentAmount().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getInstallmentAmount().getCurrency());
        assertEquals(format.getCapcuom(), result.getData().get(1).getInstallmentAmount().getBreakDown().getPrincipal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getInstallmentAmount().getBreakDown().getPrincipal().getCurrency());
        assertEquals(format.getIntcuom(), result.getData().get(1).getInstallmentAmount().getBreakDown().getRates().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getInstallmentAmount().getBreakDown().getRates().getCurrency());
        assertEquals(format.getFecvenc(), result.getData().get(1).getCurrentInstallment().getPaymentDate());
        assertEquals(format.getTotdeud(), result.getData().get(1).getTotal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getTotal().getCurrency());
        assertEquals(format.getMontotf(), result.getData().get(1).getPaidAmount().getTotal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getPaidAmount().getTotal().getCurrency());
        assertEquals(format.getCapabon(), result.getData().get(1).getPaidAmount().getPrincipal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getPaidAmount().getPrincipal().getCurrency());
        assertEquals(format.getIntabon(), result.getData().get(1).getPaidAmount().getRates().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getPaidAmount().getRates().getCurrency());
        assertEquals(format.getTotcanc(), result.getData().get(1).getPendingAmount().getTotal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getPendingAmount().getTotal().getCurrency());
        assertEquals(format.getTcuopen(), result.getData().get(1).getPendingAmount().getPrincipal().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getPendingAmount().getPrincipal().getCurrency());
        assertEquals(format.getInteres(), result.getData().get(1).getPendingAmount().getRates().getAmount());
        assertEquals(format.getMonedep(), result.getData().get(1).getPendingAmount().getRates().getCurrency());
        assertEquals(format.getNumcuot(), result.getData().get(1).getTerms().getNumber());
        assertEquals(format.getCuopdte(), result.getData().get(1).getTerms().getPending());
        assertEquals(format.getCuopaga(), result.getData().get(1).getTerms().getPaid());
        assertEquals(format.getCuovnpa(), result.getData().get(1).getTerms().getUnpaid());
        assertEquals(FRECCUO_VAL, result.getData().get(1).getTerms().getFrequency());
        assertEquals(format.getIdeofea(), result.getData().get(1).getOfferId());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        DTOIntListCashAdvances result = new DTOIntListCashAdvances();
        FormatoMPMS1DE format = FormatsMpdeMock.getInstance().getFormatoMPMS1DEEmpty();

        result = mapper.mapOutput(format, result);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(1, result.getData().size());
        assertNotNull(result.getData().get(0));
        assertNull(result.getData().get(0).getId());
        assertNull(result.getData().get(0).getDescription());
        assertNull(result.getData().get(0).getApplyDate());
        assertNull(result.getData().get(0).getAccountingDate());
        assertNull(result.getData().get(0).getMaturityDate());
        assertNull(result.getData().get(0).getRequestedAmount());
        assertNull(result.getData().get(0).getInstallmentAmount());
        assertNull(result.getData().get(0).getCurrentInstallment());
        assertNull(result.getData().get(0).getTotal());
        assertNull(result.getData().get(0).getPaidAmount());
        assertNull(result.getData().get(0).getPendingAmount());
        assertNull(result.getData().get(0).getTerms());
        assertNull(result.getData().get(0).getOfferId());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        Rate resultRate;
        FormatoMPMS2DE formato;


        DTOIntListCashAdvances result = new DTOIntListCashAdvances();

        result.setData(new ArrayList<CashAdvances>());
        result.getData().add(new CashAdvances());
        result.getData().get(0).setRates(new ArrayList<Rate>());

        List<FormatoMPMS2DE> formatos = FormatsMpdeMock.getInstance().getFormatoMPMS2DE();

        //data.rates[0]
        formato = formatos.get(0);
        Mockito.when((enumMapper.getEnumValue("cards.rates.mode", formato.getIdmodta()))).thenReturn(PERCENTAGE_VAL);
        result = mapper.mapOutput2(formato, result);

        assertEquals(1, result.getData().get(0).getRates().size());

        resultRate = result.getData().get(0).getRates().get(0);
        assertNotNull(resultRate.getRateType());
        assertNotNull(resultRate.getRateType().getId());
        assertNotNull(resultRate.getRateType().getName());
        assertNotNull(resultRate.getCalculationDate());
        assertNotNull(resultRate.getMode());
        assertNotNull(resultRate.getMode().getId());
        assertNotNull(resultRate.getMode().getName());
        assertNotNull(resultRate.getUnit());

        assertEquals(formato.getIdettas(), resultRate.getRateType().getId());
        assertEquals(formato.getDesttas(), resultRate.getRateType().getName());
        assertEquals(formato.getFecctas(), resultRate.getCalculationDate());
        assertEquals(PERCENTAGE_VAL, resultRate.getMode().getId());
        assertEquals(formato.getNommod(), resultRate.getMode().getName());

        assertEquals(formato.getValtasa(), resultRate.getUnit().getPercentage());
        assertNull(resultRate.getUnit().getCurrency());
        assertNull(resultRate.getUnit().getAmount());

        //data.rates[1]
        formato = formatos.get(1);
        Mockito.when((enumMapper.getEnumValue("cards.rates.mode", formato.getIdmodta()))).thenReturn(AMOUNT_VAL);
        result = mapper.mapOutput2(formato, result);

        assertEquals(2, result.getData().get(0).getRates().size());

        resultRate = result.getData().get(0).getRates().get(1);
        assertNotNull(resultRate.getRateType());
        assertNotNull(resultRate.getRateType().getId());
        assertNotNull(resultRate.getRateType().getName());
        assertNotNull(resultRate.getCalculationDate());
        assertNotNull(resultRate.getMode());
        assertNotNull(resultRate.getMode().getId());
        assertNotNull(resultRate.getMode().getName());
        assertNotNull(resultRate.getUnit());

        assertEquals(formato.getIdettas(), resultRate.getRateType().getId());
        assertEquals(formato.getDesttas(), resultRate.getRateType().getName());
        assertEquals(formato.getFecctas(), resultRate.getCalculationDate());
        assertEquals(AMOUNT_VAL, resultRate.getMode().getId());
        assertEquals(formato.getNommod(), resultRate.getMode().getName());

        assertEquals(formato.getIntdeut(), resultRate.getUnit().getAmount());
        assertEquals(formato.getMontasa(), resultRate.getUnit().getCurrency());
        assertNull(resultRate.getUnit().getPercentage());

    }

}



