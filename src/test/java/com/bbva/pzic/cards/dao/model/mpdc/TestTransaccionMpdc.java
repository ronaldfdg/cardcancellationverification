package com.bbva.pzic.cards.dao.model.mpdc;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPDC</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpdc {

    @InjectMocks
    private TransaccionMpdc transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {

        PeticionTransaccionMpdc peticion = new PeticionTransaccionMpdc();
        RespuestaTransaccionMpdc respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpdc.class, RespuestaTransaccionMpdc.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpdc result = transaccion.invocar(peticion);
        Assert.assertEquals(result, respuesta);
    }
}