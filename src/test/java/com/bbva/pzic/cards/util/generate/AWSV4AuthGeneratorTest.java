package com.bbva.pzic.cards.util.generate;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Created on 24/11/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class AWSV4AuthGeneratorTest {

    private static final String HTTP_METHOD_NAME = "GET";
    private static final String URL = "https://508yzjw52b.execute-api.us-east-1.amazonaws.com/prod/pois/v0/bank-pois";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @InjectMocks
    private AWSV4AuthGenerator awsv4AuthGenerator;
    @Mock
    private ConfigurationManager configurationManager;

    private TreeMap<String, List<String>> queryParameters;
    private TreeMap<String, String> headers;

    @Before
    public void setUp() {
        queryParameters = new TreeMap<>();
        queryParameters.put("latitude", Collections.singletonList("-12.0920807"));
        queryParameters.put("longitude", Collections.singletonList("-77.0144793"));

        headers = new TreeMap<>();
        headers.put("country", "PERU");

        when(configurationManager.getProperty("servicing.SNPE1710030.aws.authorization." + AWSV4AuthGenerator.ACCESS_KEY)).thenReturn("AKIAJXGTI5SN5HD4ZGYA");
        when(configurationManager.getProperty("servicing.SNPE1710030.aws.authorization." + AWSV4AuthGenerator.SECRET_KEY)).thenReturn("pOR/lkfRafLzlqYFpCkSaw/J63ZL36xwJcjBLASL");
        when(configurationManager.getProperty("servicing.SNPE1710030.aws.authorization." + AWSV4AuthGenerator.REGION)).thenReturn("us-east-1");
        when(configurationManager.getProperty("servicing.SNPE1710030.aws.authorization." + AWSV4AuthGenerator.SERVICE_NAME)).thenReturn("execute-api");
    }

    @Test
    public void createAuthorizationFullWithRegistryGetPoisAtmTest() {
        queryParameters.put("limitations", Arrays.asList("PC", "KI"));
        queryParameters.put("versions", Arrays.asList(null, "2010-05-08"));

        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(HTTP_METHOD_NAME, URL, queryParameters, this.headers);
        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }

    @Test
    public void createAuthorizationFullWithRegistryGetPoisBranchTest() {
        queryParameters.put("Action", Collections.singletonList(null));

        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(HTTP_METHOD_NAME, URL, queryParameters, this.headers);
        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }

    @Test
    public void createAuthorizationFullWithRegistryListPoisBanksTest() {
        queryParameters.put("antique", null);

        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(HTTP_METHOD_NAME, URL, queryParameters, this.headers);
        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }

    @Test
    public void createAuthorizationWithoutQueryParametersTest() {
        headers.put("My-header1", "    a   b   c  ");
        headers.put("zip", "RAR");
        headers.put("content-type", "application/x-www-form-urlencoded");
        headers.put("My-Header2", "\"a   b   c\"");

        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(HTTP_METHOD_NAME, URL, null, this.headers, null);
        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }

    @Test
    public void createAuthorizationWithoutAwsHeadersTest() {
        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(HTTP_METHOD_NAME, URL, queryParameters);
        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }

    @Test
    public void createAuthorizationWithoutQueryParametesAndAwsHeadersTest() {
        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(HTTP_METHOD_NAME, URL);
        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }

    @Test
    public void createAuthorizationWithPayloadTest() throws IOException {
        Map<String, String> headers = awsv4AuthGenerator.createAuthorization(
                "POST", URL, null, null,
                ObjectMapperHelper.getInstance().writeValueAsString(EntityMock.getInstance().getCard()));

        assertNotNull(headers);
        assertNotNull(headers.get("x-amz-date"));
        assertNotNull(headers.get("Authorization"));
        assertEquals(2, headers.size());
    }
}
