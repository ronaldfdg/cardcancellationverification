package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.PUBLIC_KEY;
import static org.junit.Assert.*;

public class GetCardSecurityDataV1MapperTest {

    private GetCardSecurityDataV1Mapper mapper = new GetCardSecurityDataV1Mapper();

    @Test
    public void mapInFullTest() {
        InputGetCardSecurityData result = mapper.mapIn(CARD_ID, PUBLIC_KEY);
        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getPublicKey());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(PUBLIC_KEY, result.getPublicKey());
    }

    @Test
    public void mapOutFullTest() {
        List<SecurityData> mockList = new ArrayList<>();
        mockList.add(new SecurityData());
        ServiceResponse<List<SecurityData>> result = mapper.mapOut(mockList);
        assertNotNull(result);
        assertEquals(mockList.size(), result.getData().size());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<SecurityData>> result = mapper.mapOut(new ArrayList<>());
        assertNull(result);
    }


    @Test
    public void mapOutNullTest() {
        ServiceResponse<List<SecurityData>> result = mapper.mapOut(null);
        assertNull(result);
    }
}
