package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanData;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.utilTest.UriInfoImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static com.bbva.pzic.cards.EntityMock.CARD_ID_ENCRYPTED;
import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN;

/**
 * Created on 15/03/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardInstallmentsPlanMapperTest {

    @InjectMocks
    private CreateCardInstallmentsPlanV0Mapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    private void mapInDecrypt() {
        Mockito.when(cypherTool.decrypt("##4232$%6778", AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN))
                .thenReturn("12345678");
    }

    @Test
    public void mapInTestFull() {
        mapInDecrypt();
        InstallmentsPlan installmentsPlan = mock.getInputInstallmentsPlan();
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlan);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getTransactionId());
        Assert.assertNotNull(result.getTermsNumber());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlan.getTransactionId(), result.getTransactionId());
        Assert.assertEquals(installmentsPlan.getTerms().getNumber(), result.getTermsNumber());
    }

    @Test
    public void mapInTestWithoutTransactionId() {
        mapInDecrypt();
        InstallmentsPlan installmentsPlan = mock.getInputInstallmentsPlan();
        installmentsPlan.setTransactionId(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlan);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNull(result.getTransactionId());
        Assert.assertNotNull(result.getTermsNumber());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlan.getTerms().getNumber(), result.getTermsNumber());
    }

    @Test
    public void mapInTestWithoutTermsNumber() {
        mapInDecrypt();
        InstallmentsPlan installmentsPlan = mock.getInputInstallmentsPlan();
        installmentsPlan.getTerms().setNumber(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlan);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getTransactionId());
        Assert.assertNull(result.getTermsNumber());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlan.getTransactionId(), result.getTransactionId());
    }

    @Test
    public void mapOutFullTest() {
        Response result = mapper.mapOut(new InstallmentsPlanData(), new UriInfoImpl());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getEntity());
    }

    @Test
    public void mapOutEmptyTest() {
        Response result = mapper.mapOut(null, new UriInfoImpl());
        Assert.assertNull(result);
    }
}
