package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.canonic.Pin;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_CARDTYPE_ID_KEY_TESTED;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ModifyCardPinMapperTest {

    @InjectMocks
    private ModifyCardPinMapper modifyCardPinMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_PIN)).thenReturn(CARD_CARDTYPE_ID_KEY_TESTED);
    }

    @Test
    public void mapInTest() {
        final Pin pin = new Pin();
        pin.setPin("123456789012345678901234567890");
        final DTOIntPin dtoIntPin = modifyCardPinMapper.mapIn(CARD_ID, pin);

        assertNotNull(dtoIntPin);
        assertNotNull(dtoIntPin.getCardId());
        assertNotNull(dtoIntPin.getPin());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntPin.getCardId());
        assertEquals(pin.getPin(), dtoIntPin.getPin());
    }

    @Test
    public void mapInWithoutPinTest() {
        final DTOIntPin dtoIntPin = modifyCardPinMapper.mapIn(CARD_ID, new Pin());

        assertNotNull(dtoIntPin);
        assertNotNull(dtoIntPin.getCardId());
        assertNull(dtoIntPin.getPin());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntPin.getCardId());
    }
}
