package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCashRefund;
import com.bbva.pzic.cards.business.dto.DTOIntCashRefund;
import com.bbva.pzic.cards.facade.v0.dto.CashRefund;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardCashRefundMapperTest {

    @InjectMocks
    private CreateCardCashRefundMapper createCardCashRefundMapper;

    @Mock
    private CypherTool cypherTool;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.CARDID)).thenReturn(CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.refundType", CCC_REFUNDTYPE)).thenReturn(CCC_TIPREEM);
    }

    @Test
    public void testMapInput() {
        CashRefund cashRefund = getCashRefund();
        DTOInputCreateCashRefund dtoInputCreateCashRefund = createCardCashRefundMapper.mapInput(CARD_ENCRYPT_ID, cashRefund);
        assertNotNull(dtoInputCreateCashRefund);
        assertNotNull(dtoInputCreateCashRefund.getCardId());
        assertEquals(CARD_ID, dtoInputCreateCashRefund.getCardId());
        assertNotNull(dtoInputCreateCashRefund.getCashRefund());
        assertNotNull(dtoInputCreateCashRefund.getCashRefund().getRefundType());
        assertEquals(CCC_TIPREEM, dtoInputCreateCashRefund.getCashRefund().getRefundType());
        assertNotNull(dtoInputCreateCashRefund.getCashRefund().getRefundAmount());
        assertNotNull(dtoInputCreateCashRefund.getCashRefund().getRefundAmount().getCurrency());
        assertEquals(cashRefund.getRefundAmount().getCurrency(), dtoInputCreateCashRefund.getCashRefund().getRefundAmount().getCurrency());
        assertNotNull(dtoInputCreateCashRefund.getCashRefund().getReceivingAccount());
        assertNotNull(dtoInputCreateCashRefund.getCashRefund().getReceivingAccount().getId());
        assertEquals(cashRefund.getReceivingAccount().getId(), dtoInputCreateCashRefund.getCashRefund().getReceivingAccount().getId());
    }

    @Test
    public void testMapOutput() {
        DTOIntCashRefund dtoIntCashRefund = getDTOIntCashRefund();
        ServiceResponse<CashRefund> serviceResponse = createCardCashRefundMapper.mapOutput(dtoIntCashRefund);
        CashRefund cashRefund = serviceResponse.getData();
        assertNotNull(cashRefund);
        assertNotNull(cashRefund.getId());
        assertEquals(dtoIntCashRefund.getId(), cashRefund.getId());
        assertNotNull(cashRefund.getDescription());
        assertEquals(dtoIntCashRefund.getDescription(), cashRefund.getDescription());
        assertNotNull(cashRefund.getRefundType());
        assertEquals(dtoIntCashRefund.getRefundType(), cashRefund.getRefundType());
        assertNotNull(cashRefund.getRefundAmount());
        assertNotNull(cashRefund.getRefundAmount().getAmount());
        assertEquals(dtoIntCashRefund.getRefundAmount().getAmount(), cashRefund.getRefundAmount().getAmount());
        assertNotNull(cashRefund.getRefundAmount().getCurrency());
        assertEquals(dtoIntCashRefund.getRefundAmount().getCurrency(), cashRefund.getRefundAmount().getCurrency());
        assertNotNull(cashRefund.getReceivingAccount());
        assertNotNull(cashRefund.getReceivingAccount().getId());
        assertEquals(dtoIntCashRefund.getReceivingAccount().getId(), cashRefund.getReceivingAccount().getId());
        assertNotNull(cashRefund.getOperationDate());
        assertEquals(dtoIntCashRefund.getOperationDate(), cashRefund.getOperationDate());
        assertNotNull(cashRefund.getAccountingDate());
        assertEquals(dtoIntCashRefund.getAccoutingDate(), cashRefund.getAccountingDate());
        assertNotNull(cashRefund.getReceivedAmount());
        assertNotNull(cashRefund.getReceivedAmount().getAmount());
        assertEquals(dtoIntCashRefund.getReceivedAmount().getAmount(), cashRefund.getReceivedAmount().getAmount());
        assertNotNull(cashRefund.getReceivedAmount().getCurrency());
        assertEquals(dtoIntCashRefund.getReceivedAmount().getCurrency(), cashRefund.getReceivedAmount().getCurrency());
        assertNotNull(cashRefund.getExchangeRate());
        assertNotNull(cashRefund.getExchangeRate().getDate());
        assertEquals(dtoIntCashRefund.getExchangeRate().getDate(), cashRefund.getExchangeRate().getDate());
        assertNotNull(cashRefund.getExchangeRate().getValues());
        assertNotNull(cashRefund.getExchangeRate().getValues().getFactor());
        assertNotNull(cashRefund.getExchangeRate().getValues().getFactor().getValue());
        assertEquals(dtoIntCashRefund.getExchangeRate().getValues().getFactor().getValue(), cashRefund.getExchangeRate().getValues().getFactor().getValue());
        assertNotNull(cashRefund.getExchangeRate().getValues().getPriceType());
        assertEquals(dtoIntCashRefund.getExchangeRate().getValues().getPriceType(), cashRefund.getExchangeRate().getValues().getPriceType());
        assertNotNull(cashRefund.getTaxes());
        assertNotNull(cashRefund.getTaxes().getTotalTaxes());
        assertNotNull(cashRefund.getTaxes().getTotalTaxes().getAmount());
        assertEquals(dtoIntCashRefund.getTaxes().getTotalTaxes().getAmount(), cashRefund.getTaxes().getTotalTaxes().getAmount());
        assertNotNull(cashRefund.getTaxes().getTotalTaxes().getCurrency());
        assertEquals(dtoIntCashRefund.getTaxes().getTotalTaxes().getCurrency(), cashRefund.getTaxes().getTotalTaxes().getCurrency());

        assertNotNull(cashRefund.getTaxes().getItemizeTaxes());
        assertFalse(cashRefund.getTaxes().getItemizeTaxes().isEmpty());
        assertNotNull(cashRefund.getTaxes().getItemizeTaxes().get(0));
        assertNotNull(cashRefund.getTaxes().getItemizeTaxes().get(0).getTaxType());
        assertEquals(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getTaxType(), cashRefund.getTaxes().getItemizeTaxes().get(0).getTaxType());
        assertNotNull(cashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount());
        assertNotNull(cashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getAmount());
        assertEquals(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getAmount(), cashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getAmount());
        assertNotNull(cashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getCurrency());
        assertEquals(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getCurrency(), cashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getCurrency());
    }

}
