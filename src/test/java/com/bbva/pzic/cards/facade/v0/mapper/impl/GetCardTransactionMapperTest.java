package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;

/**
 * Created on 26/12/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCardTransactionMapperTest {

    @InjectMocks
    private GetCardTransactionMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void init() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_TRANSACTION)).thenReturn(CARD_ID);
    }

    @Test
    public void mapInFullTest() {
        InputGetCardTransaction input = mapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.TRANSACTION_ID);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getTransactionId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getTransactionId(), EntityMock.TRANSACTION_ID);
    }

    @Test
    public void mapInWithoutCardIdTest() {
        InputGetCardTransaction input = mapper.mapIn(null, EntityMock.TRANSACTION_ID);
        Assert.assertNotNull(input);
        Assert.assertNull(input.getCardId());
        Assert.assertNotNull(input.getTransactionId());

        Assert.assertEquals(input.getTransactionId(), EntityMock.TRANSACTION_ID);
    }

    @Test
    public void mapInWithoutTransactionIdTest() {
        InputGetCardTransaction input = mapper.mapIn(EntityMock.CARD_ENCRYPT_ID, null);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNull(input.getTransactionId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
    }
}
