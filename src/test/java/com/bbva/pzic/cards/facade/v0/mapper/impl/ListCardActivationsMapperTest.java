package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.CARD_ACTIVATIONS_ID_KEY_TESTED;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListCardActivationsMapperTest {

    @InjectMocks
    private ListCardActivationsMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private final EntityMock mock = EntityMock.getInstance();

    @Before
    public void init() {
        when(cypherTool.decrypt(CARD_ID, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_ACTIVATIONS))
                .thenReturn(CARD_ACTIVATIONS_ID_KEY_TESTED);
    }

    @Test
    public void mapInFull() {
        final DTOIntCard dtoIntCard = mapper.mapIn(CARD_ID);

        assertNotNull(dtoIntCard);
        assertNotNull(dtoIntCard.getCardId());

        assertEquals(CARD_ACTIVATIONS_ID_KEY_TESTED, dtoIntCard.getCardId());
    }

    @Test
    public void mapInWithOutCardId() {
        final DTOIntCard result = mapper.mapIn(null);

        assertNotNull(result);
        assertNull(result.getCardId());
    }

    @Test
    public void mapOutFull() throws IOException {
        final List<Activation> activations = mock.buildListActivation();
        final ServiceResponse<List<Activation>> result = mapper.mapOut(activations);

        assertNotNull(result);
        assertNotNull(result.getData());

        assertEquals(7, result.getData().size());
        assertEquals(activations.get(0).getName(), result.getData().get(0).getName());
        assertEquals(activations.get(0).getActivationId(), result.getData().get(0).getActivationId());
        assertEquals(activations.get(1).getName(), result.getData().get(1).getName());
        assertEquals(activations.get(1).getActivationId(), result.getData().get(1).getActivationId());
    }

    @Test
    public void mapOutWithOut() {
        final ServiceResponse<List<Activation>> result = mapper.mapOut(Collections.emptyList());
        assertNull(result);
    }

}
