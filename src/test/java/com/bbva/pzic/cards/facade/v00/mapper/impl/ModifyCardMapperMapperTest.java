package com.bbva.pzic.cards.facade.v00.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ModifyCardMapperMapperTest {

    @InjectMocks
    private ModifyCardMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Mock
    private EnumMapper enumMapper;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_V00)).thenReturn("1223413412412331");
        Mockito.when(enumMapper.getBackendValue("cards.status.id", "OPERATIVE")).thenReturn("O");
    }

    @Test
    public void mapInTestFull() {
        Card card = mock.getCard();
        DTOIntCard result = mapper.mapIn(CARD_ENCRYPT_ID, card);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getStatusId());

        assertEquals("1223413412412331", result.getCardId());
        assertEquals("O", result.getStatusId());
    }

    @Test
    public void mapInTestWithoutStatusId() {
        Card card = mock.getCard();
        card.getStatus().setId(null);
        DTOIntCard result = mapper.mapIn(CARD_ENCRYPT_ID, card);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNull(result.getStatusId());

        assertEquals("1223413412412331", result.getCardId());
    }
}