package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.canonic.StatementList;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListCardFinancialStatementsMapperTest {

    private EntityMock entityMock = EntityMock.getInstance();
    @InjectMocks
    private ListCardFinancialStatementsMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_FINANCIAL_STATEMENTS)).thenReturn(CARD_ID);
    }

    @Test
    public void mapInFullTest() {
        InputListCardFinancialStatements result = mapper.mapIn(
                CARD_ENCRYPT_ID, EntityMock.IS_ACTIVE, EntityMock.PAGINATION_KEY,
                EntityMock.PAGINATION_SIZE);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getPaginationKey());
        Assert.assertNotNull(result.getPageSize());
        Assert.assertNotNull(result.getIsActive());
        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(EntityMock.PAGINATION_KEY,
                result.getPaginationKey());
        Assert.assertEquals(EntityMock.PAGINATION_SIZE, result.getPageSize());
        Assert.assertEquals(EntityMock.IS_ACTIVE, result.getIsActive());
    }

    @Test
    public void mapInEmptyTest() {
        InputListCardFinancialStatements result = mapper.mapIn(
                CARD_ENCRYPT_ID, null, null, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNull(result.getPaginationKey());
        Assert.assertNull(result.getPageSize());
        Assert.assertNull(result.getIsActive());
    }

    @Test
    public void mapOutFullTest() {
        StatementList result = mapper.mapOut(entityMock.getDtoOutStatementList());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        StatementList result = mapper.mapOut(null);
        Assert.assertNull(result);
    }

    @Test
    public void mapOutDataNullTest() {
        StatementList result = mapper.mapOut(new DTOIntStatementList());
        Assert.assertNull(result);
    }
}
