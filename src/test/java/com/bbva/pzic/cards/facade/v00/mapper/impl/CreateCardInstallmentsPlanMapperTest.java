package com.bbva.pzic.cards.facade.v00.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ID_ENCRYPTED;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardInstallmentsPlanMapperTest {

    @InjectMocks
    private CreateCardInstallmentsPlanMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    private void mapInDecrypt() {
        Mockito.when(cypherTool.decrypt("##4232$%6778", AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN_V00)).thenReturn("12345678");
    }

    @Test
    public void mapInTestFull() {
        mapInDecrypt();
        InstallmentsPlan installmentsPlan = mock.getInputInstallmentsPlan();
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlan);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getTransactionId());
        Assert.assertNotNull(result.getTermsNumber());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlan.getTransactionId(), result.getTransactionId());
        Assert.assertEquals(installmentsPlan.getTerms().getNumber(), result.getTermsNumber());
    }

    @Test
    public void mapInTestWithoutTransactionId() {
        mapInDecrypt();
        InstallmentsPlan installmentsPlan = mock.getInputInstallmentsPlan();
        installmentsPlan.setTransactionId(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlan);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNull(result.getTransactionId());
        Assert.assertNotNull(result.getTermsNumber());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlan.getTerms().getNumber(), result.getTermsNumber());
    }

    @Test
    public void mapInTestWithoutTermsNumber() {
        mapInDecrypt();
        InstallmentsPlan installmentsPlan = mock.getInputInstallmentsPlan();
        installmentsPlan.getTerms().setNumber(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlan);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getTransactionId());
        Assert.assertNull(result.getTermsNumber());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlan.getTransactionId(), result.getTransactionId());
    }
}