package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardReports;
import com.bbva.pzic.cards.facade.v0.dto.ReportCardCreation;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardReportsV0MapperTest {

    @InjectMocks
    private CreateCardReportsV0Mapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Mock
    private ConfigurationManager configurationManager;

    @Mock
    private Translator translator;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void init() {
        when(translator.translateFrontendEnumValueStrictly(PROPERTY_DOCUMENT_TYPE_ID, DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM)).thenReturn(DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly(PROPERTY_PROPOSAL_STATUS, PROPOSAL_STATUS_CREATED)).thenReturn(PROPOSAL_STATUS_FRONTEND_VALUE_3);
    }

    @Test
    public void mapInFullTest() throws IOException {
        ReportCardCreation input = mock.getReportCardCreationMock();
        when(configurationManager.getProperty("servicing.aap.configuration." + input.getChannel() + ".constant.user")).thenReturn(CONFIGURATION_MANAGER_USER);
        when(configurationManager.getProperty("servicing.aap.configuration." + input.getChannel() + ".constant.password")).thenReturn(CONFIGURATION_MANAGER_PASSWORD);

        InputCreateCardReports result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getUser());
        assertNotNull(result.getPassword());
        assertNotNull(result.getChannel());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getBrandAssociation());
        assertNotNull(result.getBrandAssociation().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getProduct().getSubproduct());
        assertNotNull(result.getProduct().getSubproduct().getId());
        assertNotNull(result.getProduct().getSubproduct().getName());
        assertNotNull(result.getProduct().getSubproduct().getDescription());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getHasPrintedHolderName());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0));
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getFirstName());
        assertNotNull(result.getParticipants().get(0).getMiddleName());
        assertNotNull(result.getParticipants().get(0).getLastName());
        assertNotNull(result.getParticipants().get(0).getSecondLastName());
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0));
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0).getNumber());
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType());
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getParticipants().get(0).getContactDetails());
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(0));
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(0).getEmailContact());
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(0).getEmailContact().getContactType());
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(0).getEmailContact().getAddress());
        assertNull(result.getParticipants().get(0).getContactDetails().get(0).getMobileContact());
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(1));
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(1).getMobileContact());
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(1).getMobileContact().getContactType());
        assertNotNull(result.getParticipants().get(0).getContactDetails().get(1).getMobileContact().getNumber());
        assertNull(result.getParticipants().get(0).getContactDetails().get(1).getEmailContact());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getRelatedContracts());
        assertNotNull(result.getRelatedContracts().get(0));
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());

        assertNotNull(result.getDeliveries());
        assertNotNull(result.getDeliveries().get(0));
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNull(result.getDeliveries().get(0).getAddress().getLocation());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getBranch());
        assertNotNull(result.getDeliveries().get(0).getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getBranch().getName());
        assertNotNull(result.getDeliveries().get(1));
        assertNotNull(result.getDeliveries().get(1).getId());
        assertNotNull(result.getDeliveries().get(1).getServiceType());
        assertNotNull(result.getDeliveries().get(1).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(1).getContact());
        assertNotNull(result.getDeliveries().get(1).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(1).getContact().getId());
        assertNotNull(result.getDeliveries().get(1).getContact().getContact());
        assertNotNull(result.getDeliveries().get(1).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(1).getContact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(1).getAddress());
        assertNotNull(result.getDeliveries().get(1).getAddress().getAddressType());
        assertNull(result.getDeliveries().get(1).getAddress().getId());
        assertNotNull(result.getDeliveries().get(1).getAddress().getLocation());
        assertNotNull(result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getFormattedAddress());
        assertNotNull(result.getDeliveries().get(1).getDestination());
        assertNotNull(result.getDeliveries().get(1).getDestination().getId());
        assertNotNull(result.getDeliveries().get(1).getDestination().getName());
        assertNotNull(result.getDeliveries().get(1).getBranch());
        assertNotNull(result.getDeliveries().get(1).getBranch().getId());
        assertNotNull(result.getDeliveries().get(1).getBranch().getName());
        assertNotNull(result.getContractingBranch());
        assertNotNull(result.getContractingBranch().getId());
        assertNotNull(result.getContractingBranch().getName());
        assertNotNull(result.getLoyaltyProgram());
        assertNotNull(result.getLoyaltyProgram().getId());
        assertNotNull(result.getLoyaltyProgram().getDescription());
        assertNotNull(result.getProposalStatus());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getChannel());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(CONFIGURATION_MANAGER_USER, result.getUser());
        assertEquals(CONFIGURATION_MANAGER_PASSWORD, result.getPassword());
        assertEquals(input.getChannel(), result.getChannel());
        assertEquals(input.getCardType().getId(), result.getCardType().getId());
        assertEquals(input.getNumber(), result.getNumber());
        assertEquals(input.getNumberType().getId(), result.getNumberType().getId());
        assertEquals(input.getBrandAssociation().getId(), result.getBrandAssociation().getId());
        assertEquals(input.getProduct().getId(), result.getProduct().getId());
        assertEquals(input.getProduct().getName(), result.getProduct().getName());
        assertEquals(input.getProduct().getSubproduct().getId(), result.getProduct().getSubproduct().getId());
        assertEquals(input.getProduct().getSubproduct().getName(), result.getProduct().getSubproduct().getName());
        assertEquals(input.getProduct().getSubproduct().getDescription(), result.getProduct().getSubproduct().getDescription());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getCurrencies().get(0).getCurrency());
        assertEquals(input.getCurrencies().get(0).getIsMajor(), result.getCurrencies().get(0).getIsMajor());
        assertEquals(input.getHasPrintedHolderName(), result.getHasPrintedHolderName());
        assertEquals(input.getParticipants().get(0).getId(), result.getParticipants().get(0).getId());
        assertEquals(input.getParticipants().get(0).getFirstName(), result.getParticipants().get(0).getFirstName());
        assertEquals(input.getParticipants().get(0).getMiddleName(), result.getParticipants().get(0).getMiddleName());
        assertEquals(input.getParticipants().get(0).getLastName(), result.getParticipants().get(0).getLastName());
        assertEquals(input.getParticipants().get(0).getSecondLastName(), result.getParticipants().get(0).getSecondLastName());
        assertEquals(input.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getParticipants().get(0).getIdentityDocuments().get(0).getNumber());
        assertEquals(DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertEquals(input.getParticipants().get(0).getContactDetails().get(0).getContact().getContactType(), result.getParticipants().get(0).getContactDetails().get(0).getEmailContact().getContactType());
        assertEquals(input.getParticipants().get(0).getContactDetails().get(0).getContact().getAddress(), result.getParticipants().get(0).getContactDetails().get(0).getEmailContact().getAddress());
        assertEquals(input.getParticipants().get(0).getContactDetails().get(1).getContact().getContactType(), result.getParticipants().get(0).getContactDetails().get(1).getMobileContact().getContactType());
        assertEquals(input.getParticipants().get(0).getContactDetails().get(1).getContact().getNumber(), result.getParticipants().get(0).getContactDetails().get(1).getMobileContact().getNumber());
        assertEquals(input.getPaymentMethod().getId(), result.getPaymentMethod().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getId(), result.getPaymentMethod().getFrecuency().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay(), result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getRelatedContracts().get(0).getNumber(), result.getRelatedContracts().get(0).getNumber());
        assertEquals(input.getRelatedContracts().get(0).getNumberType().getId(), result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(input.getDeliveries().get(0).getId(), result.getDeliveries().get(0).getId());
        assertEquals(input.getDeliveries().get(0).getServiceType().getId(), result.getDeliveries().get(0).getServiceType().getId());
        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getDeliveries().get(0).getContact().getContactType());
        assertEquals(input.getDeliveries().get(0).getContact().getId(), result.getDeliveries().get(0).getContact().getId());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getDeliveries().get(0).getAddress().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getDeliveries().get(0).getDestination().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getName(), result.getDeliveries().get(0).getDestination().getName());
        assertEquals(input.getDeliveries().get(0).getBranch().getId(), result.getDeliveries().get(0).getBranch().getId());
        assertEquals(input.getDeliveries().get(0).getBranch().getName(), result.getDeliveries().get(0).getBranch().getName());
        assertEquals(input.getDeliveries().get(1).getId(), result.getDeliveries().get(1).getId());
        assertEquals(input.getDeliveries().get(1).getServiceType().getId(), result.getDeliveries().get(1).getServiceType().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContactType(), result.getDeliveries().get(1).getContact().getContactType());
        assertEquals(input.getDeliveries().get(1).getContact().getId(), result.getDeliveries().get(1).getContact().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContact().getContactDetailType(), result.getDeliveries().get(1).getContact().getContact().getContactDetailType());
        assertEquals(input.getDeliveries().get(1).getContact().getContact().getAddress(), result.getDeliveries().get(1).getContact().getContact().getAddress());
        assertEquals(input.getDeliveries().get(1).getAddress().getAddressType(), result.getDeliveries().get(1).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0), result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getCode(), result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getName(), result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getFormattedAddress(), result.getDeliveries().get(1).getAddress().getLocation().getAddressComponents().get(0).getFormattedAddress());
        assertEquals(input.getDeliveries().get(1).getDestination().getId(), result.getDeliveries().get(1).getDestination().getId());
        assertEquals(input.getDeliveries().get(1).getDestination().getName(), result.getDeliveries().get(1).getDestination().getName());
        assertEquals(input.getDeliveries().get(1).getBranch().getId(), result.getDeliveries().get(1).getBranch().getId());
        assertEquals(input.getDeliveries().get(1).getBranch().getName(), result.getDeliveries().get(1).getBranch().getName());
        assertEquals(input.getContractingBranch().getId(), result.getContractingBranch().getId());
        assertEquals(input.getContractingBranch().getName(), result.getContractingBranch().getName());
        assertEquals(input.getLoyaltyProgram().getId(), result.getLoyaltyProgram().getId());
        assertEquals(input.getLoyaltyProgram().getDescription(), result.getLoyaltyProgram().getDescription());
        assertEquals(PROPOSAL_STATUS_FRONTEND_VALUE_3, result.getProposalStatus());
        assertEquals(input.getOfferId(), result.getOfferId());
        assertEquals(input.getChannel(), result.getChannel());
    }

    @Test
    public void mapInWithoutNonMandatoryParametersTest() throws IOException {
        ReportCardCreation input = mock.getReportCardCreationMock();
        when(configurationManager.getProperty("servicing.aap.configuration." + input.getChannel() + ".constant.user")).thenReturn(CONFIGURATION_MANAGER_USER);
        when(configurationManager.getProperty("servicing.aap.configuration." + input.getChannel() + ".constant.password")).thenReturn(CONFIGURATION_MANAGER_PASSWORD);

        input.setBrandAssociation(null);
        input.getProduct().setName(null);
        input.getProduct().setSubproduct(null);
        input.setHasPrintedHolderName(null);
        input.getParticipants().get(0).setMiddleName(null);
        input.getParticipants().get(0).setSecondLastName(null);
        input.getParticipants().get(0).setContactDetails(null);
        input.setPaymentMethod(null);
        input.setGrantedCredits(null);
        input.setRelatedContracts(null);
        input.setDeliveries(null);
        input.setContractingBranch(null);
        input.setLoyaltyProgram(null);
        input.setOfferId(null);

        InputCreateCardReports result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getUser());
        assertNotNull(result.getPassword());
        assertNotNull(result.getChannel());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNull(result.getBrandAssociation());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNull(result.getProduct().getName());
        assertNull(result.getProduct().getSubproduct());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNull(result.getHasPrintedHolderName());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0));
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getFirstName());
        assertNull(result.getParticipants().get(0).getMiddleName());
        assertNotNull(result.getParticipants().get(0).getLastName());
        assertNull(result.getParticipants().get(0).getSecondLastName());
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0));
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0).getNumber());
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType());
        assertNotNull(result.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNull(result.getParticipants().get(0).getContactDetails());
        assertNull(result.getPaymentMethod());
        assertNull(result.getGrantedCredits());
        assertNull(result.getRelatedContracts());
        assertNull(result.getDeliveries());
        assertNull(result.getContractingBranch());
        assertNull(result.getLoyaltyProgram());
        assertNotNull(result.getProposalStatus());
        assertNull(result.getOfferId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(CONFIGURATION_MANAGER_USER, result.getUser());
        assertEquals(CONFIGURATION_MANAGER_PASSWORD, result.getPassword());
        assertEquals(input.getChannel(), result.getChannel());
        assertEquals(input.getCardType().getId(), result.getCardType().getId());
        assertEquals(input.getNumber(), result.getNumber());
        assertEquals(input.getNumberType().getId(), result.getNumberType().getId());
        assertEquals(input.getProduct().getId(), result.getProduct().getId());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getCurrencies().get(0).getCurrency());
        assertEquals(input.getCurrencies().get(0).getIsMajor(), result.getCurrencies().get(0).getIsMajor());
        assertEquals(input.getParticipants().get(0).getId(), result.getParticipants().get(0).getId());
        assertEquals(input.getParticipants().get(0).getFirstName(), result.getParticipants().get(0).getFirstName());
        assertEquals(input.getParticipants().get(0).getLastName(), result.getParticipants().get(0).getLastName());
        assertEquals(input.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getParticipants().get(0).getIdentityDocuments().get(0).getNumber());
        assertEquals(DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getParticipants().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertEquals(PROPOSAL_STATUS_FRONTEND_VALUE_3, result.getProposalStatus());
    }

    @Test
    public void mapInEmptyBodyTest() {
        InputCreateCardReports result = mapper.mapIn(CARD_ID, new ReportCardCreation());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNull(result.getUser());
        assertNull(result.getPassword());
        assertNull(result.getCardType());
        assertNull(result.getNumber());
        assertNull(result.getNumberType());
        assertNull(result.getBrandAssociation());
        assertNull(result.getProduct());
        assertNull(result.getCurrencies());
        assertNull(result.getHasPrintedHolderName());
        assertNull(result.getParticipants());
        assertNull(result.getPaymentMethod());
        assertNull(result.getGrantedCredits());
        assertNull(result.getRelatedContracts());
        assertNull(result.getDeliveries());
        assertNull(result.getContractingBranch());
        assertNull(result.getLoyaltyProgram());
        assertNull(result.getProposalStatus());
        assertNull(result.getOfferId());
        assertNull(result.getChannel());

        assertEquals(CARD_ID, result.getCardId());
    }
}
