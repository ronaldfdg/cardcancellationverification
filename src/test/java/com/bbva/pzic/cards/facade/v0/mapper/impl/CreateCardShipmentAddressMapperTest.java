package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntSpecificAddressType;
import com.bbva.pzic.cards.business.dto.DTOIntStoredAddressType;
import com.bbva.pzic.cards.business.dto.InputCreateCardShipmentAddress;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardShipmentAddressMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.SHIPMENT_ID;
import static org.junit.Assert.*;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class CreateCardShipmentAddressMapperTest {

    private ICreateCardShipmentAddressMapper mapper;

    @Before
    public void setUp() {
        mapper = new CreateCardShipmentAddressMapper();
    }

    @Test
    public void mapInSpecificShipmentAddressFullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());

        assertEquals(SHIPMENT_ID, result.getShipmentId());
        assertEquals(input.getAddressType(), result.getAddressType());
        assertEquals(input.getLocation().getFormattedAddress(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertEquals(input.getLocation().getLocationTypes().get(0), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().get(0));
        assertEquals(input.getLocation().getAddressComponents().get(0).getComponentTypes(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(input.getLocation().getAddressComponents().get(0).getCode(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getLocation().getAddressComponents().get(0).getName(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getLocation().getAddressComponents().get(1).getComponentTypes(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(input.getLocation().getAddressComponents().get(1).getCode(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertEquals(input.getLocation().getAddressComponents().get(1).getName(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertEquals(input.getLocation().getGeolocation().getLatitude(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertEquals(input.getLocation().getGeolocation().getLongitude(), ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.setLocation(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNull(result.getShipmentAddress());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationFormattedAddressTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().setFormattedAddress(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationLocationTypesTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().setLocationTypes(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationAddressComponentsTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().setAddressComponents(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationAddressComponentsComponentTypesTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().getAddressComponents().get(0).setComponentTypes(null);
        input.getLocation().getAddressComponents().get(1).setComponentTypes(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationAddressComponentsCodeTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().getAddressComponents().get(0).setCode(null);
        input.getLocation().getAddressComponents().get(1).setCode(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressWithNullLocationAddressComponentsNameTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().getAddressComponents().get(0).setName(null);
        input.getLocation().getAddressComponents().get(1).setName(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressLocationGeolocationNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().setGeolocation(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
    }

    @Test
    public void mapInSpecificShipmentAddressLocationGeolocationLongitudeNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().getGeolocation().setLongitude(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInSpecificShipmentAddressLocationGeolocationLatitudeNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildSpecificShipmentAddress();
        input.getLocation().getGeolocation().setLatitude(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getFormattedAddress());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getLocationTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents());
        assertEquals(2, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(0).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes());
        assertEquals(1, ((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getComponentTypes().size());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getAddressComponents().get(1).getName());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation());
        assertNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLatitude());
        assertNotNull(((DTOIntSpecificAddressType) result.getShipmentAddress()).getLocation().getGeolocation().getLongitude());
    }

    @Test
    public void mapInStoredShipmentAddressFullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildStoredShipmentAddress();
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getId());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination().getId());

        assertEquals(SHIPMENT_ID, result.getShipmentId());
        assertEquals(input.getAddressType(), result.getAddressType());
        assertEquals(input.getId(), ((DTOIntStoredAddressType) result.getShipmentAddress()).getId());
        assertEquals(input.getDestination().getId(), ((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination().getId());
    }

    @Test
    public void mapInStoredShipmentAddressIdNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildStoredShipmentAddress();
        input.setId(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getId());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination().getId());
    }

    @Test
    public void mapInStoredShipmentAddressDestinationNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildStoredShipmentAddress();
        input.setDestination(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getId());
        assertNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination());
    }

    @Test
    public void mapInStoredShipmentAddressDestinationIdNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildStoredShipmentAddress();
        input.getDestination().setId(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getAddressType());
        assertNotNull(result.getShipmentAddress());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getId());
        assertNotNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination());
        assertNull(((DTOIntStoredAddressType) result.getShipmentAddress()).getDestination().getId());
    }

    @Test
    public void mapInStoredShipmentAddressAddressTypeNullTest() throws IOException {
        ShipmentAddress input = EntityMock.getInstance().buildStoredShipmentAddress();
        input.setAddressType(null);
        InputCreateCardShipmentAddress result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNull(result.getAddressType());
        assertNull(result.getShipmentAddress());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<ShipmentAddress> result = mapper.mapOut(EntityMock.getInstance().buildStoredShipmentAddress());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<ShipmentAddress> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        ServiceResponse<ShipmentAddress> result = mapper.mapOut(new ShipmentAddress());

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNull(result.getData().getAddressType());
        assertNull(result.getData().getId());
    }
}