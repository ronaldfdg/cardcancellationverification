package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.Shipment;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardShipmentMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.ADDRESSES_EXPAND;
import static com.bbva.pzic.cards.EntityMock.SHIPMENT_ID;
import static org.junit.Assert.*;

public class GetCardShipmentMapperTest {

    private IGetCardShipmentMapper mapper;

    @Before
    public void setUp() {
        mapper = new GetCardShipmentMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetCardShipment result = mapper.mapIn(SHIPMENT_ID, ADDRESSES_EXPAND);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getExpand());

        assertEquals(SHIPMENT_ID, result.getShipmentId());
        assertNotNull(ADDRESSES_EXPAND, result.getExpand());
    }

    @Test
    public void mapInExpandNullTest() {
        InputGetCardShipment result = mapper.mapIn(SHIPMENT_ID, null);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNull(result.getExpand());

        assertEquals(SHIPMENT_ID, result.getShipmentId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<Shipment> result = mapper.mapOut(EntityMock.getInstance().buildShipment());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<Shipment> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        ServiceResponse<Shipment> result = mapper.mapOut(new Shipment());

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNull(result.getData().getId());
        assertNull(result.getData().getAddresses());
        assertNull(result.getData().getExternalCode());
        assertNull(result.getData().getParticipant());
        assertNull(result.getData().getShippingCompany());
        assertNull(result.getData().getStatus());
        assertNull(result.getData().getTerm());
    }
}
