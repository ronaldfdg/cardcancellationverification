package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputInitializeCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.InitializeShipment;
import com.bbva.pzic.cards.facade.v0.mapper.IInitializeCardShipmentMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class InitializeCardShipmentMapperTest {

    private IInitializeCardShipmentMapper mapper;

    @Before
    public void setUp() {
        mapper = new InitializeCardShipmentMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());

        assertEquals(input.getExternalCode(), result.getExternalCode());
        assertEquals(input.getParticipant().getIdentityDocument().getDocumentNumber(), result.getParticipant().getIdentityDocument().getNumber());
        assertEquals(input.getParticipant().getIdentityDocument().getDocumentType().getId(), result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getParticipant().getCard().getReferenceNumber(), result.getParticipant().getCard().getReferenceNumber());
        assertEquals(input.getParticipant().getCard().getCardAgreement(), result.getParticipant().getCard().getCardAgreement());
        assertEquals(input.getShippingCompany().getId(), result.getShippingCompany().getId());
        assertEquals(input.getShippingDevice().getId(), result.getShippingDevice().getId());
        assertEquals(input.getShippingDevice().getMacAddress(), result.getShippingDevice().getMacAddress());
        assertEquals(input.getShippingDevice().getApplication().getName(), result.getShippingDevice().getApplication().getName());
        assertEquals(input.getShippingDevice().getApplication().getVersion(), result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInExternalCodeNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.setExternalCode(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInParticipantNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.setParticipant(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNull(result.getParticipant());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInParticipantIdentityDocumentNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.getParticipant().setIdentityDocument(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInParticipantIdentityDocumentDocumentTypeNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.getParticipant().getIdentityDocument().setDocumentType(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInParticipantCardNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.getParticipant().setCard(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNull(result.getParticipant().getCard());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInShippingCompanyNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.setShippingCompany(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNull(result.getShippingCompany());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNotNull(result.getShippingDevice().getApplication());
        assertNotNull(result.getShippingDevice().getApplication().getName());
        assertNotNull(result.getShippingDevice().getApplication().getVersion());
    }

    @Test
    public void mapInShippingDeviceNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.setShippingDevice(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNull(result.getShippingDevice());
    }

    @Test
    public void mapInShippingDeviceApplicationNullTest() throws IOException {
        InitializeShipment input = EntityMock.getInstance().buildInitializeShipment();
        input.getShippingDevice().setApplication(null);
        InputInitializeCardShipment result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getParticipant());
        assertNotNull(result.getParticipant().getIdentityDocument());
        assertNotNull(result.getParticipant().getIdentityDocument().getNumber());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType());
        assertNotNull(result.getParticipant().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getParticipant().getCard());
        assertNotNull(result.getParticipant().getCard().getReferenceNumber());
        assertNotNull(result.getParticipant().getCard().getCardAgreement());
        assertNotNull(result.getShippingCompany());
        assertNotNull(result.getShippingCompany().getId());
        assertNotNull(result.getShippingDevice());
        assertNotNull(result.getShippingDevice().getId());
        assertNotNull(result.getShippingDevice().getMacAddress());
        assertNull(result.getShippingDevice().getApplication());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<InitializeShipment> result = mapper.mapOut(EntityMock.getInstance().buildInitializeShipment());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<InitializeShipment> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        ServiceResponse<InitializeShipment> result = mapper.mapOut(new InitializeShipment());

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNull(result.getData().getId());
    }
}
