package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Limits;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import com.bbva.pzic.cards.EntityMock;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListCardLimitV0MapperTest {

    @InjectMocks
    private ListCardLimitV0Mapper mapper;

    @Test
    public void mapInFullTest() {
        DTOIntCard result = mapper.mapIn(CARD_ID);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertEquals(CARD_ID, result.getCardId());
    }

    @Test
    public void mapInWithoutCardIdTest() {
        DTOIntCard result = mapper.mapIn(null);
        assertNotNull(result);
        assertNull(result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        List<Limits> limits = EntityMock.getInstance().buildListLimits();
        ServiceResponse<List<Limits>> result = mapper.mapOut(limits);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(2, result.getData().size());
        assertNotNull(result.getData().get(0).getId());
        assertNotNull(result.getData().get(0).getAmountLimits());
        assertNotNull(result.getData().get(0).getAllowedInterval());
        assertNotNull(result.getData().get(1).getId());
        assertNotNull(result.getData().get(1).getAmountLimits());
        assertNotNull(result.getData().get(1).getAllowedInterval());

        assertEquals(limits.get(0).getId(), result.getData().get(0).getId());
        assertEquals(limits.get(0).getAmountLimits().get(0).getAmount(), result.getData().get(0).getAmountLimits().get(0).getAmount());
        assertEquals(limits.get(0).getAmountLimits().get(0).getCurrency(), result.getData().get(0).getAmountLimits().get(0).getCurrency());
        assertEquals(limits.get(0).getAllowedInterval().getMinimumAmount().getAmount(), result.getData().get(0).getAllowedInterval().getMinimumAmount().getAmount());
        assertEquals(limits.get(0).getAllowedInterval().getMinimumAmount().getCurrency(), result.getData().get(0).getAllowedInterval().getMinimumAmount().getCurrency());
        assertEquals(limits.get(0).getAllowedInterval().getMaximumAmount().getAmount(), result.getData().get(0).getAllowedInterval().getMaximumAmount().getAmount());
        assertEquals(limits.get(0).getAllowedInterval().getMaximumAmount().getCurrency(), result.getData().get(0).getAllowedInterval().getMaximumAmount().getCurrency());

        assertEquals(limits.get(1).getId(), result.getData().get(1).getId());
        assertEquals(limits.get(1).getAmountLimits().get(0).getAmount(), result.getData().get(1).getAmountLimits().get(0).getAmount());
        assertEquals(limits.get(1).getAmountLimits().get(0).getCurrency(), result.getData().get(1).getAmountLimits().get(0).getCurrency());
        assertEquals(limits.get(1).getAllowedInterval().getMinimumAmount().getAmount(), result.getData().get(1).getAllowedInterval().getMinimumAmount().getAmount());
        assertEquals(limits.get(1).getAllowedInterval().getMinimumAmount().getCurrency(), result.getData().get(1).getAllowedInterval().getMinimumAmount().getCurrency());
        assertEquals(limits.get(1).getAllowedInterval().getMaximumAmount().getAmount(), result.getData().get(1).getAllowedInterval().getMaximumAmount().getAmount());
        assertEquals(limits.get(1).getAllowedInterval().getMaximumAmount().getCurrency(), result.getData().get(1).getAllowedInterval().getMaximumAmount().getCurrency());
    }

    @Test
    public void mapOutWithOut() {
        final ServiceResponse<List<Limits>> result = mapper.mapOut(Collections.emptyList());
        assertNull(result);
    }
}
