package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.facade.v0.dto.Shipments;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardShipmentsMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

public class ListCardShipmentsMapperTest {

    private IListCardShipmentsMapper mapper;

    @Before
    public void setUp() {
        mapper = new ListCardShipmentsMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListCardShipments result = mapper.mapIn(CARD_AGREEMENT, CARD_REFERENCE_NUMBER, CARD_EXTERNAL_CODE, CARD_SHIPPING_COMPANY_ID);

        assertNotNull(result.getCardAgreement());
        assertNotNull(result.getExternalCode());
        assertNotNull(result.getReferenceNumber());
        assertNotNull(result.getShippingCompanyId());

        assertEquals(CARD_AGREEMENT, result.getCardAgreement());
        assertEquals(CARD_EXTERNAL_CODE, result.getExternalCode());
        assertEquals(CARD_REFERENCE_NUMBER, result.getReferenceNumber());
        assertEquals(CARD_SHIPPING_COMPANY_ID, result.getShippingCompanyId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<List<Shipments>> result = mapper.mapOut(EntityMock.getInstance().buildShipments());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<List<Shipments>> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        ServiceResponse<List<Shipments>> result = mapper.mapOut(new ArrayList<>());

        assertNull(result);
    }
}
