package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static com.bbva.pzic.cards.EntityMock.*;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardMapperTest {

    @InjectMocks
    private CreateCardMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Mock
    private CypherTool cypherTool;

    private EntityMock entityMock = EntityMock.getInstance();

    public void mapInEnumMapper() {
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", "DEBIT_CARD")).thenReturn(CARD_CARDTYPE_ID_KEY_TESTED);
        Mockito.when(enumMapper.getBackendValue("cards.physicalSupport.id", "STICKER")).thenReturn(PHYSICAL_SUPPORT_ID);
        Mockito.when(cypherTool.decrypt(CARD_RELATED_CONTRACT_ENCRYPT, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD)).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
    }

    @Test
    public void mapInFullTest() {
        mapInEnumMapper();
        Card input = entityMock.getCard();
        DTOIntCard result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardTypeId());
        Assert.assertNotNull(result.getPhysicalSupportId());
        Assert.assertNotNull(result.getRelatedContractId());

        Assert.assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, result.getCardTypeId());
        Assert.assertEquals(PHYSICAL_SUPPORT_ID, result.getPhysicalSupportId());
        Assert.assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getRelatedContractId());
    }

    @Test
    public void mapInWithoutCardTypeIdTest() {
        mapInEnumMapper();
        Card input = entityMock.getCard();
        input.getCardType().setId(null);
        DTOIntCard result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNull(result.getCardTypeId());
        Assert.assertNotNull(result.getPhysicalSupportId());

        Assert.assertEquals(PHYSICAL_SUPPORT_ID, result.getPhysicalSupportId());
        Assert.assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getRelatedContractId());
    }

    @Test
    public void mapInWithoutPhysicalIdTest() {
        mapInEnumMapper();
        Card input = entityMock.getCard();
        input.getPhysicalSupport().setId(null);
        DTOIntCard result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardTypeId());
        Assert.assertNull(result.getPhysicalSupportId());

        Assert.assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, result.getCardTypeId());
        Assert.assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getRelatedContractId());
    }

    @Test
    public void mapInWithRelatedContractNullTest() {
        mapInEnumMapper();
        Card input = entityMock.getCard();
        input.setRelatedContracts(null);
        DTOIntCard result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardTypeId());
        Assert.assertNotNull(result.getPhysicalSupportId());
        Assert.assertNull(result.getRelatedContractId());

        Assert.assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, result.getCardTypeId());
        Assert.assertEquals(PHYSICAL_SUPPORT_ID, result.getPhysicalSupportId());
    }

    @Test
    public void mapInWithRelatedContractInitializedTest() {
        mapInEnumMapper();
        Card input = entityMock.getCard();
        input.setRelatedContracts(new ArrayList<>());
        DTOIntCard result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardTypeId());
        Assert.assertNotNull(result.getPhysicalSupportId());
        Assert.assertNull(result.getRelatedContractId());

        Assert.assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, result.getCardTypeId());
        Assert.assertEquals(PHYSICAL_SUPPORT_ID, result.getPhysicalSupportId());
    }

    @Test
    public void mapInWithoutRelatedContractIdTest() {
        mapInEnumMapper();
        Card input = entityMock.getCard();
        input.getRelatedContracts().get(0).setContractId(null);
        DTOIntCard result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardTypeId());
        Assert.assertNotNull(result.getPhysicalSupportId());
        Assert.assertNull(result.getRelatedContractId());

        Assert.assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, result.getCardTypeId());
        Assert.assertEquals(PHYSICAL_SUPPORT_ID, result.getPhysicalSupportId());
    }
}