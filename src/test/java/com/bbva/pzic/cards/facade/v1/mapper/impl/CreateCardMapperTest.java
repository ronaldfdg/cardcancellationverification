package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.util.Constants.HEADER_BCS_OPERATION_TRACER;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardMapperTest {

    @InjectMocks
    private CreateCardMapper mapper;
    @Mock
    private InputHeaderManager inputHeaderManager;
    @Mock
    private Translator translator;

    @Test
    public void mapInFullForHostTest() throws IOException {
        CardPost input = EntityMock.getInstance().buildCardPost();

        when(inputHeaderManager.getHeader(HEADER_BCS_OPERATION_TRACER))
                .thenReturn(EntityMock.CONTEXT_PROVIDER_HEADER_CONTACT_ID);

        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", input.getCardType().getId()))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("cards.physicalSupport.id", input.getPhysicalSupport().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.relatedContracts.relationType", input.getRelatedContracts().get(0).getRelationType().getId()))
                .thenReturn("0");
        when(translator.translateFrontendEnumValueStrictly("cards.relatedContracts.productType", input.getRelatedContracts().get(0).getProduct().getProductType().getId()))
                .thenReturn(EntityMock.PRODUCT_ID_50);
        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.id", input.getPaymentMethod().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.frecuency", input.getPaymentMethod().getFrecuency().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.supportContractType.id", input.getSupportContractType()))
                .thenReturn("D");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.contactType", input.getDeliveries().get(0).getContact().getContactType()))
                .thenReturn("E");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.contactType", input.getDeliveries().get(1).getContact().getContactType()))
                .thenReturn("E");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.contactType", input.getDeliveries().get(2).getContact().getContactType()))
                .thenReturn("E");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destination", input.getDeliveries().get(0).getDestination().getId()))
                .thenReturn("0");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destination", input.getDeliveries().get(1).getDestination().getId()))
                .thenReturn("1");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destination", input.getDeliveries().get(2).getDestination().getId()))
                .thenReturn("2");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.addressType", input.getDeliveries().get(0).getAddress().getAddressType()))
                .thenReturn("E");
        when(translator.translateFrontendEnumValueStrictly("cards.participants.participantType", input.getParticipants().get(0).getParticipantType().getId()))
                .thenReturn("T");
        when(translator.translateFrontendEnumValueStrictly("cards.participants.participantType", input.getParticipants().get(1).getParticipantType().getId()))
                .thenReturn("A");
        when(translator.translateFrontendEnumValueStrictly("cards.participants.legalPersonType", input.getParticipants().get(0).getLegalPersonType().getId()))
                .thenReturn("N");
        when(translator.translateFrontendEnumValueStrictly("cards.participants.legalPersonType", input.getParticipants().get(1).getLegalPersonType().getId()))
                .thenReturn("I");

        InputCreateCard inputResult = mapper.mapIn(input);
        DTOIntCard result = inputResult.getCard();

        assertNotNull(inputResult.getHeaderBCSOperationTracer());
        assertEquals(EntityMock.CONTEXT_PROVIDER_HEADER_CONTACT_ID, inputResult.getHeaderBCSOperationTracer());

        commonAssertsForHost(input, result);

        // 0 - HOME
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNull(result.getDeliveries().get(0).getDestination().getBranch());

        assertEquals("0", result.getDeliveries().get(0).getDestination().getId());
        assertEquals("E", result.getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getDeliveries().get(0).getAddress().getId());
        assertEquals("E", result.getDeliveries().get(0).getContact().getContactType());
        assertEquals(input.getDeliveries().get(0).getContact().getId(), result.getDeliveries().get(0).getContact().getId());

        // 1 - CUSTOM
        assertNotNull(result.getDeliveries().get(1).getContact());
        assertNotNull(result.getDeliveries().get(1).getDestination().getId());
        assertNotNull(result.getDeliveries().get(1).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(1).getAddress().getId());
        assertNull(result.getDeliveries().get(1).getDestination().getBranch());

        assertEquals("1", result.getDeliveries().get(1).getDestination().getId());
        assertEquals("E", result.getDeliveries().get(1).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(1).getAddress().getId(), result.getDeliveries().get(1).getAddress().getId());
        assertEquals("E", result.getDeliveries().get(1).getContact().getContactType());
        assertEquals(input.getDeliveries().get(1).getContact().getId(), result.getDeliveries().get(1).getContact().getId());

        // 2 - BRANCH
        assertNotNull(result.getDeliveries().get(2).getContact());
        assertNotNull(result.getDeliveries().get(2).getDestination().getId());
        assertNull(result.getDeliveries().get(2).getAddress());
        assertNotNull(result.getDeliveries().get(2).getDestination().getBranch().getId());

        assertEquals("2", result.getDeliveries().get(2).getDestination().getId());
        assertEquals(input.getDeliveries().get(2).getDestination().getBranch().getId(), result.getDeliveries().get(2).getDestination().getBranch().getId());
        assertEquals("E", result.getDeliveries().get(2).getContact().getContactType());
        assertEquals(input.getDeliveries().get(2).getContact().getId(), result.getDeliveries().get(2).getContact().getId());
    }

    @Test
    public void mapInFullForAPXTest() throws IOException {
        CardPost input = EntityMock.getInstance().buildCardPost();

        InputCreateCard inputResult = mapper.mapIn(input);
        DTOIntCard result = inputResult.getCard();

        assertNull(inputResult.getHeaderBCSOperationTracer());

        commonAssertsForAPX(input, result);

        // 0 - HOME
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNull(result.getDeliveries().get(0).getDestination().getBranch());

        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getDeliveries().get(0).getDestination().getId());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getDeliveries().get(0).getAddress().getId());
        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getDeliveries().get(0).getContact().getContactType());
        assertEquals(input.getDeliveries().get(0).getContact().getId(), result.getDeliveries().get(0).getContact().getId());

        // 1 - CUSTOM
        assertNotNull(result.getDeliveries().get(1).getContact());
        assertNotNull(result.getDeliveries().get(1).getDestination().getId());
        assertNotNull(result.getDeliveries().get(1).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(1).getAddress().getId());
        assertNull(result.getDeliveries().get(1).getDestination().getBranch());

        assertEquals(input.getDeliveries().get(1).getDestination().getId(), result.getDeliveries().get(1).getDestination().getId());
        assertEquals(input.getDeliveries().get(1).getAddress().getAddressType(), result.getDeliveries().get(1).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(1).getAddress().getId(), result.getDeliveries().get(1).getAddress().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContactType(), result.getDeliveries().get(1).getContact().getContactType());
        assertEquals(input.getDeliveries().get(1).getContact().getId(), result.getDeliveries().get(1).getContact().getId());

        // 2 - BRANCH
        assertNotNull(result.getDeliveries().get(2).getContact());
        assertNotNull(result.getDeliveries().get(2).getDestination().getId());
        assertNull(result.getDeliveries().get(2).getAddress());
        assertNotNull(result.getDeliveries().get(2).getDestination().getBranch().getId());

        assertEquals(input.getDeliveries().get(2).getDestination().getId(), result.getDeliveries().get(2).getDestination().getId());
        assertEquals(input.getDeliveries().get(2).getDestination().getBranch().getId(), result.getDeliveries().get(2).getDestination().getBranch().getId());
        assertEquals(input.getDeliveries().get(2).getContact().getContactType(), result.getDeliveries().get(2).getContact().getContactType());
        assertEquals(input.getDeliveries().get(2).getContact().getId(), result.getDeliveries().get(2).getContact().getId());
    }

    private void commonAssertsForHost(final CardPost input, final DTOIntCard result) {
        commonAssertsNotNull(result);

        assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result.getCardTypeId());
        assertEquals(input.getProduct().getId(), result.getProductId());
        assertEquals("P", result.getPhysicalSupportId());
        assertEquals(input.getHolderName(), result.getHolderName());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getCurrenciesCurrency());
        assertEquals(input.getCurrencies().get(0).getIsMajor(), result.getCurrenciesIsMajor());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCreditsCurrency());
        assertEquals(input.getCutOffDay(), result.getCutOffDay());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getRelatedContracts().get(0).getContractId());
        assertEquals("0", result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(EntityMock.PRODUCT_ID_50, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(input.getImages().get(0).getId(), result.getImages().get(0).getId());
        assertEquals(input.getDeliveries().get(0).getServiceType().getId(), result.getDeliveries().get(0).getServiceTypeId());
        assertEquals(input.getDeliveries().get(1).getServiceType().getId(), result.getDeliveries().get(1).getServiceTypeId());
        assertEquals(input.getDeliveries().get(2).getServiceType().getId(), result.getDeliveries().get(2).getServiceTypeId());
        assertEquals(input.getParticipants().get(0).getPersonType(), result.getParticipants().get(0).getPersonType());
        assertEquals(input.getParticipants().get(0).getId(), result.getParticipants().get(0).getId());
        assertEquals("T", result.getParticipants().get(0).getParticipantTypeId());
        assertEquals("N", result.getParticipants().get(0).getLegalPersonTypeId());
        assertEquals(input.getParticipants().get(1).getPersonType(), result.getParticipants().get(1).getPersonType());
        assertEquals(input.getParticipants().get(1).getId(), result.getParticipants().get(1).getId());
        assertEquals("A", result.getParticipants().get(1).getParticipantTypeId());
        assertEquals("I", result.getParticipants().get(1).getLegalPersonTypeId());
        assertEquals("M", result.getPaymentMethod().getId());
        assertEquals("M", result.getPaymentMethod().getFrequency().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay(), result.getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertEquals("D", result.getSupportContractType());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getMembershipsNumber());
        assertEquals(input.getCardAgreement(), result.getCardAgreement());
        assertEquals(input.getContractingBranch().getId(), result.getContractingBranchId());
        assertEquals(input.getManagementBranch().getId(), result.getManagementBranchId());
        assertEquals(input.getContractingBusinessAgent().getId(), result.getContractingBusinessAgentId());
        assertEquals(input.getMarketBusinessAgent().getId(), result.getMarketBusinessAgentId());
        assertEquals(input.getBankIdentificationNumber(), result.getBankIdentificationNumber());
    }

    private void commonAssertsForAPX(final CardPost input, final DTOIntCard result) {
        commonAssertsNotNull(result);

        assertEquals(input.getCardType().getId(), result.getCardTypeId());
        assertEquals(input.getProduct().getId(), result.getProductId());
        assertEquals(input.getPhysicalSupport().getId(), result.getPhysicalSupportId());
        assertEquals(input.getHolderName(), result.getHolderName());
        assertEquals(input.getCurrencies().get(0).getCurrency(), result.getCurrenciesCurrency());
        assertEquals(input.getCurrencies().get(0).getIsMajor(), result.getCurrenciesIsMajor());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCreditsCurrency());
        assertEquals(input.getCutOffDay(), result.getCutOffDay());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getRelatedContracts().get(0).getContractId());
        assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(), result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getProductType().getId(), result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(input.getImages().get(0).getId(), result.getImages().get(0).getId());
        assertEquals(input.getDeliveries().get(0).getServiceType().getId(), result.getDeliveries().get(0).getServiceTypeId());
        assertEquals(input.getDeliveries().get(1).getServiceType().getId(), result.getDeliveries().get(1).getServiceTypeId());
        assertEquals(input.getDeliveries().get(2).getServiceType().getId(), result.getDeliveries().get(2).getServiceTypeId());
        assertEquals(input.getParticipants().get(0).getPersonType(), result.getParticipants().get(0).getPersonType());
        assertEquals(input.getParticipants().get(0).getId(), result.getParticipants().get(0).getId());
        assertEquals(input.getParticipants().get(0).getParticipantType().getId(), result.getParticipants().get(0).getParticipantTypeId());
        assertEquals(input.getParticipants().get(0).getLegalPersonType().getId(), result.getParticipants().get(0).getLegalPersonTypeId());
        assertEquals(input.getParticipants().get(1).getPersonType(), result.getParticipants().get(1).getPersonType());
        assertEquals(input.getParticipants().get(1).getId(), result.getParticipants().get(1).getId());
        assertEquals(input.getParticipants().get(1).getParticipantType().getId(), result.getParticipants().get(1).getParticipantTypeId());
        assertEquals(input.getParticipants().get(1).getLegalPersonType().getId(), result.getParticipants().get(1).getLegalPersonTypeId());
        assertEquals(input.getPaymentMethod().getId(), result.getPaymentMethod().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getId(), result.getPaymentMethod().getFrequency().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay(), result.getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertEquals(input.getSupportContractType(), result.getSupportContractType());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getMembershipsNumber());
        assertEquals(input.getCardAgreement(), result.getCardAgreement());
        assertEquals(input.getContractingBranch().getId(), result.getContractingBranchId());
        assertEquals(input.getManagementBranch().getId(), result.getManagementBranchId());
        assertEquals(input.getContractingBusinessAgent().getId(), result.getContractingBusinessAgentId());
        assertEquals(input.getMarketBusinessAgent().getId(), result.getMarketBusinessAgentId());
        assertEquals(input.getBankIdentificationNumber(), result.getBankIdentificationNumber());
    }

    private void commonAssertsNotNull(final DTOIntCard result) {
        assertNotNull(result.getCardTypeId());
        assertNotNull(result.getProductId());
        assertNotNull(result.getPhysicalSupportId());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrenciesCurrency());
        assertNotNull(result.getCurrenciesIsMajor());
        assertNotNull(result.getGrantedCreditsAmount());
        assertNotNull(result.getGrantedCreditsCurrency());
        assertNotNull(result.getCutOffDay());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getImages());
        assertNotNull(result.getDeliveries().get(0).getServiceTypeId());
        assertNotNull(result.getDeliveries().get(1).getServiceTypeId());
        assertNotNull(result.getDeliveries().get(2).getServiceTypeId());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getParticipantTypeId());
        assertNotNull(result.getParticipants().get(0).getLegalPersonTypeId());
        assertNotNull(result.getParticipants().get(1).getPersonType());
        assertNotNull(result.getParticipants().get(1).getId());
        assertNotNull(result.getParticipants().get(1).getParticipantTypeId());
        assertNotNull(result.getParticipants().get(1).getLegalPersonTypeId());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrequency().getId());
        assertNotNull(result.getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertNotNull(result.getSupportContractType());
        assertNotNull(result.getMembershipsNumber());
        assertNotNull(result.getCardAgreement());
        assertNotNull(result.getContractingBranchId());
        assertNotNull(result.getManagementBranchId());
        assertNotNull(result.getContractingBusinessAgentId());
        assertNotNull(result.getMarketBusinessAgentId());
        assertNotNull(result.getBankIdentificationNumber());
    }

    @Test
    public void mapInEmptyTest() {
        InputCreateCard inputResult = mapper.mapIn(new CardPost());
        DTOIntCard result = inputResult.getCard();

        assertNull(inputResult.getHeaderBCSOperationTracer());
        assertNull(result.getCardTypeId());
        assertNull(result.getProductId());
        assertNull(result.getPhysicalSupportId());
        assertNull(result.getHolderName());
        assertNull(result.getCurrenciesCurrency());
        assertNull(result.getCurrenciesIsMajor());
        assertNull(result.getGrantedCreditsAmount());
        assertNull(result.getGrantedCreditsCurrency());
        assertNull(result.getCutOffDay());
        assertNull(result.getRelatedContracts());
        assertNull(result.getImages());
        assertNull(result.getDeliveries());
        assertNull(result.getPaymentMethod());
        assertNull(result.getSupportContractType());
        assertNull(result.getMembershipsNumber());
        assertNull(result.getCardAgreement());
        assertNull(result.getContractingBranchId());
        assertNull(result.getManagementBranchId());
        assertNull(result.getContractingBusinessAgentId());
        assertNull(result.getMarketBusinessAgentId());
        assertNull(result.getBankIdentificationNumber());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponseCreated<CardPost> result = mapper.mapOut(new CardPost());
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponseCreated<CardPost> result = mapper.mapOut(null);
        assertNull(result);
    }
}
