package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.CardData;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Constants.BRANCH;
import static com.bbva.pzic.cards.util.Constants.HOME;
import static org.junit.Assert.*;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardMapperTest {

    private final EntityMock entityMock = EntityMock.getInstance();
    @InjectMocks
    private CreateCardMapper mapper;
    @Mock
    private Translator translator;
    @Mock
    private CypherTool cypherTool;

    private void mapInEnumMapper() {
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", "DEBIT_CARD")).thenReturn("D");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.physicalSupport.id", "STICKER")).thenReturn("P");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.statementType", "PHYSICAL")).thenReturn("S");
        Mockito.when(translator.translateFrontendEnumValueStrictly("accounts.delivery.deliveryType", "PHYSICAL")).thenReturn("F");

        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.deliveriesManagement.id", "STATEMENT")).thenReturn("001");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.deliveriesManagement.id", "STICKER")).thenReturn("002");

        Mockito.when(cypherTool.decrypt(CARD_RELATED_CONTRACT_ENCRYPT, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0)).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
        Mockito.when(cypherTool.decrypt(CARD_RELATED_CONTRACT_ENCRYPT, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V02)).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
    }

    @Test
    public void mapInFullMPW1Test() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId(HOME);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getParticipantId());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());
        assertNotNull(result.getCard().getSupportContractType());
        assertNotNull(result.getCard().getDestinationIdHome());
        assertNull(result.getCard().getDestinationIdBranch());
        assertNotNull(result.getCard().getDeliveriesManagementServiceTypeIdStatement());
        assertNotNull(result.getCard().getDeliveriesManagementContactDetailIdStatement());
        assertNotNull(result.getCard().getDeliveriesManagementAddressIdStatement());
        assertNotNull(result.getCard().getContractingBusinessAgentId());
        assertNotNull(result.getCard().getContractingBusinessAgentOriginId());
        assertNotNull(result.getCard().getMarketBusinessAgentId());
        assertNotNull(result.getCard().getImages().get(0).getId());
        assertNotNull(result.getCard().getOfferId());
        assertNotNull(result.getCard().getManagementBranchId());
        assertNotNull(result.getCard().getContractingBranchId());

        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());
        assertNull(result.getCard().getNumber());

        assertEquals(input.getParticipants().get(1).getParticipantId(), result.getCard().getParticipantId());
        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getDelivery().getAddress().getId(), result.getCard().getDestinationIdHome());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
        assertEquals("F", result.getCard().getSupportContractType());
        assertEquals("001", result.getCard().getDeliveriesManagementServiceTypeIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getContactDetail().getId(), result.getCard().getDeliveriesManagementContactDetailIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getAddress().getId(), result.getCard().getDeliveriesManagementAddressIdStatement());
        assertEquals(input.getContractingBusinessAgent().getId(), result.getCard().getContractingBusinessAgentId());
        assertEquals(input.getContractingBusinessAgent().getOriginId(), result.getCard().getContractingBusinessAgentOriginId());
        assertEquals(input.getMarketBusinessAgent().getId(), result.getCard().getMarketBusinessAgentId());
        assertEquals(input.getImage().getId(), result.getCard().getImages().get(0).getId());
        assertEquals(input.getOfferId(), result.getCard().getOfferId());
        assertEquals(input.getManagementBranch().getId(), result.getCard().getManagementBranchId());
        assertEquals(input.getContractingBranch().getId(), result.getCard().getContractingBranchId());
        assertEquals("002", result.getCard().getDeliveryAddressConstant());
    }

    @Test
    public void mapInMPW1WithoutContactDetailTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId(HOME);
        input.getDeliveriesManagement().get(0).setContactDetail(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard().getDeliveriesManagementServiceTypeIdStatement());
        assertNull(result.getCard().getDeliveriesManagementContactDetailIdStatement());
        assertNotNull(result.getCard().getDeliveriesManagementAddressIdStatement());

        assertEquals("001", result.getCard().getDeliveriesManagementServiceTypeIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getAddress().getId(), result.getCard().getDeliveriesManagementAddressIdStatement());
    }

    @Test
    public void mapInMPW1WithoutAddressTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId(HOME);
        input.getDeliveriesManagement().get(0).setAddress(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard().getDeliveriesManagementServiceTypeIdStatement());
        assertNotNull(result.getCard().getDeliveriesManagementContactDetailIdStatement());
        assertNull(result.getCard().getDeliveriesManagementAddressIdStatement());

        assertEquals("001", result.getCard().getDeliveriesManagementServiceTypeIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getContactDetail().getId(), result.getCard().getDeliveriesManagementContactDetailIdStatement());
    }

    @Test
    public void mapInMPW1DeliveriesManagementStickerTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId(HOME);
        input.getDeliveriesManagement().get(0).getServiceType().setId(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);

        assertEquals("002", result.getCard().getDeliveriesManagementServiceTypeIdSticker());
        assertEquals(input.getDeliveriesManagement().get(1).getAddress().getId(), result.getCard().getDeliveriesManagementAddressIdSticker());

    }

    @Test
    public void mapInMPW1DeliveriesManagementSMSCodeTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId(HOME);
        input.getDeliveriesManagement().get(0).getServiceType().setId(null);
        input.getDeliveriesManagement().get(1).getServiceType().setId(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);

        assertEquals(input.getDeliveriesManagement().get(2).getContactDetail().getId(), result.getCard().getDeliveriesManagementContactDetailIdSmsCode());
        assertEquals("S", result.getCard().getDeliveryManagementConstant());

    }

    @Test
    public void mapInFullMPW1BranchTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNull(result.getCard().getDestinationIdHome());
        assertNotNull(result.getCard().getDestinationIdBranch());

        assertEquals(input.getDeliveriesManagement().get(0).getContactDetail().getId(), result.getCard().getDeliveriesManagementContactDetailIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getAddress().getId(), result.getCard().getDestinationIdBranch());
    }

    @Test
    public void mapInMPRTFullTest() throws IOException {
        Card input = entityMock.getCardV0();
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getNumber());
        assertNotNull(result.getCard().getParticipantId());
        assertNotNull(result.getCard().getDeliveriesManagementContactDetailIdSmsCode());

        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getNumber(), result.getCard().getNumber());
        assertEquals(input.getParticipants().get(0).getParticipantId(), result.getCard().getParticipantId());
        assertEquals(input.getDeliveriesManagement().get(2).getContactDetail().getId(), result.getCard().getDeliveriesManagementContactDetailIdSmsCode());
    }

    @Test
    public void mapInMPRTWithRenewalDueToDamageTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).getRelationType().setId("RENEWAL_DUE_TO_DAMAGE");
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getRelatedContractIdRenewalDueTo());
        assertNotNull(result.getCard().getNumber());
        assertNotNull(result.getCard().getParticipantId());
        assertNotNull(result.getCard().getDeliveriesManagementContactDetailIdSmsCode());

        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getCard().getRelatedContractIdRenewalDueTo());
        assertEquals(input.getNumber(), result.getCard().getNumber());
        assertEquals(input.getParticipants().get(0).getParticipantId(), result.getCard().getParticipantId());
        assertEquals(input.getDeliveriesManagement().get(2).getContactDetail().getId(), result.getCard().getDeliveriesManagementContactDetailIdSmsCode());
    }


    @Test
    public void mapInWithoutSupporContractTypeTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setSupportContractType(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNull(result.getCard().getSupportContractType());
    }

    @Test
    public void mapIntWithoutParticipants() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setParticipants(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNull(result.getCard().getParticipantId());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapIntWithoutParticipantId() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getParticipants().get(0).setParticipantId(null);
        input.getParticipants().get(1).setParticipantId(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNull(result.getCard().getParticipantId());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutCardTypeIdTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getCardType().setId(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutPhysicalSupportIdTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getPhysicalSupport().setId(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(new ArrayList<>());
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractRelationTypeNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).setRelationType(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());
        assertNotNull(result.getCard().getRelatedContractIdExpedition());

        assertNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());

        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getCard().getRelatedContractIdExpedition());
        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutRelatedContractRelationTypeIdExpeditionDueTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).getRelationType().setId("EXPEDITION_DUE_TO_ADDITIONAL");
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNotNull(result.getCard().getRelatedContractIdExpedition());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getRelatedContractIdLinkedWith());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getCard().getRelatedContractIdExpedition());
        assertEquals(STATEMENT_TYPE, result.getCard().getRelatedContractIdExpeditionConstant());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractRelationTypeIdOtherTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).getRelationType().setId("OTHER");
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());
        assertNotNull(result.getCard().getRelatedContractIdExpedition());

        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdLinkedWith());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getCard().getRelatedContractIdExpedition());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutDeliverDestinationIdBranchTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getDelivery().getDestination().setId(BRANCH);
        input.setNumber(null);

        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);

        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());
        assertNotNull(result.getCard().getDestinationIdBranch());

        assertNull(result.getCard().getDestinationIdHome());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getDelivery().getAddress().getId(), result.getCard().getDestinationIdBranch());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());

    }

    @Test
    public void mapInWithoutPaymentMethodEndDateTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getPaymentMethod().setEndDay(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getPaymentMethodEndDay());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());

    }

    @Test
    public void mapInWithoutCutoffDayTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setCutOffDay(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());
        assertNotNull(result.getCard().getPaymentMethodEndDay());

        assertNull(result.getCard().getCutOffDay());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
    }


    @Test
    public void mapInWithoutMembershipNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setMemberships(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());

        assertNull(result.getCard().getMembershipsNumber());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutMembershipEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.setMemberships(new ArrayList<>());
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());

        assertNull(result.getCard().getMembershipsNumber());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutGrantedCreditTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setGrantedCredits(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getGrantedCreditsAmount());
        assertNull(result.getCard().getGrantedCreditsCurrency());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getDelivery().getDestination().getId(), result.getCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutDestinationTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().setDestination(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getCardTypeId());
        assertNotNull(result.getCard().getPhysicalSupportId());
        assertNotNull(result.getCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getCard().getTitleId());
        assertNotNull(result.getCard().getCutOffDay());
        assertNotNull(result.getCard().getPaymentMethodEndDay());
        assertNotNull(result.getCard().getStatementType());
        assertNotNull(result.getCard().getGrantedCreditsAmount());
        assertNotNull(result.getCard().getGrantedCreditsCurrency());
        assertNotNull(result.getCard().getMembershipsNumber());

        assertNull(result.getCard().getDestinationIdHome());
        assertNull(result.getCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getCard().getRelatedContractIdExpedition());
        assertNull(result.getCard().getDestinationIdBranch());

        assertEquals("D", result.getCard().getCardTypeId());
        assertEquals("P", result.getCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getCard().getTitleId());
        assertEquals("S", result.getCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getCard().getGrantedCreditsCurrency());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getCard().getCutOffDay());
    }


    @Test
    public void mapInCardEmptyTest() {
        InputCreateCard result = mapper.mapIn(new Card());
        assertNotNull(result);
        assertNotNull(result.getCard());
    }

    @Test
    public void mapOutEmptyTest() {
        CardData result = mapper.mapOut(new Card());
        assertNotNull(result);
        assertNotNull(result.getData());

    }

    @Test
    public void mapOutNullTest() {
        CardData result = mapper.mapOut(null);
        assertNull(result);
    }

    @Test
    public void mapInMPRTRelatedContractNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTRelatedContractEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(new ArrayList<>());
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTRelatedContractRelationTypeNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).setRelationType(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTDeliveriesManagementNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setDeliveriesManagement(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTDeliveriesManagementEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setDeliveriesManagement(new ArrayList<>());
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTDeliveriesManagementServiceTypeIdNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getDeliveriesManagement().get(0).setServiceType(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

}
