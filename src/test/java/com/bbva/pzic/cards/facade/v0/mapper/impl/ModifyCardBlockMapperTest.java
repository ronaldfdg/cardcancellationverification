package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.Block;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK;
import static org.junit.Assert.*;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyCardBlockMapperTest {

    @InjectMocks
    private ModifyCardBlockMapper modifyCardBlockMapper;

    @Mock
    private MapperUtils mapperUtils;

    @Mock
    private EnumMapper enumMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void init() {
        Mockito.when(cypherTool.decrypt(CARD_ID, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK))
                .thenReturn(CARD_CARDTYPE_ID_KEY_TESTED);
        Mockito.when(enumMapper.getBackendValue("cards.block.blockId", "CANCELACION X FRAUDE"))
                .thenReturn(BLOCK_ID);
        Mockito.when(mapperUtils.convertBooleanToString(Boolean.TRUE))
                .thenReturn("S");
    }

    @Test
    public void testMapInput() {
        final Block block = mock.buildBlock();
        final DTOIntBlock dtoIntBlock = modifyCardBlockMapper.mapInput(CARD_ID, "CANCELACION X FRAUDE", block);

        assertNotNull(dtoIntBlock);
        assertNotNull(dtoIntBlock.getCard());
        assertNotNull(dtoIntBlock.getCard().getCardId());
        assertNotNull(dtoIntBlock.getBlockId());
        assertNotNull(dtoIntBlock.getReasonId());
        assertNotNull(dtoIntBlock.getIsActive());
        assertNotNull(dtoIntBlock.getIsReissued());
        assertNotNull(dtoIntBlock.getAdditionalInformation());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntBlock.getCard().getCardId());
        assertEquals(BLOCK_ID, dtoIntBlock.getBlockId());
        assertEquals(block.getReason().getId(), dtoIntBlock.getReasonId());
        assertEquals("S", dtoIntBlock.getIsActive());
        assertEquals("S", dtoIntBlock.getIsReissued());
        assertEquals(block.getAdditionalInformation(), dtoIntBlock.getAdditionalInformation());
    }

    @Test
    public void testMapInputWithoutIsActive() {
        final Block block = mock.buildBlock();
        block.setIsActive(null);
        final DTOIntBlock dtoIntBlock = modifyCardBlockMapper.mapInput(CARD_ID, "CANCELACION X FRAUDE", block);

        assertNotNull(dtoIntBlock);
        assertNotNull(dtoIntBlock.getCard());
        assertNotNull(dtoIntBlock.getCard().getCardId());
        assertNotNull(dtoIntBlock.getBlockId());
        assertNotNull(dtoIntBlock.getReasonId());
        assertNull(dtoIntBlock.getIsActive());
        assertNotNull(dtoIntBlock.getIsReissued());
        assertNotNull(dtoIntBlock.getAdditionalInformation());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntBlock.getCard().getCardId());
        assertEquals(BLOCK_ID, dtoIntBlock.getBlockId());
        assertEquals(block.getReason().getId(), dtoIntBlock.getReasonId());
        assertEquals("S", dtoIntBlock.getIsReissued());
        assertEquals(block.getAdditionalInformation(), dtoIntBlock.getAdditionalInformation());
    }

    @Test
    public void testMapInputWithoutIsReissued() {
        final Block block = mock.buildBlock();
        block.setIsReissued(null);
        final DTOIntBlock dtoIntBlock = modifyCardBlockMapper.mapInput(CARD_ID, "CANCELACION X FRAUDE", block);

        assertNotNull(dtoIntBlock);
        assertNotNull(dtoIntBlock.getCard());
        assertNotNull(dtoIntBlock.getCard().getCardId());
        assertNotNull(dtoIntBlock.getBlockId());
        assertNotNull(dtoIntBlock.getReasonId());
        assertNotNull(dtoIntBlock.getIsActive());
        assertNull(dtoIntBlock.getIsReissued());
        assertNotNull(dtoIntBlock.getAdditionalInformation());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntBlock.getCard().getCardId());
        assertEquals(BLOCK_ID, dtoIntBlock.getBlockId());
        assertEquals(block.getReason().getId(), dtoIntBlock.getReasonId());
        assertEquals("S", dtoIntBlock.getIsActive());
        assertEquals(block.getAdditionalInformation(), dtoIntBlock.getAdditionalInformation());
    }

    @Test
    public void testMapInputWithoutAdditionalInformation() {
        final Block block = mock.buildBlock();
        block.setAdditionalInformation(null);
        final DTOIntBlock dtoIntBlock = modifyCardBlockMapper.mapInput(CARD_ID, "CANCELACION X FRAUDE", block);

        assertNotNull(dtoIntBlock);
        assertNotNull(dtoIntBlock.getCard());
        assertNotNull(dtoIntBlock.getCard().getCardId());
        assertNotNull(dtoIntBlock.getBlockId());
        assertNotNull(dtoIntBlock.getReasonId());
        assertNotNull(dtoIntBlock.getIsActive());
        assertNotNull(dtoIntBlock.getIsReissued());
        assertNull(dtoIntBlock.getAdditionalInformation());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntBlock.getCard().getCardId());
        assertEquals(BLOCK_ID, dtoIntBlock.getBlockId());
        assertEquals(block.getReason().getId(), dtoIntBlock.getReasonId());
        assertEquals("S", dtoIntBlock.getIsActive());
        assertEquals("S", dtoIntBlock.getIsReissued());
    }

    @Test
    public void testMapInputWithoutReasonId() {
        final Block block = mock.buildBlock();
        block.getReason().setId(null);
        final DTOIntBlock dtoIntBlock = modifyCardBlockMapper.mapInput(CARD_ID, "CANCELACION X FRAUDE", block);

        assertNotNull(dtoIntBlock);
        assertNotNull(dtoIntBlock.getCard());
        assertNotNull(dtoIntBlock.getCard().getCardId());
        assertNotNull(dtoIntBlock.getBlockId());
        assertNull(dtoIntBlock.getReasonId());
        assertNotNull(dtoIntBlock.getIsActive());
        assertNotNull(dtoIntBlock.getIsReissued());
        assertNotNull(dtoIntBlock.getAdditionalInformation());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntBlock.getCard().getCardId());
        assertEquals(BLOCK_ID, dtoIntBlock.getBlockId());
        assertEquals("S", dtoIntBlock.getIsActive());
        assertEquals("S", dtoIntBlock.getIsReissued());
        assertEquals(block.getAdditionalInformation(), dtoIntBlock.getAdditionalInformation());
    }

    @Test
    public void testMapInputWithoutReasonNull() {
        final Block block = mock.buildBlock();
        block.setReason(null);
        final DTOIntBlock dtoIntBlock = modifyCardBlockMapper.mapInput(CARD_ID, "CANCELACION X FRAUDE", block);

        assertNotNull(dtoIntBlock);
        assertNotNull(dtoIntBlock.getCard());
        assertNotNull(dtoIntBlock.getCard().getCardId());
        assertNotNull(dtoIntBlock.getBlockId());
        assertNull(dtoIntBlock.getReasonId());
        assertNotNull(dtoIntBlock.getIsActive());
        assertNotNull(dtoIntBlock.getIsReissued());
        assertNotNull(dtoIntBlock.getAdditionalInformation());

        assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, dtoIntBlock.getCard().getCardId());
        assertEquals(BLOCK_ID, dtoIntBlock.getBlockId());
        assertEquals("S", dtoIntBlock.getIsActive());
        assertEquals("S", dtoIntBlock.getIsReissued());
        assertEquals(block.getAdditionalInformation(), dtoIntBlock.getAdditionalInformation());
    }
}
