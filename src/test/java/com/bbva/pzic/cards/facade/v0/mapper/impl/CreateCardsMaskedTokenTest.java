package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardsMaskedTokenTest {

    @InjectMocks
    private CreateCardsMaskedToken mapper;
    @Mock
    private AbstractCypherTool cypherTool;
    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void setUp() {
        mapper.init();
    }

    @Test
    public void mapInFullTest() throws ParseException {
        MaskedToken maskedToken = entityMock.buildMaskedToken();

        when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARD_ID)).thenReturn(EntityMock.CARD_ID);
        InputCreateCardsMaskedToken result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED, maskedToken);

        assertNotNull(result.getCardId());
        assertNotNull(result.getExpirationDate());

        assertEquals(EntityMock.CARD_ID, result.getCardId());
        assertEquals("122018", result.getExpirationDate());
    }

    @Test
    public void mapOutFull() throws ParseException {
        ServiceResponse<MaskedToken> result = mapper.mapOut(entityMock.buildMaskedToken());

        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<MaskedToken> result = mapper.mapOut(null);

        assertNull(result);
    }
}