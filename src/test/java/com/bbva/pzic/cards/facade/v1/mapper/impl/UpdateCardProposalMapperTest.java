package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import com.bbva.pzic.cards.facade.v1.mapper.common.AbstractCardProposalMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.CardProposalDateTimeMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.CardProposalTimeMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public class UpdateCardProposalMapperTest {

    private UpdateCardProposalMapper mapper;

    private AbstractCardProposalMapper abstractCardProposalMapper = new CardProposalDateTimeMapper();

    @Before
    public void setUp() {
        mapper = new UpdateCardProposalMapper();
        mapper.setAddressMapper(abstractCardProposalMapper);
    }

    @Test
    public void mapInFullTest() throws IOException {
        Proposal input = EntityMock.getInstance().buildProposalv1();
        input.getContactability().getScheduleTimes().setStartTime("2020-11-05T16:44:44.235-0500");
        input.getContactability().getScheduleTimes().setEndTime("2020-11-05T16:44:44.235-0500");
        InputUpdateCardProposal result = mapper.mapIn(EntityMock.PROPOSAL_ID, input);

        assertNotNull(result);
        assertNotNull(result.getProposal().getContact());
        assertNotNull(result.getProposal().getContact().getContactType());
        assertNotNull(result.getProposal().getContact().getContact());
        assertNotNull(result.getProposal().getContact().getContact().getContactDetailType());
        assertNotNull(result.getProposal().getContact().getContact().getNumber());
        assertNotNull(result.getProposal().getContact().getContact().getPhoneCompany());
        assertNotNull(result.getProposal().getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getProposal().getMembership());
        assertNotNull(result.getProposal().getMembership().getId());
        assertNotNull(result.getProposal().getAdditionalProducts());
        assertEquals(1, result.getProposal().getAdditionalProducts().size());
        assertNotNull(result.getProposal().getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getProposal().getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getProposal().getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getProposal().getFees());
        assertNotNull(result.getProposal().getFees().getItemizeFees());
        assertEquals(1, result.getProposal().getFees().getItemizeFees().size());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getProposal().getRates());
        assertNotNull(result.getProposal().getRates().getItemizeRates());
        assertEquals(1, result.getProposal().getRates().getItemizeRates().size());
        assertNotNull(result.getProposal().getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getProposal().getRates().getItemizeRates().get(0).getCalculationDate());
        assertNotNull(result.getProposal().getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getProposal().getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getProposal().getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getProposal().getGrantedCredits());
        assertEquals(1, result.getProposal().getGrantedCredits().size());
        assertNotNull(result.getProposal().getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getProposal().getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getProposal().getPaymentMethod());
        assertNotNull(result.getProposal().getPaymentMethod().getId());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getId());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertNotNull(result.getProposal().getDeliveries());
        assertEquals(1, result.getProposal().getDeliveries().size());
        assertNotNull(result.getProposal().getDeliveries().get(0).getServiceTypeId());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact().getContactDetailType());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact().getContactAddress());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents());
        assertEquals(1, result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(2, result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(1));
        assertNull(result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getProposal().getPhysicalSupport());
        assertNotNull(result.getProposal().getPhysicalSupport().getId());
        assertNotNull(result.getProposal().getProduct());
        assertNotNull(result.getProposal().getProduct().getId());
        assertNotNull(result.getProposal().getProduct().getSubproduct());
        assertNotNull(result.getProposal().getProduct().getSubproduct().getId());
        assertNotNull(result.getProposal().getCardType());
        assertNotNull(result.getProposal().getCardType().getId());
        assertNotNull(result.getProposal().getContactAbility());
        assertNotNull(result.getProposal().getContactAbility().getReason());
        assertNotNull(result.getProposal().getContactAbility().getScheduletimes());
        assertNotNull(result.getProposal().getContactAbility().getScheduletimes().getStarttime());
        assertNotNull(result.getProposal().getContactAbility().getScheduletimes().getEndtime());
        assertNotNull(result.getProposal().getImage());
        assertNotNull(result.getProposalId());

        assertEquals(input.getContact().getContactType(), result.getProposal().getContact().getContactType());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getProposal().getContact().getContact().getContactDetailType());
        assertEquals(input.getContact().getContact().getNumber(), result.getProposal().getContact().getContact().getNumber());
        assertEquals(input.getContact().getContact().getPhoneCompany().getId(), result.getProposal().getContact().getContact().getPhoneCompany().getId());
        assertEquals(input.getMembership().getId(), result.getProposal().getMembership().getId());
        assertEquals(input.getAdditionalProducts().get(0).getProductType(), result.getProposal().getAdditionalProducts().get(0).getProductType());
        assertEquals(input.getAdditionalProducts().get(0).getAmount(), result.getProposal().getAdditionalProducts().get(0).getAmount());
        assertEquals(input.getAdditionalProducts().get(0).getCurrency(), result.getProposal().getAdditionalProducts().get(0).getCurrency());
        assertEquals(input.getFees().getItemizeFees().get(0).getFeeType(), result.getProposal().getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType(), result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getRates().getItemizeRates().get(0).getRateType(), result.getProposal().getRates().getItemizeRates().get(0).getRateType());
        assertEquals(input.getRates().getItemizeRates().get(0).getCalculationDate(), result.getProposal().getRates().getItemizeRates().get(0).getCalculationDate());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage(), result.getProposal().getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType(), result.getProposal().getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getProposal().getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getProposal().getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getPaymentMethod().getId(), result.getProposal().getPaymentMethod().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getId(), result.getProposal().getPaymentMethod().getFrequency().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay(), result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getCutOffDay());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay(), result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertEquals(input.getDeliveries().get(0).getServiceType().getId(), result.getProposal().getDeliveries().get(0).getServiceTypeId());
        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getProposal().getDeliveries().get(0).getContact().getContactType());
        assertEquals(input.getDeliveries().get(0).getContact().getContact().getContactDetailType(), result.getProposal().getDeliveries().get(0).getContact().getContactDetailType());
        assertEquals(input.getDeliveries().get(0).getContact().getContact().getAddress(), result.getProposal().getDeliveries().get(0).getContact().getContactAddress());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getProposal().getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getCode(), result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getName(), result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0), result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(1), result.getProposal().getDeliveries().get(0).getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(1));
        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getProposal().getDeliveries().get(0).getDestination().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getBranch().getId(), result.getProposal().getDeliveries().get(0).getDestination().getBranch().getId());
        assertEquals(input.getPhysicalSupport().getId(), result.getProposal().getPhysicalSupport().getId());
        assertEquals(input.getCardType().getId(), result.getProposal().getCardType().getId());
        assertEquals(input.getProduct().getId(), result.getProposal().getProduct().getId());
        assertEquals(input.getProduct().getSubproduct().getId(), result.getProposal().getProduct().getSubproduct().getId());
        assertEquals(input.getContactability().getReason(), result.getProposal().getContactAbility().getReason());
        assertEquals(EntityMock.PROPOSAL_ID, result.getProposalId());
    }

    @Test
    public void mapInEmptyTest() {
        InputUpdateCardProposal result = mapper.mapIn(EntityMock.PROPOSAL_ID, new Proposal());

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNull(result.getProposal().getMembership());
        assertNull(result.getProposal().getAdditionalProducts());
        assertNull(result.getProposal().getFees());
        assertNull(result.getProposal().getRates());
        assertNull(result.getProposal().getContact());
        assertNull(result.getProposal().getGrantedCredits());
        assertNull(result.getProposal().getPaymentMethod());
        assertNull(result.getProposal().getDeliveries());
        assertNull(result.getProposal().getPhysicalSupport());
        assertNull(result.getProposal().getProduct());
        assertNull(result.getProposal().getCardType());
        assertNull(result.getProposal().getImage());
        assertNull(result.getProposal().getContactAbility());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<Proposal> result = mapper.mapOut(new Proposal());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<Proposal> result = mapper.mapOut(null);

        assertNull(result);
    }
}
