package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContracts;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardRelatedContractsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ListCardRelatedContractsMapperTest {

    @InjectMocks
    private IListCardRelatedContractsMapper mapper = new ListCardRelatedContractsMapper();

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.CARDID)).thenReturn(CARD_ID);
    }

    @Test
    public void testMapInput() {

        final DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria = mapper.mapInput(EntityMock.CARD_ENCRYPT_ID);

        assertNotNull(dtoIntRelatedContractsSearchCriteria);
        assertNotNull(dtoIntRelatedContractsSearchCriteria.getCardId());
        assertEquals(EntityMock.CARD_ID, dtoIntRelatedContractsSearchCriteria.getCardId());
    }

    @Test
    public void testMapOutput() throws IOException {

        List<DTOIntRelatedContracts> dtoIntRelatedContracts = entityMock.buildDTOIntRelatedContracts();

        ServiceResponse<List<RelatedContracts>> result = mapper.mapOutput(dtoIntRelatedContracts);
        final List<RelatedContracts> relatedContracts = result.getData();

        assertNotNull(relatedContracts);
        assertFalse(relatedContracts.isEmpty());

        for (int i = 0; i < relatedContracts.size(); i++) {
            DTOIntRelatedContracts dtoIntRelatedContract = dtoIntRelatedContracts.get(i);
            RelatedContracts relatedContract = relatedContracts.get(i);

            assertNotNull(relatedContract);
            assertNotNull(relatedContract.getRelatedContractId());
            assertEquals(dtoIntRelatedContract.getRelatedContractId(), relatedContract.getRelatedContractId());
            assertNotNull(relatedContract.getContractId());
            assertEquals(dtoIntRelatedContract.getContractId(), relatedContract.getContractId());
            assertNotNull(relatedContract.getNumber());
            assertEquals(dtoIntRelatedContract.getNumber(), relatedContract.getNumber());
            assertNotNull(relatedContract.getNumberType());
            assertNotNull(relatedContract.getNumberType().getId());
            assertEquals(dtoIntRelatedContract.getNumberType().getId(), relatedContract.getNumberType().getId());
            assertNotNull(relatedContract.getNumberType().getName());
            assertEquals(dtoIntRelatedContract.getNumberType().getName(), relatedContract.getNumberType().getName());
            assertNotNull(relatedContract.getProduct());
            assertNotNull(relatedContract.getProduct().getId());
            assertEquals(dtoIntRelatedContract.getProduct().getId(), relatedContract.getProduct().getId());
            assertNotNull(relatedContract.getProduct().getName());
            assertEquals(dtoIntRelatedContract.getProduct().getName(), relatedContract.getProduct().getName());

        }

    }
}
