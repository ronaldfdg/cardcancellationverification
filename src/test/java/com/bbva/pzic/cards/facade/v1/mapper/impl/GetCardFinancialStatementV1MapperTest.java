package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.facade.v1.dto.FinancialStatement;
import org.junit.Test;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.FINANCIAL_STATEMENT_ID;
import static org.junit.Assert.*;

public class GetCardFinancialStatementV1MapperTest {

    private GetCardFinancialStatementV1Mapper mapper = new GetCardFinancialStatementV1Mapper();

    @Test
    public void mapInFullTest() {
        InputGetCardFinancialStatement result = mapper.mapIn(CARD_ID, FINANCIAL_STATEMENT_ID);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getFinancialStatementId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(FINANCIAL_STATEMENT_ID, result.getFinancialStatementId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<FinancialStatement> result = mapper.mapOut(new FinancialStatement());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<FinancialStatement> result = mapper.mapOut(null);

        assertNull(result);
    }
}
