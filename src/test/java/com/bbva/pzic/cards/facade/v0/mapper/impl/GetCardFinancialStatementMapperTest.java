package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementDetail;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.filenet.Contenido;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

/**
 * Created on 27/07/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCardFinancialStatementMapperTest {

    @InjectMocks
    private GetCardFinancialStatementMapper mapper;
    @Mock
    private CypherTool cypherTool;
    @Mock
    private Translator translator;
    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        Mockito.when(translator.translate("servicing.cards.contractId")).thenReturn(EntityMock.ID_CONTRATO);
        Mockito.when(translator.translate("servicing.cards.groupId")).thenReturn(EntityMock.ID_GRUPO);
        Mockito.when(serviceInvocationContext.getUser()).thenReturn(EntityMock.CLIENT_ID);
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT)).thenReturn(CARD_ID);
    }

    @Test
    public void mapInFullTest() {
        final InputGetCardFinancialStatement result = mapper.mapIn(CARD_ENCRYPT_ID, FINANCIAL_STATEMENT_ID, MEDIA_TYPE_PDF);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getFinancialStatementId());
        assertNotNull(result.getContentType());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(FINANCIAL_STATEMENT_ID, result.getFinancialStatementId());
        assertEquals(MEDIA_TYPE_PDF, result.getContentType());
    }

    @Test
    public void mapInWithEmptyContentTypesTest() {
        final InputGetCardFinancialStatement result = mapper.mapIn(CARD_ENCRYPT_ID, FINANCIAL_STATEMENT_ID, null);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getFinancialStatementId());
        assertNull(result.getContentType());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(FINANCIAL_STATEMENT_ID, result.getFinancialStatementId());
    }

    @Test
    public void testMapOutGetCardStatementNaturalLifeMiles() throws IOException {
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNotNull(result.getProgramId());
        assertNotNull(result.getProgramTypeId());
        assertNotNull(result.getProgramTypeName());
        assertNotNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNotNull(result.getProgramBonus());
        assertEquals(detail.getProgram().getMonthPoints().toString(), result.getProgramCurrentLoyaltyUnitsBalance());
        assertEquals(detail.getProgram().getBonus().toString(), result.getProgramBonus());

        assertEquals(dto.getRowCardStatement(), result.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNotNull(documentResult.getListaClientes());

        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
        assertEquals(EntityMock.CLIENT_ID, documentResult.getListaClientes().get(0).getCodigoCentral());
    }

    @Test
    public void testMapOutGetCardStatementNaturalPuntosVida() throws IOException {
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        dto.getDetail().getProgram().setLifeMilesNumber(null);

        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNull(result.getProgramId());
        assertNotNull(result.getProgramTypeId());
        assertNotNull(result.getProgramTypeName());
        assertNotNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNotNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNull(result.getProgramBonus());
        assertEquals(detail.getProgram().getMonthPoints().toString(), result.getProgramCurrentLoyaltyUnitsBalance());
        assertEquals(detail.getProgram().getTotalPoints().toString(), result.getProgramLifetimeAccumulatedLoyaltyUnits());

        assertEquals(dto.getRowCardStatement(), result.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNotNull(documentResult.getListaClientes());

        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
        assertEquals(EntityMock.CLIENT_ID, documentResult.getListaClientes().get(0).getCodigoCentral());
    }

    @Test
    public void testMapOutGetCardStatementWithoutUser() throws IOException {
        Mockito.when(serviceInvocationContext.getUser()).thenReturn(null);
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNotNull(result.getProgramId());
        assertNotNull(result.getProgramTypeId());
        assertNotNull(result.getProgramTypeName());
        assertNotNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNotNull(result.getProgramBonus());
        assertEquals(detail.getProgram().getMonthPoints().toString(), result.getProgramCurrentLoyaltyUnitsBalance());
        assertEquals(detail.getProgram().getBonus().toString(), result.getProgramBonus());

        assertEquals(dto.getRowCardStatement(), result.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNull(documentResult.getListaClientes());

        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
    }

    @Test
    public void testMapOutGetCardStatementEnterprise() throws IOException {
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        dto.getDetail().setParticipantType("E");
        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNull(result.getProgramId());
        assertNull(result.getProgramTypeId());
        assertNull(result.getProgramTypeName());
        assertNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNull(result.getProgramBonus());

        assertEquals(dto.getRowCardStatement(), result.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getNumeroContrato());
        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNotNull(documentResult.getListaClientes());

        assertEquals(dto.getDetail().getAccountNumberPEN(), documentResult.getNumeroContrato());
        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
        assertEquals(EntityMock.CLIENT_ID, documentResult.getListaClientes().get(0).getCodigoCentral());
    }

    private void genericAsserts(Contenido result, DTOIntCardStatementDetail detail) {
        assertNotNull(result.getCardId());
        assertNotNull(result.getCurrencyId());
        assertNotNull(result.getChargedAccount1Id());
        assertNotNull(result.getChargedAccount1Currency());
        assertNotNull(result.getChargedAccount2Id());
        assertNotNull(result.getChargedAccount2Currency());
        assertNotNull(result.getParticipantType());
        assertNotNull(result.getFormatsPan());
        assertNotNull(result.getProductName());
        assertNotNull(result.getProductCode());
        assertNotNull(result.getBranchName());
        assertNotNull(result.getParticipant1Name());
        assertNotNull(result.getParticipant2Name());
        assertNotNull(result.getPayTypeName());
        assertNotNull(result.getParticipant1AddresessIsMainAddress());
        assertNotNull(result.getParticipant1AddresessName());
        assertNotNull(result.getParticipant1AddresessStreetTypeName());
        assertNotNull(result.getParticipant1AddresessState());
        assertNotNull(result.getParticipant1AddresessUbigeo());
        assertNotNull(result.getCreditLimitValueAmount());
        assertNotNull(result.getCreditLimitValueCurrency());
        assertNotNull(result.getLimitsDisposedBalanceAmount());
        assertNotNull(result.getLimitsDisposedBalanceCurrency());
        assertNotNull(result.getLimitsAvailableBalanceAmount());
        assertNotNull(result.getLimitsAvailableBalanceCurrency());
        assertNotNull(result.getLimitsFinancingDisposedBalanceAmount());
        assertNotNull(result.getLimitsFinancingDisposedBalanceCurrency());
        assertNotNull(result.getInterestRates1Percentage());
        assertNotNull(result.getInterestRates2Percentage());
        assertNotNull(result.getInterestRates3Percentage());
        assertNotNull(result.getInterestRates4Percentage());
        assertNotNull(result.getInterestRates5Percentage());
        assertNotNull(result.getInterestRates6Percentage());
        assertNotNull(result.getInterestRates7Percentage());
        assertNotNull(result.getInterestRates8Percentage());
        assertNotNull(result.getDisposedBalanceLocalCurrencyCurrency());
        assertNotNull(result.getDisposedBalanceLocalCurrencyAmount());
        assertNotNull(result.getDisposedBalanceCurrency());
        assertNotNull(result.getDisposedBalanceAmount());
        assertNotNull(result.getTotalTransactions());
        assertNotNull(result.getOutstandingBalanceLocalCurrencyCurrency());
        assertNotNull(result.getOutstandingBalanceLocalCurrencyAmount());
        assertNotNull(result.getMinimunPaymentMinimumCapitalLocalCurrencyCurrency());
        assertNotNull(result.getMinimunPaymentMinimumCapitalLocalCurrencyAmount());
        assertNotNull(result.getInterestsBalanceLocalCurrencyCurrency());
        assertNotNull(result.getInterestsBalanceLocalCurrencyAmount());
        assertNotNull(result.getFeesBalanceLocalCurrencyCurrency());
        assertNotNull(result.getFeesBalanceLocalCurrencyAmount());
        assertNotNull(result.getFinancingTransactionLocalBalanceCurrency());
        assertNotNull(result.getFinancingTransactionLocalBalanceAmount());
        assertNotNull(result.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency());
        assertNotNull(result.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount());
        assertNotNull(result.getInvoiceAmountLocalCurrencyCurrency());
        assertNotNull(result.getInvoiceAmountLocalCurrencyAmount());
        assertNotNull(result.getOutstandingBalanceCurrency());
        assertNotNull(result.getOutstandingBalanceAmount());
        assertNotNull(result.getMinimunPaymentMinimumCapitalCurrencyCurrency());
        assertNotNull(result.getMinimunPaymentMinimumCapitalCurrencyAmount());
        assertNotNull(result.getInterestsBalanceCurrency());
        assertNotNull(result.getInterestsBalanceAmount());
        assertNotNull(result.getFeesBalanceCurrency());
        assertNotNull(result.getFeesBalanceAmount());
        assertNotNull(result.getFinancingTransactionBalanceCurrency());
        assertNotNull(result.getFinancingTransactionBalanceAmount());
        assertNotNull(result.getMinimumPaymentInvoiceMinimumAmountCurrency());
        assertNotNull(result.getMinimumPaymentInvoiceMinimumAmountAmount());
        assertNotNull(result.getInvoiceAmountCurrency());
        assertNotNull(result.getInvoiceAmountAmount());
        assertNotNull(result.getTotalDebtLocalCurrencyCurrency());
        assertNotNull(result.getTotalDebtLocalCurrencyAmount());
        assertNotNull(result.getTotalDebtCurrency());
        assertNotNull(result.getTotalDebtAmount());
        assertNotNull(result.getTotalMonthsAmortizationMinimum());
        assertNotNull(result.getTotalMonthsAmortizationMinimumFull());

        assertEquals(detail.getCardId(), result.getCardId());
        assertEquals(detail.getCurrency(), result.getCurrencyId());

        assertEquals(detail.getAccountNumberPEN(), result.getChargedAccount1Id());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getChargedAccount1Currency());

        assertEquals(detail.getAccountNumberUSD(), result.getChargedAccount2Id());
        assertEquals(DTOIntCardStatementDetail.USD, result.getChargedAccount2Currency());

        assertEquals(detail.getParticipantType(), result.getParticipantType());

        assertEquals(detail.getFormatsPan(), result.getFormatsPan());
        assertEquals(detail.getProductName(), result.getProductName());
        assertEquals(detail.getProductCode(), result.getProductCode());
        assertEquals(detail.getBranchName(), result.getBranchName());

        assertNotNull(result.getParticipant1RelationshipTypeName());
        assertNotNull(result.getParticipant1RelationshipOrder());
        assertEquals(detail.getParticipantNameFirst(), result.getParticipant1Name());

        assertNotNull(result.getParticipant2RelationshipTypeName());
        assertNotNull(result.getParticipant2RelationshipOrder());
        assertEquals(detail.getParticipantNameSecond(), result.getParticipant2Name());

        assertNotNull(result.getEndDate());
        assertEquals(detail.getPayType(), result.getPayTypeName());

        assertNotNull(result.getParticipant1AddresessIsMainAddress());
        assertEquals("TRUE", result.getParticipant1AddresessIsMainAddress());
        assertEquals(detail.getAddress().getName(), result.getParticipant1AddresessName());
        assertEquals(detail.getAddress().getStreetType(), result.getParticipant1AddresessStreetTypeName());
        assertEquals(detail.getAddress().getState(), result.getParticipant1AddresessState());
        assertEquals(detail.getAddress().getUbigeo(), result.getParticipant1AddresessUbigeo());

        assertEquals(detail.getCreditLimit().toString(), result.getCreditLimitValueAmount());
        assertEquals(detail.getCurrency(), result.getCreditLimitValueCurrency());
        assertEquals(detail.getLimits().getDisposedBalance().toString(), result.getLimitsDisposedBalanceAmount());
        assertEquals(detail.getCurrency(), result.getLimitsDisposedBalanceCurrency());
        assertEquals(detail.getLimits().getAvailableBalance().toString(), result.getLimitsAvailableBalanceAmount());
        assertEquals(detail.getCurrency(), result.getLimitsAvailableBalanceCurrency());
        assertEquals(detail.getLimits().getFinancingDisposedBalance().toString(), result.getLimitsFinancingDisposedBalanceAmount());
        assertEquals(detail.getCurrency(), result.getLimitsFinancingDisposedBalanceCurrency());

        assertNotNull(result.getPaymentDate());

        assertNotNull(result.getInterestRates1TypeId());
        assertNotNull(result.getInterestRates1TypeName());
        assertEquals(detail.getInterest().getPurchasePEN().toString(), result.getInterestRates1Percentage());

        assertNotNull(result.getInterestRates2TypeId());
        assertNotNull(result.getInterestRates2TypeName());
        assertEquals(detail.getInterest().getPurchaseUSD().toString(), result.getInterestRates2Percentage());

        assertNotNull(result.getInterestRates3TypeId());
        assertNotNull(result.getInterestRates3TypeName());
        assertEquals(detail.getInterest().getAdvancedPEN().toString(), result.getInterestRates3Percentage());

        assertNotNull(result.getInterestRates4TypeId());
        assertNotNull(result.getInterestRates4TypeName());
        assertEquals(detail.getInterest().getAdvancedUSD().toString(), result.getInterestRates4Percentage());

        assertNotNull(result.getInterestRates5TypeId());
        assertNotNull(result.getInterestRates5TypeName());
        assertEquals(detail.getInterest().getCountervailingPEN().toString(), result.getInterestRates5Percentage());

        assertNotNull(result.getInterestRates6TypeId());
        assertNotNull(result.getInterestRates6TypeName());
        assertEquals(detail.getInterest().getCountervailingUSD().toString(), result.getInterestRates6Percentage());

        assertNotNull(result.getInterestRates7TypeId());
        assertNotNull(result.getInterestRates7TypeName());
        assertEquals(detail.getInterest().getArrearsPEN().toString(), result.getInterestRates7Percentage());

        assertNotNull(result.getInterestRates8TypeId());
        assertNotNull(result.getInterestRates8TypeName());
        assertEquals(detail.getInterest().getArrearsUSD().toString(), result.getInterestRates8Percentage());

        assertEquals(DTOIntCardStatementDetail.PEN, result.getDisposedBalanceLocalCurrencyCurrency());
        assertEquals(detail.getDisposedBalanceLocalCurrency().getAmount().toString(), result.getDisposedBalanceLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getDisposedBalanceCurrency());
        assertEquals(detail.getDisposedBalance().getAmount().toString(), result.getDisposedBalanceAmount());

        assertEquals(detail.getTotalTransactions().toString(), result.getTotalTransactions());

        assertEquals(DTOIntCardStatementDetail.PEN, result.getOutstandingBalanceLocalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getOutstandingBalanceLocalCurrency().getAmount().toString(), result.getOutstandingBalanceLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getMinimunPaymentMinimumCapitalLocalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getMinimumCapitalLocalCurrency().getAmount().toString(), result.getMinimunPaymentMinimumCapitalLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getInterestsBalanceLocalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getInterestsBalanceLocalCurrency().getAmount().toString(), result.getInterestsBalanceLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getFeesBalanceLocalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getFeesBalanceLocalCurrency().getAmount().toString(), result.getFeesBalanceLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getFinancingTransactionLocalBalanceCurrency());
        assertEquals(detail.getExtraPayments().getFinancingTransactionLocalBalance().getAmount().toString(), result.getFinancingTransactionLocalBalanceAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getAmount().toString(), result.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.PEN, result.getInvoiceAmountLocalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getInvoiceAmountLocalCurrency().getAmount().toString(), result.getInvoiceAmountLocalCurrencyAmount());

        assertEquals(DTOIntCardStatementDetail.USD, result.getOutstandingBalanceCurrency());
        assertEquals(detail.getExtraPayments().getOutstandingBalance().getAmount().toString(), result.getOutstandingBalanceAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getMinimunPaymentMinimumCapitalCurrencyCurrency());
        assertEquals(detail.getExtraPayments().getMinimumCapitalCurrency().getAmount().toString(), result.getMinimunPaymentMinimumCapitalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getInterestsBalanceCurrency());
        assertEquals(detail.getExtraPayments().getInterestsBalance().getAmount().toString(), result.getInterestsBalanceAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getFeesBalanceCurrency());
        assertEquals(detail.getExtraPayments().getFeesBalance().getAmount().toString(), result.getFeesBalanceAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getFinancingTransactionBalanceCurrency());
        assertEquals(detail.getExtraPayments().getFinancingTransactionBalance().getAmount().toString(), result.getFinancingTransactionBalanceAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getMinimumPaymentInvoiceMinimumAmountCurrency());
        assertEquals(detail.getExtraPayments().getInvoiceMinimumAmountCurrency().getAmount().toString(), result.getMinimumPaymentInvoiceMinimumAmountAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getInvoiceAmountCurrency());
        assertEquals(detail.getExtraPayments().getInvoiceAmount().getAmount().toString(), result.getInvoiceAmountAmount());

        assertEquals(DTOIntCardStatementDetail.PEN, result.getTotalDebtLocalCurrencyCurrency());
        assertEquals(detail.getTotalDebtLocalCurrency().getAmount().toString(), result.getTotalDebtLocalCurrencyAmount());
        assertEquals(DTOIntCardStatementDetail.USD, result.getTotalDebtCurrency());
        assertEquals(detail.getTotalDebt().getAmount().toString(), result.getTotalDebtAmount());

        assertEquals(detail.getTotalMonthsAmortizationMinimum().toString(), result.getTotalMonthsAmortizationMinimum());
        assertEquals(detail.getTotalMonthsAmortizationMinimumFull().toString(), result.getTotalMonthsAmortizationMinimumFull());
    }

    @Test
    public void testMapOutEmpty() {
        final DocumentRequest result = mapper.mapOut(new DTOIntCardStatement());
        assertNull(result);
    }

}
