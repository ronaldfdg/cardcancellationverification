package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.EntityMock.NUMBER;
import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ModifyCardV0MapperTest {

    @InjectMocks
    private ModifyCardV0Mapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Mock
    private Translator translator;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_MODIFY_CARD)).thenReturn(NUMBER);
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.status.id", "OPERATIVE")).thenReturn("O");
    }

    @Test
    public void mapInTestFull() {
        Card card = mock.getCard();
        DTOIntCard result = mapper.mapIn(CARD_ENCRYPT_ID, card);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getStatusId());
        assertNotNull(result.getStatusReasonId());

        assertEquals(NUMBER, result.getCardId());
        assertEquals("O", result.getStatusId());
        assertEquals(card.getStatus().getReason().getId(), result.getStatusReasonId());
    }

    @Test
    public void mapInTestWithoutStatusId() {
        Card card = mock.getCard();
        card.getStatus().setId(null);
        DTOIntCard result = mapper.mapIn(CARD_ENCRYPT_ID, card);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNull(result.getStatusId());
        assertNotNull(result.getStatusReasonId());

        assertEquals(NUMBER, result.getCardId());
        assertEquals(card.getStatus().getReason().getId(), result.getStatusReasonId());
    }

    @Test
    public void mapInTestWithoutStatusReasonId() {
        Card card = mock.getCard();
        card.getStatus().getReason().setId(null);
        DTOIntCard result = mapper.mapIn(CARD_ENCRYPT_ID, card);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getStatusId());
        assertNull(result.getStatusReasonId());

        assertEquals(NUMBER, result.getCardId());
        assertEquals("O", result.getStatusId());
    }

    @Test
    public void mapInTestEmpty() {
        DTOIntCard result = mapper.mapIn(CARD_ENCRYPT_ID, new Card());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNull(result.getStatusId());
        assertNull(result.getStatusReasonId());

    }
}
