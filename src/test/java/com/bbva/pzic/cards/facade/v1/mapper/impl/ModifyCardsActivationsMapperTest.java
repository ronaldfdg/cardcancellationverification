package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.facade.v1.dto.Activation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static com.bbva.pzic.cards.EntityMock.STUB_ACTIVATION_ID;
import static org.mockito.Mockito.when;
import static com.bbva.pzic.cards.EntityMock.CUSTOMER_CODE;

/**
 * Created on 24/06/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyCardsActivationsMapperTest {

    @InjectMocks
    private ModifyCardsActivationsMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(CUSTOMER_CODE);
    }

    @Test
    public void mapInAllTest() {
        Activation input = new Activation();
        input.setIsActive(true);
        DTOIntActivation result = mapper.mapIn(STUB_ACTIVATION_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertNotNull(result.getActivationId());
        assertNotNull(result.getIsActive());
        assertEquals(CUSTOMER_CODE, result.getCustomerId());
        assertEquals(STUB_ACTIVATION_ID, result.getActivationId());
        assertEquals(true, result.getIsActive());
    }

    @Test
    public void mapInIsActiveFalseTest() {
        Activation input = new Activation();
        input.setIsActive(false);
        DTOIntActivation result = mapper.mapIn(STUB_ACTIVATION_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertNotNull(result.getActivationId());
        assertNotNull(result.getIsActive());
        assertEquals(CUSTOMER_CODE, result.getCustomerId());
        assertEquals(STUB_ACTIVATION_ID, result.getActivationId());
        assertEquals(false, result.getIsActive());
    }
}