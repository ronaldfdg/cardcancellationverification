package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntDelivery;
import com.bbva.pzic.cards.facade.v1.dto.Delivery;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Constants.STORED;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardDeliveryMapperTest {

    @InjectMocks
    private CreateCardDeliveryMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInContactTypeSpecificContactDetailTypeEmailTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliverySpecificEmail();

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn(CONTACT_TYPE_SPECIFIC_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getContact().getContact().getContactDetailType()))
                .thenReturn(CONTACT_DETAIL_TYPE_EMAIL_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.destination.id", input.getDestination().getId()))
                .thenReturn(DESTINATION_ID_ENUM);

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCardId());
        assertNotNull(result.getServiceTypeId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContactTypeOriginal());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getContactDetailTypeOriginal());
        assertNotNull(result.getContact().getContact().getAddress());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountryId());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());
        assertNull(result.getContact().getContact().getId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(input.getServiceType().getId(), result.getServiceTypeId());
        assertEquals(CONTACT_TYPE_SPECIFIC_ENUM, result.getContact().getContactType());
        assertEquals(input.getContact().getContactType(), result.getContact().getContactTypeOriginal());
        assertEquals(CONTACT_DETAIL_TYPE_EMAIL_ENUM, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getContact().getContact().getContactDetailTypeOriginal());
        assertEquals(input.getContact().getContact().getAddress(), result.getContact().getContact().getAddress());
        assertEquals(DESTINATION_ID_ENUM, result.getDestination().getId());
    }

    @Test
    public void mapInContactTypeSpecificContactDetailTypeLandlineTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliverySpecificLandline();

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn(CONTACT_TYPE_SPECIFIC_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getContact().getContact().getContactDetailType()))
                .thenReturn(CONTACT_DETAIL_TYPE_LANDLINE_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contact.phoneType", input.getContact().getContact().getPhoneType()))
                .thenReturn(PHONE_TYPE_LANDLINE_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.destination.id", input.getDestination().getId()))
                .thenReturn(DESTINATION_ID_ENUM);

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCardId());
        assertNotNull(result.getServiceTypeId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContactTypeOriginal());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getContactDetailTypeOriginal());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneType());
        assertNotNull(result.getContact().getContact().getCountryId());
        assertNotNull(result.getContact().getContact().getRegionalCode());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());
        assertNull(result.getContact().getContact().getId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(input.getServiceType().getId(), result.getServiceTypeId());
        assertEquals(CONTACT_TYPE_SPECIFIC_ENUM, result.getContact().getContactType());
        assertEquals(input.getContact().getContactType(), result.getContact().getContactTypeOriginal());
        assertEquals(CONTACT_DETAIL_TYPE_LANDLINE_ENUM, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getContact().getContact().getContactDetailTypeOriginal());
        assertEquals(input.getContact().getContact().getNumber(), result.getContact().getContact().getNumber());
        assertEquals(PHONE_TYPE_LANDLINE_ENUM, result.getContact().getContact().getPhoneType());
        assertEquals(input.getContact().getContact().getCountry().getId(), result.getContact().getContact().getCountryId());
        assertEquals(input.getContact().getContact().getRegionalCode(), result.getContact().getContact().getRegionalCode());
        assertEquals(DESTINATION_ID_ENUM, result.getDestination().getId());
    }

    @Test
    public void mapInContactTypeSpecificContactDetailTypeMobileTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliverySpecificMobile();

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn(CONTACT_TYPE_SPECIFIC_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getContact().getContact().getContactDetailType()))
                .thenReturn(CONTACT_DETAIL_TYPE_MOBILE_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.destination.id", input.getDestination().getId()))
                .thenReturn(DESTINATION_ID_ENUM);

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCardId());
        assertNotNull(result.getServiceTypeId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContactTypeOriginal());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getContactDetailTypeOriginal());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountryId());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());
        assertNull(result.getContact().getContact().getId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(input.getServiceType().getId(), result.getServiceTypeId());
        assertEquals(CONTACT_TYPE_SPECIFIC_ENUM, result.getContact().getContactType());
        assertEquals(input.getContact().getContactType(), result.getContact().getContactTypeOriginal());
        assertEquals(CONTACT_DETAIL_TYPE_MOBILE_ENUM, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getContact().getContact().getContactDetailTypeOriginal());
        assertEquals(input.getContact().getContact().getNumber(), result.getContact().getContact().getNumber());
        assertEquals(input.getContact().getContact().getPhoneCompany().getId(), result.getContact().getContact().getPhoneCompany().getId());
        assertEquals(DESTINATION_ID_ENUM, result.getDestination().getId());
    }

    @Test
    public void mapInContactTypeSpecificContactDetailTypeSocialMediaTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliverySpecificSocialMedia();

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn(CONTACT_TYPE_SPECIFIC_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getContact().getContact().getContactDetailType()))
                .thenReturn(CONTACT_DETAIL_TYPE_SOCIAL_MEDIA_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contact.socialNetwork", input.getContact().getContact().getSocialNetwork()))
                .thenReturn(SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.destination.id", input.getDestination().getId()))
                .thenReturn(DESTINATION_ID_ENUM);

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCardId());
        assertNotNull(result.getServiceTypeId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContactTypeOriginal());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getContactDetailTypeOriginal());
        assertNotNull(result.getContact().getContact().getUsername());
        assertNotNull(result.getContact().getContact().getSocialNetwork());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountryId());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(input.getServiceType().getId(), result.getServiceTypeId());
        assertEquals(CONTACT_TYPE_SPECIFIC_ENUM, result.getContact().getContactType());
        assertEquals(input.getContact().getContactType(), result.getContact().getContactTypeOriginal());
        assertEquals(CONTACT_DETAIL_TYPE_SOCIAL_MEDIA_ENUM, result.getContact().getContact().getContactDetailType());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getContact().getContact().getContactDetailTypeOriginal());
        assertEquals(input.getContact().getContact().getUsername(), result.getContact().getContact().getUsername());
        assertEquals(SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM, result.getContact().getContact().getSocialNetwork());
        assertEquals(DESTINATION_ID_ENUM, result.getDestination().getId());
    }

    @Test
    public void mapInContactTypeSpecificContactDetailTypeUnrecognizedTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliverySpecificSocialMedia();
        input.getContact().getContact().setContactDetailType("UNRECOGNIZED");

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn(CONTACT_TYPE_SPECIFIC_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactDetailType", input.getContact().getContact().getContactDetailType()))
                .thenReturn("XXX");

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertEquals(CARD_ID, result.getCardId());
        assertNotNull(result.getContact());
    }

    @Test
    public void mapInContactTypeStoredTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliveryStored();
        input.getContact().setContactType(STORED);

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn(CONTACT_TYPE_STORED_ENUM);
        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.destination.id", input.getDestination().getId()))
                .thenReturn(DESTINATION_ID_ENUM);

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCardId());
        assertNotNull(result.getServiceTypeId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContactTypeOriginal());
        assertNotNull(result.getContact().getContact().getId());
        assertNotNull(result.getDestination());
        assertNotNull(result.getDestination().getId());
        assertNull(result.getContact().getContact().getContactDetailType());
        assertNull(result.getContact().getContact().getAddress());
        assertNull(result.getContact().getContact().getNumber());
        assertNull(result.getContact().getContact().getPhoneType());
        assertNull(result.getContact().getContact().getCountryId());
        assertNull(result.getContact().getContact().getRegionalCode());
        assertNull(result.getContact().getContact().getPhoneCompany());
        assertNull(result.getContact().getContact().getUsername());
        assertNull(result.getContact().getContact().getSocialNetwork());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(input.getServiceType().getId(), result.getServiceTypeId());
        assertEquals(CONTACT_TYPE_STORED_ENUM, result.getContact().getContactType());
        assertEquals(input.getContact().getContactType(), result.getContact().getContactTypeOriginal());
        assertEquals(DESTINATION_ID_ENUM, result.getDestination().getId());
        assertEquals(input.getContact().getContact().getId(), result.getContact().getContact().getId());
    }

    @Test
    public void mapInContactTypeUnrecognizedTest() throws IOException {
        Delivery input = EntityMock.getInstance().buildDeliverySpecificSocialMedia();
        input.getContact().setContactType("UNRECOGNIZED");

        when(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", input.getContact().getContactType()))
                .thenReturn("YYY");

        DTOIntDelivery result = mapper.mapIn(CARD_ID, input);

        assertEquals(CARD_ID, result.getCardId());
        assertNotNull(result.getContact());
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntDelivery result = mapper.mapIn(CARD_ID, new Delivery());

        assertEquals(CARD_ID, result.getCardId());
        assertNull(result.getServiceTypeId());
        assertNull(result.getContact());
        assertNull(result.getDestination());
        assertNull(result.getAddress());
    }

    @Test
    public void testMapOutFull() {
        ServiceResponse<Delivery> result = mapper.mapOut(new Delivery());

        assertNotNull(result.getData());
    }

    @Test
    public void testMapOutNull() {
        ServiceResponse<Delivery> result = mapper.mapOut(null);

        assertNull(result);
    }
}
