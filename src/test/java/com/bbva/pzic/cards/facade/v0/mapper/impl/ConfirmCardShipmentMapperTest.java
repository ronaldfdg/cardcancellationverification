package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.ConfirmShipment;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.SHIPMENT_ID;
import static org.junit.Assert.*;

public class ConfirmCardShipmentMapperTest {

    private ConfirmCardShipmentMapper mapper;

    @Before
    public void setUp() {
        mapper = new ConfirmCardShipmentMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getShipmentAddressId());
        assertNotNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNotNull(result.getCurrentAddressLocationGeolocationLatitude());

        assertEquals(SHIPMENT_ID, result.getShipmentId());
        assertEquals(input.getBiometricId(), result.getBiometricId());
        assertEquals(input.getShipmentAddress().getId(), result.getShipmentAddressId());
        assertEquals(input.getCurrentAddress().getLocation().getGeolocation().getLongitude(), result.getCurrentAddressLocationGeolocationLongitude());
        assertEquals(input.getCurrentAddress().getLocation().getGeolocation().getLatitude(), result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInEmptyTest() {
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, new ConfirmShipment());

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNull(result.getBiometricId());
        assertNull(result.getShipmentAddressId());
        assertNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInShipmentAddressNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.setShipmentAddress(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNull(result.getShipmentAddressId());
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNotNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInShipmentAddressIdNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.getShipmentAddress().setId(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNull(result.getShipmentAddressId());
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNotNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInCurrentAddressNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.setCurrentAddress(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getShipmentAddressId());
        assertNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInCurrentAddressLocationNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.getCurrentAddress().setLocation(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getShipmentAddressId());
        assertNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInCurrentAddressLocationGeolocationNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.getCurrentAddress().getLocation().setGeolocation(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getShipmentAddressId());
        assertNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInCurrentAddressLocationGeolocationLatitudeNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.getCurrentAddress().getLocation().getGeolocation().setLatitude(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getShipmentAddressId());
        assertNotNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNull(result.getCurrentAddressLocationGeolocationLatitude());
    }

    @Test
    public void mapInCurrentAddressLocationGeolocationLongitudeNullTest() throws IOException {
        ConfirmShipment input = EntityMock.getInstance().buildConfirmShipment();
        input.getCurrentAddress().getLocation().getGeolocation().setLongitude(null);
        InputConfirmCardShipment result = mapper.mapIn(SHIPMENT_ID, input);

        assertNotNull(result);
        assertNotNull(result.getShipmentId());
        assertNotNull(result.getBiometricId());
        assertNotNull(result.getShipmentAddressId());
        assertNull(result.getCurrentAddressLocationGeolocationLongitude());
        assertNotNull(result.getCurrentAddressLocationGeolocationLatitude());
    }
}
