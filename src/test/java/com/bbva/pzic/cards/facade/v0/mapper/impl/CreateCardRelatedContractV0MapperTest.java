package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.canonic.RelatedContract;
import com.bbva.pzic.cards.canonic.RelatedContractData;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v01.mapper.impl.CreateCardRelatedContractMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created on 23/02/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardRelatedContractV0MapperTest {

    public static final String CARD_ID = "##4232$%6778";
    public static final String CONTRACT_ID = "CON_0001";
    public static final String RELATED_CONTRACT_ID = "#$$%%&%//(/)";
    public static final String ID_CONTRACT_ENCRYPT = "1234%&%/&&123";

    @InjectMocks
    private CreateCardRelatedContractMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    public void mapInDecrypt() {
        Mockito.when(cypherTool.decrypt("##4232$%6778", AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT)).thenReturn("12345678");
        Mockito.when(cypherTool.decrypt("#$5&&/%%1234", AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT)).thenReturn(CONTRACT_ID);
    }

    public void mapOutEncrypt() {
        Mockito.when(cypherTool.encrypt("9087 6542 3456 1234 0987", AbstractCypherTool.IDCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT)).thenReturn(RELATED_CONTRACT_ID);
        Mockito.when(cypherTool.encrypt("CON_4567", AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT)).thenReturn(ID_CONTRACT_ENCRYPT);
    }

    @Test
    public void mapInTest() {
        mapInDecrypt();
        RelatedContract relatedContract = mock.getRelatedContract();
        DTOInputCreateCardRelatedContract result = mapper.mapIn(CARD_ID, relatedContract);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertEquals(result.getCardId(), "12345678");
        Assert.assertNotNull(result.getContractId());
        Assert.assertEquals(result.getContractId(), CONTRACT_ID);
    }

    @Test
    public void mapInWithoutContractIdTest() {
        mapInDecrypt();
        DTOInputCreateCardRelatedContract result = mapper.mapIn(CARD_ID, new RelatedContract());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertEquals(result.getCardId(), "12345678");
        Assert.assertNull(result.getContractId());
    }

    @Test
    public void mapOutTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getData().getRelatedContractId());
        Assert.assertEquals(result.getData().getRelatedContractId(), RELATED_CONTRACT_ID);
        Assert.assertNotNull(result.getData().getContractId());
        Assert.assertEquals(result.getData().getContractId(), ID_CONTRACT_ENCRYPT);
        Assert.assertNotNull(result.getData().getNumberType());
        Assert.assertNotNull(result.getData().getNumberType().getId());
        Assert.assertEquals(result.getData().getNumberType().getId(), dtoOut.getNumberTypeId());
        Assert.assertNotNull(result.getData().getNumberType().getName());
        Assert.assertEquals(result.getData().getNumberType().getName(), dtoOut.getNumberTypeName());
    }

    @Test
    public void mapOutWithoutRelatedContractIdTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        dtoOut.setRelatedContractId(null);
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNull(result.getData().getRelatedContractId());
        Assert.assertNotNull(result.getData().getContractId());
        Assert.assertEquals(result.getData().getContractId(), ID_CONTRACT_ENCRYPT);
        Assert.assertNotNull(result.getData().getNumberType());
        Assert.assertNotNull(result.getData().getNumberType().getId());
        Assert.assertEquals(result.getData().getNumberType().getId(), dtoOut.getNumberTypeId());
        Assert.assertNotNull(result.getData().getNumberType().getName());
        Assert.assertEquals(result.getData().getNumberType().getName(), dtoOut.getNumberTypeName());
    }

    @Test
    public void mapOutWithoutContractIdTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        dtoOut.setContractId(null);
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getData().getRelatedContractId());
        Assert.assertEquals(result.getData().getRelatedContractId(), RELATED_CONTRACT_ID);
        Assert.assertNull(result.getData().getContractId());
        Assert.assertNotNull(result.getData().getNumberType());
        Assert.assertNotNull(result.getData().getNumberType().getId());
        Assert.assertEquals(result.getData().getNumberType().getId(), dtoOut.getNumberTypeId());
        Assert.assertNotNull(result.getData().getNumberType().getName());
        Assert.assertEquals(result.getData().getNumberType().getName(), dtoOut.getNumberTypeName());
    }

    @Test
    public void mapOutWithoutRelatedContractIdAndContractIdTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        dtoOut.setRelatedContractId(null);
        dtoOut.setContractId(null);
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNull(result.getData().getRelatedContractId());
        Assert.assertNull(result.getData().getContractId());
        Assert.assertNotNull(result.getData().getNumberType());
        Assert.assertNotNull(result.getData().getNumberType().getId());
        Assert.assertEquals(result.getData().getNumberType().getId(), dtoOut.getNumberTypeId());
        Assert.assertNotNull(result.getData().getNumberType().getName());
        Assert.assertEquals(result.getData().getNumberType().getName(), dtoOut.getNumberTypeName());
    }

    @Test
    public void mapOutWithoutNumberTypeTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        dtoOut.setNumberTypeName(null);
        dtoOut.setNumberTypeId(null);
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getData().getRelatedContractId());
        Assert.assertEquals(result.getData().getRelatedContractId(), RELATED_CONTRACT_ID);
        Assert.assertNotNull(result.getData().getContractId());
        Assert.assertEquals(result.getData().getContractId(), ID_CONTRACT_ENCRYPT);
        Assert.assertNull(result.getData().getNumberType());
    }

    @Test
    public void mapOutWithoutNumberTypeIdTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        dtoOut.setNumberTypeId(null);
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getData().getRelatedContractId());
        Assert.assertEquals(result.getData().getRelatedContractId(), RELATED_CONTRACT_ID);
        Assert.assertNotNull(result.getData().getContractId());
        Assert.assertEquals(result.getData().getContractId(), ID_CONTRACT_ENCRYPT);
        Assert.assertNotNull(result.getData().getNumberType());
        Assert.assertNull(result.getData().getNumberType().getId());
        Assert.assertNotNull(result.getData().getNumberType().getName());
        Assert.assertEquals(result.getData().getNumberType().getName(), dtoOut.getNumberTypeName());
    }

    @Test
    public void mapOutWithoutNumberTypeNameTest() {
        mapOutEncrypt();
        DTOOutCreateCardRelatedContract dtoOut = mock.geDtoOutCreateRelatedContract();
        dtoOut.setNumberTypeName(null);
        RelatedContractData result = mapper.mapOut(dtoOut);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getData().getRelatedContractId());
        Assert.assertEquals(result.getData().getRelatedContractId(), RELATED_CONTRACT_ID);
        Assert.assertNotNull(result.getData().getContractId());
        Assert.assertEquals(result.getData().getContractId(), ID_CONTRACT_ENCRYPT);
        Assert.assertNotNull(result.getData().getNumberType());
        Assert.assertNotNull(result.getData().getNumberType().getId());
        Assert.assertEquals(result.getData().getNumberType().getId(), dtoOut.getNumberTypeId());
        Assert.assertNull(result.getData().getNumberType().getName());
    }

    @Test
    public void mapOutWithDTOOutCreateRelatedContractNullTest() {
        RelatedContractData result = mapper.mapOut(new DTOOutCreateCardRelatedContract());
        Assert.assertNull(result);
    }

}
