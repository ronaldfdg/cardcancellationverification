package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;

/**
 * Created on 17/10/2017.
 *
 * @author Entelgy
 */
public class GetCardMapperTest {

    private GetCardMapper mapper = new GetCardMapper();

    private DummyMock dummyMock = new DummyMock();

    @Test
    public void mapInFull() {
        final DTOIntCard dtoIntCard = mapper.mapIn(CARD_ID);

        assertNotNull(dtoIntCard);
        assertNotNull(dtoIntCard.getCardId());

        assertEquals(CARD_ID, dtoIntCard.getCardId());
    }

    @Test
    public void mapInWithOutCardId() {
        final DTOIntCard result = mapper.mapIn(null);

        assertNotNull(result);
        assertNull(result.getCardId());
    }

    @Test
    public void mapOutFullNullTest() {
        final ServiceResponse<Card> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        final ServiceResponse<Card> result = mapper.mapOut(new Card());

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNull(result.getData().getCardId());
        assertNull(result.getData().getNumber());
        assertNull(result.getData().getImages());
    }

    @Test
    public void mapOutFull() throws IOException {
        final Card entity = dummyMock.getCardMock();
        final ServiceResponse<Card> result = mapper.mapOut(entity);

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData().getImages().get(0).getUrl());
        assertNotNull(result.getData().getImages().get(1).getUrl());
    }
}
