package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCardPaymentMethodsMapperTest {

    @InjectMocks
    private GetCardPaymentMethodsMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Test
    public void mapInFull() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_PAYMENT_METHODS)).thenReturn(CARD_ID);
        final DTOIntCard dtoIn = mapper.mapIn(CARD_ID_ENCRYPTED);

        assertNotNull(dtoIn);
        assertNotNull(dtoIn.getCardId());
        assertEquals(CARD_ID, dtoIn.getCardId());
    }

    @Test
    public void mapInWithOutCardId() {
        final DTOIntCard result = mapper.mapIn(null);

        assertNotNull(result);
        assertNull(result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<PaymentMethod>> result = mapper.mapOut(new PaymentMethod());
        assertNotNull(result);
        assertNotNull(result.getData());
        assertFalse(result.getData().isEmpty());
        assertEquals(1, result.getData().size());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<PaymentMethod>> result = mapper.mapOut(null);
        assertNull(result);
    }
}
