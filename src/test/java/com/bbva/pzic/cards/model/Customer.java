package com.bbva.pzic.cards.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 24/06/2016.
 *
 * @author Entelgy
 */
public class Customer extends Person {

    @NotNull(groups = ValidationGroup.CreateCustomer.class)
    @Size(min = 1, groups = ValidationGroup.CreateCustomer.class)
    @Valid
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
