package com.bbva.pzic.cards.model;

/**
 * Created on 24/06/2016.
 *
 * @author Entelgy
 */
public class ValidationGroup {

    public interface CreateCustomer {
    }

    public interface CreateOrModifyCustomerForPersonDocumentType {
    }

    public interface CreateOrModifyCustomerForUserType {
    }

    public interface ModifyCustomer {
    }

}
