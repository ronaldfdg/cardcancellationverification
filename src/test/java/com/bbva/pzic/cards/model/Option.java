package com.bbva.pzic.cards.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/06/2016.
 *
 * @author Entelgy
 */
public class Option {

    @NotNull
    @Size(min = 5, max = 5, groups = ValidationGroup.CreateOrModifyCustomerForPersonDocumentType.class)
    @Size(min = 10, max = 10, groups = ValidationGroup.CreateOrModifyCustomerForUserType.class)
    private String id;
    private String value;

    public Option(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
