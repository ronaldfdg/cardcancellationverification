package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class DTOIntSpecificAddressType extends DTOIntAddressType {

    private String id;
    @Valid
    private DTOIntLocation location;

    public DTOIntLocation getLocation() {
        return location;
    }

    public void setLocation(DTOIntLocation location) {
        this.location = location;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
