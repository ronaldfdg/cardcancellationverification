package com.bbva.pzic.cards.business.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
public class DTOOutCreateCardRelatedContract {
    @DatoAuditable(omitir = true)
    private String relatedContractId;
    @DatoAuditable(omitir = true)
    private String contractId;
    private String numberTypeId;
    private String numberTypeName;

    public String getRelatedContractId() {
        return relatedContractId;
    }

    public void setRelatedContractId(String relatedContractId) {
        this.relatedContractId = relatedContractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getNumberTypeId() {
        return numberTypeId;
    }

    public void setNumberTypeId(String numberTypeId) {
        this.numberTypeId = numberTypeId;
    }

    public String getNumberTypeName() {
        return numberTypeName;
    }

    public void setNumberTypeName(String numberTypeName) {
        this.numberTypeName = numberTypeName;
    }
}
