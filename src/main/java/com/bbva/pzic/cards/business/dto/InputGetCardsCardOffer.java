package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class InputGetCardsCardOffer {

    @NotNull(groups = {ValidationGroup.GetCardsCardOffer.class})
    @Size(max = 20, groups = {ValidationGroup.GetCardsCardOffer.class})
    private String cardId;

    @NotNull(groups = {ValidationGroup.GetCardsCardOffer.class})
    @Size(max = 10, groups = {ValidationGroup.GetCardsCardOffer.class})
    private String offerId;

    public InputGetCardsCardOffer() {
    }

    public InputGetCardsCardOffer(String cardId, String offerId) {
        this.cardId = cardId;
        this.offerId = offerId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
