package com.bbva.pzic.cards.business.dto;

import java.util.List;

/**
 * Created on 12/02/2016.
 *
 * @author Entelgy
 */
public class DTOIntCardStatement {

    private DTOIntCardStatementDetail detail;
    private List<String> rowCardStatement;
    private List<String> messages;

    public DTOIntCardStatementDetail getDetail() {
        return detail;
    }

    public void setDetail(DTOIntCardStatementDetail detail) {
        this.detail = detail;
    }

    public List<String> getRowCardStatement() {
        return rowCardStatement;
    }

    public void setRowCardStatement(List<String> rowCardStatement) {
        this.rowCardStatement = rowCardStatement;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
