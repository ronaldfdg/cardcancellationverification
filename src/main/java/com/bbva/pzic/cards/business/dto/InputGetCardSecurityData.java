package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
public class InputGetCardSecurityData {

    @Size(max = 16, groups = {
            ValidationGroup.GetCardSecurityData.class,
            ValidationGroup.GetCardSecurityDataV1.class
    })
    private String cardId;

    @NotNull(groups = {
            ValidationGroup.GetCardSecurityData.class,
            ValidationGroup.GetCardSecurityDataV1.class
    })
    @Size(max = 1050, groups = {
            ValidationGroup.GetCardSecurityData.class,
            ValidationGroup.GetCardSecurityDataV1.class
    })
    private String publicKey;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
