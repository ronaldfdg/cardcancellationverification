package com.bbva.pzic.cards.business.dto;

/**
 * @author Entelgy
 */
public interface ValidationGroup {

    interface CreateCard {
    }

    interface ModifyCardActivation {
    }

    interface ModifyCardBlock {
    }

    interface ListCards {
    }

    interface ListCardsV0 {
    }

    interface CreateRelatedContract {
    }

    interface GetCardSecurityData {
    }

    interface CreateInstallmentsPlan {
    }

    interface SimulateInstallmentsPlanV00 {
    }

    interface SimulateInstallmentsPlanV0 {
    }

    interface ModifyCard {
    }

    interface ModifyPartialCardBlock {
    }

    interface ListCardActivations {
    }

    interface ModifyCardActivations {
    }

    interface ListCardLimits {
    }

    interface GetCard {
    }

    interface GetCardPaymentMethodsV0 {
    }

    interface ModifyCardPin {
    }

    interface ModifyCardLimitV0 {
    }

    interface GetCardTransactionV0 {
    }

    interface CreateCardV0 {
    }

    interface CreateCardV1 {
    }

    interface ListInstallmentPlansV0 {
    }

    interface CreateInstallmentsPlanV0 {
    }

    interface ModifyCardV0 {
    }

    interface CreateRelatedContractV0 {
    }

    interface GetConditions {
    }

    interface ListCardFinancialStatementsV0 {
    }

    interface GetCardFinancialStatement {
    }

    interface GetCardFinancialStatementDocument {
    }

    interface CreateCardsMaskedToken {
    }

    interface CreateCardAdvanceV0 {
    }

    interface GetCardsCardOffer {
    }

    interface CreateCardsOfferSimulate {
    }

    interface CreateCardsProposal {
    }

    interface CreateCardsCardProposal {

        interface FirstContactDetailsContactValue {
        }

        interface SecondContactDetailsContactValue {
        }
    }

    interface CreateCardCashRefund {
    }

    interface CreateCardProposal {
    }

    interface UpdateCardProposal {
    }

    interface ModifyCardProposal {
    }

    interface CreateCardDelivery {
    }

    interface CreateCardShipmentAddress {
    }

    interface ConfirmCardShipment {
    }

    interface InitializeCardShipment {
    }

    interface GetCardSecurityDataV1 {

    }


    interface ModifyCardsActivations {
    }

    interface CreateCardReportsV0 {
    }

    interface GetCardCancellationVerificationV1 {
    }

    interface ReimburseCardTransactionTransactionRefund {
    }

    interface SimulateCardTransactionTransactionRefund {
    }

    interface GetCardFinancialStatementV1 {

    }
}
