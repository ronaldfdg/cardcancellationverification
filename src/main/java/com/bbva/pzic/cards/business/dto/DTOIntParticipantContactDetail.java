package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntParticipantContactDetail {

    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 80, groups = ValidationGroup.CreateCardsCardProposal.FirstContactDetailsContactValue.class)
    @Size(max = 9, groups = ValidationGroup.CreateCardsCardProposal.SecondContactDetailsContactValue.class)
    private String contactValue;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntContactType contactType;

    public String getContactValue() {
        return contactValue;
    }

    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

    public DTOIntContactType getContactType() {
        return contactType;
    }

    public void setContactType(DTOIntContactType contactType) {
        this.contactType = contactType;
    }
}