package com.bbva.pzic.cards.business.dto;

public class DTOIntSearchCriteria {

    private String cardId;
    private String membershipId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }


}
