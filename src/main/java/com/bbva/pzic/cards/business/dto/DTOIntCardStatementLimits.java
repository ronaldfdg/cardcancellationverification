package com.bbva.pzic.cards.business.dto;

import java.math.BigDecimal;

/**
 * Created on 13/06/2016.
 *
 * @author Entelgy
 */
public class DTOIntCardStatementLimits {

    private BigDecimal disposedBalance;
    private BigDecimal availableBalance;
    private BigDecimal financingDisposedBalance;

    public BigDecimal getDisposedBalance() {
        return disposedBalance;
    }

    public void setDisposedBalance(BigDecimal disposedBalance) {
        this.disposedBalance = disposedBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getFinancingDisposedBalance() {
        return financingDisposedBalance;
    }

    public void setFinancingDisposedBalance(BigDecimal financingDisposedBalance) {
        this.financingDisposedBalance = financingDisposedBalance;
    }
}
