package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntContactType {

    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 2, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}