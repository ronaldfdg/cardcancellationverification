package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntTitle {

    @NotNull(groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class
    })

    @Size(max = 2, groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 6, groups = ValidationGroup.CreateCardsProposal.class)
    private String id;
    private String name;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntSubProduct subproduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntSubProduct getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(DTOIntSubProduct subproduct) {
        this.subproduct = subproduct;
    }
}