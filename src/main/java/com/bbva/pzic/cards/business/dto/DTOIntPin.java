package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
public class DTOIntPin extends DTOIntCard {

    @NotNull(groups = ValidationGroup.ModifyCardPin.class)
    @Size(min = 976, max = 1050, groups = ValidationGroup.ModifyCardPin.class)
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
