package com.bbva.pzic.cards.business.dto;

import com.bbva.pzic.cards.facade.v0.dto.CashAdvances;

import java.util.List;

public class DTOIntListCashAdvances {

    private List<CashAdvances> data;

    public List<CashAdvances> getData() {
        return data;
    }

    public void setData(List<CashAdvances> data) {
        this.data = data;
    }
}
