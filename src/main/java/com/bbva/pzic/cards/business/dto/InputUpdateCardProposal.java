package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public class InputUpdateCardProposal {

    private String proposalId;
    @Valid
    private DTOIntCardProposal proposal;

    public String getProposalId() {
        return proposalId;
    }

    public void setProposalId(String proposalId) {
        this.proposalId = proposalId;
    }

    public DTOIntCardProposal getProposal() {
        return proposal;
    }

    public void setProposal(DTOIntCardProposal proposal) {
        this.proposal = proposal;
    }
}
