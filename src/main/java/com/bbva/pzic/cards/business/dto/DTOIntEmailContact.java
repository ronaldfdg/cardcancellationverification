package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntEmailContact {
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String contactType;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String address;

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
