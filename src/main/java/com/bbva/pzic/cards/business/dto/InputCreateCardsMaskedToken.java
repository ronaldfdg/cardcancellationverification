package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
public class InputCreateCardsMaskedToken {

    @Size(max = 16, groups = ValidationGroup.CreateCardsMaskedToken.class)
    private String cardId;
    @NotNull(groups = ValidationGroup.CreateCardsMaskedToken.class)
    @Size(max = 6, groups = ValidationGroup.CreateCardsMaskedToken.class)
    private String expirationDate;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}
