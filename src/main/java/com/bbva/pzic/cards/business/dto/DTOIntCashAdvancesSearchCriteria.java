package com.bbva.pzic.cards.business.dto;

public class DTOIntCashAdvancesSearchCriteria {

    private String cardId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
