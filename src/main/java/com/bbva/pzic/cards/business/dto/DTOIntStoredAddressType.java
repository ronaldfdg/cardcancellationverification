package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class DTOIntStoredAddressType extends DTOIntAddressType {

    @NotNull(groups = ValidationGroup.CreateCardShipmentAddress.class)
    private String id;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardShipmentAddress.class)
    private DTOIntDestination destination;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public DTOIntDestination getDestination() {
        return destination;
    }

    public void setDestination(DTOIntDestination destination) {
        this.destination = destination;
    }
}
