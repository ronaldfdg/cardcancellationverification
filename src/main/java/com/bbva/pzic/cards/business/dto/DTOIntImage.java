package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 20/04/2020.
 *
 * @author Entelgy.
 */
public class DTOIntImage {

    @NotNull(groups = ValidationGroup.UpdateCardProposal.class)
    @Size(max = 1, groups = ValidationGroup.CreateCardV0.class)
    private String id;
    private String name;
    @NotNull(groups = ValidationGroup.UpdateCardProposal.class)
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
