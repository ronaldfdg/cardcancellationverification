package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntCurrency {

    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String currency;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private Boolean isMajor;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(Boolean major) {
        isMajor = major;
    }
}
