package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Size;

public class InputGetCardCancellationVerification {

    @Size(max = 19, groups = ValidationGroup.GetCardCancellationVerificationV1.class)
    private String cardId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
