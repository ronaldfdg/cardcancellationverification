package com.bbva.pzic.cards.business.dto;

import com.bbva.pzic.cards.canonic.Limits;

import java.util.List;

public class DTOIntListLimits {

    private List<Limits> data;

    public List<Limits> getData() {
        return data;
    }

    public void setData(List<Limits> data) {
        this.data = data;
    }
}
