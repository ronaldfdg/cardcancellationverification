package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntLoyaltyProgram {
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String id;

    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
