package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntApplication {

    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private String name;
    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private String version;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
