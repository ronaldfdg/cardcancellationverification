package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntAuthorizedParticipant {

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntIdentityDocument identityDocument;
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 20, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String firstName;
    @Size(max = 20, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String middleName;
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 20, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String lastName;
    @Size(max = 20, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String secondLastName;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntProfession profession;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntMaritalStatus maritalStatus;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntRelationType relationType;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(min = 2, groups = ValidationGroup.CreateCardsCardProposal.class)
    private List<DTOIntParticipantContactDetail> contactDetails;

    public DTOIntIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public DTOIntProfession getProfession() {
        return profession;
    }

    public void setProfession(DTOIntProfession profession) {
        this.profession = profession;
    }

    public DTOIntMaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(DTOIntMaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public DTOIntRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(DTOIntRelationType relationType) {
        this.relationType = relationType;
    }

    public List<DTOIntParticipantContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(
            List<DTOIntParticipantContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }
}