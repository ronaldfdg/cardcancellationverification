package com.bbva.pzic.cards.business.dto;

import com.bbva.pzic.cards.canonic.Card;

import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
public class DTOOutListCards {
    private List<Card> data;
    private DTOIntPagination pagination;

    public List<Card> getData() {
        return data;
    }

    public void setData(List<Card> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}
