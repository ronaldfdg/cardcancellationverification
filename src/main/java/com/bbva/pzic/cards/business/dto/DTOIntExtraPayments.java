package com.bbva.pzic.cards.business.dto;

/**
 * Created on 11/07/2016.
 *
 * @author Entelgy
 */
public class DTOIntExtraPayments {

    private DTOMoney outstandingBalanceLocalCurrency;
    private DTOMoney minimumCapitalLocalCurrency;
    private DTOMoney interestsBalanceLocalCurrency;
    private DTOMoney feesBalanceLocalCurrency;
    private DTOMoney financingTransactionLocalBalance;
    private DTOMoney invoiceMinimumAmountLocalCurrency;
    private DTOMoney invoiceAmountLocalCurrency;
    private DTOMoney outstandingBalance;
    private DTOMoney minimumCapitalCurrency;
    private DTOMoney interestsBalance;
    private DTOMoney feesBalance;
    private DTOMoney financingTransactionBalance;
    private DTOMoney invoiceMinimumAmountCurrency;
    private DTOMoney invoiceAmount;

    public DTOMoney getOutstandingBalanceLocalCurrency() {
        return outstandingBalanceLocalCurrency;
    }

    public void setOutstandingBalanceLocalCurrency(DTOMoney outstandingBalanceLocalCurrency) {
        this.outstandingBalanceLocalCurrency = outstandingBalanceLocalCurrency;
        this.outstandingBalanceLocalCurrency.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getMinimumCapitalLocalCurrency() {
        return minimumCapitalLocalCurrency;
    }

    public void setMinimumCapitalLocalCurrency(DTOMoney minimumCapitalLocalCurrency) {
        this.minimumCapitalLocalCurrency = minimumCapitalLocalCurrency;
        this.minimumCapitalLocalCurrency.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getInterestsBalanceLocalCurrency() {
        return interestsBalanceLocalCurrency;
    }

    public void setInterestsBalanceLocalCurrency(DTOMoney interestsBalanceLocalCurrency) {
        this.interestsBalanceLocalCurrency = interestsBalanceLocalCurrency;
        this.interestsBalanceLocalCurrency.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getFeesBalanceLocalCurrency() {
        return feesBalanceLocalCurrency;
    }

    public void setFeesBalanceLocalCurrency(DTOMoney feesBalanceLocalCurrency) {
        this.feesBalanceLocalCurrency = feesBalanceLocalCurrency;
        this.feesBalanceLocalCurrency.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getFinancingTransactionLocalBalance() {
        return financingTransactionLocalBalance;
    }

    public void setFinancingTransactionLocalBalance(DTOMoney financingTransactionLocalBalance) {
        this.financingTransactionLocalBalance = financingTransactionLocalBalance;
        this.financingTransactionLocalBalance.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getInvoiceMinimumAmountLocalCurrency() {
        return invoiceMinimumAmountLocalCurrency;
    }

    public void setInvoiceMinimumAmountLocalCurrency(DTOMoney invoiceMinimumAmountLocalCurrency) {
        this.invoiceMinimumAmountLocalCurrency = invoiceMinimumAmountLocalCurrency;
        this.invoiceMinimumAmountLocalCurrency.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getInvoiceAmountLocalCurrency() {
        return invoiceAmountLocalCurrency;
    }

    public void setInvoiceAmountLocalCurrency(DTOMoney invoiceAmountLocalCurrency) {
        this.invoiceAmountLocalCurrency = invoiceAmountLocalCurrency;
        this.invoiceAmountLocalCurrency.setCurrency(DTOIntCardStatementDetail.PEN);
    }

    public DTOMoney getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(DTOMoney outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
        this.outstandingBalance.setCurrency(DTOIntCardStatementDetail.USD);
    }

    public DTOMoney getMinimumCapitalCurrency() {
        return minimumCapitalCurrency;
    }

    public void setMinimumCapitalCurrency(DTOMoney minimumCapitalCurrency) {
        this.minimumCapitalCurrency = minimumCapitalCurrency;
        this.minimumCapitalCurrency.setCurrency(DTOIntCardStatementDetail.USD);
    }

    public DTOMoney getInterestsBalance() {
        return interestsBalance;
    }

    public void setInterestsBalance(DTOMoney interestsBalance) {
        this.interestsBalance = interestsBalance;
        this.interestsBalance.setCurrency(DTOIntCardStatementDetail.USD);
    }

    public DTOMoney getFeesBalance() {
        return feesBalance;
    }

    public void setFeesBalance(DTOMoney feesBalance) {
        this.feesBalance = feesBalance;
        this.feesBalance.setCurrency(DTOIntCardStatementDetail.USD);
    }

    public DTOMoney getFinancingTransactionBalance() {
        return financingTransactionBalance;
    }

    public void setFinancingTransactionBalance(DTOMoney financingTransactionBalance) {
        this.financingTransactionBalance = financingTransactionBalance;
        this.financingTransactionBalance.setCurrency(DTOIntCardStatementDetail.USD);
    }

    public DTOMoney getInvoiceMinimumAmountCurrency() {
        return invoiceMinimumAmountCurrency;
    }

    public void setInvoiceMinimumAmountCurrency(DTOMoney invoiceMinimumAmountCurrency) {
        this.invoiceMinimumAmountCurrency = invoiceMinimumAmountCurrency;
        this.invoiceMinimumAmountCurrency.setCurrency(DTOIntCardStatementDetail.USD);
    }

    public DTOMoney getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(DTOMoney invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
        this.invoiceAmount.setCurrency(DTOIntCardStatementDetail.USD);
    }
}
