package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;

public class DTOIntCashRefund {

    private String id;
    private String description;
    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardCashRefund.class})
    private String refundType;
    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardCashRefund.class})
    private DTOIntRefundAmount refundAmount;
    private DTOIntReceivingAccount receivingAccount;
    private Calendar operationDate;
    private Calendar accoutingDate;
    private DTOIntReceivedAmount receivedAmount;
    private DTOIntExchangeRate exchangeRate;
    private DTOIntTaxes taxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public DTOIntRefundAmount getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(DTOIntRefundAmount refundAmount) {
        this.refundAmount = refundAmount;
    }

    public DTOIntReceivingAccount getReceivingAccount() {
        return receivingAccount;
    }

    public void setReceivingAccount(DTOIntReceivingAccount receivingAccount) {
        this.receivingAccount = receivingAccount;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public Calendar getAccoutingDate() {
        return accoutingDate;
    }

    public void setAccoutingDate(Calendar accoutingDate) {
        this.accoutingDate = accoutingDate;
    }

    public DTOIntReceivedAmount getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(DTOIntReceivedAmount receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public DTOIntExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(DTOIntExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public DTOIntTaxes getTaxes() {
        return taxes;
    }

    public void setTaxes(DTOIntTaxes taxes) {
        this.taxes = taxes;
    }
}
