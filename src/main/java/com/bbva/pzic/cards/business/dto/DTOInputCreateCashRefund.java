package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOInputCreateCashRefund {

    @NotNull(groups = ValidationGroup.CreateCardCashRefund.class)
    private String cardId;
    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardCashRefund.class})
    private DTOIntCashRefund cashRefund;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public DTOIntCashRefund getCashRefund() {
        return cashRefund;
    }

    public void setCashRefund(DTOIntCashRefund cashRefund) {
        this.cashRefund = cashRefund;
    }
}
