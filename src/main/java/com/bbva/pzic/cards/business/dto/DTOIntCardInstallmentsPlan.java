package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created on 3/07/2017.
 *
 * @author Entelgy
 */
public class DTOIntCardInstallmentsPlan {

    @NotNull(groups = {ValidationGroup.CreateInstallmentsPlan.class, ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.CreateInstallmentsPlanV0.class})
    @Size(max = 16, groups = {ValidationGroup.CreateInstallmentsPlan.class, ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.SimulateInstallmentsPlanV0.class, ValidationGroup.CreateInstallmentsPlanV0.class})
    private String cardId;

    @NotNull(groups = {ValidationGroup.CreateInstallmentsPlan.class, ValidationGroup.CreateInstallmentsPlanV0.class})
    @Size(max = 20, groups = {ValidationGroup.CreateInstallmentsPlan.class, ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.SimulateInstallmentsPlanV0.class, ValidationGroup.CreateInstallmentsPlanV0.class})
    private String transactionId;

    @NotNull(groups = {ValidationGroup.CreateInstallmentsPlan.class, ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.SimulateInstallmentsPlanV0.class, ValidationGroup.CreateInstallmentsPlanV0.class})
    @Digits(integer = 2, fraction = 0, groups = {ValidationGroup.CreateInstallmentsPlan.class, ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.SimulateInstallmentsPlanV0.class, ValidationGroup.CreateInstallmentsPlanV0.class})
    private Integer termsNumber;

    @NotNull(groups = ValidationGroup.SimulateInstallmentsPlanV00.class)
    @Digits(integer = 13, fraction = 2, groups = {ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.SimulateInstallmentsPlanV0.class})
    private BigDecimal amount;

    @NotNull(groups = ValidationGroup.SimulateInstallmentsPlanV00.class)
    @Size(max = 3, groups = {ValidationGroup.SimulateInstallmentsPlanV00.class, ValidationGroup.SimulateInstallmentsPlanV0.class})
    private String currency;

    public Integer getTermsNumber() {
        return termsNumber;
    }

    public void setTermsNumber(Integer termsNumber) {
        this.termsNumber = termsNumber;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
