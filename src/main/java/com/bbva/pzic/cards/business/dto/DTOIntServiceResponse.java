package com.bbva.pzic.cards.business.dto;

public class DTOIntServiceResponse<T> {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
