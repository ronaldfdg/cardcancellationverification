package com.bbva.pzic.cards.business.dto;

public class InputGetCardShipment {

    private String shipmentId;
    private String expand;

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getExpand() {
        return expand;
    }

    public void setExpand(String expand) {
        this.expand = expand;
    }
}
