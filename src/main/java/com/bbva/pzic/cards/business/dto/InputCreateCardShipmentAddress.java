package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class InputCreateCardShipmentAddress {

    @NotNull(groups = ValidationGroup.CreateCardShipmentAddress.class)
    private String shipmentId;
    @NotNull(groups = ValidationGroup.CreateCardShipmentAddress.class)
    private String addressType;
    @NotNull(groups = ValidationGroup.CreateCardShipmentAddress.class)
    @Valid
    private DTOIntAddressType shipmentAddress;

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public DTOIntAddressType getShipmentAddress() {
        return shipmentAddress;
    }

    public void setShipmentAddress(DTOIntAddressType shipmentAddress) {
        this.shipmentAddress = shipmentAddress;
    }
}
