package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOIntShippingDevice {

    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private String id;
    private String macAddress;

    @Valid
    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private DTOIntApplication application;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public DTOIntApplication getApplication() {
        return application;
    }

    public void setApplication(DTOIntApplication application) {
        this.application = application;
    }
}
