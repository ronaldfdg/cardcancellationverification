package com.bbva.pzic.cards.business.dto;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class DTOIntAddressType {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
