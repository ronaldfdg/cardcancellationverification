package com.bbva.pzic.cards.business.dto;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class InputListCardShipments {

    private String cardAgreement;
    private String referenceNumber;
    private String externalCode;
    private String shippingCompanyId;

    public String getCardAgreement() {
        return cardAgreement;
    }

    public void setCardAgreement(String cardAgreement) {
        this.cardAgreement = cardAgreement;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public String getShippingCompanyId() {
        return shippingCompanyId;
    }

    public void setShippingCompanyId(String shippingCompanyId) {
        this.shippingCompanyId = shippingCompanyId;
    }
}
