package com.bbva.pzic.cards.business.dto;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
public class DTOIntSpecificContact {

    private String contactDetailType;
    private String contactDetailTypeOriginal;
    private String address;
    private String number;
    private String phoneType;
    private String countryId;
    private String regionalCode;
    private DTOIntPhoneCompanyProposal phoneCompany;
    private String username;
    private String socialNetwork;
    private String id;

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getContactDetailTypeOriginal() {
        return contactDetailTypeOriginal;
    }

    public void setContactDetailTypeOriginal(String contactDetailTypeOriginal) {
        this.contactDetailTypeOriginal = contactDetailTypeOriginal;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getRegionalCode() {
        return regionalCode;
    }

    public void setRegionalCode(String regionalCode) {
        this.regionalCode = regionalCode;
    }

    public DTOIntPhoneCompanyProposal getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(DTOIntPhoneCompanyProposal phoneCompany) {
        this.phoneCompany = phoneCompany;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(String socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
