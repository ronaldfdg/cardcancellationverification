package com.bbva.pzic.cards.business.dto;

public class DTOIntRelatedContracts {

    private String relatedContractId;
    private String contractId;
    private String number;
    private DTOIntNumberType numberType;
    private DTOIntProduct product;

    public String getRelatedContractId() {
        return relatedContractId;
    }

    public void setRelatedContractId(String relatedContractId) {
        this.relatedContractId = relatedContractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntNumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(DTOIntNumberType numberType) {
        this.numberType = numberType;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }
}
