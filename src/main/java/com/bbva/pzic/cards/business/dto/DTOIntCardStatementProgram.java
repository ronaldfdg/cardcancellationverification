package com.bbva.pzic.cards.business.dto;

/**
 * Created on 11/07/2016.
 *
 * @author Entelgy
 */
public class DTOIntCardStatementProgram {

    private String lifeMilesNumber;
    private Integer monthPoints;
    private Integer totalPoints;
    private Integer bonus;

    public String getLifeMilesNumber() {
        return lifeMilesNumber;
    }

    public void setLifeMilesNumber(String lifeMilesNumber) {
        this.lifeMilesNumber = lifeMilesNumber;
    }

    public Integer getMonthPoints() {
        return monthPoints;
    }

    public void setMonthPoints(Integer monthPoints) {
        this.monthPoints = monthPoints;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }
}
