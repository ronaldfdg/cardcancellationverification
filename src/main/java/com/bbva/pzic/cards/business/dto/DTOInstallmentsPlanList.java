package com.bbva.pzic.cards.business.dto;

import com.bbva.pzic.cards.canonic.InstallmentsPlan;

import java.util.List;

/**
 * Created on 08/02/2018.
 *
 * @author Entelgy
 */
public class DTOInstallmentsPlanList {

    private List<InstallmentsPlan> data;
    private DTOIntPagination pagination;

    public List<InstallmentsPlan> getData() {
        return data;
    }

    public void setData(List<InstallmentsPlan> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}
