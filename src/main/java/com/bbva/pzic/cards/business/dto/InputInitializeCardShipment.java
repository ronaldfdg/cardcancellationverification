package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class InputInitializeCardShipment {

    @Valid
    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private DTOIntParticipant participant;

    @Valid
    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private DTOIntShippingCompany shippingCompany;

    @Valid
    @NotNull(groups = ValidationGroup.InitializeCardShipment.class)
    private DTOIntShippingDevice shippingDevice;
    private String externalCode;

    public DTOIntParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(DTOIntParticipant participant) {
        this.participant = participant;
    }

    public DTOIntShippingCompany getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(DTOIntShippingCompany shippingCompany) {
        this.shippingCompany = shippingCompany;
    }

    public DTOIntShippingDevice getShippingDevice() {
        return shippingDevice;
    }

    public void setShippingDevice(DTOIntShippingDevice shippingDevice) {
        this.shippingDevice = shippingDevice;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }
}
