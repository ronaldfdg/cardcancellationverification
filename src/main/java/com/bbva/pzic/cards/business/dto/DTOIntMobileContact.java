package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntMobileContact {
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String contactType;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String number;

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
