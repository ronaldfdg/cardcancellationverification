package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntCardProposal {

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntTitle title;
    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardsCardProposal.class, ValidationGroup.UpdateCardProposal.class})
    private DTOIntCardType cardType;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntAuthorizedParticipant participant;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(min = 1, groups = ValidationGroup.CreateCardsCardProposal.class)
    private List<DTOIntImport> grantedCredits;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntDelivery delivery;
    @Valid
    private DTOIntBankType bank;
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 20, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String offerId;
    private String id;
    private Date operationDate;
    @Valid
    private DTOIntFee fees;
    @Valid
    private DTOIntRate rates;
    @Valid
    private List<DTOIntAdditionalProduct> additionalProducts;
    @Valid
    private DTOIntMembership membership;
    @Valid
    @NotNull(groups = ValidationGroup.UpdateCardProposal.class)
    private DTOIntProduct product;
    @Valid
    @NotNull(groups = ValidationGroup.UpdateCardProposal.class)
    private DTOIntPhysicalSupport physicalSupport;
    @Valid
    private List<DTOIntDelivery> deliveries;
    @Valid
    private DTOIntPaymentMethod paymentMethod;
    @Valid
    private DTOIntSpecificProposalContact contact;
    @Valid
    private DTOIntImage image;
    @Valid
    private DTOIntContactAbility contactAbility;

    public DTOIntTitle getTitle() {
        return title;
    }

    public void setTitle(DTOIntTitle title) {
        this.title = title;
    }

    public DTOIntCardType getCardType() {
        return cardType;
    }

    public void setCardType(DTOIntCardType cardType) {
        this.cardType = cardType;
    }

    public DTOIntAuthorizedParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(DTOIntAuthorizedParticipant participant) {
        this.participant = participant;
    }

    public List<DTOIntImport> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<DTOIntImport> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public DTOIntDelivery getDelivery() {
        return delivery;
    }

    public void setDelivery(DTOIntDelivery delivery) {
        this.delivery = delivery;
    }

    public DTOIntBankType getBank() {
        return bank;
    }

    public void setBank(DTOIntBankType bank) {
        this.bank = bank;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getOperationDate() {
        if (operationDate == null) {
            return null;
        }
        return new Date(operationDate.getTime());
    }

    public void setOperationDate(Date operationDate) {
        if (operationDate == null) {
            this.operationDate = null;
        } else {
            this.operationDate = new Date(operationDate.getTime());
        }
    }

    public DTOIntFee getFees() {
        return fees;
    }

    public void setFees(DTOIntFee fees) {
        this.fees = fees;
    }

    public DTOIntRate getRates() {
        return rates;
    }

    public void setRates(DTOIntRate rates) {
        this.rates = rates;
    }

    public List<DTOIntAdditionalProduct> getAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(List<DTOIntAdditionalProduct> additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public DTOIntMembership getMembership() {
        return membership;
    }

    public void setMembership(DTOIntMembership membership) {
        this.membership = membership;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public DTOIntPhysicalSupport getPhysicalSupport() {
        return physicalSupport;
    }

    public void setPhysicalSupport(DTOIntPhysicalSupport physicalSupport) {
        this.physicalSupport = physicalSupport;
    }

    public List<DTOIntDelivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<DTOIntDelivery> deliveries) {
        this.deliveries = deliveries;
    }

    public DTOIntPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(DTOIntPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public DTOIntSpecificProposalContact getContact() {
        return contact;
    }

    public void setContact(DTOIntSpecificProposalContact contact) {
        this.contact = contact;
    }

    public DTOIntImage getImage() {
        return image;
    }

    public void setImage(DTOIntImage image) {
        this.image = image;
    }

    public DTOIntContactAbility getContactAbility() {
        return contactAbility;
    }

    public void setContactAbility(DTOIntContactAbility contactAbility) {
        this.contactAbility = contactAbility;
    }
}
