package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
public class InputGetCardFinancialStatement {

    @Size(max = 19, groups = {
            ValidationGroup.GetConditions.class,
            ValidationGroup.GetCardFinancialStatementV1.class})
    private String cardId;
    @Size(max = 26, groups = ValidationGroup.GetCardFinancialStatementV1.class)
    private String financialStatementId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatement.class)
    private String contentType;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getFinancialStatementId() {
        return financialStatementId;
    }

    public void setFinancialStatementId(String financialStatementId) {
        this.financialStatementId = financialStatementId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}