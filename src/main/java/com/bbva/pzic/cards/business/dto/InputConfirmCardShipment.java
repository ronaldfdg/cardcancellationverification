package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class InputConfirmCardShipment {

    @NotNull(groups = ValidationGroup.ConfirmCardShipment.class)
    private String shipmentId;
    @NotNull(groups = ValidationGroup.ConfirmCardShipment.class)
    private String biometricId;
    @NotNull(groups = ValidationGroup.ConfirmCardShipment.class)
    private String shipmentAddressId;
    @NotNull(groups = ValidationGroup.ConfirmCardShipment.class)
    private BigDecimal currentAddressLocationGeolocationLatitude;
    @NotNull(groups = ValidationGroup.ConfirmCardShipment.class)
    private BigDecimal currentAddressLocationGeolocationLongitude;

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getBiometricId() {
        return biometricId;
    }

    public void setBiometricId(String biometricId) {
        this.biometricId = biometricId;
    }

    public String getShipmentAddressId() {
        return shipmentAddressId;
    }

    public void setShipmentAddressId(String shipmentAddressId) {
        this.shipmentAddressId = shipmentAddressId;
    }

    public BigDecimal getCurrentAddressLocationGeolocationLatitude() {
        return currentAddressLocationGeolocationLatitude;
    }

    public void setCurrentAddressLocationGeolocationLatitude(BigDecimal currentAddressLocationGeolocationLatitude) {
        this.currentAddressLocationGeolocationLatitude = currentAddressLocationGeolocationLatitude;
    }

    public BigDecimal getCurrentAddressLocationGeolocationLongitude() {
        return currentAddressLocationGeolocationLongitude;
    }

    public void setCurrentAddressLocationGeolocationLongitude(BigDecimal currentAddressLocationGeolocationLongitude) {
        this.currentAddressLocationGeolocationLongitude = currentAddressLocationGeolocationLongitude;
    }
}
