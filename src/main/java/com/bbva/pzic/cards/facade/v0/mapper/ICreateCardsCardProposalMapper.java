package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardsCardProposal;
import com.bbva.pzic.cards.canonic.CardProposal;
import com.bbva.pzic.cards.canonic.CardProposalData;

import javax.ws.rs.core.Response;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public interface ICreateCardsCardProposalMapper {

    InputCreateCardsCardProposal mapIn(String cardId, CardProposal cardProposal);

    Response mapOut(CardProposalData data);
}
