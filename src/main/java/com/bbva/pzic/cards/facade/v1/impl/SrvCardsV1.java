package com.bbva.pzic.cards.facade.v1.impl;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent;
import com.bbva.jee.arq.spring.core.servicing.annotations.*;
import com.bbva.pzic.cards.business.ISrvIntCardsV1;
import com.bbva.pzic.cards.facade.v1.ISrvCardsV1;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.facade.v1.mapper.*;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.ws.rs.PATCH;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.cards.facade.RegistryIds.*;
import static com.bbva.pzic.cards.util.Enums.*;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Path("/v1")
@SN(registryID = "SNPE1900014", logicalID = "cards")
@VN(vnn = "v1")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvCardsV1 implements ISrvCardsV1, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvCardsV1.class);

    public UriInfo uriInfo;
    public HttpHeaders httpHeaders;

    @Autowired
    private ISrvIntCardsV1 srvIntCards;

    @Autowired
    private IListCardProposalsMapper listCardProposalsMapper;
    @Autowired
    private ICreateCardProposalMapper createCardProposalMapper;
    @Autowired
    private IUpdateCardProposalMapper updateCardProposalMapper;
    @Autowired
    private IModifyCardProposalMapper modifyCardProposalMapper;
    @Resource(name = "createCardMapperV1")
    private ICreateCardMapper createCardMapper;
    @Autowired
    private ICreateCardDeliveryMapper createCardDeliveryMapper;
    @Autowired
    private IGetCardSecurityDataV1Mapper getCardSecurityDataMapper;

    @Autowired
    private IModifyCardsActivationsMapper modifyCardsActivationsMapper;

    @Autowired
    private IGetCardCancellationVerificationV1Mapper getCardCancellationVerificationV1Mapper;

    @Autowired
    private ISimulateCardTransactionTransactionRefundMapper simulateCardTransactionTransactionRefundMapper;

    @Autowired
    private IReimburseCardTransactionTransactionRefundMapper reimburseCardTransactionTransactionRefundMapper;

    @Resource(name = "getCardFinancialStatementV1Mapper")
    private IGetCardFinancialStatementV1Mapper getCardFinancialStatementV1Mapper;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;

    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setUriInfo(UriInfo ui) {
        this.uriInfo = ui;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/proposals")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_CARD_PROPOSALS, logicalID = "listCardProposals")
    public ServiceResponse<List<Proposal>> listCardProposals(
            @QueryParam("status") final String status) {
        LOG.info("----- Invoking service listCardProposals -----");

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("status", status);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_PROPOSALS, null, null, queryParams);

        ServiceResponse<List<Proposal>> serviceResponse = listCardProposalsMapper.mapOut(
                srvIntCards.listCardProposals(
                        listCardProposalsMapper.mapIn((String) queryParams.get("status"))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_PROPOSALS, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/proposals")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARD_PROPOSAL, logicalID = "createCardProposal", forcedCatalog = "gabiCatalog")
    public ServiceResponseCreated<ProposalCard> createCardProposal(final ProposalCard proposalCard) {
        LOG.info("... called method SrvCardsV1.createCardProposal ...");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_PROPOSAL, proposalCard, null, null);

        ServiceResponseCreated<ProposalCard> serviceResponse = createCardProposalMapper.mapOut(
                srvIntCards.createCardProposal(
                        createCardProposalMapper.mapIn(proposalCard)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_PROPOSAL, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PUT
    @Path("/proposals/{proposal-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_UPDATE_CARD_PROPOSAL, logicalID = "updateCardProposal")
    public ServiceResponse<Proposal> updateCardProposal(
            @PathParam("proposal-id") final String proposalId,
            final Proposal proposal) {
        LOG.info("----- Invoking service updateCardProposal -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put("proposal-id", proposalId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_UPDATE_CARD_PROPOSAL, proposal, pathParams, null);

        ServiceResponse<Proposal> serviceResponse = updateCardProposalMapper.mapOut(
                srvIntCards.updateCardProposal(
                        updateCardProposalMapper.mapIn(pathParams.get("proposal-id").toString(), proposal)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_UPDATE_CARD_PROPOSAL, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PATCH
    @Path("/proposals/{proposal-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARD_PROPOSAL, logicalID = "modifyCardProposal")
    public ServiceResponse<Proposal> modifyCardProposal(
            @PathParam("proposal-id") final String proposalId,
            final Proposal proposal) {
        LOG.info("----- Invoking service modifyCardProposal -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put("proposal-id", proposalId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_PROPOSAL, proposal, pathParams, null);

        ServiceResponse<Proposal> serviceResponse = modifyCardProposalMapper.mapOut(
                srvIntCards.modifyCardProposal(
                        modifyCardProposalMapper.mapIn((String) pathParams.get("proposal-id"), proposal)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_PROPOSAL, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/cards")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARD_V1, logicalID = "createCard")
    public ServiceResponseCreated<CardPost> createCard(final CardPost cardPost) {
        LOG.info("----- Invoking service createCard -----");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_V1, cardPost, null, null);

        ServiceResponseCreated<CardPost> serviceResponse = createCardMapper.mapOut(
                srvIntCards.createCard(
                        createCardMapper.mapIn(cardPost)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_V1, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/cards/{card-id}/deliveries")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARD_DELIVERY, logicalID = "createCardDelivery")
    public ServiceResponseCreated<Delivery> createCardDelivery(
            @DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId,
            final Delivery delivery) {
        LOG.info("----- Invoking service createCardDelivery -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_DELIVERY, delivery, pathParams, null);

        ServiceResponseCreated<Delivery> serviceResponse = createCardDeliveryMapper.mapOut(
                srvIntCards.createCardDelivery(
                        createCardDeliveryMapper.mapIn(pathParams.get(CARD_ID).toString(), delivery)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_DELIVERY, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/security-data")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA_V1, logicalID = "getCardSecurityData")
    public ServiceResponse<List<SecurityData>> getCardSecurityData(
            @CasContract @SecurityFunction(inFunction = "decypher") @PathParam(CARD_ID) final String cardId,
            @QueryParam("publicKey") final String publicKey) {
        LOG.info("----- Invoking service getCardSecurityData -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("publicKey", publicKey);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA_V1, null, pathParams, queryParams);

        ServiceResponse<List<SecurityData>> serviceResponse = getCardSecurityDataMapper.mapOut(
                srvIntCards.getCardSecurityData(
                        getCardSecurityDataMapper.mapIn(
                                (String) pathParams.get(CARD_ID),
                                (String) queryParams.get("publicKey")
                        )
                ));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA_V1, serviceResponse, null, null);

        return serviceResponse;
    }


    @Override
    @PATCH
    @Path("/cards/activations/{activation-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARDS_ACTIVATIONS, logicalID = "modifyCardsActivations")
    public ServiceResponseNoContent modifyCardsActivations(
            @PathParam(ACTIVATION_ID) final String activationId,
            final Activation activation) {
        LOG.info("----- Invoking service modifyCardsActivations -----");

        HashMap<String, Object> pathParams = new HashMap<>();
        pathParams.put(ACTIVATION_ID, activationId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARDS_ACTIVATIONS, activation, pathParams, null);

        srvIntCards.modifyCardsActivations(modifyCardsActivationsMapper.mapIn((String) pathParams.get(ACTIVATION_ID), activation));

        return ServiceResponseNoContent.ServiceResponseNoContentBuilder.build();
    }

    @Override
    @GET
    @Path("/cards/{card-id}/cancellation-verification")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_CANCELLATION_VERIFICATION_V1, logicalID = "getCardCancellationVerification")
    public ServiceResponse<CancellationVerification> getCardCancellationVerification(
            @DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId) {
        LOG.info("----- Invoking service getCardCancellationVerification -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_CANCELLATION_VERIFICATION_V1, null, pathParams, null);

        ServiceResponse<CancellationVerification> serviceResponse = getCardCancellationVerificationV1Mapper.mapOut(
                srvIntCards.getCardCancellationVerification(getCardCancellationVerificationV1Mapper.mapIn(
                        (String) pathParams.get(CARD_ID)
                ))
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_CANCELLATION_VERIFICATION_V1, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/financial-statements/{financial-statement-id}")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT_V1, logicalID = "getCardFinancialStatement ")
    public ServiceResponse<FinancialStatement> getCardFinancialStatement(@PathParam(CARD_ID) final String cardId,
                                                                         @PathParam(FINANCIAL_STATEMENT_ID) final String financialStatementId) {
        LOG.info("----- Invoking service getCardFinancialStatement -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(FINANCIAL_STATEMENT_ID, financialStatementId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT_V1, null, pathParams, null);

        ServiceResponse<FinancialStatement> serviceResponse = getCardFinancialStatementV1Mapper.mapOut(
                srvIntCards.getCardFinancialStatement(getCardFinancialStatementV1Mapper.mapIn(
                        (String) pathParams.get(CARD_ID),
                        (String) pathParams.get(FINANCIAL_STATEMENT_ID)
                ))
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT_V1, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/cards/{card-id}/transactions/{transaction-id}/transaction-refunds/simulate")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_SIMULATE_CARD_TRANSACTION_REFUND, logicalID = "simulateCardTransactionTransactionRefund")
    public ServiceResponse<SimulateTransactionRefund> simulateCardTransactionTransactionRefund(@PathParam(CARD_ID) final String cardId,
                                                                                               @PathParam(TRANSACTION_ID) final String transactionId,
                                                                                               final SimulateTransactionRefund simulateTransactionRefund) {
        LOG.info("----- Invoking service simulateCardTransactionTransactionRefund -----");
        HashMap<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(TRANSACTION_ID, transactionId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SIMULATE_CARD_TRANSACTION_REFUND, simulateTransactionRefund, pathParams, null);

        ServiceResponse<SimulateTransactionRefund> serviceResponse = simulateCardTransactionTransactionRefundMapper.mapOut(
                srvIntCards.simulateCardTransactionTransactionRefund(
                        simulateCardTransactionTransactionRefundMapper.mapIn((String) pathParams.get(CARD_ID), (String) pathParams.get(TRANSACTION_ID), simulateTransactionRefund)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SIMULATE_CARD_TRANSACTION_REFUND, serviceResponse, pathParams, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/cards/{card-id}/transactions/{transaction-id}/transaction-refunds/reimburse")
    @SMC(registryID = SMC_REGISTRY_ID_OF_REIMBURSE_CARD_TRANSACTION_TRANSACTION_REFUND, logicalID = "reimburseCardTransactionTransactionRefund")
    public ServiceResponse<ReimburseCardTransactionTransactionRefund> reimburseCardTransactionTrasactionRefund(@PathParam(CARD_ID) final String cardId,
                                                                                                               @PathParam(TRANSACTION_ID) final String transactionId,
                                                                                                               final ReimburseCardTransactionTransactionRefund input) {
        LOG.info("----- Invoking service reimburseCardTransactionTrasactionRefund -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(TRANSACTION_ID, transactionId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_REIMBURSE_CARD_TRANSACTION_TRANSACTION_REFUND, input, pathParams, null);

        ServiceResponse serviceResponse = reimburseCardTransactionTransactionRefundMapper.mapOut(
                srvIntCards.reimburseCardTransactionTransactionRefund(
                        reimburseCardTransactionTransactionRefundMapper.mapIn(
                                (String) pathParams.get(CARD_ID), (String) pathParams.get(TRANSACTION_ID), input)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_REIMBURSE_CARD_TRANSACTION_TRANSACTION_REFUND, serviceResponse, pathParams, null);
        return serviceResponse;
    }

}