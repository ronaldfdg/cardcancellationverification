package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "transaction", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "transaction", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Unique identifier of the transaction.
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
