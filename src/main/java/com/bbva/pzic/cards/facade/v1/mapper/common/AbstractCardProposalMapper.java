package com.bbva.pzic.cards.facade.v1.mapper.common;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 12/2/2019.
 *
 * @author Entelgy
 */
public abstract class AbstractCardProposalMapper implements ICardProposalMapper {

    public DTOIntMembership mapInMembership(final Membership membership) {
        if (membership == null) {
            return null;
        }

        DTOIntMembership dtoIntMembership = new DTOIntMembership();
        dtoIntMembership.setId(membership.getId());
        return dtoIntMembership;
    }

    public List<DTOIntAdditionalProduct> mapInAdditionalProducts(final List<AdditionalProduct> additionalProducts) {
        if (CollectionUtils.isEmpty(additionalProducts)) {
            return null;
        }

        return additionalProducts.stream().map(this::mapInAdditionalProduct).collect(Collectors.toList());
    }

    private DTOIntAdditionalProduct mapInAdditionalProduct(final AdditionalProduct ap) {
        DTOIntAdditionalProduct dtoIntAdditionalProduct = new DTOIntAdditionalProduct();
        dtoIntAdditionalProduct.setProductType(ap.getProductType());
        dtoIntAdditionalProduct.setAmount(ap.getAmount());
        dtoIntAdditionalProduct.setCurrency(ap.getCurrency());
        return dtoIntAdditionalProduct;
    }

    public DTOIntFee mapInFees(final TransferFees fees) {
        if (fees == null) {
            return null;
        }

        DTOIntFee dtoIntFee = new DTOIntFee();
        dtoIntFee.setItemizeFees(mapInItemizeFees(fees.getItemizeFees()));
        return dtoIntFee;

    }

    private List<DTOIntItemizeFee> mapInItemizeFees(final List<ItemizeFee> itemizeFees) {
        if (CollectionUtils.isEmpty(itemizeFees)) {
            return null;
        }

        return itemizeFees.stream().map(this::mapInItemizeFee).collect(Collectors.toList());

    }

    private DTOIntItemizeFee mapInItemizeFee(final ItemizeFee ifees) {
        DTOIntItemizeFee dtoIntItemizeFee = new DTOIntItemizeFee();
        dtoIntItemizeFee.setFeeType(ifees.getFeeType());
        dtoIntItemizeFee.setItemizeFeeUnit(mapInItemizeFreeUnit(ifees.getItemizeFeeUnit()));
        return dtoIntItemizeFee;
    }

    private DTOIntItemizeFeeUnit mapInItemizeFreeUnit(final ItemizeFeeUnit itemizeFeeUnit) {
        if (itemizeFeeUnit == null) {
            return null;
        }

        DTOIntItemizeFeeUnit dtoIntItemizeFeeUnit = new DTOIntItemizeFeeUnit();
        dtoIntItemizeFeeUnit.setUnitType(itemizeFeeUnit.getUnitType());
        dtoIntItemizeFeeUnit.setAmount(itemizeFeeUnit.getAmount());
        dtoIntItemizeFeeUnit.setCurrency(itemizeFeeUnit.getCurrency());
        return dtoIntItemizeFeeUnit;
    }

    public DTOIntRate mapInRates(final ProposalRate rates) {
        if (rates == null) {
            return null;
        }

        DTOIntRate dtoIntRate = new DTOIntRate();
        dtoIntRate.setItemizeRates(mapInItemizeRates(rates.getItemizeRates()));
        return dtoIntRate;
    }

    private List<DTOIntItemizeRate> mapInItemizeRates(final List<ItemizeRate> itemizeRates) {
        if (CollectionUtils.isEmpty(itemizeRates)) {
            return null;
        }

        return itemizeRates.stream().map(this::mapInItemizeRate).collect(Collectors.toList());
    }

    private DTOIntItemizeRate mapInItemizeRate(final ItemizeRate ir) {
        DTOIntItemizeRate dtoIntItemizeRate = new DTOIntItemizeRate();
        dtoIntItemizeRate.setRateType(ir.getRateType());
        dtoIntItemizeRate.setCalculationDate(ir.getCalculationDate());
        dtoIntItemizeRate.setItemizeRatesUnit(mapInItemizeRatesUnit(ir.getItemizeRatesUnit()));
        return dtoIntItemizeRate;
    }

    private DTOIntItemizeRatesUnit mapInItemizeRatesUnit(final ItemizeRateUnit itemizeRatesUnit) {
        if (itemizeRatesUnit == null) {
            return null;
        }
        DTOIntItemizeRatesUnit dtoIntItemizeRatesUnit = new DTOIntItemizeRatesUnit();
        dtoIntItemizeRatesUnit.setUnitRateType(itemizeRatesUnit.getUnitRateType());
        dtoIntItemizeRatesUnit.setPercentage(itemizeRatesUnit.getPercentage());
        return dtoIntItemizeRatesUnit;
    }

    public DTOIntSpecificProposalContact mapInContact(final SpecificProposalContact contact) {
        if (contact == null) {
            return null;
        }

        DTOIntSpecificProposalContact dtoIntContact = new DTOIntSpecificProposalContact();
        dtoIntContact.setContactType(contact.getContactType());
        dtoIntContact.setContact(mapInSpecificProposalContact(contact.getContact()));
        return dtoIntContact;
    }

    private DTOIntMobileProposal mapInSpecificProposalContact(final MobileProposal contact) {
        if (contact == null) {
            return null;
        }
        DTOIntMobileProposal dtoIntMobileProposal = new DTOIntMobileProposal();
        dtoIntMobileProposal.setContactDetailType(contact.getContactDetailType());
        dtoIntMobileProposal.setNumber(contact.getNumber());
        dtoIntMobileProposal.setPhoneCompany(mapInPhoneCompany(contact.getPhoneCompany()));
        return dtoIntMobileProposal;

    }

    private DTOIntPhoneCompanyProposal mapInPhoneCompany(final PhoneCompanyProposal phoneCompany) {
        if (phoneCompany == null) {
            return null;
        }

        DTOIntPhoneCompanyProposal dtoIntPhoneCompanyProposal = new DTOIntPhoneCompanyProposal();
        dtoIntPhoneCompanyProposal.setId(phoneCompany.getId());
        return dtoIntPhoneCompanyProposal;
    }

    public List<DTOIntImport> mapInGrantedCredits(List<Import> grantedCredits) {
        if (CollectionUtils.isEmpty(grantedCredits)) {
            return null;
        }

        return grantedCredits.stream().map(this::mapInGrantedCredit).collect(Collectors.toList());
    }

    private DTOIntImport mapInGrantedCredit(final Import gc) {
        DTOIntImport dtoIntGrantedCredits = new DTOIntImport();
        dtoIntGrantedCredits.setAmount(gc.getAmount());
        dtoIntGrantedCredits.setCurrency(gc.getCurrency());
        return dtoIntGrantedCredits;
    }

    public DTOIntPaymentMethod mapInPaymentMethod(final com.bbva.pzic.cards.facade.v1.dto.PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }

        DTOIntPaymentMethod dtoIntPaymentMethod = new DTOIntPaymentMethod();
        dtoIntPaymentMethod.setId(paymentMethod.getId());
        dtoIntPaymentMethod.setFrequency(mapInFrequency(paymentMethod.getFrecuency()));
        return dtoIntPaymentMethod;
    }

    private DTOIntFrequency mapInFrequency(Frecuency frequency) {
        if (frequency == null) {
            return null;
        }

        DTOIntFrequency dtoIntFrequency = new DTOIntFrequency();
        dtoIntFrequency.setId(frequency.getId());
        dtoIntFrequency.setDaysOfMonth(mapInDaysOfMonth(frequency.getDaysOfMonth()));
        return dtoIntFrequency;
    }

    private DTOIntDaysOfMonth mapInDaysOfMonth(final DaysOfMonth daysOfMonth) {
        if (daysOfMonth == null) {
            return null;
        }

        DTOIntDaysOfMonth dtoIntDaysOfMonth = new DTOIntDaysOfMonth();
        dtoIntDaysOfMonth.setDay(daysOfMonth.getDay());
        dtoIntDaysOfMonth.setCutOffDay(daysOfMonth.getCutOffDay());
        return dtoIntDaysOfMonth;
    }

    public List<DTOIntDelivery> mapInDeliveries(final List<Delivery> deliveries) {
        if (CollectionUtils.isEmpty(deliveries)) {
            return null;
        }

        return deliveries.stream().map(this::mapInDelivery).collect(Collectors.toList());
    }

    private DTOIntDelivery mapInDelivery(final Delivery delivery) {
        DTOIntDelivery dtoIntDelivery = new DTOIntDelivery();

        if (delivery.getServiceType() != null) {
            dtoIntDelivery.setServiceTypeId(delivery.getServiceType().getId());
        }
        if (delivery.getContact() != null) {
            dtoIntDelivery.setContact(mapInDeliveryContact(delivery.getContact()));
        }
        if (delivery.getAddress() != null) {
            dtoIntDelivery.setAddress(mapInAddress(delivery.getAddress()));
        }
        if (delivery.getDestination() != null) {
            dtoIntDelivery.setDestination(mapInDestination(delivery.getDestination()));
        }

        return dtoIntDelivery;
    }


    private DTOIntDestination mapInDestination(final Destination destination) {
        DTOIntDestination dtoIntDestination = new DTOIntDestination();
        dtoIntDestination.setId(destination.getId());
        dtoIntDestination.setBranch(mapInBranch(destination.getBranch()));

        return dtoIntDestination;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if (branch == null) {
            return null;
        }

        DTOIntBranch dtoIntBranch = new DTOIntBranch();
        dtoIntBranch.setId(branch.getId());
        return dtoIntBranch;
    }

    private DTOIntAddress mapInAddress(final Address address) {
        DTOIntAddress dtoIntAddress = new DTOIntAddress();
        dtoIntAddress.setId(address.getId());
        dtoIntAddress.setAddressType(address.getAddressType());
        dtoIntAddress.setLocation(mapInLocation(address.getLocation()));
        return dtoIntAddress;
    }

    protected abstract DTOIntLocation mapInLocation(Location location);

    protected List<DTOIntAddressComponents> mapInAddressComponents(List<AddressComponents> addressComponents) {
        if (CollectionUtils.isEmpty(addressComponents)) {
            return null;
        }

        return addressComponents.stream().map(this::mapInAddressComponent).collect(Collectors.toList());
    }

    private DTOIntAddressComponents mapInAddressComponent(final AddressComponents adc) {
        DTOIntAddressComponents dtoIntAddressComponents = new DTOIntAddressComponents();
        dtoIntAddressComponents.setComponentTypes(adc.getComponentTypes());
        dtoIntAddressComponents.setCode(adc.getCode());
        dtoIntAddressComponents.setName(adc.getName());

        return dtoIntAddressComponents;
    }

    private DTOIntContact mapInDeliveryContact(final Contact contact) {
        DTOIntContact dtoIntContact = new DTOIntContact();
        dtoIntContact.setContactType(contact.getContactType());
        if (contact.getContact() != null) {
            dtoIntContact.setContactDetailType(contact.getContact().getContactDetailType());
            dtoIntContact.setContactAddress(contact.getContact().getAddress());
        }

        return dtoIntContact;
    }

    public DTOIntPhysicalSupport mapInPhysicalSupport(final PhysicalSupport physicalSupport) {
        if (physicalSupport == null) {
            return null;
        }

        DTOIntPhysicalSupport dtoIntPhysicalSupport = new DTOIntPhysicalSupport();
        dtoIntPhysicalSupport.setId(physicalSupport.getId());
        return dtoIntPhysicalSupport;
    }

    public DTOIntProduct mapInProduct(final Product product) {
        if (product == null) {
            return null;
        }

        DTOIntProduct dtoIntProduct = new DTOIntProduct();
        dtoIntProduct.setId(product.getId());
        if (product.getSubproduct() == null) {
            return dtoIntProduct;
        }

        DTOIntSubProduct dtoIntSubProduct = new DTOIntSubProduct();
        dtoIntSubProduct.setId(product.getSubproduct().getId());
        dtoIntProduct.setSubproduct(dtoIntSubProduct);
        return dtoIntProduct;
    }

    public DTOIntCardType mapInCardType(final CardType cardType) {
        if (cardType == null) {
            return null;
        }
        DTOIntCardType dtoIntCardType = new DTOIntCardType();
        dtoIntCardType.setId(cardType.getId());
        return dtoIntCardType;
    }

    public DTOIntImage mapInImage(final Image image) {
        if (image == null) {
            return null;
        }

        DTOIntImage dtoIntImage = new DTOIntImage();
        dtoIntImage.setId(image.getId());
        dtoIntImage.setName(image.getName());
        dtoIntImage.setUrl(image.getUrl());
        return dtoIntImage;
    }

    public DTOIntContactAbility mapInContactAbility(final ContactAbility contactability) {
        if (contactability == null) {
            return null;
        }

        DTOIntContactAbility dtoIntContactAbility = new DTOIntContactAbility();
        dtoIntContactAbility.setReason(contactability.getReason());
        dtoIntContactAbility.setScheduletimes(mapInScheduleTimes(contactability.getScheduleTimes()));
        return dtoIntContactAbility;
    }

    protected abstract DTOIntScheduletimes mapInScheduleTimes(final ScheduleTimes scheduletimes);

    public List<DTOIntParticipant> mapInParticipants(final List<KnowParticipant> participantList) {
        if (participantList == null) {
            return null;
        }

        List<DTOIntParticipant> dtoIntParticipantList = new ArrayList<>(participantList.size());
        for (KnowParticipant participant : participantList) {
            DTOIntParticipant dtoIntParticipant = new DTOIntParticipant();
            dtoIntParticipant.setPersonType(participant.getPersonType());
            dtoIntParticipant.setId(participant.getId());
            dtoIntParticipant.setIsCustomer(participant.getIsCustomer());
            dtoIntParticipant.setParticipantTypeId(participant.getParticipantType() == null ? null : participant.getParticipantType().getId());
            dtoIntParticipantList.add(dtoIntParticipant);
        }
        return dtoIntParticipantList;
    }

}