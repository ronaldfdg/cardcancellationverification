package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "shipmentStatus", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "shipmentStatus", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShipmentStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Shipment status identifier.
     */
    private String id;
    /**
     * Shipment status description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
