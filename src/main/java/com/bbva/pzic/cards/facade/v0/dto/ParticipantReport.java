package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "ParticipantReport", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "ParticipantReport", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParticipantReport implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String firstName;

    private String middleName;

    private String lastName;

    private String secondLastName;

    private List<IdentityDocument> identityDocuments;

    private List<ContactDetailReport> contactDetails;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public List<IdentityDocument> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(List<IdentityDocument> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public List<ContactDetailReport> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetailReport> contactDetails) {
        this.contactDetails = contactDetails;
    }
}
