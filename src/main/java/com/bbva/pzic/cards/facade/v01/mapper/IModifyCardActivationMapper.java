package com.bbva.pzic.cards.facade.v01.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.canonic.ActivationData;

/**
 * @author Entelgy
 */
public interface IModifyCardActivationMapper {

    /**
     * Crea una nueva instancia de {@link DTOIntActivation} y la inicializa con los parámetros enviados.
     *
     * @param cardId
     * @param activationId
     * @param activation
     * @return the created object.
     */
    DTOIntActivation mapInput(String cardId, String activationId, Activation activation);

    ActivationData mapOut(DTOIntActivation dtoIntActivation);
}
