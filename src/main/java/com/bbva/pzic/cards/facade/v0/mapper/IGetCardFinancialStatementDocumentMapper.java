package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.dao.model.filenet.Document;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

/**
 * Created on 29/07/2018.
 *
 * @author Entelgy
 */
public interface IGetCardFinancialStatementDocumentMapper {
    MultipartBody mapOut(final Document documents, final String contentType);

}
