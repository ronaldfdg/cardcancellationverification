package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.canonic.Conditions;

import java.util.List;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
public interface IGetCardConditionsMapper {

    InputGetCardConditions mapIn(String cardId);

    Conditions mapOut(List<Condition> data);
}