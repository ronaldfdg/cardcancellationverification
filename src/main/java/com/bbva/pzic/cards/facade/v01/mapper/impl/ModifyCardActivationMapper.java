package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.canonic.ActivationData;
import com.bbva.pzic.cards.facade.v01.mapper.IModifyCardActivationMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION;

/**
 * @author Entelgy
 */
@Mapper
public class ModifyCardActivationMapper implements IModifyCardActivationMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardActivationMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntActivation mapInput(String cardId, String activationId, Activation activation) {
        LOG.info("... called method ModifyCardActivationMapper.mapInput ...");
        final DTOIntActivation dtoIntActivation = new DTOIntActivation();
        final DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION));
        dtoIntActivation.setCard(dtoIntCard);
        dtoIntActivation.setActivationId(activationId);
        dtoIntActivation.setIsActive(activation.getIsActive());
        return dtoIntActivation;
    }

    @Override
    public ActivationData mapOut(DTOIntActivation dtoIntActivation) {
        LOG.info("... called method ModifyCardActivationMapper.mapOut ...");
        final Activation activation = new Activation();
        activation.setActivationId(dtoIntActivation.getActivationId());
        activation.setIsActive(dtoIntActivation.getIsActive());
        final ActivationData activationData = new ActivationData();
        activationData.setData(activation);
        return activationData;
    }

}