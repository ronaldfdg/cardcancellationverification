package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.Block;

/**
 * @author Entelgy
 */
public interface IModifyCardBlockMapper {

    /**
     * Crea una nueva instancia de {@link DTOIntBlock} y la inicializa con los parámetros enviados.
     *
     * @param cardId
     * @param blockId
     * @param block
     * @return the created object.
     */
    DTOIntBlock mapInput(String cardId, String blockId, Block block);

}