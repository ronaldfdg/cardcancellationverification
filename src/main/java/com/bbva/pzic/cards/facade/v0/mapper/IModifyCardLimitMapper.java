package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
public interface IModifyCardLimitMapper {

    InputModifyCardLimit mapIn(String cardId, String limitId, Limit limit);

    ServiceResponse<Limit> mapOut(Limit limit);
}