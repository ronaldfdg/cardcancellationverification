package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputRefundType;
import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.RefundType;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.mapper.IReimburseCardTransactionTransactionRefundMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.cards.util.Enums.TRANSACTIONREFUND_REFUNDTYPE_ID;

@Component
public class ReimburseCardTransactionTransactionRefundMapper implements IReimburseCardTransactionTransactionRefundMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    private static final Log LOG = LogFactory.getLog(ReimburseCardTransactionTransactionRefundMapper.class);

    @Override
    public InputReimburseCardTransactionTransactionRefund mapIn(final String cardId, final String transactionId, final ReimburseCardTransactionTransactionRefund input) {
        LOG.info("... called method ReimburseCardTransactionTransactionRefundMapper.mapIn ...");
        InputReimburseCardTransactionTransactionRefund dtoInt = new InputReimburseCardTransactionTransactionRefund();
        dtoInt.setCardId(cardId);
        dtoInt.setTransactionId(transactionId);
        dtoInt.setRefundType(mapInRefundType(input.getRefundType()));
        return dtoInt;
    }

    private InputRefundType mapInRefundType(final RefundType refundType) {
        if (refundType == null) {
            return null;
        }

        InputRefundType inputRefundType = new InputRefundType();
        inputRefundType.setId(translator.translateFrontendEnumValueStrictly(TRANSACTIONREFUND_REFUNDTYPE_ID, refundType.getId()));
        return inputRefundType;
    }

    @Override
    public ServiceResponse<ReimburseCardTransactionTransactionRefund> mapOut(final ReimburseCardTransactionTransactionRefund outPut) {
        LOG.info("... called method ReimburseCardTransactionTransactionRefund.mapOut ...");
        if (outPut == null) {
            return null;
        }

        return ServiceResponse.data(outPut).build();
    }
}
