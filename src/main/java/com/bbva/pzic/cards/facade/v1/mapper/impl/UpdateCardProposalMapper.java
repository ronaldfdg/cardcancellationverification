package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCardProposal;
import com.bbva.pzic.cards.business.dto.InputUpdateCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import com.bbva.pzic.cards.facade.v1.mapper.IUpdateCardProposalMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.AbstractCardProposalMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
@Mapper
public class UpdateCardProposalMapper implements IUpdateCardProposalMapper {

    private static final Log LOG = LogFactory.getLog(UpdateCardProposalMapper.class);

    private AbstractCardProposalMapper cardProposalMapper;

    @Resource(name = "cardProposalDateTimeMapper")
    public void setAddressMapper(AbstractCardProposalMapper addressMapper) {
        this.cardProposalMapper = addressMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputUpdateCardProposal mapIn(final String proposalId, final Proposal proposal) {
        LOG.info("... called method UpdateCardProposalMapper.mapIn ...");
        InputUpdateCardProposal input = new InputUpdateCardProposal();
        input.setProposalId(proposalId);
        input.setProposal(mapInProposal(proposal));
        return input;
    }

    private DTOIntCardProposal mapInProposal(Proposal proposal) {
        DTOIntCardProposal input = new DTOIntCardProposal();
        input.setCardType(cardProposalMapper.mapInCardType(proposal.getCardType()));
        input.setProduct(cardProposalMapper.mapInProduct(proposal.getProduct()));
        input.setPhysicalSupport(cardProposalMapper.mapInPhysicalSupport(proposal.getPhysicalSupport()));
        input.setDeliveries(cardProposalMapper.mapInDeliveries(proposal.getDeliveries()));
        input.setPaymentMethod(cardProposalMapper.mapInPaymentMethod(proposal.getPaymentMethod()));
        input.setGrantedCredits(cardProposalMapper.mapInGrantedCredits(proposal.getGrantedCredits()));
        input.setContact(cardProposalMapper.mapInContact(proposal.getContact()));
        input.setRates(cardProposalMapper.mapInRates(proposal.getRates()));
        input.setFees(cardProposalMapper.mapInFees(proposal.getFees()));
        input.setAdditionalProducts(cardProposalMapper.mapInAdditionalProducts(proposal.getAdditionalProducts()));
        input.setMembership(cardProposalMapper.mapInMembership(proposal.getMembership()));
        input.setImage(cardProposalMapper.mapInImage(proposal.getImage()));
        input.setContactAbility(cardProposalMapper.mapInContactAbility(proposal.getContactability()));
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<Proposal> mapOut(final Proposal proposal) {
        LOG.info("... called method UpdateCardProposalMapper.mapOut ...");
        if (proposal == null) {
            return null;
        }
        return ServiceResponse.data(proposal).pagination(null).build();
    }
}
