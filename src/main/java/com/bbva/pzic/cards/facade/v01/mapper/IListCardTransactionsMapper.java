package com.bbva.pzic.cards.facade.v01.mapper;

import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;

/**
 * Created on 06/02/2017.
 *
 * @author Entelgy
 */
public interface IListCardTransactionsMapper {

    /**
     * Creates a new {@link DTOInputListCardTransactions} instance and initializes it with the provided parameters.
     *
     * @param haveFinancingType
     * @param cardId
     * @param fromOperationDate
     * @param toOperationDate
     * @param paginationKey
     * @param pageSize
     * @return the created object.
     */
    DTOInputListCardTransactions mapInput(boolean haveFinancingType, String cardId, String fromOperationDate, String toOperationDate, String paginationKey, Long pageSize, String registryId);

}
