package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
public interface IGetCardMapper {

    DTOIntCard mapIn(String cardId);

    ServiceResponse<Card> mapOut(Card card);
}
