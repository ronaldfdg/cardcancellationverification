package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.Shipment;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardShipmentMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 18/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class GetCardShipmentMapper implements IGetCardShipmentMapper {

    private static final Log LOG = LogFactory.getLog(GetCardShipmentMapper.class);

    @Override
    public InputGetCardShipment mapIn(final String shipmentId, final String expand) {
        LOG.info("... called method GetCardShipmentMapper.mapIn ...");
        InputGetCardShipment input = new InputGetCardShipment();
        input.setShipmentId(shipmentId);
        input.setExpand(expand);
        return input;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<Shipment> mapOut(final Shipment data) {
        LOG.info("... called method GetCardShipmentMapper.mapOut ...");
        if (data == null) {
            return null;
        }

        return ServiceResponse.data(data).build();
    }
}
