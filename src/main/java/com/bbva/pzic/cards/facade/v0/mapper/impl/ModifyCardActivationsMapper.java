package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardActivationsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 9/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyCardActivationsMapper implements IModifyCardActivationsMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardActivationsMapper.class);

    private AbstractCypherTool cypherTool;
    private Translator translator;

    @Autowired
    public void setCypherTool(AbstractCypherTool cypherTool) {
        this.cypherTool = cypherTool;
    }

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public InputModifyCardActivations mapIn(final String cardId, final List<Activation> items) {
        LOG.info("... called method ModifyCardActivationsMapper.mapIn ...");
        InputModifyCardActivations dtoInt = new InputModifyCardActivations();
        dtoInt.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATIONS));

        if (items.isEmpty()) {
            return dtoInt;
        }

        for (int i = 0; i < items.size(); i++) {
            if (i == 0) {
                dtoInt.setDtoIntActivationPosition1(getDTOIntActivation(items.get(i)));
            } else if (i == 1) {
                dtoInt.setDtoIntActivationPosition2(getDTOIntActivation(items.get(i)));
            } else if (i == 2) {
                dtoInt.setDtoIntActivationPosition3(getDTOIntActivation(items.get(i)));
            } else if (i == 3) {
                dtoInt.setDtoIntActivationPosition4(getDTOIntActivation(items.get(i)));
            } else if (i == 4) {
                dtoInt.setDtoIntActivationPosition5(getDTOIntActivation(items.get(i)));
            }
        }
        return dtoInt;
    }

    private DTOIntActivation getDTOIntActivation(final Activation activation) {
        DTOIntActivation dtoIntActivation = new DTOIntActivation();
        dtoIntActivation.setIsActive(activation.getIsActive());
        dtoIntActivation.setActivationId(translator.translateFrontendEnumValueStrictly("cards.activation.activationId", activation.getActivationId()));
        return dtoIntActivation;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<Activation>> mapOut(final List<Activation> activations) {
        LOG.info("... called method ModifyCardActivationsMapper.mapOut ...");
        if (CollectionUtils.isEmpty(activations)) {
            return null;
        }
        return ServiceResponse.data(activations).build();
    }
}
