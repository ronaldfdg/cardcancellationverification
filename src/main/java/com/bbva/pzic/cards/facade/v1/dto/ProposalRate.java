package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "proposalRate", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "proposalRate", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProposalRate implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Detail of each rate.
     */
    private List<ItemizeRate> itemizeRates;

    public List<ItemizeRate> getItemizeRates() {
        return itemizeRates;
    }

    public void setItemizeRates(List<ItemizeRate> itemizeRates) {
        this.itemizeRates = itemizeRates;
    }
}
