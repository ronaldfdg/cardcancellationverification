package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContracts;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.NumberType;
import com.bbva.pzic.cards.facade.v0.dto.Product;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardRelatedContractsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Mapper
public class ListCardRelatedContractsMapper implements IListCardRelatedContractsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardRelatedContractsMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntRelatedContractsSearchCriteria mapInput(String cardId) {
        LOG.info("----- Dentro de ListCardRelatedContractsMapper.mapInput() -----");
        DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria = new DTOIntRelatedContractsSearchCriteria();
        dtoIntRelatedContractsSearchCriteria.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARDID));
        return dtoIntRelatedContractsSearchCriteria;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<RelatedContracts>> mapOutput(List<DTOIntRelatedContracts> dtoIntRelatedContracts) {
        LOG.info("----- Dentro de ListCardRelatedContractsMapper.mapOutput() -----");

        if (dtoIntRelatedContracts == null || dtoIntRelatedContracts.isEmpty()) {
            return null;
        }

        List<RelatedContracts> relatedContractsList = new ArrayList<>();

        for (DTOIntRelatedContracts dtoIntRelatedContract : dtoIntRelatedContracts) {

            RelatedContracts relatedContracts = new RelatedContracts();

            relatedContracts.setRelatedContractId(dtoIntRelatedContract.getRelatedContractId());
            relatedContracts.setContractId(dtoIntRelatedContract.getContractId());
            relatedContracts.setNumber(dtoIntRelatedContract.getNumber());

            if (dtoIntRelatedContract.getNumberType() != null) {
                NumberType numberType = new NumberType();
                numberType.setId(dtoIntRelatedContract.getNumberType().getId());
                numberType.setName(dtoIntRelatedContract.getNumberType().getName());
                relatedContracts.setNumberType(numberType);
            }

            if (dtoIntRelatedContract.getProduct() != null) {
                Product product = new Product();
                product.setId(dtoIntRelatedContract.getProduct().getId());
                product.setName(dtoIntRelatedContract.getProduct().getName());
                relatedContracts.setProduct(product);
            }

            relatedContractsList.add(relatedContracts);
        }

        return ServiceResponse.data(relatedContractsList).build();
    }
}
