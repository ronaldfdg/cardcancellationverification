package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.canonic.Proposal;
import com.bbva.pzic.cards.canonic.ProposalData;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public interface ICreateCardsProposalMapper {

    DTOIntProposal mapIn(Proposal proposal);

    ProposalData mapOut(Proposal proposal);
}
