package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "participantCards", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "participantCards", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParticipantCards implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Card contract agreement. It's the identifier of the card contract containing the card. A card contract can contain multiple cards, with different PAN numbers.
     */
    @DatoAuditable(omitir = true)
    private String cardAgreement;
    /**
     * Partial card number. The courier only have twelve digits of the card.
     */
    @DatoAuditable(omitir = true)
    private String referenceNumber;

    public String getCardAgreement() {
        return cardAgreement;
    }

    public void setCardAgreement(String cardAgreement) {
        this.cardAgreement = cardAgreement;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
}
