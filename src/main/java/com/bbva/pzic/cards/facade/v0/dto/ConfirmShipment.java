package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "confirmShipment", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "confirmShipment", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfirmShipment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Biometric validation identifier of the customer obtained in the shipment process.
     */
    @DatoAuditable(omitir = true)
    private String biometricId;
    /**
     * One of the set of addresses selected by the customer to make the shipment.
     */
    private ShipmentAddress shipmentAddress;
    /**
     * Geographical address, obtained from the device, where the shipment is being made.
     */
    private CurrentAddress currentAddress;

    public String getBiometricId() {
        return biometricId;
    }

    public void setBiometricId(String biometricId) {
        this.biometricId = biometricId;
    }

    public ShipmentAddress getShipmentAddress() {
        return shipmentAddress;
    }

    public void setShipmentAddress(ShipmentAddress shipmentAddress) {
        this.shipmentAddress = shipmentAddress;
    }

    public CurrentAddress getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(CurrentAddress currentAddress) {
        this.currentAddress = currentAddress;
    }
}
