package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.RelatedContract;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardsV0Mapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardsV0Mapper implements IListCardsV0Mapper {

    private static final Log LOG = LogFactory.getLog(ListCardsV0Mapper.class);

    private AbstractCypherTool cypherTool;

    public ListCardsV0Mapper(AbstractCypherTool cypherTool) {
        this.cypherTool = cypherTool;
    }

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.IListCardsMapper#mapIn(String, java.util.List, String, java.util.List, String, Integer)
     */
    @Override
    public DTOIntListCards mapIn(final String customerId, final List<String> cardTypeId, final String physicalSupportId,
                                 final List<String> statusId, final List<String> participantsParticipantTypeId, final String paginationKey, final Integer pageSize) {
        LOG.info("... called method ListCardsV0Mapper.mapIn ...");
        DTOIntListCards dtoIntListCards = new DTOIntListCards();
        dtoIntListCards.setCustomerId(customerId);
        dtoIntListCards.setCardTypeId(cardTypeId.isEmpty() ? null : cardTypeId);
        dtoIntListCards.setPhysicalSupportId(physicalSupportId);
        dtoIntListCards.setStatusId(statusId.isEmpty() ? null : statusId);
        dtoIntListCards.setParticipantsParticipantTypeId(participantsParticipantTypeId.isEmpty() ? null : participantsParticipantTypeId);
        dtoIntListCards.setPaginationKey(paginationKey);
        dtoIntListCards.setPageSize(pageSize);
        return dtoIntListCards;
    }

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.IListCardsMapper#mapOut(com.bbva.pzic.cards.business.dto.DTOOutListCards, String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<Card>> mapOut(final DTOOutListCards listCards, final Pagination pagination) {
        LOG.info("... called method ListCardsV0Mapper.mapOut ...");
        List<Card> data = listCards.getData();
        if (data == null || data.isEmpty()) {
            return null;
        }

        encryptRelatedContractNumbers(data);

        return ServiceResponse.data(data).pagination(pagination).build();
    }

    private void encryptRelatedContractNumbers(List<Card> cards) {
        for (final Card card : cards) {
            card.setCardId(cypherTool.encrypt(card.getNumber(), AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_LIST_CARDS));
            if (card.getRelatedContracts() != null) {
                for (final RelatedContract relatedContract : card.getRelatedContracts()) {
                    relatedContract.setRelatedContractId(cypherTool.encrypt(relatedContract.getRelatedContractId(), AbstractCypherTool.IDCOREL, SMC_REGISTRY_ID_OF_LIST_CARDS));
                    relatedContract.setContractId(cypherTool.encrypt(relatedContract.getNumber(), AbstractCypherTool.NUCOREL, SMC_REGISTRY_ID_OF_LIST_CARDS));
                }
            }
        }
    }
}

