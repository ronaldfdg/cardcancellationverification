package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "paymentMethod", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "paymentMethod", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Payment method type identifier
     */
    private String id;
    /**
     * Payment method period selected by the user for paying credit debt.
     */
    private Frecuency frecuency;
    /**
     * Payment method type description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Frecuency getFrecuency() {
        return frecuency;
    }

    public void setFrecuency(Frecuency frecuency) {
        this.frecuency = frecuency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
