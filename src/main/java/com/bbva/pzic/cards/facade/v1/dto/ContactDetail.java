package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/10/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "contactDetail", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "contactDetail", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact detail identifier.
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Contact detail name
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
