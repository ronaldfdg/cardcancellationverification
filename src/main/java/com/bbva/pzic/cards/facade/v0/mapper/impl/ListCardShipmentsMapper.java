package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.facade.v0.dto.Shipments;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardShipmentsMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ListCardShipmentsMapper implements IListCardShipmentsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardShipmentsMapper.class);

    @Override
    public InputListCardShipments mapIn(final String cardAgreement, final String referenceNumber, final String externalCode, final String shippingCompanyId) {
        LOG.info("... called method ListCardShipmentsMapper.mapIn ...");
        InputListCardShipments input = new InputListCardShipments();
        input.setCardAgreement(cardAgreement);
        input.setExternalCode(externalCode);
        input.setReferenceNumber(referenceNumber);
        input.setShippingCompanyId(shippingCompanyId);
        return input;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<Shipments>> mapOut(final List<Shipments> data) {
        LOG.info("... called method ListCardShipmentsMapper.mapOut ...");
        if (CollectionUtils.isEmpty(data)) {
            return null;
        }

        return ServiceResponse.data(data).build();
    }
}
