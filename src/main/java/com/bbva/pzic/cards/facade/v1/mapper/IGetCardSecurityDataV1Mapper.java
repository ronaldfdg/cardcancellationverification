package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;

import java.util.List;

public interface IGetCardSecurityDataV1Mapper {

    InputGetCardSecurityData mapIn(String cardId, String publicKey);

    ServiceResponse<List<SecurityData>> mapOut(List<SecurityData> securityDataList);
}
