package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Block;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyPartialCardBlockMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 29/08/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyPartialCardBlockMapper implements IModifyPartialCardBlockMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private EnumMapper enumMapper;

    @Autowired
    private MapperUtils mapperUtils;

    @Override
    public DTOIntBlock mapIn(String cardId, String blockId, Block block) {
        DTOIntBlock dtoIntBlock = new DTOIntBlock();
        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_PARTIAL_CARD_BLOCK));
        dtoIntBlock.setCard(dtoIntCard);
        dtoIntBlock.setBlockId(enumMapper.getBackendValue("cards.block.blockId", blockId));
        if (block.getReason() != null) {
            dtoIntBlock.setReasonId(block.getReason().getId());
        }
        dtoIntBlock.setIsActive(mapperUtils.convertBooleanToString(block.getIsActive() == null ? Boolean.TRUE : block.getIsActive()));

        return dtoIntBlock;
    }
}
