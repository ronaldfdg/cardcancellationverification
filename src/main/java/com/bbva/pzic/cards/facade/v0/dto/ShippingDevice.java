package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "shippingDevice", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "shippingDevice", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShippingDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Device identifier.
     */
    private String id;
    /**
     * Device mac address.
     */
    private String macAddress;
    /**
     * Mobile application.
     */
    private Application application;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
