package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardsMaskedToken;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.converter.builtin.DateToStringConverter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardsMaskedToken implements ICreateCardsMaskedToken {

    @Autowired
    private AbstractCypherTool cypherTool;
    private DateToStringConverter dateToStringConverter;

    @PostConstruct
    public void init() {
        dateToStringConverter = new DateToStringConverter("MMyyyy");
    }

    @Override
    public InputCreateCardsMaskedToken mapIn(final String cardId, final MaskedToken maskedToken) {
        InputCreateCardsMaskedToken input = new InputCreateCardsMaskedToken();
        input.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARD_ID));
        if (maskedToken.getExpirationDate() != null) {
            input.setExpirationDate(dateToStringConverter.convertTo(maskedToken.getExpirationDate(), null));
        }
        return input;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<MaskedToken> mapOut(final MaskedToken data) {
        if (data == null) {
            return null;
        }
        return ServiceResponse.data(data).build();
    }
}
