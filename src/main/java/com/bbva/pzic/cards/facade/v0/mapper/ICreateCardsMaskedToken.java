package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
public interface ICreateCardsMaskedToken {

    InputCreateCardsMaskedToken mapIn(String cardId, MaskedToken maskedToken);

    ServiceResponse<MaskedToken> mapOut(MaskedToken data);
}
