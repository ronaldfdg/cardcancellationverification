package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.facade.v1.dto.FinancialStatement;
import com.bbva.pzic.cards.facade.v1.mapper.IGetCardFinancialStatementV1Mapper;
import org.springframework.stereotype.Component;

@Component("getCardFinancialStatementV1Mapper")
public class GetCardFinancialStatementV1Mapper implements IGetCardFinancialStatementV1Mapper {

    @Override
    public InputGetCardFinancialStatement mapIn(final String cardId, final String financialStatementId) {
        InputGetCardFinancialStatement input = new InputGetCardFinancialStatement();
        input.setCardId(cardId);
        input.setFinancialStatementId(financialStatementId);
        return input;
    }

    @Override
    public ServiceResponse<FinancialStatement> mapOut(final FinancialStatement cardFinancialStatement) {
        if (cardFinancialStatement == null) {
            return null;
        }

        return ServiceResponse.data(cardFinancialStatement).build();
    }
}
