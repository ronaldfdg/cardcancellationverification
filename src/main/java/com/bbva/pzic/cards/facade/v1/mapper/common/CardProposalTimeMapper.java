package com.bbva.pzic.cards.facade.v1.mapper.common;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.DTOIntLocation;
import com.bbva.pzic.cards.business.dto.DTOIntScheduletimes;
import com.bbva.pzic.cards.facade.v1.dto.Location;
import com.bbva.pzic.cards.facade.v1.dto.ScheduleTimes;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.DateMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 26/06/2020.
 *
 * @author Entelgy.
 */
@Mapper("cardProposalTimeMapper")
public class CardProposalTimeMapper extends AbstractCardProposalMapper {

    @Override
    protected DTOIntScheduletimes mapInScheduleTimes(final ScheduleTimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        DTOIntScheduletimes dtoIntScheduletimes = new DTOIntScheduletimes();
        dtoIntScheduletimes.setEndtime(DateMapper.buildCalendar(scheduletimes.getEndTime()));
        dtoIntScheduletimes.setStarttime(DateMapper.buildCalendar(scheduletimes.getStartTime()));
        return dtoIntScheduletimes;
    }

    @Override
    protected DTOIntLocation mapInLocation(final Location location) {
        if (location == null) {
            return null;
        }

        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        dtoIntLocation.setAddressComponents(mapInAddressComponents(location.getAddressComponents()));
        dtoIntLocation.setAdditionalInformation(location.getAdditionalInformation());
        return dtoIntLocation;
    }
}