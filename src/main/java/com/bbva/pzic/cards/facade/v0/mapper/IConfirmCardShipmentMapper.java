package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.ConfirmShipment;

public interface IConfirmCardShipmentMapper {

    InputConfirmCardShipment mapIn(String shipmentId, ConfirmShipment confirmShipment);
}
