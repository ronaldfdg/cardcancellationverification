package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.canonic.RelatedContract;
import com.bbva.pzic.cards.canonic.RelatedContractData;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
public interface ICreateCardRelatedContractV0Mapper {
    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId          unique card identifier
     * @param relatedContract Object with the data that relates the card and the contract.
     * @return Object with the datas of input
     */
    DTOInputCreateCardRelatedContract mapIn(String cardId, RelatedContract relatedContract);

    /**
     * Method that exposes an object from an output DTO
     *
     * @param dtoOut Object with the datas of output
     * @return Object that exposes the data provided by HOST
     */
    RelatedContractData mapOut(DTOOutCreateCardRelatedContract dtoOut);

}
