package com.bbva.pzic.cards.facade.v00.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;

/**
 * Created on 3/07/2017.
 *
 * @author Entelgy
 */
public interface ICreateCardInstallmentsPlanMapper {

    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId           unique card identifier
     * @param installmentsPlan Object with the data of the card
     * @return Object with the data of input
     */
    DTOIntCardInstallmentsPlan mapIn(String cardId, InstallmentsPlan installmentsPlan);

}
