package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.facade.v0.mapper.IInitializeCardShipmentMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 19/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class InitializeCardShipmentMapper implements IInitializeCardShipmentMapper {

    private static final Log LOG = LogFactory.getLog(InitializeCardShipmentMapper.class);

    @Override
    public InputInitializeCardShipment mapIn(final InitializeShipment initializeShipment) {
        LOG.info("... called method InitializeCardShipmentMapper.mapIn ...");
        InputInitializeCardShipment input = new InputInitializeCardShipment();
        input.setParticipant(mapInParticipant(initializeShipment.getParticipant()));
        input.setShippingCompany(mapInShippingCompamy(initializeShipment.getShippingCompany()));
        input.setShippingDevice(mapInShippingDevice(initializeShipment.getShippingDevice()));
        input.setExternalCode(initializeShipment.getExternalCode());
        return input;
    }

    private DTOIntShippingDevice mapInShippingDevice(final ShippingDevice shippingDevice) {
        if (shippingDevice == null) {
            return null;
        }

        DTOIntShippingDevice dtoIntShippingDevice = new DTOIntShippingDevice();
        dtoIntShippingDevice.setId(shippingDevice.getId());
        dtoIntShippingDevice.setMacAddress(shippingDevice.getMacAddress());
        dtoIntShippingDevice.setApplication(mapInApplication(shippingDevice.getApplication()));
        return dtoIntShippingDevice;
    }

    private DTOIntApplication mapInApplication(final Application application) {
        if (application == null) {
            return null;
        }

        DTOIntApplication dtoIntApplication = new DTOIntApplication();
        dtoIntApplication.setName(application.getName());
        dtoIntApplication.setVersion(application.getVersion());
        return dtoIntApplication;
    }

    private DTOIntShippingCompany mapInShippingCompamy(final ShippingCompany shippingCompany) {
        if (shippingCompany == null) {
            return null;
        }

        DTOIntShippingCompany dtoIntShippingCompany = new DTOIntShippingCompany();
        dtoIntShippingCompany.setId(shippingCompany.getId());
        return dtoIntShippingCompany;
    }

    private DTOIntParticipant mapInParticipant(final Participant participant) {
        if (participant == null) {
            return null;
        }

        DTOIntParticipant dtoIntParticipant = new DTOIntParticipant();
        dtoIntParticipant.setIdentityDocument(mapInIdentityDocument(participant.getIdentityDocument()));
        dtoIntParticipant.setCard(mapInCard(participant.getCard()));
        return dtoIntParticipant;
    }

    private DTOIntCard mapInCard(final ParticipantCards card) {
        if (card == null) {
            return null;
        }

        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardAgreement(card.getCardAgreement());
        dtoIntCard.setReferenceNumber(card.getReferenceNumber());
        return dtoIntCard;
    }

    private DTOIntIdentityDocument mapInIdentityDocument(final IdentityDocument identityDocument) {
        if (identityDocument == null) {
            return null;
        }

        DTOIntIdentityDocument dtoIntIdentityDocument = new DTOIntIdentityDocument();
        dtoIntIdentityDocument.setNumber(identityDocument.getDocumentNumber());
        dtoIntIdentityDocument.setDocumentType(mapInDocumentType(identityDocument.getDocumentType()));
        return dtoIntIdentityDocument;
    }

    private DTOIntDocumentType mapInDocumentType(final DocumentType documentType) {
        if (documentType == null) {
            return null;
        }

        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        dtoIntDocumentType.setId(documentType.getId());
        return dtoIntDocumentType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<InitializeShipment> mapOut(final InitializeShipment data) {
        LOG.info("... called method InitializeCardShipmentMapper.mapOut ...");
        if (data == null) {
            return null;
        }

        return ServiceResponse.data(data).build();
    }
}
