package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.canonic.Conditions;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardConditionsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
@Mapper
public class GetCardConditionsMapper implements IGetCardConditionsMapper {

    private static final Log LOG = LogFactory.getLog(GetCardConditionsMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputGetCardConditions mapIn(final String cardId) {
        LOG.info("... called method GetConditionsMapper.mapIn ...");

        String decrypt = cypherTool.decrypt(cardId, AbstractCypherTool.CARD_ID, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_CONDITIONS);

        InputGetCardConditions input = new InputGetCardConditions();
        input.setCardId(decrypt);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Conditions mapOut(final List<Condition> data) {
        LOG.info("... called method GetConditionsMapper.mapOut ...");
        if (data.isEmpty()) {
            return null;
        }
        Conditions conditions = new Conditions();
        conditions.setData(data);
        return conditions;
    }
}