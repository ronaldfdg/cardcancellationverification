package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "financialStatement", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "financialStatement", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancialStatement implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String financialStatementNumber;

    private Integer transactionsNumber;

    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date cutOffDate;

    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date businessDayCutoffDate;

    private Integer billingPeriodDaysNumber;

    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date nextPaymentDate;

    private List<Amount> grantedCredits;

    private List<Amount> availableBalances;

    private DisposedBalanceStatement disposedBalance;

    private List<RelatedContract> relatedContracts;

    private List<Currency> currencies;

    private List<Condition> conditions;

    private Rate rates;

    private List<SpecificAmount> specificAmounts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFinancialStatementNumber() {
        return financialStatementNumber;
    }

    public void setFinancialStatementNumber(String financialStatementNumber) {
        this.financialStatementNumber = financialStatementNumber;
    }

    public Integer getTransactionsNumber() {
        return transactionsNumber;
    }

    public void setTransactionsNumber(Integer transactionsNumber) {
        this.transactionsNumber = transactionsNumber;
    }

    public Date getCutOffDate() {
        return cutOffDate;
    }

    public void setCutOffDate(Date cutOffDate) {
        this.cutOffDate = cutOffDate;
    }

    public Date getBusinessDayCutoffDate() {
        return businessDayCutoffDate;
    }

    public void setBusinessDayCutoffDate(Date businessDayCutoffDate) {
        this.businessDayCutoffDate = businessDayCutoffDate;
    }

    public Integer getBillingPeriodDaysNumber() {
        return billingPeriodDaysNumber;
    }

    public void setBillingPeriodDaysNumber(Integer billingPeriodDaysNumber) {
        this.billingPeriodDaysNumber = billingPeriodDaysNumber;
    }

    public Date getNextPaymentDate() {
        return nextPaymentDate;
    }

    public void setNextPaymentDate(Date nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    public List<Amount> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Amount> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public List<Amount> getAvailableBalances() {
        return availableBalances;
    }

    public void setAvailableBalances(List<Amount> availableBalances) {
        this.availableBalances = availableBalances;
    }

    public DisposedBalanceStatement getDisposedBalance() {
        return disposedBalance;
    }

    public void setDisposedBalance(DisposedBalanceStatement disposedBalance) {
        this.disposedBalance = disposedBalance;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public Rate getRates() {
        return rates;
    }

    public void setRates(Rate rates) {
        this.rates = rates;
    }

    public List<SpecificAmount> getSpecificAmounts() {
        return specificAmounts;
    }

    public void setSpecificAmounts(List<SpecificAmount> specificAmounts) {
        this.specificAmounts = specificAmounts;
    }
}
