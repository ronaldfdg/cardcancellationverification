package com.bbva.pzic.cards.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreatedWithNoContent;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
public interface ISrvCardsV0 {

    /**
     * Method for retrieving the list of cards related to a specific user.
     *
     * @param customerId        Customer ID
     * @param cardTypeId        Type card ID
     * @param physicalSupportId Physical Support ID
     * @param statusId          Status ID
     * @param expand            expand the selected sub-resource
     * @param paginationKey     Pagination ID
     * @param pageSize          Size of pagination
     * @return {@link List<Card>}
     */
    ServiceResponse<List<Card>> listCards(String customerId, List<String> cardTypeId, String physicalSupportId,
                                          List<String> statusId, List<String> participantsParticipantTypeId, String expand, String paginationKey, Integer pageSize);

    /**
     * Method for retrieving the list of limits related to the current card. The limits are only amount.
     *
     * @param cardId card identifier
     * @return {@link List<Limits>}
     */
    ServiceResponse<List<Limits>> listCardLimits(String cardId);

    /**
     * Method for adding a new block to the card. Independently of the kind of blocking,
     * the status of the card becomes "INOPERATIVE".
     *
     * @param cardId  card identifier
     * @param blockId block identifier
     * @param block   card block
     * @return the card block added or modified
     */
    BlockData modifyCardBlock(String cardId, String blockId, Block block);

    /**
     * Method for simulate the purchase in installments.
     *
     * @param cardId                     card identifier
     * @param installmentsPlanSimulation payload
     * @return object cardData
     */
    InstallmentsPlanSimulationData simulateCardInstallmentsPlan(String cardId, InstallmentsPlanSimulation installmentsPlanSimulation);

    /**
     * Method for simulate the purchase in installments.
     *
     * @param cardId card identifier
     */
    ServiceResponse<List<Activation>> listCardActivations(String cardId);

    /**
     * Method for simulate the purchase in installments.
     *
     * @param cardId card identifier
     * @param items  array activations
     */
    ServiceResponse<List<Activation>> modifyCardActivations(String cardId, List<Activation> items);

    /**
     * Method for simulate the purchase in installments.
     *
     * @param cardId card identifier
     * @param expand expand the selected sub-resource
     * @return object cardData
     */
    ServiceResponse<Card> getCard(String cardId, String expand);

    /**
     * @param cardId unique card identifier
     * @return {@link List<com.bbva.pzic.cards.canonic.PaymentMethod>}
     */
    ServiceResponse<List<com.bbva.pzic.cards.canonic.PaymentMethod>> getCardPaymentMethods(String cardId);

    /**
     * @param cardId unique card identifier
     * @param pin    payload
     * @return {@link Response}
     */
    Response modifyCardPin(String cardId, Pin pin);

    /**
     * Method for retrieving the security data related to the specified card.
     *
     * @param cardId    unique card identifier
     * @param publicKey key identifier
     * @return object SecurityData
     */
    SecurityDataArray getCardSecurityData(String cardId, String publicKey);

    /**
     * Method for modifying a single block of the card. Due that a card can have only
     * an active block, the possibility of succeeding in the attempt to become a block
     * as active will depend of some business rules of the bank. Attempting to become
     * active a block would be possible only if it is a more restrictive blocking than the other ones.
     * Independently of the kind of blocking, the status of the card becomes "BLOCKED".
     *
     * @param cardId  card identifier
     * @param blockId block identifier
     * @param block   payload
     */
    Response modifyPartialCardBlock(String cardId, String blockId, Block block);

    /**
     * Service for retrieving all transactions that match with searching criteria.
     * These criteria parameters may be concept, financing type, contract, operation date, transaction
     * type and the currency that was used for the differents operations.
     *
     * @param cardId            unique card identifier
     * @param fromOperationDate filters the transactions which operation date is later than this one (ISO-8601 date format)
     * @param toOperationDate   filters the transactions which operation date is earlier than this one (ISO-8601 date format)
     * @param paginationKey     key to obtain a single page
     * @param pageSize          number of elements per page
     * @return {@link List<Transaction>}
     */
    ServiceResponse<List<Transaction>> listCardTransactions(String cardId, String fromOperationDate, String toOperationDate, String paginationKey, Long pageSize);

    /**
     * Method for updating the configuration of a specific limit related to the
     * current card.
     *
     * @param cardId  unique card identifier
     * @param limitId unique limit identifier
     * @param limit   payload
     * @return {@link ServiceResponse<Limit>}
     */
    ServiceResponse<Limit> modifyCardLimit(String cardId, String limitId, Limit limit);

    /**
     * Service for retrieving the details of a given transaction.
     *
     * @param cardId        unique card identifier
     * @param transactionId unique transaction identifier
     * @return {@link Transaction}
     */
    ServiceResponse<Transaction> getCardTransaction(String cardId, String transactionId);

    /**
     * Service for creating a new card related to a specific customer.
     *
     * @param card card
     * @return {@link CardData}
     */
    CardData createCard(Card card);

    /**
     * @param cardId        unique card identifier
     * @param paginationKey key to obtain a single page
     * @param pageSize      number of elements per page
     * @return object {@link List<InstallmentsPlan>}
     */
    ServiceResponse<List<InstallmentsPlan>> listInstallmentPlans(String cardId, String paginationKey, Long pageSize);

    /**
     * Method for retrieving the purchase in installments.
     *
     * @param installmentsPlan payload
     * @param cardId           card identifier
     * @return object cardData
     */
    Response createCardInstallmentsPlan(String cardId, InstallmentsPlan installmentsPlan);

    /**
     * Method for updating a card. The status of a card can only be changed from INOPERATIVE to OPERATIVE when the card is new. Once the card has been set to operative, its status cannot be changed by using this method.
     *
     * @param cardId card identifier
     * @param card   payload
     */
    Response modifyCard(String cardId, Card card);

    /**
     * Method for adding a new related contract to a card.
     *
     * @param cardId          unique card identifier
     * @param relatedContract Object with the data that relates the card and the contract.
     * @return object RelatedContractData
     */
    RelatedContractData createCardRelatedContract(String cardId, com.bbva.pzic.cards.canonic.RelatedContract relatedContract);

    /**
     * Service for retrieving the conditions of the given card.
     *
     * @param cardId unique card identifier
     * @return {@link Conditions}
     */
    Conditions getCardConditions(String cardId);

    /**
     * Service for retrieving the list of periods related to financial
     * statements documents for determined product type.
     *
     * @param cardId        unique card identifier
     * @param isActive      filters the periods of financial statements list by active or
     *                      historic.
     * @param paginationKey key to obtain a single page
     * @param pageSize      number of elements per page
     * @return {@link StatementList}
     */
    StatementList listCardFinancialStatements(String cardId, Boolean isActive,
                                              String paginationKey, Integer pageSize);

    MultipartBody getCardFinancialStatement(String cardId, String financialStatementId);

    /**
     * Generation of the Visa token associated to a MasterCard card.
     *
     * @param cardId      unique card identifier
     * @param maskedToken payload
     * @return {@link MaskedToken}
     */
    ServiceResponse<MaskedToken> createCardsMaskedToken(String cardId, MaskedToken maskedToken);

    /**
     * Service for retrieving a membership related to a card.
     *
     * @param cardId       unique card identifier
     * @param membershipId payload
     * @return {@link com.bbva.pzic.cards.facade.v0.dto.Membership}
     */
    ServiceResponse<com.bbva.pzic.cards.facade.v0.dto.Membership> getCardMembership(String cardId, String membershipId);

    /**
     * Service for listing the detailed query of the offer sent to the customer.
     *
     * @param cardId  unique card identifier
     * @param offerId unique identifier of the offer sent to the customer.
     * @return {@link Offer}
     */
    ServiceResponse<Offer> getCardsCardOffer(String cardId, String offerId);

    /**
     * Service to recording the pre-application that the customer generated for
     * the formalization of a credit card, which is finalized when the customer
     * goes to the branch of his choice for a pick up the card. This operation
     * could be derived from an offer.
     *
     * @param proposal payload
     * @return {@link ServiceResponse<Proposal>}
     */
    Response createCardsProposal(Proposal proposal);

    /**
     * Service for retrieving the related contracts of a card.
     *
     * @param cardId unique card identifier
     * @return {@link List<RelatedContracts>}
     */
    ServiceResponse<List<RelatedContracts>> listCardRelatedContracts(String cardId);

    /**
     * Manages the pre-application that the customer has generated for the
     * formalization of an additional credit card, which is finalized when the
     * customer goes to the branch of his choice for a pick up the card or the
     * card is sent to the customer´s preferential address. This operation could
     * be derived from an offer.
     *
     * @param cardId       unique card identifier
     * @param cardProposal payload
     * @return {@link CardProposal}
     */
    Response createCardsCardProposal(String cardId, CardProposal cardProposal);

    /**
     * Service for listing the cash advances executed from the credit card.
     *
     * @param cardId unique card identifier
     * @return {@link List<CashAdvances>}
     */
    ServiceResponse<List<CashAdvances>> listCardCashAdvances(String cardId);

    /**
     * Service for simulating the hiring of a credit card.
     *
     * @param offerId          unique identifier of the offer sent to the customer.
     * @param holderSimulation Payload request
     * @return {@link HolderSimulation}
     */
    ServiceResponse<HolderSimulation> createCardsOfferSimulate(String offerId,
                                                               HolderSimulation holderSimulation);

    /**
     * It allows the creation of a refund associated to a card.
     *
     * @param cardId     unique card identifier
     * @param cashRefund payload
     * @return {@link CashRefund}
     */
    ServiceResponse<CashRefund> createCardCashRefund(String cardId, CashRefund cashRefund);

    /**
     * List of different shipments related to cards.
     *
     * @param cardAgreement     card contract agreement. It's the identifier of the card contract containing the card.
     *                          A card contract can contain multiple cards, with different PAN numbers.
     * @param referenceNumber   partial card number. The courier only have twelve digits of the card.
     * @param externalCode      external code of the shipment. This code is generated by the shipping company.
     * @param shippingCompanyId shipping company identifier.
     * @return {@link List<Shipments>}
     */
    ServiceResponse<List<Shipments>> listCardShipments(String cardAgreement,
                                                       String referenceNumber,
                                                       String externalCode,
                                                       String shippingCompanyId);

    /**
     * Register a shipment address.
     *
     * @param shipmentId      unique identifier of the shipment.
     * @param shipmentAddress payload
     * @return {@link ShipmentAddress}
     */
    ServiceResponse<ShipmentAddress> createCardShipmentAddress(String shipmentId, ShipmentAddress shipmentAddress);

    /**
     * Confirmation and validation of the current shipment.
     *
     * @param shipmentId      unique identifier of the shipment.
     * @param confirmShipment payload
     * @return no content
     */
    ServiceResponseCreatedWithNoContent confirmCardShipment(String shipmentId, ConfirmShipment confirmShipment);

    /**
     * Retrieve the information of a specific shipment.
     *
     * @param shipmentId unique identifier of the shipment.
     * @param expand     expand the selected sub-resource
     * @return {@link Shipment}
     */
    ServiceResponse<Shipment> getCardShipment(String shipmentId, String expand);

    /**
     * Initialization and validation of the shipment process.
     *
     * @param initializeShipment payload
     * @return {@link InitializeShipment}
     */
    ServiceResponse<InitializeShipment> initializeCardShipment(InitializeShipment initializeShipment);

    /**
     * Register the information for the sales channels report.
     *
     * @param reportCardCreation payload
     * @return no content
     */
    ServiceResponseNoContent createCardReports(String cardId, ReportCardCreation reportCardCreation);
}
