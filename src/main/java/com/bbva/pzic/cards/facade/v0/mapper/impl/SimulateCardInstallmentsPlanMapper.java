package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanSimulation;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.ISimulateCardInstallmentsPlanMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Mapper
public class SimulateCardInstallmentsPlanMapper implements ISimulateCardInstallmentsPlanMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntCardInstallmentsPlan mapIn(String cardId, InstallmentsPlanSimulation installmentsPlanSimulation) {
        DTOIntCardInstallmentsPlan dtoInt = new DTOIntCardInstallmentsPlan();
        dtoInt.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_SIMULATE_CARD_INSTALLMENTS_PLAN));
        dtoInt.setTransactionId(installmentsPlanSimulation.getTransactionId());
        if (installmentsPlanSimulation.getCapital() != null) {
            dtoInt.setAmount(installmentsPlanSimulation.getCapital().getAmount());
            dtoInt.setCurrency(installmentsPlanSimulation.getCapital().getCurrency());
        }
        if (installmentsPlanSimulation.getTerms() != null) {
            dtoInt.setTermsNumber(installmentsPlanSimulation.getTerms().getNumber());
        }
        return dtoInt;
    }
}
