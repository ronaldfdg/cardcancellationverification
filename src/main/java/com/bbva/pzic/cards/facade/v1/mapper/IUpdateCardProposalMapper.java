package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputUpdateCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public interface IUpdateCardProposalMapper {

    InputUpdateCardProposal mapIn(String proposalId, Proposal proposal);

    ServiceResponse<Proposal> mapOut(Proposal proposal);
}
