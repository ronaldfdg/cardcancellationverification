package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "cancellationVerification", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "cancellationVerification", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CancellationVerification implements Serializable {

    private static final long serialVersionUID = 1L;

    private CardHolder cardHolder;

    private Amount grantedCredit;

    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date cutOffDate;

    private List<RelatedContract> relatedContracts;

    private List<DebtDetail> debtDetail;

    public CardHolder getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(CardHolder cardHolder) {
        this.cardHolder = cardHolder;
    }

    public Amount getGrantedCredit() {
        return grantedCredit;
    }

    public void setGrantedCredit(Amount grantedCredit) {
        this.grantedCredit = grantedCredit;
    }

    public Date getCutOffDate() {
        return cutOffDate;
    }

    public void setCutOffDate(Date cutOffDate) {
        this.cutOffDate = cutOffDate;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public List<DebtDetail> getDebtDetail() {
        return debtDetail;
    }

    public void setDebtDetail(List<DebtDetail> debtDetail) {
        this.debtDetail = debtDetail;
    }
}
