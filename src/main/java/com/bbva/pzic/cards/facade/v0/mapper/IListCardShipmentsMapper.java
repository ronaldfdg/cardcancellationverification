package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.facade.v0.dto.Shipments;

import java.util.List;

public interface IListCardShipmentsMapper {

    InputListCardShipments mapIn(String cardAgreement, String referenceNumber, String externalCode, String shippingCompanyId);

    ServiceResponse<List<Shipments>> mapOut(List<Shipments> listCardShipments);
}
