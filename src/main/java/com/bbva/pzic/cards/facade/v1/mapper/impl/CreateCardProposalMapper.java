package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.pzic.cards.business.dto.InputCreateCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.ProposalCard;
import com.bbva.pzic.cards.facade.v1.mapper.ICreateCardProposalMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.AbstractCardProposalMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;

import javax.annotation.Resource;

/**
 * Created on 19/11/2019.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardProposalMapper implements ICreateCardProposalMapper {

    private AbstractCardProposalMapper cardProposalMapper;

    @Resource(name = "cardProposalTimeMapper")
    public void setAddressMapper(AbstractCardProposalMapper addressMapper) {
        this.cardProposalMapper = addressMapper;
    }

    @Override
    public InputCreateCardProposal mapIn(final ProposalCard proposal) {
        InputCreateCardProposal input = new InputCreateCardProposal();
        input.setCardType(cardProposalMapper.mapInCardType(proposal.getCardType()));
        input.setProduct(cardProposalMapper.mapInProduct(proposal.getProduct()));
        input.setPhysicalSupport(cardProposalMapper.mapInPhysicalSupport(proposal.getPhysicalSupport()));
        input.setGrantedCredits(cardProposalMapper.mapInGrantedCredits(proposal.getGrantedCredits()));
        input.setContact(cardProposalMapper.mapInContact(proposal.getContact()));
        input.setRates(cardProposalMapper.mapInRates(proposal.getRates()));
        input.setFees(cardProposalMapper.mapInFees(proposal.getFees()));
        input.setParticipants(cardProposalMapper.mapInParticipants(proposal.getParticipants()));
        input.setOfferId(proposal.getOfferId());
        return input;
    }

    @Override
    public ServiceResponseCreated mapOut(final ProposalCard cardProposal) {
        if (cardProposal == null) {
            return null;
        }

        return ServiceResponseCreated.data(cardProposal).build();
    }
}
