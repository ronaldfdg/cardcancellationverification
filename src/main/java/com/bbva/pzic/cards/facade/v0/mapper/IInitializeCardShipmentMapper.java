package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputInitializeCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.InitializeShipment;

public interface IInitializeCardShipmentMapper {

    InputInitializeCardShipment mapIn(InitializeShipment initializeShipment);

    ServiceResponse<InitializeShipment> mapOut(InitializeShipment initializeCardShipment);
}
