package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.Status;
import com.bbva.pzic.cards.canonic.Transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "reimburseCardTransactionTransactionRefund", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "reimburseCardTransactionTransactionRefund", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReimburseCardTransactionTransactionRefund implements Serializable {

    private static final long serialVersionUID = 1L;

    private RefundType refundType;

    private Card card;

    private Transaction transaction;

    private Status status;

    public RefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundType refundType) {
        this.refundType = refundType;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
