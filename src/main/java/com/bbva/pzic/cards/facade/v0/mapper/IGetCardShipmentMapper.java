package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.Shipment;

public interface IGetCardShipmentMapper {

    InputGetCardShipment mapIn(String memberhipId, String expand);

    ServiceResponse<Shipment> mapOut(Shipment sipment);
}
