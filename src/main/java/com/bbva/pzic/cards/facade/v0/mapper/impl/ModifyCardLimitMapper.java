package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardLimitMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyCardLimitMapper implements IModifyCardLimitMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardLimitMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public InputModifyCardLimit mapIn(final String cardId,
                                      final String limitId,
                                      final Limit limit) {
        LOG.info("... called method ModifyCardLimitMapper.mapIn ...");
        InputModifyCardLimit inputModifyCardLimit = new InputModifyCardLimit();
        inputModifyCardLimit.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_LIMIT));
        inputModifyCardLimit.setLimitId(limitId);
        if (limit.getAmountLimits() != null && !limit.getAmountLimits().isEmpty()) {
            inputModifyCardLimit.setAmountLimitAmount(limit.getAmountLimits().get(0).getAmount());
            inputModifyCardLimit.setAmountLimitCurrency(limit.getAmountLimits().get(0).getCurrency());
        }
        inputModifyCardLimit.setOfferId(limit.getOfferId());
        return inputModifyCardLimit;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<Limit> mapOut(final Limit limit) {
        LOG.info("... called method ModifyCardLimitMapper.mapOut ...");
        if (limit == null || limit.getOperationDate() == null) {
            return null;
        }
        return ServiceResponse.data(limit).pagination(null).build();
    }
}
