package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class GetCardMapper implements IGetCardMapper {

    @Override
    public DTOIntCard mapIn(final String cardId) {
        DTOIntCard dtoIn = new DTOIntCard();
        dtoIn.setCardId(cardId);
        return dtoIn;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<Card> mapOut(final Card card) {
        if (card == null) {
            return null;
        }
        return ServiceResponse.data(card).build();
    }
}
