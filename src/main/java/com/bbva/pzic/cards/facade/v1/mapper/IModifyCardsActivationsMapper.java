package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.facade.v1.dto.Activation;

/**
 * Created on 24/06/2020.
 *
 * @author Entelgy
 */
public interface IModifyCardsActivationsMapper {

    DTOIntActivation mapIn(String activationId, Activation activation);

}
