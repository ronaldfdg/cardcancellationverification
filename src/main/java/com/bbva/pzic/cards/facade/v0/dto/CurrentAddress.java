package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "currentAddress", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "currentAddress", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrentAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Address location.
     */
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
