package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "knowParticipant", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "knowParticipant", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class KnowParticipant implements Serializable {

    private static final long serialVersionUID = 1L;

    private String personType;
    /**
     * Participant identifier.
     */
    private String id;
    /**
     * Name of the person.
     */
    private String firstName;
    /**
     * Person's name occurring between the firstName and family names, as a second or additonal given name.
     */
    private String middleName;
    /**
     * Family name. Name(s) written after the first name and the middle name. First surname.
     */
    private String lastName;
    /**
     * Second surname. Mother's last name.
     */
    private String secondLastName;
    /**
     * Participation role.
     */
    private ParticipantType participantType;
    /**
     * Legal person type.
     */
    private LegalPersonType legalPersonType;
    /**
     * Indicator to identify if the participant is customer or not.
     */
    private Boolean isCustomer;

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public ParticipantType getParticipantType() {
        return participantType;
    }

    public void setParticipantType(ParticipantType participantType) {
        this.participantType = participantType;
    }

    public LegalPersonType getLegalPersonType() {
        return legalPersonType;
    }

    public void setLegalPersonType(LegalPersonType legalPersonType) {
        this.legalPersonType = legalPersonType;
    }

    public Boolean getIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(Boolean customer) {
        isCustomer = customer;
    }
}
