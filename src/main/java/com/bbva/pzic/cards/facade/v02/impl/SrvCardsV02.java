package com.bbva.pzic.cards.facade.v02.impl;

import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.cards.business.ISrvIntCardsV0;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.CardData;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardMapper;
import com.bbva.pzic.cards.facade.v02.ISrvCardsV02;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Entelgy
 */
@Path("/V02")
@SN(registryID = "SNPE1800099", logicalID = "cards")
@VN(vnn = "V02")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvCardsV02 implements ISrvCardsV02 {

    private static final Log LOG = LogFactory.getLog(SrvCardsV02.class);

    @Autowired
    private ISrvIntCardsV0 srvIntCardsV0;

    @Autowired
    private ICreateCardMapper createCardMapper;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    /**
     * @see com.bbva.pzic.cards.facade.v02.ISrvCardsV02#createCard(com.bbva.pzic.cards.canonic.Card)
     */
    @Override
    @POST
    @Path("/cards")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V02, logicalID = "createCard")
    public CardData createCard(final Card card) {
        LOG.info("... called method SrvCardsV02.createCard ...");

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V02, card, null, null);

        CardData data = createCardMapper.mapOut(srvIntCardsV0.createCard(createCardMapper.mapIn(card)));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V02, data, null, null);

        return data;
    }
}