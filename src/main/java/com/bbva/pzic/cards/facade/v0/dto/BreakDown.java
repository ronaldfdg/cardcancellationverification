package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "BreakDown", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "BreakDown", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BreakDown implements Serializable {

    private static final long serialVersionUID = 1L;

    private Import principal;
    private Import rates;

    public Import getPrincipal() {
        return principal;
    }

    public void setPrincipal(Import principal) {
        this.principal = principal;
    }

    public Import getRates() {
        return rates;
    }

    public void setRates(Import rates) {
        this.rates = rates;
    }
}
