package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.model.filenet.Cliente;
import com.bbva.pzic.cards.dao.model.filenet.Contenido;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardFinancialStatementMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
@Component("getCardFinancialStatementMapper")
public class GetCardFinancialStatementMapper implements IGetCardFinancialStatementMapper {

    private static final Log LOG = LogFactory.getLog(GetCardFinancialStatementMapper.class);

    private static final String PARTICIPANT_TYPE_NATURAL = "N";
    private static final String PARTICIPANT_RELATIONSHIP_TYPE = "TITULAR";
    private static final String PARTICIPANT_RELATIONSHIP_ORDER_FIRST = "PRIMERO";
    private static final String PARTICIPANT_RELATIONSHIP_ORDER_SECOND = "SEGUNDO";
    private static final String PUNTOS_VIDA_ID = "PV";
    private static final String PUNTOS_VIDA_NAME = "PUNTOS_VIDA";
    private static final String LIFEMILES_ID = "LM";
    private static final String LIFEMILES_NAME = "LIFEMILES";

    private static final String TEA_COMPRAS_SOLES = "TEA_COMPRAS_SOLES";
    private static final String TEA_COMPRAS_DOLARES = "TEA_COMPRAS_DOLARES";
    private static final String TEA_AVANCE_SOLES = "TEA_AVANCE_SOLES";
    private static final String TEA_AVANCE_DOLARES = "TEA_AVANCE_DOLARES";
    private static final String TEA_COMPENSATORIO_SOLES = "TEA_COMPENSATORIO_SOLES";
    private static final String TEA_COMPENSATORIO_DOLARES = "TEA_COMPENSATORIO_DOLARES";
    private static final String TEA_MORATORIO_SOLES = "TEA_MORATORIO_SOLES";
    private static final String TEA_MORATORIO_DOLARES = "TEA_MORATORIO_DOLARES";

    private static final String TRUE = "TRUE";

    private Translator translator;

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    private CypherTool cypherTool;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public InputGetCardFinancialStatement mapIn(final String cardId, final String financialStatementId, String contentType) {
        LOG.info("... called method GetCardFinancialStatementMapper.mapIn ...");
        InputGetCardFinancialStatement inputGetCardFinancialStatement = new InputGetCardFinancialStatement();
        inputGetCardFinancialStatement.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT));
        inputGetCardFinancialStatement.setFinancialStatementId(financialStatementId);
        inputGetCardFinancialStatement.setContentType(contentType);
        return inputGetCardFinancialStatement;
    }

    @Override
    public DocumentRequest mapOut(final DTOIntCardStatement dtoInt) {
        LOG.info("... called method GetCardFinancialStatementMapper.mapOut ...");
        if (dtoInt.getDetail() == null) {
            return null;
        }
        final DocumentRequest documentRequest = new DocumentRequest();
        final Contenido contenido = mapOutContenido(dtoInt.getDetail());

        contenido.setRowCardStatement(dtoInt.getRowCardStatement());
        contenido.setMessage(StringUtils.join(dtoInt.getMessages(), " "));

        documentRequest.setNumeroContrato(dtoInt.getDetail().getAccountNumberPEN());
        documentRequest.setContenido(Collections.singletonList(contenido));
        documentRequest.setIdContrato(translator.translate("servicing.cards.contractId"));
        documentRequest.setIdGrupo(translator.translate("servicing.cards.groupId"));
        String user = serviceInvocationContext.getUser();
        if (StringUtils.isNotEmpty(user)) {
            Cliente cliente = new Cliente();
            cliente.setCodigoCentral(user);
            documentRequest.setListaClientes(Collections.singletonList(cliente));
        }
        return documentRequest;
    }

    private Contenido mapOutContenido(final DTOIntCardStatementDetail dtoIntDetail) {
        Contenido contenido = new Contenido();
        contenido.setCardId(dtoIntDetail.getCardId());
        contenido.setFormatsPan(dtoIntDetail.getFormatsPan());
        contenido.setProductName(dtoIntDetail.getProductName());
        contenido.setProductCode(dtoIntDetail.getProductCode());
        contenido.setBranchName(dtoIntDetail.getBranchName());
        contenido.setCurrencyId(dtoIntDetail.getCurrency());
        contenido.setParticipantType(dtoIntDetail.getParticipantType());
        contenido.setEndDate(dateToString(dtoIntDetail.getEndDate()));
        contenido.setPayTypeName(dtoIntDetail.getPayType());
        contenido.setCreditLimitValueAmount(bigDecimalToString(dtoIntDetail.getCreditLimit()));
        contenido.setCreditLimitValueCurrency(dtoIntDetail.getCurrency());
        contenido.setPaymentDate(dateToString(dtoIntDetail.getPaymentDate()));
        mapOutDisposedBalanceLocalCurrency(contenido, dtoIntDetail.getDisposedBalanceLocalCurrency());
        mapOutDisposedBalance(contenido, dtoIntDetail.getDisposedBalance());
        mapOutTotalDebtLocalCurrency(contenido, dtoIntDetail.getTotalDebtLocalCurrency());
        mapOutTotalDebt(contenido, dtoIntDetail.getTotalDebt());
        contenido.setTotalTransactions(integerToString(dtoIntDetail.getTotalTransactions()));
        contenido.setTotalMonthsAmortizationMinimum(integerToString(dtoIntDetail.getTotalMonthsAmortizationMinimum()));
        contenido.setTotalMonthsAmortizationMinimumFull(integerToString(dtoIntDetail.getTotalMonthsAmortizationMinimumFull()));
        mapOutLimits(contenido, dtoIntDetail.getLimits(), dtoIntDetail.getCurrency());
        mapOutAddress(contenido, dtoIntDetail.getAddress());
        if (PARTICIPANT_TYPE_NATURAL.equalsIgnoreCase(dtoIntDetail.getParticipantType())) {
            mapOutProgram(contenido, dtoIntDetail.getProgram());
        }
        mapOutExtraPayments(contenido, dtoIntDetail.getExtraPayments());
        mapParticipants(contenido, dtoIntDetail);
        mapOutChargedAccounts(contenido, dtoIntDetail);
        mapOutInterestsRate(contenido, dtoIntDetail.getInterest());
        return contenido;
    }

    private void mapOutDisposedBalanceLocalCurrency(final Contenido contenido, final DTOMoney dtoIntDisposedBalanceLocalCurrency) {
        if (dtoIntDisposedBalanceLocalCurrency == null) {
            return;
        }
        contenido.setDisposedBalanceLocalCurrencyCurrency(dtoIntDisposedBalanceLocalCurrency.getCurrency());
        contenido.setDisposedBalanceLocalCurrencyAmount(bigDecimalToString(dtoIntDisposedBalanceLocalCurrency.getAmount()));
    }

    private void mapOutDisposedBalance(final Contenido contenido, final DTOMoney dtoIntDisposedBalance) {
        if (dtoIntDisposedBalance == null) {
            return;
        }
        contenido.setDisposedBalanceCurrency(dtoIntDisposedBalance.getCurrency());
        contenido.setDisposedBalanceAmount(bigDecimalToString(dtoIntDisposedBalance.getAmount()));
    }

    private void mapOutTotalDebtLocalCurrency(final Contenido contenido, final DTOMoney dtoIntTotalDebtLocalCurrency) {
        if (dtoIntTotalDebtLocalCurrency == null) {
            return;
        }
        contenido.setTotalDebtLocalCurrencyAmount(bigDecimalToString(dtoIntTotalDebtLocalCurrency.getAmount()));
        contenido.setTotalDebtLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
    }

    private void mapOutTotalDebt(final Contenido contenido, final DTOMoney dtoIntTotalDebt) {
        if (dtoIntTotalDebt == null) {
            return;
        }
        contenido.setTotalDebtAmount(bigDecimalToString(dtoIntTotalDebt.getAmount()));
        contenido.setTotalDebtCurrency(DTOIntCardStatementDetail.USD);
    }

    private void mapOutProgram(final Contenido contenido, final DTOIntCardStatementProgram dtoIntProgram) {
        if (dtoIntProgram == null) {
            return;
        }
        contenido.setProgramId(dtoIntProgram.getLifeMilesNumber());
        contenido.setProgramCurrentLoyaltyUnitsBalance(integerToString(dtoIntProgram.getMonthPoints()));
        contenido.setProgramLifetimeAccumulatedLoyaltyUnits(integerToString(dtoIntProgram.getTotalPoints()));
        contenido.setProgramBonus(integerToString(dtoIntProgram.getBonus()));
        mapOutProgramType(contenido);
    }

    private void mapOutProgramType(final Contenido documentRequest) {
        if (documentRequest.getProgramId() == null) {
            documentRequest.setProgramTypeId(PUNTOS_VIDA_ID);
            documentRequest.setProgramTypeName(PUNTOS_VIDA_NAME);
            documentRequest.setProgramBonus(null);
        } else {
            documentRequest.setProgramTypeId(LIFEMILES_ID);
            documentRequest.setProgramTypeName(LIFEMILES_NAME);
            documentRequest.setProgramLifetimeAccumulatedLoyaltyUnits(null);
        }
    }

    private void mapOutLimits(final Contenido contenido, final DTOIntCardStatementLimits dtoIntLimits, final String currency) {
        if (dtoIntLimits == null) {
            return;
        }
        contenido.setLimitsFinancingDisposedBalanceAmount(bigDecimalToString(dtoIntLimits.getFinancingDisposedBalance()));
        contenido.setLimitsFinancingDisposedBalanceCurrency(currency);
        contenido.setLimitsAvailableBalanceAmount(bigDecimalToString(dtoIntLimits.getAvailableBalance()));
        contenido.setLimitsAvailableBalanceCurrency(currency);
        contenido.setLimitsDisposedBalanceAmount(bigDecimalToString(dtoIntLimits.getDisposedBalance()));
        contenido.setLimitsDisposedBalanceCurrency(currency);
    }

    private void mapOutAddress(final Contenido contenido, final DTOIntAddress dtoIntAddress) {
        if (dtoIntAddress == null) {
            return;
        }
        contenido.setParticipant1AddresessName(dtoIntAddress.getName());
        contenido.setParticipant1AddresessStreetTypeName(dtoIntAddress.getStreetType());
        contenido.setParticipant1AddresessState(dtoIntAddress.getState());
        contenido.setParticipant1AddresessUbigeo(dtoIntAddress.getUbigeo());
    }

    private void mapOutExtraPayments(final Contenido contenido, final DTOIntExtraPayments dtoIntExtraPayments) {
        if (dtoIntExtraPayments == null) {
            return;
        }
        if (dtoIntExtraPayments.getOutstandingBalanceLocalCurrency() != null) {
            contenido.setOutstandingBalanceLocalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getOutstandingBalanceLocalCurrency().getAmount()));
            contenido.setOutstandingBalanceLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getMinimumCapitalLocalCurrency() != null) {
            contenido.setMinimunPaymentMinimumCapitalLocalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getMinimumCapitalLocalCurrency().getAmount()));
            contenido.setMinimunPaymentMinimumCapitalLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getInterestsBalanceLocalCurrency() != null) {
            contenido.setInterestsBalanceLocalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getInterestsBalanceLocalCurrency().getAmount()));
            contenido.setInterestsBalanceLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getFeesBalanceLocalCurrency() != null) {
            contenido.setFeesBalanceLocalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getFeesBalanceLocalCurrency().getAmount()));
            contenido.setFeesBalanceLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getFinancingTransactionLocalBalance() != null) {
            contenido.setFinancingTransactionLocalBalanceAmount(bigDecimalToString(dtoIntExtraPayments.getFinancingTransactionLocalBalance().getAmount()));
            contenido.setFinancingTransactionLocalBalanceCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getInvoiceMinimumAmountLocalCurrency() != null) {
            contenido.setMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getInvoiceMinimumAmountLocalCurrency().getAmount()));
            contenido.setMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getInvoiceAmountLocalCurrency() != null) {
            contenido.setInvoiceAmountLocalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getInvoiceAmountLocalCurrency().getAmount()));
            contenido.setInvoiceAmountLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (dtoIntExtraPayments.getOutstandingBalance() != null) {
            contenido.setOutstandingBalanceAmount(bigDecimalToString(dtoIntExtraPayments.getOutstandingBalance().getAmount()));
            contenido.setOutstandingBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (dtoIntExtraPayments.getMinimumCapitalCurrency() != null) {
            contenido.setMinimunPaymentMinimumCapitalCurrencyAmount(bigDecimalToString(dtoIntExtraPayments.getMinimumCapitalCurrency().getAmount()));
            contenido.setMinimunPaymentMinimumCapitalCurrencyCurrency(DTOIntCardStatementDetail.USD);
        }
        if (dtoIntExtraPayments.getInterestsBalance() != null) {
            contenido.setInterestsBalanceAmount(bigDecimalToString(dtoIntExtraPayments.getInterestsBalance().getAmount()));
            contenido.setInterestsBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (dtoIntExtraPayments.getFeesBalance() != null) {
            contenido.setFeesBalanceAmount(bigDecimalToString(dtoIntExtraPayments.getFeesBalance().getAmount()));
            contenido.setFeesBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (dtoIntExtraPayments.getFinancingTransactionBalance() != null) {
            contenido.setFinancingTransactionBalanceAmount(bigDecimalToString(dtoIntExtraPayments.getFinancingTransactionBalance().getAmount()));
            contenido.setFinancingTransactionBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (dtoIntExtraPayments.getInvoiceMinimumAmountCurrency() != null) {
            contenido.setMinimumPaymentInvoiceMinimumAmountAmount(bigDecimalToString(dtoIntExtraPayments.getInvoiceMinimumAmountCurrency().getAmount()));
            contenido.setMinimumPaymentInvoiceMinimumAmountCurrency(DTOIntCardStatementDetail.USD);
        }
        if (dtoIntExtraPayments.getInvoiceAmount() != null) {
            contenido.setInvoiceAmountAmount(bigDecimalToString(dtoIntExtraPayments.getInvoiceAmount().getAmount()));
            contenido.setInvoiceAmountCurrency(DTOIntCardStatementDetail.USD);
        }

    }


    private void mapParticipants(final Contenido documentRequest, final DTOIntCardStatementDetail detail) {
        documentRequest.setParticipant1RelationshipTypeName(PARTICIPANT_RELATIONSHIP_TYPE);
        documentRequest.setParticipant1RelationshipOrder(PARTICIPANT_RELATIONSHIP_ORDER_FIRST);
        documentRequest.setParticipant1Name(detail.getParticipantNameFirst());
        documentRequest.setParticipant1AddresessIsMainAddress(TRUE);

        documentRequest.setParticipant2RelationshipTypeName(PARTICIPANT_RELATIONSHIP_TYPE);
        documentRequest.setParticipant2RelationshipOrder(PARTICIPANT_RELATIONSHIP_ORDER_SECOND);
        documentRequest.setParticipant2Name(detail.getParticipantNameSecond());
    }

    private void mapOutChargedAccounts(final Contenido documentRequest, final DTOIntCardStatementDetail detail) {
        if (detail.getAccountNumberPEN() == null && detail.getAccountNumberUSD() == null) {
            return;
        }

        if (detail.getAccountNumberPEN() != null) {
            documentRequest.setChargedAccount1Id(detail.getAccountNumberPEN());
            documentRequest.setChargedAccount1Currency(DTOIntCardStatementDetail.PEN);
        }
        if (detail.getAccountNumberUSD() != null) {
            documentRequest.setChargedAccount2Id(detail.getAccountNumberUSD());
            documentRequest.setChargedAccount2Currency(DTOIntCardStatementDetail.USD);
        }
    }

    private void mapOutInterestsRate(final Contenido contenido, final DTOIntCardStatementInterest dtoIntInterest) {
        if (dtoIntInterest == null) {
            return;
        }

        if (dtoIntInterest.getPurchasePEN() != null) {
            contenido.setInterestRates1TypeId("1");
            contenido.setInterestRates1TypeName(TEA_COMPRAS_SOLES);
            contenido.setInterestRates1Percentage(dtoIntInterest.getPurchasePEN().toString());
        }

        if (dtoIntInterest.getPurchaseUSD() != null) {
            contenido.setInterestRates2TypeId("2");
            contenido.setInterestRates2TypeName(TEA_COMPRAS_DOLARES);
            contenido.setInterestRates2Percentage(dtoIntInterest.getPurchaseUSD().toString());
        }

        if (dtoIntInterest.getAdvancedPEN() != null) {
            contenido.setInterestRates3TypeId("3");
            contenido.setInterestRates3TypeName(TEA_AVANCE_SOLES);
            contenido.setInterestRates3Percentage(dtoIntInterest.getAdvancedPEN().toString());
        }

        if (dtoIntInterest.getAdvancedUSD() != null) {
            contenido.setInterestRates4TypeId("4");
            contenido.setInterestRates4TypeName(TEA_AVANCE_DOLARES);
            contenido.setInterestRates4Percentage(dtoIntInterest.getAdvancedUSD().toString());
        }

        if (dtoIntInterest.getCountervailingPEN() != null) {
            contenido.setInterestRates5TypeId("5");
            contenido.setInterestRates5TypeName(TEA_COMPENSATORIO_SOLES);
            contenido.setInterestRates5Percentage(dtoIntInterest.getCountervailingPEN().toString());
        }

        if (dtoIntInterest.getCountervailingUSD() != null) {
            contenido.setInterestRates6TypeId("6");
            contenido.setInterestRates6TypeName(TEA_COMPENSATORIO_DOLARES);
            contenido.setInterestRates6Percentage(dtoIntInterest.getCountervailingUSD().toString());
        }

        if (dtoIntInterest.getArrearsPEN() != null) {
            contenido.setInterestRates7TypeId("7");
            contenido.setInterestRates7TypeName(TEA_MORATORIO_SOLES);
            contenido.setInterestRates7Percentage(dtoIntInterest.getArrearsPEN().toString());
        }

        if (dtoIntInterest.getArrearsUSD() != null) {
            contenido.setInterestRates8TypeId("8");
            contenido.setInterestRates8TypeName(TEA_MORATORIO_DOLARES);
            contenido.setInterestRates8Percentage(dtoIntInterest.getArrearsUSD().toString());
        }
    }


    private String dateToString(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    private String bigDecimalToString(final BigDecimal bigDecimal) {
        return bigDecimal == null ? null : bigDecimal.toString();
    }

    private String integerToString(final Integer integer) {
        return integer == null ? null : integer.toString();
    }
}