package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.business.dto.DTOIntListLimits;
import com.bbva.pzic.cards.canonic.Limits;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardLimitV0Mapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 18/05/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardLimitV0Mapper implements IListCardLimitV0Mapper {

    private static final Log LOG = LogFactory.getLog(ListCardLimitV0Mapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntCard mapIn(final String cardId) {
        LOG.info("... called method ListCardLimitV0Mapper.mapIn ...");
        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cardId);
        return dtoIntCard;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse<List<Limits>> mapOut(final List<Limits> limits) {
        LOG.info("... called de ListCardLimitV0Mapper.mapOutput() ...");
        if (CollectionUtils.isEmpty(limits)) {
            return null;
        }
        return ServiceResponse.data(limits).pagination(null).build();
    }
}
