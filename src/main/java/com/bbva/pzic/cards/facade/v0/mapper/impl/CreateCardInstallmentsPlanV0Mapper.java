package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanData;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardInstallmentsPlanV0Mapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN;

/**
 * Created on 03/07/2017.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardInstallmentsPlanV0Mapper implements ICreateCardInstallmentsPlanV0Mapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntCardInstallmentsPlan mapIn(String cardId, InstallmentsPlan installmentsPlan) {
        DTOIntCardInstallmentsPlan dtoInt = new DTOIntCardInstallmentsPlan();
        dtoInt.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN));
        dtoInt.setTransactionId(installmentsPlan.getTransactionId());
        if (installmentsPlan.getTerms() != null) {
            dtoInt.setTermsNumber(installmentsPlan.getTerms().getNumber());
        }
        return dtoInt;
    }

    @Override
    public Response mapOut(InstallmentsPlanData cardInstallmentsPlan, UriInfo uriInfo) {
        if (cardInstallmentsPlan == null) {
            return null;
        }

        if (cardInstallmentsPlan.getData() == null || cardInstallmentsPlan.getData().getId() == null) {
            return Response.status(Response.Status.CREATED).build();
        }

        URI uriOfCreatedResource = UriBuilder.fromPath(uriInfo.getPath())
                .path("/{id}").build(cardInstallmentsPlan.getData().getId());

        return Response
                .created(uriOfCreatedResource)
                .contentLocation(uriOfCreatedResource)
                .status(Response.Status.CREATED).entity(cardInstallmentsPlan).build();
    }
}
