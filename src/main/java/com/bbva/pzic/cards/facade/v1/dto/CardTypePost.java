package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "cardTypePost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "cardTypePost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardTypePost implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product type identifier.
     */
    private String id;
    /**
     * Financial product type description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
