package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.Membership;

public interface IGetCardMembershipMapper {

    DTOIntSearchCriteria mapInput(String cardId, String membershipId);

    ServiceResponse<Membership> mapOutput(DTOIntMembershipServiceResponse dtoIntMembershipServiceResponse);


}
