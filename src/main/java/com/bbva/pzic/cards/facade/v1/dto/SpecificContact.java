package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "specificContact", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "specificContact", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpecificContact implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information identifier.
     */
    private String id;
    /**
     * Contact detail type.
     */
    private String contactDetailType;
    /**
     * Customer email address. This value will be masked when basic
     * authentication state. If the authentication state is advanced the
     * value will be clear.
     */
    @DatoAuditable(omitir = true)
    private String address;
    /**
     * Contact landline number. This value will be masked when basic
     * authentication state. If the authentication state is advanced the
     * value will be clear.<p>
     * Contact mobile number. This value will be masked when basic
     * authentication state. If the authentication state is advanced the
     * value will be clear.
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * The type of landine.
     */
    private String phoneType;
    /**
     * Phone number's country.
     */
    private Country country;
    /**
     * This attribute is not implemented in the USA.
     */
    private String regionalCode;
    /**
     * Contact vendor information.
     */
    private PhoneCompany phoneCompany;
    /**
     * Customer social media handle.
     */
    @DatoAuditable(omitir = true)
    private String username;
    /**
     * Social network that this handle belongs too.
     */
    private String socialNetwork;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getRegionalCode() {
        return regionalCode;
    }

    public void setRegionalCode(String regionalCode) {
        this.regionalCode = regionalCode;
    }

    public PhoneCompany getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(PhoneCompany phoneCompany) {
        this.phoneCompany = phoneCompany;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(String socialNetwork) {
        this.socialNetwork = socialNetwork;
    }
}
