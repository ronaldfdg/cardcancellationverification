package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "conditionFormatAmount", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "conditionFormatAmount", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConditionFormat implements Serializable {

    private static final long serialVersionUID = 1L;

    private String formatType;

    private Amount amount;

    private Amount minimumAmount;

    private Amount maximumAmount;

    public String getFormatType() {
        return formatType;
    }

    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public Amount getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Amount minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public Amount getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Amount maximumAmount) {
        this.maximumAmount = maximumAmount;
    }
}
