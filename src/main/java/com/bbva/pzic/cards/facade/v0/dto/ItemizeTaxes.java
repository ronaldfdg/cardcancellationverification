package com.bbva.pzic.cards.facade.v0.dto;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "ItemizeTaxes", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "ItemizeTaxes", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeTaxes implements Serializable {

    private static final long serialVersionUID = 1L;

    private String taxType;
    private MonetaryAmount monetaryAmount;

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public MonetaryAmount getMonetaryAmount() {
        return monetaryAmount;
    }

    public void setMonetaryAmount(MonetaryAmount monetaryAmount) {
        this.monetaryAmount = monetaryAmount;
    }
}
