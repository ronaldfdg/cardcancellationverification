package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v01.mapper.ICreateCardMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 29/12/2016.
 *
 * @author Entelgy
 */
@Mapper("createCardMapper")
public class CreateCardMapper extends ConfigurableMapper implements ICreateCardMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Autowired
    private CypherTool cypherTool;

    @Override
    public DTOIntCard mapIn(final Card card) {
        DTOIntCard dto = new DTOIntCard();

        if (card.getCardType() != null) {
            dto.setCardTypeId(enumMapper.getBackendValue("cards.cardType.id", card.getCardType().getId()));
        }
        if (card.getPhysicalSupport() != null) {
            dto.setPhysicalSupportId(enumMapper.getBackendValue("cards.physicalSupport.id", card.getPhysicalSupport().getId()));
        }
        if (card.getRelatedContracts() != null && !card.getRelatedContracts().isEmpty()) {
            dto.setRelatedContractId(cypherTool.decrypt(card.getRelatedContracts().get(0).getContractId(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD));
        }

        return dto;
    }
}
