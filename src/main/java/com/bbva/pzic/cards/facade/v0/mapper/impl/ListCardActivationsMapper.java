package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardActivationsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardActivationsMapper implements IListCardActivationsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardActivationsMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntCard mapIn(final String cardId) {
        LOG.info("... called method ListCardActivationsMapper.mapInput ...");

        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_ACTIVATIONS));
        return dtoIntCard;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<Activation>> mapOut(final List<Activation> activations) {
        LOG.info("... called method ListCardActivationsMapper.mapOut ...");
        if (CollectionUtils.isEmpty(activations)) {
            return null;
        }
        return ServiceResponse.data(activations).build();
    }

}
