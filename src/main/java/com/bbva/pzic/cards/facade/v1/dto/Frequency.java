package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "frequency", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "frequency", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Frequency implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Period identifier.
     */
    private String id;
    /**
     * Period description.
     */
    private String description;
    /**
     * Specifies the days of the week in which the weekly frequency applies. For example, weekly frequency, but only for weekends.
     */
    private DaysOfWeek daysOfWeek;
    /**
     * Specifies the days of the month in which the fortnight or monthly frequency applies. For example, fortnight frequency which restarts the 2ยบ day of month and the 18ยบ day of month.
     */
    private DaysOfMonth daysOfMonth;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DaysOfWeek getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(DaysOfWeek daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public DaysOfMonth getDaysOfMonth() {
        return daysOfMonth;
    }

    public void setDaysOfMonth(DaysOfMonth daysOfMonth) {
        this.daysOfMonth = daysOfMonth;
    }
}
