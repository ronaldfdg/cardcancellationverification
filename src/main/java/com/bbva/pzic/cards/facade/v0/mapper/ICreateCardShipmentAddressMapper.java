package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputCreateCardShipmentAddress;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;

public interface ICreateCardShipmentAddressMapper {

    InputCreateCardShipmentAddress mapIn(String shipmentId, ShipmentAddress shipmentAddress);

    ServiceResponse<ShipmentAddress> mapOut(ShipmentAddress shipmentAddress);

}
