package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCashRefund;
import com.bbva.pzic.cards.business.dto.DTOIntCashRefund;
import com.bbva.pzic.cards.facade.v0.dto.CashRefund;

public interface ICreateCardCashRefundMapper {

    DTOInputCreateCashRefund mapInput(String cardId, CashRefund cashRefund);

    ServiceResponse<CashRefund> mapOutput(DTOIntCashRefund dtoIntCashRefund);

}
