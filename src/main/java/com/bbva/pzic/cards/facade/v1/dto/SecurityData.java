package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "securityData", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "securityData", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurityData implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Security code card identifier.
     */
    private String id;
    /**
     * Security code card description.
     */
    private String description;
    /**
     * This value will be protected because the code of the Card Verification Value is
     * a sensitive information. The client will need the public key to be able to decode it.
     */
    @DatoAuditable(omitir = true)
    private String code;
    /**
     * Period of validity of the Card Verification Value.
     * DISCLAIMER: This applies to dynamic validation codes that are limited by validation time such as DCVV (Dynamic Card Verification Value).
     */
    private ValidityPeriod validityPeriod;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ValidityPeriod getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(ValidityPeriod validityPeriod) {
        this.validityPeriod = validityPeriod;
    }
}
