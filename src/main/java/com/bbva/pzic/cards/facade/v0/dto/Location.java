package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "location", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "location", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Complete address of the location.
     */
    @DatoAuditable(omitir = true)
    private String formattedAddress;

    private List<String> locationTypes;
    /**
     * Additional geographic information.
     */
    private List<AddressComponent> addressComponents;
    /**
     * Geographic coordinates of a point.
     */
    private Geolocation geolocation;

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public List<String> getLocationTypes() {
        return locationTypes;
    }

    public void setLocationTypes(List<String> locationTypes) {
        this.locationTypes = locationTypes;
    }

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }
}
