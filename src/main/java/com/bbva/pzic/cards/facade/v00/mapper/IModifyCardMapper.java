package com.bbva.pzic.cards.facade.v00.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;

/**
 * Created on 11/08/2017.
 *
 * @author Entelgy
 */
public interface IModifyCardMapper {

    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId unique card identifier
     * @param card   Object with the data of the card
     * @return Object with the data of input
     */
    DTOIntCard mapIn(String cardId, Card card);

}
