package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "simulateTransactionRefund", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "simulateTransactionRefund", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SimulateTransactionRefund implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Refund type. It is the type of refund operation that is applied for the refund request.
     */
    private RefundType refundType;
    /**
     * Card information.
     */
    private Card card;
    /**
     * Transaction information.
     */
    private Transaction transaction;
    /**
     * Status of the simulation. Indicates whether the request for a refund of the transaction was approved or rejected.
     */
    private Status status;

    public RefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundType refundType) {
        this.refundType = refundType;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
