package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "geolocation", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "geolocation", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Geolocation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Geographic coordinate that indicates the north-south position of a point regarding the Equator.
     */
    @DatoAuditable(omitir = true)
    private BigDecimal latitude;
    /**
     * Geographic coordinate that indicates the east-west position of a point regarding the Prime Meridian.
     */
    @DatoAuditable(omitir = true)
    private BigDecimal longitude;

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
}
