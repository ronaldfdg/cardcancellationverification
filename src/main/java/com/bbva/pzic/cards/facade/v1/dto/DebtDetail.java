package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "debtDetail", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "debtDetail", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DebtDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String currencyDebt;

    private Amount cancellationAmount;

    private DisposedBalance disposedBalance;

    private Amount liquidPendingDebt;

    private Amount liquidPendingCapital;

    private CurrentDebt currentDebt;

    private OverdueDebt overdueDebt;

    public String getCurrencyDebt() {
        return currencyDebt;
    }

    public void setCurrencyDebt(String currencyDebt) {
        this.currencyDebt = currencyDebt;
    }

    public Amount getCancellationAmount() {
        return cancellationAmount;
    }

    public void setCancellationAmount(Amount cancellationAmount) {
        this.cancellationAmount = cancellationAmount;
    }

    public DisposedBalance getDisposedBalance() {
        return disposedBalance;
    }

    public void setDisposedBalance(DisposedBalance disposedBalance) {
        this.disposedBalance = disposedBalance;
    }

    public Amount getLiquidPendingDebt() {
        return liquidPendingDebt;
    }

    public void setLiquidPendingDebt(Amount liquidPendingDebt) {
        this.liquidPendingDebt = liquidPendingDebt;
    }

    public Amount getLiquidPendingCapital() {
        return liquidPendingCapital;
    }

    public void setLiquidPendingCapital(Amount liquidPendingCapital) {
        this.liquidPendingCapital = liquidPendingCapital;
    }

    public CurrentDebt getCurrentDebt() {
        return currentDebt;
    }

    public void setCurrentDebt(CurrentDebt currentDebt) {
        this.currentDebt = currentDebt;
    }

    public OverdueDebt getOverdueDebt() {
        return overdueDebt;
    }

    public void setOverdueDebt(OverdueDebt overdueDebt) {
        this.overdueDebt = overdueDebt;
    }
}
