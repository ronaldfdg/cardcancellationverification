package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "currentDebt", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "currentDebt", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrentDebt implements Serializable {

    private static final long serialVersionUID = 1L;

    private Amount creditInsurance;

    private Amount capitalInstallment;

    private Amount capitalDebt;

    private Amount overdraftBalances;

    private List<ConditionCancellationVerification> conditions;

    private RatesCancellationVerification rates;

    public Amount getCreditInsurance() {
        return creditInsurance;
    }

    public void setCreditInsurance(Amount creditInsurance) {
        this.creditInsurance = creditInsurance;
    }

    public Amount getCapitalInstallment() {
        return capitalInstallment;
    }

    public void setCapitalInstallment(Amount capitalInstallment) {
        this.capitalInstallment = capitalInstallment;
    }

    public Amount getCapitalDebt() {
        return capitalDebt;
    }

    public void setCapitalDebt(Amount capitalDebt) {
        this.capitalDebt = capitalDebt;
    }

    public Amount getOverdraftBalances() {
        return overdraftBalances;
    }

    public void setOverdraftBalances(Amount overdraftBalances) {
        this.overdraftBalances = overdraftBalances;
    }

    public List<ConditionCancellationVerification> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionCancellationVerification> conditions) {
        this.conditions = conditions;
    }

    public RatesCancellationVerification getRates() {
        return rates;
    }

    public void setRates(RatesCancellationVerification rates) {
        this.rates = rates;
    }
}