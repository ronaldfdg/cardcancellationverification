package com.bbva.pzic.cards.facade.v1;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent;
import com.bbva.pzic.cards.facade.v1.dto.*;

import java.util.List;

/**
 * Created on 19/11/2019.
 *
 * @author Entelgy
 */
public interface ISrvCardsV1 {

    /**
     * Allows to consult the card proposals.
     *
     * @param status filter by the proposal status.
     * @return {@link List<Proposal>}
     */
    ServiceResponse<List<Proposal>> listCardProposals(String status);

    /**
     * It stores a customer generated pre-application of the formalization
     * of a credit card, which is finalized when the customer goes to the branch
     * of his choice to pick up the card. This operation could be derived from an offer.
     *
     * @param proposalCard payload
     * @return {@link ProposalCard}
     */
    ServiceResponseCreated<ProposalCard> createCardProposal(ProposalCard proposalCard);

    /**
     * It modifies a card proposal.
     *
     * @param proposalId unique card proposal identifier.
     * @param proposal   payload
     * @return {@link Proposal}
     */
    ServiceResponse<Proposal> modifyCardProposal(String proposalId, Proposal proposal);

    /**
     * It creates or replace a card proposal.
     *
     * @param proposalId unique card proposal identifier.
     * @param proposal   payload
     * @return {@link Proposal}
     */
    ServiceResponse<Proposal> updateCardProposal(String proposalId, Proposal proposal);

    /**
     * It creates a new card related to a specific customer.
     *
     * @param cardPost payload
     * @return {@link CardPost}
     */
    ServiceResponseCreated<CardPost> createCard(CardPost cardPost);

    /**
     * Add a new delivery configuration for a card to be sent, or for documents associated with a card as well.
     *
     * @param cardId   unique card identifier.
     * @param delivery payload
     * @return {@link Delivery}
     */
    ServiceResponseCreated<Delivery> createCardDelivery(String cardId, Delivery delivery);

    /**
     * It manages the security data related to a card.
     *
     * @param cardId
     * @param publicKey
     * @return {@link List<SecurityData>}
     */

    ServiceResponse<List<SecurityData>> getCardSecurityData(String cardId, String publicKey);

    /**
     * It updates the information of an operational activation related to a card.
     *
     * @param activationId unique activation identifier.
     * @param activation   payload
     * @return {@link ServiceResponseNoContent}
     */
    ServiceResponseNoContent modifyCardsActivations(String activationId, Activation activation);

    /**
     * It manages the verifications of the debts that have a credit card to carry out your cancellation as well
     * as your contract.
     *
     * @param cardId unique card identifier.
     * @return {@link ServiceResponse<CancellationVerification>}
     */
    ServiceResponse<CancellationVerification> getCardCancellationVerification(String cardId);

    /**
     * It manages a financial statement related to a specific card
     *
     * @param cardId
     * @param financialStatementId
     * @return
     */
    ServiceResponse<FinancialStatement> getCardFinancialStatement(String cardId, String financialStatementId);

    /**
     * Simulate the refund of an unrecognized transactions of a card, through a fraud rules engine and determines
     * if the return is accepted or rejected.
     *
     * @param cardId
     * @param transactionId
     * @param simulateTransactionRefund
     * @return
     */
    ServiceResponse<SimulateTransactionRefund> simulateCardTransactionTransactionRefund(String cardId, String transactionId, SimulateTransactionRefund simulateTransactionRefund);

    ServiceResponse<ReimburseCardTransactionTransactionRefund> reimburseCardTransactionTrasactionRefund(String cardId, String transactionId, ReimburseCardTransactionTransactionRefund input);
}
