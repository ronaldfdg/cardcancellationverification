package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;

public interface IReimburseCardTransactionTransactionRefundMapper {

    InputReimburseCardTransactionTransactionRefund mapIn(String cardId, String transactionId, ReimburseCardTransactionTransactionRefund input);

    ServiceResponse<ReimburseCardTransactionTransactionRefund> mapOut(ReimburseCardTransactionTransactionRefund outPut);
}
