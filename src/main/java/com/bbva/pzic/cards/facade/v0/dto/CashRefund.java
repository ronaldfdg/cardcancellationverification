package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@XmlRootElement(name = "CashRefund", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "CashRefund", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CashRefund implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String description;
    private String refundType;
    private RefundAmount refundAmount;
    private ReceivingAccount receivingAccount;
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar operationDate;
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar accountingDate;
    private ReceivedAmount receivedAmount;
    private ExchangeRate exchangeRate;
    private Taxes taxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public RefundAmount getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(RefundAmount refundAmount) {
        this.refundAmount = refundAmount;
    }

    public ReceivingAccount getReceivingAccount() {
        return receivingAccount;
    }

    public void setReceivingAccount(ReceivingAccount receivingAccount) {
        this.receivingAccount = receivingAccount;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public Calendar getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Calendar accountingDate) {
        this.accountingDate = accountingDate;
    }

    public ReceivedAmount getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(ReceivedAmount receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Taxes getTaxes() {
        return taxes;
    }

    public void setTaxes(Taxes taxes) {
        this.taxes = taxes;
    }
}
