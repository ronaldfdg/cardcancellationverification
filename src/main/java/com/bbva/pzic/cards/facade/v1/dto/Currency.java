package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "currency", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "currency", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Currency implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * String based on ISO-4217 for specifying the currency.
     */
    private String currency;
    /**
     * Flag indicating whether this currency corresponds to the main currency.
     */
    private Boolean isMajor;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(Boolean isMajor) {
        this.isMajor = isMajor;
    }
}
