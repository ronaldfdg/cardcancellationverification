package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "transferFees", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "transferFees", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransferFees implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * This attribute is not implemented in Mexico.
     */
    private Amount totalFees;
    /**
     * Detail of each fee.
     */
    private List<ItemizeFee> itemizeFees;

    public Amount getTotalFees() {
        return totalFees;
    }

    public void setTotalFees(Amount totalFees) {
        this.totalFees = totalFees;
    }

    public List<ItemizeFee> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<ItemizeFee> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }
}
