package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "disposedBalance", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "disposedBalance", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DisposedBalance implements Serializable {

    private static final long serialVersionUID = 1L;

    private Amount currentBalances;

    private Amount pendingBalances;

    public Amount getCurrentBalances() {
        return currentBalances;
    }

    public void setCurrentBalances(Amount currentBalances) {
        this.currentBalances = currentBalances;
    }

    public Amount getPendingBalances() {
        return pendingBalances;
    }

    public void setPendingBalances(Amount pendingBalances) {
        this.pendingBalances = pendingBalances;
    }
}
