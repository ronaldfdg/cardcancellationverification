package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.facade.v1.mapper.ICreateCardDeliveryMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.bbva.pzic.cards.util.Constants.*;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardDeliveryMapper implements ICreateCardDeliveryMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardDeliveryMapper.class);

    private Translator translator;

    public CreateCardDeliveryMapper(Translator translator) {
        this.translator = translator;
    }

    @Override
    public DTOIntDelivery mapIn(final String cardId, final Delivery delivery) {
        LOG.info("... called method CreateCardDeliveryMapper.mapIn ...");
        DTOIntDelivery input = new DTOIntDelivery();
        input.setCardId(cardId);
        input.setServiceTypeId(mapInDeliveryServiceType(delivery.getServiceType()));
        input.setContact(mapInDeliveryContact(delivery.getContact()));
        input.setDestination(mapInDeliveryDestination(delivery.getDestination()));
        return input;
    }

    // ------------------------------------------ Card_Delivery_Type ------------------------------------------
    private String mapInDeliveryServiceType(final ServiceType serviceType) {
        if (serviceType == null) {
            return null;
        }
        return serviceType.getId();
    }

    private DTOIntContact mapInDeliveryContact(final Contact contact) {
        if (contact == null) {
            return null;
        }
        DTOIntContact dtoIntContact = new DTOIntContact();
        dtoIntContact.setContactTypeOriginal(contact.getContactType());
        dtoIntContact.setContactType(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.contactType", contact.getContactType()));
        dtoIntContact.setContact(new DTOIntSpecificContact());

        SpecificContact specificContact = contact.getContact();
        if (SPECIFIC.equals(contact.getContactType())) {

            dtoIntContact.getContact().setContactDetailTypeOriginal(specificContact.getContactDetailType());
            dtoIntContact.getContact().setContactDetailType(translator.translateFrontendEnumValueStrictly(
                    "cards.delivery.contactDetailType", specificContact.getContactDetailType()));

            switch (specificContact.getContactDetailType()) {
                case EMAIL:
                    dtoIntContact.getContact().setAddress(specificContact.getAddress());
                    break;

                case LANDLINE:
                    dtoIntContact.getContact().setNumber(specificContact.getNumber());
                    dtoIntContact.getContact().setPhoneType(translator.translateFrontendEnumValueStrictly(
                            "cards.delivery.contact.phoneType", specificContact.getPhoneType()));
                    if (specificContact.getCountry() != null) {
                        dtoIntContact.getContact().setCountryId(specificContact.getCountry().getId());
                    }
                    dtoIntContact.getContact().setRegionalCode(specificContact.getRegionalCode());
                    break;

                case MOBILE:
                    DTOIntPhoneCompanyProposal dtoIntPhoneCompany = new DTOIntPhoneCompanyProposal();
                    dtoIntPhoneCompany.setId(specificContact.getPhoneCompany().getId());

                    dtoIntContact.getContact().setNumber(specificContact.getNumber());
                    dtoIntContact.getContact().setPhoneCompany(dtoIntPhoneCompany);
                    break;

                case SOCIAL_MEDIA:
                    dtoIntContact.getContact().setUsername(specificContact.getUsername());
                    dtoIntContact.getContact().setSocialNetwork(translator.translateFrontendEnumValueStrictly(
                            "cards.delivery.contact.socialNetwork", specificContact.getSocialNetwork()));
                    break;

                default:
                    LOG.warn(String.format("Unrecognized contact detail type %s", specificContact.getContactDetailType()));
            }
        } else if (STORED.equals(contact.getContactType())) {

            dtoIntContact.getContact().setId(specificContact.getId());

        } else {
            LOG.warn(String.format("Unrecognized contact type %s", contact.getContactType()));
        }

        return dtoIntContact;
    }

    private DTOIntDestination mapInDeliveryDestination(final Destination destination) {
        if (destination == null) {
            return null;
        }
        DTOIntDestination dtoIntDestination = new DTOIntDestination();
        dtoIntDestination.setId(translator.translateFrontendEnumValueStrictly(
                "cards.delivery.destination.id", destination.getId()));
        return dtoIntDestination;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponseCreated<Delivery> mapOut(final Delivery delivery) {
        LOG.info("... called method CreateCardDeliveryMapper.mapOut ...");
        if (delivery == null) {
            return null;
        }

        return ServiceResponseCreated.data(delivery).build();
    }
}

