package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.canonic.TransactionsData;

import java.util.List;

/**
 * Created on 06/02/2017.
 *
 * @author Entelgy
 */
public interface IListCardTransactionsV0Mapper {

    DTOInputListCardTransactions mapInput(boolean haveFinancingType, String cardId, String fromOperationDate, String toOperationDate,
                                          String paginationKey, Long pageSize, String registryId);

    ServiceResponse<List<Transaction>> mapOut(TransactionsData transactionsData, Pagination pagination);

}
