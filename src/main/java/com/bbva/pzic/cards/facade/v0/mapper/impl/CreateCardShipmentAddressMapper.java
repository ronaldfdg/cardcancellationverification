package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardShipmentAddressMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.SPECIFIC;
import static com.bbva.pzic.cards.util.Constants.STORED;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class CreateCardShipmentAddressMapper implements ICreateCardShipmentAddressMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardShipmentAddressMapper.class);

    @Override
    public InputCreateCardShipmentAddress mapIn(final String shipmentId, final ShipmentAddress shipmentAddress) {
        LOG.info("... called method CreateCardShipmentAddressMapper.mapIn ...");
        InputCreateCardShipmentAddress input = new InputCreateCardShipmentAddress();
        input.setShipmentId(shipmentId);
        input.setAddressType(shipmentAddress.getAddressType());
        input.setShipmentAddress(mapInShipmentAddress(shipmentAddress));

        return input;
    }

    private DTOIntAddressType mapInShipmentAddress(final ShipmentAddress shipmentAddress) {
        if (SPECIFIC.equalsIgnoreCase(shipmentAddress.getAddressType())) {
            return mapInSpecificShipmentAddress(shipmentAddress);
        }

        if (STORED.equalsIgnoreCase(shipmentAddress.getAddressType())) {
            return mapInStoredShipmentAddress(shipmentAddress);
        }

        return null;
    }

    private DTOIntAddressType mapInStoredShipmentAddress(final ShipmentAddress shipmentAddress) {
        DTOIntStoredAddressType dtoIntAddressType = new DTOIntStoredAddressType();
        dtoIntAddressType.setId(shipmentAddress.getId());
        dtoIntAddressType.setDestination(mapInDestination(shipmentAddress.getDestination()));
        return dtoIntAddressType;
    }

    private DTOIntDestination mapInDestination(final Destination destination) {
        if (destination == null) {
            return null;
        }

        DTOIntDestination dtoIntDestination = new DTOIntDestination();
        dtoIntDestination.setId(destination.getId());
        return dtoIntDestination;
    }

    private DTOIntAddressType mapInSpecificShipmentAddress(final ShipmentAddress shipmentAddress) {
        if (shipmentAddress.getLocation() == null) {
            return null;
        }

        DTOIntSpecificAddressType dtoIntAddressType = new DTOIntSpecificAddressType();
        dtoIntAddressType.setLocation(mapInLocation(shipmentAddress.getLocation()));
        return dtoIntAddressType;
    }

    private DTOIntLocation mapInLocation(final Location location) {
        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        dtoIntLocation.setFormattedAddress(location.getFormattedAddress());
        dtoIntLocation.setLocationTypes(location.getLocationTypes());
        dtoIntLocation.setAddressComponents(mapInAddressComponents(location.getAddressComponents()));
        dtoIntLocation.setGeolocation(mapInGeolocation(location.getGeolocation()));
        return dtoIntLocation;
    }

    private DTOIntGeolocation mapInGeolocation(final Geolocation geolocation) {
        if (geolocation == null) {
            return null;
        }

        DTOIntGeolocation dtoIntGeolocation = new DTOIntGeolocation();
        dtoIntGeolocation.setLatitude(geolocation.getLatitude());
        dtoIntGeolocation.setLongitude(geolocation.getLongitude());
        return dtoIntGeolocation;
    }

    private List<DTOIntAddressComponents> mapInAddressComponents(final List<AddressComponent> addressComponents) {
        if (CollectionUtils.isEmpty(addressComponents)) {
            return null;
        }

        return addressComponents.stream()
                .filter(Objects::nonNull)
                .map(this::mapAddressComponent)
                .collect(Collectors.toList());
    }

    private DTOIntAddressComponents mapAddressComponent(final AddressComponent addressComponent) {
        DTOIntAddressComponents dtoIntAddressComponents = new DTOIntAddressComponents();
        dtoIntAddressComponents.setComponentTypes(addressComponent.getComponentTypes());
        dtoIntAddressComponents.setName(addressComponent.getName());
        dtoIntAddressComponents.setCode(addressComponent.getCode());
        return dtoIntAddressComponents;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<ShipmentAddress> mapOut(final ShipmentAddress data) {
        LOG.info("... called method ListCardShipmentsMapper.mapOut ...");
        if (data == null) {
            return null;
        }

        return ServiceResponse.data(data).build();
    }
}
