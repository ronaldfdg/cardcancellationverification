package com.bbva.pzic.cards.facade.v1.mapper.common;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.DTOIntLocation;
import com.bbva.pzic.cards.business.dto.DTOIntScheduletimes;
import com.bbva.pzic.cards.facade.v1.dto.Location;
import com.bbva.pzic.cards.facade.v1.dto.ScheduleTimes;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.DateMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;

import java.text.ParseException;

/**
 * Created on 26/06/2020.
 *
 * @author Entelgy.
 */
@Mapper("cardProposalDateTimeMapper")
public class CardProposalDateTimeMapper extends AbstractCardProposalMapper {

    @Override
    protected DTOIntScheduletimes mapInScheduleTimes(final ScheduleTimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        try {
            DTOIntScheduletimes dtoIntScheduletimes = new DTOIntScheduletimes();
            dtoIntScheduletimes.setEndtime(DateMapper.buildCalendar(DateUtils.toDate(scheduletimes.getEndTime(), DateMapper.DATE_TIME_PATTERN)));
            dtoIntScheduletimes.setStarttime(DateMapper.buildCalendar(DateUtils.toDate(scheduletimes.getStartTime(), DateMapper.DATE_TIME_PATTERN)));
            return dtoIntScheduletimes;
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    @Override
    protected DTOIntLocation mapInLocation(final Location location) {
        if (location == null) {
            return null;
        }

        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        dtoIntLocation.setAddressComponents(mapInAddressComponents(location.getAddressComponents()));
        return dtoIntLocation;
    }
}