package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "itemizeRate", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "itemizeRate", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeRate implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the rate type.
     */
    private String rateType;
    /**
     * Description of the rate type.
     */
    private String description;
    /**
     * Date on which the interest will be calculated.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date calculationDate;
    /**
     * Detail mode in which the collection rate is applied.
     */
    private ItemizeRateUnit itemizeRatesUnit;

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Date calculationDate) {
        this.calculationDate = calculationDate;
    }

    public ItemizeRateUnit getItemizeRatesUnit() {
        return itemizeRatesUnit;
    }

    public void setItemizeRatesUnit(ItemizeRateUnit itemizeRatesUnit) {
        this.itemizeRatesUnit = itemizeRatesUnit;
    }
}
