package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "participant", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "participant", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Customer full name.
     */
    @DatoAuditable(omitir = true)
    private String fullName;
    /**
     * Document used to identify the customer.
     */
    private IdentityDocument identityDocument;
    /**
     * Cards, of the participant, to be delivered.
     */
    private List<ParticipantCards> cards;

    private ParticipantCards card;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public List<ParticipantCards> getCards() {
        return cards;
    }

    public void setCards(List<ParticipantCards> cards) {
        this.cards = cards;
    }

    public ParticipantCards getCard() {
        return card;
    }

    public void setCard(ParticipantCards card) {
        this.card = card;
    }
}
