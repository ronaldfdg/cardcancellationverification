package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "PendingAmount", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "PendingAmount", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PendingAmount implements Serializable {

    private static final long serialVersionUID = 1L;

    private Import total;
    private Import principal;
    private Import rates;

    public Import getTotal() {
        return total;
    }

    public void setTotal(Import total) {
        this.total = total;
    }

    public Import getPrincipal() {
        return principal;
    }

    public void setPrincipal(Import principal) {
        this.principal = principal;
    }

    public Import getRates() {
        return rates;
    }

    public void setRates(Import rates) {
        this.rates = rates;
    }
}
