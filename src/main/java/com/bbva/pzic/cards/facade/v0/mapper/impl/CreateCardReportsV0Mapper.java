package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardReportsV0Mapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.*;

@Mapper
public class CreateCardReportsV0Mapper implements ICreateCardReportsV0Mapper {

    private static final Log LOG = LogFactory.getLog(CreateCardReportsV0Mapper.class);

    private Translator translator;

    private ConfigurationManager configurationManager;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Autowired
    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    @Override
    public InputCreateCardReports mapIn(final String cardId, final ReportCardCreation reportCardCreation) {
        LOG.info("... called method CreateCardReportsV0Mapper.mapIn ...");
        String channel = reportCardCreation.getChannel();
        InputCreateCardReports input = new InputCreateCardReports();
        input.setCardId(cardId);
        input.setUser(StringUtils.isEmpty(channel) ? null : configurationManager.getProperty("servicing.aap.configuration." + channel + ".constant.user"));
        input.setPassword(StringUtils.isEmpty(channel) ? null : configurationManager.getProperty("servicing.aap.configuration." + channel + ".constant.password"));
        input.setChannel(channel);
        input.setCardType(mapInCardType(reportCardCreation.getCardType()));
        input.setNumber(reportCardCreation.getNumber());
        input.setNumberType(mapInNumberType(reportCardCreation.getNumberType()));
        input.setBrandAssociation(mapInBrandAssociation(reportCardCreation.getBrandAssociation()));
        input.setProduct(mapInProduct(reportCardCreation.getProduct()));
        input.setCurrencies(mapInCurrencies(reportCardCreation.getCurrencies()));
        input.setHasPrintedHolderName(reportCardCreation.getHasPrintedHolderName());
        input.setParticipants(mapInParticipants(reportCardCreation.getParticipants()));
        input.setPaymentMethod(mapInPaymentMethod(reportCardCreation.getPaymentMethod()));
        input.setGrantedCredits(mapInGrantedCredits(reportCardCreation.getGrantedCredits()));
        input.setRelatedContracts(mapInRelatedContracts(reportCardCreation.getRelatedContracts()));
        input.setDeliveries(mapInDeliveries(reportCardCreation.getDeliveries()));
        input.setContractingBranch(mapInContractingBranch(reportCardCreation.getContractingBranch()));
        input.setLoyaltyProgram(mapInLoyaltyProgram(reportCardCreation.getLoyaltyProgram()));
        input.setProposalStatus(translator.translateFrontendEnumValueStrictly(PROPERTY_PROPOSAL_STATUS, reportCardCreation.getProposalStatus()));
        input.setOfferId(reportCardCreation.getOfferId());
        return input;
    }

    private DTOIntCardType mapInCardType(final CardType cardType) {
        if (cardType == null) {
            return null;
        }
        DTOIntCardType dtoIntCardType = new DTOIntCardType();
        dtoIntCardType.setId(cardType.getId());
        return dtoIntCardType;
    }

    private DTOIntNumberType mapInNumberType(final NumberType numberType) {
        if (numberType == null) {
            return null;
        }
        DTOIntNumberType dtoIntNumberType = new DTOIntNumberType();
        dtoIntNumberType.setId(numberType.getId());
        return dtoIntNumberType;
    }

    private DTOIntBrandAssociation mapInBrandAssociation(final BrandAssociation brandAssociation) {
        if (brandAssociation == null) {
            return null;
        }
        DTOIntBrandAssociation dtoIntBrandAssociation = new DTOIntBrandAssociation();
        dtoIntBrandAssociation.setId(brandAssociation.getId());
        return dtoIntBrandAssociation;
    }

    private DTOIntProduct mapInProduct(final Product product) {
        if (product == null) {
            return null;
        }
        DTOIntProduct dtoIntProduct = new DTOIntProduct();
        dtoIntProduct.setId(product.getId());
        dtoIntProduct.setName(product.getName());
        dtoIntProduct.setSubproduct(mapInSubproduct(product.getSubproduct()));
        return dtoIntProduct;
    }

    private DTOIntSubProduct mapInSubproduct(final Subproduct subproduct) {
        if (subproduct == null) {
            return null;
        }
        DTOIntSubProduct dtoIntSubProduct = new DTOIntSubProduct();
        dtoIntSubProduct.setId(subproduct.getId());
        dtoIntSubProduct.setName(subproduct.getName());
        dtoIntSubProduct.setDescription(subproduct.getDescription());
        return dtoIntSubProduct;
    }

    private List<DTOIntCurrency> mapInCurrencies(final List<Currency> currencyList) {
        if (CollectionUtils.isEmpty(currencyList)) {
            return null;
        }
        return currencyList.stream().filter(Objects::nonNull).map(this::mapInCurrency).collect(Collectors.toList());
    }

    private DTOIntCurrency mapInCurrency(final Currency currency) {
        DTOIntCurrency dtoIntCurrency = new DTOIntCurrency();
        dtoIntCurrency.setCurrency(currency.getCurrency());
        dtoIntCurrency.setIsMajor(currency.getIsMajor());
        return dtoIntCurrency;
    }

    private List<DTOIntParticipantReport> mapInParticipants(final List<ParticipantReport> participantList) {
        if (participantList == null) {
            return null;
        }
        return participantList.stream().filter(Objects::nonNull).map(this::mapInParticipant).collect(Collectors.toList());
    }

    private DTOIntParticipantReport mapInParticipant(final ParticipantReport participant) {
        DTOIntParticipantReport dtoIntParticipant = new DTOIntParticipantReport();
        dtoIntParticipant.setId(participant.getId());
        dtoIntParticipant.setFirstName(participant.getFirstName());
        dtoIntParticipant.setMiddleName(participant.getMiddleName());
        dtoIntParticipant.setLastName(participant.getLastName());
        dtoIntParticipant.setSecondLastName(participant.getSecondLastName());
        dtoIntParticipant.setIdentityDocuments(mapInIdentityDocuments(participant.getIdentityDocuments()));
        dtoIntParticipant.setContactDetails(mapInContactDetails(participant.getContactDetails()));
        return dtoIntParticipant;
    }


    private List<DTOIntIdentityDocument> mapInIdentityDocuments(final List<IdentityDocument> identityDocumentList) {
        if (CollectionUtils.isEmpty(identityDocumentList)) {
            return null;
        }
        return identityDocumentList.stream().filter(Objects::nonNull).map(this::mapInIdentityDocument).collect(Collectors.toList());
    }

    private DTOIntIdentityDocument mapInIdentityDocument(final IdentityDocument identityDocument) {
        DTOIntIdentityDocument dtoIntIdentityDocument = new DTOIntIdentityDocument();
        dtoIntIdentityDocument.setNumber(identityDocument.getDocumentNumber());
        dtoIntIdentityDocument.setDocumentType(mapInIdentityDocumentType(identityDocument.getDocumentType()));
        return dtoIntIdentityDocument;
    }

    private DTOIntDocumentType mapInIdentityDocumentType(final DocumentType documentType) {
        if (documentType == null) {
            return null;
        }
        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        dtoIntDocumentType.setId(translator.translateFrontendEnumValueStrictly(PROPERTY_DOCUMENT_TYPE_ID, documentType.getId()));
        return dtoIntDocumentType;
    }

    private List<DTOIntContactDetailReport> mapInContactDetails(final List<ContactDetailReport> contactDetailList) {
        if (CollectionUtils.isEmpty(contactDetailList)) {
            return null;
        }
        return contactDetailList.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private DTOIntContactDetailReport mapInContactDetail(final ContactDetailReport contactDetail) {
        if (contactDetail.getContact() == null) {
            return null;
        }
        DTOIntContactDetailReport dtoIntContactDetail = new DTOIntContactDetailReport();
        if (EMAIL.equalsIgnoreCase(contactDetail.getContact().getContactType())) {
            DTOIntEmailContact dtoIntEmailContact = new DTOIntEmailContact();
            dtoIntEmailContact.setContactType(contactDetail.getContact().getContactType());
            dtoIntEmailContact.setAddress(contactDetail.getContact().getAddress());
            dtoIntContactDetail.setEmailContact(dtoIntEmailContact);
        } else if (MOBILE.equalsIgnoreCase(contactDetail.getContact().getContactType())) {
            DTOIntMobileContact dtoIntMobileContact = new DTOIntMobileContact();
            dtoIntMobileContact.setContactType(contactDetail.getContact().getContactType());
            dtoIntMobileContact.setNumber(contactDetail.getContact().getNumber());
            dtoIntContactDetail.setMobileContact(dtoIntMobileContact);
        }
        return dtoIntContactDetail;
    }


    private DTOIntPaymentMethod mapInPaymentMethod(final PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }
        DTOIntPaymentMethod dtoIntPaymentMethod = new DTOIntPaymentMethod();
        dtoIntPaymentMethod.setId(paymentMethod.getId());
        dtoIntPaymentMethod.setFrecuency(mapInFrecuency(paymentMethod.getFrecuency()));
        return dtoIntPaymentMethod;
    }

    private DTOIntFrequency mapInFrecuency(final Frecuency frecuency) {
        if (frecuency == null) {
            return null;
        }
        DTOIntFrequency dtoIntFrequency = new DTOIntFrequency();
        dtoIntFrequency.setId(frecuency.getId());
        dtoIntFrequency.setDaysOfMonth(mapInDaysOfMonth(frecuency.getDaysOfMonth()));
        return dtoIntFrequency;
    }

    private DTOIntDaysOfMonth mapInDaysOfMonth(final DaysOfMonth daysOfMonth) {
        if (daysOfMonth == null) {
            return null;
        }
        DTOIntDaysOfMonth dtoIntDaysOfMonth = new DTOIntDaysOfMonth();
        dtoIntDaysOfMonth.setDay(daysOfMonth.getDay());
        return dtoIntDaysOfMonth;
    }

    private List<DTOIntAmount> mapInGrantedCredits(final List<Amount> grantedCreditList) {
        if (grantedCreditList == null) {
            return null;
        }
        return grantedCreditList.stream().filter(Objects::nonNull).map(this::mapInAmount).collect(Collectors.toList());
    }

    private DTOIntAmount mapInAmount(final Amount amount) {
        if (amount == null) {
            return null;
        }
        DTOIntAmount dtoIntAmount = new DTOIntAmount();
        dtoIntAmount.setAmount(amount.getAmount());
        dtoIntAmount.setCurrency(amount.getCurrency());
        return dtoIntAmount;
    }

    private List<DTOIntRelatedContract> mapInRelatedContracts(final List<RelatedContract> relatedContractList) {
        if (relatedContractList == null) {
            return null;
        }
        return relatedContractList.stream().filter(Objects::nonNull).map(this::mapInRelatedContract).collect(Collectors.toList());
    }

    private DTOIntRelatedContract mapInRelatedContract(final RelatedContract relatedContract) {
        if (relatedContract == null) {
            return null;
        }
        DTOIntRelatedContract dtoIntRelatedContract = new DTOIntRelatedContract();
        dtoIntRelatedContract.setNumber(relatedContract.getNumber());
        dtoIntRelatedContract.setNumberType(mapInNumberType(relatedContract.getNumberType()));
        return dtoIntRelatedContract;
    }

    private List<DTOIntDelivery> mapInDeliveries(final List<Delivery> deliveryList) {
        if (deliveryList == null) {
            return null;
        }
        return deliveryList.stream().filter(Objects::nonNull).map(this::mapInDelivery).collect(Collectors.toList());
    }

    private DTOIntDelivery mapInDelivery(final Delivery delivery) {
        DTOIntDelivery dtoIntDelivery = new DTOIntDelivery();
        dtoIntDelivery.setId(delivery.getId());
        dtoIntDelivery.setServiceType(mapInServiceType(delivery.getServiceType()));
        dtoIntDelivery.setContact(mapInContact(delivery.getContact()));
        dtoIntDelivery.setAddress(mapInAddress(delivery.getAddress()));
        dtoIntDelivery.setDestination(mapInDestination(delivery.getDestination()));
        dtoIntDelivery.setBranch(mapInBranch(delivery.getBranch()));
        return dtoIntDelivery;
    }

    private DTOIntServiceType mapInServiceType(final ServiceType serviceType) {
        if (serviceType == null) {
            return null;
        }
        DTOIntServiceType dtoIntServiceType = new DTOIntServiceType();
        dtoIntServiceType.setId(serviceType.getId());
        return dtoIntServiceType;
    }

    private DTOIntContact mapInContact(final Contact contact) {
        if (contact == null) {
            return null;
        }
        DTOIntContact dtoIntContact = new DTOIntContact();
        dtoIntContact.setContactType(contact.getContactType());
        dtoIntContact.setId(contact.getId());
        dtoIntContact.setContact(mapInSpecificContact(contact.getContactType(), contact.getContact()));
        return dtoIntContact;
    }

    private DTOIntSpecificContact mapInSpecificContact(final String contactType, final SpecificContact specificContact) {
        if (specificContact == null) {
            return null;
        }
        DTOIntSpecificContact dtoIntSpecificContact = new DTOIntSpecificContact();
        if (SPECIFIC.equalsIgnoreCase(contactType)) {
            dtoIntSpecificContact.setContactDetailType(specificContact.getContactDetailType());
        }
        if (EMAIL.equalsIgnoreCase(specificContact.getContactDetailType())) {
            dtoIntSpecificContact.setAddress(specificContact.getAddress());
        }
        return dtoIntSpecificContact;
    }

    private DTOIntAddress mapInAddress(final Address address) {
        if (address == null) {
            return null;
        }
        DTOIntAddress dtoIntAddress = new DTOIntAddress();
        dtoIntAddress.setAddressType(address.getAddressType());
        if (STORED.equalsIgnoreCase(address.getAddressType())) {
            dtoIntAddress.setId(address.getId());
        } else if (SPECIFIC.equalsIgnoreCase(address.getAddressType())) {
            dtoIntAddress.setLocation(mapInLocation(address.getLocation()));
        }
        return dtoIntAddress;
    }

    private DTOIntLocation mapInLocation(final Location location) {
        if (location == null) {
            return null;
        }
        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        dtoIntLocation.setAddressComponents(mapInAddressComponents(location.getAddressComponents()));
        return dtoIntLocation;
    }

    private List<DTOIntAddressComponents> mapInAddressComponents(final List<AddressComponent> addressComponentList) {
        if (CollectionUtils.isEmpty(addressComponentList)) {
            return null;
        }
        return addressComponentList.stream().filter(Objects::nonNull).map(this::mapInAddressComponent).collect(Collectors.toList());
    }

    private DTOIntAddressComponents mapInAddressComponent(final AddressComponent addressComponent) {
        DTOIntAddressComponents dtoIntAddressComponents = new DTOIntAddressComponents();
        dtoIntAddressComponents.setComponentTypes(addressComponent.getComponentTypes());
        dtoIntAddressComponents.setCode(addressComponent.getCode());
        dtoIntAddressComponents.setName(addressComponent.getName());
        dtoIntAddressComponents.setFormattedAddress(addressComponent.getFormattedAddress());
        return dtoIntAddressComponents;
    }

    private DTOIntDestination mapInDestination(final Destination destination) {
        if (destination == null) {
            return null;
        }
        DTOIntDestination dtoIntDestination = new DTOIntDestination();
        dtoIntDestination.setId(destination.getId());
        dtoIntDestination.setName(destination.getName());
        return dtoIntDestination;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if (branch == null) {
            return null;
        }
        DTOIntBranch dtoIntBranch = new DTOIntBranch();
        dtoIntBranch.setId(branch.getId());
        dtoIntBranch.setName(branch.getName());
        return dtoIntBranch;
    }

    private DTOIntContractingBranch mapInContractingBranch(final ContractingBranch contractingBranch) {
        if (contractingBranch == null) {
            return null;
        }
        DTOIntContractingBranch dtoIntContractingBranch = new DTOIntContractingBranch();
        dtoIntContractingBranch.setId(contractingBranch.getId());
        dtoIntContractingBranch.setName(contractingBranch.getName());
        return dtoIntContractingBranch;
    }

    private DTOIntLoyaltyProgram mapInLoyaltyProgram(final LoyaltyProgram loyaltyProgram) {
        if (loyaltyProgram == null) {
            return null;
        }
        DTOIntLoyaltyProgram dtoIntLoyaltyProgram = new DTOIntLoyaltyProgram();
        dtoIntLoyaltyProgram.setId(loyaltyProgram.getId());
        dtoIntLoyaltyProgram.setDescription(loyaltyProgram.getDescription());
        return dtoIntLoyaltyProgram;
    }
}
