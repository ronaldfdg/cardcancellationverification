package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "delivery", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "delivery", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Destination type of card delivery.
     */
    private Destination destination;
    /**
     * Custom address where the card will be deliver. This can be an address
     * that the customer enters manually or and address previously selected by
     * the customer.
     */
    private Address address;
    /**
     * Detail of delivery contact.
     */
    private Contact contact;
    /**
     * Delivery service type of a product or document. These can be physical or digital, depending on the case the delivery will be at an address or a contact.
     */
    private ServiceType serviceType;
    /**
     * Delivery manager identifier.
     */
    private String id;

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}