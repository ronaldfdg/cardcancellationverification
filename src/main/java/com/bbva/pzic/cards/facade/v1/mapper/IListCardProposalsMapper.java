package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputListCardProposals;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;

import java.util.List;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public interface IListCardProposalsMapper {

    InputListCardProposals mapIn(String status);

    ServiceResponse<List<Proposal>> mapOut(List<Proposal> proposal);
}
