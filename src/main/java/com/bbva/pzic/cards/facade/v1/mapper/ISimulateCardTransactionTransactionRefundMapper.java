package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.SimulateTransactionRefund;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
public interface ISimulateCardTransactionTransactionRefundMapper {

    InputSimulateCardTransactionTransactionRefund mapIn(String cardId, String transactionId, SimulateTransactionRefund simulateTransactionRefund);

    ServiceResponse<SimulateTransactionRefund> mapOut(SimulateTransactionRefund simulateCardTransactionTransactionRefund);
}
