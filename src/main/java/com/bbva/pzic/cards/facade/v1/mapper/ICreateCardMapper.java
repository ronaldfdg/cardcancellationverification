package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
public interface ICreateCardMapper {

    InputCreateCard mapIn(CardPost cardPost);

    ServiceResponseCreated<CardPost> mapOut(CardPost cardPost);
}
