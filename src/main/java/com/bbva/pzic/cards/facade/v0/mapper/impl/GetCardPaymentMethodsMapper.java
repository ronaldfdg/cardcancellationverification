package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardPaymentMethodsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 24/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class GetCardPaymentMethodsMapper implements IGetCardPaymentMethodsMapper {
    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntCard mapIn(String cardId) {
        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_PAYMENT_METHODS));
        return dtoIntCard;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<PaymentMethod>> mapOut(final PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }
        List<PaymentMethod> paymentMethods = new ArrayList<>();
        paymentMethods.add(paymentMethod);
        return ServiceResponse.data(paymentMethods).pagination(null).build();
    }
}
