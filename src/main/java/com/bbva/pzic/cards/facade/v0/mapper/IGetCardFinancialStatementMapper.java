package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;

/**
 * Created on 26/07/2018.
 *
 * @author Entelgy
 */
public interface IGetCardFinancialStatementMapper {

    InputGetCardFinancialStatement mapIn(final String cardId, final String financialStatementId, final String contentTypes);

    /**
     * Metodo de mapeo de salida
     *
     * @param dtoInt objeto con los datos de salida
     * @return DocumentRequest Objeto para el consumo de la capa de presentacion
     */
    DocumentRequest mapOut(DTOIntCardStatement dtoInt);

}
