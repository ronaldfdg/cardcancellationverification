package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 22/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "disposedBalanceStatement", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "disposedBalanceStatement", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DisposedBalanceStatement implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<AmountFinancialStatement> pendingBalances;

    public List<AmountFinancialStatement> getPendingBalances() {
        return pendingBalances;
    }

    public void setPendingBalances(List<AmountFinancialStatement> pendingBalances) {
        this.pendingBalances = pendingBalances;
    }
}
