package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardFinancialStatementDocumentMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.attachment.ByteDataSource;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
@Mapper("getCardFinancialStatementDocumentMapper")
public class GetCardFinancialStatementDocumentMapper implements IGetCardFinancialStatementDocumentMapper {
    private static final Log LOG = LogFactory.getLog(GetCardFinancialStatementDocumentMapper.class);

    @Override
    public MultipartBody mapOut(final Document document, final String contentType) {
        LOG.info("... called method GetCardFinancialStatementDocumentMapper.mapOut ...");
        if (document == null) {
            return null;
        }
        final byte[] bytes = java.util.Base64.getDecoder().decode(document.getDocumento());

        List<Attachment> attachments = new ArrayList<>();
        attachments.add(new Attachment("rootPart", MediaType.APPLICATION_JSON, new Document()));
        attachments.add(new Attachment("attachmentPart", contentType, new ByteDataSource(bytes)));

        return new MultipartBody(attachments);
    }
}
