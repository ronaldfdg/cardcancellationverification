package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Block;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardBlockMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK;

/**
 * @author Entelgy
 */
@Mapper
public class ModifyCardBlockMapper implements IModifyCardBlockMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardBlockMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private EnumMapper enumMapper;

    @Autowired
    private MapperUtils mapperUtils;

    /**
     * @see IModifyCardBlockMapper#mapInput(String, String, Block)
     */
    @Override
    public DTOIntBlock mapInput(final String cardId, final String blockId, final Block block) {
        LOG.info("... called method ModifyCardBlockMapper.mapInput ...");
        final DTOIntBlock dtoIntBlock = new DTOIntBlock();
        final DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK));
        dtoIntBlock.setCard(dtoIntCard);
        dtoIntBlock.setBlockId(enumMapper.getBackendValue("cards.block.blockId", blockId));
        dtoIntBlock.setIsActive(mapperUtils.convertBooleanToString(block.getIsActive()));
        dtoIntBlock.setIsReissued(mapperUtils.convertBooleanToString(block.getIsReissued()));
        dtoIntBlock.setAdditionalInformation(block.getAdditionalInformation());
        if (block.getReason() != null) {
            dtoIntBlock.setReasonId(block.getReason().getId());
        }

        return dtoIntBlock;
    }

}