package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "ReportCardCreation", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "ReportCardCreation", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportCardCreation implements Serializable {

    private static final long serialVersionUID = 1L;

    private CardType cardType;

    private String number;

    private NumberType numberType;

    private BrandAssociation brandAssociation;

    private Product product;

    private List<Currency> currencies;

    private Boolean hasPrintedHolderName;

    private List<ParticipantReport> participants;

    private PaymentMethod paymentMethod;

    private List<Amount> grantedCredits;

    private List<RelatedContract> relatedContracts;

    private List<Delivery> deliveries;

    private ContractingBranch contractingBranch;

    private LoyaltyProgram loyaltyProgram;

    private String proposalStatus;

    private String offerId;

    private String channel;

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public BrandAssociation getBrandAssociation() {
        return brandAssociation;
    }

    public void setBrandAssociation(BrandAssociation brandAssociation) {
        this.brandAssociation = brandAssociation;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public Boolean getHasPrintedHolderName() {
        return hasPrintedHolderName;
    }

    public void setHasPrintedHolderName(Boolean hasPrintedHolderName) {
        this.hasPrintedHolderName = hasPrintedHolderName;
    }

    public List<ParticipantReport> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ParticipantReport> participants) {
        this.participants = participants;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<Amount> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Amount> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public List<Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    public ContractingBranch getContractingBranch() {
        return contractingBranch;
    }

    public void setContractingBranch(ContractingBranch contractingBranch) {
        this.contractingBranch = contractingBranch;
    }

    public LoyaltyProgram getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgram loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public String getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(String proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
