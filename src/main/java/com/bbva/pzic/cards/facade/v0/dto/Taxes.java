package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "Taxes", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "Taxes", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Taxes implements Serializable {

    private static final long serialVersionUID = 1L;

    private TotalTaxes totalTaxes;
    private List<ItemizeTaxes> itemizeTaxes;

    public TotalTaxes getTotalTaxes() {
        return totalTaxes;
    }

    public void setTotalTaxes(TotalTaxes totalTaxes) {
        this.totalTaxes = totalTaxes;
    }

    public List<ItemizeTaxes> getItemizeTaxes() {
        return itemizeTaxes;
    }

    public void setItemizeTaxes(List<ItemizeTaxes> itemizeTaxes) {
        this.itemizeTaxes = itemizeTaxes;
    }
}
