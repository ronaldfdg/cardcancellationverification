package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.facade.v0.mapper.IListInstallmentPlansMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS;

/**
 * Created on 08/02/2018.
 *
 * @author Entelgy
 */
@Mapper("listInstallmentPlansMapper")
public class ListInstallmentPlansMapper implements IListInstallmentPlansMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public InputListInstallmentPlans mapIn(final String cardId, final String paginationKey, final Long pageSize) {
        InputListInstallmentPlans installmentPlans = new InputListInstallmentPlans();
        installmentPlans.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS));
        installmentPlans.setPaginationKey(paginationKey);
        installmentPlans.setPageSize(pageSize);
        return installmentPlans;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<InstallmentsPlan>> mapOut(final DTOInstallmentsPlanList dtoInstallmentsPlanList, final Pagination pagination) {
        if (dtoInstallmentsPlanList == null || CollectionUtils.isEmpty(dtoInstallmentsPlanList.getData())) {
            return null;
        }
        return ServiceResponse.data(dtoInstallmentsPlanList.getData()).pagination(pagination).build();
    }
}
