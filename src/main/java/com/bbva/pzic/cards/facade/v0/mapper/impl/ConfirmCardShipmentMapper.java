package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.facade.v0.dto.ConfirmShipment;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;
import com.bbva.pzic.cards.facade.v0.mapper.IConfirmCardShipmentMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ConfirmCardShipmentMapper implements IConfirmCardShipmentMapper {

    private static final Log LOG = LogFactory.getLog(ConfirmCardShipmentMapper.class);

    @Override
    public InputConfirmCardShipment mapIn(String shipmentId, ConfirmShipment confirmShipment) {
        LOG.info("... called method ConfirmCardShipmentMapper.mapIn ...");
        InputConfirmCardShipment inputConfirmCardShipment = new InputConfirmCardShipment();
        inputConfirmCardShipment.setShipmentId(shipmentId);
        inputConfirmCardShipment.setBiometricId(confirmShipment.getBiometricId());
        inputConfirmCardShipment.setShipmentAddressId(mapInShipmentAddress(confirmShipment.getShipmentAddress()));
        if (confirmShipment.getCurrentAddress() != null && confirmShipment.getCurrentAddress().getLocation() != null &&
                confirmShipment.getCurrentAddress().getLocation().getGeolocation() != null) {
            inputConfirmCardShipment.setCurrentAddressLocationGeolocationLatitude(confirmShipment.getCurrentAddress().getLocation().getGeolocation().getLatitude());
            inputConfirmCardShipment.setCurrentAddressLocationGeolocationLongitude(confirmShipment.getCurrentAddress().getLocation().getGeolocation().getLongitude());
        }
        return inputConfirmCardShipment;
    }

    private String mapInShipmentAddress(final ShipmentAddress shipmentAddress) {
        if (shipmentAddress == null) {
            return null;
        }

        return shipmentAddress.getId();
    }
}
