package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.canonic.Pin;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardPinMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyCardPinMapper implements IModifyCardPinMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntPin mapIn(String cardId, Pin pin) {
        DTOIntPin dtoIntPin = new DTOIntPin();
        dtoIntPin.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_PIN));
        dtoIntPin.setPin(pin.getPin());

        return dtoIntPin;
    }
}
