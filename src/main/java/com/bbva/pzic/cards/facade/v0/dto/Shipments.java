package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "shipments", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "shipments", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Shipments implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Unique identifier of the shipment.
     */
    private String id;
    /**
     * Shipment status.
     */
    private ShipmentStatus status;
    /**
     * Owner of the card.
     */
    private Participant participant;
    /**
     * Shipping company.
     */
    private ShippingCompany shippingCompany;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ShipmentStatus getStatus() {
        return status;
    }

    public void setStatus(ShipmentStatus status) {
        this.status = status;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public ShippingCompany getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(ShippingCompany shippingCompany) {
        this.shippingCompany = shippingCompany;
    }
}
