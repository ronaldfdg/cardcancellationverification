package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.facade.v1.dto.Activation;
import com.bbva.pzic.cards.facade.v1.mapper.IModifyCardsActivationsMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 24/06/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyCardsActivationsMapper implements IModifyCardsActivationsMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardsActivationsMapper.class);

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public DTOIntActivation mapIn(final String activationId, final Activation activation) {
        LOG.info("... Invoking method ModifyCardsActivationsMapper.mapIn ...");
        DTOIntActivation input = new DTOIntActivation();
        input.setCustomerId(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID));
        input.setActivationId(activationId);
        input.setIsActive(activation.getIsActive());
        return input;
    }
}
