package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "AvailableBalance", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "AvailableBalance", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AvailableBalance implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * This balance means the maximum expendable amount at the moment of
     * retrieval. This balance may be provided in several currencies (depending
     * on the country), represented by the current available balance monetary
     * amount and the currency related to that amount. For each card type:<br>
     * <ul>
     * <li>Credit cards: This balance matches the remaining granted credit.</li>
     * <li>Debit cards: This balance matches the current balance of the account
     * from where the debit card takes the money.</li>
     * <li>Prepaid cards: This balance matches the remaining loaded amount.</li>
     * </ul>
     */
    private List<CurrentBalance> currentBalances;
    /**
     * This balance may be provided in several currencies (depending on the
     * country), represented by the posted available balance monetary amount and
     * the currency related to that amount. For each card type:<br>
     * <ul>
     * <li>Credit cards: This balance is the maximum expendable amount
     * (remaining granted credit) calculated at the end of the last working day.
     * </li>
     * <li>Debit cards: This balance is the maximum expendable amount calculated
     * at the end of the last working day (posted balance of the account from
     * where the debit card takes the money).</li>
     * <li>Prepaid cards: This balance does not apply to prepaid cards.</li>
     * </ul>
     */
    private List<PostedBalance> postedBalances;
    /**
     * This balance may be provided in several currencies (depending on the
     * country), represented by the pending available balance monetary amount
     * and the currency related to that amount. For each card type:<br>
     * <ul>
     * <li>Credit cards: This balance is the aggregated amount of all the
     * transactions done with the card. This value matches the result of
     * substracting postedBalances to currentBalances. This value is 0 or a
     * negative value.</li>
     * <li>Debit cards: This balance is the aggregated amount of all the
     * transactions done with the card but not settled yet. This value is 0 or a
     * negative value.</li>
     * <li>Prepaid cards: This balance does not apply to prepaid cards.</li>
     * </ul>
     */
    private List<PendingBalance> pendingBalances;

    public List<CurrentBalance> getCurrentBalances() {
        return currentBalances;
    }

    public void setCurrentBalances(List<CurrentBalance> currentBalances) {
        this.currentBalances = currentBalances;
    }

    public List<PostedBalance> getPostedBalances() {
        return postedBalances;
    }

    public void setPostedBalances(List<PostedBalance> postedBalances) {
        this.postedBalances = postedBalances;
    }

    public List<PendingBalance> getPendingBalances() {
        return pendingBalances;
    }

    public void setPendingBalances(List<PendingBalance> pendingBalances) {
        this.pendingBalances = pendingBalances;
    }
}