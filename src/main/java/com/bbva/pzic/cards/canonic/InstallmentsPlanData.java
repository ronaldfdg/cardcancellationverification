package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/07/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "InstallmentsPlanData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "InstallmentsPlanData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstallmentsPlanData implements Serializable {

    private static final long serialVersionUID = 1L;

    private InstallmentsPlan data;

    public InstallmentsPlan getData() {
        return data;
    }

    public void setData(InstallmentsPlan data) {
        this.data = data;
    }
}
