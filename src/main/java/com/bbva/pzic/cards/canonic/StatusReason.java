package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 26/10/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "StatusReason", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "StatusReason", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusReason implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unique reason identifier.
     */
    private String id;
    /**
     * Name of the reason.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
