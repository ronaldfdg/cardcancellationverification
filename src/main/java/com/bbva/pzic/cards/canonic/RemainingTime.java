package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "remainingTime", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "remainingTime", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class RemainingTime implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unity time remaining to complete the evaluation period.
     */
    private String unit;
    /**
     * Number of unity time remaining until the next date when the Facts will be
     * evaluated.
     */
    private Integer number;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}