package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "creditLimitIncrease", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "creditLimitIncrease", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditLimitIncrease implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Limit minimum  that the customer can request.
     */
    private List<Import> minimumAmount;
    /**
     * Limit maximum  that the customer can request.
     */
    private List<Import> maximumAmount;
    /**
     * Amount offered to the client. This value is between the minimum and maximum limits.
     */
    private List<Import> suggestedAmount;

    public List<Import> getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(List<Import> minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public List<Import> getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(List<Import> maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public List<Import> getSuggestedAmount() {
        return suggestedAmount;
    }

    public void setSuggestedAmount(List<Import> suggestedAmount) {
        this.suggestedAmount = suggestedAmount;
    }
}
