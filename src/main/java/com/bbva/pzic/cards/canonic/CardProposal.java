package com.bbva.pzic.cards.canonic;

import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "cardProposal", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "cardProposal", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardProposal implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product title.
     */
    private Title title;
    /**
     * Financial product type.
     */
    private CardType cardType;
    /**
     * On a business card the participantId will be the authorized person that
     * will hold the card. It is required to specify AUTHORIZED as participant
     * type also.
     */
    private AuthorizedParticipant participant;
    /**
     * Granted credit. This amount may be provided in several currencies
     * (depending on the country). This attribute is mandatory for credit cards.
     */
    private List<Import> grantedCredits;
    /**
     * Card delivery details for a physical card.
     */
    private Delivery delivery;
    /**
     * Entity associated to card.
     */
    private BankType bank;
    /**
     * Unique offer associated with the card.
     */
    private String offerId;
    /**
     * Porposal unique identifier.
     */
    private String id;
    /**
     * The customer contact information.
     */
    private List<ContactDetail> contactDetails;
    /**
     * String based on the ISO-8601 time stamp format to specify the operation
     * date.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar operationDate;

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public AuthorizedParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(AuthorizedParticipant participant) {
        this.participant = participant;
    }

    public List<Import> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Import> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }
}