package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "AdditionalInformation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "AdditionalInformation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Brief information that allows customers to know details about the
     * operative that originates the transaction.
     */
    private String reference;
    /**
     * Additional data that sometimes complements transaction information.
     */
    private String additionalData;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}