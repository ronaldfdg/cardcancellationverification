package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * Created on 4/07/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Rate", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Rate", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rate implements Serializable {

    private static final long serialVersionUID = 1L;

    private RateType rateType;

    private String percentage;
    /**
     * Mode in which the collection rate is applied.
     */
    private Mode mode;
    /**
     * Detail mode in which the collection of the rate is applied.
     */
    private Percentage unit;

    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date calculationDate;

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Percentage getUnit() {
        return unit;
    }

    public void setUnit(Percentage unit) {
        this.unit = unit;
    }

    public Date getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Date calculationDate) {
        this.calculationDate = calculationDate;
    }

}
