package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "fee", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "fee", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fee implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Detail of each fee.
     */
    private List<ItemizeFee> itemizeFees;

    public List<ItemizeFee> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<ItemizeFee> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }
}