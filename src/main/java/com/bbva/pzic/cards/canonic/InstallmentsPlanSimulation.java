package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "InstallmentsPlanSimulation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "InstallmentsPlanSimulation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstallmentsPlanSimulation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String transactionId;

    private Terms terms;

    private Money capital;

    private Money interest;

    private Money total;

    private List<Rate> rates;

    private List<ScheduleInstallment> scheduledPayments;
    /**
     * Amount corresponding to the first payment of the financing plan.
     */
    private Import firstInstallment;
    /**
     * String based on ISO-8601 date format for providing the first installment date.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date firstInstallmentDate;
    /**
     * String based on ISO-8601 date format (DD/MM/YYYY) for the date of the simulation.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date simulationDate;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Terms getTerms() {
        return terms;
    }

    public void setTerms(Terms terms) {
        this.terms = terms;
    }

    public Money getCapital() {
        return capital;
    }

    public void setCapital(Money capital) {
        this.capital = capital;
    }

    public Money getInterest() {
        return interest;
    }

    public void setInterest(Money interest) {
        this.interest = interest;
    }

    public Money getTotal() {
        return total;
    }

    public void setTotal(Money total) {
        this.total = total;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public List<ScheduleInstallment> getScheduledPayments() {
        return scheduledPayments;
    }

    public void setScheduledPayments(List<ScheduleInstallment> scheduledPayments) {
        this.scheduledPayments = scheduledPayments;
    }

    public Import getFirstInstallment() {
        return firstInstallment;
    }

    public void setFirstInstallment(Import firstInstallment) {
        this.firstInstallment = firstInstallment;
    }

    public Date getFirstInstallmentDate() {
        return firstInstallmentDate;
    }

    public void setFirstInstallmentDate(Date firstInstallmentDate) {
        this.firstInstallmentDate = firstInstallmentDate;
    }

    public Date getSimulationDate() {
        return simulationDate;
    }

    public void setSimulationDate(Date simulationDate) {
        this.simulationDate = simulationDate;
    }
}
