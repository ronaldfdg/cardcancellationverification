package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "MoneyFlow", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "MoneyFlow", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class MoneyFlow implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Money flow identifier.
     */
    private String id;
    /**
     * Description of money flow.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}