package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "SecurityDataArray", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "SecurityDataArray", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurityDataArray implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<SecurityData> data;

    public List<SecurityData> getData() {
        return data;
    }

    public void setData(List<SecurityData> data) {
        this.data = data;
    }
}
