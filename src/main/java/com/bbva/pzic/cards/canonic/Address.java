package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.cards.facade.v1.dto.Location;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "address", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "address", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Shipping address identifier for the card delivery. It is the identifier
     * of an address that belongs to the customer. DISCLAIMER: This field is
     * only used when the stored addresses of the customer have been obtained
     * previously and one of them is selected.
     */
    private String id;
    @DatoAuditable(omitir = true)
    private String addressName;
    /**
     * Address delivery type identifier.
     */
    private String addressType;
    /**
     * Address location.
     */
    private Location location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}