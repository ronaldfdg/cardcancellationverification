package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "BlockData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "BlockData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class BlockData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Block data;

    public Block getData() {
        return data;
    }

    public void setData(Block data) {
        this.data = data;
    }
}