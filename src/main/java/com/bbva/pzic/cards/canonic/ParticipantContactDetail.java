package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "participantContactDetail", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "participantContactDetail", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParticipantContactDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information value. This value will be masked when basic
     * authentication state. If the authentication state is advanced the value
     * will be clear.
     */
    private String contactValue;
    /**
     * Contact information type.
     */
    private ContactType contactType;

    public String getContactValue() {
        return contactValue;
    }

    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }
}