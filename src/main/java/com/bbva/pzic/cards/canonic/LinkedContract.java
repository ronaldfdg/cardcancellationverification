package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "LinkedContract", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "LinkedContract", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class LinkedContract implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated to the contract.
     */
    private String id;
    /**
     * Contract number.
     */
    private String number;
    /**
     * Contract number type based on the financial product type.
     */
    private ContractNumberType numberType;
    /**
     * Financial product associated to the contract.
     */
    private Product product;
    /**
     * Alias assigned to the contract customized by the user.
     */
    private String alias;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ContractNumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(ContractNumberType numberType) {
        this.numberType = numberType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}