package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Condition", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Condition", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Condition implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Condition identifier.
     */
    private String conditionId;
    /**
     * Condition name or description.
     */
    private String name;
    /**
     * Condition periodical information. It includes the date when the condition
     * will be evaluated.
     */
    private Period period;
    /**
     * Facts that must be met to apply the condition. All the facts will need to
     * be fulfilled in order to apply this condition.
     */
    private List<Fact> facts;
    /**
     * Outcome or result of applying the condition.
     */
    private List<Outcome> outcomes;
    /**
     * Cumulative amount of the consumption made in the period of time used for
     * evaluation of outcome.
     */
    private AccumulatedAmount accumulatedAmount;

    public String getConditionId() {
        return conditionId;
    }

    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public List<Fact> getFacts() {
        return facts;
    }

    public void setFacts(List<Fact> facts) {
        this.facts = facts;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    public AccumulatedAmount getAccumulatedAmount() {
        return accumulatedAmount;
    }

    public void setAccumulatedAmount(AccumulatedAmount accumulatedAmount) {
        this.accumulatedAmount = accumulatedAmount;
    }
}
