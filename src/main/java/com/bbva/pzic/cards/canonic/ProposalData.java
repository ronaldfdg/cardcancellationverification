package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "proposalData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "proposalData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProposalData implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Proposal Data
     */
    private Proposal data;

    public Proposal getData() {
        return data;
    }

    public void setData(Proposal data) {
        this.data = data;
    }
}