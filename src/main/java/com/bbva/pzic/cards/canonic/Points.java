package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Points", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Points", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Points implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * For each purchase made with a credit card will receive points that can be
     * used for more purchases in the country.
     */
    private BigDecimal obtained;
    /**
     * Points used in the purchase of products or services.
     */
    private BigDecimal applied;
    /**
     * Monetary amount related to points.
     */
    private Import monetaryEquivalence;
    /**
     * Total monetary amount of purchase with points.
     */
    private Import totalAmount;

    public BigDecimal getObtained() {
        return obtained;
    }

    public void setObtained(BigDecimal obtained) {
        this.obtained = obtained;
    }

    public BigDecimal getApplied() {
        return applied;
    }

    public void setApplied(BigDecimal applied) {
        this.applied = applied;
    }

    public Import getMonetaryEquivalence() {
        return monetaryEquivalence;
    }

    public void setMonetaryEquivalence(Import monetaryEquivalence) {
        this.monetaryEquivalence = monetaryEquivalence;
    }

    public Import getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Import totalAmount) {
        this.totalAmount = totalAmount;
    }
}