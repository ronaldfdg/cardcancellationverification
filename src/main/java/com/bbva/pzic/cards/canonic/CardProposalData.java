package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "cardProposalData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "cardProposalData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardProposalData implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * CardProposal Data
     */
    private CardProposal data;

    public CardProposal getData() {
        return data;
    }

    public void setData(CardProposal data) {
        this.data = data;
    }
}