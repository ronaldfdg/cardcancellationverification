package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "offer", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "offer", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Unique offer associated with the card.
     */
    private String id;
    /**
     * Type of offer.
     */
    private OfferType offerType;
    /**
     * It indicates the source of origin of the offer.
     */
    private Origin origin;
    /**
     * Indicates whether the quantity fields you are requesting are editable.
     */
    private Boolean isEditable;
    /**
     * Detailed information according to the offer type.
     */
    private CreditLimitIncrease details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OfferType getOfferType() {
        return offerType;
    }

    public void setOfferType(OfferType offerType) {
        this.offerType = offerType;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(Boolean editable) {
        isEditable = editable;
    }

    public CreditLimitIncrease getDetails() {
        return details;
    }

    public void setDetails(CreditLimitIncrease details) {
        this.details = details;
    }
}
