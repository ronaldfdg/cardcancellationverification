package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "RelatedContractData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "RelatedContractData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class RelatedContractData implements Serializable {

    private static final long serialVersionUID = 1L;

    private RelatedContract data;

    public RelatedContract getData() {
        return data;
    }

    public void setData(RelatedContract data) {
        this.data = data;
    }
}
