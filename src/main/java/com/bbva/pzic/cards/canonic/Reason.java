package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Reason", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Reason", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reason implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier for the reason of blocking. This identifier permits to
     * classify this reason according to the options available for each type of
     * blockade.
     */
    private String id;
    /**
     * Description of the reason of blocking.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}