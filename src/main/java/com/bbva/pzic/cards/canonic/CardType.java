package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "CardType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "CardType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product type identifier.
     */
    private String id;
    /**
     * Financial product type description.
     */
    @DatoAuditable(omitir = true)
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}