package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Outcome", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Outcome", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Outcome implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Outcome type.
     */
    private OutcomeType outcomeType;
    /**
     * Outcome percental value.
     */
    private BigDecimal feePercentage;
    /**
     * Outcome amount.
     */
    private FeeAmount feeAmount;
    /**
     * Property or value over which Outcome fee applies.
     */
    private Apply apply;
    /**
     * String based on ISO-8601 date format for specifying the next date when
     * the Outcome will be generated.
     */
    @XmlSchemaType(name = "datetime")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dueDate;

    public OutcomeType getOutcomeType() {
        return outcomeType;
    }

    public void setOutcomeType(OutcomeType outcomeType) {
        this.outcomeType = outcomeType;
    }

    public BigDecimal getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(BigDecimal feePercentage) {
        this.feePercentage = feePercentage;
    }

    public FeeAmount getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(FeeAmount feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Apply getApply() {
        return apply;
    }

    public void setApply(Apply apply) {
        this.apply = apply;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}