package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Period", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Period", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Period implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Period identifier.
     */
    private String id;
    /**
     * Period name.
     */
    private String name;
    /**
     * String based on ISO-8601 date format for specifying the next date when
     * the Facts will be evaluated.
     */
    @XmlSchemaType(name = "dateTime")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date checkDate;
    /**
     * Time remaining to complete the evaluation period
     */
    private RemainingTime remainingTime;
    /**
     * String based on ISO-8601 date format indicating the start of the period
     * of time covering the outcome to evaluate conditions.
     */
    @XmlSchemaType(name = "dateTime")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date startDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public RemainingTime getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(RemainingTime remainingTime) {
        this.remainingTime = remainingTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
