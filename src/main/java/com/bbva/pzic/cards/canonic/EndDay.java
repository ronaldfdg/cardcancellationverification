package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/12/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "endDay", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "endDay", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class EndDay implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Day when the payment will be made.
     */
    private Integer day;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}
