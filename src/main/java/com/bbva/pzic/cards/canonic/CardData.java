package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 30/12/2016.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "CardData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "CardData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Card data;

    public Card getData() {
        return data;
    }

    public void setData(Card data) {
        this.data = data;
    }
}
