package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "companyEmbossingNameType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "companyEmbossingNameType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyEmbossingNameType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Company embossing name type identifier.
     */
    private String id;
    /**
     * Company embossing name type description.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}