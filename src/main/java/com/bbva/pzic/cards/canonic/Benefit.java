package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Benefit", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Benefit", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Benefit implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Benefit identifier.
     */
    private String id;
    /**
     * Benefit identifier.
     */
    private String benefitId;
    /**
     * Benefit description.
     */
    private String description;
    /**
     * Benefit code.
     */
    private String code;
    /**
     * String based on ISO-8601 for specifying the date when the benefit will
     * expire.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date expirationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBenefitId() {
        return benefitId;
    }

    public void setBenefitId(String benefitId) {
        this.benefitId = benefitId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}