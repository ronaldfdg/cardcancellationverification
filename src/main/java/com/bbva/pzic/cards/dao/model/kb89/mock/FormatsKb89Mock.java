package com.bbva.pzic.cards.dao.model.kb89.mock;

import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS1KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS2KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTSCKB89;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public final class FormatsKb89Mock {

    private static final FormatsKb89Mock INSTANCE = new FormatsKb89Mock();
    private ObjectMapperHelper objectMapper;

    private FormatsKb89Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatsKb89Mock getInstance() {
        return INSTANCE;
    }

    public List<FormatoKTSCKB89> getListFormatoKTSCKB89() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream(
                                "com/bbva/pzic/cards/dao/model/kb89/mock/formatoKTSCKB89.json"),
                new TypeReference<List<FormatoKTSCKB89>>() {
                });
    }


    public List<FormatoKTS1KB89> getListFormatoKTS1KB89() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream(
                                "com/bbva/pzic/cards/dao/model/kb89/mock/formatoKTS1KB89.json"),
                new TypeReference<List<FormatoKTS1KB89>>() {
                });
    }


    public List<FormatoKTS2KB89> getListFormatoKTS2KB89() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream(
                                "com/bbva/pzic/cards/dao/model/kb89/mock/formatoKTS2KB89.json"),
                new TypeReference<List<FormatoKTS2KB89>>() {
                });
    }

}