package com.bbva.pzic.cards.dao.model.mpb1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPB1</code>
 *
 * @see PeticionTransaccionMpb1
 * @see RespuestaTransaccionMpb1
 */
@Component
public class TransaccionMpb1 implements InvocadorTransaccion<PeticionTransaccionMpb1,RespuestaTransaccionMpb1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpb1 invocar(PeticionTransaccionMpb1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb1.class, RespuestaTransaccionMpb1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpb1 invocarCache(PeticionTransaccionMpb1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb1.class, RespuestaTransaccionMpb1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
