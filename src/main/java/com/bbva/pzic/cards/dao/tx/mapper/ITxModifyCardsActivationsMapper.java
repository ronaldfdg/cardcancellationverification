package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpaf.FormatoMPM0AFE;

/**
 * Created on 24/06/2020.
 *
 * @author Entelgy
 */
public interface ITxModifyCardsActivationsMapper {

    FormatoMPM0AFE mapIn(DTOIntActivation dtoInt);

}
