package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMENG2;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;

import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
public interface ITxListCardActivationsMapperV0 {
    /**
     * @param dtoIn
     * @return
     */
    FormatoMPMENG2 mapInt(DTOIntCard dtoIn);

    List<Activation> mapOut(FormatoMPMS1G2 formatoMPMS1G2, List<Activation> activationList);

}
