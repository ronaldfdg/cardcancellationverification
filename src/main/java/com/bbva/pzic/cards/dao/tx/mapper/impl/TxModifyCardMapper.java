package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpb5.FormatoMPM0B5E;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 14/08/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxModifyCardMapper implements ITxModifyCardMapper {

    private static final Log LOG = LogFactory.getLog(TxModifyCardMapper.class);

    @Override
    public FormatoMPM0B5E mapInput(DTOIntCard dtoIntCard) {
        LOG.info("... called method TxModifyCard.mapInput ...");
        final FormatoMPM0B5E formatoMPM0B5E = new FormatoMPM0B5E();
        formatoMPM0B5E.setIdetarj(dtoIntCard.getCardId());
        formatoMPM0B5E.setEsttarj(dtoIntCard.getStatusId());
        formatoMPM0B5E.setCodraz(dtoIntCard.getStatusReasonId());

        return formatoMPM0B5E;
    }
}
