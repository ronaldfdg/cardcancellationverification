package com.bbva.pzic.cards.dao.model.productofferdetail;

/**
 * Created on 06/02/2019.
 *
 * @author Entelgy
 */
public final class CardHolderSimulationQueryParams {

    public static final String ID_PROPUESTA = "idPropuesta";
    public static final String CUSTOMER_ID = "customerId";
    public static final String BIN = "bin";
    public static final String ID_SUB_PRODUCTO = "idSubproducto";

    private CardHolderSimulationQueryParams() {
    }
}
