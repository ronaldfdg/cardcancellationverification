package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.Image;
import com.bbva.pzic.cards.dao.model.awsimages.*;
import com.bbva.pzic.cards.dao.rest.mapper.IRestListImagesCoversMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 04/11/2019.
 *
 * @author Entelgy
 */
@Mapper
public class RestListImagesCoversMapper implements IRestListImagesCoversMapper {

    private static final Log LOG = LogFactory.getLog(RestListImagesCoversMapper.class);

    private static final DateTimeFormatter BASE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final DateTimeFormatter DESTINY_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    public AWSImagesRequest mapIn(final List<Card> cards) {
        LOG.info("... called method RestListImagesCoversMapper.mapIn ...");

        if (CollectionUtils.isEmpty(cards)) {
            return null;
        }

        AWSImagesRequest request = new AWSImagesRequest();

        List<ModelCard> modelCards = new ArrayList<>();
        boolean isFirstCard = true;
        for (Card card : cards) {
            if (CollectionUtils.isNotEmpty(card.getImages())) {
                for (Image image : card.getImages()) {
                    String s = image.getUrl();
                    if (StringUtils.isEmpty(s)) {
                        continue;
                    }
                    LOG.debug(String.format("Splitting value pair '%s'", s));
                    modelCards.add(getValorDTOUrl(s));
                    if (isFirstCard) {
                        getValorDTOUrlData0(request, s);
                        isFirstCard = false;
                    }
                }
            }
        }

        request.setCards(modelCards);
        return request;
    }

    private ModelCard getValorDTOUrl(final String urlParams) {
        String[] nameValuePair = urlParams.split("&");
        ModelCard card = new ModelCard();

        for (String s : nameValuePair) {
            String[] keyValue = s.split("=");
            LOG.debug(String.format("Length of keyValue '%s' for '%s'", keyValue.length, s));

            if (keyValue.length == 2) {
                String key = keyValue[0];
                String value = keyValue[1];

                switch (key) {
                    case "pg":
                        card.setCodProd(value);
                        break;

                    case "bin":
                        card.setBin(value);
                        break;

                    case "type":
                        card.getType().setId(value);
                        break;

                    case "issue_date":
                        card.setIssueDate(parseDate(value));
                        break;

                    case "back":
                        card.setIsBackImage(Boolean.valueOf(value));
                        break;

                    default:
                        LOG.warn(String.format("Unrecognized key %s", key));
                        break;
                }
            }
        }
        return card;
    }

    private String parseDate(final String value) {
        try {
            // "yyyyMMdd" -> "yyyy-MM-dd"
            LocalDate parse = LocalDate.parse(value, BASE_FORMAT);
            return parse.format(DESTINY_FORMAT);
        } catch (DateTimeParseException e) {
            LOG.warn(e.getMessage());
            return null;
        }
    }

    private void getValorDTOUrlData0(final AWSImagesRequest input, final String urlParams) {
        String[] nameValuePair = urlParams.split("&");

        for (String s : nameValuePair) {
            String[] keyValue = s.split("=");
            LOG.debug(String.format("Length of keyValue '%s' for '%s'", keyValue.length, s));

            if (keyValue.length == 2) {
                String key = keyValue[0];
                String value = keyValue[1];

                switch (key) {
                    case "country":
                        input.setCountry(new ModelCountry());
                        input.getCountry().setId(value);
                        break;

                    case "width":
                        input.setWidth(value);
                        break;

                    case "height":
                        input.setHeight(value);
                        break;

                    default:
                        LOG.warn(String.format("Unrecognized key %s", key));
                        break;
                }
            }
        }
    }

    @Override
    public void mapOut(final AWSImagesResponse response, final List<Card> cards) {
        LOG.info("... called method RestListImagesCoversMapper.mapOut ...");
        if (response != null) {
            int j = 0;
            for (Card card : cards) {
                if (CollectionUtils.isNotEmpty(card.getImages())) {
                    for (int i = 0; i < card.getImages().size() && j < response.getData().size(); i++) {
                        Image image = card.getImages().get(i);

                        if (StringUtils.isNotEmpty(image.getUrl())) {
                            List<ModelImage> imagesCover = response.getData().get(j);

                            boolean urlNotFound = true;
                            for (ModelImage imageCover : imagesCover) {
                                if ("url".equals(imageCover.getKey())) {
                                    LOG.debug(String.format("URL founded, updating with value %s", imageCover.getValue()));
                                    image.setUrl(imageCover.getValue());
                                    urlNotFound = false;
                                    break;
                                }
                            }
                            if (urlNotFound) {
                                LOG.warn("URL not found, updating with empty values");
                                card.getImages().set(i, new Image());
                            }
                            j++;
                        }
                    }
                }
            }
        }
    }
}
