package com.bbva.pzic.cards.dao.model.mpg3.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMS1G3;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 11/10/2017.
 *
 * @author Entelgy
 */
public class FormatoMPMS1G3Mock {

    private final ObjectMapperHelper mapper;

    public FormatoMPMS1G3Mock() {
        mapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPMS1G3 getFormatoMPMS1G3() {
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "com/bbva/pzic/cards/dao/model/mpg3/mock/formatoMPMS1G3.json"),
                    FormatoMPMS1G3.class);
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }
}
