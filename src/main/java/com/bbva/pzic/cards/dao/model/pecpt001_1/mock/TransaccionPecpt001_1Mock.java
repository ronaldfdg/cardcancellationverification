package com.bbva.pzic.cards.dao.model.pecpt001_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.pecpt001_1.PeticionTransaccionPecpt001_1;
import com.bbva.pzic.cards.dao.model.pecpt001_1.RespuestaTransaccionPecpt001_1;
import com.bbva.pzic.cards.util.Errors;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>PECPT001</code>
 *
 * @see PeticionTransaccionPecpt001_1
 * @see RespuestaTransaccionPecpt001_1
 */
@Component("transaccionPecpt001_1")
public class TransaccionPecpt001_1Mock implements InvocadorTransaccion<PeticionTransaccionPecpt001_1, RespuestaTransaccionPecpt001_1> {

    public static final String TEST_EMPTY = "999";

    @Override
    public RespuestaTransaccionPecpt001_1 invocar(final PeticionTransaccionPecpt001_1 transaccion) {
        try {
            if (StringUtils.isNotEmpty(transaccion.getCardagreement()) &&
                    TEST_EMPTY.equalsIgnoreCase(transaccion.getCardagreement())) {
                return new RespuestaTransaccionPecpt001_1();
            }
            return Pecpt001_1Stubs.getInstance().getShipments();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPecpt001_1 invocarCache(PeticionTransaccionPecpt001_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
