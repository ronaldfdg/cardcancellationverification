package com.bbva.pzic.cards.dao.model.kb94;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>KB94</code>
 *
 * @see PeticionTransaccionKb94
 * @see RespuestaTransaccionKb94
 */
@Component
public class TransaccionKb94 implements InvocadorTransaccion<PeticionTransaccionKb94, RespuestaTransaccionKb94> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKb94 invocar(PeticionTransaccionKb94 transaccion) {
        return servicioTransacciones.invocar(PeticionTransaccionKb94.class, RespuestaTransaccionKb94.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKb94 invocarCache(PeticionTransaccionKb94 transaccion) {
        return servicioTransacciones.invocar(PeticionTransaccionKb94.class, RespuestaTransaccionKb94.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
