package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.dao.model.pecpt004_1.PeticionTransaccionPecpt004_1;
import com.bbva.pzic.cards.dao.model.pecpt004_1.RespuestaTransaccionPecpt004_1;
import com.bbva.pzic.cards.facade.v0.dto.Shipment;

public interface IApxGetCardShipmentMapper {

    PeticionTransaccionPecpt004_1 mapIn(InputGetCardShipment input);

    Shipment mapOut(RespuestaTransaccionPecpt004_1 response);
}
