package com.bbva.pzic.cards.dao.model.ppcut002_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>itemizeFeeOut</code>, utilizado por la clase <code>Itemizefeesout</code></p>
 * 
 * @see Itemizefeesout
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Itemizefeeout {
	
	/**
	 * <p>Campo <code>feeType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "feeType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
	private String feetype;
	
	/**
	 * <p>Campo <code>description</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "description", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
	private String description;
	
	/**
	 * <p>Campo <code>itemizeFeeUnit</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "itemizeFeeUnit", tipo = TipoCampo.DTO)
	private Itemizefeeunit itemizefeeunit;
	
}