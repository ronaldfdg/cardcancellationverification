package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMEN6I;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMS16I;
import com.bbva.pzic.cards.dao.model.mp6i.PeticionTransaccionMp6i;
import com.bbva.pzic.cards.dao.model.mp6i.RespuestaTransaccionMp6i;
import com.bbva.pzic.cards.dao.tx.mapper.ITxSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.SimulateTransactionRefund;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 12/06/2017.
 *
 * @author Entelgy
 */
@Component
public class TxSimulateCardTransactionTransactionRefund extends SingleOutputFormat<InputSimulateCardTransactionTransactionRefund, FormatoMPMEN6I, SimulateTransactionRefund, FormatoMPMS16I> {

    @Resource(name = "txSimulateCardTransactionTransactionRefundMapper")
    private ITxSimulateCardTransactionTransactionRefund mapper;

    @Autowired
    public TxSimulateCardTransactionTransactionRefund(@Qualifier("transaccionMp6i") InvocadorTransaccion<PeticionTransaccionMp6i, RespuestaTransaccionMp6i> transaction) {
        super(transaction, PeticionTransaccionMp6i::new, SimulateTransactionRefund::new, FormatoMPMS16I.class);
    }

    @Override
    protected FormatoMPMEN6I mapInput(InputSimulateCardTransactionTransactionRefund inputSimulateCardTransactionTransactionRefund) {
        return mapper.mapIn(inputSimulateCardTransactionTransactionRefund);
    }

    @Override
    protected SimulateTransactionRefund mapFirstOutputFormat(FormatoMPMS16I formatoMPMS16I, InputSimulateCardTransactionTransactionRefund inputSimulateCardTransactionTransactionRefund, SimulateTransactionRefund simulateTransactionRefund) {
        return mapper.mapOut(formatoMPMS16I);
    }
}
