package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMENG7;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMS1G7;
import com.bbva.pzic.cards.dao.model.mpg7.PeticionTransaccionMpg7;
import com.bbva.pzic.cards.dao.model.mpg7.RespuestaTransaccionMpg7;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardLimitV0Mapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
@Component("txModifyCardLimitV0")
public class TxModifyCardLimitV0
        extends SingleOutputFormat<InputModifyCardLimit, FormatoMPMENG7, Limit, FormatoMPMS1G7> {

    @Resource(name = "txModifyCardLimitV0Mapper")
    private ITxModifyCardLimitV0Mapper txModifyCardLimitV0Mapper;

    @Autowired
    public TxModifyCardLimitV0(@Qualifier("transaccionMpg7") InvocadorTransaccion<PeticionTransaccionMpg7, RespuestaTransaccionMpg7> transaction) {
        super(transaction, PeticionTransaccionMpg7::new, Limit::new, FormatoMPMS1G7.class);
    }

    @Override
    protected FormatoMPMENG7 mapInput(InputModifyCardLimit inputModifyCardLimit) {
        return txModifyCardLimitV0Mapper.mapIn(inputModifyCardLimit);
    }

    @Override
    protected Limit mapFirstOutputFormat(FormatoMPMS1G7 formatoMPMS1G7, InputModifyCardLimit inputModifyCardLimit, Limit limit) {
        return txModifyCardLimitV0Mapper.mapOut(formatoMPMS1G7);
    }
}
