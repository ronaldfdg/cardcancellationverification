package com.bbva.pzic.cards.dao.model.mpg5;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS2G5</code> de la transacci&oacute;n <code>MPG5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS2G5")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS2G5 {

	/**
	 * <p>Campo <code>IDFORPA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDFORPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idforpa;

	/**
	 * <p>Campo <code>DSFORPA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DSFORPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String dsforpa;

	/**
	 * <p>Campo <code>IMPAMIS</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 3, nombre = "IMPAMIS", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
	private BigDecimal impamis;

	/**
	 * <p>Campo <code>MOPAMIS</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "MOPAMIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String mopamis;

}