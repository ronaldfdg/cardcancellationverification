package com.bbva.pzic.cards.dao.model.mpaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPAF</code>
 * 
 * @see PeticionTransaccionMpaf
 * @see RespuestaTransaccionMpaf
 */
@Component
public class TransaccionMpaf implements InvocadorTransaccion<PeticionTransaccionMpaf,RespuestaTransaccionMpaf> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMpaf invocar(PeticionTransaccionMpaf transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpaf.class, RespuestaTransaccionMpaf.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMpaf invocarCache(PeticionTransaccionMpaf transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpaf.class, RespuestaTransaccionMpaf.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}