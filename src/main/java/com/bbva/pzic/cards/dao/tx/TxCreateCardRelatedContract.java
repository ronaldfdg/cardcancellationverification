package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1E;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1S;
import com.bbva.pzic.cards.dao.model.mpv1.PeticionTransaccionMpv1;
import com.bbva.pzic.cards.dao.model.mpv1.RespuestaTransaccionMpv1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardRelatedContractMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
@Component("txCreateCardRelatedContract")
public class TxCreateCardRelatedContract
        extends SingleOutputFormat<DTOInputCreateCardRelatedContract, FormatoMPM0V1E, DTOOutCreateCardRelatedContract, FormatoMPM0V1S> {

    @Resource(name = "txCreateRelatedContractMapper")
    private ITxCreateCardRelatedContractMapper mapper;

    @Autowired
    public TxCreateCardRelatedContract(@Qualifier("transaccionMpv1") InvocadorTransaccion<PeticionTransaccionMpv1, RespuestaTransaccionMpv1> transaction) {
        super(transaction, PeticionTransaccionMpv1::new, DTOOutCreateCardRelatedContract::new, FormatoMPM0V1S.class);
    }

    @Override
    protected FormatoMPM0V1E mapInput(DTOInputCreateCardRelatedContract dtoInputCreateCardRelatedContract) {
        return mapper.mapIn(dtoInputCreateCardRelatedContract);
    }

    @Override
    protected DTOOutCreateCardRelatedContract mapFirstOutputFormat(FormatoMPM0V1S formatoMPM0V1S, DTOInputCreateCardRelatedContract dtoInputCreateCardRelatedContract, DTOOutCreateCardRelatedContract dtoOutCreateCardRelatedContract) {
        return mapper.mapOut(formatoMPM0V1S, dtoInputCreateCardRelatedContract);
    }
}
