package com.bbva.pzic.cards.dao.model.mpgl.mock;

import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMS1GL;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy
 */
public class FormatoMPMS1GLMock {

    private static final FormatoMPMS1GLMock INSTANCE = new FormatoMPMS1GLMock();

    private ObjectMapperHelper mapper;

    public FormatoMPMS1GLMock() {
        mapper = ObjectMapperHelper.getInstance();
    }

    public static FormatoMPMS1GLMock getInstance() {
        return INSTANCE;
    }

    public List<FormatoMPMS1GL> getFormatoMPMS1GL() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpgl/mock/formatoMPMS1GL.json"),
                new TypeReference<List<FormatoMPMS1GL>>() {
                });
    }
}
