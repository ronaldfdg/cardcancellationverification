package com.bbva.pzic.cards.dao.model.pecpt003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>application</code>, utilizado por la clase <code>Shippingdevice</code></p>
 * 
 * @see Shippingdevice
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Application {
	
	/**
	 * <p>Campo <code>name</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 32, signo = true, obligatorio = true)
	private String name;
	
	/**
	 * <p>Campo <code>version</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "version", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String version;
	
}