package com.bbva.pzic.cards.dao.rest.mock.stubs;

import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 04/11/2019.
 *
 * @author Entelgy
 */
public enum AWSImagesStubs {
    INSTANCE;

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    public AWSImagesResponse buildAWSImagesResponse() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/rest/mock/awsImagesResponse.json"), AWSImagesResponse.class);
    }

    public AWSImagesResponse buildAWSImagesHolderSimulationResponse() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/rest/mock/awsImagesHolderSimulationResponse.json"), AWSImagesResponse.class);
    }
}
