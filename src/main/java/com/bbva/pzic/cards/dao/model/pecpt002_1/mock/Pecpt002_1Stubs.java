package com.bbva.pzic.cards.dao.model.pecpt002_1.mock;

import com.bbva.pzic.cards.dao.model.pecpt001_1.RespuestaTransaccionPecpt001_1;
import com.bbva.pzic.cards.dao.model.pecpt002_1.RespuestaTransaccionPecpt002_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 12/02/2020.
 *
 * @author Entelgy
 */
public final class Pecpt002_1Stubs {

    private static final Pecpt002_1Stubs INSTANCE = new Pecpt002_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Pecpt002_1Stubs() {
    }

    public static Pecpt002_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPecpt002_1 buildSpecificShipmentAddressResponse() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/pecpt002_1/mock/specificRespuestaTransaccionPecpt002_1.json"), RespuestaTransaccionPecpt002_1.class);
    }

    public RespuestaTransaccionPecpt002_1 buildStoredShipmentAddressResponse() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/pecpt002_1/mock/storedRespuestaTransaccionPecpt002_1.json"), RespuestaTransaccionPecpt002_1.class);
    }
}
