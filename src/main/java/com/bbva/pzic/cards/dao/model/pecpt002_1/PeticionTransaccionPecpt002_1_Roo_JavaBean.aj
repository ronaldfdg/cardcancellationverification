// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.pecpt002_1;

import com.bbva.pzic.cards.dao.model.pecpt002_1.Entityin;
import com.bbva.pzic.cards.dao.model.pecpt002_1.PeticionTransaccionPecpt002_1;

privileged aspect PeticionTransaccionPecpt002_1_Roo_JavaBean {
    
    /**
     * Gets shipmentid value
     * 
     * @return String
     */
    public String PeticionTransaccionPecpt002_1.getShipmentid() {
        return this.shipmentid;
    }
    
    /**
     * Sets shipmentid value
     * 
     * @param shipmentid
     * @return PeticionTransaccionPecpt002_1
     */
    public PeticionTransaccionPecpt002_1 PeticionTransaccionPecpt002_1.setShipmentid(String shipmentid) {
        this.shipmentid = shipmentid;
        return this;
    }
    
    /**
     * Gets entityin value
     * 
     * @return Entityin
     */
    public Entityin PeticionTransaccionPecpt002_1.getEntityin() {
        return this.entityin;
    }
    
    /**
     * Sets entityin value
     * 
     * @param entityin
     * @return PeticionTransaccionPecpt002_1
     */
    public PeticionTransaccionPecpt002_1 PeticionTransaccionPecpt002_1.setEntityin(Entityin entityin) {
        this.entityin = entityin;
        return this;
    }
    
}
