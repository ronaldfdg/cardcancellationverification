// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcut002_1;

import com.bbva.pzic.cards.dao.model.ppcut002_1.Additionalproducts;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Cardtype;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Contactability;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Deliveries;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Entityin;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Fees;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Grantedcredits;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Image;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Membership;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Paymentmethod;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Physicalsupport;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Product;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Rates;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Specificcontact;
import java.util.List;

privileged aspect Entityin_Roo_JavaBean {
    
    /**
     * Gets deliveries value
     * 
     * @return List
     */
    public List<Deliveries> Entityin.getDeliveries() {
        return this.deliveries;
    }
    
    /**
     * Sets deliveries value
     * 
     * @param deliveries
     * @return Entityin
     */
    public Entityin Entityin.setDeliveries(List<Deliveries> deliveries) {
        this.deliveries = deliveries;
        return this;
    }
    
    /**
     * Gets grantedcredits value
     * 
     * @return List
     */
    public List<Grantedcredits> Entityin.getGrantedcredits() {
        return this.grantedcredits;
    }
    
    /**
     * Sets grantedcredits value
     * 
     * @param grantedcredits
     * @return Entityin
     */
    public Entityin Entityin.setGrantedcredits(List<Grantedcredits> grantedcredits) {
        this.grantedcredits = grantedcredits;
        return this;
    }
    
    /**
     * Gets additionalproducts value
     * 
     * @return List
     */
    public List<Additionalproducts> Entityin.getAdditionalproducts() {
        return this.additionalproducts;
    }
    
    /**
     * Sets additionalproducts value
     * 
     * @param additionalproducts
     * @return Entityin
     */
    public Entityin Entityin.setAdditionalproducts(List<Additionalproducts> additionalproducts) {
        this.additionalproducts = additionalproducts;
        return this;
    }
    
    /**
     * Gets cardtype value
     * 
     * @return Cardtype
     */
    public Cardtype Entityin.getCardtype() {
        return this.cardtype;
    }
    
    /**
     * Sets cardtype value
     * 
     * @param cardtype
     * @return Entityin
     */
    public Entityin Entityin.setCardtype(Cardtype cardtype) {
        this.cardtype = cardtype;
        return this;
    }
    
    /**
     * Gets product value
     * 
     * @return Product
     */
    public Product Entityin.getProduct() {
        return this.product;
    }
    
    /**
     * Sets product value
     * 
     * @param product
     * @return Entityin
     */
    public Entityin Entityin.setProduct(Product product) {
        this.product = product;
        return this;
    }
    
    /**
     * Gets physicalsupport value
     * 
     * @return Physicalsupport
     */
    public Physicalsupport Entityin.getPhysicalsupport() {
        return this.physicalsupport;
    }
    
    /**
     * Sets physicalsupport value
     * 
     * @param physicalsupport
     * @return Entityin
     */
    public Entityin Entityin.setPhysicalsupport(Physicalsupport physicalsupport) {
        this.physicalsupport = physicalsupport;
        return this;
    }
    
    /**
     * Gets paymentmethod value
     * 
     * @return Paymentmethod
     */
    public Paymentmethod Entityin.getPaymentmethod() {
        return this.paymentmethod;
    }
    
    /**
     * Sets paymentmethod value
     * 
     * @param paymentmethod
     * @return Entityin
     */
    public Entityin Entityin.setPaymentmethod(Paymentmethod paymentmethod) {
        this.paymentmethod = paymentmethod;
        return this;
    }
    
    /**
     * Gets specificcontact value
     * 
     * @return Specificcontact
     */
    public Specificcontact Entityin.getSpecificcontact() {
        return this.specificcontact;
    }
    
    /**
     * Sets specificcontact value
     * 
     * @param specificcontact
     * @return Entityin
     */
    public Entityin Entityin.setSpecificcontact(Specificcontact specificcontact) {
        this.specificcontact = specificcontact;
        return this;
    }
    
    /**
     * Gets rates value
     * 
     * @return Rates
     */
    public Rates Entityin.getRates() {
        return this.rates;
    }
    
    /**
     * Sets rates value
     * 
     * @param rates
     * @return Entityin
     */
    public Entityin Entityin.setRates(Rates rates) {
        this.rates = rates;
        return this;
    }
    
    /**
     * Gets fees value
     * 
     * @return Fees
     */
    public Fees Entityin.getFees() {
        return this.fees;
    }
    
    /**
     * Sets fees value
     * 
     * @param fees
     * @return Entityin
     */
    public Entityin Entityin.setFees(Fees fees) {
        this.fees = fees;
        return this;
    }
    
    /**
     * Gets membership value
     * 
     * @return Membership
     */
    public Membership Entityin.getMembership() {
        return this.membership;
    }
    
    /**
     * Sets membership value
     * 
     * @param membership
     * @return Entityin
     */
    public Entityin Entityin.setMembership(Membership membership) {
        this.membership = membership;
        return this;
    }
    
    /**
     * Gets image value
     * 
     * @return Image
     */
    public Image Entityin.getImage() {
        return this.image;
    }
    
    /**
     * Sets image value
     * 
     * @param image
     * @return Entityin
     */
    public Entityin Entityin.setImage(Image image) {
        this.image = image;
        return this;
    }
    
    /**
     * Gets offerid value
     * 
     * @return String
     */
    public String Entityin.getOfferid() {
        return this.offerid;
    }
    
    /**
     * Sets offerid value
     * 
     * @param offerid
     * @return Entityin
     */
    public Entityin Entityin.setOfferid(String offerid) {
        this.offerid = offerid;
        return this;
    }
    
    /**
     * Gets contactability value
     * 
     * @return Contactability
     */
    public Contactability Entityin.getContactability() {
        return this.contactability;
    }
    
    /**
     * Sets contactability value
     * 
     * @param contactability
     * @return Entityin
     */
    public Entityin Entityin.setContactability(Contactability contactability) {
        this.contactability = contactability;
        return this;
    }
    
}
