package com.bbva.pzic.cards.dao.model.productofferdetail;

import java.math.BigDecimal;
import java.util.List;

public class CardHolderSimulation {

    private String titularTarjeta;
    private String descTitularidad;
    private String idClasificacion;
    private String nombreClasificacion;
    private String idImagen;
    private String nombreImagen;
    private String urlImagen;
    private String bin;
    private String nombreMarca;
    private String idSubproducto;
    private String nombreSubproducto;
    private String descSubproducto;
    private BigDecimal limiteMinLinea;
    private String monedaLimiteMin;
    private BigDecimal limiteMaxLinea;
    private String monedaLimiteMax;
    private BigDecimal lineaSugerida;
    private String monedaLineaSug;
    private String idComisionMemb;
    private String nombreMembresia;
    private String idModAplicMemb;
    private String modAplicacionMemb;
    private BigDecimal montoMembr;
    private String monedaMembr;
    private String idComisionEstadoCuenta;
    private String nombreMembresiaEstadoCuenta;
    private String idModAplicMembEstadoCuenta;
    private String modAplicacionMembEstadoCuenta;
    private BigDecimal montoMembrEstadoCuenta;
    private String monedaMembrEstadoCuenta;
    private List<Tasa> tasa;
    private String tipoSeguro;
    private BigDecimal montoSeguro;
    private String divisaMontoSeguro;
    private List<Beneficio> beneficio;
    private List<Condicion> condicion;
    private String idProgBenef;
    private String nombreProgBenef;
    private String idPeriodicidad;
    private String nombrePeriodicidad;
    private List<DiaPago> diapago; // diaPago1, diaPago2
    private String idTipoDeuda;
    private String nombreTipoDeuda;

    public String getTitularTarjeta() {
        return titularTarjeta;
    }

    public void setTitularTarjeta(String titularTarjeta) {
        this.titularTarjeta = titularTarjeta;
    }

    public String getDescTitularidad() {
        return descTitularidad;
    }

    public void setDescTitularidad(String descTitularidad) {
        this.descTitularidad = descTitularidad;
    }

    public String getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(String idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getNombreClasificacion() {
        return nombreClasificacion;
    }

    public void setNombreClasificacion(String nombreClasificacion) {
        this.nombreClasificacion = nombreClasificacion;
    }

    public String getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(String idImagen) {
        this.idImagen = idImagen;
    }

    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getNombreMarca() {
        return nombreMarca;
    }

    public void setNombreMarca(String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }

    public String getIdSubproducto() {
        return idSubproducto;
    }

    public void setIdSubproducto(String idSubproducto) {
        this.idSubproducto = idSubproducto;
    }

    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    public String getDescSubproducto() {
        return descSubproducto;
    }

    public void setDescSubproducto(String descSubproducto) {
        this.descSubproducto = descSubproducto;
    }

    public BigDecimal getLimiteMinLinea() {
        return limiteMinLinea;
    }

    public void setLimiteMinLinea(BigDecimal limiteMinLinea) {
        this.limiteMinLinea = limiteMinLinea;
    }

    public String getMonedaLimiteMin() {
        return monedaLimiteMin;
    }

    public void setMonedaLimiteMin(String monedaLimiteMin) {
        this.monedaLimiteMin = monedaLimiteMin;
    }

    public BigDecimal getLimiteMaxLinea() {
        return limiteMaxLinea;
    }

    public void setLimiteMaxLinea(BigDecimal limiteMaxLinea) {
        this.limiteMaxLinea = limiteMaxLinea;
    }

    public String getMonedaLimiteMax() {
        return monedaLimiteMax;
    }

    public void setMonedaLimiteMax(String monedaLimiteMax) {
        this.monedaLimiteMax = monedaLimiteMax;
    }

    public BigDecimal getLineaSugerida() {
        return lineaSugerida;
    }

    public void setLineaSugerida(BigDecimal lineaSugerida) {
        this.lineaSugerida = lineaSugerida;
    }

    public String getMonedaLineaSug() {
        return monedaLineaSug;
    }

    public void setMonedaLineaSug(String monedaLineaSug) {
        this.monedaLineaSug = monedaLineaSug;
    }

    public String getIdComisionMemb() {
        return idComisionMemb;
    }

    public void setIdComisionMemb(String idComisionMemb) {
        this.idComisionMemb = idComisionMemb;
    }

    public String getNombreMembresia() {
        return nombreMembresia;
    }

    public void setNombreMembresia(String nombreMembresia) {
        this.nombreMembresia = nombreMembresia;
    }

    public String getIdModAplicMemb() {
        return idModAplicMemb;
    }

    public void setIdModAplicMemb(String idModAplicMemb) {
        this.idModAplicMemb = idModAplicMemb;
    }

    public String getModAplicacionMemb() {
        return modAplicacionMemb;
    }

    public void setModAplicacionMemb(String modAplicacionMemb) {
        this.modAplicacionMemb = modAplicacionMemb;
    }

    public BigDecimal getMontoMembr() {
        return montoMembr;
    }

    public void setMontoMembr(BigDecimal montoMembr) {
        this.montoMembr = montoMembr;
    }

    public String getMonedaMembr() {
        return monedaMembr;
    }

    public void setMonedaMembr(String monedaMembr) {
        this.monedaMembr = monedaMembr;
    }

    public String getIdComisionEstadoCuenta() {
        return idComisionEstadoCuenta;
    }

    public void setIdComisionEstadoCuenta(String idComisionEstadoCuenta) {
        this.idComisionEstadoCuenta = idComisionEstadoCuenta;
    }

    public String getNombreMembresiaEstadoCuenta() {
        return nombreMembresiaEstadoCuenta;
    }

    public void setNombreMembresiaEstadoCuenta(String nombreMembresiaEstadoCuenta) {
        this.nombreMembresiaEstadoCuenta = nombreMembresiaEstadoCuenta;
    }

    public String getIdModAplicMembEstadoCuenta() {
        return idModAplicMembEstadoCuenta;
    }

    public void setIdModAplicMembEstadoCuenta(String idModAplicMembEstadoCuenta) {
        this.idModAplicMembEstadoCuenta = idModAplicMembEstadoCuenta;
    }

    public String getModAplicacionMembEstadoCuenta() {
        return modAplicacionMembEstadoCuenta;
    }

    public void setModAplicacionMembEstadoCuenta(String modAplicacionMembEstadoCuenta) {
        this.modAplicacionMembEstadoCuenta = modAplicacionMembEstadoCuenta;
    }

    public BigDecimal getMontoMembrEstadoCuenta() {
        return montoMembrEstadoCuenta;
    }

    public void setMontoMembrEstadoCuenta(BigDecimal montoMembrEstadoCuenta) {
        this.montoMembrEstadoCuenta = montoMembrEstadoCuenta;
    }

    public String getMonedaMembrEstadoCuenta() {
        return monedaMembrEstadoCuenta;
    }

    public void setMonedaMembrEstadoCuenta(String monedaMembrEstadoCuenta) {
        this.monedaMembrEstadoCuenta = monedaMembrEstadoCuenta;
    }

    public List<Tasa> getTasa() {
        return tasa;
    }

    public void setTasa(List<Tasa> tasa) {
        this.tasa = tasa;
    }

    public String getTipoSeguro() {
        return tipoSeguro;
    }

    public void setTipoSeguro(String tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

    public BigDecimal getMontoSeguro() {
        return montoSeguro;
    }

    public void setMontoSeguro(BigDecimal montoSeguro) {
        this.montoSeguro = montoSeguro;
    }

    public String getDivisaMontoSeguro() {
        return divisaMontoSeguro;
    }

    public void setDivisaMontoSeguro(String divisaMontoSeguro) {
        this.divisaMontoSeguro = divisaMontoSeguro;
    }

    public List<Beneficio> getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(List<Beneficio> beneficio) {
        this.beneficio = beneficio;
    }

    public List<Condicion> getCondicion() {
        return condicion;
    }

    public void setCondicion(List<Condicion> condicion) {
        this.condicion = condicion;
    }

    public String getIdProgBenef() {
        return idProgBenef;
    }

    public void setIdProgBenef(String idProgBenef) {
        this.idProgBenef = idProgBenef;
    }

    public String getNombreProgBenef() {
        return nombreProgBenef;
    }

    public void setNombreProgBenef(String nombreProgBenef) {
        this.nombreProgBenef = nombreProgBenef;
    }

    public String getIdPeriodicidad() {
        return idPeriodicidad;
    }

    public void setIdPeriodicidad(String idPeriodicidad) {
        this.idPeriodicidad = idPeriodicidad;
    }

    public String getNombrePeriodicidad() {
        return nombrePeriodicidad;
    }

    public void setNombrePeriodicidad(String nombrePeriodicidad) {
        this.nombrePeriodicidad = nombrePeriodicidad;
    }

    public List<DiaPago> getDiapago() {
        return diapago;
    }

    public void setDiapago(List<DiaPago> diapago) {
        this.diapago = diapago;
    }

    public String getIdTipoDeuda() {
        return idTipoDeuda;
    }

    public void setIdTipoDeuda(String idTipoDeuda) {
        this.idTipoDeuda = idTipoDeuda;
    }

    public String getNombreTipoDeuda() {
        return nombreTipoDeuda;
    }

    public void setNombreTipoDeuda(String nombreTipoDeuda) {
        this.nombreTipoDeuda = nombreTipoDeuda;
    }
}
