package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntDelivery;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTECKB94;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTSCKB94;
import com.bbva.pzic.cards.dao.model.kb94.PeticionTransaccionKb94;
import com.bbva.pzic.cards.dao.model.kb94.RespuestaTransaccionKb94;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardDeliveryMapper;
import com.bbva.pzic.cards.facade.v1.dto.Delivery;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("txCreateCardDelivery")
public class TxCreateCardDelivery
        extends SingleOutputFormat<DTOIntDelivery, FormatoKTECKB94, Delivery, FormatoKTSCKB94> {

    @Resource(name = "txCreateCardDeliveryMapper")
    private ITxCreateCardDeliveryMapper mapper;

    @Autowired
    public TxCreateCardDelivery(@Qualifier("transaccionKb94") InvocadorTransaccion<PeticionTransaccionKb94, RespuestaTransaccionKb94> transaction) {
        super(transaction, PeticionTransaccionKb94::new, Delivery::new, FormatoKTSCKB94.class);
    }

    @Override
    protected FormatoKTECKB94 mapInput(DTOIntDelivery dtoIntDelivery) {
        return mapper.mapIn(dtoIntDelivery);
    }

    @Override
    protected Delivery mapFirstOutputFormat(FormatoKTSCKB94 formatoKTSCKB94, DTOIntDelivery dtoIntDelivery, Delivery delivery) {
        return mapper.mapOut(formatoKTSCKB94);
    }
}
