package com.bbva.pzic.cards.dao.model.mpb4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPB4</code>
 *
 * @see PeticionTransaccionMpb4
 * @see RespuestaTransaccionMpb4
 */
@Component
public class TransaccionMpb4 implements InvocadorTransaccion<PeticionTransaccionMpb4,RespuestaTransaccionMpb4> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpb4 invocar(PeticionTransaccionMpb4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb4.class, RespuestaTransaccionMpb4.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpb4 invocarCache(PeticionTransaccionMpb4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb4.class, RespuestaTransaccionMpb4.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
