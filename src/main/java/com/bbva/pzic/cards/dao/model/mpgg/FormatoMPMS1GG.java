package com.bbva.pzic.cards.dao.model.mpgg;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Formato de datos <code>MPMS1GG</code> de la transacci&oacute;n <code>MPGG</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1GG")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1GG {

	/**
	 * <p>Campo <code>IDENTIF</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDENTIF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String identif;

	/**
	 * <p>Campo <code>DSENTIF</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DSENTIF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dsentif;

	/**
	 * <p>Campo <code>IDFINAN</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDFINAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idfinan;

	/**
	 * <p>Campo <code>DSFINAN</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "DSFINAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dsfinan;

	/**
	 * <p>Campo <code>FECTRAP</code>, &iacute;ndice: <code>5</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 5, nombre = "FECTRAP", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fectrap;

	/**
	 * <p>Campo <code>TOTCUO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 6, nombre = "TOTCUO", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer totcuo;

	/**
	 * <p>Campo <code>PENCUO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "PENCUO", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer pencuo;

	/**
	 * <p>Campo <code>FRECUO</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "FRECUO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String frecuo;

	/**
	 * <p>Campo <code>IMPORI</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "IMPORI", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal impori;

	/**
	 * <p>Campo <code>MONEDA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "MONEDA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moneda;

	/**
	 * <p>Campo <code>IMPTOT</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 11, nombre = "IMPTOT", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal imptot;

	/**
	 * <p>Campo <code>MONTRES</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 12, nombre = "MONTRES", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal montres;

	/**
	 * <p>Campo <code>MONPAG</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "MONPAG", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal monpag;

	/**
	 * <p>Campo <code>PORCPAG</code>, &iacute;ndice: <code>14</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 14, nombre = "PORCPAG", tipo = TipoCampo.DECIMAL, longitudMinima = 9, longitudMaxima = 9, decimales = 4)
	private BigDecimal porcpag;

	/**
	 * <p>Campo <code>FECOPEO</code>, &iacute;ndice: <code>15</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 15, nombre = "FECOPEO", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecopeo;

	/**
	 * <p>Campo <code>FECCUO1</code>, &iacute;ndice: <code>16</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 16, nombre = "FECCUO1", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date feccuo1;

	/**
	 * <p>Campo <code>IMPCUO1</code>, &iacute;ndice: <code>17</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 17, nombre = "IMPCUO1", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal impcuo1;

}