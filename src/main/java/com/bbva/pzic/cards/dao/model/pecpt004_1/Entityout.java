package com.bbva.pzic.cards.dao.model.pecpt004_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityOut</code>, utilizado por la clase <code>RespuestaTransaccionPecpt004_1</code></p>
 * 
 * @see RespuestaTransaccionPecpt004_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityout {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>term</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "term", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4000, signo = true, obligatorio = true)
	private String term;
	
	/**
	 * <p>Campo <code>addresses</code>, &iacute;ndice: <code>3</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 3, nombre = "addresses", tipo = TipoCampo.LIST)
	private List<Addresses> addresses;
	
	/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "status", tipo = TipoCampo.DTO)
	private Status status;
	
	/**
	 * <p>Campo <code>participant</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "participant", tipo = TipoCampo.DTO)
	private Participant participant;
	
	/**
	 * <p>Campo <code>shippingCompany</code>, &iacute;ndice: <code>6</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 6, nombre = "shippingCompany", tipo = TipoCampo.DTO)
	private Shippingcompany shippingcompany;
	
	/**
	 * <p>Campo <code>externalCode</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "externalCode", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
	private String externalcode;
	
}