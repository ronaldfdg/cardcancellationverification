package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMENDV;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMS1DV;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;

import java.util.List;

public interface ITxGetCardSecurityDataV1Mapper {

    FormatoMPMENDV mapIn(InputGetCardSecurityData input);

    List<SecurityData> mapOut(FormatoMPMS1DV formatoMPMS1DV, List<SecurityData> dtoOut);
}
