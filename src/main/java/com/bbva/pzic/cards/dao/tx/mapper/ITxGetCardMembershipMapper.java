package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PF;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PS;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
public interface ITxGetCardMembershipMapper {

    FormatoMPM0PF mapInput(DTOIntSearchCriteria dtoIntSearchCriteria);

    DTOIntMembershipServiceResponse mapOutput(FormatoMPM0PS formatOutput, DTOIntSearchCriteria dtoIntSearchCriteria);

}
