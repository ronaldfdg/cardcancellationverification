package com.bbva.pzic.cards.dao.model.mppf.mock;

import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PF;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PS;
import com.bbva.pzic.cards.dao.model.mppf.PeticionTransaccionMppf;
import com.bbva.pzic.cards.dao.model.mppf.RespuestaTransaccionMppf;

/**
 * Invocador de la transacci&oacute;n <code>MPPF</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mppf.PeticionTransaccionMppf
 * @see com.bbva.pzic.cards.dao.model.mppf.RespuestaTransaccionMppf
 */
@Component("transaccionMppf")
public class TransaccionMppfMock implements InvocadorTransaccion<PeticionTransaccionMppf, RespuestaTransaccionMppf> {

    public static final String WITH_DATA_CARD_ID ="01234567890123456";
    public static final String WITH_DATA_ID_PREMI ="01";
    public static final String WITH_DATA_ID_PPAS="02";
    public static final String WITH_NO_DATA="0000000000000000";
    public static final String WITH_NO_DATA_PPAS="1111111111111111";
    public static final String WITH_ERROR="XXXXXXXXXXXXXXXX";

    @Override
    public RespuestaTransaccionMppf invocar (PeticionTransaccionMppf transaccion) {
        RespuestaTransaccionMppf response = new RespuestaTransaccionMppf();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoMPM0PF formatInput = transaccion.getCuerpo().getParte(FormatoMPM0PF.class);
        if(WITH_DATA_CARD_ID.equals(formatInput.getNtarjlf())&&
        		WITH_DATA_ID_PREMI.equals(formatInput.getIdpremi()) ){
        	FormatoMPM0PS formatoOutput = new FormatoMPM0PS();
            formatoOutput.setIdpremi("01");
            formatoOutput.setDescpre("LIFEMILE");
            formatoOutput.setNpasfre("00902638052");
            formatoOutput.setFecalta(new Date(
    				new GregorianCalendar(2018, 3, 20).getTimeInMillis()));
            formatoOutput.setFecbaja(new Date(
    				new GregorianCalendar(2018, 3, 20).getTimeInMillis()));
            formatoOutput.setIndlife("S");
            formatoOutput.setIndmigr("N");
            formatoOutput.setFecven(new Date(new GregorianCalendar(2022,3,20).getTimeInMillis()));

            CopySalida copySalida = new CopySalida();
            copySalida.setCopy(formatoOutput);
            response.getCuerpo().getPartes().add(copySalida);
        }
        if(WITH_DATA_CARD_ID.equals(formatInput.getNtarjlf())&&
                WITH_DATA_ID_PPAS.equals(formatInput.getIdpremi())){
            FormatoMPM0PS formatoMPM0PS = new FormatoMPM0PS();
            formatoMPM0PS.setIdpremi("2");
            formatoMPM0PS.setDescpre("PRIORITY PASS");
            formatoMPM0PS.setNpasfre("1234567890123456789");
            formatoMPM0PS.setFecalta(new Date(new GregorianCalendar(2018,3,10).getTimeInMillis()));
            formatoMPM0PS.setFecbaja(new Date(new GregorianCalendar(2026,3,10).getTimeInMillis()));
            formatoMPM0PS.setIndlife("");
            formatoMPM0PS.setIndmigr("");
            formatoMPM0PS.setFecven(new Date(new GregorianCalendar(2022,3,10).getTimeInMillis()));

            CopySalida copySalida1 = new CopySalida();
            copySalida1.setCopy(formatoMPM0PS);
            response.getCuerpo().getPartes().add(copySalida1);
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMppf invocarCache(PeticionTransaccionMppf transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
