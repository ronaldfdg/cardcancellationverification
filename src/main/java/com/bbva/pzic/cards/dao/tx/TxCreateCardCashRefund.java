package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCashRefund;
import com.bbva.pzic.cards.business.dto.DTOIntCashRefund;
import com.bbva.pzic.cards.dao.model.mpdc.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardCashRefundMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("txCreateCardCashRefund")
public class TxCreateCardCashRefund
        extends DoubleOutputFormat<DTOInputCreateCashRefund, FormatoMCEMDC0, DTOIntCashRefund, FormatoMCRMDC0, FormatoMCRMDC1> {

    @Resource(name = "txCreateCardCashRefundMapper")
    private ITxCreateCardCashRefundMapper mapper;

    @Autowired
    public TxCreateCardCashRefund(@Qualifier("transaccionMpdc") InvocadorTransaccion<PeticionTransaccionMpdc, RespuestaTransaccionMpdc> transaction) {
        super(transaction, PeticionTransaccionMpdc::new, DTOIntCashRefund::new, FormatoMCRMDC0.class, FormatoMCRMDC1.class);
    }


    @Override
    protected FormatoMCEMDC0 mapInput(DTOInputCreateCashRefund dtoInputCreateCashRefund) {
        return mapper.mapInput(dtoInputCreateCashRefund);
    }

    @Override
    protected DTOIntCashRefund mapFirstOutputFormat(FormatoMCRMDC0 formatoMCRMDC0, DTOInputCreateCashRefund dtoInputCreateCashRefund, DTOIntCashRefund dtoOut) {
        return mapper.mapOutput(formatoMCRMDC0, dtoOut);
    }

    @Override
    protected DTOIntCashRefund mapSecondOutputFormat(FormatoMCRMDC1 formatoMCRMDC1, DTOInputCreateCashRefund dtoInputCreateCashRefund, DTOIntCashRefund dtoOut) {
        return mapper.mapOutput(formatoMCRMDC1, dtoOut);
    }
}
