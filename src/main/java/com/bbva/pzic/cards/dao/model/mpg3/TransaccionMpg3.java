package com.bbva.pzic.cards.dao.model.mpg3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPG3</code>
 *
 * @see PeticionTransaccionMpg3
 * @see RespuestaTransaccionMpg3
 */
@Component("transaccionMpg3")
public class TransaccionMpg3 implements InvocadorTransaccion<PeticionTransaccionMpg3,RespuestaTransaccionMpg3> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpg3 invocar(PeticionTransaccionMpg3 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpg3.class, RespuestaTransaccionMpg3.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpg3 invocarCache(PeticionTransaccionMpg3 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpg3.class, RespuestaTransaccionMpg3.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
