package com.bbva.pzic.cards.dao.model.mpq1.mock;

import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPNCCS1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
public final class FormatsMpq1Mock {

    private static final FormatsMpq1Mock INSTANCE = new FormatsMpq1Mock();
    private ObjectMapperHelper objectMapper;

    private FormatsMpq1Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatsMpq1Mock getInstance() {
        return INSTANCE;
    }

    public List<FormatoMPNCCS1> getFormatoMPNCCS1() throws IOException {
        return objectMapper
                .readValue(
                        Thread.currentThread()
                                .getContextClassLoader()
                                .getResourceAsStream(
                                        "com/bbva/pzic/cards/dao/model/mpq1/mock/formatoMPNCCS1.json"),
                        new TypeReference<List<FormatoMPNCCS1>>() {
                        });
    }
}