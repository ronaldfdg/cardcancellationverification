package com.bbva.pzic.cards.dao.rest.mapper;

import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.Documents;

/**
 * Created on 27/03/2018.
 *
 * @author Entelgy
 */
public interface IRestDigitizeDocumentFileMapper {

    Document mapOut(Documents output);
}