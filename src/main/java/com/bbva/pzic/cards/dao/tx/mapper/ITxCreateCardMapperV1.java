package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPME0W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS1W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS2W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS3W2;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
public interface ITxCreateCardMapperV1 {

    FormatoMPME0W2 mapIn(DTOIntCard dtoIn);

    CardPost mapOut1(FormatoMPMS1W2 formatOutput);

    CardPost mapOut2(FormatoMPMS2W2 formatOutput, CardPost dtoOut);

    CardPost mapOut3(FormatoMPMS3W2 formatOutput, CardPost dtoOut);
}
