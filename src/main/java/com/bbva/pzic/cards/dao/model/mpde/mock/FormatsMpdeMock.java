package com.bbva.pzic.cards.dao.model.mpde.mock;

import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS1DE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS2DE;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

public final class FormatsMpdeMock {

    private static final FormatsMpdeMock INSTANCE = new FormatsMpdeMock();
    private ObjectMapperHelper objectMapper;

    private FormatsMpdeMock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatsMpdeMock getInstance() {
        return INSTANCE;
    }

    public List<FormatoMPMS1DE> getFormatoMPMS1DE() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpde/mock/formatosMPMS1DE.json"),
                new TypeReference<List<FormatoMPMS1DE>>() {
                });
    }

    public List<FormatoMPMS2DE> getFormatoMPMS2DE() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpde/mock/formatosMPMS2DE.json"),
                new TypeReference<List<FormatoMPMS2DE>>() {
                });
    }

    public FormatoMPMS1DE getFormatoMPMS1DEEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpde/mock/formatoMPMS1DE-EMPTY.json"),
                FormatoMPMS1DE.class);
    }
}
