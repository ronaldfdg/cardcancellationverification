package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.model.mpw1.PeticionTransaccionMpw1;
import com.bbva.pzic.cards.dao.model.mpw1.RespuestaTransaccionMpw1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapperV0;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
@Component("txCreateCardV0")
public class TxCreateCardV0
        extends SingleOutputFormat<InputCreateCard, FormatoMPME0W1, Card, FormatoMPMS1W1> {

    @Resource(name = "txCreateCardMapperV0")
    private ITxCreateCardMapperV0 mapper;

    @Autowired
    public TxCreateCardV0(@Qualifier("transaccionMpw1") InvocadorTransaccion<PeticionTransaccionMpw1, RespuestaTransaccionMpw1> transaction) {
        super(transaction, PeticionTransaccionMpw1::new, Card::new, FormatoMPMS1W1.class);
    }

    @Override
    protected FormatoMPME0W1 mapInput(InputCreateCard inputCreateCard) {
        return mapper.mapIn(inputCreateCard);
    }

    @Override
    protected Card mapFirstOutputFormat(FormatoMPMS1W1 formatoMPMS1W1, InputCreateCard inputCreateCard, Card card) {
        return mapper.mapOut(formatoMPMS1W1);
    }
}
