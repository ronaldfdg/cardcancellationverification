package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMENG7;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMS1G7;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardLimitV0Mapper;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
@Mapper("txModifyCardLimitV0Mapper")
public class TxModifyCardLimitV0Mapper implements ITxModifyCardLimitV0Mapper {

    @Override
    public FormatoMPMENG7 mapIn(final InputModifyCardLimit dtoIn) {
        FormatoMPMENG7 formatoMPMENG7 = new FormatoMPMENG7();
        formatoMPMENG7.setIdetarj(dtoIn.getCardId());
        formatoMPMENG7.setTiplim(dtoIn.getLimitId());
        formatoMPMENG7.setDivisa(dtoIn.getAmountLimitCurrency());
        formatoMPMENG7.setImporte(dtoIn.getAmountLimitAmount());
        formatoMPMENG7.setCodofer(dtoIn.getOfferId());
        return formatoMPMENG7;
    }

    @Override
    public Limit mapOut(final FormatoMPMS1G7 formatOutput) {
        if (formatOutput == null || formatOutput.getFecoper() == null || formatOutput.getHoroper() == null) {
            return null;
        }
        Limit limit = new Limit();
        try {
            if (formatOutput.getFecoper() != null && !StringUtils.isEmpty(formatOutput.getHoroper())) {
                limit.setOperationDate(DateUtils.toDateTime(formatOutput.getFecoper(), formatOutput.getHoroper()));
            }
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
        return limit;
    }
}
