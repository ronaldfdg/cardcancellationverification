package com.bbva.pzic.cards.dao.model.mpcv;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMENCV</code> de la transacci&oacute;n <code>MPCV</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMENCV")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMENCV {

	/**
	 * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String numtarj;

	/**
	 * <p>Campo <code>KEYPB01</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 2, nombre = "KEYPB01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb01;

	/**
	 * <p>Campo <code>KEYPB02</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 3, nombre = "KEYPB02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb02;

	/**
	 * <p>Campo <code>KEYPB03</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 4, nombre = "KEYPB03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb03;

	/**
	 * <p>Campo <code>KEYPB04</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 5, nombre = "KEYPB04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb04;

	/**
	 * <p>Campo <code>KEYPB05</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 6, nombre = "KEYPB05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb05;

	/**
	 * <p>Campo <code>KEYPB06</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 7, nombre = "KEYPB06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb06;

	/**
	 * <p>Campo <code>KEYPB07</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 8, nombre = "KEYPB07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb07;

	/**
	 * <p>Campo <code>KEYPB08</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 9, nombre = "KEYPB08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb08;

	/**
	 * <p>Campo <code>KEYPB09</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 10, nombre = "KEYPB09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb09;

	/**
	 * <p>Campo <code>KEYPB10</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 11, nombre = "KEYPB10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb10;

	/**
	 * <p>Campo <code>KEYPB11</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 12, nombre = "KEYPB11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb11;

	/**
	 * <p>Campo <code>KEYPB12</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 13, nombre = "KEYPB12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb12;

	/**
	 * <p>Campo <code>KEYPB13</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 14, nombre = "KEYPB13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb13;

	/**
	 * <p>Campo <code>KEYPB14</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 15, nombre = "KEYPB14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String keypb14;

}