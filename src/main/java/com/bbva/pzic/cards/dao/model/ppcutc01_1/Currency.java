package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>currency</code>, utilizado por la clase <code>Currencies</code></p>
 * 
 * @see Currencies
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Currency {
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String currency;
	
	/**
	 * <p>Campo <code>isMajor</code>, &iacute;ndice: <code>2</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 2, nombre = "isMajor", tipo = TipoCampo.BOOLEAN, signo = true, obligatorio = true)
	private Boolean ismajor;
	
}