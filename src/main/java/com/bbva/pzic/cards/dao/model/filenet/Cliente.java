package com.bbva.pzic.cards.dao.model.filenet;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.util.List;

/**
 * Created on 05/10/2018.
 *
 * @author Entelgy
 */
public class Cliente {

    private String codCliente;
    @DatoAuditable
    private String codigoCentral;
    private List<Document> documentos;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodigoCentral() {
        return codigoCentral;
    }

    public void setCodigoCentral(String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }

    public List<Document> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Document> documentos) {
        this.documentos = documentos;
    }
}
