package com.bbva.pzic.cards.dao.model.awsimages;

import java.util.List;

/**
 * Created on 30/07/2019.
 *
 * @author Entelgy
 */
public class AWSImagesResponse {

    private List<List<ModelImage>> data;

    public List<List<ModelImage>> getData() {
        return data;
    }

    public void setData(List<List<ModelImage>> data) {
        this.data = data;
    }
}
