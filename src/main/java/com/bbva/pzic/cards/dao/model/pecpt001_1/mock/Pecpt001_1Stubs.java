package com.bbva.pzic.cards.dao.model.pecpt001_1.mock;

import com.bbva.pzic.cards.dao.model.pecpt001_1.RespuestaTransaccionPecpt001_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 12/02/2020.
 *
 * @author Entelgy
 */
public final class Pecpt001_1Stubs {

    private static final Pecpt001_1Stubs INSTANCE = new Pecpt001_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Pecpt001_1Stubs() {
    }

    public static Pecpt001_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPecpt001_1 getShipments() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/pecpt001_1/mock/respuestaTransaccionPecpt001_1.json"), RespuestaTransaccionPecpt001_1.class);
    }
}
