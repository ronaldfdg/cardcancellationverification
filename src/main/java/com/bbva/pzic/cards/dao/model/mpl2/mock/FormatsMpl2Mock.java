package com.bbva.pzic.cards.dao.model.mpl2.mock;

import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS1L2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS2L2;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 20/07/2017.
 *
 * @author Entelgy
 */
public final class FormatsMpl2Mock {

    private static final FormatsMpl2Mock INSTANCE = new FormatsMpl2Mock();
    private ObjectMapperHelper objectMapper;

    private FormatsMpl2Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatsMpl2Mock getInstance() {
        return INSTANCE;
    }

    public List<FormatoMPMS1L2> buildFormatosMPMS1L2() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpl2/mock/FormatoMPMS1L2.json"), new TypeReference<List<FormatoMPMS1L2>>() {
        });
    }

    public FormatoMPMS2L2 buildFormatoMPMS2L2() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpl2/mock/FormatoMPMS2L2.json"), FormatoMPMS2L2.class);
    }
}
