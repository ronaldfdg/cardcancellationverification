package com.bbva.pzic.cards.dao.model.mpg7;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPG7</code>
 *
 * @see PeticionTransaccionMpg7
 * @see RespuestaTransaccionMpg7
 */
@Component("transaccionMpg7")
public class TransaccionMpg7 implements InvocadorTransaccion<PeticionTransaccionMpg7, RespuestaTransaccionMpg7> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionMpg7 invocar(PeticionTransaccionMpg7 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpg7.class, RespuestaTransaccionMpg7.class, transaccion);
    }

    @Override
    public RespuestaTransaccionMpg7 invocarCache(PeticionTransaccionMpg7 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpg7.class, RespuestaTransaccionMpg7.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
