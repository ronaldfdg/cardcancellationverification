package com.bbva.pzic.cards.dao.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.ICardsDAO;
import com.bbva.pzic.cards.dao.tx.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Entelgy
 */
@Repository
public class CardsDAO implements ICardsDAO {

    private static final Log LOG = LogFactory.getLog(CardsDAO.class);

    @Autowired
    private TxCreateCard txCreateCard;

    @Autowired
    private TxModifyCardBlock txModifyCardBlock;

    @Autowired
    private TxModifyCardActivation txModifyCardActivation;

    @Autowired
    private TxListCards txListCards;

    @Autowired
    private TxListCardTransactions txListCardTransactions;

    @Autowired
    private TxCreateCardRelatedContract txCreateCardRelatedContract;

    @Autowired
    private TxGetCardSecurityData txGetCardSecurityData;

    @Autowired
    private TxCreateCardInstallmentsPlan txCreateCardInstallmentsPlan;

    @Autowired
    private TxSimulateCardInstallmentsPlan txSimulateCardInstallmentsPlan;

    @Autowired
    private TxModifyCard txModifyCard;

    @Autowired
    private TxModifyPartialCardBlock txModifyPartialCardBlock;

    @Autowired
    private TxModifyCardPin txModifyCardPin;

    @Autowired
    private TxListCardActivations txListCardActivations;

    @Autowired
    private TxModifyCardActivations txModifyCardActivations;

    @Autowired
    private TxCreateCardsCardProposal txCreateCardsCardProposal;

    @Override
    public CardData createCard(final DTOIntCard input) {
        LOG.info("... called method CardsDAO.createCard ...");
        return txCreateCard.perform(input);
    }

    @Override
    public BlockData modifyCardBlock(final DTOIntBlock block) {
        LOG.info("... called method CardsDAO.modifyCardBlock ...");
        return txModifyCardBlock.perform(block);
    }

    @Override
    public void modifyCardActivation(final DTOIntActivation activation) {
        LOG.info("... called method CardsDAO.modifyCardActivation ...");
        txModifyCardActivation.perform(activation);
    }

    @Override
    public DTOOutListCards listCards(final DTOIntListCards dtoIn) {
        LOG.info("... called method CardsDAO.listCards ...");
        return txListCards.perform(dtoIn);
    }

    @Override
    public TransactionsData listCardTransactions(final DTOInputListCardTransactions queryFilter) {
        LOG.info("... called method CardsDAO.listCardTransactions ...");
        return txListCardTransactions.perform(queryFilter);
    }

    @Override
    public DTOOutCreateCardRelatedContract createCardRelatedContract(final DTOInputCreateCardRelatedContract dtoIn) {
        LOG.info("... called method CardsDAO.createRelatedContract ...");
        return txCreateCardRelatedContract.perform(dtoIn);
    }

    @Override
    public SecurityData getCardSecurityData(final InputGetCardSecurityData input) {
        LOG.info("... called method CardsDAO.getCardSecurityData ...");
        return txGetCardSecurityData.perform(input);
    }

    @Override
    public InstallmentsPlanData createInstallmentsPlan(final DTOIntCardInstallmentsPlan input) {
        LOG.info("... called method CardsDAO.createInstallmentsPlan ...");
        return txCreateCardInstallmentsPlan.perform(input);
    }

    @Override
    public InstallmentsPlanSimulationData simulateInstallmentsPlan(final DTOIntCardInstallmentsPlan input) {
        LOG.info("... called method CardsDAO.simulateInstallmentsPlan ...");
        return txSimulateCardInstallmentsPlan.perform(input);
    }

    @Override
    public void modifyCard(final DTOIntCard input) {
        LOG.info("... called method CardsDAO.modifyCard ...");
        txModifyCard.perform(input);
    }

    @Override
    public void modifyPartialCardBlock(final DTOIntBlock input) {
        LOG.info("... called method CardsDAO.modifyPartialCardBlock ...");
        txModifyPartialCardBlock.perform(input);
    }

    @Override
    public void modifyCardPin(final DTOIntPin input) {
        LOG.info("... called method CardsDAO.modifyCardPin ...");
        txModifyCardPin.perform(input);
    }

    @Override
    public List<Activation> listCardActivations(final DTOIntCard input) {
        LOG.info("... called method CardsDAO.listCardActivations ...");
        return txListCardActivations.perform(input);
    }

    @Override
    public List<Activation> modifyCardActivations(final InputModifyCardActivations dtoIn) {
        LOG.info("... called method CardsDAO.modifyCardActivations ...");
        return txModifyCardActivations.perform(dtoIn);
    }

    @Override
    public CardProposal createCardsCardProposal(final InputCreateCardsCardProposal input) {
        LOG.info("... Invoking method CardsDAO.createCardsCardProposal ...");
        return txCreateCardsCardProposal.perform(input);
    }
}
