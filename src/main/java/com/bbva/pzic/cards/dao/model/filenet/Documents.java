package com.bbva.pzic.cards.dao.model.filenet;

import java.util.List;

/**
 * Created on 15/06/2018.
 *
 * @author Entelgy
 */
public class Documents {

    private List<Document> documentos;
    private List<Cliente> clientes;

    public List<Document> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Document> documentos) {
        this.documentos = documentos;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
