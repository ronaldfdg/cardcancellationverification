package com.bbva.pzic.cards.dao.model.ppcut001_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import java.util.List;

/**
 * <p>Bean fila para el campo tabular <code>fees</code>, utilizado por la clase <code>Entityin</code></p>
 * 
 * @see Entityin
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Fees {
	
	/**
	 * <p>Campo <code>itemizeFees</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "itemizeFees", tipo = TipoCampo.LIST)
	private List<Itemizefees> itemizefees;
	
}