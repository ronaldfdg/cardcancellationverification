package com.bbva.pzic.cards.dao.model.kb94.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTECKB94;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTSCKB94;
import com.bbva.pzic.cards.dao.model.kb94.PeticionTransaccionKb94;
import com.bbva.pzic.cards.dao.model.kb94.RespuestaTransaccionKb94;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>KB94</code>
 *
 * @see PeticionTransaccionKb94
 * @see RespuestaTransaccionKb94
 */
@Component("transaccionKb94")
public class TransaccionKb94Fake implements InvocadorTransaccion<PeticionTransaccionKb94, RespuestaTransaccionKb94> {

    public static final String TEST_NO_DATA = "gHiJkLmN";
    public static final String TEST_EMPTY = "OpQrStUv";

    @Override
    public RespuestaTransaccionKb94 invocar(PeticionTransaccionKb94 transaccion) {
        RespuestaTransaccionKb94 rs = new RespuestaTransaccionKb94();

        final FormatoKTECKB94 format = transaccion.getCuerpo().getParte(FormatoKTECKB94.class);

        if (TEST_NO_DATA.equalsIgnoreCase(format.getNumtar())) {
            return rs;
        }

        try {
            FormatsKb94Stubs stubs = FormatsKb94Stubs.getInstance();

            if (TEST_EMPTY.equalsIgnoreCase(format.getNumtar())) {
                rs.getCuerpo().getPartes().add(buildDataCopy(stubs.buildFormatoKTSCKB94Empty()));
            } else {
                rs.getCuerpo().getPartes().add(buildDataCopy(stubs.buildFormatoKTSCKB94()));
            }

            return rs;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionKb94 invocarCache(PeticionTransaccionKb94 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildDataCopy(final FormatoKTSCKB94 formatoKTSCKB94) throws IOException {
        CopySalida copy = new CopySalida();
        copy.setCopy(formatoKTSCKB94);
        return copy;
    }
}
