package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.BlockData;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2S;
import com.bbva.pzic.cards.dao.model.mpb2.PeticionTransaccionMpb2;
import com.bbva.pzic.cards.dao.model.mpb2.RespuestaTransaccionMpb2;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardBlockMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author Entelgy
 */
@Component("txModifyCardBlock")
public class TxModifyCardBlock
        extends SingleOutputFormat<DTOIntBlock, FormatoMPM0B2, BlockData, FormatoMPM0B2S> {

    @Autowired
    private ITxModifyCardBlockMapper txModifyCardBlockMapper;

    @Autowired
    public TxModifyCardBlock(@Qualifier("transaccionMpb2") InvocadorTransaccion<PeticionTransaccionMpb2, RespuestaTransaccionMpb2> transaction) {
        super(transaction, PeticionTransaccionMpb2::new, BlockData::new, FormatoMPM0B2S.class);
    }

    @Override
    protected FormatoMPM0B2 mapInput(DTOIntBlock dtoIntBlock) {
        return txModifyCardBlockMapper.mapInput(dtoIntBlock);
    }

    @Override
    protected BlockData mapFirstOutputFormat(FormatoMPM0B2S formatoMPM0B2S, DTOIntBlock dtoIntBlock, BlockData blockData) {
        return txModifyCardBlockMapper.mapOutput(formatoMPM0B2S, dtoIntBlock);
    }
}