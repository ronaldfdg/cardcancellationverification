package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardShipmentAddress;
import com.bbva.pzic.cards.dao.model.pecpt002_1.PeticionTransaccionPecpt002_1;
import com.bbva.pzic.cards.dao.model.pecpt002_1.RespuestaTransaccionPecpt002_1;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public interface IApxCreateCardShipmentAddressMapper {

    PeticionTransaccionPecpt002_1 mapIn(InputCreateCardShipmentAddress input);

    ShipmentAddress mapOut(RespuestaTransaccionPecpt002_1 response);
}
