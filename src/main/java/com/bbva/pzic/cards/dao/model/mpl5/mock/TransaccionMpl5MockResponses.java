package com.bbva.pzic.cards.dao.model.mpl5.mock;

import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransaccionMpl5MockResponses {

    private static ObjectMapperHelper objectMapper;

    static {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static List<CopySalida> getFormatoMPMS1L5() throws IOException {
        List<FormatoMPMS1L5> formatoMPMS1L5List = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpl5/mock/FormatoMPMS1L5.json"),
                new TypeReference<List<FormatoMPMS1L5>>() {
                });
        List<CopySalida> copySalidas = new ArrayList<>();
        for (FormatoMPMS1L5 formatoMPMS1L5 : formatoMPMS1L5List) {
            CopySalida copySalida = new CopySalida();
            copySalida.setCopy(formatoMPMS1L5);
            copySalidas.add(copySalida);
        }
        return copySalidas;
    }
}
