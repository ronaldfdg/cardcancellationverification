package com.bbva.pzic.cards.dao.model.pecpt002_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PECPT002</code>
 * 
 * @see PeticionTransaccionPecpt002_1
 * @see RespuestaTransaccionPecpt002_1
 */
@Component
public class TransaccionPecpt002_1 implements InvocadorTransaccion<PeticionTransaccionPecpt002_1,RespuestaTransaccionPecpt002_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPecpt002_1 invocar(PeticionTransaccionPecpt002_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt002_1.class, RespuestaTransaccionPecpt002_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPecpt002_1 invocarCache(PeticionTransaccionPecpt002_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt002_1.class, RespuestaTransaccionPecpt002_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}