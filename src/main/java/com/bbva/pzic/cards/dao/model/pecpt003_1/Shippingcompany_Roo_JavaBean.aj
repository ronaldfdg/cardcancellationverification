// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.pecpt003_1;

import com.bbva.pzic.cards.dao.model.pecpt003_1.Shippingcompany;

privileged aspect Shippingcompany_Roo_JavaBean {
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Shippingcompany.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Shippingcompany
     */
    public Shippingcompany Shippingcompany.setId(String id) {
        this.id = id;
        return this;
    }
    
}
