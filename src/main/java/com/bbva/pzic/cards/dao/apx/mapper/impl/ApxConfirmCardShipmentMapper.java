package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxConfirmCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt005_1.*;
import com.bbva.pzic.cards.util.mappers.Mapper;

import java.math.BigDecimal;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@Mapper
public class ApxConfirmCardShipmentMapper implements IApxConfirmCardShipmentMapper {

    @Override
    public PeticionTransaccionPecpt005_1 mapIn(final InputConfirmCardShipment input) {
        PeticionTransaccionPecpt005_1 peticion = new PeticionTransaccionPecpt005_1();
        peticion.setShipmentid(input.getShipmentId());
        peticion.setEntityin(mapInEntityIn(input));
        return peticion;
    }

    private Entityin mapInEntityIn(final InputConfirmCardShipment input) {
        Entityin entityin = new Entityin();
        entityin.setBiometricid(input.getBiometricId());
        entityin.setCurrentaddress(mapInCurrentAddress(input.getCurrentAddressLocationGeolocationLatitude(), input.getCurrentAddressLocationGeolocationLongitude()));
        entityin.setShipmentaddress(mapInShipmentAddress(input.getShipmentAddressId()));
        return entityin;
    }

    private Shipmentaddress mapInShipmentAddress(final String shipmentAddressId) {
        Shipmentaddress shipmentaddress = new Shipmentaddress();
        shipmentaddress.setId(shipmentAddressId);
        return shipmentaddress;
    }

    private Currentaddress mapInCurrentAddress(final BigDecimal latitude, final BigDecimal longitude) {
        Currentaddress currentaddress = new Currentaddress();
        currentaddress.setLocation(mapInLocation(latitude, longitude));
        return currentaddress;
    }

    private Location mapInLocation(final BigDecimal latitude, final BigDecimal longitude) {
        Location location = new Location();
        location.setGeolocation(mapInGeolocation(latitude, longitude));
        return location;
    }

    private Geolocation mapInGeolocation(final BigDecimal latitude, final BigDecimal longitude) {
        Geolocation geolocation = new Geolocation();
        geolocation.setLatitude(latitude);
        geolocation.setLongitude(longitude);
        return geolocation;
    }
}
