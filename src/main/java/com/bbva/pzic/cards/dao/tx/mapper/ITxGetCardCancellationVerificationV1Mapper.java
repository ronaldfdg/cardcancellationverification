package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardCancellationVerification;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS13G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS23G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS33G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPRM3G0;
import com.bbva.pzic.cards.facade.v1.dto.CancellationVerification;

public interface ITxGetCardCancellationVerificationV1Mapper {

    FormatoMPRM3G0 mapIn(InputGetCardCancellationVerification input);

    CancellationVerification mapOutFormatoMPMS13G(FormatoMPMS13G formatoMPMS13G, CancellationVerification cancellationVerification);

    CancellationVerification mapOutFormatoMPMS23G(FormatoMPMS23G formatoMPMS23G, CancellationVerification cancellationVerification);

    CancellationVerification mapOutFormatoMPMS33G(FormatoMPMS33G formatoMPMS33G, CancellationVerification cancellationVerification);

}
