package com.bbva.pzic.cards.dao.model.filenet;

import com.bbva.pzic.cards.business.dto.ValidationGroup;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 15/06/2018.
 *
 * @author Entelgy
 */
public class DocumentRequest {

    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @Size(min = 1, groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @Valid
    private List<Contenido> contenido;
    private String numeroContrato;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("idContrato")
    private String idContrato;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("idGrupo")
    private String idGrupo;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("listaClientes")
    private List<Cliente> listaClientes;

    public List<Contenido> getContenido() {
        return contenido;
    }

    public void setContenido(List<Contenido> contenido) {
        this.contenido = contenido;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
}
