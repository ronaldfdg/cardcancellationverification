package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.dao.model.pecpt005_1.PeticionTransaccionPecpt005_1;

public interface IApxConfirmCardShipmentMapper {

    PeticionTransaccionPecpt005_1 mapIn(InputConfirmCardShipment input);
}
