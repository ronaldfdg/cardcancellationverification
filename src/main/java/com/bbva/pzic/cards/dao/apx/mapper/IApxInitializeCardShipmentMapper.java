package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputInitializeCardShipment;
import com.bbva.pzic.cards.dao.model.pecpt003_1.PeticionTransaccionPecpt003_1;
import com.bbva.pzic.cards.dao.model.pecpt003_1.RespuestaTransaccionPecpt003_1;
import com.bbva.pzic.cards.facade.v0.dto.InitializeShipment;

public interface IApxInitializeCardShipmentMapper {

    PeticionTransaccionPecpt003_1 mapIn(InputInitializeCardShipment input);

    InitializeShipment mapOut(RespuestaTransaccionPecpt003_1 response);
}
