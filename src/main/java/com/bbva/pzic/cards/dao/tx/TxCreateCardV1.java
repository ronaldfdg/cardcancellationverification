package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpw2.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapperV1;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.ThreefoldOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Component("txCreateCardV1")
public class TxCreateCardV1
        extends ThreefoldOutputFormat<DTOIntCard, FormatoMPME0W2, CardPost, FormatoMPMS1W2, FormatoMPMS2W2, FormatoMPMS3W2> {

    @Resource(name = "txCreateCardMapperV1")
    private ITxCreateCardMapperV1 mapper;

    @Autowired
    public TxCreateCardV1(@Qualifier("transaccionMpw2") InvocadorTransaccion<PeticionTransaccionMpw2, RespuestaTransaccionMpw2> transaction) {
        super(transaction, PeticionTransaccionMpw2::new, CardPost::new, FormatoMPMS1W2.class, FormatoMPMS2W2.class, FormatoMPMS3W2.class);
    }

    @Override
    protected FormatoMPME0W2 mapInput(final DTOIntCard dtoIn) {
        return mapper.mapIn(dtoIn);
    }

    @Override
    protected CardPost mapFirstOutputFormat(final FormatoMPMS1W2 formatOutput, final DTOIntCard dtoIn, final CardPost dtoOut) {
        return mapper.mapOut1(formatOutput);
    }

    @Override
    protected CardPost mapSecondOutputFormat(final FormatoMPMS2W2 formatOutput, final DTOIntCard dtoIn, final CardPost dtoOut) {
        return mapper.mapOut2(formatOutput, dtoOut);
    }

    @Override
    protected CardPost mapThirdOutputFormat(final FormatoMPMS3W2 formatOutput, final DTOIntCard dtoIn, final CardPost dtoOut) {
        return mapper.mapOut3(formatOutput, dtoOut);
    }
}
