package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardCancellationVerification;
import com.bbva.pzic.cards.dao.model.mp3g.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardCancellationVerificationV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.CancellationVerification;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.ThreefoldOutputFormat;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TxGetCardCancellationVerificationV1
        extends ThreefoldOutputFormat<InputGetCardCancellationVerification, FormatoMPRM3G0, CancellationVerification, FormatoMPMS13G, FormatoMPMS23G, FormatoMPMS33G> {

    @Resource(name = "txGetCardCancellationVerificationV1Mapper")
    private ITxGetCardCancellationVerificationV1Mapper mapper;

    public TxGetCardCancellationVerificationV1(InvocadorTransaccion<PeticionTransaccionMp3g, RespuestaTransaccionMp3g> transaction) {
        super(transaction, PeticionTransaccionMp3g::new, CancellationVerification::new, FormatoMPMS13G.class, FormatoMPMS23G.class, FormatoMPMS33G.class);
    }

    @Override
    protected FormatoMPRM3G0 mapInput(InputGetCardCancellationVerification input) {
        return mapper.mapIn(input);
    }

    @Override
    protected CancellationVerification mapFirstOutputFormat(FormatoMPMS13G format, InputGetCardCancellationVerification input, CancellationVerification output) {
        return mapper.mapOutFormatoMPMS13G(format, output);
    }

    @Override
    protected CancellationVerification mapSecondOutputFormat(FormatoMPMS23G format, InputGetCardCancellationVerification input, CancellationVerification output) {
        return mapper.mapOutFormatoMPMS23G(format, output);
    }

    @Override
    protected CancellationVerification mapThirdOutputFormat(FormatoMPMS33G format, InputGetCardCancellationVerification input, CancellationVerification output) {
        return mapper.mapOutFormatoMPMS33G(format, output);
    }
}