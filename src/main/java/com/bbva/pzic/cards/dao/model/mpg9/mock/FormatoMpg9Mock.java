package com.bbva.pzic.cards.dao.model.mpg9.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMS1G9;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 09/11/2018.
 *
 * @author Entelgy
 */
public class FormatoMpg9Mock {

    private static final FormatoMpg9Mock INSTANCE = new FormatoMpg9Mock();
    private ObjectMapperHelper mapper;

    private FormatoMpg9Mock() {
        mapper = ObjectMapperHelper.getInstance();
    }

    public static FormatoMpg9Mock getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS1G9 getFormatoMPMS1G9() {
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "com/bbva/pzic/cards/dao/model/mpg9/mock/formatoMPMS1G9.json"),
                    FormatoMPMS1G9.class);
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    public FormatoMPMS1G9 getFormatoMPMS1G9Empty() {
        return new FormatoMPMS1G9();
    }

}
