package com.bbva.pzic.cards.dao.model.pecpt002_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>geolocation</code>, utilizado por la clase <code>Location</code></p>
 * 
 * @see Location
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Geolocation {
	
	/**
	 * <p>Campo <code>latitude</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 1, nombre = "latitude", tipo = TipoCampo.DECIMAL, longitudMaxima = 15, signo = true, obligatorio = true)
	private BigDecimal latitude;
	
	/**
	 * <p>Campo <code>longitude</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 2, nombre = "longitude", tipo = TipoCampo.DECIMAL, longitudMaxima = 15, signo = true, obligatorio = true)
	private BigDecimal longitude;
	
}