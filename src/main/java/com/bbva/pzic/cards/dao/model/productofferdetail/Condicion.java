package com.bbva.pzic.cards.dao.model.productofferdetail;

/**
 * Created on 20/02/2019.
 *
 * @author Entelgy
 */
public class Condicion {

    private String id;
    private String descripcion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
