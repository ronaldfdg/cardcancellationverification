package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMEN6I;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMS16I;
import com.bbva.pzic.cards.facade.v1.dto.SimulateTransactionRefund;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
public interface ITxSimulateCardTransactionTransactionRefund {

    FormatoMPMEN6I mapIn(InputSimulateCardTransactionTransactionRefund inputSimulateCardTransactionTransactionRefund);

    SimulateTransactionRefund mapOut(FormatoMPMS16I formatoMPMS16I);
}
