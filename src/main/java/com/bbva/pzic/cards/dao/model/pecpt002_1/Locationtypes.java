package com.bbva.pzic.cards.dao.model.pecpt002_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>locationTypes</code>, utilizado por la clase <code>Location</code></p>
 * 
 * @see Location
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Locationtypes {
	
	/**
	 * <p>Campo <code>locationType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "locationType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String locationtype;
	
}