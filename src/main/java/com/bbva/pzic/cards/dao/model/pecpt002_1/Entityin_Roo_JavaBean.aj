// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.pecpt002_1;

import com.bbva.pzic.cards.dao.model.pecpt002_1.Destination;
import com.bbva.pzic.cards.dao.model.pecpt002_1.Entityin;
import com.bbva.pzic.cards.dao.model.pecpt002_1.Location;

privileged aspect Entityin_Roo_JavaBean {
    
    /**
     * Gets addresstype value
     * 
     * @return String
     */
    public String Entityin.getAddresstype() {
        return this.addresstype;
    }
    
    /**
     * Sets addresstype value
     * 
     * @param addresstype
     * @return Entityin
     */
    public Entityin Entityin.setAddresstype(String addresstype) {
        this.addresstype = addresstype;
        return this;
    }
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Entityin.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Entityin
     */
    public Entityin Entityin.setId(String id) {
        this.id = id;
        return this;
    }
    
    /**
     * Gets location value
     * 
     * @return Location
     */
    public Location Entityin.getLocation() {
        return this.location;
    }
    
    /**
     * Sets location value
     * 
     * @param location
     * @return Entityin
     */
    public Entityin Entityin.setLocation(Location location) {
        this.location = location;
        return this;
    }
    
    /**
     * Gets destination value
     * 
     * @return Destination
     */
    public Destination Entityin.getDestination() {
        return this.destination;
    }
    
    /**
     * Sets destination value
     * 
     * @param destination
     * @return Entityin
     */
    public Entityin Entityin.setDestination(Destination destination) {
        this.destination = destination;
        return this;
    }
    
}
