package com.bbva.pzic.cards.dao.model.kb93.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTECKB93;
import com.bbva.pzic.cards.dao.model.kb93.PeticionTransaccionKb93;
import com.bbva.pzic.cards.dao.model.kb93.RespuestaTransaccionKb93;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>KB93</code>
 *
 * @see PeticionTransaccionKb93
 * @see RespuestaTransaccionKb93
 */
@Component("transaccionKb93")
public class TransaccionKb93Mock implements InvocadorTransaccion<PeticionTransaccionKb93, RespuestaTransaccionKb93> {

    public static final String TEST_EMPTY = "9999999999999999";


    @Override
    public RespuestaTransaccionKb93 invocar(PeticionTransaccionKb93 transaccion) throws ExcepcionTransaccion {
        RespuestaTransaccionKb93 response = new RespuestaTransaccionKb93();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatsKb93Mock mock = FormatsKb93Mock.getInstance();

        final FormatoKTECKB93 format = transaccion.getCuerpo().getParte(FormatoKTECKB93.class);

        if (TEST_EMPTY.equalsIgnoreCase(format.getNumtarj())) {
            return response;
        }

        try {
            CopySalida copy = new CopySalida();
            copy.setCopy(mock.getFormatoKTSKB931());
            response.getCuerpo().getPartes().add(copy);
            return response;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }
    }

    @Override
    public RespuestaTransaccionKb93 invocarCache(PeticionTransaccionKb93 transaccion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
