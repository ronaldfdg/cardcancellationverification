package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputUpdateCardProposal;
import com.bbva.pzic.cards.dao.apx.mapper.IApxUpdateCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut002_1.PeticionTransaccionPpcut002_1;
import com.bbva.pzic.cards.dao.model.ppcut002_1.RespuestaTransaccionPpcut002_1;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
@Component("apxUpdateCardProposal")
public class ApxUpdateCardProposal {

    @Resource(name = "apxUpdateCardProposalMapper")
    private IApxUpdateCardProposalMapper mapper;

    @Resource(name = "transaccionPpcut002_1")
    private transient InvocadorTransaccion<PeticionTransaccionPpcut002_1, RespuestaTransaccionPpcut002_1> transaccion;

    public Proposal invoke(final InputUpdateCardProposal input) {
        PeticionTransaccionPpcut002_1 request = mapper.mapIn(input);
        RespuestaTransaccionPpcut002_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
