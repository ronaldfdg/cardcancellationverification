package com.bbva.pzic.cards.dao.model.mpg1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpg1.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 17/10/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpg1")
public class TransaccionMpg1Mock implements InvocadorTransaccion<PeticionTransaccionMpg1, RespuestaTransaccionMpg1> {

    public static final String TEST_NO_DATA = "9992";
    public static final String TEST_EMPTY = "9999";
    public static final String TEST_NO_IMAGE_BACK = "9993";

    private FormatoMPG1Mock formatoMPG1Mock = FormatoMPG1Mock.getInstance();

    @Override
    public RespuestaTransaccionMpg1 invocar(PeticionTransaccionMpg1 peticion) {
        RespuestaTransaccionMpg1 response = new RespuestaTransaccionMpg1();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENG1 format = peticion.getCuerpo().getParte(FormatoMPMENG1.class);
        try {
            if (TEST_NO_DATA.equals(format.getIdetarj())) {
                return response;

            } else if (TEST_EMPTY.equals(format.getIdetarj())) {
                response.getCuerpo().getPartes().add(buildData(formatoMPG1Mock.getFormatoMPMS1G1Emtpy()));
                response.getCuerpo().getPartes().add(buildDataFormatoMPMS4G1List(formatoMPG1Mock.getFormatoMPMS4G1Empty()));
                response.getCuerpo().getPartes().add(buildData(new FormatoMPMS8G1()));

            } else if (TEST_NO_IMAGE_BACK.equals(format.getIdetarj())) {
                FormatoMPMS1G1 formatCard = formatoMPG1Mock.getFormatoMPMS1G1();
                formatCard.setImgtar2(null);
                response.getCuerpo().getPartes().add(buildData(formatCard));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS2G1List(formatoMPG1Mock.getFormatoMPMS2G1()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS3G1List(formatoMPG1Mock.getFormatoMPMS3G1()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS4G1List(formatoMPG1Mock.getFormatoMPMS4G1()));
                response.getCuerpo().getPartes().add(buildData(formatoMPG1Mock.getFormatoMPMS5G1()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS6G1List(formatoMPG1Mock.getFormatoMPMS6G1List()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS7G1List(formatoMPG1Mock.getFormatoMPMS7G1List()));
                response.getCuerpo().getPartes().add(buildData(formatoMPG1Mock.getFormatoMPMS8G1()));

            } else {
                FormatoMPMS1G1 formatCard = formatoMPG1Mock.getFormatoMPMS1G1();
                formatCard.setImgtar1("pg=&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&type=V&width=512&height=324");
                formatCard.setImgtar2("pg=&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&back=true&type=V&width=512&height=324");

                response.getCuerpo().getPartes().add(buildData(formatCard));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS2G1List(formatoMPG1Mock.getFormatoMPMS2G1()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS3G1List(formatoMPG1Mock.getFormatoMPMS3G1()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS4G1List(formatoMPG1Mock.getFormatoMPMS4G1()));
                response.getCuerpo().getPartes().add(buildData(formatoMPG1Mock.getFormatoMPMS5G1()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS6G1List(formatoMPG1Mock.getFormatoMPMS6G1List()));
                response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS7G1List(formatoMPG1Mock.getFormatoMPMS7G1List()));
                response.getCuerpo().getPartes().add(buildData(formatoMPG1Mock.getFormatoMPMS8G1()));
            }
        } catch (IOException e) {
            throw new AssertionError(e);
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpg1 invocarCache(PeticionTransaccionMpg1 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(Object object) {
        CopySalida copy = new CopySalida();
        copy.setCopy(object);
        return copy;
    }

    private List<CopySalida> buildDataFormatoMPMS2G1List(List<FormatoMPMS2G1> formatoMPMS2G1s) {
        return formatoMPMS2G1s.stream().map(this::buildData).collect(Collectors.toList());
    }

    private List<CopySalida> buildDataFormatoMPMS3G1List(List<FormatoMPMS3G1> formatoMPMS3G1s) {
        return formatoMPMS3G1s.stream().map(this::buildData).collect(Collectors.toList());
    }

    private List<CopySalida> buildDataFormatoMPMS4G1List(List<FormatoMPMS4G1> formatoMPMS4G1s) {
        return formatoMPMS4G1s.stream().map(this::buildData).collect(Collectors.toList());
    }

    private List<CopySalida> buildDataFormatoMPMS6G1List(List<FormatoMPMS6G1> formatoMPMS6G1s) {
        return formatoMPMS6G1s.stream().map(this::buildData).collect(Collectors.toList());
    }

    private List<CopySalida> buildDataFormatoMPMS7G1List(List<FormatoMPMS7G1> formatoMPMS7G1s) {
        return formatoMPMS7G1s.stream().map(this::buildData).collect(Collectors.toList());
    }
}
