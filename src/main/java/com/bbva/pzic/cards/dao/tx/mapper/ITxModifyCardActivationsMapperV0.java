package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMENG3;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMS1G3;

import java.util.List;

/**
 * Created on 10/10/2017.
 *
 * @author Entelgy
 */
public interface ITxModifyCardActivationsMapperV0 {

    FormatoMPMENG3 mapIn(InputModifyCardActivations dtoIn);

    List<Activation> mapOut(FormatoMPMS1G3 formatoMPMS1G3);
}
