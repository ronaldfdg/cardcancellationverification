package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpb5.FormatoMPM0B5E;

/**
 * Created on 14/08/2017.
 *
 * @author Entelgy
 */
public interface ITxModifyCardMapper {

    /**
     * Crea una nueva instancia de {@link FormatoMPM0B5E} y la inicializa con el parámetro enviado.
     *
     * @param dtoIntCard input DTO
     * @return the created object.
     */
    FormatoMPM0B5E mapInput(DTOIntCard dtoIntCard);
}
