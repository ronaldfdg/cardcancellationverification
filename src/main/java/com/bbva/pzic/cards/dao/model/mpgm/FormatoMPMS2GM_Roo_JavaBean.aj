// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpgm;

import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMS2GM;

privileged aspect FormatoMPMS2GM_Roo_JavaBean {
    
    /**
     * Gets idtiend value
     * 
     * @return String
     */
    public String FormatoMPMS2GM.getIdtiend() {
        return this.idtiend;
    }
    
    /**
     * Sets idtiend value
     * 
     * @param idtiend
     * @return FormatoMPMS2GM
     */
    public FormatoMPMS2GM FormatoMPMS2GM.setIdtiend(String idtiend) {
        this.idtiend = idtiend;
        return this;
    }
    
    /**
     * Gets nomtien value
     * 
     * @return String
     */
    public String FormatoMPMS2GM.getNomtien() {
        return this.nomtien;
    }
    
    /**
     * Sets nomtien value
     * 
     * @param nomtien
     * @return FormatoMPMS2GM
     */
    public FormatoMPMS2GM FormatoMPMS2GM.setNomtien(String nomtien) {
        this.nomtien = nomtien;
        return this;
    }
    
    /**
     * Gets idcateg value
     * 
     * @return String
     */
    public String FormatoMPMS2GM.getIdcateg() {
        return this.idcateg;
    }
    
    /**
     * Sets idcateg value
     * 
     * @param idcateg
     * @return FormatoMPMS2GM
     */
    public FormatoMPMS2GM FormatoMPMS2GM.setIdcateg(String idcateg) {
        this.idcateg = idcateg;
        return this;
    }
    
    /**
     * Gets nomcat value
     * 
     * @return String
     */
    public String FormatoMPMS2GM.getNomcat() {
        return this.nomcat;
    }
    
    /**
     * Sets nomcat value
     * 
     * @param nomcat
     * @return FormatoMPMS2GM
     */
    public FormatoMPMS2GM FormatoMPMS2GM.setNomcat(String nomcat) {
        this.nomcat = nomcat;
        return this;
    }
    
    /**
     * Gets tippag value
     * 
     * @return String
     */
    public String FormatoMPMS2GM.getTippag() {
        return this.tippag;
    }
    
    /**
     * Sets tippag value
     * 
     * @param tippag
     * @return FormatoMPMS2GM
     */
    public FormatoMPMS2GM FormatoMPMS2GM.setTippag(String tippag) {
        this.tippag = tippag;
        return this;
    }
    
    /**
     * Gets despag value
     * 
     * @return String
     */
    public String FormatoMPMS2GM.getDespag() {
        return this.despag;
    }
    
    /**
     * Sets despag value
     * 
     * @param despag
     * @return FormatoMPMS2GM
     */
    public FormatoMPMS2GM FormatoMPMS2GM.setDespag(String despag) {
        this.despag = despag;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String FormatoMPMS2GM.toString() {
        return "FormatoMPMS2GM {" + 
                "idtiend='" + idtiend + '\'' + 
                ", nomtien='" + nomtien + '\'' + 
                ", idcateg='" + idcateg + '\'' + 
                ", nomcat='" + nomcat + '\'' + 
                ", tippag='" + tippag + '\'' + 
                ", despag='" + despag + '\'' + "}" + super.toString();
    }
    
}
