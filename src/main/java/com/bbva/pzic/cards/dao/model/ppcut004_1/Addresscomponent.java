package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import java.util.List;

/**
 * <p>Bean fila para el campo tabular <code>addressComponent</code>, utilizado por la clase <code>Addresscomponents</code></p>
 * 
 * @see Addresscomponents
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Addresscomponent {
	
	/**
	 * <p>Campo <code>componentTypes</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "componentTypes", tipo = TipoCampo.LIST)
	private List<Componenttypes> componenttypes;
	
	/**
	 * <p>Campo <code>code</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "code", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 7, signo = true)
	private String code;
	
	/**
	 * <p>Campo <code>name</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
	private String name;
	
}