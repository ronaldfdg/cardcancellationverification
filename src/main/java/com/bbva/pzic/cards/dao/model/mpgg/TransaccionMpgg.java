package com.bbva.pzic.cards.dao.model.mpgg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPGG</code>
 *
 * @see PeticionTransaccionMpgg
 * @see RespuestaTransaccionMpgg
 */
@Component("transaccionMpgg")
public class TransaccionMpgg implements InvocadorTransaccion<PeticionTransaccionMpgg,RespuestaTransaccionMpgg> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpgg invocar(PeticionTransaccionMpgg transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpgg.class, RespuestaTransaccionMpgg.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpgg invocarCache(PeticionTransaccionMpgg transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpgg.class, RespuestaTransaccionMpgg.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
