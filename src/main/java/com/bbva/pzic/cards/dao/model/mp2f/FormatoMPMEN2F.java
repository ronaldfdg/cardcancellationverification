package com.bbva.pzic.cards.dao.model.mp2f;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMEN2F</code> de la transacci&oacute;n <code>MP2F</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMEN2F")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMEN2F {

	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String idetarj;

	/**
	 * <p>Campo <code>FECCAD</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "FECCAD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
	private String feccad;

}