package com.bbva.pzic.cards.dao.model.mpaf;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MPAF</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpaf</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpaf</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: &apos;PEBT.QGFD.FIX.QGDTCCT.MPAF.D1200623&apos;.txt
 * MPAFAFILIACION A CVV2 DINAMICO         MP        MP2CMPAF     01 MPM0AFE             MPAF  NS3000CNNNNN    SSTN     E  NNNSSNNN  NN                2020-06-23P005231 2020-06-2313.46.38P005231 2020-06-23-13.31.50.352630P005231 0001-01-010001-01-01
 * FICHERO: &apos;PEBT.QGFD.FIX.QGDTFDF.MPM0AFE.D1200623&apos;.txt
 * MPM0AFE �ENTRADA ACTIVACION DE TARJETA �F�03�00013�01�00001�CODCLIE�CODIGO DE CLIENTE   �A�008�0�O�        �
 * MPM0AFE �ENTRADA ACTIVACION DE TARJETA �F�03�00013�02�00009�INDESTA�INDICA ESTADO AFILIA�A�001�0�R�        �
 * MPM0AFE �ENTRADA ACTIVACION DE TARJETA �F�03�00013�03�00010�CODSERV�CODIGO SERV AFILIAR �A�004�0�R�        �
</pre></code>
 * 
 * @see RespuestaTransaccionMpaf
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPAF",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpaf.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0AFE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpaf implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}