package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputUpdateCardProposal;
import com.bbva.pzic.cards.dao.model.ppcut002_1.PeticionTransaccionPpcut002_1;
import com.bbva.pzic.cards.dao.model.ppcut002_1.RespuestaTransaccionPpcut002_1;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public interface IApxUpdateCardProposalMapper {

    PeticionTransaccionPpcut002_1 mapIn(InputUpdateCardProposal input);

    Proposal mapOut(RespuestaTransaccionPpcut002_1 response);
}
