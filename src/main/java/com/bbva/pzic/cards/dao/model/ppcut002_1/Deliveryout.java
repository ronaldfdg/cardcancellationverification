package com.bbva.pzic.cards.dao.model.ppcut002_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>deliveryOut</code>, utilizado por la clase <code>Deliveriesout</code></p>
 * 
 * @see Deliveriesout
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Deliveryout {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 12, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>serviceType</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "serviceType", tipo = TipoCampo.DTO)
	private Servicetype servicetype;
	
	/**
	 * <p>Campo <code>deliveryContactOut</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "deliveryContactOut", tipo = TipoCampo.DTO)
	private Deliverycontactout deliverycontactout;
	
	/**
	 * <p>Campo <code>address</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "address", tipo = TipoCampo.DTO)
	private Address address;
	
	/**
	 * <p>Campo <code>destinationOut</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "destinationOut", tipo = TipoCampo.DTO)
	private Destinationout destinationout;
	
}