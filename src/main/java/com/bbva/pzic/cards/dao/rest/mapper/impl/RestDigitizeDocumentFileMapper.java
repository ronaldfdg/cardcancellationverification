package com.bbva.pzic.cards.dao.rest.mapper.impl;


import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.dao.rest.mapper.IRestDigitizeDocumentFileMapper;
import org.springframework.stereotype.Component;

/**
 * Created on 27/03/2018.
 *
 * @author Entelgy
 */
@Component
public class RestDigitizeDocumentFileMapper implements IRestDigitizeDocumentFileMapper {

    @Override
    public Document mapOut(final Documents output) {
        if (output == null || output.getDocumentos() == null || output.getDocumentos().isEmpty()) {
            return null;
        }
        return output.getDocumentos().get(0);
    }
}
