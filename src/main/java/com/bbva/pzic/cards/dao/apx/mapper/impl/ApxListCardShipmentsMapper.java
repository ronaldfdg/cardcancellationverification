package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.dao.apx.mapper.IApxListCardShipmentsMapper;
import com.bbva.pzic.cards.dao.model.pecpt001_1.Participant;
import com.bbva.pzic.cards.dao.model.pecpt001_1.Status;
import com.bbva.pzic.cards.dao.model.pecpt001_1.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ApxListCardShipmentsMapper implements IApxListCardShipmentsMapper {

    @Override
    public PeticionTransaccionPecpt001_1 mapIn(final InputListCardShipments input) {
        PeticionTransaccionPecpt001_1 peticion = new PeticionTransaccionPecpt001_1();
        peticion.setCardagreement(input.getCardAgreement());
        peticion.setExternalcode(input.getExternalCode());
        peticion.setReferencenumber(input.getReferenceNumber());
        peticion.setShippingcompanyid(input.getShippingCompanyId());
        return peticion;
    }

    @Override
    public List<Shipments> mapOut(final RespuestaTransaccionPecpt001_1 response) {
        if (response == null || CollectionUtils.isEmpty(response.getEntityout())) {
            return null;
        }

        return response.getEntityout().stream()
                .filter(Objects::nonNull)
                .map(this::mapOutEntity)
                .collect(Collectors.toList());
    }

    private Shipments mapOutEntity(final Entityout entityout) {
        if (entityout.getData() == null) {
            return null;
        }

        Shipments shipments = new Shipments();
        shipments.setId(entityout.getData().getId());
        shipments.setStatus(mapOutShipmentStatus(entityout.getData().getStatus()));
        shipments.setParticipant(mapOutParticipant(entityout.getData().getParticipant()));
        shipments.setShippingCompany(mapOutShippingCompany(entityout.getData().getShippingcompany()));
        return shipments;
    }

    private ShippingCompany mapOutShippingCompany(final Shippingcompany shippingcompany) {
        if (shippingcompany == null) {
            return null;
        }

        ShippingCompany shippingCompany = new ShippingCompany();
        shippingCompany.setId(shippingcompany.getId());
        shippingCompany.setName(shippingcompany.getName());
        return shippingCompany;
    }

    private com.bbva.pzic.cards.facade.v0.dto.Participant mapOutParticipant(final Participant participant) {
        if (participant == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v0.dto.Participant result = new com.bbva.pzic.cards.facade.v0.dto.Participant();
        result.setFullName(participant.getFullname());
        result.setIdentityDocument(mapOutIdentityDocument(participant.getIdentitydocument()));
        result.setCards(mapOutParticipantCards(participant.getCards()));
        return result;
    }

    private List<ParticipantCards> mapOutParticipantCards(final List<Cards> cards) {
        if (CollectionUtils.isEmpty(cards)) {
            return null;
        }

        return cards.stream()
                .filter(Objects::nonNull)
                .map(this::mapOutParticipantCard)
                .collect(Collectors.toList());
    }

    private ParticipantCards mapOutParticipantCard(final Cards cards) {
        if (cards.getCard() == null) {
            return null;
        }

        ParticipantCards participantCards = new ParticipantCards();
        participantCards.setCardAgreement(cards.getCard().getCardagreement());
        participantCards.setReferenceNumber(cards.getCard().getReferencenumber());
        return participantCards;
    }

    private IdentityDocument mapOutIdentityDocument(final Identitydocument identitydocument) {
        if (identitydocument == null) {
            return null;
        }

        IdentityDocument result = new IdentityDocument();
        result.setDocumentNumber(identitydocument.getDocumentnumber());
        result.setDocumentType(mapOutDocumentType(identitydocument.getDocumenttype()));
        return result;
    }

    private DocumentType mapOutDocumentType(final Documenttype documenttype) {
        if (documenttype == null) {
            return null;
        }

        DocumentType result = new DocumentType();
        result.setId(documenttype.getId());
        result.setDescription(documenttype.getDescription());
        return result;
    }

    private ShipmentStatus mapOutShipmentStatus(final Status status) {
        if (status == null) {
            return null;
        }

        ShipmentStatus shipmentStatus = new ShipmentStatus();
        shipmentStatus.setId(status.getId());
        shipmentStatus.setDescription(status.getDescription());
        return shipmentStatus;
    }
}
