package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.apx.mapper.IApxInitializeCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt003_1.*;
import com.bbva.pzic.cards.facade.v0.dto.InitializeShipment;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.lang.StringUtils;

/**
 * Created on 19/02/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ApxInitializeCardShipmentMapper implements IApxInitializeCardShipmentMapper {

    @Override
    public PeticionTransaccionPecpt003_1 mapIn(final InputInitializeCardShipment input) {
        PeticionTransaccionPecpt003_1 request = new PeticionTransaccionPecpt003_1();
        request.setEntityin(mapInEntityIn(input));
        return request;
    }

    private Entityin mapInEntityIn(final InputInitializeCardShipment input) {
        Entityin entityin = new Entityin();
        entityin.setExternalcode(input.getExternalCode());
        entityin.setParticipant(mapInParticipant(input.getParticipant()));
        entityin.setShippingcompany(mapInShippingCompany(input.getShippingCompany()));
        entityin.setShippingdevice(mapInShippingDevice(input.getShippingDevice()));
        return entityin;
    }

    private Shippingdevice mapInShippingDevice(final DTOIntShippingDevice shippingDevice) {
        Shippingdevice result = new Shippingdevice();
        result.setId(shippingDevice.getId());
        result.setMacaddress(shippingDevice.getMacAddress());
        result.setApplication(mapInApplication(shippingDevice.getApplication()));
        return result;
    }

    private Application mapInApplication(final DTOIntApplication application) {
        Application result = new Application();
        result.setName(application.getName());
        result.setVersion(application.getVersion());
        return result;
    }

    private Shippingcompany mapInShippingCompany(final DTOIntShippingCompany shippingCompany) {
        Shippingcompany result = new Shippingcompany();
        result.setId(shippingCompany.getId());
        return result;
    }

    private Participant mapInParticipant(final DTOIntParticipant participant) {
        Participant result = new Participant();
        result.setIdentitydocument(mapInIdentityDocument(participant.getIdentityDocument()));
        result.setCard(mapInCard(participant.getCard()));
        return result;
    }

    private Card mapInCard(final DTOIntCard card) {
        Card result = new Card();
        result.setCardagreement(card.getCardAgreement());
        result.setReferencenumber(card.getReferenceNumber());
        return result;
    }

    private Identitydocument mapInIdentityDocument(final DTOIntIdentityDocument identityDocument) {
        Identitydocument result = new Identitydocument();
        result.setDocumentnumber(identityDocument.getNumber());
        result.setDocumenttype(mapInDocumentType(identityDocument.getDocumentType()));
        return result;
    }

    private Documenttype mapInDocumentType(final DTOIntDocumentType documentType) {
        Documenttype result = new Documenttype();
        result.setId(documentType.getId());
        return result;
    }

    @Override
    public InitializeShipment mapOut(final RespuestaTransaccionPecpt003_1 response) {
        if (response == null || StringUtils.isEmpty(response.getId())) {
            return null;
        }

        InitializeShipment initializeShipment = new InitializeShipment();
        initializeShipment.setId(response.getId());
        return initializeShipment;
    }
}
