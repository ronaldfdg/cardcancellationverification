package com.bbva.pzic.cards.dao.model.mpg1;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMS4G1</code> de la transacci&oacute;n <code>MPG1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS4G1")
@RooJavaBean
@RooSerializable
public class FormatoMPMS4G1 {
	
	/**
	 * <p>Campo <code>IDCONME</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDCONME", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idconme;
	
	/**
	 * <p>Campo <code>DSCONME</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DSCONME", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dsconme;
	
	/**
	 * <p>Campo <code>IDPERME</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDPERME", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idperme;
	
	/**
	 * <p>Campo <code>DSPERME</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "DSPERME", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dsperme;
	
	/**
	 * <p>Campo <code>FECMEM</code>, &iacute;ndice: <code>5</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 5, nombre = "FECMEM", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecmem;
	
	/**
	 * <p>Campo <code>TIPCOME</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "TIPCOME", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipcome;
	
	/**
	 * <p>Campo <code>DSCOMME</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "DSCOMME", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dscomme;
	
	/**
	 * <p>Campo <code>IMPORTE</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "IMPORTE", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal importe;
	
	/**
	 * <p>Campo <code>MONIMP</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "MONIMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String monimp;
	
	/**
	 * <p>Campo <code>FECINME</code>, &iacute;ndice: <code>10</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 10, nombre = "FECINME", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecinme;
	
	/**
	 * <p>Campo <code>CANMESM</code>, &iacute;ndice: <code>11</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 11, nombre = "CANMESM", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
	private Integer canmesm;
	
	/**
	 * <p>Campo <code>IMPMETA</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 12, nombre = "IMPMETA", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal impmeta;
	
	/**
	 * <p>Campo <code>IMPACUM</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "IMPACUM", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal impacum;
	
	/**
	 * <p>Campo <code>IDPERIO</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "IDPERIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idperio;
	
	/**
	 * <p>Campo <code>DSPERIO</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "DSPERIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String dsperio;
	
	/**
	 * <p>Campo <code>FECEVAL</code>, &iacute;ndice: <code>16</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 16, nombre = "FECEVAL", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date feceval;
	
	/**
	 * <p>Campo <code>IDLSGOL</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "IDLSGOL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idlsgol;
	
	/**
	 * <p>Campo <code>DSLSGOL</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "DSLSGOL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String dslsgol;
	
	/**
	 * <p>Campo <code>IDCUBAL</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "IDCUBAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idcubal;
	
	/**
	 * <p>Campo <code>DSCUBAL</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "DSCUBAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String dscubal;
	
}