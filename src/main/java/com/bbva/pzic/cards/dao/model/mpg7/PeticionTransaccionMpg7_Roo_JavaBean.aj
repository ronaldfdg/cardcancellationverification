// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpg7;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.cards.dao.model.mpg7.PeticionTransaccionMpg7;

privileged aspect PeticionTransaccionMpg7_Roo_JavaBean {
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return PeticionTransaccionMpg7
     */
    public PeticionTransaccionMpg7 PeticionTransaccionMpg7.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String PeticionTransaccionMpg7.toString() {
        return "PeticionTransaccionMpg7 {" + 
        "}" + super.toString();
    }
    
}
