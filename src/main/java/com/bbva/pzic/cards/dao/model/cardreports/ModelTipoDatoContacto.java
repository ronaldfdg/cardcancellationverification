package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelTipoDatoContacto {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
