package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.dao.apx.mapper.IApxListCardShipmentsMapper;
import com.bbva.pzic.cards.dao.model.pecpt001_1.PeticionTransaccionPecpt001_1;
import com.bbva.pzic.cards.dao.model.pecpt001_1.RespuestaTransaccionPecpt001_1;
import com.bbva.pzic.cards.facade.v0.dto.Shipments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Component
public class ApxListCardShipments {

    @Autowired
    private IApxListCardShipmentsMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionPecpt001_1, RespuestaTransaccionPecpt001_1> transaccion;

    public List<Shipments> perform(final InputListCardShipments input) {
        PeticionTransaccionPecpt001_1 request = mapper.mapIn(input);
        RespuestaTransaccionPecpt001_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
