package com.bbva.pzic.cards.dao.model.pecpt005_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.dao.model.pecpt005_1.PeticionTransaccionPecpt005_1;
import com.bbva.pzic.cards.dao.model.pecpt005_1.RespuestaTransaccionPecpt005_1;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>PECPT001</code>
 *
 * @see PeticionTransaccionPecpt005_1
 * @see RespuestaTransaccionPecpt005_1
 */
@Component("transaccionPecpt005_1")
public class TransaccionPecpt005_1Mock implements InvocadorTransaccion<PeticionTransaccionPecpt005_1, RespuestaTransaccionPecpt005_1> {

    @Override
    public RespuestaTransaccionPecpt005_1 invocar(final PeticionTransaccionPecpt005_1 transaccion) {
        return new RespuestaTransaccionPecpt005_1();
    }

    @Override
    public RespuestaTransaccionPecpt005_1 invocarCache(PeticionTransaccionPecpt005_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
