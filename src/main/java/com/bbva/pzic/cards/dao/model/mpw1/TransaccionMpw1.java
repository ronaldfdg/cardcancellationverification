package com.bbva.pzic.cards.dao.model.mpw1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPW1</code>
 *
 * @see PeticionTransaccionMpw1
 * @see RespuestaTransaccionMpw1
 */
@Component
public class TransaccionMpw1 implements InvocadorTransaccion<PeticionTransaccionMpw1,RespuestaTransaccionMpw1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpw1 invocar(PeticionTransaccionMpw1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpw1.class, RespuestaTransaccionMpw1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpw1 invocarCache(PeticionTransaccionMpw1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpw1.class, RespuestaTransaccionMpw1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
