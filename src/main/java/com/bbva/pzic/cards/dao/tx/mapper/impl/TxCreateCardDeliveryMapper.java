package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntDelivery;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTECKB94;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTSCKB94;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardDeliveryMapper;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.bbva.pzic.cards.util.Constants.*;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
@Mapper("txCreateCardDeliveryMapper")
public class TxCreateCardDeliveryMapper extends ConfigurableMapper implements ITxCreateCardDeliveryMapper {

    private static final Log LOG = LogFactory.getLog(TxCreateCardDeliveryMapper.class);

    private Translator translator;

    public TxCreateCardDeliveryMapper(Translator translator) {
        this.translator = translator;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(DTOIntDelivery.class, FormatoKTECKB94.class)
                .field("cardId", "numtar")
                .field("serviceTypeId", "tipent")
                .field("contact.contactType", "natcont")
                .field("contact.contact.contactDetailType", "tipcont")
                .field("contact.contact.address", "correo")
                .field("contact.contact.number", "numfijo")
                .field("contact.contact.phoneType", "tiponum")
                .field("contact.contact.countryId", "codpais")
                .field("contact.contact.regionalCode", "codreg")
                .field("contact.contact.number", "numcel")
                .field("contact.contact.phoneCompany.id", "compcel")
                .field("contact.contact.username", "usuari")
                .field("contact.contact.socialNetwork", "redsoc")
                .field("contact.contact.id", "idcont")
                .field("destination.id", "tipdest")
                .register();

        factory.classMap(FormatoKTSCKB94.class, Delivery.class)
                .field("idafil", "id")
                .field("tipent", "serviceType.id")
                .field("tipdest", "destination.id")
                .field("desctd", "destination.name")
                .register();
    }

    @Override
    public FormatoKTECKB94 mapIn(final DTOIntDelivery dtoIn) {
        LOG.info("... called method TxCreateCardDeliveryMapper.mapIn ...");
        FormatoKTECKB94 formatInput = map(dtoIn, FormatoKTECKB94.class);

        if (Constants.SPECIFIC.equals(dtoIn.getContact().getContactTypeOriginal())) {
            switch (dtoIn.getContact().getContact().getContactDetailTypeOriginal()) {
                case EMAIL:
                    break;

                case LANDLINE:
                    formatInput.setNumcel(null);
                    break;

                case MOBILE:
                    formatInput.setNumfijo(null);
                    break;

                case SOCIAL_MEDIA:
                    break;

                default:
                    LOG.warn(String.format("Unrecognized contact detail type %s", dtoIn.getContact().getContact().getContactDetailTypeOriginal()));
            }
        }

        return formatInput;
    }

    @Override
    public Delivery mapOut(final FormatoKTSCKB94 formatOutput) {
        LOG.info("... called method TxCreateCardDeliveryMapper.mapOut ...");
        Delivery delivery = map(formatOutput, Delivery.class);

        if (formatOutput.getNatcont() != null) {
            Contact contact = new Contact();
            contact.setContactType(translator.translateBackendEnumValueStrictly(
                    "cards.delivery.contactType", formatOutput.getNatcont()));

            SpecificContact specificContact = new SpecificContact();
            if (SPECIFIC.equals(contact.getContactType())) {
                specificContact.setContactDetailType(
                        translator.translateBackendEnumValueStrictly(
                                "cards.delivery.contactDetailType", formatOutput.getTipcont()));

                if (EMAIL.equals(specificContact.getContactDetailType())) {
                    specificContact.setAddress(formatOutput.getCorreo());

                } else if (LANDLINE.equals(specificContact.getContactDetailType())) {
                    specificContact.setNumber(formatOutput.getNumfijo());
                    specificContact.setPhoneType(translator.translateBackendEnumValueStrictly(
                            "cards.delivery.contact.phoneType", formatOutput.getTiponum()));

                    Country country = new Country();
                    country.setId(formatOutput.getCodpais());
                    country.setName(formatOutput.getDespais());
                    specificContact.setCountry(country);

                    specificContact.setRegionalCode(formatOutput.getCodreg());

                } else if (MOBILE.equals(specificContact.getContactDetailType())) {
                    specificContact.setNumber(formatOutput.getNumcel());

                    PhoneCompany phoneCompany = new PhoneCompany();
                    phoneCompany.setId(formatOutput.getCompcel());
                    phoneCompany.setName(formatOutput.getCompmov());

                    specificContact.setPhoneCompany(phoneCompany);

                } else if (SOCIAL_MEDIA.equals(specificContact.getContactDetailType())) {
                    specificContact.setUsername(formatOutput.getUsuari());
                    specificContact.setSocialNetwork(translator.translateBackendEnumValueStrictly(
                            "cards.delivery.contact.socialNetwork", formatOutput.getRedsoc()));

                } else {
                    LOG.warn(String.format("Unrecognized contact detail type %s", specificContact.getContactDetailType()));
                }

            } else if (STORED.equals(contact.getContactType())) {
                specificContact.setId(String.valueOf(formatOutput.getIdcont()));

            } else {
                LOG.warn(String.format("Unrecognized contact type %s", contact.getContactType()));
            }

            contact.setContact(specificContact);
            delivery.setContact(contact);
        }

        if (!StringUtils.isEmpty(formatOutput.getTipdest())) {
            delivery.getDestination().setId(
                    translator.translateBackendEnumValueStrictly("cards.delivery.destination.id", formatOutput.getTipdest()));
        }
        return delivery;
    }
}
