package com.bbva.pzic.cards.dao.model.mpwp;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMENWP</code> de la transacci&oacute;n <code>MPWP</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMENWP")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMENWP {

	/**
	 * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String numtarj;

	/**
	 * <p>Campo <code>DTCIF01</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DTCIF01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif01;

	/**
	 * <p>Campo <code>DTCIF02</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "DTCIF02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif02;

	/**
	 * <p>Campo <code>DTCIF03</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "DTCIF03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif03;

	/**
	 * <p>Campo <code>DTCIF04</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DTCIF04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif04;

	/**
	 * <p>Campo <code>DTCIF05</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "DTCIF05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif05;

	/**
	 * <p>Campo <code>DTCIF06</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "DTCIF06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif06;

	/**
	 * <p>Campo <code>DTCIF07</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "DTCIF07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif07;

	/**
	 * <p>Campo <code>DTCIF08</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "DTCIF08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif08;

	/**
	 * <p>Campo <code>DTCIF09</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "DTCIF09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif09;

	/**
	 * <p>Campo <code>DTCIF10</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "DTCIF10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif10;

	/**
	 * <p>Campo <code>DTCIF11</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "DTCIF11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif11;

	/**
	 * <p>Campo <code>DTCIF12</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "DTCIF12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif12;

	/**
	 * <p>Campo <code>DTCIF13</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "DTCIF13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif13;

	/**
	 * <p>Campo <code>DTCIF14</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "DTCIF14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String dtcif14;

}