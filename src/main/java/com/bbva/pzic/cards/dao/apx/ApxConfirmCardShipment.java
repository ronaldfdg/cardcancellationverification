package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputConfirmCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxConfirmCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt005_1.PeticionTransaccionPecpt005_1;
import com.bbva.pzic.cards.dao.model.pecpt005_1.RespuestaTransaccionPecpt005_1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Component
public class ApxConfirmCardShipment {

    @Autowired
    private IApxConfirmCardShipmentMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionPecpt005_1, RespuestaTransaccionPecpt005_1> transaccion;

    public void invoke(final InputConfirmCardShipment input) {
        transaccion.invocar(mapper.mapIn(input));
    }
}
