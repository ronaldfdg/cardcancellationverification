package com.bbva.pzic.cards.dao.model.mp3g;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MP3G</code>
 * 
 * @see PeticionTransaccionMp3g
 * @see RespuestaTransaccionMp3g
 */
@Component
public class TransaccionMp3g implements InvocadorTransaccion<PeticionTransaccionMp3g,RespuestaTransaccionMp3g> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMp3g invocar(PeticionTransaccionMp3g transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp3g.class, RespuestaTransaccionMp3g.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMp3g invocarCache(PeticionTransaccionMp3g transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp3g.class, RespuestaTransaccionMp3g.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}