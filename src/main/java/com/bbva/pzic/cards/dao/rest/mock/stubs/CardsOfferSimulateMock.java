package com.bbva.pzic.cards.dao.rest.mock.stubs;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationResponse;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 04/12/2018.
 *
 * @author Entelgy
 */

public class CardsOfferSimulateMock {

    private static final CardsOfferSimulateMock INSTANCE = new CardsOfferSimulateMock();

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private CardsOfferSimulateMock() {
    }

    public static CardsOfferSimulateMock getInstance() {
        return INSTANCE;
    }

    public HolderSimulation buildCarHolderSimulationRequest() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("com/bbva/pzic/cards/dao/rest/mock/request_create_cards_offer_simulate.json"),
                HolderSimulation.class);
    }

    public CardHolderSimulationResponse buildCarHolderSimulationResponse() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("com/bbva/pzic/cards/dao/rest/mock/response_create_cards_offer_simulate.json"),
                CardHolderSimulationResponse.class);
    }

    public Message getFatalMessage() {
        Message message = new Message();
        message.setCode("1");
        message.setMessage("Ingrese RUC valido del Empleador");
        message.setType(ErrorSeverity.FATAL);
        return message;
    }
}
