package com.bbva.pzic.cards.dao.model.mp3g;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMS33G</code> de la transacci&oacute;n <code>MP3G</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS33G")
@RooJavaBean
@RooSerializable
public class FormatoMPMS33G {
	
	/**
	 * <p>Campo <code>MONEDA2</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "MONEDA2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moneda2;
	
	/**
	 * <p>Campo <code>AMITYP2</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "AMITYP2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amityp2;
	
	/**
	 * <p>Campo <code>AMCTYP2</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "AMCTYP2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amctyp2;
	
	/**
	 * <p>Campo <code>CAPVENC</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "CAPVENC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal capvenc;
	
	/**
	 * <p>Campo <code>IDPVMON</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "IDPVMON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvmon;
	
	/**
	 * <p>Campo <code>INTERES</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "INTERES", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal interes;
	
	/**
	 * <p>Campo <code>IDPVEC2</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "IDPVEC2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvec2;
	
	/**
	 * <p>Campo <code>SEGVEN1</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "SEGVEN1", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal segven1;
	
	/**
	 * <p>Campo <code>SEGVEN2</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "SEGVEN2", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal segven2;
	
	/**
	 * <p>Campo <code>IDPVCUO</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "IDPVCUO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvcuo;
	
	/**
	 * <p>Campo <code>CINCPEV</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 11, nombre = "CINCPEV", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal cincpev;
	
	/**
	 * <p>Campo <code>IDPVMEM</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "IDPVMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvmem;
	
	/**
	 * <p>Campo <code>CUOVENC</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "CUOVENC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal cuovenc;
	
	/**
	 * <p>Campo <code>IDPVDIS</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "IDPVDIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvdis;
	
	/**
	 * <p>Campo <code>COMFVEN</code>, &iacute;ndice: <code>15</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 15, nombre = "COMFVEN", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal comfven;
	
	/**
	 * <p>Campo <code>IDPVGRI</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "IDPVGRI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvgri;
	
	/**
	 * <p>Campo <code>COMGVEN</code>, &iacute;ndice: <code>17</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 17, nombre = "COMGVEN", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal comgven;
	
	/**
	 * <p>Campo <code>IDPVMAN</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "IDPVMAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvman;
	
	/**
	 * <p>Campo <code>COMPVEN</code>, &iacute;ndice: <code>19</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 19, nombre = "COMPVEN", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal compven;
	
	/**
	 * <p>Campo <code>IDPVPEN</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "IDPVPEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvpen;
	
	/**
	 * <p>Campo <code>COMPAG3</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 21, nombre = "COMPAG3", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal compag3;
	
	/**
	 * <p>Campo <code>IDPVSOB</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "IDPVSOB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvsob;
	
	/**
	 * <p>Campo <code>COMVESO</code>, &iacute;ndice: <code>23</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 23, nombre = "COMVESO", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal comveso;
	
	/**
	 * <p>Campo <code>IDPVEC1</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "IDPVEC1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvec1;
	
	/**
	 * <p>Campo <code>PORVENC</code>, &iacute;ndice: <code>25</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 25, nombre = "PORVENC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal porvenc;
	
	/**
	 * <p>Campo <code>MONEXCV</code>, &iacute;ndice: <code>26</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 26, nombre = "MONEXCV", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal monexcv;
	
	/**
	 * <p>Campo <code>IDPVIMO</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "IDPVIMO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvimo;
	
	/**
	 * <p>Campo <code>MORAVEN</code>, &iacute;ndice: <code>28</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 28, nombre = "MORAVEN", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal moraven;
	
	/**
	 * <p>Campo <code>IDPVICO</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 29, nombre = "IDPVICO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idpvico;
	
	/**
	 * <p>Campo <code>INTVENC</code>, &iacute;ndice: <code>30</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 30, nombre = "INTVENC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal intvenc;
	
	/**
	 * <p>Campo <code>CISCPEV</code>, &iacute;ndice: <code>31</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 31, nombre = "CISCPEV", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal ciscpev;
	
}