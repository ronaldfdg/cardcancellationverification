package com.bbva.pzic.cards.dao.rest.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationResponse;

import java.util.HashMap;

public interface IRestCreateCardsOfferSimulateMapper {

    HashMap<String, String> mapInQueryParams(DTOIntDetailSimulation input);

    HolderSimulation mapOut(CardHolderSimulationResponse output);
}
