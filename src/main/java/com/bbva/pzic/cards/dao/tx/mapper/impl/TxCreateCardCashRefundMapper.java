package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCEMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardCashRefundMapper;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;

@Mapper
public class TxCreateCardCashRefundMapper implements ITxCreateCardCashRefundMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public FormatoMCEMDC0 mapInput(DTOInputCreateCashRefund dtoInt) {
        FormatoMCEMDC0 formatoMCEMDC0 = new FormatoMCEMDC0();
        formatoMCEMDC0.setNrotarj(dtoInt.getCardId());
        formatoMCEMDC0.setTipreem(dtoInt.getCashRefund().getRefundType());
        formatoMCEMDC0.setDivisa(dtoInt.getCashRefund().getRefundAmount().getCurrency());
        formatoMCEMDC0.setNcuenta(dtoInt.getCashRefund().getReceivingAccount().getId());
        return formatoMCEMDC0;
    }

    @Override
    public DTOIntCashRefund mapOutput(FormatoMCRMDC0 formatoMCRMDC0, DTOIntCashRefund dtoOut) {
        dtoOut.setId(formatoMCRMDC0.getIdoper());
        dtoOut.setDescription(formatoMCRMDC0.getDesoper());
        dtoOut.setRefundType(enumMapper.getEnumValue("cards.refundType", formatoMCRMDC0.getTipreem()));

        if (formatoMCRMDC0.getIsalacr() != null || formatoMCRMDC0.getDivsacr() != null) {
            DTOIntRefundAmount dtoIntRefundAmount = new DTOIntRefundAmount();
            dtoIntRefundAmount.setAmount(formatoMCRMDC0.getIsalacr());
            dtoIntRefundAmount.setCurrency(formatoMCRMDC0.getDivsacr());
            dtoOut.setRefundAmount(dtoIntRefundAmount);
        }

        if (formatoMCRMDC0.getNcuenta() != null) {
            DTOIntReceivingAccount dtoIntReceivingAccount = new DTOIntReceivingAccount();
            dtoIntReceivingAccount.setId(formatoMCRMDC0.getNcuenta());
            dtoOut.setReceivingAccount(dtoIntReceivingAccount);
        }

        try {
            if (formatoMCRMDC0.getFecoper() != null && !StringUtils.isEmpty(formatoMCRMDC0.getHoraope())) {
                dtoOut.setOperationDate(DateUtils.toDateTime(formatoMCRMDC0.getFecoper(), formatoMCRMDC0.getHoraope()));
            }
            if (formatoMCRMDC0.getFecfact() != null && !StringUtils.isEmpty(formatoMCRMDC0.getHorafac())) {
                dtoOut.setAccoutingDate(DateUtils.toDateTime(formatoMCRMDC0.getFecfact(), formatoMCRMDC0.getHorafac()));
            }
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }


        if (formatoMCRMDC0.getIsalcta() != null || formatoMCRMDC0.getDivsact() != null) {
            DTOIntReceivedAmount dtoIntReceivedAmount = new DTOIntReceivedAmount();
            dtoIntReceivedAmount.setAmount(formatoMCRMDC0.getIsalcta());
            dtoIntReceivedAmount.setCurrency(formatoMCRMDC0.getDivsact());
            dtoOut.setReceivedAmount(dtoIntReceivedAmount);
        }

        if (formatoMCRMDC0.getTipcam() != null || formatoMCRMDC0.getTipprec() != null ||
                dtoOut.getOperationDate() != null) {
            DTOIntExchangeRate dtoIntExchangeRate = new DTOIntExchangeRate();
            dtoIntExchangeRate.setDate(dtoOut.getOperationDate());
            if (formatoMCRMDC0.getTipcam() != null || formatoMCRMDC0.getTipprec() != null) {
                DTOIntValues dtoIntValues = new DTOIntValues();
                if (formatoMCRMDC0.getTipcam() != null) {
                    DTOIntFactor dtoIntFactor = new DTOIntFactor();
                    dtoIntFactor.setValue(formatoMCRMDC0.getTipcam());
                    dtoIntValues.setFactor(dtoIntFactor);
                }
                if (formatoMCRMDC0.getTipprec() != null) {
                    dtoIntValues.setPriceType(enumMapper.getEnumValue("exchangeRate.values.priceType", formatoMCRMDC0.getTipprec()));
                }
                dtoIntExchangeRate.setValues(dtoIntValues);
            }
            dtoOut.setExchangeRate(dtoIntExchangeRate);
        }

        if (formatoMCRMDC0.getTotimp() != null || formatoMCRMDC0.getDivtoti() != null) {
            DTOIntTaxes dtoIntTaxes = new DTOIntTaxes();
            DTOIntTotalTaxes dtoIntTotalTaxes = new DTOIntTotalTaxes();
            dtoIntTotalTaxes.setAmount(formatoMCRMDC0.getTotimp());
            dtoIntTotalTaxes.setCurrency(formatoMCRMDC0.getDivtoti());
            dtoIntTaxes.setTotalTaxes(dtoIntTotalTaxes);
            dtoOut.setTaxes(dtoIntTaxes);
        }

        return dtoOut;
    }

    @Override
    public DTOIntCashRefund mapOutput(FormatoMCRMDC1 formatoMCRMDC1, DTOIntCashRefund dtoOut) {

        if (dtoOut.getTaxes() == null) {
            dtoOut.setTaxes(new DTOIntTaxes());
        }

        if (dtoOut.getTaxes().getItemizeTaxes() == null) {
            dtoOut.getTaxes().setItemizeTaxes(new ArrayList<>());
        }

        DTOIntItemizeTaxes dtoIntItemizeTaxes = new DTOIntItemizeTaxes();
        dtoIntItemizeTaxes.setTaxType(enumMapper.getEnumValue("transfers.taxes.taxType", formatoMCRMDC1.getTipoimp()));

        if (formatoMCRMDC1.getMonimp() != null || formatoMCRMDC1.getDivimp() != null) {
            DTOIntMonetaryAmount dtoIntMonetaryAmount = new DTOIntMonetaryAmount();
            dtoIntMonetaryAmount.setAmount(formatoMCRMDC1.getMonimp());
            dtoIntMonetaryAmount.setCurrency(formatoMCRMDC1.getDivimp());
            dtoIntItemizeTaxes.setMonetaryAmount(dtoIntMonetaryAmount);
        }

        dtoOut.getTaxes().getItemizeTaxes().add(dtoIntItemizeTaxes);


        return dtoOut;
    }
}
