// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpl2;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.cards.dao.model.mpl2.RespuestaTransaccionMpl2;

privileged aspect RespuestaTransaccionMpl2_Roo_JavaBean {
    
    /**
     * Gets codigoRetorno value
     * 
     * @return String
     */
    public String RespuestaTransaccionMpl2.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    /**
     * Sets codigoRetorno value
     * 
     * @param codigoRetorno
     * @return RespuestaTransaccionMpl2
     */
    public RespuestaTransaccionMpl2 RespuestaTransaccionMpl2.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }
    
    /**
     * Gets codigoControl value
     * 
     * @return String
     */
    public String RespuestaTransaccionMpl2.getCodigoControl() {
        return this.codigoControl;
    }
    
    /**
     * Sets codigoControl value
     * 
     * @param codigoControl
     * @return RespuestaTransaccionMpl2
     */
    public RespuestaTransaccionMpl2 RespuestaTransaccionMpl2.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
        return this;
    }
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return RespuestaTransaccionMpl2
     */
    public RespuestaTransaccionMpl2 RespuestaTransaccionMpl2.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String RespuestaTransaccionMpl2.toString() {
        return "RespuestaTransaccionMpl2 {" + 
                "codigoRetorno='" + codigoRetorno + '\'' + 
                ", codigoControl='" + codigoControl + '\'' + "}" + super.toString();
    }
    
}
