package com.bbva.pzic.cards.dao.model.mpg5.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS1G5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS2G5;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
public class FormatoMPMSNG5Mock {

    private ObjectMapperHelper mapper;

    public FormatoMPMSNG5Mock() {
        mapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPMS1G5 getFormatoMPMS1G5() {
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader()
                            .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpg5/mock/formatoMPMS1G5.json"),
                    FormatoMPMS1G5.class);
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    public List<FormatoMPMS2G5> getFormatoMPMS2G5() {
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpg5/mock/formatoMPMS2G5.json"), new TypeReference<List<FormatoMPMS2G5>>() {
            });
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    public FormatoMPMS2G5 getFormatoMPMS2G5Emtpy() {
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader()
                            .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpg5/mock/formatoMPMS2G5-Empty.json"),
                    FormatoMPMS2G5.class);
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

}
