package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardProposal;
import com.bbva.pzic.cards.dao.model.ppcut001_1.PeticionTransaccionPpcut001_1;
import com.bbva.pzic.cards.dao.model.ppcut001_1.RespuestaTransaccionPpcut001_1;
import com.bbva.pzic.cards.facade.v1.dto.ProposalCard;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public interface IApxCreateCardProposalMapper {

    PeticionTransaccionPpcut001_1 mapIn(InputCreateCardProposal input);

    ProposalCard mapOut(RespuestaTransaccionPpcut001_1 response);
}
