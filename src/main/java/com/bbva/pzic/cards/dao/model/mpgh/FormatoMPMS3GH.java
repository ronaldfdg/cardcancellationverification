package com.bbva.pzic.cards.dao.model.mpgh;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS3GH</code> de la transacci&oacute;n <code>MPGH</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS3GH")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS3GH {

	/**
	 * <p>Campo <code>LINME01</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "LINME01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 85, longitudMaxima = 85)
	private String linme01;

	/**
	 * <p>Campo <code>LINME02</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "LINME02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 85, longitudMaxima = 85)
	private String linme02;

	/**
	 * <p>Campo <code>LINME03</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "LINME03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 85, longitudMaxima = 85)
	private String linme03;

	/**
	 * <p>Campo <code>LINME04</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "LINME04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 85, longitudMaxima = 85)
	private String linme04;

}