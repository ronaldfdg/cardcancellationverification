package com.bbva.pzic.cards.dao.model.ppcut002_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.ppcut002_1.PeticionTransaccionPpcut002_1;
import com.bbva.pzic.cards.dao.model.ppcut002_1.RespuestaTransaccionPpcut002_1;
import com.bbva.pzic.cards.util.Errors;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>PCUNT004</code>
 *
 * @see PeticionTransaccionPpcut002_1
 * @see RespuestaTransaccionPpcut002_1
 */
@Component("transaccionPpcut002_1")
public class TransaccionPpcut002_1Mock implements InvocadorTransaccion<PeticionTransaccionPpcut002_1, RespuestaTransaccionPpcut002_1> {

    public static final String TEST_EMPTY = "999";

    @Override
    public RespuestaTransaccionPpcut002_1 invocar(PeticionTransaccionPpcut002_1 transaccion) {
        try {
            if (transaccion.getEntityin() != null &&
                    StringUtils.isNotEmpty(transaccion.getProposalId()) &&
                    TEST_EMPTY.equalsIgnoreCase(transaccion.getProposalId())) {
                return new RespuestaTransaccionPpcut002_1();
            }
            return Ppcut002_1Stubs.getInstance().getProposal();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPpcut002_1 invocarCache(PeticionTransaccionPpcut002_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
