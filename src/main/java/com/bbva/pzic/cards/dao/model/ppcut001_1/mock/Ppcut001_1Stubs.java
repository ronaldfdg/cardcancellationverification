package com.bbva.pzic.cards.dao.model.ppcut001_1.mock;

import com.bbva.pzic.cards.dao.model.ppcut001_1.RespuestaTransaccionPpcut001_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 06/11/2019.
 *
 * @author Entelgy
 */
public final class Ppcut001_1Stubs {

    private static final Ppcut001_1Stubs INSTANCE = new Ppcut001_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Ppcut001_1Stubs() {
    }

    public static Ppcut001_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPpcut001_1 getProposal() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/ppcut001_1/mock/proposal_output.json"), RespuestaTransaccionPpcut001_1.class);
    }
}
