package com.bbva.pzic.cards.dao.model.mpg9;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPG9</code>
 *
 * @see PeticionTransaccionMpg9
 * @see RespuestaTransaccionMpg9
 */
@Component("transaccionMpg9")
public class TransaccionMpg9 implements InvocadorTransaccion<PeticionTransaccionMpg9, RespuestaTransaccionMpg9> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionMpg9 invocar(PeticionTransaccionMpg9 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpg9.class, RespuestaTransaccionMpg9.class, transaccion);
    }

    @Override
    public RespuestaTransaccionMpg9 invocarCache(PeticionTransaccionMpg9 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpg9.class, RespuestaTransaccionMpg9.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
