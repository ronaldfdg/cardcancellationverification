package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMENCV;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMS1CV;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardSecurityDataMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@Mapper("txGetCardSecurityDataMapper")
public class TxGetCardSecurityDataMapper implements ITxGetCardSecurityDataMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public FormatoMPMENCV mapIn(final InputGetCardSecurityData dtoIn) {
        FormatoMPMENCV format = new FormatoMPMENCV();
        format.setNumtarj(dtoIn.getCardId());

        String publicKey = dtoIn.getPublicKey();

        format.setKeypb01(StringUtils.trimToNull(StringUtils.substring(publicKey, 0, 75)));
        format.setKeypb02(StringUtils.trimToNull(StringUtils.substring(publicKey, 75, 150)));
        format.setKeypb03(StringUtils.trimToNull(StringUtils.substring(publicKey, 150, 225)));
        format.setKeypb04(StringUtils.trimToNull(StringUtils.substring(publicKey, 225, 300)));
        format.setKeypb05(StringUtils.trimToNull(StringUtils.substring(publicKey, 300, 375)));
        format.setKeypb06(StringUtils.trimToNull(StringUtils.substring(publicKey, 375, 450)));
        format.setKeypb07(StringUtils.trimToNull(StringUtils.substring(publicKey, 450, 525)));
        format.setKeypb08(StringUtils.trimToNull(StringUtils.substring(publicKey, 525, 600)));
        format.setKeypb09(StringUtils.trimToNull(StringUtils.substring(publicKey, 600, 675)));
        format.setKeypb10(StringUtils.trimToNull(StringUtils.substring(publicKey, 675, 750)));
        format.setKeypb11(StringUtils.trimToNull(StringUtils.substring(publicKey, 750, 825)));
        format.setKeypb12(StringUtils.trimToNull(StringUtils.substring(publicKey, 825, 900)));
        format.setKeypb13(StringUtils.trimToNull(StringUtils.substring(publicKey, 900, 975)));
        format.setKeypb14(StringUtils.trimToNull(StringUtils.substring(publicKey, 975, 1050)));

        return format;
    }

    @Override
    public SecurityData mapOut(final FormatoMPMS1CV formatOutput) {
        if (formatOutput == null) {
            return null;
        }

        SecurityData securityData = new SecurityData();
        securityData.setId(enumMapper.getEnumValue("securityData.id", formatOutput.getIdcodsg()));
        securityData.setName(formatOutput.getDscodsg());

        StringBuilder codeTotal = new StringBuilder();
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse01()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse02()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse03()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse04()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse05()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse06()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse07()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse08()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse09()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse10()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse11()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse12()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse13()));
        codeTotal.append(StringUtils.trimToEmpty(formatOutput.getCodse14()));

        securityData.setCode(new String(codeTotal));

        return securityData;
    }
}
