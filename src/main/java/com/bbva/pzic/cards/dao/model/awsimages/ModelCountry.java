package com.bbva.pzic.cards.dao.model.awsimages;

/**
 * Created on 30/07/2019.
 *
 * @author Entelgy
 */
public class ModelCountry {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
