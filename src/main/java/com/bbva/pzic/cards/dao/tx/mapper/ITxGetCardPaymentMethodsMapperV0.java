package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMENG5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS1G5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS2G5;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
public interface ITxGetCardPaymentMethodsMapperV0 {
    /**
     * @param dtoIn
     * @return
     */
    FormatoMPMENG5 mapIn(DTOIntCard dtoIn);

    /**
     * @param formatOutput
     * @param dtoOut
     * @return
     */
    PaymentMethod mapOut1(FormatoMPMS1G5 formatOutput, PaymentMethod dtoOut);

    /**
     * @param formatOutput
     * @param dtoOut
     * @return
     */
    PaymentMethod mapOut2(FormatoMPMS2G5 formatOutput, PaymentMethod dtoOut);

}
