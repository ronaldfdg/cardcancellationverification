package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMENG3;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMS1G3;
import com.bbva.pzic.cards.dao.model.mpg3.PeticionTransaccionMpg3;
import com.bbva.pzic.cards.dao.model.mpg3.RespuestaTransaccionMpg3;
import com.bbva.pzic.cards.dao.tx.mapper.impl.TxModifyCardActivationsMapperV0;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 10/10/2017.
 *
 * @author Entelgy
 */
@Component("txModifyCardActivations")
public class TxModifyCardActivations extends
        SingleOutputFormat<InputModifyCardActivations, FormatoMPMENG3, List<Activation>, FormatoMPMS1G3> {

    @Autowired
    private TxModifyCardActivationsMapperV0 mapper;

    @Autowired
    public TxModifyCardActivations(@Qualifier("transaccionMpg3") InvocadorTransaccion<PeticionTransaccionMpg3, RespuestaTransaccionMpg3> transaction) {
        super(transaction, PeticionTransaccionMpg3::new, ArrayList::new, FormatoMPMS1G3.class);
    }

    @Override
    protected FormatoMPMENG3 mapInput(InputModifyCardActivations dtoIn) {
        return mapper.mapIn(dtoIn);
    }

    @Override
    protected List<Activation> mapFirstOutputFormat(FormatoMPMS1G3 formatoMPMS1G3, InputModifyCardActivations dtoIn, List<Activation> activations) {
        return mapper.mapOut(formatoMPMS1G3);
    }
}
