package com.bbva.pzic.cards.dao.rest.mock.stubs;

import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 15/02/2018.
 *
 * @author Entelgy
 */

public enum ResponseDocumentsMock {
    INSTANCE;

    private ObjectMapperHelper objectMapper;

    ResponseDocumentsMock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public Documents buildDocumentFile() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/rest/mock/response_get_document_file.json"), Documents.class);
    }
}
