package com.bbva.pzic.cards.dao.model.mpv1;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPV1</code>
 *
 * @see PeticionTransaccionMpv1
 * @see RespuestaTransaccionMpv1
 */
@Component("transaccionMpv1")
public class TransaccionMpv1 implements InvocadorTransaccion<PeticionTransaccionMpv1, RespuestaTransaccionMpv1> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionMpv1 invocar(PeticionTransaccionMpv1 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpv1.class, RespuestaTransaccionMpv1.class, transaccion);
    }

    @Override
    public RespuestaTransaccionMpv1 invocarCache(PeticionTransaccionMpv1 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpv1.class, RespuestaTransaccionMpv1.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
