package com.bbva.pzic.cards.dao.rest;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;
import com.bbva.pzic.cards.dao.rest.mapper.IRestListImagesCoversHolderSimulationMapper;
import com.bbva.pzic.cards.util.connection.rest.RestPostConnection;
import com.bbva.pzic.cards.util.generate.AWSV4AuthGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
@Component
public class RestListImagesCoversHolderSimulation extends RestPostConnection<AWSImagesRequest, AWSImagesResponse> {

    private static final Log LOG = LogFactory.getLog(RestListImagesCoversHolderSimulation.class);
    private static final String URL_PROPERTY_AWS = "servicing.url.cards.listCards.AWS";

    @Autowired
    public IRestListImagesCoversHolderSimulationMapper mapper;

    @Autowired
    private AWSV4AuthGenerator awsv4AuthGenerator;

    @PostConstruct
    public void init() {
        useProxy = Boolean.TRUE;
    }

    public void perform(final HolderSimulation holderSimulation) {
        AWSImagesRequest request = mapper.mapIn(holderSimulation);

        if (request == null || request.getCards().isEmpty()) {
            LOG.warn("[AWS Invocation] cards image is empty, omitting AWS request");
        } else {
            LOG.info("[AWS Invocation] making AWS request");

            Map<String, String> headers = awsv4AuthGenerator.createAuthorization(
                    "POST", getProperty(URL_PROPERTY_AWS), null, null,
                    buildPayload(request));

            mapper.mapOut(connect(URL_PROPERTY_AWS, null, null, headers, request), holderSimulation);
        }
    }

    private List<Message> generateErrorMessages(final int statusCode) {
        Message message = new Message();
        message.setCode(String.valueOf(statusCode));
        message.setType(ErrorSeverity.ERROR);

        return Collections.singletonList(message);
    }

    @Override
    protected void evaluateResponse(final AWSImagesResponse response, int statusCode) {
        evaluateMessagesResponse(generateErrorMessages(statusCode), SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE, statusCode);
    }
}
