package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.canonic.Proposal;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTECKB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS1KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS2KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTSCKB89;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateCardsProposalMapper {

    FormatoKTECKB89 mapIn(DTOIntProposal dtoIn);

    Proposal mapOut(FormatoKTSCKB89 formatOutput);

    Proposal mapOut2(FormatoKTS1KB89 formatOutput, Proposal dtoOut);

    Proposal mapOut3(FormatoKTS2KB89 formatOutput, Proposal dtoOut);
}