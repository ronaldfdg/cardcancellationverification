package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mpg1.*;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
public interface ITxGetCardMapper {

    FormatoMPMENG1 mapIn(DTOIntCard dtoIn);

    Card mapOut1(FormatoMPMS1G1 formatOutput, Card card);

    Card mapOut2(FormatoMPMS2G1 formatOutput, Card card);

    Card mapOut3(FormatoMPMS3G1 formatOutput, Card card);

    Card mapOut4(FormatoMPMS4G1 formatOutput, Card card);

    Card mapOut5(FormatoMPMS5G1 formatOutput, Card dtoOut);

    Card mapOut6(FormatoMPMS6G1 formatOutput, Card dtoOut);

    Card mapOut7(FormatoMPMS7G1 formatOutput, Card dtoOut);

    Card mapOut8(FormatoMPMS8G1 formatOutput, Card card);
}
