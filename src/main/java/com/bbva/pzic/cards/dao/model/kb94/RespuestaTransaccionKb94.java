package com.bbva.pzic.cards.dao.model.kb94;

import com.bbva.jee.arq.spring.core.host.*;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * Bean de respuesta para la transacci&oacute;n <code>KB94</code>
 *
 * @author Arquitectura Spring BBVA
 * @see PeticionTransaccionKb94
 */
@RespuestaTransaccion
@Multiformato(formatos = {FormatoKTSCKB94.class})
@RooJavaBean
@RooSerializable
public class RespuestaTransaccionKb94 implements MensajeMultiparte {

    /**
     * <p>Cabecera <code>serviceResponse</code></p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
    private String codigoRetorno;

    /**
     * <p>Cabecera <code>processControl</code></p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_CONTROL)
    private String codigoControl;

    /**
     * <p>Cuerpo del mensaje de respuesta multiparte</p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
