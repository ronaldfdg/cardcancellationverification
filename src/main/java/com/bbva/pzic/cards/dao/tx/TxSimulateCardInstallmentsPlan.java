package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanSimulationData;
import com.bbva.pzic.cards.dao.model.mpws.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxSimulateCardInstallmentsPlanMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Component("txSimulateCardInstallmentsPlan")
public class TxSimulateCardInstallmentsPlan
        extends DoubleOutputFormat<DTOIntCardInstallmentsPlan, FormatoMPM0WSE, InstallmentsPlanSimulationData, FormatoMPM0WSC, FormatoMPM0DET> {

    @Resource(name = "txSimulateCardInstallmentsPlanMapper")
    private ITxSimulateCardInstallmentsPlanMapper mapper;

    @Autowired
    public TxSimulateCardInstallmentsPlan(@Qualifier("transaccionMpws") InvocadorTransaccion<PeticionTransaccionMpws, RespuestaTransaccionMpws> transaction) {
        super(transaction, PeticionTransaccionMpws::new, InstallmentsPlanSimulationData::new, FormatoMPM0WSC.class, FormatoMPM0DET.class);
    }

    @Override
    protected FormatoMPM0WSE mapInput(DTOIntCardInstallmentsPlan dtoIntCardInstallmentsPlan) {
        return mapper.mapIn(dtoIntCardInstallmentsPlan);
    }

    @Override
    protected InstallmentsPlanSimulationData mapFirstOutputFormat(FormatoMPM0WSC formatoMPM0WSC, DTOIntCardInstallmentsPlan dtoIntCardInstallmentsPlan, InstallmentsPlanSimulationData installmentsPlanSimulationData) {
        return mapper.mapOut1(formatoMPM0WSC, dtoIntCardInstallmentsPlan);
    }

    @Override
    protected InstallmentsPlanSimulationData mapSecondOutputFormat(FormatoMPM0DET formatoMPM0DET, DTOIntCardInstallmentsPlan dtoIntCardInstallmentsPlan, InstallmentsPlanSimulationData installmentsPlanSimulationData) {
        return mapper.mapOut2(formatoMPM0DET, installmentsPlanSimulationData);
    }
}
