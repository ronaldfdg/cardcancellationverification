package com.bbva.pzic.cards.dao.model.mpl5;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPL5</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpl5</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpl5</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPL5.txt
 * MPL5LISTADO DE CONTRATOS RELACIONADOS  MP        MP2CMPL5PBDMPPO MPMENL5             MPL5  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-11-19XP93683 2018-11-1916.02.10XP93683 2018-11-19-15.07.21.995202XP93683 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENL5.txt
 * MPMENL5 �LISTADO DE CONTRATOS RELAC.   �F�01�00019�01�00001�IDETARJ�NUMERO DE TARJETA   �A�019�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1L5.txt
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�01�00001�IDRELA �IDENT. TJ. Y CONTRAT�A�025�0�S�        �
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�02�00026�NUCOREL�NUM. CONTRATO RELAC.�A�020�0�S�        �
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�03�00046�NUMPROD�NUM. PRODUCTO RELAC.�A�020�0�S�        �
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�04�00066�TNUMID �IDENT. TIPO DE CONTR�A�001�0�S�        �
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�05�00067�TNUMDES�DES. TIPO NUM. CONTR�A�022�0�S�        �
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�06�00089�PRODID �IDEN.PRODUCTO CONTRA�A�002�0�S�        �
 * MPMS1L5 �LISTADO CONTRATOS RELAC.      �X�07�00110�07�00091�PRODDES�DESC. PRODUCTO RELAC�A�020�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPL5.txt
 * MPL5MPMS1L5 MPNCS1L5MP2CMPL51S                             XP93683 2018-11-19-16.26.43.579147XP93683 2018-11-19-16.26.43.579185
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpl5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPL5",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpl5.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENL5.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpl5 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}