package com.bbva.pzic.cards.dao.model.mpdc;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPDC</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpdc</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpdc</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPDC.txt
 * MPDCRETIRO DE SALDO ACREEDOR           MP        MP2CDC00PBDMPPO MCEMDC0             MPDC  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-11-09XP92314 2018-11-1611.31.52XP92314 2018-11-09-16.17.26.844515XP92314 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MCEMDC0.txt
 * MCEMDC0 �RETIRO DE SALDO ACREEDOR      �F�04�00044�01�00001�NROTARJ�NUMERO DE TARJETA   �A�019�0�R�        �
 * MCEMDC0 �RETIRO DE SALDO ACREEDOR      �F�04�00044�02�00020�TIPREEM�TIPO DE REEMBOLSO   �A�002�0�R�        �
 * MCEMDC0 �RETIRO DE SALDO ACREEDOR      �F�04�00044�03�00022�DIVISA �DIVISA SALDO ACRE.  �A�003�0�R�        �
 * MCEMDC0 �RETIRO DE SALDO ACREEDOR      �F�04�00044�04�00025�NCUENTA�NRO CTA CLIENTE     �A�020�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MCRMDC0.txt
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�01�00001�IDOPER �IDENT. OPERACION    �A�007�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�02�00008�DESOPER�DESCRIP. OPERACION  �A�030�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�03�00038�TIPREEM�TIPO REEMBOLSO      �A�002�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�04�00040�ISALACR�SALDO ACREEDOR      �N�017�2�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�05�00057�DIVSACR�DIVISA SALDO ACREED.�A�003�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�06�00060�FECOPER�FECHA OPERACION     �A�010�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�07�00070�HORAOPE�HORA OPERACION      �A�008�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�08�00078�FECFACT�FECHA CONTABLE      �A�010�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�09�00088�HORAFAC�HORA CONTABLE       �A�008�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�10�00096�NCUENTA�NRO CUENTA          �A�020�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�11�00116�ISALCTA�MONTO ABONADO EN CTA�N�017�2�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�12�00133�DIVSACT�DIVISA DE CTA ABONO �A�003�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�13�00136�TIPCAM �TIPO DE CAMBIO      �N�007�4�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�14�00143�TIPPREC�TIPO PRECIO ASOC. TC�A�001�0�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�15�00144�TOTIMP �MONTO TOTAL IMP     �N�017�2�S�        �
 * MCRMDC0 �RETIRO DE SALDO ACREEDOR      �X�16�00163�16�00161�DIVTOTI�DIVISA MONTO TOT IMP�A�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MCRMDC1.txt
 * MCRMDC1 �DEPOSITO SALDO ACREEDOR       �X�03�00022�01�00001�TIPOIMP�CODIGO TIPO IMPUESTO�A�002�0�S�        �
 * MCRMDC1 �DEPOSITO SALDO ACREEDOR       �X�03�00022�02�00003�MONIMP �IMPORTE A PAGAR     �N�017�2�S�        �
 * MCRMDC1 �DEPOSITO SALDO ACREEDOR       �X�03�00022�03�00020�DIVIMP �DIVISA IMPORTE IMPTO�A�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPDC.txt
 * MPDCMCRMDC0 MPNCDCR0MP2CDC001S                             XP92314 2018-11-12-14.59.13.196219XP92314 2018-11-12-14.59.13.196846
 * MPDCMCRMDC1 MPNCDCR1MP2CDC001S                             XP92314 2018-11-12-15.00.23.359583XP92314 2018-11-27-14.25.29.658057
</pre></code>
 *
 * @see RespuestaTransaccionMpdc
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPDC",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpdc.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMCEMDC0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpdc implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}