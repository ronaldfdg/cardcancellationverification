package com.bbva.pzic.cards.dao.model.mpde;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPDE</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpde</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpde</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPDE.D1181123.txt
 * MPDEDATOS EFECTIVO PLUS                MP        MP2CMPEPPBDMPPO MPMENDE             MPDE  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-10-30XP93687 2018-11-2116.57.38XP93687 2018-10-30-10.32.49.379960XP93687 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENDE.D1181123.txt
 * MPMENDE �DATOS EFECTIVO PLUS           �F�01�00019�01�00001�NROTARJ�NUMERO DE TARJETA   �A�019�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1DE.D1181122.txt
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�01�00001�IDENDEP�ID EFECTIVO PLUS    �A�010�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�02�00011�DESCDEP�DESC DE EP CONSUMIDO�A�025�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�03�00036�MONEDEP�MONEDA DEL EFEC PLUS�A�003�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�04�00039�DEUDMES�CUOTA DEL MES       �N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�05�00056�CAPCUOM�CAPITAL CUOTA DE MES�N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�06�00073�INTCUOM�INTERES CUOTA DE MES�N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�07�00090�FECVENC�FECHA VENC CUOTA MES�A�010�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�08�00100�MONTOTF�MONTO TOT PAG ACTUAL�N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�09�00117�CAPABON�CAPITAL ABONADO     �N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�10�00134�INTABON�INTERES ABONADO     �N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�11�00151�TOTCANC�MONTO TOTAL DE CUOTA�N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�12�00168�TCUOPEN�TOTAL CUOTA PENDIENT�N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�13�00185�INTERES�INTERES TOTAL CUO PE�N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�14�00202�TOTDEUD�TOTAL DEUDA         �N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�15�00219�MTOSOLI�MONTO SOLICITADO    �N�017�2�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�16�00236�FECINIC�FECHA INICIO        �A�010�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�17�00246�HORINIC�HORA DESEMBOLSO     �A�008�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�18�00254�FECCONT�FECHA CONTABLE      �A�010�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�19�00264�HORCONT�HORA CONTABLE       �A�008�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�20�00272�FECHFIN�FECHA ULTIMA CUOTA  �A�010�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�21�00282�NUMCUOT�NUMERO DE CUOTAS TOT�N�004�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�22�00286�CUOPAGA�CUOTAS PAGADAS      �N�004�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�23�00290�CUOPDTE�CUOTAS PENDIENTES   �N�004�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�24�00294�CUOVNPA�CUOTAS NOTIFICADAS  �N�004�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�25�00298�FRECCUO�FRECUENCIA DE CUOTAS�A�001�0�S�        �
 * MPMS1DE �DATOS DE EFECTIVO PLUS        �X�26�00319�26�00299�IDEOFEA�IDE DE OFERTA       �A�021�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2DE.D1181123.txt
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�01�00001�IDETTAS�ID TIPO DE TASA     �A�005�0�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�02�00006�DESTTAS�DESCRIPCION TASA    �A�030�0�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�03�00036�FECCTAS�FECHA CALCULO TASA  �A�010�0�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�04�00046�IDMODTA�ID MODALIDAD        �A�001�0�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�05�00047�NOMMOD �NOMBRE DE MODALIDAD �A�015�0�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�06�00062�VALTASA�VALOR NUMERICO TASA �N�011�4�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�07�00073�INTDEUT�INTERES TOTAL       �N�017�2�S�        �
 * MPMS2DE �DATOS DE EFECTIVO PLUS        �X�08�00092�08�00090�MONTASA�MONEDA DE MODALIDAD �A�003�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPDE.D1181123.txt
 * MPDEMPMS1DE MPNCSEP1MP2CMPEP1S                             XP93687 2018-11-21-15.08.21.079296XP93687 2018-11-21-15.08.21.079329
 * MPDEMPMS2DE MPNCSEP2MP2CMPEP1S                             XP93687 2018-11-21-16.21.53.291625XP93687 2018-11-21-16.21.53.291651
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpde
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPDE",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpde.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENDE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpde implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}