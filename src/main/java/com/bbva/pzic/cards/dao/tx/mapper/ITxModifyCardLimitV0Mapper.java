package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMENG7;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMS1G7;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
public interface ITxModifyCardLimitV0Mapper {

    FormatoMPMENG7 mapIn(InputModifyCardLimit dtoIn);

    Limit mapOut(FormatoMPMS1G7 formatOutput);
}
