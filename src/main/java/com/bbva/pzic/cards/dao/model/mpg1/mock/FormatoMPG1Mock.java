package com.bbva.pzic.cards.dao.model.mpg1.mock;

import com.bbva.pzic.cards.dao.model.mpg1.*;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 17/10/2017.
 *
 * @author Entelgy
 */
public class FormatoMPG1Mock {

    private static final FormatoMPG1Mock INSTANCE = new FormatoMPG1Mock();
    private ObjectMapperHelper objectMapper;

    private FormatoMPG1Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatoMPG1Mock getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS1G1 getFormatoMPMS1G1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS1G1.json"), FormatoMPMS1G1.class);
    }

    public FormatoMPMS1G1 getFormatoMPMS1G1Emtpy() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS1G1Empty.json"), FormatoMPMS1G1.class);
    }

    public List<FormatoMPMS2G1> getFormatoMPMS2G1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS2G1.json"), new TypeReference<List<FormatoMPMS2G1>>() {
        });
    }

    public List<FormatoMPMS3G1> getFormatoMPMS3G1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS3G1.json"), new TypeReference<List<FormatoMPMS3G1>>() {
        });
    }

    public List<FormatoMPMS4G1> getFormatoMPMS4G1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS4G1.json"), new TypeReference<List<FormatoMPMS4G1>>() {
        });
    }

    public List<FormatoMPMS4G1> getFormatoMPMS4G1Empty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS4G1Empty.json"), new TypeReference<List<FormatoMPMS4G1>>() {
        });
    }

    public FormatoMPMS5G1 getFormatoMPMS5G1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS5G1.json"), FormatoMPMS5G1.class);
    }

    public List<FormatoMPMS6G1> getFormatoMPMS6G1List() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS6G1List.json"), new TypeReference<List<FormatoMPMS6G1>>() {
        });
    }

    public List<FormatoMPMS7G1> getFormatoMPMS7G1List() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS7G1List.json"), new TypeReference<List<FormatoMPMS7G1>>() {
        });
    }

    public FormatoMPMS8G1 getFormatoMPMS8G1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpg1/mock/formatoMPMS8G1.json"), FormatoMPMS8G1.class);
    }
}
