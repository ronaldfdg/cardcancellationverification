package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PPCUTC01</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPpcutc01_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPpcutc01_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PPCUTC01-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PPCUTC01&quot; application=&quot;PPCU&quot; version=&quot;01&quot; country=&quot;PE&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto name=&quot;EntityIn&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.CardDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;bankIdentificationNumber&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;supportContractType&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;cardAgreement&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;holderName&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;list name=&quot;currencies&quot; order=&quot;6&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;currency&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.CurrencyDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;isMajor&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;7&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.GrantedCreditDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;12&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;relatedContracts&quot; order=&quot;8&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;relatedContract&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.RelatedContractDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contractId&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;relationType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.RelationTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ProductDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;productType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ProductTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;deliveries&quot; order=&quot;9&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;delivery&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.DeliveryDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ServiceTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ContactDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.AddressDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.DestinationDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.BranchDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;participants&quot; order=&quot;10&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;participant&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ParticipantDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;personType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;participantType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ParticipantTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;legalPersonType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.LegalPersonTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;memberships&quot; order=&quot;11&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;membership&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.MembershipDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;images&quot; order=&quot;15&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;image&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ImageDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.CardTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ProductDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;13&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.PhysicalSupportDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;14&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;contractingBranch&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ContractingBranchDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;16&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;managementBranch&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ManagementBranchDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;17&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;contractingBusinessAgent&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ContractingBusinessAgentDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;18&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;marketBusinessAgent&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.MarketBusinessAgentDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;19&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;paymentMethod&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.PaymentMethodDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;20&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;frecuency&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.FrecuencyDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;daysOfMonth&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.DaysOfMonthDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;day&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto name=&quot;EntityOut&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.CardDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;22&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;expirationDate&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;holderName&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;openingDate&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;supportContractType&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;cardAgreement&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;24&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;25&quot; name=&quot;bankIdentificationNumber&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;list name=&quot;currencies&quot; order=&quot;6&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;currency&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.CurrencyDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;isMajor&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;7&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.GrantedCreditDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;relatedContracts&quot; order=&quot;8&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;relatedContract&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.RelatedContractDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;contractId&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;relationType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.RelationTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ProductDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;productType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ProductTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;deliveries&quot; order=&quot;9&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;delivery&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.DeliveryDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ServiceTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ContactDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.AddressDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.DestinationDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.BranchDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;memberships&quot; order=&quot;16&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;membership&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.MembershipDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ProductDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;brandAssociation&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.BrandAssociationDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;11&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.PhysicalSupportDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;status&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.StatusDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;13&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;contractingBranch&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ContractingBranchDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;18&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;managementBranch&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ManagementBranchDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;19&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;contractingBusinessAgent&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.ContractingBusinessAgentDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;20&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;marketBusinessAgent&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.MarketBusinessAgentDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;0&quot; order=&quot;21&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;numberType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.NumberTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;22&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.cards.dto.CardTypeDTO&quot; artifactId=&quot;PPCUCC01&quot; mandatory=&quot;1&quot; order=&quot;23&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Servicio de alta de tarjetas&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPpcutc01_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PPCUTC01",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPpcutc01_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPpcutc01_1 {
		
		/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}