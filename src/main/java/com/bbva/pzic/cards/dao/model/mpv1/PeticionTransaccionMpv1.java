package com.bbva.pzic.cards.dao.model.mpv1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPV1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpv1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpv1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPV1.D1170213.txt
 * MPV1VINCULACION DE TARJ/CONTR          MP        MP2CMPV1PBDMPPO MPM0V1E             MPV1  NS0000CNNNNN    SSTN    C   NNNNSNNN  NN                2017-02-09XP88366 2017-02-1309.13.53XP88366 2017-02-09-14.51.22.256117XP88366 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0V1E.D1170213.txt
 * MPM0V1E �ENT. VINCULACION DE TARJ/CONTR�F�02�00036�01�00001�NUMTARJ�PAN TARJ.VINCULAR   �A�016�0�R�        �
 * MPM0V1E �ENT. VINCULACION DE TARJ/CONTR�F�02�00036�02�00017�NUCOREL�NRO. CONTRATO REL   �A�020�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0V1S.D1170213.txt
 * MPM0V1S �SAL. VINCULACION DE TARJ/CONTR�X�04�00111�01�00001�IDCOREL�ID CONTRATO RELA.   �A�060�0�S�        �
 * MPM0V1S �SAL. VINCULACION DE TARJ/CONTR�X�04�00111�02�00061�NUCOREL�NRO. CONTRATO RELA. �A�020�0�S�        �
 * MPM0V1S �SAL. VINCULACION DE TARJ/CONTR�X�04�00111�03�00081�IDTCORE�ID.TIP NRO CONTR.REL�A�001�0�S�        �
 * MPM0V1S �SAL. VINCULACION DE TARJ/CONTR�X�04�00111�04�00082�DETCORE�DESC.TIPO NRO CONTR.�A�030�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPV1.D1170213.txt
 * MPV1MPM0V1S MPECV1S MP2CMPV11S                             XP88366 2017-02-09-15.29.49.185482XP88366 2017-02-09-15.29.49.187681
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpv1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPV1",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpv1.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0V1E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpv1 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}