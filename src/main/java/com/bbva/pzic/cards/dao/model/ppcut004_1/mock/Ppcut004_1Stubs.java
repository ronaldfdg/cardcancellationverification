package com.bbva.pzic.cards.dao.model.ppcut004_1.mock;

import com.bbva.pzic.cards.dao.model.ppcut004_1.RespuestaTransaccionPpcut004_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 27/12/2019.
 *
 * @author Entelgy
 */
public final class Ppcut004_1Stubs {

    private static final Ppcut004_1Stubs INSTANCE = new Ppcut004_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Ppcut004_1Stubs() {
    }

    public static Ppcut004_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPpcut004_1 getProposal() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/ppcut004_1/mock/respuestaTransaccionPpcut004_1.json"), RespuestaTransaccionPpcut004_1.class);
    }
}
