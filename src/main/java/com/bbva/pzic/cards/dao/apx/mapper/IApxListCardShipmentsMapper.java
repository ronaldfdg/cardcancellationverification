package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputListCardShipments;
import com.bbva.pzic.cards.dao.model.pecpt001_1.PeticionTransaccionPecpt001_1;
import com.bbva.pzic.cards.dao.model.pecpt001_1.RespuestaTransaccionPecpt001_1;
import com.bbva.pzic.cards.facade.v0.dto.Shipments;

import java.util.List;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public interface IApxListCardShipmentsMapper {

    PeticionTransaccionPecpt001_1 mapIn(InputListCardShipments input);

    List<Shipments> mapOut(RespuestaTransaccionPecpt001_1 response);
}
