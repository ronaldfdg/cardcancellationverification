package com.bbva.pzic.cards.dao.model.ppcut002_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PPCUT002</code>
 * 
 * @see PeticionTransaccionPpcut002_1
 * @see RespuestaTransaccionPpcut002_1
 */
@Component
public class TransaccionPpcut002_1 implements InvocadorTransaccion<PeticionTransaccionPpcut002_1,RespuestaTransaccionPpcut002_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPpcut002_1 invocar(PeticionTransaccionPpcut002_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut002_1.class, RespuestaTransaccionPpcut002_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPpcut002_1 invocarCache(PeticionTransaccionPpcut002_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut002_1.class, RespuestaTransaccionPpcut002_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}