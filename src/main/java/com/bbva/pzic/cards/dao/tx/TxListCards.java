package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.dao.model.mpl1.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Component("txListCards")
public class TxListCards
        extends DoubleOutputFormat<DTOIntListCards, FormatoMPMENL1, DTOOutListCards, FormatoMPMS1L1, FormatoMPMS2L1> {

    private static final Log LOG = LogFactory.getLog(TxListCards.class);

    @Autowired
    private ITxListCardsMapper txListCardsMapper;

    public TxListCards(@Qualifier("transaccionMpl1") InvocadorTransaccion<PeticionTransaccionMpl1, RespuestaTransaccionMpl1> transaction) {
        super(transaction, PeticionTransaccionMpl1::new, DTOOutListCards::new, FormatoMPMS1L1.class, FormatoMPMS2L1.class);
    }

    @Override
    protected FormatoMPMENL1 mapInput(DTOIntListCards dtoIntListCards) {
        LOG.info(" ... call TxListCards.mapDtoInToRequestFormat ... ");
        return txListCardsMapper.mapIn(dtoIntListCards);
    }

    @Override
    protected DTOOutListCards mapFirstOutputFormat(FormatoMPMS1L1 formatoMPMS1L1, DTOIntListCards dtoIntListCards, DTOOutListCards dtoOut) {
        LOG.info(" ... call TxListCards.mapResponseFormatToDtoOut ... ");
        return txListCardsMapper.mapOut(formatoMPMS1L1, null, dtoOut);
    }

    @Override
    protected DTOOutListCards mapSecondOutputFormat(FormatoMPMS2L1 formatoMPMS2L1, DTOIntListCards dtoIntListCards, DTOOutListCards dtoOut) {
        LOG.info(" ... call TxListCards.mapResponseFormatToDtoOut3 ... ");
        return txListCardsMapper.mapOut2(formatoMPMS2L1, null, dtoOut);
    }
}
