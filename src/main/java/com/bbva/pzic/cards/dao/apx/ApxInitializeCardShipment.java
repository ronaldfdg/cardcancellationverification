package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputInitializeCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxInitializeCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt003_1.PeticionTransaccionPecpt003_1;
import com.bbva.pzic.cards.dao.model.pecpt003_1.RespuestaTransaccionPecpt003_1;
import com.bbva.pzic.cards.facade.v0.dto.InitializeShipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApxInitializeCardShipment {

    @Autowired
    private IApxInitializeCardShipmentMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionPecpt003_1, RespuestaTransaccionPecpt003_1> transaccion;

    public InitializeShipment invoke(final InputInitializeCardShipment input) {
        PeticionTransaccionPecpt003_1 request = mapper.mapIn(input);
        RespuestaTransaccionPecpt003_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
