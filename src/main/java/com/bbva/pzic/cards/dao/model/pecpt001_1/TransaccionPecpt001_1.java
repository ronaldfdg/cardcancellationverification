package com.bbva.pzic.cards.dao.model.pecpt001_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PECPT001</code>
 * 
 * @see PeticionTransaccionPecpt001_1
 * @see RespuestaTransaccionPecpt001_1
 */
@Component
public class TransaccionPecpt001_1 implements InvocadorTransaccion<PeticionTransaccionPecpt001_1,RespuestaTransaccionPecpt001_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPecpt001_1 invocar(PeticionTransaccionPecpt001_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt001_1.class, RespuestaTransaccionPecpt001_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPecpt001_1 invocarCache(PeticionTransaccionPecpt001_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt001_1.class, RespuestaTransaccionPecpt001_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}