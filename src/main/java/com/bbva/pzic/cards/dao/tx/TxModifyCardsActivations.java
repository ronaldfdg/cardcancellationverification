package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpaf.FormatoMPM0AFE;
import com.bbva.pzic.cards.dao.model.mpaf.PeticionTransaccionMpaf;
import com.bbva.pzic.cards.dao.model.mpaf.RespuestaTransaccionMpaf;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardsActivationsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.NoneOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 24/06/2020.
 *
 * @author Entelgy
 */
@Component("txModifyCardsActivations")
public class TxModifyCardsActivations extends NoneOutputFormat<DTOIntActivation, FormatoMPM0AFE> {

    @Resource(name = "txModifyCardsActivationsMapper")
    private ITxModifyCardsActivationsMapper mapper;

    @Autowired
    public TxModifyCardsActivations(@Qualifier("transaccionMpaf") InvocadorTransaccion<PeticionTransaccionMpaf, RespuestaTransaccionMpaf> transaction) {
        super(transaction, PeticionTransaccionMpaf::new);
    }

    @Override
    protected FormatoMPM0AFE mapInput(DTOIntActivation dtoIntActivation) {
        return mapper.mapIn(dtoIntActivation);
    }
}
