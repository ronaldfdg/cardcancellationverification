package com.bbva.pzic.cards.dao.model.mpl2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPL2</code>
 *
 * @see PeticionTransaccionMpl2
 * @see RespuestaTransaccionMpl2
 */
@Component
public class TransaccionMpl2 implements InvocadorTransaccion<PeticionTransaccionMpl2,RespuestaTransaccionMpl2> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpl2 invocar(PeticionTransaccionMpl2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpl2.class, RespuestaTransaccionMpl2.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpl2 invocarCache(PeticionTransaccionMpl2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpl2.class, RespuestaTransaccionMpl2.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
