package com.bbva.pzic.cards.dao.model.pecpt005_1;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityIn</code>, utilizado por la clase <code>PeticionTransaccionPecpt005_1</code></p>
 *
 * @see PeticionTransaccionPecpt005_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityin {

	/**
	 * <p>Campo <code>biometricId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "biometricId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 36, signo = true, obligatorio = true)
	private String biometricid;

	/**
	 * <p>Campo <code>shipmentAddress</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "shipmentAddress", tipo = TipoCampo.DTO)
	private Shipmentaddress shipmentaddress;

	/**
	 * <p>Campo <code>currentAddress</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "currentAddress", tipo = TipoCampo.DTO)
	private Currentaddress currentaddress;

}
