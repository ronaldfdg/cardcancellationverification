package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulation;
import com.bbva.pzic.cards.dao.model.productofferdetail.*;
import com.bbva.pzic.cards.dao.rest.mapper.IRestCreateCardsOfferSimulateMapper;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Enums.CARDS_RATES_MODE;
import static org.apache.commons.lang.StringUtils.isEmpty;

@Component
public class RestCreateCardsOfferSimulateMapper implements IRestCreateCardsOfferSimulateMapper {

    @Autowired
    private Translator translator;

    @Autowired
    private InputHeaderManager inputHeaderManager;

    @Override
    public HashMap<String, String> mapInQueryParams(final DTOIntDetailSimulation input) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put(CardHolderSimulationQueryParams.ID_PROPUESTA, input.getOfferId());
        queryParams.put(CardHolderSimulationQueryParams.CUSTOMER_ID, input.getClientId());
        if (input.getProduct() != null) {
            queryParams.put(CardHolderSimulationQueryParams.BIN, input.getProduct().getId());

            if (input.getProduct().getSubproduct() != null) {
                queryParams.put(CardHolderSimulationQueryParams.ID_SUB_PRODUCTO, input.getProduct().getSubproduct().getId());
            }
        }
        return queryParams;
    }

    @Override
    public HolderSimulation mapOut(final CardHolderSimulationResponse output) {
        if (output == null || output.getData() == null) {
            return null;
        }
        CardHolderSimulation modelCardHolderSimulation = output.getData();
        com.bbva.pzic.cards.canonic.CardHolderSimulation cardHolderSimulation = new com.bbva.pzic.cards.canonic.CardHolderSimulation();
        cardHolderSimulation.setCardType(mapOutCardType(modelCardHolderSimulation));
        cardHolderSimulation.setProduct(mapOutProduct(modelCardHolderSimulation));
        cardHolderSimulation.setMinimumAmount(mapOutMinimumAmount(modelCardHolderSimulation));
        cardHolderSimulation.setMaximumAmount(mapOutMaximumAmount(modelCardHolderSimulation));
        cardHolderSimulation.setSuggestedAmount(mapOutSuggestedAmount(modelCardHolderSimulation));
        cardHolderSimulation.setFees(mapOutFees(modelCardHolderSimulation));
        cardHolderSimulation.setRates(mapOutRates(modelCardHolderSimulation.getTasa()));
        cardHolderSimulation.setAdditionalProducts(mapOutAdditionalProducts(modelCardHolderSimulation));
        cardHolderSimulation.setBenefits(mapOutBenefits(modelCardHolderSimulation.getBeneficio()));
        cardHolderSimulation.setMembership(mapOutMembership(modelCardHolderSimulation));
        cardHolderSimulation.setPaymentPeriod(mapOutPaymentPeriod(modelCardHolderSimulation));
        cardHolderSimulation.setPaymentMethod(mapOutPaymentMethod(modelCardHolderSimulation));
        cardHolderSimulation.setImage(mapOutImage(modelCardHolderSimulation));
        HolderSimulation holderSimulation = new HolderSimulation();
        holderSimulation.setDetails(cardHolderSimulation);
        return holderSimulation;
    }


    private CardType mapOutCardType(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getIdClasificacion()) && isEmpty(modelHolderSimulation.getNombreClasificacion())) {
            return null;
        }
        CardType cardType = new CardType();
        cardType.setId(translator.translateBackendEnumValueStrictly(Enums.CARDS_CARD_TYPE_ID, modelHolderSimulation.getIdClasificacion()));
        cardType.setName(modelHolderSimulation.getNombreClasificacion());
        return cardType;
    }

    private Product mapOutProduct(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getBin())
                && isEmpty(modelHolderSimulation.getNombreMarca())
                && isEmpty(modelHolderSimulation.getIdSubproducto())
                && isEmpty(modelHolderSimulation.getNombreSubproducto())
                && isEmpty(modelHolderSimulation.getDescSubproducto())) {
            return null;
        }
        Product product = new Product();
        product.setId(modelHolderSimulation.getBin());
        product.setName(modelHolderSimulation.getNombreMarca());
        product.setSubproduct(mapOutSubProduct(modelHolderSimulation));
        return product;
    }

    private SubProduct mapOutSubProduct(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getIdSubproducto())
                && isEmpty(modelHolderSimulation.getNombreSubproducto())
                && isEmpty(modelHolderSimulation.getDescSubproducto())) {
            return null;
        }
        SubProduct subProduct = new SubProduct();
        subProduct.setId(modelHolderSimulation.getIdSubproducto());
        subProduct.setName(modelHolderSimulation.getNombreSubproducto());
        subProduct.setDescription(modelHolderSimulation.getDescSubproducto());
        return subProduct;
    }

    private MinimumAmount mapOutMinimumAmount(final CardHolderSimulation modelHolderSimulation) {
        if (modelHolderSimulation.getLimiteMinLinea() == null && isEmpty(modelHolderSimulation.getMonedaLimiteMin())) {
            return null;
        }
        MinimumAmount minimumAmount = new MinimumAmount();
        minimumAmount.setAmount(modelHolderSimulation.getLimiteMinLinea());
        minimumAmount.setCurrency(modelHolderSimulation.getMonedaLimiteMin());
        return minimumAmount;
    }

    private MaximumAmount mapOutMaximumAmount(final CardHolderSimulation modelHolderSimulation) {
        if (modelHolderSimulation.getLimiteMaxLinea() == null && isEmpty(modelHolderSimulation.getMonedaLimiteMax())) {
            return null;
        }
        MaximumAmount maximumAmount = new MaximumAmount();
        maximumAmount.setAmount(modelHolderSimulation.getLimiteMaxLinea());
        maximumAmount.setCurrency(modelHolderSimulation.getMonedaLimiteMax());
        return maximumAmount;
    }

    private SuggestedAmount mapOutSuggestedAmount(final CardHolderSimulation modelHolderSimulation) {
        if (modelHolderSimulation.getLineaSugerida() == null && isEmpty(modelHolderSimulation.getMonedaLineaSug())) {
            return null;
        }
        SuggestedAmount suggestedAmount = new SuggestedAmount();
        suggestedAmount.setAmount(modelHolderSimulation.getLineaSugerida());
        suggestedAmount.setCurrency(modelHolderSimulation.getMonedaLineaSug());
        return suggestedAmount;
    }

    private Fees mapOutFees(final CardHolderSimulation modelHolderSimulation) {
        Fees fees = new Fees();
        fees.setItemizeFees(mapOutItemizeFees(modelHolderSimulation));
        return fees;
    }

    private List<Rate> mapOutRates(final List<Tasa> tasa) {
        if (CollectionUtils.isEmpty(tasa)) {
            return null;
        }
        return tasa.stream().filter(Objects::nonNull).map(this::mapOutRate).collect(Collectors.toList());
    }

    private Rate mapOutRate(final Tasa tasa) {
        if (tasa == null) {
            return null;
        }
        Rate rate = new Rate();
        rate.setRateType(mapOutRateType(tasa));
        rate.setCalculationDate(tasa.getFecha());
        rate.setMode(mapOutRateMode(tasa));
        rate.setUnit(mapOutUnit(tasa.getValor()));
        return rate;
    }


    private RateType mapOutRateType(final Tasa tasa) {
        if (isEmpty(tasa.getId()) && isEmpty(tasa.getNombre())) {
            return null;
        }
        RateType rateType = new RateType();
        rateType.setId(tasa.getId());
        rateType.setName(tasa.getNombre());
        return rateType;
    }

    private Mode mapOutRateMode(final Tasa tasa) {
        if (isEmpty(tasa.getModalidad()) && isEmpty(tasa.getDescModalidad())) {
            return null;
        }
        Mode mode = new Mode();
        mode.setId(translator.translateBackendEnumValueStrictly(CARDS_RATES_MODE, tasa.getModalidad()));
        mode.setName(tasa.getDescModalidad());
        return mode;
    }

    private Percentage mapOutUnit(final BigDecimal valor) {
        if (valor == null) {
            return null;
        }
        Percentage percentage = new Percentage();
        percentage.setPercentage(valor);
        return percentage;
    }

    private List<ItemizeFee> mapOutItemizeFees(final CardHolderSimulation modelHolderSimulation) {
        List<ItemizeFee> itemizeFeeList = new ArrayList<>();
        itemizeFeeList.add(mapOutItemizeFee(
                modelHolderSimulation.getIdComisionMemb(),
                modelHolderSimulation.getNombreMembresia(),
                modelHolderSimulation.getIdModAplicMemb(),
                modelHolderSimulation.getModAplicacionMemb(),
                modelHolderSimulation.getMontoMembr(),
                modelHolderSimulation.getMonedaMembr()
        ));
        itemizeFeeList.add(mapOutItemizeFee(
                modelHolderSimulation.getIdComisionEstadoCuenta(),
                modelHolderSimulation.getNombreMembresiaEstadoCuenta(),
                modelHolderSimulation.getIdModAplicMembEstadoCuenta(),
                modelHolderSimulation.getModAplicacionMembEstadoCuenta(),
                modelHolderSimulation.getMontoMembrEstadoCuenta(),
                modelHolderSimulation.getMonedaMembrEstadoCuenta()
        ));
        return itemizeFeeList;
    }

    private ItemizeFee mapOutItemizeFee(final String feeType, final String name,
                                        final String modeId, final String modeName,
                                        final BigDecimal itemizeFeeUnitAmount, final String itemizeFeeUnitCurrency) {
        ItemizeFee itemizeFee = new ItemizeFee();
        itemizeFee.setFeeType(translator.translateBackendEnumValueStrictly(Enums.CARDS_ITEMIZE_FEES_FEE_TYPE, feeType));
        itemizeFee.setName(name);
        itemizeFee.setMode(mapOutItemizeFeeMode(modeId, modeName));
        itemizeFee.setItemizeFeeUnit(createItemizeFeeUnitInstance(itemizeFeeUnitAmount, itemizeFeeUnitCurrency));
        return itemizeFee;
    }


    private Mode mapOutItemizeFeeMode(final String modeId, final String modeName) {
        if (isEmpty(modeId) && isEmpty(modeName)) {
            return null;
        }
        Mode mode = new Mode();
        mode.setId(translator.translateBackendEnumValueStrictly(Enums.CARDS_ITEMIZE_FEES_MODE, modeId));
        mode.setName(modeName);
        return mode;
    }

    private ItemizeFeeUnit createItemizeFeeUnitInstance(final BigDecimal amount, final String currency) {
        if (amount == null && isEmpty(currency)) {
            return null;
        }
        ItemizeFeeUnit itemizeFeeUnit = new ItemizeFeeUnit();
        itemizeFeeUnit.setAmount(amount);
        itemizeFeeUnit.setCurrency(currency);
        return itemizeFeeUnit;
    }

    private List<AdditionalProducts> mapOutAdditionalProducts(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getTipoSeguro())
                && modelHolderSimulation.getMontoSeguro() == null
                && isEmpty(modelHolderSimulation.getDivisaMontoSeguro())) {
            return null;
        }
        AdditionalProducts additionalProducts = new AdditionalProducts();
        additionalProducts.setProductType(translator.translateBackendEnumValueStrictly(Enums.CARDS_OFFERS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE,
                modelHolderSimulation.getTipoSeguro()));
        additionalProducts.setAmount(modelHolderSimulation.getMontoSeguro());
        additionalProducts.setCurrency(modelHolderSimulation.getDivisaMontoSeguro());
        List<AdditionalProducts> additionalProductsList = new ArrayList<>();
        additionalProductsList.add(additionalProducts);
        return additionalProductsList;
    }

    private List<Benefit> mapOutBenefits(final List<Beneficio> beneficio) {
        if (CollectionUtils.isEmpty(beneficio)) {
            return null;
        }
        return beneficio.stream().filter(Objects::nonNull).map(this::mapOutBenefit).collect(Collectors.toList());
    }

    private Benefit mapOutBenefit(final Beneficio beneficio) {
        if (beneficio == null) {
            return null;
        }
        Benefit benefit = new Benefit();
        benefit.setId(beneficio.getId());
        benefit.setDescription(beneficio.getDescripcion());
        return benefit;
    }

    private Membership mapOutMembership(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getIdProgBenef()) && isEmpty(modelHolderSimulation.getNombreProgBenef())) {
            return null;
        }
        Membership membership = new Membership();
        membership.setId(modelHolderSimulation.getIdProgBenef());
        membership.setDescription(modelHolderSimulation.getNombreProgBenef());
        return membership;
    }

    private PaymentPeriod mapOutPaymentPeriod(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getIdPeriodicidad())
                && isEmpty(modelHolderSimulation.getNombrePeriodicidad())
                && CollectionUtils.isEmpty(modelHolderSimulation.getDiapago())) {
            return null;
        }
        PaymentPeriod paymentPeriod = new PaymentPeriod();
        paymentPeriod.setId(translator.translateBackendEnumValueStrictly(Enums.CARDS_PERIOD_ID, modelHolderSimulation.getIdPeriodicidad()));
        paymentPeriod.setName(modelHolderSimulation.getNombrePeriodicidad());
        paymentPeriod.setEndDays(mapOutPatmentMethodEndDays(modelHolderSimulation.getDiapago()));
        return paymentPeriod;
    }

    private List<EndDay> mapOutPatmentMethodEndDays(final List<DiaPago> diapagoList) {
        if (CollectionUtils.isEmpty(diapagoList)) {
            return null;
        }
        List<EndDay> endDayList = new ArrayList<>();
        if (diapagoList.get(0) != null) {
            EndDay endDay = new EndDay();
            endDay.setDay(diapagoList.get(0).getValor());
            endDayList.add(endDay);
        }
        if (diapagoList.size() > 1 && diapagoList.get(1) != null) {
            EndDay endDay = new EndDay();
            endDay.setDay(diapagoList.get(1).getValor());
            endDayList.add(endDay);
        }
        return endDayList;
    }

    private PaymentMethod mapOutPaymentMethod(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getIdTipoDeuda())
                && isEmpty(modelHolderSimulation.getNombreTipoDeuda())
                && isEmpty(modelHolderSimulation.getIdPeriodicidad())
                && isEmpty(modelHolderSimulation.getNombrePeriodicidad())) {
            return null;
        }
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(translator.translateBackendEnumValueStrictly(Enums.CARDS_PAYMENT_METHOD_ID, modelHolderSimulation.getIdTipoDeuda()));
        paymentMethod.setName(modelHolderSimulation.getNombreTipoDeuda());
        paymentMethod.setPeriod(mapOutPaymentMethodPeriod(modelHolderSimulation));
        return paymentMethod;
    }

    private Period mapOutPaymentMethodPeriod(final CardHolderSimulation modelHolderSimulation) {
        if (isEmpty(modelHolderSimulation.getIdPeriodicidad()) && isEmpty(modelHolderSimulation.getNombrePeriodicidad())) {
            return null;
        }
        Period period = new Period();
        period.setId(translator.translateBackendEnumValueStrictly(Enums.CARDS_PERIOD_ID, modelHolderSimulation.getIdPeriodicidad()));
        period.setName(modelHolderSimulation.getNombrePeriodicidad());
        return period;
    }

    private Image mapOutImage(final CardHolderSimulation cardHolderSimulation) {
        if (cardHolderSimulation.getIdImagen() == null
                && cardHolderSimulation.getNombreImagen() == null
                && cardHolderSimulation.getUrlImagen() == null) {
            return null;
        }
        Image image = new Image();
        image.setId(cardHolderSimulation.getIdImagen());
        image.setName(cardHolderSimulation.getNombreImagen());
        image.setUrl(BusinessServiceUtil.buildUrl(inputHeaderManager.getHeader(Constants.HEADER_USER_AGENT), cardHolderSimulation.getUrlImagen()));
        return image;
    }
}
