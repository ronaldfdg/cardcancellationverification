package com.bbva.pzic.cards.dao.model.mprt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPRT</code>
 *
 * @see PeticionTransaccionMprt
 * @see RespuestaTransaccionMprt
 */
@Component
public class TransaccionMprt implements InvocadorTransaccion<PeticionTransaccionMprt,RespuestaTransaccionMprt> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMprt invocar(PeticionTransaccionMprt transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMprt.class, RespuestaTransaccionMprt.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMprt invocarCache(PeticionTransaccionMprt transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMprt.class, RespuestaTransaccionMprt.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
