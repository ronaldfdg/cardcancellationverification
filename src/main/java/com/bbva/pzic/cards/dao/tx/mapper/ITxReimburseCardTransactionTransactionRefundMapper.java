package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMEN6J;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMS16J;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;

public interface ITxReimburseCardTransactionTransactionRefundMapper {

    FormatoMPMEN6J mapIn(InputReimburseCardTransactionTransactionRefund input);

    ReimburseCardTransactionTransactionRefund mapOut(FormatoMPMS16J formatOutput);
}
