package com.bbva.pzic.cards.dao.model.mpg7.mock;

import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMS1G7;

import java.util.Date;

/**
 * Created on 23/01/2019.
 *
 * @author Entelgy
 */
public final class FormatsMpg7Mock {

    private static final FormatsMpg7Mock INSTANCE = new FormatsMpg7Mock();

    private FormatsMpg7Mock() {
    }

    public static FormatsMpg7Mock getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS1G7 getFormatoMPMS1G7() {
        FormatoMPMS1G7 formatoMPMS1G7 = new FormatoMPMS1G7();
        formatoMPMS1G7.setFecoper(new Date());
        formatoMPMS1G7.setHoroper("18:15:30");
        return formatoMPMS1G7;
    }
}
