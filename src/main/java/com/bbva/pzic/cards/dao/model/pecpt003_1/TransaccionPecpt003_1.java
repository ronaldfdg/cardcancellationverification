package com.bbva.pzic.cards.dao.model.pecpt003_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PECPT003</code>
 * 
 * @see PeticionTransaccionPecpt003_1
 * @see RespuestaTransaccionPecpt003_1
 */
@Component
public class TransaccionPecpt003_1 implements InvocadorTransaccion<PeticionTransaccionPecpt003_1,RespuestaTransaccionPecpt003_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPecpt003_1 invocar(PeticionTransaccionPecpt003_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt003_1.class, RespuestaTransaccionPecpt003_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPecpt003_1 invocarCache(PeticionTransaccionPecpt003_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt003_1.class, RespuestaTransaccionPecpt003_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}