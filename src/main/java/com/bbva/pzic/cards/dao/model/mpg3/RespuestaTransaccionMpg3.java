package com.bbva.pzic.cards.dao.model.mpg3;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;

/**
 * Bean de respuesta para la transacci&oacute;n <code>MPG3</code>
 *
 * @author Arquitectura Spring BBVA
 * @see PeticionTransaccionMpg3
 */
@RespuestaTransaccion
@Multiformato(formatos = {FormatoMPMS1G3.class})
@RooJavaBean
@RooSerializable
public class RespuestaTransaccionMpg3 implements MensajeMultiparte {

    /**
     * <p>Cabecera <code>serviceResponse</code></p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
    private String codigoRetorno;

    /**
     * <p>Cabecera <code>processControl</code></p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_CONTROL)
    private String codigoControl;

    /**
     * <p>Cuerpo del mensaje de respuesta multiparte</p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
