package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPFMCE1;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPNCCS1;

import java.util.List;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
public interface ITxGetCardConditionsMapper {

    FormatoMPFMCE1 mapIn(InputGetCardConditions dtoIn);

    List<Condition> mapOut(FormatoMPNCCS1 formatOutput, List<Condition> conditionList);
}