package com.bbva.pzic.cards.dao.model.pecpt003_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.pecpt003_1.PeticionTransaccionPecpt003_1;
import com.bbva.pzic.cards.dao.model.pecpt003_1.RespuestaTransaccionPecpt003_1;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>PECPT001</code>
 *
 * @see PeticionTransaccionPecpt003_1
 * @see RespuestaTransaccionPecpt003_1
 */
@Component("transaccionPecpt003_1")
public class TransaccionPecpt003_1Mock implements InvocadorTransaccion<PeticionTransaccionPecpt003_1, RespuestaTransaccionPecpt003_1> {

    public static final String TEST_EMPTY = "999";

    @Override
    public RespuestaTransaccionPecpt003_1 invocar(final PeticionTransaccionPecpt003_1 transaccion) {
        try {
            if (TEST_EMPTY.equalsIgnoreCase(transaccion.getEntityin().getExternalcode())) {
                return new RespuestaTransaccionPecpt003_1();
            }
            return Pecpt003_1Stubs.getInstance().buildRespuestaTransaccionPecpt003_1();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPecpt003_1 invocarCache(PeticionTransaccionPecpt003_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
