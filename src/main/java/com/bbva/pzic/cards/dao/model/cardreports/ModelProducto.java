package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelProducto {

    private String id;

    private String nombre;

    private ModelSubproducto subproducto;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ModelSubproducto getSubproducto() {
        return subproducto;
    }

    public void setSubproducto(ModelSubproducto subProducto) {
        this.subproducto = subProducto;
    }
}
