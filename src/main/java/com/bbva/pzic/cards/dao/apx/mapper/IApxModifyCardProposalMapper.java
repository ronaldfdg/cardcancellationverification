package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputModifyCardProposal;
import com.bbva.pzic.cards.dao.model.ppcut003_1.PeticionTransaccionPpcut003_1;
import com.bbva.pzic.cards.dao.model.ppcut003_1.RespuestaTransaccionPpcut003_1;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public interface IApxModifyCardProposalMapper {

    PeticionTransaccionPpcut003_1 mapIn(InputModifyCardProposal input);

    Proposal mapOut(RespuestaTransaccionPpcut003_1 response);
}
