package com.bbva.pzic.cards.dao.model.pecpt003_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PECPT003</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPecpt003_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPecpt003_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PECPT003-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PECPT003&quot; application=&quot;PECP&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto name=&quot;EntityIn&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.CardShipmentDTO&quot; artifactId=&quot;PECPC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;externalCode&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;participant&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ParticipantDTO&quot; artifactId=&quot;PECPC001&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;identityDocument&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.IdentityDocumentDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;documentNumber&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;documentType&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.DocumentTypeDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;card&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.CardDTO&quot; artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot;
 * order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;cardAgreement&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;referenceNumber&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;shippingCompany&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ShippingCompanyDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;shippingDevice&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ShippingDeviceDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;macAddress&quot; type=&quot;String&quot; size=&quot;17&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;application&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ApplicationDTO&quot; artifactId=&quot;PECPC001&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;32&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;version&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Método encargado para inicializar el proceso de entrega.&amp;#xD;
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPecpt003_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PECPT003",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPecpt003_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPecpt003_1 {
		
		/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}