package com.bbva.pzic.cards.dao.model.kb89.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTECKB89;
import com.bbva.pzic.cards.dao.model.kb89.PeticionTransaccionKb89;
import com.bbva.pzic.cards.dao.model.kb89.RespuestaTransaccionKb89;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 12/12/2018.
 *
 * @author Entelgy
 */
@Component("transaccionKb89")
public class TransaccionKb89Mock implements InvocadorTransaccion<PeticionTransaccionKb89, RespuestaTransaccionKb89> {

    public static final String TEST_EMPTY = "6666";
    public static final String TEST_NO_RESPONSE = "9999";

    @Override
    public RespuestaTransaccionKb89 invocar(PeticionTransaccionKb89 peticion) throws ExcepcionTransaccion {
        RespuestaTransaccionKb89 response = new RespuestaTransaccionKb89();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoKTECKB89 format = peticion.getCuerpo().getParte(FormatoKTECKB89.class);
        String idprodu = format.getIdprodu();
        try {
            switch (idprodu) {
                case TEST_NO_RESPONSE:
                    return response;
                case TEST_EMPTY:
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTSCKB89().get(1)));
                    return response;
                default:
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTSCKB89().get(0)));
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTS1KB89().get(0)));
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTS1KB89().get(1)));
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTS1KB89().get(2)));
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTS2KB89().get(0)));
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTS2KB89().get(1)));
                    response.getCuerpo().getPartes().add(this.buildData(FormatsKb89Mock.getInstance().getListFormatoKTS2KB89().get(2)));
                    return response;
            }
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionKb89 invocarCache(PeticionTransaccionKb89 peticionTransaccionKb89) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(Object object) {
        CopySalida copy = new CopySalida();
        copy.setCopy(object);
        return copy;
    }

}
