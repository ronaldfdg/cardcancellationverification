package com.bbva.pzic.cards.dao;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ICardsDAOV1 {

    List<Proposal> listCardProposals(InputListCardProposals input);

    ProposalCard createCardProposal(InputCreateCardProposal input);

    Proposal updateCardProposal(InputUpdateCardProposal input);

    Proposal modifyCardProposal(InputModifyCardProposal input);

    CardPost createCard(InputCreateCard input);

    Delivery createCardDelivery(DTOIntDelivery dtoInt);

    List<SecurityData> getCardSecurityData(InputGetCardSecurityData input);

    void modifyCardsActivations(DTOIntActivation dtoInt);

    CancellationVerification getCardCancellationVerification(InputGetCardCancellationVerification input);

    SimulateTransactionRefund simulateCardTransactionTransactionRefund(InputSimulateCardTransactionTransactionRefund input);

    ReimburseCardTransactionTransactionRefund reimburseCardTransactionTransactionRefund(InputReimburseCardTransactionTransactionRefund input);

    FinancialStatement getCardFinancialStatement(InputGetCardFinancialStatement input);
}
