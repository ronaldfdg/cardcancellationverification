package com.bbva.pzic.cards.dao.model.mpb1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.dao.model.mpb1.PeticionTransaccionMpb1;
import com.bbva.pzic.cards.dao.model.mpb1.RespuestaTransaccionMpb1;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPB1</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpb1.PeticionTransaccionMpb1
 * @see com.bbva.pzic.cards.dao.model.mpb1.RespuestaTransaccionMpb1
 */
@Component("transaccionMpb1")
public class TransaccionMpb1Mock implements InvocadorTransaccion<PeticionTransaccionMpb1, RespuestaTransaccionMpb1> {

    @Override
    public RespuestaTransaccionMpb1 invocar(PeticionTransaccionMpb1 transaccion) {
        RespuestaTransaccionMpb1 response = new RespuestaTransaccionMpb1();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");
        return response;
    }

    @Override
    public RespuestaTransaccionMpb1 invocarCache(PeticionTransaccionMpb1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
