package com.bbva.pzic.cards.dao.model.mpl5;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS1L5</code> de la transacci&oacute;n <code>MPL5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1L5")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1L5 {

	/**
	 * <p>Campo <code>IDRELA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDRELA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	@DatoAuditable(omitir = true)
	private String idrela;

	/**
	 * <p>Campo <code>NUCOREL</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUCOREL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nucorel;

	/**
	 * <p>Campo <code>NUMPROD</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NUMPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	@DatoAuditable(omitir = true)
	private String numprod;

	/**
	 * <p>Campo <code>TNUMID</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "TNUMID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tnumid;

	/**
	 * <p>Campo <code>TNUMDES</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "TNUMDES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
	private String tnumdes;

	/**
	 * <p>Campo <code>PRODID</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "PRODID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String prodid;

	/**
	 * <p>Campo <code>PRODDES</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "PRODDES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String proddes;

}