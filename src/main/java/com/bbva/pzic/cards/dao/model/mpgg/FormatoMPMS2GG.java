package com.bbva.pzic.cards.dao.model.mpgg;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS2GG</code> de la transacci&oacute;n <code>MPGG</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS2GG")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS2GG {

	/**
	 * <p>Campo <code>FECPAGO</code>, &iacute;ndice: <code>1</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 1, nombre = "FECPAGO", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecpago;

	/**
	 * <p>Campo <code>MONTPAG</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 2, nombre = "MONTPAG", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
	private BigDecimal montpag;

}