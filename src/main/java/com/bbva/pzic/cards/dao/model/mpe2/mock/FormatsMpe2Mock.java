package com.bbva.pzic.cards.dao.model.mpe2.mock;

import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS2E2;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
public final class FormatsMpe2Mock {

    private static final FormatsMpe2Mock INSTANCE = new FormatsMpe2Mock();
    private ObjectMapperHelper objectMapper;

    private FormatsMpe2Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatsMpe2Mock getInstance() {
        return INSTANCE;
    }

    public List<FormatoMPMS1E2> getFormatoMPMS1E2() throws IOException {
        return objectMapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "com/bbva/pzic/cards/dao/model/mpe2/mock/formatoMPMS1E2.json"),
                new TypeReference<List<FormatoMPMS1E2>>() {
                });
    }

    public FormatoMPMS2E2 getFormatoMPMS2E2(String idPagin) throws IOException {
        String name;
        if (TransaccionMpe2Mock.TEST_NULL_IDPAGIN.equalsIgnoreCase(idPagin)) {
            name = "com/bbva/pzic/cards/dao/model/mpe2/mock/formatoMPMS2E2_idpagin_null.json";
        } else if (TransaccionMpe2Mock.TEST_NULL_TAMPAGI.equalsIgnoreCase(idPagin)) {
            name = "com/bbva/pzic/cards/dao/model/mpe2/mock/formatoMPMS2E2_tampagi_null.json";
        } else {
            name = "com/bbva/pzic/cards/dao/model/mpe2/mock/formatoMPMS2E2.json";
        }
        return objectMapper
                .readValue(
                        Thread.currentThread()
                                .getContextClassLoader()
                                .getResourceAsStream(
                                        name),
                        FormatoMPMS2E2.class);
    }

}