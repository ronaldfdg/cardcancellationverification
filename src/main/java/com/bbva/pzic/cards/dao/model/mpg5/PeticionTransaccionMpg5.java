package com.bbva.pzic.cards.dao.model.mpg5;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPG5</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpg5</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpg5</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPG5.D1180125.txt
 * MPG5SITUACION FINANCIERA TARJETA       MP        MP2CMPG5PBDMPPO MPMENG5             MPG5  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-10-20P014658 2017-10-2311.14.37P014658 2017-10-20-16.06.35.973823P014658 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENG5.D1180125.txt
 * MPMENG5 �SITUACION FINANCIERA TARJETA  �F�01�00019�01�00001�IDETARJ�NRO.TARJETA CLIENTE �A�019�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1G5.D1180125.txt
 * MPMS1G5 �SITUACION FINANCIERA TARJETA  �X�03�00031�01�00001�IDFORPA�IDE. FORMA DE PAGO  �A�001�0�S�        �
 * MPMS1G5 �SITUACION FINANCIERA TARJETA  �X�03�00031�02�00002�DSFORPA�DES. FORMA DE PAGO  �A�020�0�S�        �
 * MPMS1G5 �SITUACION FINANCIERA TARJETA  �X�03�00031�03�00022�FECPAGO�FEC. PAGO           �A�010�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2G5.D1180125.txt
 * MPMS2G5 �SITUACION FINANCIERA TARJETA  �X�04�00044�01�00001�IDFORPA�ID. TIPO DE PAGO    �A�001�0�S�        �
 * MPMS2G5 �SITUACION FINANCIERA TARJETA  �X�04�00044�02�00002�DSFORPA�DES. TIPO DE PAGO   �A�025�0�S�        �
 * MPMS2G5 �SITUACION FINANCIERA TARJETA  �X�04�00044�03�00027�IMPAMIS�IMPORTE PAGO        �S�015�2�S�        �
 * MPMS2G5 �SITUACION FINANCIERA TARJETA  �X�04�00044�04�00042�MOPAMIS�MONEDA IMPORTE PAGO �A�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPG5.D1180125.txt
 * MPG5MPMS1G5 MPNCS1G5MP2CMPG51S                             P014658 2017-10-23-10.44.58.885846P014658 2017-10-23-10.44.58.885928
 * MPG5MPMS2G5 MPNCS2G5MP2CMPG51S                             P014658 2017-10-23-10.45.15.226707P014658 2017-10-23-10.45.15.227562
</pre></code>
 *
 * @see RespuestaTransaccionMpg5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPG5",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpg5.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENG5.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpg5 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}