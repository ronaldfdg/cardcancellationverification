package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCardProposal;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut001_1.PeticionTransaccionPpcut001_1;
import com.bbva.pzic.cards.dao.model.ppcut001_1.RespuestaTransaccionPpcut001_1;
import com.bbva.pzic.cards.facade.v1.dto.ProposalCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@Component
public class ApxCreateCardProposal {

    @Autowired
    private IApxCreateCardProposalMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPpcut001_1, RespuestaTransaccionPpcut001_1> transaccion;

    public ProposalCard invoke(final InputCreateCardProposal input) {
        PeticionTransaccionPpcut001_1 request = mapper.mapIn(input);
        RespuestaTransaccionPpcut001_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
