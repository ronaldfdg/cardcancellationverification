package com.bbva.pzic.cards.dao.model.pecpt004_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>location</code>, utilizado por la clase <code>Shipmentaddress</code></p>
 * 
 * @see Shipmentaddress
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Location {
	
	/**
	 * <p>Campo <code>formattedAddress</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "formattedAddress", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 200, signo = true)
	private String formattedaddress;
	
	/**
	 * <p>Campo <code>locationTypes</code>, &iacute;ndice: <code>2</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 2, nombre = "locationTypes", tipo = TipoCampo.LIST)
	private List<Locationtypes> locationtypes;
	
	/**
	 * <p>Campo <code>addressComponents</code>, &iacute;ndice: <code>3</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 3, nombre = "addressComponents", tipo = TipoCampo.LIST)
	private List<Addresscomponents> addresscomponents;
	
	/**
	 * <p>Campo <code>geolocation</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "geolocation", tipo = TipoCampo.DTO)
	private Geolocation geolocation;
	
}