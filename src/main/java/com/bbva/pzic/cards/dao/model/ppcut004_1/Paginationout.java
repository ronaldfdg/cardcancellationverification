package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>paginationOut</code>, utilizado por la clase <code>RespuestaTransaccionPpcut004_1</code></p>
 * 
 * @see RespuestaTransaccionPpcut004_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Paginationout {
	
	/**
	 * <p>Campo <code>page</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "page", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true)
	private Integer page;
	
	/**
	 * <p>Campo <code>totalPages</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "totalPages", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true)
	private Integer totalpages;
	
	/**
	 * <p>Campo <code>totalElements</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "totalElements", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true)
	private Integer totalelements;
	
	/**
	 * <p>Campo <code>pageSize</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 4, nombre = "pageSize", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true)
	private Integer pagesize;
	
	/**
	 * <p>Campo <code>links</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "links", tipo = TipoCampo.DTO)
	private Links links;
	
}