package com.bbva.pzic.cards.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;
import com.bbva.pzic.cards.dao.rest.RestListImagesCovers;
import com.bbva.pzic.cards.dao.rest.mock.stubs.AWSImagesStubs;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.routine.commons.utils.RESTUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 04/11/2019.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestListImagesCoversMock extends RestListImagesCovers {

    @Override
    protected AWSImagesResponse connect(final String urlPropertyValue,
                                        final Map<String, String> pathParams,
                                        final HashMap<String, String> queryParams,
                                        final Map<String, String> headers,
                                        final AWSImagesRequest entityPayload) {

        try {
            return AWSImagesStubs.INSTANCE.buildAWSImagesResponse();

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);

        } catch (BusinessServiceException e) {
            if (RESTUtils.isTimeoutException(e)) {
                return null;
            } else {
                throw e;
            }
        }
    }
}
