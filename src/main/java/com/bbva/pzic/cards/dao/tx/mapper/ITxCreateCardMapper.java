package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.CardData;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;

/**
 * Created on 30/12/2016.
 *
 * @author Entelgy
 */
public interface ITxCreateCardMapper {

    FormatoMPME0W1 mapIn(DTOIntCard dtoIn);

    CardData mapOut(FormatoMPMS1W1 formatOutput);
}
