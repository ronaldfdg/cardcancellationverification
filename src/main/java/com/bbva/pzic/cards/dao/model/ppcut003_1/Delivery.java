package com.bbva.pzic.cards.dao.model.ppcut003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>delivery</code>, utilizado por la clase <code>Deliveries</code></p>
 * 
 * @see Deliveries
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Delivery {

	/**
	 * <p>Campo <code>serviceType</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "serviceType", tipo = TipoCampo.DTO)
	private Servicetype servicetype;
	
	/**
	 * <p>Campo <code>specificContact</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "contact", tipo = TipoCampo.DTO)
	private Specificcontact specificcontact;
	
	/**
	 * <p>Campo <code>address</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "address", tipo = TipoCampo.DTO)
	private Address address;
	
	/**
	 * <p>Campo <code>destination</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "destination", tipo = TipoCampo.DTO)
	private Destination destination;

	@Campo(indice = 5, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 18, signo = true, obligatorio = true)
	private String id;

}