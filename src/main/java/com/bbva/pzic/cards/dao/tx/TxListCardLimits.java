package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Limits;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMENGL;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMS1GL;
import com.bbva.pzic.cards.dao.model.mpgl.PeticionTransaccionMpgl;
import com.bbva.pzic.cards.dao.model.mpgl.RespuestaTransaccionMpgl;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardLimitsMapperV0;
import com.bbva.pzic.cards.util.tx.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/05/2020.
 *
 * @author Entelgy
 */
@Tx
public class TxListCardLimits extends SingleOutputFormat<DTOIntCard, FormatoMPMENGL, List<Limits>, FormatoMPMS1GL> {

    private static final Log LOG = LogFactory.getLog(TxListCardLimits.class);

    @Resource(name = "txListCardLimitsMapperV0")
    private ITxListCardLimitsMapperV0 txListCardLimitsMapperV0;

    public TxListCardLimits(@Qualifier("transaccionMpgl") InvocadorTransaccion<PeticionTransaccionMpgl, RespuestaTransaccionMpgl> transaction) {
        super(transaction, PeticionTransaccionMpgl::new, ArrayList::new, FormatoMPMS1GL.class);
    }

    @Override
    protected FormatoMPMENGL mapInput(DTOIntCard dtoIntCard) {
        LOG.info("... called method TxListCardLimits.mapInput ...");
        return txListCardLimitsMapperV0.mapIn(dtoIntCard);
    }

    @Override
    protected List<Limits> mapFirstOutputFormat(FormatoMPMS1GL formatoMPMS1GL, DTOIntCard dtoIntCard, List<Limits> limits) {
        LOG.info("... called method TxListCardLimits.mapFirstOutputFormat ...");
        return txListCardLimitsMapperV0.mapOut(formatoMPMS1GL, limits);
    }
}
