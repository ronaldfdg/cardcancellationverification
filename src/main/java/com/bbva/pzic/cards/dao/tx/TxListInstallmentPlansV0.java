package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.dao.model.mpgg.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListInstallmentPlansV0Mapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.ThreefoldOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created on 8/02/2018.
 *
 * @author Entelgy
 */
@Component("txListInstallmentPlansV0")
public class TxListInstallmentPlansV0
        extends ThreefoldOutputFormat<InputListInstallmentPlans, FormatoMPMENGG, DTOInstallmentsPlanList, FormatoMPMS1GG, FormatoMPMS2GG, FormatoMPMS3GG> {

    @Autowired
    private ITxListInstallmentPlansV0Mapper mapper;

    @Autowired
    public TxListInstallmentPlansV0(@Qualifier("transaccionMpgg") InvocadorTransaccion<PeticionTransaccionMpgg, RespuestaTransaccionMpgg> transaction) {
        super(transaction, PeticionTransaccionMpgg::new, DTOInstallmentsPlanList::new, FormatoMPMS1GG.class, FormatoMPMS2GG.class, FormatoMPMS3GG.class);
    }

    /**
     * @param dtoIn DTO con los datos de entrada
     * @return formato de entrada
     */
    @Override
    protected FormatoMPMENGG mapInput(
            final InputListInstallmentPlans dtoIn) {
        return mapper.mapIn(dtoIn);
    }

    /**
     * @param formatOutput formato 1 de salida a mapear
     * @param dtoIn        DTO de entrada
     * @param dtoOut       DTO de salida
     * @return DTO mapeado
     */
    @Override
    protected DTOInstallmentsPlanList mapFirstOutputFormat(
            final FormatoMPMS1GG formatOutput,
            final InputListInstallmentPlans dtoIn,
            final DTOInstallmentsPlanList dtoOut) {
        return mapper.mapOut(formatOutput, dtoOut);
    }

    /**
     * @param formatOutput Formato 2 de salida a mapear
     * @param dtoIn        DTO de entrada
     * @param dtoOut       DTO de salida
     * @return DTO mapeado
     */
    @Override
    protected DTOInstallmentsPlanList mapSecondOutputFormat(
            final FormatoMPMS2GG formatOutput,
            final InputListInstallmentPlans dtoIn,
            final DTOInstallmentsPlanList dtoOut) {
        return mapper.mapOut2(formatOutput, dtoOut);
    }

    /**
     * @param formatOutput Formato 3 de salida a mapear
     * @param dtoIn        DTO de entrada
     * @param dtoOut       DTO de salida
     * @return DTO mapeado
     */
    @Override
    protected DTOInstallmentsPlanList mapThirdOutputFormat(
            final FormatoMPMS3GG formatOutput,
            final InputListInstallmentPlans dtoIn,
            final DTOInstallmentsPlanList dtoOut) {
        return mapper.mapOut3(formatOutput, dtoOut);
    }
}
