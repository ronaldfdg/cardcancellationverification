package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMENGG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS1GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS2GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS3GG;

/**
 * Created on 8/02/2018.
 *
 * @author Entelgy
 */
public interface ITxListInstallmentPlansV0Mapper {


    FormatoMPMENGG mapIn(InputListInstallmentPlans dtoIn);

    DTOInstallmentsPlanList mapOut(FormatoMPMS1GG formatOutput,
                                   DTOInstallmentsPlanList dtoOut);

    DTOInstallmentsPlanList mapOut2(FormatoMPMS2GG formatOutput,
                                    DTOInstallmentsPlanList dtoOut);

    DTOInstallmentsPlanList mapOut3(FormatoMPMS3GG formatOutput,
                                    DTOInstallmentsPlanList dtoOut);
}
