package com.bbva.pzic.cards.dao.model.ppcut003_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PPCUT003</code>
 * 
 * @see PeticionTransaccionPpcut003_1
 * @see RespuestaTransaccionPpcut003_1
 */
@Component
public class TransaccionPpcut003_1 implements InvocadorTransaccion<PeticionTransaccionPpcut003_1,RespuestaTransaccionPpcut003_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPpcut003_1 invocar(PeticionTransaccionPpcut003_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut003_1.class, RespuestaTransaccionPpcut003_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPpcut003_1 invocarCache(PeticionTransaccionPpcut003_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut003_1.class, RespuestaTransaccionPpcut003_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}