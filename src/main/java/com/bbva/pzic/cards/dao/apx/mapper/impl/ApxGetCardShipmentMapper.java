package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxGetCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt004_1.Status;
import com.bbva.pzic.cards.dao.model.pecpt004_1.*;
import com.bbva.pzic.cards.facade.v0.dto.Address;
import com.bbva.pzic.cards.facade.v0.dto.Destination;
import com.bbva.pzic.cards.facade.v0.dto.Geolocation;
import com.bbva.pzic.cards.facade.v0.dto.Location;
import com.bbva.pzic.cards.facade.v0.dto.Participant;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.SPECIFIC;
import static com.bbva.pzic.cards.util.Constants.STORED;

/**
 * Created on 18/02/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ApxGetCardShipmentMapper implements IApxGetCardShipmentMapper {

    private static final Log LOG = LogFactory.getLog(ApxGetCardShipmentMapper.class);

    @Override
    public PeticionTransaccionPecpt004_1 mapIn(final InputGetCardShipment input) {
        PeticionTransaccionPecpt004_1 peticion = new PeticionTransaccionPecpt004_1();
        peticion.setShipmentid(input.getShipmentId());
        peticion.setExpand(input.getExpand());
        return peticion;
    }

    @Override
    public Shipment mapOut(final RespuestaTransaccionPecpt004_1 response) {
        if (response == null || response.getEntityout() == null) {
            return null;
        }

        Shipment shipment = new Shipment();
        shipment.setId(response.getEntityout().getId());
        shipment.setStatus(mapOutStatus(response.getEntityout().getStatus()));
        shipment.setTerm(response.getEntityout().getTerm());
        shipment.setParticipant(mapOutParticipant(response.getEntityout().getParticipant()));
        shipment.setShippingCompany(mapOutShippingCompany(response.getEntityout().getShippingcompany()));
        shipment.setExternalCode(response.getEntityout().getExternalcode());
        shipment.setAddresses(mapOutAddresses(response.getEntityout().getAddresses()));
        return shipment;
    }

    private Participant mapOutParticipant(final com.bbva.pzic.cards.dao.model.pecpt004_1.Participant participant) {
        if (participant == null) {
            return null;
        }

        Participant result = new Participant();
        result.setFullName(participant.getFullname());
        result.setIdentityDocument(mapOutIdentityDocument(participant.getIdentitydocument()));
        result.setCards(mapOutParticipantCards(participant.getCards()));
        return result;
    }

    private List<ParticipantCards> mapOutParticipantCards(final List<com.bbva.pzic.cards.dao.model.pecpt004_1.Cards> cards) {
        if (CollectionUtils.isEmpty(cards)) {
            return null;
        }

        return cards.stream()
                .filter(Objects::nonNull)
                .map(this::mapOutParticipantCard)
                .collect(Collectors.toList());
    }

    private IdentityDocument mapOutIdentityDocument(final Identitydocument identitydocument) {
        if (identitydocument == null) {
            return null;
        }

        IdentityDocument result = new IdentityDocument();
        result.setDocumentNumber(identitydocument.getDocumentnumber());
        result.setDocumentType(mapOutDocumentType(identitydocument.getDocumenttype()));
        return result;
    }

    private DocumentType mapOutDocumentType(final Documenttype documenttype) {
        if (documenttype == null) {
            return null;
        }

        DocumentType result = new DocumentType();
        result.setId(documenttype.getId());
        result.setDescription(documenttype.getDescription());
        return result;
    }

    private ParticipantCards mapOutParticipantCard(final Cards cards) {
        if (cards.getCard() == null) {
            return null;
        }

        ParticipantCards participantCards = new ParticipantCards();
        participantCards.setCardAgreement(cards.getCard().getCardagreement());
        participantCards.setReferenceNumber(cards.getCard().getReferencenumber());
        return participantCards;
    }

    private ShipmentStatus mapOutStatus(final Status status) {
        if (status == null) {
            return null;
        }

        ShipmentStatus shipmentStatus = new ShipmentStatus();
        shipmentStatus.setId(status.getId());
        shipmentStatus.setDescription(status.getDescription());
        return shipmentStatus;
    }

    private ShippingCompany mapOutShippingCompany(final Shippingcompany shippingcompany) {
        if (shippingcompany == null) {
            return null;
        }

        ShippingCompany shippingCompany = new ShippingCompany();
        shippingCompany.setId(shippingcompany.getId());
        shippingCompany.setName(shippingcompany.getName());
        return shippingCompany;
    }

    private List<Address> mapOutAddresses(final List<Addresses> addresses) {
        if (CollectionUtils.isEmpty(addresses)) {
            return null;
        }

        List<Address> result = addresses.stream()
                .filter(Objects::nonNull)
                .map(this::mapOutAddress)
                .collect(Collectors.toList());

        result.removeAll(Collections.singleton(null));
        return result;
    }

    private Address mapOutAddress(final Addresses addresses) {
        if (addresses.getAddress() == null || addresses.getAddress().getShipmentaddress() == null ||
                StringUtils.isEmpty(addresses.getAddress().getShipmentaddress().getAddresstype())) {
            return null;
        }

        Address result = new Address();
        if (SPECIFIC.equalsIgnoreCase(addresses.getAddress().getShipmentaddress().getAddresstype())) {
            result.setShipmentAddress(mapOutSpecificShipmentAddress(addresses.getAddress().getShipmentaddress()));
        } else if (STORED.equalsIgnoreCase(addresses.getAddress().getShipmentaddress().getAddresstype())) {
            result.setShipmentAddress(mapOutStoredShipmentAddress(addresses.getAddress().getShipmentaddress()));
        }

        return result;
    }

    private ShipmentAddress mapOutStoredShipmentAddress(final Shipmentaddress address) {
        ShipmentAddress storedShipmentAddress = new ShipmentAddress();
        storedShipmentAddress.setAddressType(address.getAddresstype());
        storedShipmentAddress.setId(address.getId());
        storedShipmentAddress.setDestination(mapOutDestination(address.getDestination()));
        return storedShipmentAddress;
    }

    private Destination mapOutDestination(final com.bbva.pzic.cards.dao.model.pecpt004_1.Destination destination) {
        if (destination == null) {
            return null;
        }

        Destination result = new Destination();
        result.setId(destination.getId());
        result.setName(destination.getName());
        return result;
    }

    private ShipmentAddress mapOutSpecificShipmentAddress(final Shipmentaddress address) {
        ShipmentAddress specificShipmentAddress = new ShipmentAddress();
        specificShipmentAddress.setAddressType(address.getAddresstype());
        specificShipmentAddress.setId(address.getId());
        specificShipmentAddress.setLocation(mapOutLocation(address.getLocation()));
        return specificShipmentAddress;
    }

    private Location mapOutLocation(final com.bbva.pzic.cards.dao.model.pecpt004_1.Location location) {
        if (location == null) {
            return null;
        }

        Location result = new Location();
        result.setFormattedAddress(location.getFormattedaddress());
        result.setLocationTypes(mapOutLocationTypes(location.getLocationtypes()));
        result.setAddressComponents(mapOutAddressComponents(location.getAddresscomponents()));
        result.setGeolocation(mapOutGeolocation(location.getGeolocation()));
        return result;
    }

    private List<String> mapOutLocationTypes(final List<Locationtypes> locationtypes) {
        if (CollectionUtils.isEmpty(locationtypes)) {
            return null;
        }

        return locationtypes.stream().filter(Objects::nonNull).map(Locationtypes::getLocationtype).collect(Collectors.toList());
    }

    private List<AddressComponent> mapOutAddressComponents(final List<Addresscomponents> addresscomponents) {
        if (CollectionUtils.isEmpty(addresscomponents)) {
            return null;
        }

        return addresscomponents.stream().filter(Objects::nonNull).map(this::mapOutAddressComponent).collect(Collectors.toList());
    }

    private AddressComponent mapOutAddressComponent(final Addresscomponents addresscomponents) {
        if (addresscomponents.getAddresscomponent() == null) {
            return null;
        }

        AddressComponent addressComponent = new AddressComponent();
        addressComponent.setComponentTypes(mapOutComponentTypes(addresscomponents.getAddresscomponent().getComponenttypes()));
        addressComponent.setName(addresscomponents.getAddresscomponent().getName());
        addressComponent.setCode(addresscomponents.getAddresscomponent().getCode());
        return addressComponent;
    }

    private Geolocation mapOutGeolocation(final com.bbva.pzic.cards.dao.model.pecpt004_1.Geolocation geolocation) {
        if (geolocation == null) {
            return null;
        }

        Geolocation result = new Geolocation();
        result.setLatitude(converToBigDecimal(geolocation.getLatitude()));
        result.setLongitude(converToBigDecimal(geolocation.getLongitude()));
        return result;
    }

    private BigDecimal converToBigDecimal(final String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }

        try {
            return new BigDecimal(value);
        } catch (NumberFormatException io) {
            LOG.error("... Error trying to parse geolocation value to number value");
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }
    }

    private List<String> mapOutComponentTypes(final List<Componenttypes> addresscomponents) {
        if (CollectionUtils.isEmpty(addresscomponents)) {
            return null;
        }

        return addresscomponents.stream().filter(Objects::nonNull).map(Componenttypes::getComponenttype).collect(Collectors.toList());
    }
}
