package com.bbva.pzic.cards.dao.model.mpwt.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpwt.*;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpwt")
public class TransaccionMpwtMock implements InvocadorTransaccion<PeticionTransaccionMpwt, RespuestaTransaccionMpwt> {

    private FormatMpwtMock mock;

    @PostConstruct
    public void init() {
        mock = new FormatMpwtMock();
    }

    public static final Integer EMPTY_TEST = 66;
    public static final Integer NULL_TEST = 77;

    @Override
    public RespuestaTransaccionMpwt invocar(PeticionTransaccionMpwt peticionTransaccionMpwt) {
        RespuestaTransaccionMpwt result = new RespuestaTransaccionMpwt();
        result.setCodigoRetorno("OK_COMMIT");
        result.setCodigoControl("OK");

        FormatoMPM0TSE formtIn = peticionTransaccionMpwt.getCuerpo().getParte(FormatoMPM0TSE.class);
        if (NULL_TEST.equals(formtIn.getNcuotas())) {
            return result;
        }
        if (EMPTY_TEST.equals(formtIn.getNcuotas())) {
            try {
                result.getCuerpo().getPartes().add(createFormatoMPM0TSCEmpty());
                result.getCuerpo().getPartes().addAll(createFormatoMPM0DETEmpty());
                return result;
            } catch (IOException e) {
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
            }
        }
        try {
            result.getCuerpo().getPartes().add(createFormatoMPM0TSCSalidaDefault());
            result.getCuerpo().getPartes().addAll(createFormatoMPM0DETSalidaDefault());
            return result;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionMpwt invocarCache(PeticionTransaccionMpwt peticionTransaccionMpwt) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida createFormatoMPM0TSCEmpty() throws IOException {
        FormatoMPM0TSC outFormat = mock.getFormatoMPM0TSCEmpty();
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(outFormat);
        return copySalida;
    }

    private List<CopySalida> createFormatoMPM0DETEmpty() throws IOException {
        List<FormatoMPM0DET> formats = mock.getFormatoMPM0DETEmpty();
        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPM0DET format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

    private CopySalida createFormatoMPM0TSCSalidaDefault() throws IOException {
        FormatoMPM0TSC formatoMPM0TSC = mock.getFormatoMPM0TSC();
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoMPM0TSC);
        return copySalida;
    }

    private List<CopySalida> createFormatoMPM0DETSalidaDefault() throws IOException {
        List<FormatoMPM0DET> formats = mock.getFormatoMPM0DET();
        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPM0DET format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

}
