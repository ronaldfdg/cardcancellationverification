package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.InputGetCardCancellationVerification;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS13G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS23G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS33G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPRM3G0;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardCancellationVerificationV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Mapper
public class TxGetCardCancellationVerificationV1Mapper implements ITxGetCardCancellationVerificationV1Mapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPRM3G0 mapIn(final InputGetCardCancellationVerification input) {
        FormatoMPRM3G0 formatoMPRM3G0 = new FormatoMPRM3G0();
        formatoMPRM3G0.setIdetarj(input.getCardId());
        return formatoMPRM3G0;
    }

    @Override
    public CancellationVerification mapOutFormatoMPMS13G(final FormatoMPMS13G formatoMPMS13G, final CancellationVerification cancellationVerification) {
        cancellationVerification.setCardHolder(mapOutCardHolder(formatoMPMS13G));
        cancellationVerification.setGrantedCredit(createAmount(formatoMPMS13G.getLincred(), formatoMPMS13G.getMonedl()));
        if (formatoMPMS13G.getFultliq() != null) {
            try {
                cancellationVerification.setCutOffDate(DateUtils.toDate(formatoMPMS13G.getFultliq()));
            } catch (ParseException e) {
                throw new BusinessServiceException("wrongDate", e);
            }
        }
        cancellationVerification.setRelatedContracts(mapOutRelatedContracts(formatoMPMS13G));
        return cancellationVerification;
    }

    private CardHolder mapOutCardHolder(final FormatoMPMS13G formatoMPMS13G) {
        if (formatoMPMS13G.getNomclie() == null) {
            return null;
        }
        CardHolder cardHolder = new CardHolder();
        cardHolder.setHolderName(formatoMPMS13G.getNomclie());
        return cardHolder;
    }

    private List<RelatedContract> mapOutRelatedContracts(final FormatoMPMS13G formatoMPMS13G) {
        if (formatoMPMS13G.getContrat() == null) {
            return null;
        }
        RelatedContract relatedContract = new RelatedContract();
        relatedContract.setId(formatoMPMS13G.getContrat());
        relatedContract.setContractId(formatoMPMS13G.getContrat());
        List<RelatedContract> relatedContractList = new ArrayList<>();
        relatedContractList.add(relatedContract);
        return relatedContractList;
    }


    @Override
    public CancellationVerification mapOutFormatoMPMS23G(final FormatoMPMS23G formatoMPMS23G, final CancellationVerification cancellationVerification) {
        if (cancellationVerification.getDebtDetail() == null) {
            cancellationVerification.setDebtDetail(new ArrayList<>());
        }
        cancellationVerification.getDebtDetail().add(mapOutDebtDetail(formatoMPMS23G));
        return cancellationVerification;
    }

    private DebtDetail mapOutDebtDetail(final FormatoMPMS23G formatoMPMS23G) {
        DebtDetail debtDetail = new DebtDetail();
        debtDetail.setCurrencyDebt(formatoMPMS23G.getMoneda1());
        debtDetail.setCancellationAmount(createAmount(formatoMPMS23G.getImpcanc(), formatoMPMS23G.getMoneda1()));
        debtDetail.setDisposedBalance(mapOutDisposedBalance(formatoMPMS23G));
        debtDetail.setLiquidPendingDebt(createAmount(formatoMPMS23G.getCitotfc(), formatoMPMS23G.getMoneda1()));
        debtDetail.setLiquidPendingCapital(createAmount(formatoMPMS23G.getCcapliq(), formatoMPMS23G.getMoneda1()));
        debtDetail.setCurrentDebt(mapOutCurrentDebt(formatoMPMS23G));
        return debtDetail;
    }

    private DisposedBalance mapOutDisposedBalance(final FormatoMPMS23G formatoMPMS23G) {
        if (formatoMPMS23G.getTotcobr() == null) {
            return null;
        }
        DisposedBalance disposedBalance = new DisposedBalance();
        disposedBalance.setCurrentBalances(createAmount(formatoMPMS23G.getTotcobr(), formatoMPMS23G.getMoneda1()));
        disposedBalance.setPendingBalances(createAmount(formatoMPMS23G.getImpauto(), formatoMPMS23G.getMoneda1()));
        return disposedBalance;
    }

    private CurrentDebt mapOutCurrentDebt(final FormatoMPMS23G formatoMPMS23G) {
        CurrentDebt currentDebt = new CurrentDebt();
        currentDebt.setCreditInsurance(createAmount(formatoMPMS23G.getSegdesg(), formatoMPMS23G.getMoneda1()));
        currentDebt.setCapitalInstallment(createAmount(formatoMPMS23G.getCiscpet(), formatoMPMS23G.getMoneda1()));
        currentDebt.setCapitalDebt(createAmount(formatoMPMS23G.getCapvige(), formatoMPMS23G.getMoneda1()));
        currentDebt.setOverdraftBalances(createAmount(formatoMPMS23G.getExcline(), formatoMPMS23G.getMoneda1()));
        currentDebt.setConditions(mapOutCurrentDebtConditions(formatoMPMS23G));
        currentDebt.setRates(mapOutCurrentDebtRates(formatoMPMS23G));
        return currentDebt;
    }

    private List<ConditionCancellationVerification> mapOutCurrentDebtConditions(final FormatoMPMS23G formatoMPMS23G) {
        if (formatoMPMS23G.getIdcomem() == null
                && formatoMPMS23G.getAmctyp1() == null
                && formatoMPMS23G.getCuotvig() == null
                && formatoMPMS23G.getIdcodis() == null
                && formatoMPMS23G.getComflat() == null
                && formatoMPMS23G.getIdcogri() == null
                && formatoMPMS23G.getComgrif() == null
                && formatoMPMS23G.getIdcoman() == null
                && formatoMPMS23G.getCommant() == null
                && formatoMPMS23G.getIdcosob() == null
                && formatoMPMS23G.getComsobr() == null
                && formatoMPMS23G.getIdcoec1() == null
                && formatoMPMS23G.getPorvige() == null
                && formatoMPMS23G.getIdcoec2() == null
                && formatoMPMS23G.getSegries() == null
                && formatoMPMS23G.getIdcompe() == null
                && formatoMPMS23G.getCompena() == null) {
            return null;
        }
        String formatType = translator.translateBackendEnumValueStrictly("cancellationVerification.condition.formatType", formatoMPMS23G.getAmctyp1());
        List<ConditionCancellationVerification> conditionList = new ArrayList<>();
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcomem(), formatType, formatoMPMS23G.getCuotvig(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcodis(), formatType, formatoMPMS23G.getComflat(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcogri(), formatType, formatoMPMS23G.getComgrif(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcoman(), formatType, formatoMPMS23G.getCommant(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcosob(), formatType, formatoMPMS23G.getComsobr(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcoec1(), formatType, formatoMPMS23G.getPorvige(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcoec2(), formatType, formatoMPMS23G.getSegries(), formatoMPMS23G.getMoneda1()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS23G.getIdcompe(), formatType, formatoMPMS23G.getCompena(), formatoMPMS23G.getMoneda1()));
        return conditionList;
    }

    private RatesCancellationVerification mapOutCurrentDebtRates(final FormatoMPMS23G formatoMPMS23G) {
        List<ItemizeRate> itemizeRateList = new ArrayList<>();
        String unitRateType = translator.translateBackendEnumValueStrictly("cancellationVerification.rate.unitRateType", formatoMPMS23G.getAmityp1());
        itemizeRateList.add(createItemizeRate(formatoMPMS23G.getIdinava(), unitRateType, formatoMPMS23G.getIntavan(), formatoMPMS23G.getMoneda1()));
        itemizeRateList.add(createItemizeRate(formatoMPMS23G.getIdincom(), unitRateType, formatoMPMS23G.getIntcomp(), formatoMPMS23G.getMoneda1()));
        itemizeRateList.add(createItemizeRate(formatoMPMS23G.getIdincuo(), unitRateType, formatoMPMS23G.getIntinic(), formatoMPMS23G.getMoneda1()));
        itemizeRateList.add(createItemizeRate(formatoMPMS23G.getIdeximo(), unitRateType, formatoMPMS23G.getMoravig(), formatoMPMS23G.getMoneda1()));
        itemizeRateList.add(createItemizeRate(formatoMPMS23G.getIdexico(), unitRateType, formatoMPMS23G.getCompvig(), formatoMPMS23G.getMoneda1()));
        RatesCancellationVerification ratesCancellationVerification = new RatesCancellationVerification();
        ratesCancellationVerification.setItemizeRates(itemizeRateList);
        return ratesCancellationVerification;
    }

    @Override
    public CancellationVerification mapOutFormatoMPMS33G(final FormatoMPMS33G formatoMPMS33G, final CancellationVerification cancellationVerification) {
        if (cancellationVerification.getDebtDetail() == null) {
            cancellationVerification.setDebtDetail(new ArrayList<>());
            cancellationVerification.getDebtDetail().add(new DebtDetail());
        }
        cancellationVerification.getDebtDetail().get(cancellationVerification.getDebtDetail().size() - 1).setOverdueDebt(mapOutOverdueDebt(formatoMPMS33G));
        return cancellationVerification;
    }

    private OverdueDebt mapOutOverdueDebt(final FormatoMPMS33G formatoMPMS33G) {
        OverdueDebt overdueDebt = new OverdueDebt();
        overdueDebt.setCreditInsurance(createAmount(formatoMPMS33G.getSegven2(), formatoMPMS33G.getMoneda2()));
        overdueDebt.setCapitalInstallment(createAmount(formatoMPMS33G.getCiscpev(), formatoMPMS33G.getMoneda2()));
        overdueDebt.setCapitalDebt(createAmount(formatoMPMS33G.getCapvenc(), formatoMPMS33G.getMoneda2()));
        overdueDebt.setOverdraftBalances(createAmount(formatoMPMS33G.getMonexcv(), formatoMPMS33G.getMoneda2()));
        overdueDebt.setConditions(mapOutOverdueConditions(formatoMPMS33G));
        overdueDebt.setRates(mapOutOverdueDebtRates(formatoMPMS33G));
        return overdueDebt;
    }

    private List<ConditionCancellationVerification> mapOutOverdueConditions(final FormatoMPMS33G formatoMPMS33G) {
        List<ConditionCancellationVerification> conditionList = new ArrayList<>();
        String formatType = translator.translateBackendEnumValueStrictly("cancellationVerification.condition.formatType", formatoMPMS33G.getAmctyp2());
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvec2(), formatType, formatoMPMS33G.getSegven1(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvmem(), formatType, formatoMPMS33G.getCuovenc(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvdis(), formatType, formatoMPMS33G.getComfven(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvgri(), formatType, formatoMPMS33G.getComgven(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvman(), formatType, formatoMPMS33G.getCompven(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvpen(), formatType, formatoMPMS33G.getCompag3(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvsob(), formatType, formatoMPMS33G.getComveso(), formatoMPMS33G.getMoneda2()));
        conditionList.add(createConditionCancellationVerification(formatoMPMS33G.getIdpvec1(), formatType, formatoMPMS33G.getPorvenc(), formatoMPMS33G.getMoneda2()));
        return conditionList;
    }

    private RatesCancellationVerification mapOutOverdueDebtRates(final FormatoMPMS33G formatoMPMS33G) {
        List<ItemizeRate> itemizerateList = new ArrayList<>();
        String unitRateType = translator.translateBackendEnumValueStrictly("cancellationVerification.rate.unitRateType", formatoMPMS33G.getAmityp2());
        itemizerateList.add(createItemizeRate(formatoMPMS33G.getIdpvmon(), unitRateType, formatoMPMS33G.getInteres(), formatoMPMS33G.getMoneda2()));
        itemizerateList.add(createItemizeRate(formatoMPMS33G.getIdpvcuo(), unitRateType, formatoMPMS33G.getCincpev(), formatoMPMS33G.getMoneda2()));
        itemizerateList.add(createItemizeRate(formatoMPMS33G.getIdpvimo(), unitRateType, formatoMPMS33G.getMoraven(), formatoMPMS33G.getMoneda2()));
        itemizerateList.add(createItemizeRate(formatoMPMS33G.getIdpvico(), unitRateType, formatoMPMS33G.getIntvenc(), formatoMPMS33G.getMoneda2()));
        RatesCancellationVerification ratesCancellationVerification = new RatesCancellationVerification();
        ratesCancellationVerification.setItemizeRates(itemizerateList);
        return ratesCancellationVerification;
    }

    private ConditionCancellationVerification createConditionCancellationVerification(final String id, final String formatType, final BigDecimal amount, final String currency) {
        ConditionFormat format = new ConditionFormat();
        format.setFormatType(formatType);
        if ("AMOUNT".equalsIgnoreCase(formatType)) {
            format.setAmount(createAmount(amount, currency));
        }
        ConditionCancellationVerification conditionCancellationVerification = new ConditionCancellationVerification();
        conditionCancellationVerification.setId(id);
        conditionCancellationVerification.setFormat(format);
        return conditionCancellationVerification;
    }

    private ItemizeRate createItemizeRate(final String rateType, final String unitRateType, final BigDecimal amount, final String currency) {
        ItemizeRate itemizeRate = new ItemizeRate();
        itemizeRate.setRateType(rateType);
        itemizeRate.setItemizeRatesUnit(createItemizeRatesUnit(unitRateType, amount, currency));
        return itemizeRate;
    }

    private ItemizeRateUnit createItemizeRatesUnit(final String unitRateType, final BigDecimal amount, final String currency) {
        if (unitRateType == null && amount == null && currency == null) {
            return null;
        }
        ItemizeRateUnit itemizeRateUnit = new ItemizeRateUnit();
        itemizeRateUnit.setUnitRateType(unitRateType);
        itemizeRateUnit.setAmount(amount);
        itemizeRateUnit.setCurrency(currency);
        return itemizeRateUnit;
    }

    private Amount createAmount(final BigDecimal amount, final String currency) {
        if (amount == null) {
            return null;
        }
        Amount amount1 = new Amount();
        amount1.setAmount(amount);
        amount1.setCurrency(currency);
        return amount1;
    }
}