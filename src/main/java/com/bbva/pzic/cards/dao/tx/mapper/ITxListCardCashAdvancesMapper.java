package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntListCashAdvances;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMENDE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS1DE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS2DE;

public interface ITxListCardCashAdvancesMapper {

    FormatoMPMENDE mapInput(DTOIntCashAdvancesSearchCriteria dtoIn);

    DTOIntListCashAdvances mapOutput(FormatoMPMS1DE formatOutput, DTOIntListCashAdvances dtoOut);

    DTOIntListCashAdvances mapOutput2(FormatoMPMS2DE formatOutput, DTOIntListCashAdvances dtoOut);
}
