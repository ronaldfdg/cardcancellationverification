package com.bbva.pzic.cards.dao.model.mp6h;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMQ6GS</code> de la transacci&oacute;n <code>MP6H</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMQ6GS")
@RooJavaBean
@RooSerializable
public class FormatoMPMQ6GS {
	
	/**
	 * <p>Campo <code>AMOITY3</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "AMOITY3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amoity3;
	
	/**
	 * <p>Campo <code>AMOCTY3</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "AMOCTY3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amocty3;
	
	/**
	 * <p>Campo <code>IDMCIG1</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDMCIG1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcig1;
	
	/**
	 * <p>Campo <code>MCIPAGV</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "MCIPAGV", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcipagv;
	
	/**
	 * <p>Campo <code>IDIINAC</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "IDIINAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinac;
	
	/**
	 * <p>Campo <code>MCIINAC</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "MCIINAC", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinac;
	
	/**
	 * <p>Campo <code>IDIINAM</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "IDIINAM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinam;
	
	/**
	 * <p>Campo <code>MCIINAM</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "MCIINAM", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinam;
	
	/**
	 * <p>Campo <code>IDIINSD</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "IDIINSD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinsd;
	
	/**
	 * <p>Campo <code>MCIINSD</code>, &iacute;ndice: <code>10</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 10, nombre = "MCIINSD", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinsd;
	
	/**
	 * <p>Campo <code>IDIINSM</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "IDIINSM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinsm;
	
	/**
	 * <p>Campo <code>MCIINSM</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 12, nombre = "MCIINSM", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinsm;
	
	/**
	 * <p>Campo <code>IDITOCU</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "IDITOCU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String iditocu;
	
	/**
	 * <p>Campo <code>MCITOCU</code>, &iacute;ndice: <code>14</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 14, nombre = "MCITOCU", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitocu;
	
	/**
	 * <p>Campo <code>IDMCIE1</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "IDMCIE1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcie1;
	
	/**
	 * <p>Campo <code>MCIEXCL</code>, &iacute;ndice: <code>16</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 16, nombre = "MCIEXCL", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciexcl;
	
	/**
	 * <p>Campo <code>IDDCIM1</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "IDDCIM1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String iddcim1;
	
	/**
	 * <p>Campo <code>MCITOIS</code>, &iacute;ndice: <code>18</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 18, nombre = "MCITOIS", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitois;
	
	/**
	 * <p>Campo <code>IDIINNE</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "IDIINNE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinne;
	
	/**
	 * <p>Campo <code>MCIINNE</code>, &iacute;ndice: <code>20</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 20, nombre = "MCIINNE", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinne;
	
	/**
	 * <p>Campo <code>IDIDEU1</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "IDIDEU1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idideu1;
	
	/**
	 * <p>Campo <code>IDEUMES</code>, &iacute;ndice: <code>22</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 22, nombre = "IDEUMES", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal ideumes;
	
	/**
	 * <p>Campo <code>IDMCIR1</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "IDMCIR1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcir1;
	
	/**
	 * <p>Campo <code>MCICARP</code>, &iacute;ndice: <code>24</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 24, nombre = "MCICARP", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcicarp;
	
	/**
	 * <p>Campo <code>MONCON4</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "MONCON4", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moncon4;
	
	/**
	 * <p>Campo <code>AMOITYP</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "AMOITYP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amoityp;
	
	/**
	 * <p>Campo <code>AMOCTYP</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "AMOCTYP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amoctyp;
	
	/**
	 * <p>Campo <code>IDMCIG2</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "IDMCIG2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcig2;
	
	/**
	 * <p>Campo <code>MCIPAGB</code>, &iacute;ndice: <code>29</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 29, nombre = "MCIPAGB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcipagb;
	
	/**
	 * <p>Campo <code>IDIINAB</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "IDIINAB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinab;
	
	/**
	 * <p>Campo <code>MCIINAB</code>, &iacute;ndice: <code>31</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 31, nombre = "MCIINAB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinab;
	
	/**
	 * <p>Campo <code>IDIIAMB</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "IDIIAMB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiiamb;
	
	/**
	 * <p>Campo <code>MCIIAMB</code>, &iacute;ndice: <code>33</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 33, nombre = "MCIIAMB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciiamb;
	
	/**
	 * <p>Campo <code>IDIINCB</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 34, nombre = "IDIINCB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiincb;
	
	/**
	 * <p>Campo <code>MCIINCB</code>, &iacute;ndice: <code>35</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 35, nombre = "MCIINCB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciincb;
	
	/**
	 * <p>Campo <code>IDIINMB</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 36, nombre = "IDIINMB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiinmb;
	
	/**
	 * <p>Campo <code>MCIINMB</code>, &iacute;ndice: <code>37</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 37, nombre = "MCIINMB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciinmb;
	
	/**
	 * <p>Campo <code>IDITCMB</code>, &iacute;ndice: <code>38</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 38, nombre = "IDITCMB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String iditcmb;
	
	/**
	 * <p>Campo <code>MCITCMB</code>, &iacute;ndice: <code>39</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 39, nombre = "MCITCMB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitcmb;
	
	/**
	 * <p>Campo <code>IDMCIE2</code>, &iacute;ndice: <code>40</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 40, nombre = "IDMCIE2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcie2;
	
	/**
	 * <p>Campo <code>MCIEXCB</code>, &iacute;ndice: <code>41</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 41, nombre = "MCIEXCB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciexcb;
	
	/**
	 * <p>Campo <code>IDDCIM2</code>, &iacute;ndice: <code>42</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 42, nombre = "IDDCIM2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String iddcim2;
	
	/**
	 * <p>Campo <code>MCITCCB</code>, &iacute;ndice: <code>43</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 43, nombre = "MCITCCB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitccb;
	
	/**
	 * <p>Campo <code>IDIICUB</code>, &iacute;ndice: <code>44</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 44, nombre = "IDIICUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idiicub;
	
	/**
	 * <p>Campo <code>MCIICUB</code>, &iacute;ndice: <code>45</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 45, nombre = "MCIICUB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mciicub;
	
	/**
	 * <p>Campo <code>IDIDEU2</code>, &iacute;ndice: <code>46</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 46, nombre = "IDIDEU2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idideu2;
	
	/**
	 * <p>Campo <code>IDEUMBM</code>, &iacute;ndice: <code>47</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 47, nombre = "IDEUMBM", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal ideumbm;
	
	/**
	 * <p>Campo <code>IDMCIR2</code>, &iacute;ndice: <code>48</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 48, nombre = "IDMCIR2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcir2;
	
	/**
	 * <p>Campo <code>MCICPLB</code>, &iacute;ndice: <code>49</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 49, nombre = "MCICPLB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcicplb;
	
}