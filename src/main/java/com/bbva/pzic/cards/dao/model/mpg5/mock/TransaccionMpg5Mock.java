package com.bbva.pzic.cards.dao.model.mpg5.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMENG5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS2G5;
import com.bbva.pzic.cards.dao.model.mpg5.PeticionTransaccionMpg5;
import com.bbva.pzic.cards.dao.model.mpg5.RespuestaTransaccionMpg5;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpg5")
public class TransaccionMpg5Mock implements InvocadorTransaccion<PeticionTransaccionMpg5, RespuestaTransaccionMpg5> {

    public static final String TEST_EMPTY = "9999";
    public static final String TEST_NO_DATA = "9992";
    private FormatoMPMSNG5Mock formatoMPMSNG5Mock;

    @PostConstruct
    public void init() {
        formatoMPMSNG5Mock = new FormatoMPMSNG5Mock();
    }

    @Override
    public RespuestaTransaccionMpg5 invocar(PeticionTransaccionMpg5 peticion) {
        RespuestaTransaccionMpg5 response = new RespuestaTransaccionMpg5();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENG5 format = peticion.getCuerpo().getParte(FormatoMPMENG5.class);
        final String ideTarj = format.getIdetarj();

        if (TEST_NO_DATA.equals(ideTarj)) {
            return response;
        } else if (TEST_EMPTY.equals(ideTarj)) {

            response.getCuerpo().getPartes().add(buildData(formatoMPMSNG5Mock.getFormatoMPMS2G5Emtpy()));

        } else {

            response.getCuerpo().getPartes().add(buildData(formatoMPMSNG5Mock.getFormatoMPMS1G5()));
            response.getCuerpo().getPartes().addAll(buildDataFormatoMPMS2G5List(formatoMPMSNG5Mock.getFormatoMPMS2G5()));
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpg5 invocarCache(PeticionTransaccionMpg5 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(Object object) {
        CopySalida copy = new CopySalida();
        copy.setCopy(object);
        return copy;
    }

    private List<CopySalida> buildDataFormatoMPMS2G5List(List<FormatoMPMS2G5> formatoMPMS2G5s) {
        List<CopySalida> copySalidaList = new ArrayList<>();
        for (FormatoMPMS2G5 formatoMPMS2G5 : formatoMPMS2G5s) {
            CopySalida copySalida = new CopySalida();
            copySalida.setCopy(formatoMPMS2G5);
            copySalidaList.add(copySalida);
        }
        return copySalidaList;
    }

}
