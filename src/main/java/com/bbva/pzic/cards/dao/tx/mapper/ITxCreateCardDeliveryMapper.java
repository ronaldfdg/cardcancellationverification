package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntDelivery;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTECKB94;
import com.bbva.pzic.cards.dao.model.kb94.FormatoKTSCKB94;
import com.bbva.pzic.cards.facade.v1.dto.Delivery;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
public interface ITxCreateCardDeliveryMapper {

    FormatoKTECKB94 mapIn(DTOIntDelivery dtoIn);

    Delivery mapOut(FormatoKTSCKB94 formatoKTSCKB94);

}
