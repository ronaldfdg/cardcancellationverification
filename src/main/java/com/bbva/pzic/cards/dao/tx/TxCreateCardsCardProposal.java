package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCardsCardProposal;
import com.bbva.pzic.cards.canonic.CardProposal;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTECKB93;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTSKB931;
import com.bbva.pzic.cards.dao.model.kb93.PeticionTransaccionKb93;
import com.bbva.pzic.cards.dao.model.kb93.RespuestaTransaccionKb93;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsCardProposalMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
@Component("txCreateCardsCardProposal")
public class TxCreateCardsCardProposal
        extends SingleOutputFormat<InputCreateCardsCardProposal, FormatoKTECKB93, CardProposal, FormatoKTSKB931> {

    @Resource(name = "txCreateCardsCardProposalMapper")
    private ITxCreateCardsCardProposalMapper mapper;

    @Autowired
    public TxCreateCardsCardProposal(@Qualifier("transaccionKb93") InvocadorTransaccion<PeticionTransaccionKb93, RespuestaTransaccionKb93> transaction) {
        super(transaction, PeticionTransaccionKb93::new, CardProposal::new, FormatoKTSKB931.class);
    }

    @Override
    protected FormatoKTECKB93 mapInput(InputCreateCardsCardProposal inputCreateCardsCardProposal) {
        return mapper.mapIn(inputCreateCardsCardProposal);
    }

    @Override
    protected CardProposal mapFirstOutputFormat(FormatoKTSKB931 formatoKTSKB931, InputCreateCardsCardProposal inputCreateCardsCardProposal, CardProposal cardProposal) {
        return mapper.mapOut(formatoKTSKB931);
    }
}