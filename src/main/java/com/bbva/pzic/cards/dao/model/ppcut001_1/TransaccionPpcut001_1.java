package com.bbva.pzic.cards.dao.model.ppcut001_1;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>PPCUT001</code>
 * 
 * @see PeticionTransaccionPpcut001_1
 * @see RespuestaTransaccionPpcut001_1
 */
@Component
public class TransaccionPpcut001_1 implements InvocadorTransaccion<PeticionTransaccionPpcut001_1,RespuestaTransaccionPpcut001_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPpcut001_1 invocar(PeticionTransaccionPpcut001_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut001_1.class, RespuestaTransaccionPpcut001_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPpcut001_1 invocarCache(PeticionTransaccionPpcut001_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut001_1.class, RespuestaTransaccionPpcut001_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}