package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6GS;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6HE;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6HS;
import com.bbva.pzic.cards.facade.v1.dto.FinancialStatement;

public interface ITxGetCardFinancialStatementV1Mapper {

    FormatoMPMQ6HE mapIn(InputGetCardFinancialStatement input);

    FinancialStatement mapOutFormatoMPMQ6HS(FormatoMPMQ6HS format, FinancialStatement dtoOut);

    FinancialStatement mapOutFormatoMPMQ6GS(FormatoMPMQ6GS format, FinancialStatement dtoOut);

}
