package com.bbva.pzic.cards.dao.model.mcdv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MCDV</code>
 * 
 * @see PeticionTransaccionMcdv
 * @see RespuestaTransaccionMcdv
 */
@Component("transaccionMcdv")
public class TransaccionMcdv implements InvocadorTransaccion<PeticionTransaccionMcdv,RespuestaTransaccionMcdv> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMcdv invocar(PeticionTransaccionMcdv transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMcdv.class, RespuestaTransaccionMcdv.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMcdv invocarCache(PeticionTransaccionMcdv transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMcdv.class, RespuestaTransaccionMcdv.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}