package com.bbva.pzic.cards.dao.model.pecpt004_1.mock;

import com.bbva.pzic.cards.dao.model.pecpt004_1.RespuestaTransaccionPecpt004_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 18/02/2020.
 *
 * @author Entelgy
 */
public final class Pecpt004_1Stubs {

    private static final Pecpt004_1Stubs INSTANCE = new Pecpt004_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Pecpt004_1Stubs() {
    }

    public static Pecpt004_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPecpt004_1 buildRespuestaTransaccionPecpt004_1() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/pecpt004_1/mock/respuestaTransaccionPecpt004_1.json"), RespuestaTransaccionPecpt004_1.class);
    }
}
