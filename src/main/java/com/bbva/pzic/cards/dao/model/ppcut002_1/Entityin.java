package com.bbva.pzic.cards.dao.model.ppcut002_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityIn</code>, utilizado por la clase <code>PeticionTransaccionPpcut002_1</code></p>
 * 
 * @see PeticionTransaccionPpcut002_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityin {
	
	/**
	 * <p>Campo <code>deliveries</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "deliveries", tipo = TipoCampo.LIST)
	private List<Deliveries> deliveries;
	
	/**
	 * <p>Campo <code>grantedCredits</code>, &iacute;ndice: <code>2</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 2, nombre = "grantedCredits", tipo = TipoCampo.LIST)
	private List<Grantedcredits> grantedcredits;
	
	/**
	 * <p>Campo <code>additionalProducts</code>, &iacute;ndice: <code>3</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 3, nombre = "additionalProducts", tipo = TipoCampo.LIST)
	private List<Additionalproducts> additionalproducts;
	
	/**
	 * <p>Campo <code>cardType</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "cardType", tipo = TipoCampo.DTO)
	private Cardtype cardtype;
	
	/**
	 * <p>Campo <code>product</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "product", tipo = TipoCampo.DTO)
	private Product product;
	
	/**
	 * <p>Campo <code>physicalSupport</code>, &iacute;ndice: <code>6</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 6, nombre = "physicalSupport", tipo = TipoCampo.DTO)
	private Physicalsupport physicalsupport;
	
	/**
	 * <p>Campo <code>paymentMethod</code>, &iacute;ndice: <code>7</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 7, nombre = "paymentMethod", tipo = TipoCampo.DTO)
	private Paymentmethod paymentmethod;
	
	/**
	 * <p>Campo <code>specificContact</code>, &iacute;ndice: <code>8</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 8, nombre = "specificContact", tipo = TipoCampo.DTO)
	private Specificcontact specificcontact;
	
	/**
	 * <p>Campo <code>rates</code>, &iacute;ndice: <code>9</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 9, nombre = "rates", tipo = TipoCampo.DTO)
	private Rates rates;
	
	/**
	 * <p>Campo <code>fees</code>, &iacute;ndice: <code>10</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 10, nombre = "fees", tipo = TipoCampo.DTO)
	private Fees fees;
	
	/**
	 * <p>Campo <code>membership</code>, &iacute;ndice: <code>11</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 11, nombre = "membership", tipo = TipoCampo.DTO)
	private Membership membership;
	
	/**
	 * <p>Campo <code>image</code>, &iacute;ndice: <code>12</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 12, nombre = "image", tipo = TipoCampo.DTO)
	private Image image;
	
	/**
	 * <p>Campo <code>offerId</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "offerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String offerid;
	
	/**
	 * <p>Campo <code>contactability</code>, &iacute;ndice: <code>14</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 14, nombre = "contactability", tipo = TipoCampo.DTO)
	private Contactability contactability;
	
}