package com.bbva.pzic.cards.dao.model.pecpt004_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PECPT004</code>
 * 
 * @see PeticionTransaccionPecpt004_1
 * @see RespuestaTransaccionPecpt004_1
 */
@Component
public class TransaccionPecpt004_1 implements InvocadorTransaccion<PeticionTransaccionPecpt004_1,RespuestaTransaccionPecpt004_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPecpt004_1 invocar(PeticionTransaccionPecpt004_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt004_1.class, RespuestaTransaccionPecpt004_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPecpt004_1 invocarCache(PeticionTransaccionPecpt004_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt004_1.class, RespuestaTransaccionPecpt004_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}