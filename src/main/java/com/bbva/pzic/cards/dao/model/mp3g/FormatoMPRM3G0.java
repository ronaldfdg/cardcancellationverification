package com.bbva.pzic.cards.dao.model.mp3g;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPRM3G0</code> de la transacci&oacute;n <code>MP3G</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPRM3G0")
@RooJavaBean
@RooSerializable
public class FormatoMPRM3G0 {

	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String idetarj;
	
}