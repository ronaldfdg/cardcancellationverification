package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.dao.model.mpwp.FormatoMPMENWP;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
public interface ITxModifyCardPinMapper {

    FormatoMPMENWP mapInput(DTOIntPin dtoIntCard);
}
