package com.bbva.pzic.cards.dao.model.mpg7;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS1G7</code> de la transacci&oacute;n <code>MPG7</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1G7")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1G7 {

	/**
	 * <p>Campo <code>FECOPER</code>, &iacute;ndice: <code>1</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 1, nombre = "FECOPER", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecoper;

	/**
	 * <p>Campo <code>HOROPER</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "HOROPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horoper;

}