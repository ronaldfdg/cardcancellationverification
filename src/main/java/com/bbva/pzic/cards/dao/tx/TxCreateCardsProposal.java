package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.canonic.Proposal;
import com.bbva.pzic.cards.dao.model.kb89.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsProposalMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.ThreefoldOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
@Component("txCreateCardsProposal")
public class TxCreateCardsProposal
        extends ThreefoldOutputFormat<DTOIntProposal, FormatoKTECKB89, Proposal, FormatoKTSCKB89, FormatoKTS1KB89, FormatoKTS2KB89> {

    @Resource(name = "txCreateCardsProposalMapper")
    private ITxCreateCardsProposalMapper mapper;

    @Autowired
    public TxCreateCardsProposal(@Qualifier("transaccionKb89") InvocadorTransaccion<PeticionTransaccionKb89, RespuestaTransaccionKb89> transaction) {
        super(transaction, PeticionTransaccionKb89::new, Proposal::new, FormatoKTSCKB89.class, FormatoKTS1KB89.class, FormatoKTS2KB89.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FormatoKTECKB89 mapInput(final DTOIntProposal dtoIn) {
        return mapper.mapIn(dtoIn);
    }

    @Override
    protected Proposal mapFirstOutputFormat(FormatoKTSCKB89 formatOutput, DTOIntProposal dtoIn, Proposal dtoOut) {
        return mapper.mapOut(formatOutput);
    }

    @Override
    protected Proposal mapSecondOutputFormat(FormatoKTS1KB89 formatOutput, DTOIntProposal dtoIn, Proposal dtoOut) {
        return mapper.mapOut2(formatOutput, dtoOut);
    }

    @Override
    protected Proposal mapThirdOutputFormat(FormatoKTS2KB89 formatOutput, DTOIntProposal dtoIn, Proposal dtoOut) {
        return mapper.mapOut3(formatOutput, dtoOut);
    }
}
