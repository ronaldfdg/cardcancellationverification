package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityOut</code>, utilizado por la clase <code>RespuestaTransaccionPpcut004_1</code></p>
 * 
 * @see RespuestaTransaccionPpcut004_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityout {
	
	/**
	 * <p>Campo <code>data</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "data", tipo = TipoCampo.DTO)
	private Data data;
	
}