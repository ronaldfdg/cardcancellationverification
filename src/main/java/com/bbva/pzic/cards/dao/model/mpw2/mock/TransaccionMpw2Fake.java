package com.bbva.pzic.cards.dao.model.mpw2.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS1W2;
import com.bbva.pzic.cards.dao.model.mpw2.PeticionTransaccionMpw2;
import com.bbva.pzic.cards.dao.model.mpw2.RespuestaTransaccionMpw2;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionMpw2")
public class TransaccionMpw2Fake implements InvocadorTransaccion<PeticionTransaccionMpw2, RespuestaTransaccionMpw2> {

    @Override
    public RespuestaTransaccionMpw2 invocar(PeticionTransaccionMpw2 peticion) {
        RespuestaTransaccionMpw2 response = new RespuestaTransaccionMpw2();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        try {
            response.getCuerpo().getPartes().add(buildCopyMPMS1W2(FormatsMpw2Stubs.getInstance().getFormatoMPMS1W2()));
            return response;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private CopySalida buildCopyMPMS1W2(FormatoMPMS1W2 formatoMPMS1W2) {
        CopySalida copy = new CopySalida();
        copy.setCopy(formatoMPMS1W2);
        return copy;
    }

    @Override
    public RespuestaTransaccionMpw2 invocarCache(PeticionTransaccionMpw2 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
