package com.bbva.pzic.cards.dao.model.mpg3.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMENG3;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMS1G3;
import com.bbva.pzic.cards.dao.model.mpg3.PeticionTransaccionMpg3;
import com.bbva.pzic.cards.dao.model.mpg3.RespuestaTransaccionMpg3;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 11/10/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpg3")
public class TransaccionMpg3Mock implements InvocadorTransaccion<PeticionTransaccionMpg3, RespuestaTransaccionMpg3> {

    public static final String EMPTY_TEST = "9999";
    private FormatoMPMS1G3Mock formatoMPMS1G3Mock;

    @PostConstruct
    private void setUp() {
        formatoMPMS1G3Mock = new FormatoMPMS1G3Mock();
    }

    @Override
    public RespuestaTransaccionMpg3 invocar(PeticionTransaccionMpg3 peticion) {
        final RespuestaTransaccionMpg3 response = new RespuestaTransaccionMpg3();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENG3 formatIn = peticion.getCuerpo().getParte(FormatoMPMENG3.class);
        String ideTarj = formatIn.getIdetarj();

        if (EMPTY_TEST.equals(ideTarj)) {
            return response;
        } else {
            response.getCuerpo().getPartes().add(buildDataCopies());
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpg3 invocarCache(PeticionTransaccionMpg3 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildDataCopies() {
        FormatoMPMS1G3 formatoMPMS1G3 = formatoMPMS1G3Mock.getFormatoMPMS1G3();
        CopySalida copy = new CopySalida();
        copy.setCopy(formatoMPMS1G3);
        return copy;
    }
}
