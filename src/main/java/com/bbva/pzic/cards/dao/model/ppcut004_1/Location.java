package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import java.util.List;

/**
 * <p>Bean fila para el campo tabular <code>location</code>, utilizado por la clase <code>Address</code></p>
 * 
 * @see Address
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Location {
	
	/**
	 * <p>Campo <code>addressComponents</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "addressComponents", tipo = TipoCampo.LIST)
	private List<Addresscomponents> addresscomponents;
	
	/**
	 * <p>Campo <code>additionalInformation</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "additionalInformation", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 80, signo = true)
	private String additionalinformation;
	
}