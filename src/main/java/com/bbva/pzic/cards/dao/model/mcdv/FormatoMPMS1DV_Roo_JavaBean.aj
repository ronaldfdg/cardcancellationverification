// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mcdv;

import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMS1DV;

privileged aspect FormatoMPMS1DV_Roo_JavaBean {
    
    /**
     * Gets idcodsg value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getIdcodsg() {
        return this.idcodsg;
    }
    
    /**
     * Sets idcodsg value
     * 
     * @param idcodsg
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setIdcodsg(String idcodsg) {
        this.idcodsg = idcodsg;
        return this;
    }
    
    /**
     * Gets dscodsg value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getDscodsg() {
        return this.dscodsg;
    }
    
    /**
     * Sets dscodsg value
     * 
     * @param dscodsg
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setDscodsg(String dscodsg) {
        this.dscodsg = dscodsg;
        return this;
    }
    
    /**
     * Gets codse01 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse01() {
        return this.codse01;
    }
    
    /**
     * Sets codse01 value
     * 
     * @param codse01
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse01(String codse01) {
        this.codse01 = codse01;
        return this;
    }
    
    /**
     * Gets codse02 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse02() {
        return this.codse02;
    }
    
    /**
     * Sets codse02 value
     * 
     * @param codse02
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse02(String codse02) {
        this.codse02 = codse02;
        return this;
    }
    
    /**
     * Gets codse03 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse03() {
        return this.codse03;
    }
    
    /**
     * Sets codse03 value
     * 
     * @param codse03
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse03(String codse03) {
        this.codse03 = codse03;
        return this;
    }
    
    /**
     * Gets codse04 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse04() {
        return this.codse04;
    }
    
    /**
     * Sets codse04 value
     * 
     * @param codse04
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse04(String codse04) {
        this.codse04 = codse04;
        return this;
    }
    
    /**
     * Gets codse05 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse05() {
        return this.codse05;
    }
    
    /**
     * Sets codse05 value
     * 
     * @param codse05
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse05(String codse05) {
        this.codse05 = codse05;
        return this;
    }
    
    /**
     * Gets codse06 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse06() {
        return this.codse06;
    }
    
    /**
     * Sets codse06 value
     * 
     * @param codse06
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse06(String codse06) {
        this.codse06 = codse06;
        return this;
    }
    
    /**
     * Gets codse07 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse07() {
        return this.codse07;
    }
    
    /**
     * Sets codse07 value
     * 
     * @param codse07
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse07(String codse07) {
        this.codse07 = codse07;
        return this;
    }
    
    /**
     * Gets codse08 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse08() {
        return this.codse08;
    }
    
    /**
     * Sets codse08 value
     * 
     * @param codse08
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse08(String codse08) {
        this.codse08 = codse08;
        return this;
    }
    
    /**
     * Gets codse09 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse09() {
        return this.codse09;
    }
    
    /**
     * Sets codse09 value
     * 
     * @param codse09
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse09(String codse09) {
        this.codse09 = codse09;
        return this;
    }
    
    /**
     * Gets codse10 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse10() {
        return this.codse10;
    }
    
    /**
     * Sets codse10 value
     * 
     * @param codse10
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse10(String codse10) {
        this.codse10 = codse10;
        return this;
    }
    
    /**
     * Gets codse11 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse11() {
        return this.codse11;
    }
    
    /**
     * Sets codse11 value
     * 
     * @param codse11
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse11(String codse11) {
        this.codse11 = codse11;
        return this;
    }
    
    /**
     * Gets codse12 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse12() {
        return this.codse12;
    }
    
    /**
     * Sets codse12 value
     * 
     * @param codse12
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse12(String codse12) {
        this.codse12 = codse12;
        return this;
    }
    
    /**
     * Gets codse13 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse13() {
        return this.codse13;
    }
    
    /**
     * Sets codse13 value
     * 
     * @param codse13
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse13(String codse13) {
        this.codse13 = codse13;
        return this;
    }
    
    /**
     * Gets codse14 value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getCodse14() {
        return this.codse14;
    }
    
    /**
     * Sets codse14 value
     * 
     * @param codse14
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setCodse14(String codse14) {
        this.codse14 = codse14;
        return this;
    }
    
    /**
     * Gets timerns value
     * 
     * @return Integer
     */
    public Integer FormatoMPMS1DV.getTimerns() {
        return this.timerns;
    }
    
    /**
     * Sets timerns value
     * 
     * @param timerns
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setTimerns(Integer timerns) {
        this.timerns = timerns;
        return this;
    }
    
    /**
     * Gets timerun value
     * 
     * @return String
     */
    public String FormatoMPMS1DV.getTimerun() {
        return this.timerun;
    }
    
    /**
     * Sets timerun value
     * 
     * @param timerun
     * @return FormatoMPMS1DV
     */
    public FormatoMPMS1DV FormatoMPMS1DV.setTimerun(String timerun) {
        this.timerun = timerun;
        return this;
    }
    
}
