package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntNumberType;
import com.bbva.pzic.cards.business.dto.DTOIntProduct;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContracts;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMENL5;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardRelatedContractsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper("txListCardRelatedContractsMapper")
public class TxListCardRelatedContractsMapper implements ITxListCardRelatedContractsMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public FormatoMPMENL5 mapInput(DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria) {
        FormatoMPMENL5 formatoMPMENL5 = new FormatoMPMENL5();
        formatoMPMENL5.setIdetarj(dtoIntRelatedContractsSearchCriteria.getCardId());
        return formatoMPMENL5;
    }

    @Override
    public List<DTOIntRelatedContracts> mapOutput(FormatoMPMS1L5 formatoMPMS1L5, List<DTOIntRelatedContracts> dtoIntRelatedContractsList) {
        DTOIntRelatedContracts data = new DTOIntRelatedContracts();
        data.setRelatedContractId(formatoMPMS1L5.getIdrela());
        data.setContractId(cypherTool.encrypt(formatoMPMS1L5.getNucorel(), AbstractCypherTool.RELATED_CONTRACT_ID));
        data.setNumber(cypherTool.mask(formatoMPMS1L5.getNumprod(), AbstractCypherTool.CARD_NUMBER));

        if (formatoMPMS1L5.getTnumid() != null || formatoMPMS1L5.getTnumdes() != null) {
            DTOIntNumberType dtoIntNumberType = new DTOIntNumberType();
            dtoIntNumberType.setId(enumMapper.getEnumValue("cards.numberType.id", formatoMPMS1L5.getTnumid()));
            dtoIntNumberType.setName(formatoMPMS1L5.getTnumdes());
            data.setNumberType(dtoIntNumberType);
        }

        if (formatoMPMS1L5.getProdid() != null || formatoMPMS1L5.getProddes() != null) {
            DTOIntProduct dtoIntProduct = new DTOIntProduct();
            dtoIntProduct.setId(enumMapper.getEnumValue("cards.product.id", formatoMPMS1L5.getProdid()));
            dtoIntProduct.setName(formatoMPMS1L5.getProddes());
            data.setProduct(dtoIntProduct);
        }
        dtoIntRelatedContractsList.add(data);
        return dtoIntRelatedContractsList;
    }
}
