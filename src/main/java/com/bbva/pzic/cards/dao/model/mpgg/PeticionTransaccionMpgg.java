package com.bbva.pzic.cards.dao.model.mpgg;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPGG</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpgg</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpgg</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2GG.D1180508.TXT
 * MPMS2GG �HIST. MOVTOS FINANCIADOS      �X�02�00025�01�00001�FECPAGO�FECHA EXPIRAC. PAGO �A�010�0�S�        �
 * MPMS2GG �HIST. MOVTOS FINANCIADOS      �X�02�00025�02�00011�MONTPAG�MONTO PAGO MENSUAL  �S�015�2�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1GG.D1180508.TXT
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�01�00001�IDENTIF�IDENTIF. MOVIMIENTO �A�008�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�02�00009�DSENTIF�DESCRIP. MOVIMIENTO �A�030�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�03�00039�IDFINAN�ID FINANCIAMIENTO   �A�002�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�04�00041�DSFINAN�DESC. FINANCIAMIENTO�A�030�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�05�00071�FECTRAP�FECHA TRASP. CUOTAS �A�010�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�06�00081�TOTCUO �TOTAL CUOTAS OPERAC.�N�003�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�07�00084�PENCUO �NRO. CUOTAS PENDIENT�N�003�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�08�00087�FRECUO �FRECUENCIA CUOTAS   �A�001�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�09�00088�IMPORI �IMPORTE OPERAC. ORIG�N�015�2�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�10�00103�MONEDA �MONEDA OPERACION    �A�003�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�11�00106�IMPTOT �IMPORTE TOTAL OPERAC�N�015�2�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�12�00121�MONTRES�MONTO RESTANTE PAGAR�N�015�2�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�13�00136�MONPAG �MONTO PAGADO        �N�015�2�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�14�00151�PORCPAG�PORC. COMPLETADO PAG�N�009�4�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�15�00160�FECOPEO�FECHA OPER ORIGINAL �A�010�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�16�00170�FECCUO1�FECHA PRIMERA CUOTA �A�010�0�S�        �
 * MPMS1GG �CABECERA HIST. MOVTOS FINANC  �X�17�00192�17�00180�IMPCUO1�IMPORT PRIMERA CUOTA�N�013�2�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPGG.D1180508.TXT
 * MPGGMPMS1GG MPMCS1GGMP2CMPGG1S                             XP92348 2018-02-02-11.15.35.876771XP92348 2018-02-02-11.15.35.876825
 * MPGGMPMS2GG MPMCS2GGMP2CMPGG1S                             XP92348 2018-02-02-11.15.47.245305XP92348 2018-02-02-11.15.47.274249
 * MPGGMPMS3GG MPNCS3GGMP2CMPGG1S                             XP92348 2018-02-05-16.28.45.111733XP92348 2018-02-05-16.28.45.111757
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS3GG.D1180508.TXT
 * MPMS3GG �FORMATO DE PAGINACION         �X�02�00027�01�00001�IDPAGIN�IDENTIFICADOR DE PAG�A�024�0�S�        �
 * MPMS3GG �FORMATO DE PAGINACION         �X�02�00027�02�00025�TAMPAGI�NRO REG A DEVOLVER  �N�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENGG.D1180508.TXT
 * MPMENGG �HIST. MOVTOS FINANCIADOS      �F�03�00047�01�00001�NUMTARJ�NUMERO TARJETA      �A�020�0�R�        �
 * MPMENGG �HIST. MOVTOS FINANCIADOS      �F�03�00047�02�00021�IDPAGIN�CLAVE SIG. IDENTIF  �A�024�0�O�        �
 * MPMENGG �HIST. MOVTOS FINANCIADOS      �F�03�00047�03�00045�TAMPAGI�NRO. REG. DEVOLVER  �N�003�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPGG.D1180508.TXT
 * MPGGHISTORICO MOVIMIENTOS FINANCIADOS  MP        MP2CMPGGPBDMPPO MPMENGG             MPGG  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-02-02XP92348 2018-03-0513.24.39XP92348 2018-02-02-11.12.32.623359XP92348 0001-01-010001-01-01
</pre></code>
 *
 * @see RespuestaTransaccionMpgg
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPGG",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpgg.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENGG.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpgg implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}