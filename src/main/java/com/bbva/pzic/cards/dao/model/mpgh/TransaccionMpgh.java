package com.bbva.pzic.cards.dao.model.mpgh;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPGH</code>
 *
 * @see PeticionTransaccionMpgh
 * @see RespuestaTransaccionMpgh
 */
@Component
public class TransaccionMpgh implements InvocadorTransaccion<PeticionTransaccionMpgh, RespuestaTransaccionMpgh> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionMpgh invocar(PeticionTransaccionMpgh transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpgh.class, RespuestaTransaccionMpgh.class, transaccion);
    }

    @Override
    public RespuestaTransaccionMpgh invocarCache(PeticionTransaccionMpgh transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpgh.class, RespuestaTransaccionMpgh.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
