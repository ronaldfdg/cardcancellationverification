package com.bbva.pzic.cards.dao.model.mpcv.mock;

import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMS1CV;
import org.springframework.stereotype.Component;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@Component
public class FormatMpcvMock {


    public FormatoMPMS1CV getFormatoMPMS1CV() {
        FormatoMPMS1CV format = new FormatoMPMS1CV();
        format.setIdcodsg("CVV2");
        format.setDscodsg("Codigo de seguridad");
        format.setCodse01("1073");
        format.setCodse02("5184");
        format.setCodse03("7295");
        format.setCodse04("5316");
        format.setCodse05("2427");
        format.setCodse06("4538");
        format.setCodse07("3649");
        format.setCodse08("3751");
        format.setCodse09("0862");
        format.setCodse10("1973");
        format.setCodse11("2084");
        format.setCodse12("3195");
        format.setCodse13("4216");
        format.setCodse14("5327");
        return format;
    }
}
