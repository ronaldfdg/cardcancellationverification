package com.bbva.pzic.cards.dao.model.ppcut003_1.mock;

import com.bbva.pzic.cards.dao.model.ppcut003_1.RespuestaTransaccionPpcut003_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 06/11/2019.
 *
 * @author Entelgy
 */
public final class Ppcut003_1Stubs {

    private static final Ppcut003_1Stubs INSTANCE = new Ppcut003_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Ppcut003_1Stubs() {
    }

    public static Ppcut003_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPpcut003_1 getProposal() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/ppcut003_1/mock/respuestaTransaccionPpcut003_1.json"), RespuestaTransaccionPpcut003_1.class);
    }
}
