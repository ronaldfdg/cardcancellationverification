package com.bbva.pzic.cards.dao.model.mpws;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPWS</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpws</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpws</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPWS.D1180508.txt
 * MPWSMPM0DET MPECMPWDMP2CMPWS2S                             XP88366 2017-06-21-17.35.36.774731XP88366 2017-06-21-17.35.36.778911
 * MPWSMPM0WSC MPECMPSCMP2CMPWS1S                             XP88366 2017-06-21-17.32.10.827892XP88366 2017-06-21-17.32.10.832055
 * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPWS.D1180508.txt
 * MPWSSIMULACION DE TRASPASO A CUOTAS    MP        MP2CMPWSPBDMPPO MPM0WSE             MPWS  NS0000CNNNNN    SSTN    C   NNNNSNNN  NN                2017-06-21XP88366 2017-09-1915.28.43XP92460 2017-06-21-15.52.40.792732XP88366 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0WSE.D1180508.txt
 * MPM0WSE �ENTRADA SIMULACION DE CUOTAS  �F�05�00056�01�00001�NUMTARJ�NUMERO TARJ OPERA   �A�016�0�R�        �
 * MPM0WSE �ENTRADA SIMULACION DE CUOTAS  �F�05�00056�02�00017�IMPOPER�IMPORTE OPERACION   �N�015�2�O�        �
 * MPM0WSE �ENTRADA SIMULACION DE CUOTAS  �F�05�00056�03�00032�NCUOTAS�NRO CUOTAS SIMULA   �N�002�0�R�        �
 * MPM0WSE �ENTRADA SIMULACION DE CUOTAS  �F�05�00056�04�00034�DIVOPER�DIV IMPORTE SIMULAR �A�003�0�O�        �
 * MPM0WSE �ENTRADA SIMULACION DE CUOTAS  �F�05�00056�05�00037�NUMOPER�NRO OPERAC. SIMULAR �A�020�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0WSC.D1180508.txt
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�01�00001�TASAEFA�TASA EFECTIVA ANUAL �N�009�2�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�02�00010�TOTCAPI�TOTAL CAPITAL CRONO �N�013�2�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�03�00023�DIVTCAP�DIVISA TOTAL CAPITAL�A�003�0�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�04�00026�TOTINTE�TOTAL INTERES CRONO �N�013�2�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�05�00039�DIVTINT�DIVISA TOTAL INTERES�A�003�0�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�06�00042�TOTCUOT�TOTAL SUMA CUO CRONO�N�013�2�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�07�00055�DIVTCUO�DIVISA TOTAL CUOTAS �A�003�0�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�08�00058�PORTCEA�PORCENTAJE TCEA     �N�007�4�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�09�00065�FECCUO1�FECHA PRIMERA CUOTA �A�010�0�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�10�00075�IMPCUO1�IMPORT PRIMERA CUOTA�N�013�2�S�        �
 * MPM0WSC �CABECERA SIMULACION DE CUOTAS �X�11�00097�11�00088�FECSIMU�FECHA SIMULACION    �A�010�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0DET.D1180508.txt
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�01�00001�FECPAGO�FECHA PAGO CUOTA    �A�010�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�02�00011�CAPITAL�CAPITAL DE CUOTA    �N�011�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�03�00022�DIVCAPC�DIVISA CAPITAL CUOTA�A�003�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�04�00025�INTERES�INTERES DE CUOTA    �N�011�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�05�00036�DIVINTE�DIVISA INTERES CUOTA�A�003�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�06�00039�COMISIO�COMISION DE CUOTA   �N�011�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�07�00050�DIVCOMI�DIVISA COMISION CUOT�A�003�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�08�00053�IMPCUOT�IMPORTE CUOTA       �N�015�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�09�00068�DIVCUOT�DIVISA IMP CUOTA    �A�003�0�S�        �
</pre></code>
 *
 * @see RespuestaTransaccionMpws
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPWS",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpws.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0WSE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpws implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}