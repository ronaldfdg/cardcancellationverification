package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.apx.mapper.IApxUpdateCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Address;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Branch;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Delivery;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Destination;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Frecuency;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Image;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Location;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Membership;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Product;
import com.bbva.pzic.cards.dao.model.ppcut002_1.Subproduct;
import com.bbva.pzic.cards.dao.model.ppcut002_1.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.mappers.DateMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
@Mapper
public class ApxUpdateCardProposalMapper implements IApxUpdateCardProposalMapper {

    @Override
    public PeticionTransaccionPpcut002_1 mapIn(final InputUpdateCardProposal input) {
        PeticionTransaccionPpcut002_1 request = new PeticionTransaccionPpcut002_1();
        request.setProposalId(input.getProposalId());
        if (input.getProposal() != null) {
            Entityin entityin = new Entityin();
            entityin.setCardtype(mapInCardType(input.getProposal().getCardType()));
            entityin.setProduct(mapInProduct(input.getProposal().getProduct()));
            entityin.setPhysicalsupport(mapInPhysicalSupport(input.getProposal().getPhysicalSupport()));
            entityin.setDeliveries(mapInDeliveries(input.getProposal().getDeliveries()));
            entityin.setPaymentmethod(mapInPaymentMethod(input.getProposal().getPaymentMethod()));
            entityin.setGrantedcredits(mapInGrantedCredits(input.getProposal().getGrantedCredits()));
            entityin.setSpecificcontact(mapInSpecificcontact(input.getProposal().getContact()));
            entityin.setRates(mapInRate(input.getProposal().getRates()));
            entityin.setFees(mapInFees(input.getProposal().getFees()));
            entityin.setAdditionalproducts(mapInAdditionalProducts(input.getProposal().getAdditionalProducts()));
            entityin.setMembership(mapInMembership(input.getProposal().getMembership()));
            entityin.setImage(mapInImage(input.getProposal().getImage()));
            entityin.setContactability(mapInContactAbility(input.getProposal().getContactAbility()));
            request.setEntityin(entityin);
        }
        return request;
    }

    private Contactability mapInContactAbility(final DTOIntContactAbility contactAbility) {
        if (contactAbility == null) {
            return null;
        }

        Contactability result = new Contactability();
        result.setReason(contactAbility.getReason());
        result.setScheduletimes(mapInScheduleTimes(contactAbility.getScheduletimes()));
        return result;
    }

    private Scheduletimes mapInScheduleTimes(final DTOIntScheduletimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        Scheduletimes result = new Scheduletimes();
        result.setEndtime(scheduletimes.getEndtime());
        result.setStarttime(scheduletimes.getStarttime());
        return result;
    }

    private Image mapInImage(final DTOIntImage image) {
        if (image == null) {
            return null;
        }

        Image result = new Image();
        result.setId(image.getId());
        result.setName(image.getName());
        result.setUrl(image.getUrl());
        return result;
    }

    private List<Additionalproducts> mapInAdditionalProducts(final List<DTOIntAdditionalProduct> dtoIntAdditionalProducts) {
        if (CollectionUtils.isEmpty(dtoIntAdditionalProducts)) {
            return null;
        }

        return dtoIntAdditionalProducts.stream().map(this::mapInAdditionalProduct).collect(Collectors.toList());
    }

    private Additionalproducts mapInAdditionalProduct(final DTOIntAdditionalProduct dtoIntAdditionalProduct) {
        Additionalproducts additionalproducts = new Additionalproducts();
        Additionalproduct additionalproduct = new Additionalproduct();
        additionalproduct.setAmount(dtoIntAdditionalProduct.getAmount() == null ? null : dtoIntAdditionalProduct.getAmount().toString());
        additionalproduct.setCurrency(dtoIntAdditionalProduct.getCurrency());
        additionalproduct.setProducttype(dtoIntAdditionalProduct.getProductType());

        additionalproducts.setAdditionalproduct(additionalproduct);
        return additionalproducts;
    }

    private Fees mapInFees(final DTOIntFee dtoIntFee) {
        if (dtoIntFee == null) {
            return null;
        }

        Fees fees = new Fees();
        fees.setItemizefees(mapInItemizeFees(dtoIntFee.getItemizeFees()));
        return fees;
    }

    private List<Itemizefees> mapInItemizeFees(final List<DTOIntItemizeFee> dtoIntItemizeFees) {
        if (dtoIntItemizeFees == null) {
            return null;
        }

        return dtoIntItemizeFees.stream().map(this::mapInItemizeFee).collect(Collectors.toList());

    }

    private Itemizefees mapInItemizeFee(final DTOIntItemizeFee dtoIntItemizeFee) {
        if (dtoIntItemizeFee == null) {
            return null;
        }

        Itemizefees itemizefees = new Itemizefees();
        Itemizefee itemizefee = new Itemizefee();
        itemizefee.setFeetype(dtoIntItemizeFee.getFeeType());
        itemizefee.setItemizefeeunit(mapInItemizeFreeUnit(dtoIntItemizeFee.getItemizeFeeUnit()));

        itemizefees.setItemizefee(itemizefee);
        return itemizefees;
    }

    private Itemizefeeunit mapInItemizeFreeUnit(final DTOIntItemizeFeeUnit dtoIntItemizeFeeUnit) {
        if (dtoIntItemizeFeeUnit == null) {
            return null;
        }

        Itemizefeeunit itemizefeeunit = new Itemizefeeunit();
        itemizefeeunit.setAmount(dtoIntItemizeFeeUnit.getAmount() == null ? null : dtoIntItemizeFeeUnit.getAmount().toString());
        itemizefeeunit.setCurrency(dtoIntItemizeFeeUnit.getCurrency());
        itemizefeeunit.setUnittype(dtoIntItemizeFeeUnit.getUnitType());
        return itemizefeeunit;
    }

    private Rates mapInRate(final DTOIntRate dtoIntRate) {
        if (dtoIntRate == null) {
            return null;
        }

        Rates rates = new Rates();
        rates.setItemizerates(mapInItemizeRates(dtoIntRate.getItemizeRates()));
        return rates;
    }

    private List<Itemizerates> mapInItemizeRates(final List<DTOIntItemizeRate> dtoIntItemizeRates) {
        if (CollectionUtils.isEmpty(dtoIntItemizeRates)) {
            return null;
        }

        return dtoIntItemizeRates.stream().map(this::mapInItemizeRate).collect(Collectors.toList());
    }

    private Itemizerates mapInItemizeRate(final DTOIntItemizeRate dtoIntItemizeRate) {
        if (dtoIntItemizeRate == null) {
            return null;
        }

        Itemizerates itemizerates = new Itemizerates();
        Itemizerate itemizerate = new Itemizerate();
        itemizerate.setRatetype(dtoIntItemizeRate.getRateType());
        itemizerate.setItemizeratesunit(mapInItemizeRateUnit(dtoIntItemizeRate.getItemizeRatesUnit()));

        itemizerates.setItemizerate(itemizerate);
        return itemizerates;
    }

    private Itemizeratesunit mapInItemizeRateUnit(final DTOIntItemizeRatesUnit dtoIntItemizeRatesUnit) {
        if (dtoIntItemizeRatesUnit == null) {
            return null;
        }

        Itemizeratesunit itemizeratesunit = new Itemizeratesunit();
        itemizeratesunit.setUnitratetype(dtoIntItemizeRatesUnit.getUnitRateType());
        itemizeratesunit.setPercentage(dtoIntItemizeRatesUnit.getPercentage() == null ? null : dtoIntItemizeRatesUnit.getPercentage().toString());
        return itemizeratesunit;
    }

    private Membership mapInMembership(final DTOIntMembership dtoIntMembership) {
        if (dtoIntMembership == null) {
            return null;
        }

        Membership membership = new Membership();
        membership.setId(dtoIntMembership.getId());
        return membership;
    }

    private Specificcontact mapInSpecificcontact(final DTOIntSpecificProposalContact dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }

        Specificcontact specificcontact = new Specificcontact();
        specificcontact.setMobilecontact(mapInContact(dtoIntContact.getContact()));
        specificcontact.setContacttype(dtoIntContact.getContactType());
        return specificcontact;
    }

    private Mobilecontact mapInContact(final DTOIntMobileProposal dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }

        Mobilecontact contact = new Mobilecontact();
        contact.setContactdetailtype(dtoIntContact.getContactDetailType());
        contact.setNumber(dtoIntContact.getNumber());
        contact.setPhonecompany(mapInPhoneCompany(dtoIntContact.getPhoneCompany()));
        return contact;
    }

    private Phonecompany mapInPhoneCompany(final DTOIntPhoneCompanyProposal contactPhoneCompany) {
        if (contactPhoneCompany == null) {
            return null;
        }

        Phonecompany phonecompany = new Phonecompany();
        phonecompany.setId(contactPhoneCompany.getId());
        return phonecompany;
    }

    private List<Grantedcredits> mapInGrantedCredits(final List<DTOIntImport> dtoIntGrantedCredits) {
        if (CollectionUtils.isEmpty(dtoIntGrantedCredits)) {
            return null;
        }

        return dtoIntGrantedCredits.stream().map(this::mapInGrantedCredit).collect(Collectors.toList());
    }

    private Grantedcredits mapInGrantedCredit(DTOIntImport dtoIntGrantedCredits) {
        Grantedcredits grantedcredits = new Grantedcredits();
        Grantedcredit grantedcredit = new Grantedcredit();
        grantedcredit.setAmount(dtoIntGrantedCredits.getAmount() == null ? null : dtoIntGrantedCredits.getAmount().toString());
        grantedcredit.setCurrency(dtoIntGrantedCredits.getCurrency());
        grantedcredits.setGrantedcredit(grantedcredit);
        return grantedcredits;
    }

    private Paymentmethod mapInPaymentMethod(final DTOIntPaymentMethod dtoIntPaymentMethod) {
        if (dtoIntPaymentMethod == null) {
            return null;
        }
        Paymentmethod paymentmethod = new Paymentmethod();
        paymentmethod.setId(dtoIntPaymentMethod.getId());
        paymentmethod.setFrecuency(mapInFrequency(dtoIntPaymentMethod.getFrequency()));
        return paymentmethod;
    }

    private Frecuency mapInFrequency(final DTOIntFrequency dtoIntFrequency) {
        if (dtoIntFrequency == null) {
            return null;
        }

        Frecuency frecuency = new Frecuency();
        frecuency.setId(dtoIntFrequency.getId());
        frecuency.setDaysofmonth(mapInDaysofmonth(dtoIntFrequency.getDaysOfMonth()));
        return frecuency;
    }

    private Daysofmonth mapInDaysofmonth(final DTOIntDaysOfMonth dtoIntDaysOfMonth) {
        if (dtoIntDaysOfMonth == null) {
            return null;
        }

        Daysofmonth daysofmonth = new Daysofmonth();
        daysofmonth.setDay(dtoIntDaysOfMonth.getDay());
        daysofmonth.setCutoffday(dtoIntDaysOfMonth.getCutOffDay());
        return daysofmonth;
    }

    private List<Deliveries> mapInDeliveries(final List<DTOIntDelivery> dtoIntDeliveries) {
        if (CollectionUtils.isEmpty(dtoIntDeliveries)) {
            return null;
        }

        return dtoIntDeliveries.stream().map(this::mapInDelivery).collect(Collectors.toList());
    }

    private Deliveries mapInDelivery(final DTOIntDelivery dtoIntDelivery) {
        Deliveries deliveries = new Deliveries();
        Delivery delivery = new Delivery();
        delivery.setServicetype(mapInServiceType(dtoIntDelivery.getServiceTypeId()));
        delivery.setDeliverycontact(mapInDeliveryContact(dtoIntDelivery.getContact()));
        delivery.setAddress(mapInAddress(dtoIntDelivery.getAddress()));
        delivery.setDestination(mapInDestination(dtoIntDelivery.getDestination()));

        deliveries.setDelivery(delivery);
        return deliveries;
    }

    private Destination mapInDestination(final DTOIntDestination dtoIntDestination) {
        if (dtoIntDestination == null) {
            return null;
        }

        Destination destination = new Destination();
        destination.setId(dtoIntDestination.getId());
        destination.setBranch(mapInBranch(dtoIntDestination.getBranch()));
        return destination;
    }

    private Branch mapInBranch(final DTOIntBranch dtoIntBranch) {
        if (dtoIntBranch == null) {
            return null;
        }

        Branch branch = new Branch();
        branch.setId(dtoIntBranch.getId());
        return branch;
    }

    private Address mapInAddress(final DTOIntAddress dtoIntAddress) {
        if (dtoIntAddress == null) {
            return null;
        }

        Address address = new Address();
        address.setAddresstype(dtoIntAddress.getAddressType());
        address.setLocation(mapInLocation(dtoIntAddress.getLocation()));
        return address;
    }

    private Location mapInLocation(final DTOIntLocation dtoIntLocation) {
        Location location = new Location();
        location.setAddresscomponents(mapInAddressComponents(dtoIntLocation.getAddressComponents()));
        return location;
    }

    private List<Addresscomponents> mapInAddressComponents(final List<DTOIntAddressComponents> dtoIntAddressComponents) {
        if (CollectionUtils.isEmpty(dtoIntAddressComponents)) {
            return null;
        }

        return dtoIntAddressComponents.stream().map(this::mapInAddressComponent).collect(Collectors.toList());
    }

    private Addresscomponents mapInAddressComponent(final DTOIntAddressComponents dtoIntAddressComponents) {
        Addresscomponents addresscomponents = new Addresscomponents();
        Addresscomponent addresscomponent = new Addresscomponent();
        addresscomponent.setCode(dtoIntAddressComponents.getCode());
        addresscomponent.setName(dtoIntAddressComponents.getName());
        addresscomponent.setComponenttypes(mapInComponentTypes(dtoIntAddressComponents.getComponentTypes()));
        addresscomponents.setAddresscomponent(addresscomponent);
        return addresscomponents;
    }

    private List<Componenttypes> mapInComponentTypes(final List<String> dtoIntComponentTypes) {
        if (CollectionUtils.isEmpty(dtoIntComponentTypes)) {
            return null;
        }

        return dtoIntComponentTypes.stream().map(ct -> {
            Componenttypes componenttypes = new Componenttypes();
            componenttypes.setComponenttype(ct);
            return componenttypes;
        }).collect(Collectors.toList());
    }

    private Deliverycontact mapInDeliveryContact(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }

        Deliverycontact contact = new Deliverycontact();
        contact.setContacttype(dtoIntContact.getContactType());
        contact.setEmailcontact(mapInEmailSpecificContact(dtoIntContact));
        return contact;
    }

    private Emailcontact mapInEmailSpecificContact(final DTOIntContact dtoIntContact) {
        Emailcontact contactdetail = new Emailcontact();
        contactdetail.setAddress(dtoIntContact.getContactAddress());
        contactdetail.setContactdetailtype(dtoIntContact.getContactDetailType());
        return contactdetail;
    }

    private Servicetype mapInServiceType(final String serviceTypeId) {
        if (StringUtils.isEmpty(serviceTypeId)) {
            return null;
        }

        Servicetype servicetype = new Servicetype();
        servicetype.setId(serviceTypeId);
        return servicetype;
    }

    private Physicalsupport mapInPhysicalSupport(final DTOIntPhysicalSupport dtoIntPhysicalSupport) {
        if (dtoIntPhysicalSupport == null) {
            return null;
        }

        Physicalsupport physicalsupport = new Physicalsupport();
        physicalsupport.setId(dtoIntPhysicalSupport.getId());
        return physicalsupport;
    }

    private Product mapInProduct(final DTOIntProduct dtoIntProduct) {
        if (dtoIntProduct == null) {
            return null;
        }

        Product product = new Product();
        product.setId(dtoIntProduct.getId());

        if (dtoIntProduct.getSubproduct() == null) {
            return product;
        }

        Subproduct subproduct = new Subproduct();
        subproduct.setId(dtoIntProduct.getSubproduct().getId());
        product.setSubproduct(subproduct);
        return product;
    }

    private Cardtype mapInCardType(final DTOIntCardType cardType) {
        if (cardType == null) {
            return null;
        }

        Cardtype cardtype = new Cardtype();
        cardtype.setId(cardType.getId());
        return cardtype;
    }

    @Override
    public Proposal mapOut(RespuestaTransaccionPpcut002_1 response) {
        if (response == null || response.getEntityout() == null) {
            return null;
        }

        Entityout respuestaTx = response.getEntityout();
        Proposal result = new Proposal();
        result.setId(respuestaTx.getId());
        result.setCardType(mapOutCardType(respuestaTx.getCardtype()));
        result.setProduct(mapOutProduct(respuestaTx.getProductout()));
        result.setPhysicalSupport(mapOutPhysicalSupport(respuestaTx.getPhysicalsupport()));
        result.setDeliveries(mapOutDeliveries(respuestaTx.getDeliveriesout()));
        result.setPaymentMethod(mapOutPaymentMethod(respuestaTx.getPaymentmethod()));
        result.setGrantedCredits(mapOutGrantedCredits(respuestaTx.getGrantedcredits()));
        result.setContact(mapOutSpecificContact(respuestaTx.getSpecificcontactout()));
        result.setRates(mapOutRates(respuestaTx.getRatesout()));
        result.setFees(mapOutFees(respuestaTx.getFeesout()));
        result.setAdditionalProducts(mapOutAdditionalProducts(respuestaTx.getAdditionalproducts()));
        result.setMembership(mapOutMembership(respuestaTx.getMembership()));
        result.setStatus(respuestaTx.getStatus());
        result.setImage(mapOutImage(respuestaTx.getImage()));
        result.setContactability(mapOutContactAbility(respuestaTx.getContactability()));
        result.setOfferId(respuestaTx.getOfferid());
        return result;
    }

    private ContactAbility mapOutContactAbility(final Contactability contactability) {
        if (contactability == null) {
            return null;
        }

        ContactAbility result = new ContactAbility();
        result.setReason(contactability.getReason());
        result.setScheduleTimes(mapOutScheduleTimes(contactability.getScheduletimes()));
        return result;
    }

    private ScheduleTimes mapOutScheduleTimes(final Scheduletimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        ScheduleTimes result = new ScheduleTimes();
        result.setEndTime(DateMapper.mapDateToStringDateTime(scheduletimes.getEndtime()));
        result.setStartTime(DateMapper.mapDateToStringDateTime(scheduletimes.getStarttime()));
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Image mapOutImage(final Image image) {
        if (image == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Image result = new com.bbva.pzic.cards.facade.v1.dto.Image();
        result.setId(image.getId());
        result.setName(image.getName());
        result.setUrl(image.getUrl());
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Membership mapOutMembership(final Membership membershipOut) {
        if (membershipOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Membership membership = new com.bbva.pzic.cards.facade.v1.dto.Membership();
        membership.setId(membershipOut.getId());
        membership.setDescription(membershipOut.getDescription());
        return membership;
    }

    private List<AdditionalProduct> mapOutAdditionalProducts(final List<Additionalproducts> additionalproductsOut) {
        if (CollectionUtils.isEmpty(additionalproductsOut)) {
            return null;
        }

        return additionalproductsOut.stream().map(this::mapOutAdditionalProduct).collect(Collectors.toList());
    }

    private AdditionalProduct mapOutAdditionalProduct(final Additionalproducts additionalproductsOut) {
        if (additionalproductsOut.getAdditionalproduct() == null) {
            return null;
        }

        AdditionalProduct additionalProduct = new AdditionalProduct();
        additionalProduct.setProductType(additionalproductsOut.getAdditionalproduct().getProducttype());
        additionalProduct.setAmount(NumberUtils.isNumber(additionalproductsOut.getAdditionalproduct().getAmount()) ? NumberUtils.createBigDecimal(additionalproductsOut.getAdditionalproduct().getAmount()) : null);
        additionalProduct.setCurrency(additionalproductsOut.getAdditionalproduct().getCurrency());
        return additionalProduct;
    }

    private TransferFees mapOutFees(final Feesout feesOut) {
        if (feesOut == null) {
            return null;
        }
        TransferFees fee = new TransferFees();
        fee.setItemizeFees(mapOutItemizeFees(feesOut.getItemizefeesout()));
        return fee;
    }

    private List<ItemizeFee> mapOutItemizeFees(final List<Itemizefeesout> itemizefeesOut) {
        if (CollectionUtils.isEmpty(itemizefeesOut)) {
            return null;
        }

        return itemizefeesOut.stream().map(this::mapOutItemizeFee).collect(Collectors.toList());
    }

    private ItemizeFee mapOutItemizeFee(final Itemizefeesout itemizefeesOut) {
        if (itemizefeesOut.getItemizefeeout() == null) {
            return null;
        }

        ItemizeFee itemizeFee = new ItemizeFee();
        itemizeFee.setDescription(itemizefeesOut.getItemizefeeout().getDescription());
        itemizeFee.setFeeType(itemizefeesOut.getItemizefeeout().getFeetype());
        itemizeFee.setItemizeFeeUnit(mapOutItemizeFreeUnit(itemizefeesOut.getItemizefeeout().getItemizefeeunit()));
        return itemizeFee;
    }

    private ItemizeFeeUnit mapOutItemizeFreeUnit(final Itemizefeeunit itemizefeeunitOut) {
        if (itemizefeeunitOut == null) {
            return null;
        }

        ItemizeFeeUnit itemizeFeeUnit = new ItemizeFeeUnit();
        itemizeFeeUnit.setUnitType(itemizefeeunitOut.getUnittype());
        itemizeFeeUnit.setAmount(NumberUtils.isNumber(itemizefeeunitOut.getAmount()) ? NumberUtils.createBigDecimal(itemizefeeunitOut.getAmount()) : null);
        itemizeFeeUnit.setCurrency(itemizefeeunitOut.getCurrency());
        return itemizeFeeUnit;
    }

    private ProposalRate mapOutRates(final Ratesout ratesOut) {
        if (ratesOut == null) {
            return null;
        }

        ProposalRate proposalRate = new ProposalRate();
        proposalRate.setItemizeRates(mapOutItemizeRates(ratesOut.getItemizeratesout()));
        return proposalRate;
    }

    private List<ItemizeRate> mapOutItemizeRates(final List<Itemizeratesout> itemizeratesOut) {
        if (CollectionUtils.isEmpty(itemizeratesOut)) {
            return null;
        }

        return itemizeratesOut.stream().map(this::mapOutItemizeRate).collect(Collectors.toList());
    }

    private ItemizeRate mapOutItemizeRate(final Itemizeratesout itemizeratesOut) {
        if (itemizeratesOut.getItemizerateout() == null) {
            return null;
        }

        ItemizeRate itemizeRate = new ItemizeRate();
        itemizeRate.setRateType(itemizeratesOut.getItemizerateout().getRatetype());
        itemizeRate.setDescription(itemizeratesOut.getItemizerateout().getDescription());
        itemizeRate.setItemizeRatesUnit(mapOutItemizeRatesUnit(itemizeratesOut.getItemizerateout().getItemizeratesunit()));
        return itemizeRate;
    }

    private ItemizeRateUnit mapOutItemizeRatesUnit(Itemizeratesunit itemizeratesunitOut) {
        if (itemizeratesunitOut == null) {
            return null;
        }

        ItemizeRateUnit itemizeRateUnit = new ItemizeRateUnit();
        itemizeRateUnit.setPercentage(NumberUtils.isNumber(itemizeratesunitOut.getPercentage()) ? NumberUtils.createBigDecimal(itemizeratesunitOut.getPercentage()) : null);
        itemizeRateUnit.setUnitRateType(itemizeratesunitOut.getUnitratetype());
        return itemizeRateUnit;
    }

    private SpecificProposalContact mapOutSpecificContact(final Specificcontactout specificcontactOut) {
        if (specificcontactOut == null) {
            return null;
        }
        SpecificProposalContact contact = new SpecificProposalContact();
        contact.setId(specificcontactOut.getId());
        contact.setContactType(specificcontactOut.getContacttype());
        contact.setContact(mapOutContact(specificcontactOut.getMobilecontact()));
        return contact;
    }

    private MobileProposal mapOutContact(final Mobilecontact contactOut) {
        if (contactOut == null) {
            return null;
        }

        MobileProposal specificContact = new MobileProposal();
        specificContact.setContactDetailType(contactOut.getContactdetailtype());
        specificContact.setNumber(contactOut.getNumber());
        specificContact.setPhoneCompany(mapOutPhoneCompany(contactOut.getPhonecompany()));
        return specificContact;
    }

    private PhoneCompanyProposal mapOutPhoneCompany(final Phonecompany phonecompanyOut) {
        if (phonecompanyOut == null) {
            return null;
        }

        PhoneCompanyProposal phoneCompany = new PhoneCompanyProposal();
        phoneCompany.setId(phonecompanyOut.getId());
        phoneCompany.setName(phonecompanyOut.getName());
        return phoneCompany;
    }

    private List<Import> mapOutGrantedCredits(final List<Grantedcredits> grantedcreditsOut) {
        if (CollectionUtils.isEmpty(grantedcreditsOut)) {
            return null;
        }

        return grantedcreditsOut.stream().map(this::mapOutGrantedCredit).collect(Collectors.toList());
    }

    private Import mapOutGrantedCredit(final Grantedcredits grantedcreditsOut) {
        if (grantedcreditsOut.getGrantedcredit() == null) {
            return null;
        }

        Import amount = new Import();
        amount.setAmount(NumberUtils.isNumber(grantedcreditsOut.getGrantedcredit().getAmount()) ? NumberUtils.createBigDecimal(grantedcreditsOut.getGrantedcredit().getAmount()) : null);
        amount.setCurrency(grantedcreditsOut.getGrantedcredit().getCurrency());
        return amount;
    }

    private PaymentMethod mapOutPaymentMethod(final Paymentmethod paymentmethodOut) {
        if (paymentmethodOut == null) {
            return null;
        }

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(paymentmethodOut.getId());
        paymentMethod.setFrecuency(mapOutFrecuency(paymentmethodOut.getFrecuency()));
        return paymentMethod;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Frecuency mapOutFrecuency(final Frecuency frecuencyOut) {
        if (frecuencyOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Frecuency frequency = new com.bbva.pzic.cards.facade.v1.dto.Frecuency();
        frequency.setId(frecuencyOut.getId());
        frequency.setDescription(frecuencyOut.getDescription());
        frequency.setDaysOfMonth(mapOutDaysOfMonth(frecuencyOut.getDaysofmonth()));
        return frequency;
    }

    private DaysOfMonth mapOutDaysOfMonth(final Daysofmonth daysofmonthOut) {
        if (daysofmonthOut == null) {
            return null;
        }

        DaysOfMonth daysOfMonth = new DaysOfMonth();
        daysOfMonth.setCutOffDay(daysofmonthOut.getCutoffday());
        daysOfMonth.setDay(daysofmonthOut.getDay());
        return daysOfMonth;
    }

    private List<com.bbva.pzic.cards.facade.v1.dto.Delivery> mapOutDeliveries(List<Deliveriesout> deliveriesOut) {
        if (CollectionUtils.isEmpty(deliveriesOut)) {
            return null;
        }

        return deliveriesOut.stream().map(this::mapOutDelivery).collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v1.dto.Delivery mapOutDelivery(final Deliveriesout deliveriesOut) {
        if (deliveriesOut.getDeliveryout() == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Delivery delivery = new com.bbva.pzic.cards.facade.v1.dto.Delivery();
        delivery.setId(deliveriesOut.getDeliveryout().getId());
        delivery.setServiceType(mapOutServiceType(deliveriesOut.getDeliveryout().getServicetype()));
        delivery.setContact(mapOutDeliveryContact(deliveriesOut.getDeliveryout().getDeliverycontactout()));
        delivery.setAddress(mapOutAddress(deliveriesOut.getDeliveryout().getAddress()));
        delivery.setDestination(mapOutDestination(deliveriesOut.getDeliveryout().getDestinationout()));
        return delivery;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Destination mapOutDestination(final Destinationout destinationOut) {
        if (destinationOut == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Destination destination = new com.bbva.pzic.cards.facade.v1.dto.Destination();
        destination.setId(destinationOut.getId());
        destination.setName(destinationOut.getName());
        destination.setBranch(mapOutBranch(destinationOut.getBranch()));
        return destination;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Branch mapOutBranch(final Branch branchOut) {
        if (branchOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Branch branch = new com.bbva.pzic.cards.facade.v1.dto.Branch();
        branch.setId(branchOut.getId());
        branch.setName(branchOut.getName());
        return branch;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Address mapOutAddress(final Address addressOut) {
        if (addressOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Address address = new com.bbva.pzic.cards.facade.v1.dto.Address();
        address.setAddressType(addressOut.getAddresstype());
        address.setLocation(mapOutLocation(addressOut.getLocation()));
        return address;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Location mapOutLocation(final Location locationOut) {
        if (locationOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Location location = new com.bbva.pzic.cards.facade.v1.dto.Location();
        location.setAddressComponents(mapOutAddressComponents(locationOut.getAddresscomponents()));
        return location;
    }

    private List<AddressComponents> mapOutAddressComponents(final List<Addresscomponents> addresscomponentsOut) {
        if (addresscomponentsOut == null) {
            return null;
        }

        return addresscomponentsOut.stream().map(this::mapOutAddressComponent).collect(Collectors.toList());
    }

    private AddressComponents mapOutAddressComponent(final Addresscomponents addresscomponentsOut) {
        if (addresscomponentsOut.getAddresscomponent() == null) {
            return null;
        }

        AddressComponents addressComponents = new AddressComponents();
        addressComponents.setCode(addresscomponentsOut.getAddresscomponent().getCode());
        addressComponents.setName(addresscomponentsOut.getAddresscomponent().getName());
        addressComponents.setComponentTypes(mapOutComponentTypes(addresscomponentsOut.getAddresscomponent().getComponenttypes()));
        return addressComponents;
    }

    private List<String> mapOutComponentTypes(final List<Componenttypes> componenttypesOut) {
        if (CollectionUtils.isEmpty(componenttypesOut)) {
            return null;
        }

        return componenttypesOut.stream().map(Componenttypes::getComponenttype).collect(Collectors.toList());
    }


    private com.bbva.pzic.cards.facade.v1.dto.Contact mapOutDeliveryContact(final Deliverycontactout deliverycontactOut) {
        if (deliverycontactOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Contact contact = new com.bbva.pzic.cards.facade.v1.dto.Contact();
        contact.setId(deliverycontactOut.getId());
        contact.setContactType(deliverycontactOut.getContacttype());
        contact.setContact(mapOutContactDetail(deliverycontactOut.getEmailcontact()));
        return contact;
    }

    private SpecificContact mapOutContactDetail(final Emailcontact contactdetailOut) {
        if (contactdetailOut == null) {
            return null;
        }

        SpecificContact specificContact = new SpecificContact();
        specificContact.setContactDetailType(contactdetailOut.getContactdetailtype());
        specificContact.setAddress(contactdetailOut.getAddress());
        return specificContact;
    }

    private ServiceType mapOutServiceType(final Servicetype servicetypeOut) {
        if (servicetypeOut == null) {
            return null;
        }

        ServiceType serviceType = new ServiceType();
        serviceType.setId(servicetypeOut.getId());
        serviceType.setDescription(servicetypeOut.getDescription());
        return serviceType;
    }

    private PhysicalSupport mapOutPhysicalSupport(final Physicalsupport physicalsupportOut) {
        if (physicalsupportOut == null) {
            return null;
        }

        PhysicalSupport physicalSupport = new PhysicalSupport();
        physicalSupport.setId(physicalsupportOut.getId());
        physicalSupport.setDescription(physicalsupportOut.getDescription());
        return physicalSupport;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Product mapOutProduct(final Productout productOut) {
        if (productOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Product product = new com.bbva.pzic.cards.facade.v1.dto.Product();
        product.setId(productOut.getId());
        product.setName(productOut.getName());
        if (productOut.getSubproduct() == null) {
            return product;
        }

        com.bbva.pzic.cards.facade.v1.dto.Subproduct subProduct = new com.bbva.pzic.cards.facade.v1.dto.Subproduct();
        subProduct.setId(productOut.getSubproduct().getId());
        product.setSubproduct(subProduct);
        return product;
    }

    private CardType mapOutCardType(final Cardtype cardtype) {
        if (cardtype == null) {
            return null;
        }

        CardType cardType = new CardType();
        cardType.setId(cardtype.getId());
        cardType.setName(cardtype.getName());
        return cardType;
    }
}
