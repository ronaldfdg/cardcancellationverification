package com.bbva.pzic.cards.dao.model.mpdc;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MCRMDC1</code> de la transacci&oacute;n <code>MPDC</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MCRMDC1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMCRMDC1 {

	/**
	 * <p>Campo <code>TIPOIMP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPOIMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipoimp;

	/**
	 * <p>Campo <code>MONIMP</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 2, nombre = "MONIMP", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal monimp;

	/**
	 * <p>Campo <code>DIVIMP</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "DIVIMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divimp;

}