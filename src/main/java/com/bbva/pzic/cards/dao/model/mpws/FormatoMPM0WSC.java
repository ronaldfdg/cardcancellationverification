package com.bbva.pzic.cards.dao.model.mpws;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Formato de datos <code>MPM0WSC</code> de la transacci&oacute;n <code>MPWS</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPM0WSC")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPM0WSC {

	/**
	 * <p>Campo <code>TASAEFA</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 1, nombre = "TASAEFA", tipo = TipoCampo.DECIMAL, longitudMinima = 9, longitudMaxima = 9, decimales = 2)
	private BigDecimal tasaefa;

	/**
	 * <p>Campo <code>TOTCAPI</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 2, nombre = "TOTCAPI", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal totcapi;

	/**
	 * <p>Campo <code>DIVTCAP</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "DIVTCAP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divtcap;

	/**
	 * <p>Campo <code>TOTINTE</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "TOTINTE", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal totinte;

	/**
	 * <p>Campo <code>DIVTINT</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DIVTINT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divtint;

	/**
	 * <p>Campo <code>TOTCUOT</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "TOTCUOT", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal totcuot;

	/**
	 * <p>Campo <code>DIVTCUO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "DIVTCUO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divtcuo;

	/**
	 * <p>Campo <code>PORTCEA</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "PORTCEA", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal portcea;

	/**
	 * <p>Campo <code>FECCUO1</code>, &iacute;ndice: <code>9</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 9, nombre = "FECCUO1", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date feccuo1;

	/**
	 * <p>Campo <code>IMPCUO1</code>, &iacute;ndice: <code>10</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 10, nombre = "IMPCUO1", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, decimales = 2)
	private BigDecimal impcuo1;

	/**
	 * <p>Campo <code>FECSIMU</code>, &iacute;ndice: <code>11</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 11, nombre = "FECSIMU", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecsimu;

}