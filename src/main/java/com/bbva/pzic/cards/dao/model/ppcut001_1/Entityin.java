package com.bbva.pzic.cards.dao.model.ppcut001_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import java.util.List;

/**
 * <p>Bean fila para el campo tabular <code>EntityIn</code>, utilizado por la clase <code>PeticionTransaccionPpcut001_1</code></p>
 * 
 * @see PeticionTransaccionPpcut001_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityin {
	
	/**
	 * <p>Campo <code>cardType</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "cardType", tipo = TipoCampo.DTO)
	private Cardtype cardtype;
	
	/**
	 * <p>Campo <code>product</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "product", tipo = TipoCampo.DTO)
	private Product product;
	
	/**
	 * <p>Campo <code>physicalSupport</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "physicalSupport", tipo = TipoCampo.DTO)
	private Physicalsupport physicalsupport;
	
	/**
	 * <p>Campo <code>grantedCredits</code>, &iacute;ndice: <code>4</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 4, nombre = "grantedCredits", tipo = TipoCampo.LIST)
	private List<Grantedcredits> grantedcredits;
	
	/**
	 * <p>Campo <code>contact</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "contact", tipo = TipoCampo.DTO)
	private Contact contact;
	
	/**
	 * <p>Campo <code>rates</code>, &iacute;ndice: <code>6</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 6, nombre = "rates", tipo = TipoCampo.DTO)
	private Rates rates;
	
	/**
	 * <p>Campo <code>fees</code>, &iacute;ndice: <code>7</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 7, nombre = "fees", tipo = TipoCampo.DTO)
	private Fees fees;
	
	/**
	 * <p>Campo <code>offerId</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "offerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String offerid;
	
	/**
	 * <p>Campo <code>participants</code>, &iacute;ndice: <code>9</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 9, nombre = "participants", tipo = TipoCampo.LIST)
	private List<Participants> participants;
	
}