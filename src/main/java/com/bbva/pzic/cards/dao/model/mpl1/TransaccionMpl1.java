package com.bbva.pzic.cards.dao.model.mpl1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPL1</code>
 * 
 * @see PeticionTransaccionMpl1
 * @see RespuestaTransaccionMpl1
 */
@Component
public class TransaccionMpl1 implements InvocadorTransaccion<PeticionTransaccionMpl1,RespuestaTransaccionMpl1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMpl1 invocar(PeticionTransaccionMpl1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpl1.class, RespuestaTransaccionMpl1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMpl1 invocarCache(PeticionTransaccionMpl1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpl1.class, RespuestaTransaccionMpl1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}