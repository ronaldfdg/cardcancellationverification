package com.bbva.pzic.cards.dao.model.pecpt004_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.pecpt004_1.PeticionTransaccionPecpt004_1;
import com.bbva.pzic.cards.dao.model.pecpt004_1.RespuestaTransaccionPecpt004_1;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>PECPT001</code>
 *
 * @see PeticionTransaccionPecpt004_1
 * @see RespuestaTransaccionPecpt004_1
 */
@Component("transaccionPecpt004_1")
public class TransaccionPecpt004_1Mock implements InvocadorTransaccion<PeticionTransaccionPecpt004_1, RespuestaTransaccionPecpt004_1> {

    public static final String TEST_EMPTY = "999";

    @Override
    public RespuestaTransaccionPecpt004_1 invocar(final PeticionTransaccionPecpt004_1 transaccion) {
        try {
            if (TEST_EMPTY.equalsIgnoreCase(transaccion.getShipmentid())) {
                return new RespuestaTransaccionPecpt004_1();
            }
            RespuestaTransaccionPecpt004_1 respuesta = Pecpt004_1Stubs.getInstance().buildRespuestaTransaccionPecpt004_1();
            if (transaccion.getExpand() == null) {
                respuesta.getEntityout().setAddresses(null);
            }
            return respuesta;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPecpt004_1 invocarCache(PeticionTransaccionPecpt004_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
