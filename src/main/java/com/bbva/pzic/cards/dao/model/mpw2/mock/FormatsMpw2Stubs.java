package com.bbva.pzic.cards.dao.model.mpw2.mock;

import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS1W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS2W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS3W2;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 08/03/2020.
 *
 * @author Entelgy
 */
public final class FormatsMpw2Stubs {

    private static final FormatsMpw2Stubs INSTANCE = new FormatsMpw2Stubs();
    private ObjectMapperHelper objectMapperHelper;

    private FormatsMpw2Stubs() {
        objectMapperHelper = ObjectMapperHelper.getInstance();
    }

    public static FormatsMpw2Stubs getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS1W2 getFormatoMPMS1W2() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpw2/mock/MPMS1W2.json"), FormatoMPMS1W2.class);
    }

    public FormatoMPMS1W2 getFormatoMPMS1W2Empty() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpw2/mock/MPMS1W2-EMPTY.json"), FormatoMPMS1W2.class);
    }

    public List<FormatoMPMS2W2> getFormatoMPMS2W2s() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpw2/mock/MPMS2W2.json"), new TypeReference<List<FormatoMPMS2W2>>() {
        });
    }

    public List<FormatoMPMS3W2> getFormatoMPMS3W2s() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpw2/mock/MPMS3W2.json"), new TypeReference<List<FormatoMPMS3W2>>() {
        });
    }
}
