package com.bbva.pzic.cards.dao.model.mpg7.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMENG7;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMS1G7;
import com.bbva.pzic.cards.dao.model.mpg7.PeticionTransaccionMpg7;
import com.bbva.pzic.cards.dao.model.mpg7.RespuestaTransaccionMpg7;
import org.springframework.stereotype.Component;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpg7")
public class TransaccionMpg7Mock implements InvocadorTransaccion<PeticionTransaccionMpg7, RespuestaTransaccionMpg7> {

    public static final String TEST_EMPTY = "9999";
    public static final String TEST_NO_DATA = "9992";

    @Override
    public RespuestaTransaccionMpg7 invocar(PeticionTransaccionMpg7 peticion) {
        RespuestaTransaccionMpg7 response = new RespuestaTransaccionMpg7();
        response.setCodigoControl("OK");
        response.setCodigoRetorno("OK_COMMIT");

        final FormatoMPMENG7 format = peticion.getCuerpo().getParte(FormatoMPMENG7.class);
        final String ideTarj = format.getIdetarj();

        if (TEST_NO_DATA.equals(ideTarj)) {
            return response;
        } else if (TEST_EMPTY.equals(ideTarj)) {
            response.getCuerpo().getPartes().add(buildData(Boolean.TRUE));
        } else {
            response.getCuerpo().getPartes().add(buildData(Boolean.FALSE));

        }
        return response;
    }

    @Override
    public RespuestaTransaccionMpg7 invocarCache(PeticionTransaccionMpg7 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(boolean isEmpty) {
        FormatoMPMS1G7 formato;
        if (isEmpty) {
            formato = new FormatoMPMS1G7();
        } else {
            formato = FormatsMpg7Mock.getInstance().getFormatoMPMS1G7();
        }
        CopySalida copy = new CopySalida();
        copy.setCopy(formato);
        return copy;
    }
}
