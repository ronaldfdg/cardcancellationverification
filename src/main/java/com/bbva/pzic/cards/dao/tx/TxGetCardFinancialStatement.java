package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mpgh.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.ThreefoldOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Component("TxGetCardFinancialStatement")
public class TxGetCardFinancialStatement
        extends ThreefoldOutputFormat<InputGetCardFinancialStatement, FormatoMPME1GH, DTOIntCardStatement, FormatoMPMS1GH, FormatoMPMS2GH, FormatoMPMS3GH> {

    private static final Log LOG = LogFactory.getLog(TxGetCardFinancialStatement.class);

    @Autowired
    private ITxGetCardFinancialStatementMapper txGetCardFinancialStatementMapper;

    @Autowired
    public TxGetCardFinancialStatement(@Qualifier("transaccionMpgh") InvocadorTransaccion<PeticionTransaccionMpgh, RespuestaTransaccionMpgh> transaction) {
        super(transaction, PeticionTransaccionMpgh::new, DTOIntCardStatement::new, FormatoMPMS1GH.class, FormatoMPMS2GH.class, FormatoMPMS3GH.class);
    }

    @Override
    protected FormatoMPME1GH mapInput(InputGetCardFinancialStatement dtoIn) {
        LOG.info(" ... call TxGetCardFinancialStatement.mapDtoInToRequestFormat ... ");
        return txGetCardFinancialStatementMapper.mapIn(dtoIn);
    }

    @Override
    protected DTOIntCardStatement mapFirstOutputFormat(FormatoMPMS1GH formatOutput, InputGetCardFinancialStatement dtoIn, DTOIntCardStatement dtoOut) {
        LOG.info(" ... call TxGetCardFinancialStatement.mapResponseFormatToDtoOut ... ");
        return txGetCardFinancialStatementMapper.mapOut(formatOutput);
    }

    @Override
    protected DTOIntCardStatement mapSecondOutputFormat(FormatoMPMS2GH formatOutput, InputGetCardFinancialStatement dtoIn, DTOIntCardStatement dtoOut) {
        LOG.info(" ... call TxGetCardFinancialStatement.mapResponseFormatToDtoOut2 ... ");
        return txGetCardFinancialStatementMapper.mapOut2(formatOutput, dtoOut);
    }

    @Override
    protected DTOIntCardStatement mapThirdOutputFormat(FormatoMPMS3GH formatOutput, InputGetCardFinancialStatement dtoIn, DTOIntCardStatement dtoOut) {
        LOG.info(" ... call TxGetCardFinancialStatement.mapResponseFormatToDtoOut3 ... ");
        return txGetCardFinancialStatementMapper.mapOut3(formatOutput, dtoOut);
    }
}
