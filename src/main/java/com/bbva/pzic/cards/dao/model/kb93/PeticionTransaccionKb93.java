package com.bbva.pzic.cards.dao.model.kb93;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KB93</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKb93</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKb93</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBD.QGFD.FIX.QGDTCCT.KB93.D1190122
 * KB93SOLICITUD DE TARJETA ADIC - GLOMO  CN        KT1CKB93BVDCNPO KTECKB93            KB93  NS0000CNNNNN    SSTN    C   SNNSNNNN  NN                2018-12-07XP90540 2019-01-2114.23.01XP90540 2018-12-07-10.44.44.316730XP90540 0001-01-010001-01-01
 * FICHERO: PEBD.QGFD.FIX.QGDTFDX.KB93.D1190122
 * KB93KTSKB931KTSKB931KT1CKB931S                             XP90540 2019-01-22-08.53.31.841110XP90540 2019-01-22-08.53.31.841136
 * FICHERO: PEBD.QGFD.FIX.QGDTFDF.KTECKB93.D1190122
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�02�00017�IDTIPTA�ID TIPO DE TARJETA  �A�001�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�03�00018�IDSPROD�ID DEL SUBPRODUCTO  �A�002�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�04�00020�MONLINE�MONTO DE LA LINEA   �N�012�2�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�05�00032�DIVISAL�DIVISA DEL MONTO    �A�003�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�06�00035�IDDESTI�ID DEL DESTINO      �A�001�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�07�00036�IDBANCO�ID DE BANCO         �A�015�0�O�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�08�00051�IDOFICI�ID OFICINA          �A�015�0�O�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�09�00066�IDOFERT�ID OFERTA           �A�020�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�10�00086�NUMDOC �NUMERO DE DOCUMENTO �A�011�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�11�00097�TIPDOC �TIPO DE DOCUMENTO   �A�001�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�12�00098�NOMBRE1�PRIMER NOMBRE ADIC  �A�020�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�13�00118�NOMBRE2�SEGUNDO NOMBRE ADIC �A�020�0�O�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�14�00138�APELLI1�PRIMER APELLIDO ADIC�A�020�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�15�00158�APELLI2�SEGUNDO APELLIDO ADI�A�020�0�O�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�16�00178�IDESTCI�ID ESTADO CIVIL     �A�001�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�17�00179�IDPROFE�ID PROFESION        �A�030�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�18�00209�IDVINC �ID VINCULO          �A�010�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�19�00219�TIPCON1�TIPO CONTACTO ADIC 1�A�002�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�20�00221�VALCON1�VALOR CONTACTO ADI 1�A�080�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�21�00301�TIPCON2�TIPO CONTACTO ADIC 2�A�002�0�R�        �
 * KTECKB93�SOLICITUD TARJETA ADIC-GLOMO  �F�22�00311�22�00303�VALCON2�VALOR CONTACTO ADI 2�A�009�0�R�        �
 * FICHERO: PEBD.QGFD.FIX.QGDTFDF.KTSKB931.D1190122
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�01�00001�IDSOLIC�ID DE SOLICITUD     �A�025�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�02�00026�IDPRODU�ID DEL PRODUCTO     �A�002�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�03�00028�DESPROD�DESCRIPCION DEL PROD�A�030�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�04�00058�IDTIPTA�ID DE TIPO DE TARJET�A�001�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�05�00059�DESCTA �DESCRIP TIPO TARJETA�A�050�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�06�00109�IDDESTI�ID DEL DESTINO      �A�001�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�07�00110�NOMDEST�DESCRIPCION DESTINO �A�008�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�08�00118�MONLINE�MONTO DE LINEA      �N�012�2�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�09�00130�DIVISAL�DIVISA DE LA LINEA  �A�003�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�10�00133�IDBANCO�ID DEL BANCO        �A�015�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�11�00148�DESBANC�DESCRIPCION DE BANCO�A�080�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�12�00228�IDOFICI�ID DE OFICINA       �A�015�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�13�00243�DESCOFI�DESCRIPCION OFICINA �A�080�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�14�00323�NUMDOC �NUMERO DE DOCUMENTO �A�011�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�15�00334�TIPDOC �TIPO DE DOCUMENTO   �A�001�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�16�00335�DESTDO �DESCRIPCION TIPO DOC�A�050�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�17�00385�NOMBRE1�PRIMER NOMBRE ADIC  �A�020�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�18�00405�NOMBRE2�SEGUNDO NOMBRE ADIC �A�020�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�19�00425�APELLI1�PRIMER APELLIDO ADI1�A�020�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�20�00445�APELLI2�SEGUNDO APELLIDO AD2�A�020�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�21�00465�IDESTCI�ID ESTADO CIVIL     �A�001�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�22�00466�DESCEC �DESCRIP ESTADO CIVIL�A�030�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�23�00496�IDPROFE�ID PROFESION        �A�030�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�24�00526�DESCPRO�DESCRIPCION DE PROFE�A�050�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�25�00576�IDVINC �ID DE VINCULO       �A�010�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�26�00586�DESVINC�DESCRIP DE VINCULO  �A�050�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�27�00636�FECOPE �FECHA DE OPERACION  �A�010�0�S�        �
 * KTSKB931�SOLICITUD TARJETA ADIC-GLOMO  �X�28�00653�28�00646�HOROPE �HORA DE OPERACION   �A�008�0�S�        �
</pre></code>
 *
 * @see RespuestaTransaccionKb93
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KB93",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKb93.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKTECKB93.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKb93 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}