package com.bbva.pzic.cards.dao.rest;

import com.bbva.pzic.cards.business.dto.InputCreateCardReports;
import com.bbva.pzic.cards.dao.model.cardreports.ModelCreateCardReportsRequest;
import com.bbva.pzic.cards.dao.rest.mapper.IRestCreateCardReportsV0Mapper;
import com.bbva.pzic.cards.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class RestCreateCardReportsV0 extends RestPostConnection<ModelCreateCardReportsRequest, Object> {

    private static final String CREATE_CARD_REPORTS_V0_URL = "servicing.smc.configuration.SMGG20200411.backend.url";

    @Autowired
    private IRestCreateCardReportsV0Mapper mapper;

    @PostConstruct
    public void init() {
        useProxy = false;
    }

    public void invoke(final InputCreateCardReports dtoInt) {
        connect(CREATE_CARD_REPORTS_V0_URL, mapper.mapInBody(dtoInt));
    }

    @Override
    protected void evaluateResponse(Object response, int statusCode) {

    }
}
