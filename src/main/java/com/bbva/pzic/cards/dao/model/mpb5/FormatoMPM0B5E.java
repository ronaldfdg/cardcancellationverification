package com.bbva.pzic.cards.dao.model.mpb5;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * Formato de datos <code>MPM0B5E</code> de la transacci&oacute;n <code>MPB5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPM0B5E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPM0B5E {

    /**
     * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String idetarj;

    /**
     * <p>Campo <code>ESTTARJ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "ESTTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String esttarj;

    /**
     * <p>Campo <code>CODRAZ</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "CODRAZ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String codraz;

}