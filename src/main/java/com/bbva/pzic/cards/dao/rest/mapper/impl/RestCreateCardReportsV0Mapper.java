package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.model.cardreports.*;
import com.bbva.pzic.cards.dao.rest.mapper.IRestCreateCardReportsV0Mapper;
import com.bbva.pzic.cards.util.Converter;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.*;

@Mapper
public class RestCreateCardReportsV0Mapper implements IRestCreateCardReportsV0Mapper {

    @Override
    public ModelCreateCardReportsRequest mapInBody(final InputCreateCardReports input) {
        ModelCreateCardReportsRequest requestModel = new ModelCreateCardReportsRequest();
        requestModel.setData(mapInData(input));
        requestModel.setSecurity(mapInSecurity(input));
        return requestModel;
    }

    private ModelCardReport mapInData(final InputCreateCardReports input) {
        ModelCardReport modelCardReport = new ModelCardReport();
        modelCardReport.setId(input.getCardId());
        modelCardReport.setCanal(input.getChannel());
        modelCardReport.setTipoTarjeta(mapInTipoTarjeta(input.getCardType()));
        modelCardReport.setNumeroProducto(input.getNumber());
        modelCardReport.setTipoNumeroTarjeta(mapInTipoNumeroTarjeta(input.getNumberType()));
        modelCardReport.setMarca(mapInMarca(input.getBrandAssociation()));
        modelCardReport.setProducto(mapInProducto(input.getProduct()));
        modelCardReport.setSubproducto(mapInSubproducto(input.getProduct()));
        if (!CollectionUtils.isEmpty(input.getCurrencies())
                && input.getCurrencies().get(0) != null) {
            modelCardReport.setImporte(mapInImporte(input.getCurrencies().get(0), input.getGrantedCredits()));
            modelCardReport.setFlagMonedaPrincipal(Converter.booleanToInteger(input.getCurrencies().get(0).getIsMajor()));
        }
        modelCardReport.setFlagInnominada(Converter.booleanToInteger(input.getHasPrintedHolderName()));
        modelCardReport.setCliente(mapInCliente(input.getParticipants()));
        modelCardReport.setDatosPago(mapInDatosPago(input.getPaymentMethod()));
        if (!CollectionUtils.isEmpty(input.getRelatedContracts())
                && input.getRelatedContracts().get(0) != null) {
            modelCardReport.setCuentaCargo(input.getRelatedContracts().get(0).getNumber());
            modelCardReport.setTipoCuentaCargo(mapInTipoCuentaCargo(input.getRelatedContracts().get(0)));
        }
        modelCardReport.setDatosEntrega(mapInDatosEntrega(input.getDeliveries()));
        modelCardReport.setOficina(mapInOficina(input.getContractingBranch()));
        modelCardReport.setProgramaBeneficios(mapInProgramaBeneficios(input.getLoyaltyProgram()));
        modelCardReport.setEstado(input.getProposalStatus() == null ? null : Integer.parseInt(input.getProposalStatus()));
        modelCardReport.setOferta(mapInOferta(input.getOfferId()));
        return modelCardReport;
    }

    private ModelTipoTarjeta mapInTipoTarjeta(final DTOIntCardType cardType) {
        if (cardType == null) {
            return null;
        }
        ModelTipoTarjeta modelTipoTarjeta = new ModelTipoTarjeta();
        modelTipoTarjeta.setId(cardType.getId());
        return modelTipoTarjeta;
    }

    private ModelTipoNumeroTarjeta mapInTipoNumeroTarjeta(final DTOIntNumberType numberType) {
        if (numberType == null) {
            return null;
        }
        ModelTipoNumeroTarjeta modelTipoNumeroTarjeta = new ModelTipoNumeroTarjeta();
        modelTipoNumeroTarjeta.setId(numberType.getId());
        return modelTipoNumeroTarjeta;
    }

    private ModelMarca mapInMarca(final DTOIntBrandAssociation brandAssociation) {
        if (brandAssociation == null) {
            return null;
        }
        ModelMarca modelMarca = new ModelMarca();
        modelMarca.setId(brandAssociation.getId());
        return modelMarca;
    }

    private ModelProducto mapInProducto(final DTOIntProduct product) {
        if (product == null) {
            return null;
        }
        ModelProducto modelProducto = new ModelProducto();
        modelProducto.setId(product.getId());
        modelProducto.setNombre(product.getName());
        modelProducto.setSubproducto(mapInSubproductoId(product.getSubproduct()));
        return modelProducto;
    }

    private ModelSubproducto mapInSubproductoId(final DTOIntSubProduct subproduct) {
        if (subproduct == null) {
            return null;
        }
        ModelSubproducto modelSubproducto = new ModelSubproducto();
        modelSubproducto.setId(subproduct.getId());
        return modelSubproducto;
    }

    private ModelSubproducto mapInSubproducto(final DTOIntProduct product) {
        if (product == null || product.getSubproduct() == null) {
            return null;
        }
        ModelSubproducto modelSubproducto = new ModelSubproducto();
        modelSubproducto.setNombre(product.getSubproduct().getName());
        modelSubproducto.setDescripcion(product.getSubproduct().getDescription());
        return modelSubproducto;
    }


    private ModelCliente mapInCliente(final List<DTOIntParticipantReport> participantList) {
        if (CollectionUtils.isEmpty(participantList)
                || participantList.get(0) == null) {
            return null;
        }
        ModelCliente modelCliente = new ModelCliente();
        modelCliente.setCodigoCentral(participantList.get(0).getId());
        modelCliente.setPrimerNombre(participantList.get(0).getFirstName());
        modelCliente.setSegundoNombre(participantList.get(0).getMiddleName());
        modelCliente.setApellidoPaterno(participantList.get(0).getLastName());
        modelCliente.setApellidoMaterno(participantList.get(0).getSecondLastName());
        modelCliente.setDocumentoIdentidad(mapInDocumentoIdentidad(participantList.get(0).getIdentityDocuments()));
        modelCliente.setDatosContacto(mapInDatosContacto(participantList.get(0).getContactDetails()));
        return modelCliente;
    }


    private ModelDocumentoIdentidad mapInDocumentoIdentidad(final List<DTOIntIdentityDocument> identityDocumentList) {
        if (CollectionUtils.isEmpty(identityDocumentList)
                || identityDocumentList.get(0) == null) {
            return null;
        }
        ModelDocumentoIdentidad modelDocumentoIdentidad = new ModelDocumentoIdentidad();
        modelDocumentoIdentidad.setNumero(identityDocumentList.get(0).getNumber());
        modelDocumentoIdentidad.setId(identityDocumentList.get(0).getDocumentType() == null ? null : identityDocumentList.get(0).getDocumentType().getId());
        return modelDocumentoIdentidad;
    }

    private List<ModelDatoContacto> mapInDatosContacto(final List<DTOIntContactDetailReport> contactDetailList) {
        if (CollectionUtils.isEmpty(contactDetailList)) {
            return null;
        }
        return contactDetailList.stream().filter(Objects::nonNull).map(this::mapInDatoContacto).collect(Collectors.toList());
    }

    private ModelDatoContacto mapInDatoContacto(final DTOIntContactDetailReport dtoIntContactDetailReport) {
        ModelDatoContacto modelDatoContacto = new ModelDatoContacto();
        ModelTipoDatoContacto modelTipoDatoContacto = new ModelTipoDatoContacto();
        if (dtoIntContactDetailReport.getEmailContact() != null) {
            modelTipoDatoContacto.setId(dtoIntContactDetailReport.getEmailContact().getContactType());
            modelDatoContacto.setValor(dtoIntContactDetailReport.getEmailContact().getAddress());
        } else if (dtoIntContactDetailReport.getMobileContact() != null) {
            modelTipoDatoContacto.setId(dtoIntContactDetailReport.getMobileContact().getContactType());
            modelDatoContacto.setValor(dtoIntContactDetailReport.getMobileContact().getNumber());
        }
        modelDatoContacto.setTipo(modelTipoDatoContacto);
        return modelDatoContacto;
    }

    private ModelDatosPago mapInDatosPago(final DTOIntPaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }
        ModelDatosPago modelDatosPago = new ModelDatosPago();
        modelDatosPago.setId(paymentMethod.getId());
        if (paymentMethod.getFrecuency() != null) {
            modelDatosPago.setFrecuencia(paymentMethod.getFrecuency().getId());
            modelDatosPago.setDia(mapInDia(paymentMethod.getFrecuency().getDaysOfMonth()));
        }
        return modelDatosPago;
    }

    private Integer mapInDia(final DTOIntDaysOfMonth daysOfMonth) {
        if (daysOfMonth == null) {
            return null;
        }
        return Integer.parseInt(daysOfMonth.getDay());
    }


    private ModelImporte mapInImporte(final DTOIntCurrency currency, final List<DTOIntAmount> grantedCreditList) {
        ModelImporte modelImporte = new ModelImporte();
        modelImporte.setMoneda(currency.getCurrency());
        if (!CollectionUtils.isEmpty(grantedCreditList)
                && grantedCreditList.get(0) != null) {
            modelImporte.setMonto(grantedCreditList.get(0).getAmount());
        }
        return modelImporte;
    }

    private ModelTipoCuentaCargo mapInTipoCuentaCargo(final DTOIntRelatedContract dtoIntRelatedContract) {
        if (dtoIntRelatedContract.getNumberType() == null) {
            return null;
        }
        ModelTipoCuentaCargo modelTipoCuentaCargo = new ModelTipoCuentaCargo();
        modelTipoCuentaCargo.setId(dtoIntRelatedContract.getNumberType().getId());
        return modelTipoCuentaCargo;
    }

    private List<ModelDatoEntrega> mapInDatosEntrega(final List<DTOIntDelivery> deliveryList) {
        if (CollectionUtils.isEmpty(deliveryList)) {
            return null;
        }
        return deliveryList.stream().filter(Objects::nonNull).map(this::mapInDatoEntrega).collect(Collectors.toList());
    }

    private ModelDatoEntrega mapInDatoEntrega(final DTOIntDelivery dtoIntDelivery) {
        ModelDatoEntrega modelDatoEntrega = new ModelDatoEntrega();
        modelDatoEntrega.setId(dtoIntDelivery.getId());
        modelDatoEntrega.setTipo(mapInTipoDatoEntrega(dtoIntDelivery.getServiceType()));
        modelDatoEntrega.setContacto(mapInContacto(dtoIntDelivery.getContact()));
        modelDatoEntrega.setDireccion(mapInDireccion(dtoIntDelivery.getAddress()));
        modelDatoEntrega.setDestino(mapInDestino(dtoIntDelivery.getDestination()));
        modelDatoEntrega.setOficina(mapInOficinaEntrega(dtoIntDelivery.getBranch()));
        return modelDatoEntrega;
    }

    private ModelTipoDatoEntrega mapInTipoDatoEntrega(final DTOIntServiceType serviceType) {
        if (serviceType == null) {
            return null;
        }
        ModelTipoDatoEntrega modelTipoDatoEntrega = new ModelTipoDatoEntrega();
        modelTipoDatoEntrega.setId(serviceType.getId());
        return modelTipoDatoEntrega;
    }

    private ModelContacto mapInContacto(final DTOIntContact contact) {
        if (contact == null) {
            return null;
        }
        ModelContacto modelContacto = new ModelContacto();
        modelContacto.setTipoContacto(mapInTipoContacto(contact.getContactType()));
        modelContacto.setId(contact.getId());
        if (contact.getContact() != null) {
            if (SPECIFIC.equalsIgnoreCase(contact.getContactType())) {
                modelContacto.setTipo(mapInTipoDetalleContacto(contact.getContact().getContactDetailType()));
            }
            if (contact.getContact().getAddress() != null) {
                modelContacto.setValor(contact.getContact().getAddress());
            }
        }

        return modelContacto;
    }

    private ModelTipoContacto mapInTipoContacto(final String contactType) {
        if (contactType == null) {
            return null;
        }
        ModelTipoContacto modelTipoContacto = new ModelTipoContacto();
        modelTipoContacto.setId(contactType);
        return modelTipoContacto;
    }


    private ModelTipoDetalleContacto mapInTipoDetalleContacto(final String contactDetailType) {
        ModelTipoDetalleContacto modelTipoDetalleContacto = new ModelTipoDetalleContacto();
        modelTipoDetalleContacto.setId(contactDetailType);
        return modelTipoDetalleContacto;
    }

    private ModelDireccion mapInDireccion(final DTOIntAddress address) {
        if (address == null) {
            return null;
        }
        ModelDireccion modelDireccion = new ModelDireccion();
        modelDireccion.setTipo(mapInTipoDireccion(address.getAddressType()));
        if (STORED.equalsIgnoreCase(address.getAddressType())) {
            modelDireccion.setId(address.getId());
        } else if (SPECIFIC.equalsIgnoreCase(address.getAddressType())) {
            if (address.getLocation() != null
                    && !CollectionUtils.isEmpty(address.getLocation().getAddressComponents())
                    && address.getLocation().getAddressComponents().get(0) != null) {
                modelDireccion.setDireccionDetallada(mapInDireccionDetalladaList(address.getLocation().getAddressComponents()));
                modelDireccion.setDireccionCompleta(address.getLocation().getAddressComponents().get(0).getFormattedAddress());
            }
        }
        return modelDireccion;
    }

    private ModelTipoDireccion mapInTipoDireccion(final String addressType) {
        if (addressType == null) {
            return null;
        }
        ModelTipoDireccion modelTipoDireccion = new ModelTipoDireccion();
        modelTipoDireccion.setId(addressType);
        return modelTipoDireccion;
    }

    private List<ModelDireccionDetallada> mapInDireccionDetalladaList(final List<DTOIntAddressComponents> addressComponentList) {
        return addressComponentList.stream().filter(Objects::nonNull).map(this::mapInDireccionDetallada).collect(Collectors.toList());
    }

    private ModelDireccionDetallada mapInDireccionDetallada(final DTOIntAddressComponents dtoIntAddressComponents) {
        ModelDireccionDetallada modelDireccionDetallada = new ModelDireccionDetallada();
        modelDireccionDetallada.setTipo(mapInTipoDireccionDetallada(dtoIntAddressComponents.getComponentTypes()));
        modelDireccionDetallada.setCodigo(dtoIntAddressComponents.getCode());
        modelDireccionDetallada.setNombre(dtoIntAddressComponents.getName());
        return modelDireccionDetallada;
    }

    private ModelTipoDireccionDetallada mapInTipoDireccionDetallada(final List<String> componentTypes) {
        if (CollectionUtils.isEmpty(componentTypes)) {
            return null;
        }
        ModelTipoDireccionDetallada modelTipoDireccionDetallada = new ModelTipoDireccionDetallada();
        modelTipoDireccionDetallada.setId(componentTypes.get(0));
        return modelTipoDireccionDetallada;
    }

    private ModelDestino mapInDestino(final DTOIntDestination destination) {
        if (destination == null) {
            return null;
        }
        ModelDestino modelDestino = new ModelDestino();
        modelDestino.setId(destination.getId());
        modelDestino.setNombre(destination.getName());
        return modelDestino;
    }

    private ModelOficina mapInOficinaEntrega(final DTOIntBranch branch) {
        if (branch == null) {
            return null;
        }
        ModelOficina modelOficina = new ModelOficina();
        modelOficina.setId(branch.getId());
        modelOficina.setNombre(branch.getName());
        return modelOficina;
    }


    private ModelOficina mapInOficina(final DTOIntContractingBranch contractingBranch) {
        if (contractingBranch == null) {
            return null;
        }
        ModelOficina modelOficina = new ModelOficina();
        modelOficina.setId(contractingBranch.getId());
        modelOficina.setNombre(contractingBranch.getName());
        return modelOficina;
    }

    private ModelProgramaBeneficios mapInProgramaBeneficios(final DTOIntLoyaltyProgram loyaltyProgram) {
        if (loyaltyProgram == null) {
            return null;
        }
        ModelProgramaBeneficios modelProgramaBeneficios = new ModelProgramaBeneficios();
        modelProgramaBeneficios.setId(loyaltyProgram.getId());
        modelProgramaBeneficios.setNombre(loyaltyProgram.getDescription());
        return modelProgramaBeneficios;
    }

    private ModelOferta mapInOferta(final String offerId) {
        if (offerId == null) {
            return null;
        }
        ModelOferta modelOferta = new ModelOferta();
        modelOferta.setId(offerId);
        return modelOferta;
    }


    private ModelSecurity mapInSecurity(final InputCreateCardReports input) {
        ModelSecurity modelSecurity = new ModelSecurity();
        modelSecurity.setUsuario(input.getUser());
        modelSecurity.setPassword(input.getPassword());
        return modelSecurity;
    }
}
