package com.bbva.pzic.cards.dao.model.mpl1;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Formato de datos <code>MPMS1L1</code> de la transacci&oacute;n <code>MPL1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1L1")
@RooJavaBean
@RooSerializable
public class FormatoMPMS1L1 {

    /**
     * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String numtarj;

    /**
     * <p>Campo <code>FECVENC</code>, &iacute;ndice: <code>2</code>, tipo: <code>FECHA</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 2, nombre = "FECVENC", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecvenc;

    /**
     * <p>Campo <code>NOMBCLI</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 3, nombre = "NOMBCLI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String nombcli;

    /**
     * <p>Campo <code>SDISPON</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "SDISPON", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal sdispon;

    /**
     * <p>Campo <code>MDISPON</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "MDISPON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String mdispon;

    /**
     * <p>Campo <code>SDISPUE</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 6, nombre = "SDISPUE", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal sdispue;

    /**
     * <p>Campo <code>MDISPUE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "MDISPUE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String mdispue;

    /**
     * <p>Campo <code>SDISPUB</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "SDISPUB", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal sdispub;

    /**
     * <p>Campo <code>MDISPUB</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "MDISPUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String mdispub;

    /**
     * <p>Campo <code>IPROTAR</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "IPROTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String iprotar;

    /**
     * <p>Campo <code>DPROTAR</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "DPROTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String dprotar;

    /**
     * <p>Campo <code>ILIMCRE</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 12, nombre = "ILIMCRE", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
    private BigDecimal ilimcre;

    /**
     * <p>Campo <code>DLIMCRE</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "DLIMCRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String dlimcre;

    /**
     * <p>Campo <code>TIPTARJ</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "TIPTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String tiptarj;

    /**
     * <p>Campo <code>DTITARJ</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 15, nombre = "DTITARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String dtitarj;

    /**
     * <p>Campo <code>SOPFISS</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "SOPFISS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String sopfiss;

    /**
     * <p>Campo <code>DSOPFIS</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "DSOPFIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String dsopfis;

    /**
     * <p>Campo <code>IDTNUCO</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "IDTNUCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idtnuco;

    /**
     * <p>Campo <code>DETNUCO</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "DETNUCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String detnuco;

    /**
     * <p>Campo <code>ESTARJS</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "ESTARJS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String estarjs;

    /**
     * <p>Campo <code>DESTARJ</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "DESTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String destarj;

    /**
     * <p>Campo <code>IDMATAR</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "IDMATAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idmatar;

    /**
     * <p>Campo <code>NOMATAR</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "NOMATAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String nomatar;

    /**
     * <p>Campo <code>IDCOREL</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 24, nombre = "IDCOREL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String idcorel;

    /**
     * <p>Campo <code>NUCOREL</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 25, nombre = "NUCOREL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String nucorel;

    /**
     * <p>Campo <code>IDTCORE</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 26, nombre = "IDTCORE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idtcore;

    /**
     * <p>Campo <code>DETCORE</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 27, nombre = "DETCORE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String detcore;

    /**
     * <p>Campo <code>IDIMGT1</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 28, nombre = "IDIMGT1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String idimgt1;

    /**
     * <p>Campo <code>NIMGTA1</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 29, nombre = "NIMGTA1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String nimgta1;

    /**
     * <p>Campo <code>IMGTAR1</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 30, nombre = "IMGTAR1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 143, longitudMaxima = 143)
    private String imgtar1;

    /**
     * <p>Campo <code>IDIMGT2</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 31, nombre = "IDIMGT2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String idimgt2;

    /**
     * <p>Campo <code>NIMGTA2</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 32, nombre = "NIMGTA2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String nimgta2;

    /**
     * <p>Campo <code>IMGTAR2</code>, &iacute;ndice: <code>33</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 33, nombre = "IMGTAR2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 143, longitudMaxima = 143)
    private String imgtar2;

    /**
     * <p>Campo <code>IDEBLOQ</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 34, nombre = "IDEBLOQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idebloq;

    /**
     * <p>Campo <code>DESBLOQ</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 35, nombre = "DESBLOQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String desbloq;

    /**
     * <p>Campo <code>IDERAZO</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 36, nombre = "IDERAZO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String iderazo;

    /**
     * <p>Campo <code>DESRAZO</code>, &iacute;ndice: <code>37</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 37, nombre = "DESRAZO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String desrazo;

    /**
     * <p>Campo <code>FECBLOQ</code>, &iacute;ndice: <code>38</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 38, nombre = "FECBLOQ", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecbloq;

    /**
     * <p>Campo <code>IDACTBL</code>, &iacute;ndice: <code>39</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 39, nombre = "IDACTBL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idactbl;

    /**
     * <p>Campo <code>CODACTV</code>, &iacute;ndice: <code>40</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 40, nombre = "CODACTV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String codactv;

    /**
     * <p>Campo <code>INDACTV</code>, &iacute;ndice: <code>41</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 41, nombre = "INDACTV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indactv;

    /**
     * <p>Campo <code>IDPART</code>, &iacute;ndice: <code>42</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 42, nombre = "IDPART", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String idpart;

    /**
     * <p>Campo <code>NOMPART</code>, &iacute;ndice: <code>43</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 43, nombre = "NOMPART", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String nompart;

    /**
     * <p>Campo <code>APEPART</code>, &iacute;ndice: <code>44</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 44, nombre = "APEPART", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String apepart;

    /**
     * <p>Campo <code>TIPPART</code>, &iacute;ndice: <code>45</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 45, nombre = "TIPPART", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String tippart;

    /**
     * <p>Campo <code>DESPART</code>, &iacute;ndice: <code>46</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 46, nombre = "DESPART", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String despart;

    /**
     * <p>Campo <code>CODRETI</code>, &iacute;ndice: <code>47</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 47, nombre = "CODRETI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String codreti;

    /**
     * <p>Campo <code>INDRETI</code>, &iacute;ndice: <code>48</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 48, nombre = "INDRETI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indreti;

    /**
     * <p>Campo <code>CODINTE</code>, &iacute;ndice: <code>49</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 49, nombre = "CODINTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String codinte;

    /**
     * <p>Campo <code>INDINTE</code>, &iacute;ndice: <code>50</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 50, nombre = "INDINTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indinte;

    /**
     * <p>Campo <code>CODCOEX</code>, &iacute;ndice: <code>51</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 51, nombre = "CODCOEX", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String codcoex;

    /**
     * <p>Campo <code>INDCOEX</code>, &iacute;ndice: <code>52</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 52, nombre = "INDCOEX", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indcoex;

    /**
     * <p>Campo <code>FECSITT</code>, &iacute;ndice: <code>53</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 53, nombre = "FECSITT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecsitt;

    /**
     * <p>Campo <code>IDCORTV</code>, &iacute;ndice: <code>54</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 54, nombre = "IDCORTV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String idcortv;

    /**
     * <p>Campo <code>NUCORTV</code>, &iacute;ndice: <code>55</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 55, nombre = "NUCORTV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String nucortv;

    /**
     * <p>Campo <code>IDTCOTV</code>, &iacute;ndice: <code>56</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 56, nombre = "IDTCOTV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idtcotv;

    /**
     * <p>Campo <code>DETCOTV</code>, &iacute;ndice: <code>57</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 57, nombre = "DETCOTV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String detcotv;

    /**
     * <p>Campo <code>CODSOBR</code>, &iacute;ndice: <code>58</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 58, nombre = "CODSOBR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String codsobr;

    /**
     * <p>Campo <code>INDSOBR</code>, &iacute;ndice: <code>59</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 59, nombre = "INDSOBR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indsobr;

}
