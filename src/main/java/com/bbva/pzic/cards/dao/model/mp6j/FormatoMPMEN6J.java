package com.bbva.pzic.cards.dao.model.mp6j;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMEN6J</code> de la transacci&oacute;n <code>MP6J</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMEN6J")
@RooJavaBean
@RooSerializable
public class FormatoMPMEN6J {

	/**
	 * <p>Campo <code>MCINDOP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "MCINDOP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String mcindop;
	
	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String idetarj;
	
	/**
	 * <p>Campo <code>IDENTIF</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDENTIF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String identif;
	
}