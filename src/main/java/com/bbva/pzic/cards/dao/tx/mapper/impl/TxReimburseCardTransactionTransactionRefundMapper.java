package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.Status;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMEN6J;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMS16J;
import com.bbva.pzic.cards.dao.tx.mapper.ITxReimburseCardTransactionTransactionRefundMapper;
import com.bbva.pzic.cards.facade.v1.dto.RefundType;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.cards.util.Enums.TRANSACTIONREFUND_REFUNDTYPE_ID;
import static com.bbva.pzic.cards.util.Enums.TRANSACTIONREFUND_STATUS_ID;

@Component
public class TxReimburseCardTransactionTransactionRefundMapper implements ITxReimburseCardTransactionTransactionRefundMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPMEN6J mapIn(final InputReimburseCardTransactionTransactionRefund input) {
        FormatoMPMEN6J format = new FormatoMPMEN6J();
        format.setIdetarj(input.getCardId());
        format.setIdentif(input.getTransactionId());
        if (input.getRefundType() != null) {
            format.setMcindop(input.getRefundType().getId());
        }
        return format;
    }

    @Override
    public ReimburseCardTransactionTransactionRefund mapOut(final FormatoMPMS16J formatOutput) {
        if (formatOutput == null) {
            return null;
        }

        ReimburseCardTransactionTransactionRefund output = new ReimburseCardTransactionTransactionRefund();
        output.setRefundType(mapOutRefundType(formatOutput));
        output.setCard(mapOutCard(formatOutput));
        output.setTransaction(mapOutTransaction(formatOutput));
        output.setStatus(mapOutStatus(formatOutput));
        return output;
    }

    private Status mapOutStatus(final FormatoMPMS16J formatOutput) {
        if (StringUtils.isEmpty(formatOutput.getCodope()) && StringUtils.isEmpty(formatOutput.getDesoper())) {
            return null;
        }

        Status status = new Status();
        status.setId(translator.translateBackendEnumValueStrictly(TRANSACTIONREFUND_STATUS_ID, formatOutput.getCodope()));
        status.setDescription(formatOutput.getDesoper());
        return status;
    }

    private Transaction mapOutTransaction(final FormatoMPMS16J formatOutput) {
        if (StringUtils.isEmpty(formatOutput.getIdentif())) {
            return null;
        }

        Transaction transaction = new Transaction();
        transaction.setId(formatOutput.getIdentif());
        return transaction;
    }

    private Card mapOutCard(final FormatoMPMS16J formatOutput) {
        if (StringUtils.isEmpty(formatOutput.getIdetarj())) {
            return null;
        }

        Card card = new Card();
        card.setId(formatOutput.getIdetarj());
        return card;
    }

    private RefundType mapOutRefundType(final FormatoMPMS16J formatOutput) {
        if (StringUtils.isEmpty(formatOutput.getMcindop()) && StringUtils.isEmpty(formatOutput.getMcindes())) {
            return null;
        }
        RefundType refundType = new RefundType();
        refundType.setId(translator.translateBackendEnumValueStrictly(TRANSACTIONREFUND_REFUNDTYPE_ID, formatOutput.getMcindop()));
        refundType.setName(formatOutput.getMcindes());
        return refundType;
    }

}
