package com.bbva.pzic.cards.dao.model.mprt;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPRMRT1</code> de la transacci&oacute;n <code>MPRT</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPRMRT1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPRMRT1 {

	/**
	 * <p>Campo <code>TARJNUE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TARJNUE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String tarjnue;

}