package com.bbva.pzic.cards.dao.model.kb94;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>KB94</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKb94</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKb94</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: &apos;PEBT.QGFD.FIX.QGDTCCT.KB94.D1191204&apos;
 * KB94AFILIACION EECC SERV PAAS          CN        KT1CKB94     01 KTECKB94            KB94  NS0000CNNNNN    SSTN    C   SNNNSNNN  NN                2019-11-18P027589 2019-12-0215.57.38P027589 2019-11-18-17.56.30.169875P027589 0001-01-010001-01-01
 * FICHERO: &apos;PEBT.QGFD.FIX.QGDTFDF.KTECKB94.D1191204&apos;
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�01�00001�NUMTAR �NUMERO TARJETA      �A�020�0�R�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�02�00021�TIPENT �TIPO DE ENTREGA     �A�001�0�R�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�03�00022�NATCONT�NATURALEZA CONTACTO �A�001�0�R�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�04�00023�TIPCONT�TIPO DE CONTACTO    �A�001�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�05�00024�NUMFIJO�NUMERO TELEFONO FIJO�A�007�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�06�00031�TIPONUM�TIPO TELETONO FIJO  �A�001�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�07�00032�CODPAIS�CODIGO PAIS         �A�004�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�08�00036�CODREG �CODIGO REGION NUM FI�A�003�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�09�00039�NUMCEL �NUMERO MOVIL        �A�009�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�10�00048�COMPCEL�COMPANIA MOVIL      �A�001�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�11�00049�CORREO �CORREO ELECTRONICO  �A�080�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�12�00129�USUARI �USER CONTAC EN RED  �A�040�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�13�00169�REDSOC �RED SOCIAL CONTACTO �A�001�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�14�00170�IDCONT �IDENTIFICADOR CONTAC�N�004�0�O�        �
 * KTECKB94�ENVIO ESTADOS CONTRATO-ENTRADA�F�15�00174�15�00174�TIPDEST�TIPO DE DESTINO     �A�001�0�R�        �
 * FICHERO: &apos;PEBT.QGFD.FIX.QGDTFDF.KTSCKB94.D1191204&apos;
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�01�00001�IDAFIL �ID DE AFILIACION    �A�021�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�02�00022�TIPENT �TIPO DE ENTREGA     �A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�03�00023�NATCONT�NATURALEZA CONTACTO �A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�04�00024�TIPCONT�TIPO DE CONTACTO    �A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�05�00025�NUMFIJO�NUMERO DE TELF FIJO �A�007�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�06�00032�TIPONUM�TIPO DE TELF FIJO   �A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�07�00033�CODPAIS�CODIGO PAIS TELF FIJ�A�004�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�08�00037�DESPAIS�NOMBRE PAIS DEL TEFL�A�020�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�09�00057�CODREG �CODIGO DE REGION NUM�A�003�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�10�00060�NUMCEL �NUMERO MOVIL        �A�009�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�11�00069�COMPCEL�COMPANIA MOVIL      �A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�12�00070�COMPMOV�NOMBRE COMPANIA MOVI�A�020�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�13�00090�CORREO �CORREO ELECTRONICO  �A�080�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�14�00170�USUARI �USER CONTACTO DE RED�A�040�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�15�00210�REDSOC �RED SOCIAL DE CONTAC�A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�16�00211�IDCONT �IDENTIFICADOR CONTAC�N�004�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�17�00215�TIPDEST�TIPO DE DESTINO     �A�001�0�S�        �
 * KTSCKB94�ENVIA ESTADOS CONTRATO-SALIDA �X�18�00235�18�00216�DESCTD �DESCRIPCION TIP DEST�A�020�0�S�        �
 * FICHERO: &apos;PEBT.QGFD.FIX.QGDTFDX.KB94.D1191204&apos;
 * KB94KTSCKB94KTSCKB94KT1CKB941S                             P027589 2019-11-20-16.48.03.520147P027589 2019-12-02-16.21.57.191683
</pre></code>
 *
 * @see RespuestaTransaccionKb94
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KB94",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKb94.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKTECKB94.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionKb94 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
