package com.bbva.pzic.cards.dao.model.mpcv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPCV</code>
 *
 * @see PeticionTransaccionMpcv
 * @see RespuestaTransaccionMpcv
 */
@Component
public class TransaccionMpcv implements InvocadorTransaccion<PeticionTransaccionMpcv,RespuestaTransaccionMpcv> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpcv invocar(PeticionTransaccionMpcv transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpcv.class, RespuestaTransaccionMpcv.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpcv invocarCache(PeticionTransaccionMpcv transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpcv.class, RespuestaTransaccionMpcv.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
