// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mcdv;

import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMENDV;

privileged aspect FormatoMPMENDV_Roo_JavaBean {
    
    /**
     * Gets numtarj value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getNumtarj() {
        return this.numtarj;
    }
    
    /**
     * Sets numtarj value
     * 
     * @param numtarj
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setNumtarj(String numtarj) {
        this.numtarj = numtarj;
        return this;
    }
    
    /**
     * Gets keypb01 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb01() {
        return this.keypb01;
    }
    
    /**
     * Sets keypb01 value
     * 
     * @param keypb01
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb01(String keypb01) {
        this.keypb01 = keypb01;
        return this;
    }
    
    /**
     * Gets keypb02 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb02() {
        return this.keypb02;
    }
    
    /**
     * Sets keypb02 value
     * 
     * @param keypb02
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb02(String keypb02) {
        this.keypb02 = keypb02;
        return this;
    }
    
    /**
     * Gets keypb03 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb03() {
        return this.keypb03;
    }
    
    /**
     * Sets keypb03 value
     * 
     * @param keypb03
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb03(String keypb03) {
        this.keypb03 = keypb03;
        return this;
    }
    
    /**
     * Gets keypb04 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb04() {
        return this.keypb04;
    }
    
    /**
     * Sets keypb04 value
     * 
     * @param keypb04
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb04(String keypb04) {
        this.keypb04 = keypb04;
        return this;
    }
    
    /**
     * Gets keypb05 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb05() {
        return this.keypb05;
    }
    
    /**
     * Sets keypb05 value
     * 
     * @param keypb05
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb05(String keypb05) {
        this.keypb05 = keypb05;
        return this;
    }
    
    /**
     * Gets keypb06 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb06() {
        return this.keypb06;
    }
    
    /**
     * Sets keypb06 value
     * 
     * @param keypb06
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb06(String keypb06) {
        this.keypb06 = keypb06;
        return this;
    }
    
    /**
     * Gets keypb07 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb07() {
        return this.keypb07;
    }
    
    /**
     * Sets keypb07 value
     * 
     * @param keypb07
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb07(String keypb07) {
        this.keypb07 = keypb07;
        return this;
    }
    
    /**
     * Gets keypb08 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb08() {
        return this.keypb08;
    }
    
    /**
     * Sets keypb08 value
     * 
     * @param keypb08
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb08(String keypb08) {
        this.keypb08 = keypb08;
        return this;
    }
    
    /**
     * Gets keypb09 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb09() {
        return this.keypb09;
    }
    
    /**
     * Sets keypb09 value
     * 
     * @param keypb09
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb09(String keypb09) {
        this.keypb09 = keypb09;
        return this;
    }
    
    /**
     * Gets keypb10 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb10() {
        return this.keypb10;
    }
    
    /**
     * Sets keypb10 value
     * 
     * @param keypb10
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb10(String keypb10) {
        this.keypb10 = keypb10;
        return this;
    }
    
    /**
     * Gets keypb11 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb11() {
        return this.keypb11;
    }
    
    /**
     * Sets keypb11 value
     * 
     * @param keypb11
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb11(String keypb11) {
        this.keypb11 = keypb11;
        return this;
    }
    
    /**
     * Gets keypb12 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb12() {
        return this.keypb12;
    }
    
    /**
     * Sets keypb12 value
     * 
     * @param keypb12
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb12(String keypb12) {
        this.keypb12 = keypb12;
        return this;
    }
    
    /**
     * Gets keypb13 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb13() {
        return this.keypb13;
    }
    
    /**
     * Sets keypb13 value
     * 
     * @param keypb13
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb13(String keypb13) {
        this.keypb13 = keypb13;
        return this;
    }
    
    /**
     * Gets keypb14 value
     * 
     * @return String
     */
    public String FormatoMPMENDV.getKeypb14() {
        return this.keypb14;
    }
    
    /**
     * Sets keypb14 value
     * 
     * @param keypb14
     * @return FormatoMPMENDV
     */
    public FormatoMPMENDV FormatoMPMENDV.setKeypb14(String keypb14) {
        this.keypb14 = keypb14;
        return this;
    }
    
}
