package com.bbva.pzic.cards.dao.model.mp3g;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMS23G</code> de la transacci&oacute;n <code>MP3G</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS23G")
@RooJavaBean
@RooSerializable
public class FormatoMPMS23G {
	
	/**
	 * <p>Campo <code>MONEDA1</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "MONEDA1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moneda1;
	
	/**
	 * <p>Campo <code>AMITYP1</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "AMITYP1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amityp1;
	
	/**
	 * <p>Campo <code>AMCTYP1</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "AMCTYP1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String amctyp1;
	
	/**
	 * <p>Campo <code>IMPCANC</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "IMPCANC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal impcanc;
	
	/**
	 * <p>Campo <code>CCAPLIQ</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 5, nombre = "CCAPLIQ", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal ccapliq;
	
	/**
	 * <p>Campo <code>TOTCOBR</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "TOTCOBR", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal totcobr;
	
	/**
	 * <p>Campo <code>IMPAUTO</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 7, nombre = "IMPAUTO", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal impauto;
	
	/**
	 * <p>Campo <code>CITOTFC</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "CITOTFC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal citotfc;
	
	/**
	 * <p>Campo <code>CAPVIGE</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "CAPVIGE", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal capvige;
	
	/**
	 * <p>Campo <code>IDINAVA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "IDINAVA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idinava;
	
	/**
	 * <p>Campo <code>INTAVAN</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 11, nombre = "INTAVAN", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal intavan;
	
	/**
	 * <p>Campo <code>IDINCOM</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "IDINCOM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idincom;
	
	/**
	 * <p>Campo <code>INTCOMP</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "INTCOMP", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal intcomp;
	
	/**
	 * <p>Campo <code>IDINCUO</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "IDINCUO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idincuo;
	
	/**
	 * <p>Campo <code>INTINIC</code>, &iacute;ndice: <code>15</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 15, nombre = "INTINIC", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal intinic;
	
	/**
	 * <p>Campo <code>IDCOMEM</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "IDCOMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcomem;
	
	/**
	 * <p>Campo <code>CUOTVIG</code>, &iacute;ndice: <code>17</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 17, nombre = "CUOTVIG", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal cuotvig;
	
	/**
	 * <p>Campo <code>IDCODIS</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "IDCODIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcodis;
	
	/**
	 * <p>Campo <code>COMFLAT</code>, &iacute;ndice: <code>19</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 19, nombre = "COMFLAT", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal comflat;
	
	/**
	 * <p>Campo <code>IDCOGRI</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "IDCOGRI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcogri;
	
	/**
	 * <p>Campo <code>COMGRIF</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 21, nombre = "COMGRIF", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal comgrif;
	
	/**
	 * <p>Campo <code>IDCOMAN</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "IDCOMAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcoman;
	
	/**
	 * <p>Campo <code>COMMANT</code>, &iacute;ndice: <code>23</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 23, nombre = "COMMANT", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal commant;
	
	/**
	 * <p>Campo <code>IDCOSOB</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "IDCOSOB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcosob;
	
	/**
	 * <p>Campo <code>COMSOBR</code>, &iacute;ndice: <code>25</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 25, nombre = "COMSOBR", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal comsobr;
	
	/**
	 * <p>Campo <code>IDCOEC1</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "IDCOEC1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcoec1;
	
	/**
	 * <p>Campo <code>PORVIGE</code>, &iacute;ndice: <code>27</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 27, nombre = "PORVIGE", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal porvige;
	
	/**
	 * <p>Campo <code>SEGDESG</code>, &iacute;ndice: <code>28</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 28, nombre = "SEGDESG", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal segdesg;
	
	/**
	 * <p>Campo <code>IDCOEC2</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 29, nombre = "IDCOEC2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcoec2;
	
	/**
	 * <p>Campo <code>SEGRIES</code>, &iacute;ndice: <code>30</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 30, nombre = "SEGRIES", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal segries;
	
	/**
	 * <p>Campo <code>CISCPET</code>, &iacute;ndice: <code>31</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 31, nombre = "CISCPET", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal ciscpet;
	
	/**
	 * <p>Campo <code>IDCOMPE</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "IDCOMPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcompe;
	
	/**
	 * <p>Campo <code>COMPENA</code>, &iacute;ndice: <code>33</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 33, nombre = "COMPENA", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal compena;
	
	/**
	 * <p>Campo <code>EXCLINE</code>, &iacute;ndice: <code>34</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 34, nombre = "EXCLINE", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal excline;
	
	/**
	 * <p>Campo <code>IDEXIMO</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 35, nombre = "IDEXIMO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ideximo;
	
	/**
	 * <p>Campo <code>MORAVIG</code>, &iacute;ndice: <code>36</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 36, nombre = "MORAVIG", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal moravig;
	
	/**
	 * <p>Campo <code>IDEXICO</code>, &iacute;ndice: <code>37</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 37, nombre = "IDEXICO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idexico;
	
	/**
	 * <p>Campo <code>COMPVIG</code>, &iacute;ndice: <code>38</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 38, nombre = "COMPVIG", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal compvig;
	
}