package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardShipment;
import com.bbva.pzic.cards.dao.apx.mapper.IApxGetCardShipmentMapper;
import com.bbva.pzic.cards.dao.model.pecpt004_1.PeticionTransaccionPecpt004_1;
import com.bbva.pzic.cards.dao.model.pecpt004_1.RespuestaTransaccionPecpt004_1;
import com.bbva.pzic.cards.facade.v0.dto.Shipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApxGetCardShipment {

    @Autowired
    private IApxGetCardShipmentMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionPecpt004_1, RespuestaTransaccionPecpt004_1> transaccion;

    public Shipment invoke(final InputGetCardShipment input) {
        PeticionTransaccionPecpt004_1 request = mapper.mapIn(input);
        RespuestaTransaccionPecpt004_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
