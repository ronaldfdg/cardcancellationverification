package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardShipmentAddressMapper;
import com.bbva.pzic.cards.dao.model.pecpt002_1.*;
import com.bbva.pzic.cards.facade.v0.dto.AddressComponent;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.SPECIFIC;
import static com.bbva.pzic.cards.util.Constants.STORED;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ApxCreateCardShipmentAddressMapper implements IApxCreateCardShipmentAddressMapper {

    private static final Log LOG = LogFactory.getLog(ApxCreateCardShipmentAddressMapper.class);

    @Override
    public PeticionTransaccionPecpt002_1 mapIn(final InputCreateCardShipmentAddress input) {
        PeticionTransaccionPecpt002_1 peticion = new PeticionTransaccionPecpt002_1();
        peticion.setShipmentid(input.getShipmentId());
        peticion.setEntityin(mapInEntityIn(input));

        return peticion;
    }

    private Entityin mapInEntityIn(final InputCreateCardShipmentAddress input) {
        Entityin entityin = null;

        if (SPECIFIC.equalsIgnoreCase(input.getAddressType())) {
            entityin = mapSpecificShipment(input.getShipmentAddress());
        }

        if (STORED.equalsIgnoreCase(input.getAddressType())) {
            entityin = mapStoredShipment(input.getShipmentAddress());
        }

        if (entityin == null) {
            return null;
        }

        entityin.setAddresstype(input.getAddressType());
        return entityin;
    }

    private Entityin mapStoredShipment(DTOIntAddressType shipmentAddress) {
        try {
            DTOIntStoredAddressType dtoIntStoredAddressType = (DTOIntStoredAddressType) shipmentAddress;
            if (dtoIntStoredAddressType == null) {
                return null;
            }

            Entityin entityin = new Entityin();
            entityin.setId(dtoIntStoredAddressType.getId());
            entityin.setDestination(mapInDestination(dtoIntStoredAddressType));
            return entityin;
        } catch (ClassCastException ex) {
            LOG.error("... Error trying to parse Stored addressType to StoredShipmentAddress ...");
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }
    }

    private Entityin mapSpecificShipment(final DTOIntAddressType shipmentAddress) {
        try {
            DTOIntSpecificAddressType dtoIntSpecificAddressType = (DTOIntSpecificAddressType) shipmentAddress;
            if (dtoIntSpecificAddressType == null) {
                return null;
            }

            Entityin entityin = new Entityin();
            entityin.setLocation(mapInLocation(dtoIntSpecificAddressType));
            return entityin;
        } catch (ClassCastException ex) {
            LOG.error("... Error trying to parse Specific addressType to SpecificShipmentAddress ...");
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }
    }

    private Location mapInLocation(final DTOIntSpecificAddressType dtoIntSpecificAddressType) {
        if (dtoIntSpecificAddressType.getLocation() == null) {
            return null;
        }

        Location location = new Location();
        location.setFormattedaddress(dtoIntSpecificAddressType.getLocation().getFormattedAddress());
        location.setLocationtypes(mapInLocationTypes(dtoIntSpecificAddressType.getLocation().getLocationTypes()));
        location.setAddresscomponents(mapInAddressComponents(dtoIntSpecificAddressType.getLocation().getAddressComponents()));
        location.setGeolocation(mapInGeolocation(dtoIntSpecificAddressType.getLocation().getGeolocation()));
        return location;
    }

    private Geolocation mapInGeolocation(final DTOIntGeolocation dtoIntGeolocation) {
        if (dtoIntGeolocation == null) {
            return null;
        }

        Geolocation geolocation = new Geolocation();
        geolocation.setLatitude(dtoIntGeolocation.getLatitude());
        geolocation.setLongitude(dtoIntGeolocation.getLongitude());
        return geolocation;
    }

    private List<Addresscomponents> mapInAddressComponents(final List<DTOIntAddressComponents> addressComponents) {
        if (CollectionUtils.isEmpty(addressComponents)) {
            return null;
        }

        return addressComponents.stream().filter(Objects::nonNull).map(this::mapInAddressComponent).collect(Collectors.toList());
    }

    private Addresscomponents mapInAddressComponent(DTOIntAddressComponents dtoIntAddressComponents) {
        Addresscomponents addresscomponent = new Addresscomponents();
        addresscomponent.setAddresscomponent(mapInComponent(dtoIntAddressComponents));
        return addresscomponent;
    }

    private Addresscomponent mapInComponent(DTOIntAddressComponents dtoIntAddressComponents) {
        Addresscomponent addresscomponent = new Addresscomponent();
        addresscomponent.setComponenttypes(mapInComponentTypes(dtoIntAddressComponents.getComponentTypes()));
        addresscomponent.setCode(dtoIntAddressComponents.getCode());
        addresscomponent.setName(dtoIntAddressComponents.getName());
        return addresscomponent;
    }

    private List<Componenttypes> mapInComponentTypes(final List<String> componentTypes) {
        if (CollectionUtils.isEmpty(componentTypes)) {
            return null;
        }

        return componentTypes.stream().map(this::mapInComponentType).collect(Collectors.toList());
    }

    private Componenttypes mapInComponentType(final String componentType) {
        Componenttypes componenttypes = new Componenttypes();
        componenttypes.setComponenttype(componentType);
        return componenttypes;
    }

    private List<Locationtypes> mapInLocationTypes(final List<String> locationTypes) {
        if (CollectionUtils.isEmpty(locationTypes)) {
            return null;
        }

        return locationTypes.stream().map(this::mapInLocationType).collect(Collectors.toList());
    }

    private Locationtypes mapInLocationType(final String locationType) {
        Locationtypes locationtypes = new Locationtypes();
        locationtypes.setLocationtype(locationType);
        return locationtypes;
    }

    private Destination mapInDestination(final DTOIntStoredAddressType dtoIntStoredAddressType) {
        if (dtoIntStoredAddressType.getDestination() == null) {
            return null;
        }

        Destination destination = new Destination();
        destination.setId(dtoIntStoredAddressType.getDestination().getId());
        return destination;
    }

    @Override
    public ShipmentAddress mapOut(final RespuestaTransaccionPecpt002_1 response) {
        if (response == null || response.getEntityout() == null ||
                StringUtils.isEmpty(response.getEntityout().getAddresstype())) {
            return null;
        }

        if (SPECIFIC.equalsIgnoreCase(response.getEntityout().getAddresstype())) {
            return mapOutSpecificShipmentAddress(response.getEntityout());
        }

        if (STORED.equalsIgnoreCase(response.getEntityout().getAddresstype())) {
            return mapOutStoredShipmentAddress(response.getEntityout());
        }

        return null;
    }

    private ShipmentAddress mapOutStoredShipmentAddress(final Entityout entityout) {
        ShipmentAddress storedShipmentAddress = new ShipmentAddress();
        storedShipmentAddress.setAddressType(entityout.getAddresstype());
        storedShipmentAddress.setId(entityout.getId());
        storedShipmentAddress.setDestination(mapOutDestination(entityout.getDestination()));
        return storedShipmentAddress;
    }

    private com.bbva.pzic.cards.facade.v0.dto.Destination mapOutDestination(final Destination destination) {
        if (destination == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v0.dto.Destination result = new com.bbva.pzic.cards.facade.v0.dto.Destination();
        result.setId(destination.getId());
        result.setName(destination.getName());
        return result;
    }

    private ShipmentAddress mapOutSpecificShipmentAddress(final Entityout entityout) {
        ShipmentAddress specificShipmentAddress = new ShipmentAddress();
        specificShipmentAddress.setAddressType(entityout.getAddresstype());
        specificShipmentAddress.setId(entityout.getId());
        specificShipmentAddress.setLocation(mapOutLocation(entityout.getLocation()));
        return specificShipmentAddress;
    }

    private com.bbva.pzic.cards.facade.v0.dto.Location mapOutLocation(final Location location) {
        if (location == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v0.dto.Location result = new com.bbva.pzic.cards.facade.v0.dto.Location();
        result.setFormattedAddress(location.getFormattedaddress());
        result.setLocationTypes(mapOutLocationTypes(location.getLocationtypes()));
        result.setAddressComponents(mapOutAddressComponents(location.getAddresscomponents()));
        result.setGeolocation(mapOutGeolocation(location.getGeolocation()));
        return result;
    }

    private com.bbva.pzic.cards.facade.v0.dto.Geolocation mapOutGeolocation(final Geolocation geolocation) {
        if (geolocation == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v0.dto.Geolocation result = new com.bbva.pzic.cards.facade.v0.dto.Geolocation();
        result.setLatitude(geolocation.getLatitude());
        result.setLongitude(geolocation.getLongitude());
        return result;
    }

    private List<AddressComponent> mapOutAddressComponents(final List<Addresscomponents> addresscomponents) {
        if (CollectionUtils.isEmpty(addresscomponents)) {
            return null;
        }

        return addresscomponents.stream().filter(Objects::nonNull).map(this::mapOutAddressComponent).collect(Collectors.toList());
    }

    private AddressComponent mapOutAddressComponent(Addresscomponents addresscomponents) {
        if (addresscomponents.getAddresscomponent() == null) {
            return null;
        }

        AddressComponent addressComponent = new AddressComponent();
        addressComponent.setComponentTypes(mapOutComponentTypes(addresscomponents.getAddresscomponent().getComponenttypes()));
        addressComponent.setName(addresscomponents.getAddresscomponent().getName());
        addressComponent.setCode(addresscomponents.getAddresscomponent().getCode());
        return addressComponent;
    }

    private List<String> mapOutComponentTypes(List<Componenttypes> addresscomponents) {
        if (CollectionUtils.isEmpty(addresscomponents)) {
            return null;
        }

        return addresscomponents.stream().filter(Objects::nonNull).map(Componenttypes::getComponenttype).collect(Collectors.toList());
    }

    private List<String> mapOutLocationTypes(final List<Locationtypes> locationtypes) {
        if (CollectionUtils.isEmpty(locationtypes)) {
            return null;
        }

        return locationtypes.stream().filter(Objects::nonNull).map(Locationtypes::getLocationtype).collect(Collectors.toList());
    }
}
