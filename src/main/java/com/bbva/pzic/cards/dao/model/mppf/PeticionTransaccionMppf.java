package com.bbva.pzic.cards.dao.model.mppf;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPPF</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMppf</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMppf</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPPF.TXT
 * MPPFCONSULTA PASJERO FRECUENTE         MP        MP2CMPPFPBDMPPO MPM0PF              MPPF  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-05-23XP86166 2018-10-2916.29.49XP94441 2018-05-23-14.43.53.028113XP86166 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0PF.TXT
 * MPM0PF  �CONSULTA PASJERO FRECUENTE    �F�02�00017�01�00001�NTARJLF�NRO TARJETA LIFEMILE�A�016�0�R�        �
 * MPM0PF  �CONSULTA PASJERO FRECUENTE    �F�02�00017�02�00017�IDPREMI�ID PREMIO           �A�001�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0PS.TXT
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�01�00001�IDPREMI�ID PREMIO SALIDA    �A�001�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�02�00002�DESCPRE�DESC DEL PREMIO     �A�015�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�03�00017�NPASFRE�NRO PASAJERO FRECTE �A�019�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�04�00036�FECALTA�FECHA DE ALTA       �A�010�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�05�00046�FECBAJA�FECHA DE BAJA       �A�010�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�06�00056�INDLIFE�IND. DE LIFIMILE    �A�001�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�07�00057�INDMIGR�IND DE MIGRACION    �A�001�0�S�        �
 * MPM0PS  �CONSULTA PASJERO FRECUENTE    �X�08�00067�08�00058�FECVEN �FECHA DE EXPIRACION �A�010�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPPF.TXT
 * MPPFMPM0PS  MPWMPPFSMP2CMPPF1S                             XP93771 2018-07-24-14.29.18.568176XP94441 2018-10-29-10.48.01.182170
</pre></code>
 *
 * @see RespuestaTransaccionMppf
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPPF",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMppf.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0PF.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMppf implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}