package com.bbva.pzic.cards.dao.model.mpl2;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPL2</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpl2</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpl2</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPL2.D1190426.TXT
 * MPL2LISTADO DE MOVIMIENTOS DE TARJETA  MP        MP2CMPL2PBDMPPO MPMENL2             MPL2  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-01-26P018426 2018-08-2712.36.01XP93683 2017-01-26-11.57.05.590172P018426 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENL2.D1190426.TXT
 * MPMENL2 �LISTADO MOVIMIENTOS DE TARJETA�F�05�00059�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPMENL2 �LISTADO MOVIMIENTOS DE TARJETA�F�05�00059�02�00017�FECHINI�FECHA DE INICIO     �A�010�0�O�        �
 * MPMENL2 �LISTADO MOVIMIENTOS DE TARJETA�F�05�00059�03�00027�FECHFIN�FECHA DE FIN        �A�010�0�O�        �
 * MPMENL2 �LISTADO MOVIMIENTOS DE TARJETA�F�05�00059�04�00037�IDPAGIN�IDENT. DE PAGINA    �A�020�0�O�        �
 * MPMENL2 �LISTADO MOVIMIENTOS DE TARJETA�F�05�00059�05�00057�TAMPAGI�TAMA#O DE PAGINA    �N�003�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1L2.D1190426.TXT
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�01�00001�NUMOPER�NUMERO DE OPERACION �A�020�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�02�00021�DESOPER�DESCRIPCION MOVIMIEN�A�030�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�03�00051�IMPMORI�IMPORTE MOV.ORIGINAL�S�015�2�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�04�00066�MONMORI�MONEDA MOV.ORIGINAL �A�003�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�05�00069�IMPMTIT�IMP.MOV.MONEDA CONTR�S�015�2�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�06�00084�MOPMTIT�MONEDA DEL CONTRATO �A�003�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�07�00087�FECHOPE�FECHA DE OPERACION  �A�010�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�08�00097�HORAOPE�HORA DE OPERACION   �A�006�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�09�00103�FECHCON�FECHA CONTABLE      �A�010�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�10�00113�ESTOPER�ID ESTADO DE OPERAC �A�001�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�11�00114�DESTOPE�DESCRIP ESTADO OPERA�A�010�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�12�00124�NUMCONV�NUMERO CONTRAT RELAC�A�020�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�13�00144�IDTCORE�ID TIPO NROCONT/REL �A�001�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�14�00145�DETCORE�DES.TIPO NROCONT/REL�A�030�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�15�00175�IDTIPOP�ID TIPO DE OPERACION�A�001�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�16�00176�NOTIPOP�NOMBRE TIPO DE OPERA�A�020�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�17�00196�CODOPER�COD OPERACION M.PAGO�A�003�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�18�00199�IDTIPFN�ID TIPO FINANC.OPER.�A�001�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�19�00200�DETIPFN�DESCR.TIPO.FINANC.OP�A�025�0�S�        �
 * MPMS1L2 �LISTADO MOVIMIENTOS DE TARJETA�X�20�00264�20�00225�NOMCOD �DESCRIP.CONCEPTO MOV�A�040�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2L2.D1190426.TXT
 * MPMS2L2 �FORMATO DE PAGINACION         �X�02�00023�01�00001�IDPAGIN�IDENTIFICADO PAGINA �A�020�0�S�        �
 * MPMS2L2 �FORMATO DE PAGINACION         �X�02�00023�02�00021�TAMPAGI�NRO REG A DEVOLVER  �N�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPL2.D1190426.TXT
 * MPL2MPMS1L2 MPNCS1L2MP2CMPL21S                             P018426 2017-01-27-18.10.17.014346P018426 2017-01-27-18.10.17.014373
 * MPL2MPMS2L2 MPNCS2L2MP2CMPL21S                             P018426 2017-01-27-18.10.35.457785P018426 2017-01-27-18.10.35.457811
</pre></code>
 *
 * @see RespuestaTransaccionMpl2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPL2",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpl2.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENL2.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpl2 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}