package com.bbva.pzic.cards.dao.model.mpg5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPG5</code>
 *
 * @see PeticionTransaccionMpg5
 * @see RespuestaTransaccionMpg5
 */
@Component("transaccionMpg5")
public class TransaccionMpg5 implements InvocadorTransaccion<PeticionTransaccionMpg5,RespuestaTransaccionMpg5> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpg5 invocar(PeticionTransaccionMpg5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpg5.class, RespuestaTransaccionMpg5.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpg5 invocarCache(PeticionTransaccionMpg5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpg5.class, RespuestaTransaccionMpg5.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
