package com.bbva.pzic.cards.dao.model.mpb4.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.dao.model.mpb4.PeticionTransaccionMpb4;
import com.bbva.pzic.cards.dao.model.mpb4.RespuestaTransaccionMpb4;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPB4</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpb4.PeticionTransaccionMpb4
 * @see com.bbva.pzic.cards.dao.model.mpb4.RespuestaTransaccionMpb4
 */
@Component("transaccionMpb4")
public class TransaccionMpb4Mock implements InvocadorTransaccion<PeticionTransaccionMpb4, RespuestaTransaccionMpb4> {

    @Override
    public RespuestaTransaccionMpb4 invocar(PeticionTransaccionMpb4 transaccion) {
        RespuestaTransaccionMpb4 response = new RespuestaTransaccionMpb4();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");
        return response;
    }

    @Override
    public RespuestaTransaccionMpb4 invocarCache(PeticionTransaccionMpb4 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
