package com.bbva.pzic.cards.dao.model.pecpt004_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PECPT004</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPecpt004_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPecpt004_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PECPT004-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;PECPT004&quot; application=&quot;PECP&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;shipmentId&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;expand&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto name=&quot;EntityOut&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.CardShipmentDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;term&quot; type=&quot;String&quot; size=&quot;4000&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;externalCode&quot; type=&quot;String&quot;
 * size=&quot;40&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;addresses&quot; order=&quot;3&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;address&quot;
 * package=&quot;com.bbva.pecp.dto.shipments.dtos.ShipmentAddressDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;shipmentAddress&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.CardShipmentAddressDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot;
 * size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;location&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.LocationDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;formattedAddress&quot; type=&quot;String&quot;
 * size=&quot;200&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;locationTypes&quot; order=&quot;2&quot; mandatory=&quot;0&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;locationType&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;addressComponents&quot; order=&quot;3&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;addressComponent&quot;
 * package=&quot;com.bbva.pecp.dto.shipments.dtos.AddressComponentDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;code&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;componentTypes&quot; order=&quot;4&quot; mandatory=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;componentType&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;geolocation&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.GeolocationDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;latitude&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;longitude&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.DestinationDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;status&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.StatusDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot;
 * size=&quot;30&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;participant&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ParticipantDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;fullName&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;list name=&quot;cards&quot; order=&quot;2&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;card&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.CardDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;cardAgreement&quot; type=&quot;String&quot;
 * size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;referenceNumber&quot; type=&quot;String&quot;
 * size=&quot;16&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;identityDocument&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.IdentityDocumentDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;documentNumber&quot; type=&quot;String&quot;
 * size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;documentType&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.DocumentTypeDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;16&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;shippingCompany&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ShippingCompanyDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot;
 * mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Método encargado de obtener el detalle información de&amp;#xD;
 * la&amp;#xD;&amp;#xD;
 * entrega, previa validación.&amp;#xD;
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPecpt004_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PECPT004",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPecpt004_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPecpt004_1 {
		
		/**
	 * <p>Campo <code>shipmentId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "shipmentId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String shipmentid;
	
	/**
	 * <p>Campo <code>expand</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "expand", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
	private String expand;
	
}