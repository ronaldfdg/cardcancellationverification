package com.bbva.pzic.cards.dao.model.mprt;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPRT</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMprt</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMprt</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPRT.D1181003.txt
 * MPRTALTA/RENV/REP DE TARJETA DE DEBITO MP        MP2CRT00MP49AD  MPRMRT0             MPRT  NN3000NNNNNN    SSTN     E  NNNSSNNN  NN                2017-11-30XP92063 2018-07-2610.24.33XP91864 2017-11-30-09.01.16.540555XP92063 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPRMRT0.D1181003.txt
 * MPRMRT0 �REGISTRA RENOV/REPOS TARJ DEB �F�06�00078�01�00001�NUCOREL�NRO TARJETA ANTIGUA �A�020�0�O�        �
 * MPRMRT0 �REGISTRA RENOV/REPOS TARJ DEB �F�06�00078�02�00021�TARINOM�NRO TARJETA NUEVA   �A�016�0�R�        �
 * MPRMRT0 �REGISTRA RENOV/REPOS TARJ DEB �F�06�00078�03�00037�CTACARG�CUENTA DE CARGO     �A�020�0�O�        �
 * MPRMRT0 �REGISTRA RENOV/REPOS TARJ DEB �F�06�00078�04�00057�NUMCLIE�NUMERO DE CLIENTE   �A�008�0�O�        �
 * MPRMRT0 �REGISTRA RENOV/REPOS TARJ DEB �F�06�00078�05�00065�INDCSMS�IND.ENVIO SMS (S/N) �A�001�0�O�        �
 * MPRMRT0 �REGISTRA RENOV/REPOS TARJ DEB �F�06�00078�06�00066�IDTESMS�TELF.CELULAR SMS    �A�013�0�O�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPRMRT1.D1181003.txt
 * MPRMRT1 �REGISTRA INFO TARJ DEB REN/REP�X�01�00019�01�00001�TARJNUE�NRO TARJETA NUEVA   �A�019�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPRT.D1181003.txt
 * MPRTMPRMRT1 MPSCCRT2MP2CRT001S                             XP92063 2017-11-30-09.55.11.637712XP92063 2017-12-18-14.08.04.782448
 *
</pre></code>
 *
 * @see RespuestaTransaccionMprt
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPRT",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMprt.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPRMRT0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMprt implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}