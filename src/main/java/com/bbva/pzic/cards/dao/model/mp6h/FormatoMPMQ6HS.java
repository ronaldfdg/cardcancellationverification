package com.bbva.pzic.cards.dao.model.mp6h;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMQ6HS</code> de la transacci&oacute;n <code>MP6H</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMQ6HS")
@RooJavaBean
@RooSerializable
public class FormatoMPMQ6HS {
	
	/**
	 * <p>Campo <code>IDDOCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDDOCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
	private String iddocta;
	
	/**
	 * <p>Campo <code>NCONTCC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NCONTCC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ncontcc;
	
	/**
	 * <p>Campo <code>NCMOTIT</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NCMOTIT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String ncmotit;
	
	/**
	 * <p>Campo <code>NEXTRA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "NEXTRA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String nextra;
	
	/**
	 * <p>Campo <code>MCNMOVI</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 5, nombre = "MCNMOVI", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
	private Integer mcnmovi;
	
	/**
	 * <p>Campo <code>MCILMCR</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "MCILMCR", tipo = TipoCampo.DECIMAL, longitudMinima = 18, longitudMaxima = 18, decimales = 2)
	private BigDecimal mcilmcr;
	
	/**
	 * <p>Campo <code>MONLINC</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "MONLINC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String monlinc;
	
	/**
	 * <p>Campo <code>MCITOEX</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "MCITOEX", tipo = TipoCampo.DECIMAL, longitudMinima = 10, longitudMaxima = 10, signo = true, decimales = 2)
	private BigDecimal mcitoex;
	
	/**
	 * <p>Campo <code>MCIDBTO</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "MCIDBTO", tipo = TipoCampo.DECIMAL, longitudMinima = 18, longitudMaxima = 18, signo = true, decimales = 2)
	private BigDecimal mcidbto;
	
	/**
	 * <p>Campo <code>MCFEXTA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "MCFEXTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String mcfexta;
	
	/**
	 * <p>Campo <code>MCFEXTR</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "MCFEXTR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String mcfextr;
	
	/**
	 * <p>Campo <code>MCFRPAG</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "MCFRPAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String mcfrpag;
	
	/**
	 * <p>Campo <code>MCNDIAE</code>, &iacute;ndice: <code>13</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 13, nombre = "MCNDIAE", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer mcndiae;
	
	/**
	 * <p>Campo <code>PORTYPE</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "PORTYPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String portype;
	
	/**
	 * <p>Campo <code>IDAVANM</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "IDAVANM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idavanm;
	
	/**
	 * <p>Campo <code>PORAVAN</code>, &iacute;ndice: <code>16</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 16, nombre = "PORAVAN", tipo = TipoCampo.DECIMAL, longitudMinima = 5, longitudMaxima = 5, decimales = 2)
	private BigDecimal poravan;
	
	/**
	 * <p>Campo <code>IDCOMPM</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "IDCOMPM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcompm;
	
	/**
	 * <p>Campo <code>PORCOMP</code>, &iacute;ndice: <code>18</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 18, nombre = "PORCOMP", tipo = TipoCampo.DECIMAL, longitudMinima = 5, longitudMaxima = 5, decimales = 2)
	private BigDecimal porcomp;
	
	/**
	 * <p>Campo <code>IDAVABM</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "IDAVABM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idavabm;
	
	/**
	 * <p>Campo <code>PAVANBM</code>, &iacute;ndice: <code>20</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 20, nombre = "PAVANBM", tipo = TipoCampo.DECIMAL, longitudMinima = 5, longitudMaxima = 5, decimales = 2)
	private BigDecimal pavanbm;
	
	/**
	 * <p>Campo <code>IDCOMBM</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "IDCOMBM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idcombm;
	
	/**
	 * <p>Campo <code>PCOMPBM</code>, &iacute;ndice: <code>22</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 22, nombre = "PCOMPBM", tipo = TipoCampo.DECIMAL, longitudMinima = 5, longitudMaxima = 5, decimales = 2)
	private BigDecimal pcompbm;
	
	/**
	 * <p>Campo <code>MONCON1</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "MONCON1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moncon1;
	
	/**
	 * <p>Campo <code>IDLIQP1</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "IDLIQP1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idliqp1;
	
	/**
	 * <p>Campo <code>ILIQPEN</code>, &iacute;ndice: <code>25</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 25, nombre = "ILIQPEN", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal iliqpen;
	
	/**
	 * <p>Campo <code>IDSALM1</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "IDSALM1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idsalm1;
	
	/**
	 * <p>Campo <code>ISALMES</code>, &iacute;ndice: <code>27</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 27, nombre = "ISALMES", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal isalmes;
	
	/**
	 * <p>Campo <code>IDMCIT1</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "IDMCIT1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcit1;
	
	/**
	 * <p>Campo <code>MCITOCO</code>, &iacute;ndice: <code>29</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 29, nombre = "MCITOCO", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitoco;
	
	/**
	 * <p>Campo <code>IDMCIV1</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "IDMCIV1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmciv1;
	
	/**
	 * <p>Campo <code>MCITOAV</code>, &iacute;ndice: <code>31</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 31, nombre = "MCITOAV", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitoav;
	
	/**
	 * <p>Campo <code>IDMCIA1</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "IDMCIA1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcia1;
	
	/**
	 * <p>Campo <code>MCITOPA</code>, &iacute;ndice: <code>33</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 33, nombre = "MCITOPA", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitopa;
	
	/**
	 * <p>Campo <code>IDMCIU1</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 34, nombre = "IDMCIU1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmciu1;
	
	/**
	 * <p>Campo <code>MCIDSTO</code>, &iacute;ndice: <code>35</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 35, nombre = "MCIDSTO", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcidsto;
	
	/**
	 * <p>Campo <code>IDMCIL1</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 36, nombre = "IDMCIL1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcil1;
	
	/**
	 * <p>Campo <code>MCILIBR</code>, &iacute;ndice: <code>37</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 37, nombre = "MCILIBR", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcilibr;
	
	/**
	 * <p>Campo <code>IDMCIC1</code>, &iacute;ndice: <code>38</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 38, nombre = "IDMCIC1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcic1;
	
	/**
	 * <p>Campo <code>MCIDTCU</code>, &iacute;ndice: <code>39</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 39, nombre = "MCIDTCU", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcidtcu;
	
	/**
	 * <p>Campo <code>IDMCPA1</code>, &iacute;ndice: <code>40</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 40, nombre = "IDMCPA1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcpa1;
	
	/**
	 * <p>Campo <code>MCPAGMI</code>, &iacute;ndice: <code>41</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 41, nombre = "MCPAGMI", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcpagmi;
	
	/**
	 * <p>Campo <code>IDMCPT1</code>, &iacute;ndice: <code>42</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 42, nombre = "IDMCPT1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcpt1;
	
	/**
	 * <p>Campo <code>MCICARG</code>, &iacute;ndice: <code>43</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 43, nombre = "MCICARG", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcicarg;
	
	/**
	 * <p>Campo <code>IDMCIS1</code>, &iacute;ndice: <code>44</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 44, nombre = "IDMCIS1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcis1;
	
	/**
	 * <p>Campo <code>MCISDOA</code>, &iacute;ndice: <code>45</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 45, nombre = "MCISDOA", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcisdoa;
	
	/**
	 * <p>Campo <code>MONCON2</code>, &iacute;ndice: <code>46</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 46, nombre = "MONCON2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moncon2;
	
	/**
	 * <p>Campo <code>IDLIQP2</code>, &iacute;ndice: <code>47</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 47, nombre = "IDLIQP2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idliqp2;
	
	/**
	 * <p>Campo <code>ILIQPBM</code>, &iacute;ndice: <code>48</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 48, nombre = "ILIQPBM", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal iliqpbm;
	
	/**
	 * <p>Campo <code>IDSALM2</code>, &iacute;ndice: <code>49</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 49, nombre = "IDSALM2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idsalm2;
	
	/**
	 * <p>Campo <code>ISALMBM</code>, &iacute;ndice: <code>50</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 50, nombre = "ISALMBM", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal isalmbm;
	
	/**
	 * <p>Campo <code>IDMCIT2</code>, &iacute;ndice: <code>51</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 51, nombre = "IDMCIT2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcit2;
	
	/**
	 * <p>Campo <code>MCITOCB</code>, &iacute;ndice: <code>52</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 52, nombre = "MCITOCB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitocb;
	
	/**
	 * <p>Campo <code>IDMCIV2</code>, &iacute;ndice: <code>53</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 53, nombre = "IDMCIV2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmciv2;
	
	/**
	 * <p>Campo <code>MCITOAB</code>, &iacute;ndice: <code>54</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 54, nombre = "MCITOAB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitoab;
	
	/**
	 * <p>Campo <code>IDMCIA2</code>, &iacute;ndice: <code>55</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 55, nombre = "IDMCIA2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcia2;
	
	/**
	 * <p>Campo <code>MCITOPB</code>, &iacute;ndice: <code>56</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 56, nombre = "MCITOPB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcitopb;
	
	/**
	 * <p>Campo <code>IDMCIU2</code>, &iacute;ndice: <code>57</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 57, nombre = "IDMCIU2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmciu2;
	
	/**
	 * <p>Campo <code>MCIDSTB</code>, &iacute;ndice: <code>58</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 58, nombre = "MCIDSTB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcidstb;
	
	/**
	 * <p>Campo <code>IDMCIL2</code>, &iacute;ndice: <code>59</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 59, nombre = "IDMCIL2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcil2;
	
	/**
	 * <p>Campo <code>MCILBRB</code>, &iacute;ndice: <code>60</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 60, nombre = "MCILBRB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcilbrb;
	
	/**
	 * <p>Campo <code>IDMCIC2</code>, &iacute;ndice: <code>61</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 61, nombre = "IDMCIC2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcic2;
	
	/**
	 * <p>Campo <code>MCIDTCB</code>, &iacute;ndice: <code>62</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 62, nombre = "MCIDTCB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcidtcb;
	
	/**
	 * <p>Campo <code>IDMCPA2</code>, &iacute;ndice: <code>63</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 63, nombre = "IDMCPA2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcpa2;
	
	/**
	 * <p>Campo <code>MCPAGMB</code>, &iacute;ndice: <code>64</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 64, nombre = "MCPAGMB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcpagmb;
	
	/**
	 * <p>Campo <code>IDMCPT2</code>, &iacute;ndice: <code>65</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 65, nombre = "IDMCPT2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcpt2;
	
	/**
	 * <p>Campo <code>MCICARB</code>, &iacute;ndice: <code>66</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 66, nombre = "MCICARB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcicarb;
	
	/**
	 * <p>Campo <code>IDMCIS2</code>, &iacute;ndice: <code>67</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 67, nombre = "IDMCIS2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idmcis2;
	
	/**
	 * <p>Campo <code>MCISDOB</code>, &iacute;ndice: <code>68</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 68, nombre = "MCISDOB", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, signo = true, decimales = 2)
	private BigDecimal mcisdob;
	
	/**
	 * <p>Campo <code>MONCON3</code>, &iacute;ndice: <code>69</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 69, nombre = "MONCON3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String moncon3;
	
}