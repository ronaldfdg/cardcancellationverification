package com.bbva.pzic.cards.dao.model.mp6j;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MP6J</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMp6j</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMp6j</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: QGDTCCT_MP6J.TXT
 * MP6JOPERACION NO RECONOCIDA            MP        MP2CMP6J     01 MPMEN6J             MP6I  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2020-09-14XP94134 2020-09-1417.07.31XP94134 2020-09-14-16.23.33.947706XP94134 0001-01-010001-01-01
 * 
 * FICHERO: QGDTFDF_MPMEN6J.TXT
 * MPMEN6J �SIM.OP.NO RECONOCIDA          �F�03�00041�01�00001�MCINDOP�TIPO DE MOVIMIENTO  �A�002�0�R�        �
 * MPMEN6J �SIM.OP.NO RECONOCIDA          �F�03�00041�02�00003�IDETARJ�NUMERO DE TARJETA   �A�019�0�R�        �
 * MPMEN6J �SIM.OP.NO RECONOCIDA          �F�03�00041�03�00022�IDENTIF�ID.MOVIMIENTO       �A�020�0�R�        �
 * 
 * FICHERO: QGDTFDF_MPMS16J.TXT
 * MPMS16J �OPERACIONES NO RECONOCIDA     �X�06�00082�01�00001�MCINDOP�TIPO DE MOVIMIENTO  �A�002�0�S�        �
 * MPMS16J �OPERACIONES NO RECONOCIDA     �X�06�00082�02�00003�MCINDES�DESCRIP.TP.MOVIMIENT�A�010�0�S�        �
 * MPMS16J �OPERACIONES NO RECONOCIDA     �X�06�00082�03�00013�IDETARJ�NUMERO DE TARJETA   �A�019�0�S�        �
 * MPMS16J �OPERACIONES NO RECONOCIDA     �X�06�00082�04�00032�IDENTIF�IDENT.NUM.MOVIMIENTO�A�020�0�S�        �
 * MPMS16J �OPERACIONES NO RECONOCIDA     �X�06�00082�05�00052�CODOPE �CODIGO RESULT.OPERAC�A�001�0�S�        �
 * MPMS16J �OPERACIONES NO RECONOCIDA     �X�06�00082�06�00053�DESOPER�DESCRIP.RESLT.OPERAC�A�030�0�S�        �
 * 
 * FICHERO: QGDTFDX_MP6J.TXT
 * MP6JMPMS16J MPWC6J1 MP2CMP6J1S                             XP94134 2020-09-14-17.07.18.178562XP94134 2020-09-14-17.07.18.178936
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionMp6j
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MP6J",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMp6j.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMEN6J.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMp6j implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}