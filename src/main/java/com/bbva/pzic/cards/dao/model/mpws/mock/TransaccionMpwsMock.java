package com.bbva.pzic.cards.dao.model.mpws.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpws.*;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpws")
public class TransaccionMpwsMock implements InvocadorTransaccion<PeticionTransaccionMpws, RespuestaTransaccionMpws> {

    private FormatMpwsMock mock;

    @PostConstruct
    public void init() {
        mock = new FormatMpwsMock();
    }

    public static final Integer EMPTY_TEST = 66;
    public static final Integer NULL_TEST = 77;

    @Override
    public RespuestaTransaccionMpws invocar(PeticionTransaccionMpws peticionTransaccionMpws) {
        RespuestaTransaccionMpws result = new RespuestaTransaccionMpws();
        result.setCodigoRetorno("OK_COMMIT");
        result.setCodigoControl("OK");

        FormatoMPM0WSE formtIn = peticionTransaccionMpws.getCuerpo().getParte(FormatoMPM0WSE.class);
        if (NULL_TEST.equals(formtIn.getNcuotas())) {
            return result;
        }
        if (EMPTY_TEST.equals(formtIn.getNcuotas())) {
            try {
                result.getCuerpo().getPartes().add(createFormatoMPM0WSCEmpty());
                result.getCuerpo().getPartes().addAll(createFormatoMPM0DETEmpty());
                return result;
            } catch (IOException e) {
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
            }
        }
        try {
            result.getCuerpo().getPartes().add(createFormatoMPM0WSCSalidaDefault());
            result.getCuerpo().getPartes().addAll(createFormatoMPM0DETSalidaDefault());
            return result;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionMpws invocarCache(PeticionTransaccionMpws peticionTransaccionMpws) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida createFormatoMPM0WSCEmpty() throws IOException {
        FormatoMPM0WSC outFormat = mock.getFormatoMPM0WSCEmpty();
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(outFormat);
        return copySalida;
    }

    private List<CopySalida> createFormatoMPM0DETEmpty() throws IOException {
        List<FormatoMPM0DET> formats = mock.getFormatoMPM0DETEmpty();
        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPM0DET format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

    private CopySalida createFormatoMPM0WSCSalidaDefault() throws IOException {
        FormatoMPM0WSC formatoMPM0WSC = mock.getFormatoMPM0WSC();
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoMPM0WSC);
        return copySalida;
    }

    private List<CopySalida> createFormatoMPM0DETSalidaDefault() throws IOException {
        List<FormatoMPM0DET> formats = mock.getFormatoMPM0DET();
        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPM0DET format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

}
