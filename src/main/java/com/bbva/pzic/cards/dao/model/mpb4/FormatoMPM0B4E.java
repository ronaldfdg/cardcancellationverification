package com.bbva.pzic.cards.dao.model.mpb4;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * Formato de datos <code>MPM0B4E</code> de la transacci&oacute;n <code>MPB4</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPM0B4E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPM0B4E {

    /**
     * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String idetarj;

    /**
     * <p>Campo <code>IDEBLOQ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "IDEBLOQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idebloq;

    /**
     * <p>Campo <code>IDERAZO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "IDERAZO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String iderazo;

    /**
     * <p>Campo <code>IDACTBL</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "IDACTBL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String idactbl;

}