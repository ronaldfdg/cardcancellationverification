package com.bbva.pzic.cards.dao.model.kb94.mock;

import com.bbva.pzic.cards.dao.model.kb94.FormatoKTSCKB94;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 21/12/2019.
 *
 * @author Entelgy
 */
public final class FormatsKb94Stubs {

    private static final FormatsKb94Stubs INSTANCE = new FormatsKb94Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private FormatsKb94Stubs() {
    }

    public static FormatsKb94Stubs getInstance() {
        return INSTANCE;
    }

    public FormatoKTSCKB94 buildFormatoKTSCKB94() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/kb94/mock/KTSCKB94.json"), FormatoKTSCKB94.class);
    }

    public FormatoKTSCKB94 buildFormatoKTSCKB94Empty() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/kb94/mock/KTSCKB94-EMPTY.json"), FormatoKTSCKB94.class);
    }
}
