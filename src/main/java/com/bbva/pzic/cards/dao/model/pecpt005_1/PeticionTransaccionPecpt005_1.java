package com.bbva.pzic.cards.dao.model.pecpt005_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PECPT005</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPecpt005_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPecpt005_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PECPT005-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;PECPT005&quot; application=&quot;PECP&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;shipmentid&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;EntityIn&quot;
 * package=&quot;com.bbva.pecp.dto.shipments.dtos.CardShipmentDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;biometricId&quot; type=&quot;String&quot; size=&quot;36&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;shipmentAddress&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.ShipmentAddressDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;32&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;currentAddress&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.CurrentAddressDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;dto name=&quot;location&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.LocationDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;geolocation&quot; package=&quot;com.bbva.pecp.dto.shipments.dtos.GeolocationDTO&quot;
 * artifactId=&quot;PECPC001&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;latitude&quot; type=&quot;Double&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;longitude&quot; type=&quot;Double&quot;
 * size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut /&gt;
 * &lt;description&gt;Confirmar entrega&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPecpt005_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PECPT005",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPecpt005_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPecpt005_1 {
		
		/**
	 * <p>Campo <code>shipmentid</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "shipmentid", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String shipmentid;
	
	/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}