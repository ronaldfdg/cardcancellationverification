package com.bbva.pzic.cards.dao.model.mp6h;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MP6H</code>
 * 
 * @see PeticionTransaccionMp6h
 * @see RespuestaTransaccionMp6h
 */
@Component("transaccionMp6h")
public class TransaccionMp6h implements InvocadorTransaccion<PeticionTransaccionMp6h,RespuestaTransaccionMp6h> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMp6h invocar(PeticionTransaccionMp6h transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp6h.class, RespuestaTransaccionMp6h.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMp6h invocarCache(PeticionTransaccionMp6h transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp6h.class, RespuestaTransaccionMp6h.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}