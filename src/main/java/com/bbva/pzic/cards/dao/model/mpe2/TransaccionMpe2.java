package com.bbva.pzic.cards.dao.model.mpe2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPE2</code>
 *
 * @see PeticionTransaccionMpe2
 * @see RespuestaTransaccionMpe2
 */
@Component("transaccionMpe2")
public class TransaccionMpe2 implements InvocadorTransaccion<PeticionTransaccionMpe2,RespuestaTransaccionMpe2> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpe2 invocar(PeticionTransaccionMpe2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpe2.class, RespuestaTransaccionMpe2.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpe2 invocarCache(PeticionTransaccionMpe2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpe2.class, RespuestaTransaccionMpe2.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
