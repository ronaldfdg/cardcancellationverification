package com.bbva.pzic.cards.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationQueryParams;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationResponse;
import com.bbva.pzic.cards.dao.rest.RestCreateCardsOfferSimulate;
import com.bbva.pzic.cards.dao.rest.mock.stubs.CardsOfferSimulateMock;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestCreateCardsOfferSimulateMock extends RestCreateCardsOfferSimulate {

    public static final String ERROR_RESPONSE = "99999995";
    public static final String EMPTY_RESPONSE = "99999994";
    public static final String WITHOUT_CODPROD_PG_RESPONSE = "99999993";

    @Override
    public CardHolderSimulationResponse connect(final String urlPropertyValue, final HashMap<String, String> queryParams) {
        if (EMPTY_RESPONSE.equals(queryParams.get(CardHolderSimulationQueryParams.ID_PROPUESTA))) {
            return new CardHolderSimulationResponse();
        }
        CardHolderSimulationResponse response;
        try {
            response = CardsOfferSimulateMock.getInstance().buildCarHolderSimulationResponse();
            if (WITHOUT_CODPROD_PG_RESPONSE.equals(queryParams.get(CardHolderSimulationQueryParams.ID_PROPUESTA))) {
                response.getData().setUrlImagen("pg=&bin=491910&default_image=true&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true");
            }

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }

        int statusCode = 200;
        List<Message> messages = new ArrayList<>();

        if (ERROR_RESPONSE.equals(queryParams.get(CardHolderSimulationQueryParams.ID_PROPUESTA))) {
            messages.add(CardsOfferSimulateMock.getInstance().getFatalMessage());
            statusCode = 400;
        }

        response.setMessages(messages);

        evaluateResponse(response, statusCode);
        return response;
    }
}
