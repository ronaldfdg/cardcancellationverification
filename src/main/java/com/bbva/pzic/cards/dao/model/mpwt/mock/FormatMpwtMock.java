package com.bbva.pzic.cards.dao.model.mpwt.mock;

import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0TSC;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
public class FormatMpwtMock {

    private ObjectMapperHelper objectMapper;

    public FormatMpwtMock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPM0TSC getFormatoMPM0TSC() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpwt/mock/formatoMPM0TSC.json"), FormatoMPM0TSC.class);
    }

    public List<FormatoMPM0DET> getFormatoMPM0DET() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpwt/mock/formatoMPM0DET.json"), new TypeReference<List<FormatoMPM0DET>>() {
        });
    }

    public List<FormatoMPM0DET> getFormatoMPM0DETEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpwt/mock/formatoMPM0DETEmpty.json"), new TypeReference<List<FormatoMPM0DET>>() {
        });
    }

    public FormatoMPM0TSC getFormatoMPM0TSCEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpwt/mock/formatoMPM0TSCEmpty.json"), FormatoMPM0TSC.class);
    }

}
