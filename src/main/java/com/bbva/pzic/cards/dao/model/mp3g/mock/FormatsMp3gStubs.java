package com.bbva.pzic.cards.dao.model.mp3g.mock;

import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS13G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS23G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS33G;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

public class FormatsMp3gStubs {

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private static final FormatsMp3gStubs INSTANCE = new FormatsMp3gStubs();

    private FormatsMp3gStubs() {
    }

    public static FormatsMp3gStubs getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS13G getFormatoMPMS13GMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mp3g/mock/formatoMPMS13G.json"), new TypeReference<FormatoMPMS13G>() {
        });
    }

    public FormatoMPMS23G getFormatoMPMS23GMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mp3g/mock/formatoMPMS23G.json"), new TypeReference<FormatoMPMS23G>() {
        });
    }

    public FormatoMPMS33G getFormatoMPMS33GMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mp3g/mock/formatoMPMS33G.json"), new TypeReference<FormatoMPMS33G>() {
        });
    }
}