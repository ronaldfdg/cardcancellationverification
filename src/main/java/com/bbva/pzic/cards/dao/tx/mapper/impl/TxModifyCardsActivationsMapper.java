package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpaf.FormatoMPM0AFE;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardsActivationsMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 24/06/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxModifyCardsActivationsMapper implements ITxModifyCardsActivationsMapper {

    private static final Log LOG = LogFactory.getLog(TxModifyCardsActivationsMapper.class);

    @Override
    public FormatoMPM0AFE mapIn(final DTOIntActivation dtoInt) {
        LOG.info("... called method TxModifyCardsActivationsMapper.mapIn ...");
        FormatoMPM0AFE formatoMPM0AFE = new FormatoMPM0AFE();
        formatoMPM0AFE.setCodclie(dtoInt.getCustomerId());
        formatoMPM0AFE.setCodserv(dtoInt.getActivationId());

        if (dtoInt.getIsActive() != null)
            formatoMPM0AFE.setIndesta(dtoInt.getIsActive() ? "1" : "0");

        return formatoMPM0AFE;
    }
}
