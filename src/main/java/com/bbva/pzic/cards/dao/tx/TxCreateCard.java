package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.CardData;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.model.mpw1.PeticionTransaccionMpw1;
import com.bbva.pzic.cards.dao.model.mpw1.RespuestaTransaccionMpw1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 12/06/2017.
 *
 * @author Entelgy
 */
@Component("txCreateCard")
public class TxCreateCard
        extends SingleOutputFormat<DTOIntCard, FormatoMPME0W1, CardData, FormatoMPMS1W1> {

    @Resource(name = "txCreateCardMapper")
    private ITxCreateCardMapper mapper;

    @Autowired
    public TxCreateCard(@Qualifier("transaccionMpw1") InvocadorTransaccion<PeticionTransaccionMpw1, RespuestaTransaccionMpw1> transaction) {
        super(transaction, PeticionTransaccionMpw1::new, CardData::new, FormatoMPMS1W1.class);
    }

    @Override
    protected FormatoMPME0W1 mapInput(DTOIntCard dtoIntCard) {
        return mapper.mapIn(dtoIntCard);
    }

    @Override
    protected CardData mapFirstOutputFormat(FormatoMPMS1W1 formatoMPMS1W1, DTOIntCard dtoIntCard, CardData cardData) {
        return mapper.mapOut(formatoMPMS1W1);
    }
}
