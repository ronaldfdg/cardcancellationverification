package com.bbva.pzic.cards.dao.rest.mapper;

import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;

/**
 * Created on 20/02/2020.
 *
 * @author Entelgy
 */
public interface IRestListImagesCoversHolderSimulationMapper {

    AWSImagesRequest mapIn(HolderSimulation holderSimulation);

    void mapOut(AWSImagesResponse response, HolderSimulation holderSimulation);

}
