package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.dao.model.mpb4.FormatoMPM0B4E;
import com.bbva.pzic.cards.dao.model.mpb4.PeticionTransaccionMpb4;
import com.bbva.pzic.cards.dao.model.mpb4.RespuestaTransaccionMpb4;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyPartialCardBlockMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.NoneOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created on 29/08/2017.
 *
 * @author Entelgy
 */
@Component("txModifyPartialCardBlock")
public class TxModifyPartialCardBlock
        extends NoneOutputFormat<DTOIntBlock, FormatoMPM0B4E> {

    @Autowired
    private ITxModifyPartialCardBlockMapper txModifyPartialCardBlockMapper;

    @Autowired
    public TxModifyPartialCardBlock(@Qualifier("transaccionMpb4") InvocadorTransaccion<PeticionTransaccionMpb4, RespuestaTransaccionMpb4> transaction) {
        super(transaction, PeticionTransaccionMpb4::new);
    }

    @Override
    protected FormatoMPM0B4E mapInput(DTOIntBlock dtoIntBlock) {
        return txModifyPartialCardBlockMapper.mapIn(dtoIntBlock);
    }
}
