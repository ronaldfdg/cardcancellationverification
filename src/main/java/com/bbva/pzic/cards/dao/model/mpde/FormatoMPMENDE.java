package com.bbva.pzic.cards.dao.model.mpde;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMENDE</code> de la transacci&oacute;n <code>MPDE</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMENDE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMENDE {

	/**
	 * <p>Campo <code>NROTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NROTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	@DatoAuditable(omitir = true)
	private String nrotarj;

}