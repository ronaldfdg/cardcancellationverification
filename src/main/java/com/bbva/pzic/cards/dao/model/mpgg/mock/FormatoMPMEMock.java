package com.bbva.pzic.cards.dao.model.mpgg.mock;

import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS1GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS2GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS3GG;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 9/02/2018.
 *
 * @author Entelgy
 */
public class FormatoMPMEMock {

    private ObjectMapperHelper
            mapper;

    public FormatoMPMEMock() {
        this.mapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPMS1GG getFormatoMPMS1GG() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgg/mock/formatoMPMS1GG.json"), FormatoMPMS1GG.class);
    }

    public FormatoMPMS1GG getFormatoMPMS1GGEmpty() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgg/mock/formatoMPMS1GG-EMPTY.json"), FormatoMPMS1GG.class);
    }

    public FormatoMPMS2GG getFormatoMPMS2GG() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgg/mock/formatoMPMS2GG.json"), FormatoMPMS2GG.class);
    }

    public FormatoMPMS2GG getFormatoMPMS2GGEmpty() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgg/mock/formatoMPMS2GG-EMPTY.json"), FormatoMPMS2GG.class);
    }

    public FormatoMPMS3GG getFormatoMPMS3GG() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgg/mock/formatoMPMS3GG.json"), FormatoMPMS3GG.class);
    }

    public FormatoMPMS3GG getFormatoMPMS3GGEmpty() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgg/mock/formatoMPMS3GG-EMPTY.json"), FormatoMPMS3GG.class);
    }
}
