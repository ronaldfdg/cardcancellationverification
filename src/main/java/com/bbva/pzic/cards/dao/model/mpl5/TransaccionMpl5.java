package com.bbva.pzic.cards.dao.model.mpl5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPL5</code>
 *
 * @see PeticionTransaccionMpl5
 * @see RespuestaTransaccionMpl5
 */
@Component
public class TransaccionMpl5 implements InvocadorTransaccion<PeticionTransaccionMpl5,RespuestaTransaccionMpl5> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpl5 invocar(PeticionTransaccionMpl5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpl5.class, RespuestaTransaccionMpl5.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpl5 invocarCache(PeticionTransaccionMpl5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpl5.class, RespuestaTransaccionMpl5.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
