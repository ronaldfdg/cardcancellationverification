package com.bbva.pzic.cards.dao.rest;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.dao.rest.mapper.IRestDigitizeDocumentFileMapper;
import com.bbva.pzic.cards.util.connection.rest.RestPostConnection;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT;

/**
 * Created on 27/03/2018.
 *
 * @author Entelgy
 */
@Component
public class RestDigitizeDocumentFile extends RestPostConnection<DocumentRequest, Documents> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1810268.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1810268.backend.proxy";

    private Translator translator;

    @Autowired
    private IRestDigitizeDocumentFileMapper mapper;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public Document perform(final DocumentRequest input) {
        return mapper.mapOut(connect(URL_PROPERTY, input));
    }

    @Override
    protected void evaluateResponse(Documents response, int statusCode) {
        evaluateMessagesResponse(generateErrorMessages(statusCode), SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT, statusCode);
    }

    private List<Message> generateErrorMessages(int statusCode) {
        List<Message> messages = new ArrayList<>();
        Message message = new Message();
        message.setCode(String.valueOf(statusCode));
        message.setType(ErrorSeverity.ERROR);
        messages.add(message);
        return messages;
    }
}
