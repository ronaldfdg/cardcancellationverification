package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PPCUTC01</code>
 * 
 * @see PeticionTransaccionPpcutc01_1
 * @see RespuestaTransaccionPpcutc01_1
 */
@Component
public class TransaccionPpcutc01_1 implements InvocadorTransaccion<PeticionTransaccionPpcutc01_1,RespuestaTransaccionPpcutc01_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPpcutc01_1 invocar(PeticionTransaccionPpcutc01_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcutc01_1.class, RespuestaTransaccionPpcutc01_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPpcutc01_1 invocarCache(PeticionTransaccionPpcutc01_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcutc01_1.class, RespuestaTransaccionPpcutc01_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}