package com.bbva.pzic.cards.dao.model.ppcut002_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PPCUT002</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPpcut002_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPpcut002_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PPCUT002-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PPCUT002&quot; application=&quot;PPCU&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;proposal-id&quot; type=&quot;String&quot; size=&quot;36&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;EntityIn&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.CardProposalDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;list name=&quot;deliveries&quot; order=&quot;1&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;delivery&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DeliveryDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ServiceTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;deliveryContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;emailContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;address&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;location&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.LocationDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;list name=&quot;addressComponents&quot; order=&quot;1&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;addressComponent&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressComponentDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;code&quot; type=&quot;String&quot; size=&quot;7&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;componentTypes&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;componentType&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.DestinationDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;2&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;additionalProducts&quot; order=&quot;3&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;additionalProduct&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AdditionalProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;productType&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;subproduct&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.SubproductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;14&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;paymentMethod&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaymentMethodDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;7&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;frecuency&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FrequencyDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;daysOfMonth&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DaysOfMonthDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;day&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;specificContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;mobileContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;phoneCompany&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;rates&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;9&quot;&gt;
 * &lt;list name=&quot;itemizeRates&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRate&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;fees&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;10&quot;&gt;
 * &lt;list name=&quot;itemizeFees&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;membership&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;11&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;image&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ImageDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;url&quot; type=&quot;String&quot; size=&quot;200&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;contactability&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactabilityDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;14&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;scheduleTimes&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ScheduleTimesDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;startTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;endTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto name=&quot;EntityOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.CardProposalDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;36&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;status&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;deliveriesOut&quot; order=&quot;5&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;deliveryOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DeliveryDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;deliveryContactOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;emailContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;address&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;location&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.LocationDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;list name=&quot;addressComponents&quot; order=&quot;1&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;addressComponent&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressComponentDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;code&quot; type=&quot;String&quot; size=&quot;7&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;componentTypes&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;componentType&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destinationOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.DestinationDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;7&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list name=&quot;additionalProducts&quot; order=&quot;11&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;additionalProduct&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AdditionalProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;productType&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;productOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;subproduct&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.SubproductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;14&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;paymentMethod&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaymentMethodDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;frecuency&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FrequencyDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;daysOfMonth&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DaysOfMonthDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;day&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;specificContactOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;mobileContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;phoneCompany&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;ratesOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;9&quot;&gt;
 * &lt;list name=&quot;itemizeRatesOut&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRateOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;feesOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;10&quot;&gt;
 * &lt;list name=&quot;itemizeFeesOut&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFeeOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;membership&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;image&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ImageDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;14&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;url&quot; type=&quot;String&quot; size=&quot;200&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;contactability&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactabilityDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;16&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;scheduleTimes&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ScheduleTimesDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;startTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;endTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Update Proposal&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPpcut002_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PPCUT002",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPpcut002_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPpcut002_1 {
		
		/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
	/**
	 * <p>Campo <code>proposal-id</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "proposal-id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 36, signo = true, obligatorio = true)
	private String proposalId;
	
}