package com.bbva.pzic.cards.dao.model.pecpt003_1.mock;

import com.bbva.pzic.cards.dao.model.pecpt003_1.RespuestaTransaccionPecpt003_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 19/02/2020.
 *
 * @author Entelgy
 */
public final class Pecpt003_1Stubs {

    private static final Pecpt003_1Stubs INSTANCE = new Pecpt003_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Pecpt003_1Stubs() {
    }

    public static Pecpt003_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPecpt003_1 buildRespuestaTransaccionPecpt003_1() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/pecpt003_1/mock/respuestaTransaccionPecpt003_1.json"), RespuestaTransaccionPecpt003_1.class);
    }
}
