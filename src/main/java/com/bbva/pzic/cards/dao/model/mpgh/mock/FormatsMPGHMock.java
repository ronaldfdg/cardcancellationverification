package com.bbva.pzic.cards.dao.model.mpgh.mock;

import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS2GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS3GH;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 9/02/2018.
 *
 * @author Entelgy
 */
public enum FormatsMPGHMock {
    INSTANCE;

    private ObjectMapperHelper mapper;

    FormatsMPGHMock() {
        this.mapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPMS1GH getFormatoMPMS1GHNaturalLifeMilles() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/FormatoMPMS1GHNaturalLifeMiles.json"), new TypeReference<FormatoMPMS1GH>() {
        });
    }

    public FormatoMPMS1GH getFormatoMPMS1GHValidateMandatoryParams() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/FormatoMPMS1GHMandatoryParams.json"), new TypeReference<FormatoMPMS1GH>() {
        });
    }

    public FormatoMPMS1GH getFormatoMPMS1GHNaturalPuntosVida() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/FormatoMPMS1GHNaturalPuntosVida.json"), new TypeReference<FormatoMPMS1GH>() {
        });
    }

    public FormatoMPMS1GH getFormatoMPMS1GHEnterprise() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/FormatoMPMS1GHEnterprise.json"), new TypeReference<FormatoMPMS1GH>() {
        });
    }

    public FormatoMPMS1GH getFormatoMPMS1GHNaturalMandatories() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/FormatoMPMS1GHNaturalMandatories.json"), new TypeReference<FormatoMPMS1GH>() {
        });
    }

    public FormatoMPMS1GH getFormatoMPMS1GHEmpty() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/formatoMPMS1GH-EMPTY.json"), FormatoMPMS1GH.class);
    }

    public List<FormatoMPMS2GH> getFormatoMPMS2GH() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/formatoMPMS2GH.json"), new TypeReference<List<FormatoMPMS2GH>>() {
        });
    }

    public List<FormatoMPMS3GH> getFormatoMPMS3GH() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgh/mock/formatoMPMS3GH.json"), new TypeReference<List<FormatoMPMS3GH>>() {
        });
    }
}
