package com.bbva.pzic.cards.dao.model.pecpt003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>participant</code>, utilizado por la clase <code>Entityin</code></p>
 * 
 * @see Entityin
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Participant {
	
	/**
	 * <p>Campo <code>identityDocument</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "identityDocument", tipo = TipoCampo.DTO)
	private Identitydocument identitydocument;
	
	/**
	 * <p>Campo <code>card</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "card", tipo = TipoCampo.DTO)
	private Card card;
	
}