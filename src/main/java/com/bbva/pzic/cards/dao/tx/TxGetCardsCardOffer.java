package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMENG9;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMS1G9;
import com.bbva.pzic.cards.dao.model.mpg9.PeticionTransaccionMpg9;
import com.bbva.pzic.cards.dao.model.mpg9.RespuestaTransaccionMpg9;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardsCardOfferMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Component("txGetCardsCardOffer")
public class TxGetCardsCardOffer
        extends SingleOutputFormat<InputGetCardsCardOffer, FormatoMPMENG9, Offer, FormatoMPMS1G9> {

    @Resource(name = "txGetCardsCardOfferMapper")
    private ITxGetCardsCardOfferMapper mapper;

    @Autowired
    public TxGetCardsCardOffer(@Qualifier("transaccionMpg9") InvocadorTransaccion<PeticionTransaccionMpg9, RespuestaTransaccionMpg9> transaction) {
        super(transaction, PeticionTransaccionMpg9::new, Offer::new, FormatoMPMS1G9.class);
    }

    @Override
    protected FormatoMPMENG9 mapInput(InputGetCardsCardOffer inputGetCardsCardOffer) {
        return mapper.mapIn(inputGetCardsCardOffer);
    }

    @Override
    protected Offer mapFirstOutputFormat(FormatoMPMS1G9 formatoMPMS1G9, InputGetCardsCardOffer inputGetCardsCardOffer, Offer offer) {
        return mapper.mapOut(formatoMPMS1G9);
    }
}