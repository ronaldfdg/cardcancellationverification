package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMENCV;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMS1CV;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
public interface ITxGetCardSecurityDataMapper {

    FormatoMPMENCV mapIn(InputGetCardSecurityData dtoIn);

    SecurityData mapOut(FormatoMPMS1CV formatOutput);
}
