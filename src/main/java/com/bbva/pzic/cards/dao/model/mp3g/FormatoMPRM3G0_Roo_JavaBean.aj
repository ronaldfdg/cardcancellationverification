// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mp3g;

import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPRM3G0;

privileged aspect FormatoMPRM3G0_Roo_JavaBean {
    
    /**
     * Gets idetarj value
     * 
     * @return String
     */
    public String FormatoMPRM3G0.getIdetarj() {
        return this.idetarj;
    }
    
    /**
     * Sets idetarj value
     * 
     * @param idetarj
     * @return FormatoMPRM3G0
     */
    public FormatoMPRM3G0 FormatoMPRM3G0.setIdetarj(String idetarj) {
        this.idetarj = idetarj;
        return this;
    }
    
}
