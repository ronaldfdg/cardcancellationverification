package com.bbva.pzic.cards.dao.model.mp6h.mock;

import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6GS;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6HS;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public final class FormatsMp6hStubs {
    private static final FormatsMp6hStubs INSTANCE = new FormatsMp6hStubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private FormatsMp6hStubs() {
    }

    public static FormatsMp6hStubs getInstance() {
        return INSTANCE;
    }

    public FormatoMPMQ6HS getFormatoMPMQ6HSMock() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mp6h/mock/formatoMPMQ6HS.json"), FormatoMPMQ6HS.class);
    }

    public FormatoMPMQ6GS getFormatoMPMQ6GSMock() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mp6h/mock/formatoMPMQ6GS.json"), FormatoMPMQ6GS.class);
    }
}