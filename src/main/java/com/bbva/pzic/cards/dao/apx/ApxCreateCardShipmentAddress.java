package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCardShipmentAddress;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardShipmentAddressMapper;
import com.bbva.pzic.cards.dao.model.pecpt002_1.PeticionTransaccionPecpt002_1;
import com.bbva.pzic.cards.dao.model.pecpt002_1.RespuestaTransaccionPecpt002_1;
import com.bbva.pzic.cards.facade.v0.dto.ShipmentAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
@Component
public class ApxCreateCardShipmentAddress {

    @Autowired
    private IApxCreateCardShipmentAddressMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionPecpt002_1, RespuestaTransaccionPecpt002_1> transaccion;

    public ShipmentAddress invoke(final InputCreateCardShipmentAddress input) {
        PeticionTransaccionPecpt002_1 request = mapper.mapIn(input);
        RespuestaTransaccionPecpt002_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
