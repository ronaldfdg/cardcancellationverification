package com.bbva.pzic.cards.dao.rest.mapper;

import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;

import java.util.List;

/**
 * Created on 04/11/2019.
 *
 * @author Entelgy
 */
public interface IRestListImagesCoversMapper {

    AWSImagesRequest mapIn(List<Card> cards);

    void mapOut(AWSImagesResponse response, List<Card> cards);
}
