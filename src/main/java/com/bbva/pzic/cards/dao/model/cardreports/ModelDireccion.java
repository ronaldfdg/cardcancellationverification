package com.bbva.pzic.cards.dao.model.cardreports;

import java.util.List;

public class ModelDireccion {

    private String id;

    private ModelTipoDireccion tipo;

    private String direccionCompleta;

    private List<ModelDireccionDetallada> direccionDetallada;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelTipoDireccion getTipo() {
        return tipo;
    }

    public void setTipo(ModelTipoDireccion tipo) {
        this.tipo = tipo;
    }

    public String getDireccionCompleta() {
        return direccionCompleta;
    }

    public void setDireccionCompleta(String direccionCompleta) {
        this.direccionCompleta = direccionCompleta;
    }

    public List<ModelDireccionDetallada> getDireccionDetallada() {
        return direccionDetallada;
    }

    public void setDireccionDetallada(List<ModelDireccionDetallada> direccionDetallada) {
        this.direccionDetallada = direccionDetallada;
    }
}
