package com.bbva.pzic.cards.dao.model.pecpt004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>address</code>, utilizado por la clase <code>Addresses</code></p>
 * 
 * @see Addresses
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Address {
	
	/**
	 * <p>Campo <code>shipmentAddress</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "shipmentAddress", tipo = TipoCampo.DTO)
	private Shipmentaddress shipmentaddress;
	
}