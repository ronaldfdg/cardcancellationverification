package com.bbva.pzic.cards.dao.model.kb93;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>KB93</code>
 *
 * @see PeticionTransaccionKb93
 * @see RespuestaTransaccionKb93
 */
@Component
public class TransaccionKb93 implements InvocadorTransaccion<PeticionTransaccionKb93,RespuestaTransaccionKb93> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionKb93 invocar(PeticionTransaccionKb93 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionKb93.class, RespuestaTransaccionKb93.class, transaccion);
	}

	@Override
	public RespuestaTransaccionKb93 invocarCache(PeticionTransaccionKb93 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionKb93.class, RespuestaTransaccionKb93.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
