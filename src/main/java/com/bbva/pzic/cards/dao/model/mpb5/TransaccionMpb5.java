package com.bbva.pzic.cards.dao.model.mpb5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPB5</code>
 *
 * @see PeticionTransaccionMpb5
 * @see RespuestaTransaccionMpb5
 */
@Component
public class TransaccionMpb5 implements InvocadorTransaccion<PeticionTransaccionMpb5,RespuestaTransaccionMpb5> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpb5 invocar(PeticionTransaccionMpb5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb5.class, RespuestaTransaccionMpb5.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpb5 invocarCache(PeticionTransaccionMpb5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb5.class, RespuestaTransaccionMpb5.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
