package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanData;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0TSC;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0TSE;

/**
 * Created on 3/07/2017.
 *
 * @author Entelgy
 */
public interface ITxCreateCardInstallmentsPlanMapper {

    FormatoMPM0TSE mapIn(DTOIntCardInstallmentsPlan dtoIn);

    InstallmentsPlanData mapOut1(FormatoMPM0TSC formatoMPM0TSC, DTOIntCardInstallmentsPlan dtoIn);

    InstallmentsPlanData mapOut2(FormatoMPM0DET formatoMPM0DET, InstallmentsPlanData installmentPlanData);
}
