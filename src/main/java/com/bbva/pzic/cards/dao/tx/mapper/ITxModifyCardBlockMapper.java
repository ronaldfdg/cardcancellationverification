package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.BlockData;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2S;

/**
 * @author Entelgy
 */
public interface ITxModifyCardBlockMapper {

    /**
     * Crea una nueva instancia de {@link FormatoMPM0B2} y la inicializa con el parámetro enviado.
     *
     * @param dtoIntBlock
     * @return the created object.
     */
    FormatoMPM0B2 mapInput(DTOIntBlock dtoIntBlock);

    /**
     * Crea una nueva instancia de {@link FormatoMPM0B2S} y la inicializa con los parámetros enviados.
     *
     * @param formatoMPM0B2S
     * @param dtoIntBlock
     * @return the created object.
     */
    BlockData mapOutput(FormatoMPM0B2S formatoMPM0B2S, DTOIntBlock dtoIntBlock);

}
