package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.dao.model.mprt.FormatoMPRMRT0;

/**
 * Created on 04/10/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateCardAdvanceMapperV0 {

    FormatoMPRMRT0 mapIn(InputCreateCard input);
}
