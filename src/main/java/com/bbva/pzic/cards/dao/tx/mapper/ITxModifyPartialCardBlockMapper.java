package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.dao.model.mpb4.FormatoMPM0B4E;

/**
 * Created on 29/08/2017.
 *
 * @author Entelgy
 */
public interface ITxModifyPartialCardBlockMapper {

    FormatoMPM0B4E mapIn(DTOIntBlock dtoIntBlock);
}
