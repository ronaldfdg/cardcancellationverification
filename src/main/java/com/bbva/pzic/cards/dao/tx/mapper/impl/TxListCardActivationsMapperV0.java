package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMENG2;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardActivationsMapperV0;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxListCardActivationsMapperV0 implements ITxListCardActivationsMapperV0 {

    private static final Log LOG = LogFactory.getLog(TxListCardActivationsMapperV0.class);

    private final Translator translator;
    private final BooleanToStringConverter booleanToStringConverter;

    public TxListCardActivationsMapperV0(Translator translator) {
        this.translator = translator;
        this.booleanToStringConverter = new BooleanToStringConverter();
    }

    @Override
    public FormatoMPMENG2 mapInt(final DTOIntCard dtoIn) {
        LOG.info("... called method TxListCardActivationsMapperV0.mapInt ...");
        FormatoMPMENG2 formatoMPMENG2 = new FormatoMPMENG2();
        formatoMPMENG2.setIdetarj(dtoIn.getCardId());
        return formatoMPMENG2;
    }

    @Override
    public List<Activation> mapOut(final FormatoMPMS1G2 formatoMPMS1G2, List<Activation> activationList) {
        LOG.info("... called method TxListCardActivationsMapperV0.mapOut ...");
        Activation activation = new Activation();
        activation.setName(formatoMPMS1G2.getDesactv());
        activation.setActivationId(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS1G2.getCodactv()));
        activation.setIsActive(booleanToStringConverter.convertFrom(formatoMPMS1G2.getInvactv(), null));
        activationList.add(activation);
        return activationList;
    }
}
