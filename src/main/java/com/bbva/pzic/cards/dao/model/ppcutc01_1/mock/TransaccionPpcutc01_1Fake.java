package com.bbva.pzic.cards.dao.model.ppcutc01_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.PeticionTransaccionPpcutc01_1;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.RespuestaTransaccionPpcutc01_1;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionPpcutc01_1")
public class TransaccionPpcutc01_1Fake implements InvocadorTransaccion<PeticionTransaccionPpcutc01_1, RespuestaTransaccionPpcutc01_1> {

    @Override
    public RespuestaTransaccionPpcutc01_1 invocar(PeticionTransaccionPpcutc01_1 peticion) {
        RespuestaTransaccionPpcutc01_1 response = new RespuestaTransaccionPpcutc01_1();
        response.setCodigoRetorno("OK_COMMIT");

        try {
            response.setEntityout(FormatsPpcutc01_1Stubs.getInstance().getEntityOut());
            return response;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Override
    public RespuestaTransaccionPpcutc01_1 invocarCache(PeticionTransaccionPpcutc01_1 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
