package com.bbva.pzic.cards.dao.model.ppcut002_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>deliveryContactOut</code>, utilizado por la clase <code>Deliveryout</code></p>
 * 
 * @see Deliveryout
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Deliverycontactout {
	
	/**
	 * <p>Campo <code>contactType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "contactType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String contacttype;
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 80, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>emailContact</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "emailContact", tipo = TipoCampo.DTO)
	private Emailcontact emailcontact;
	
}