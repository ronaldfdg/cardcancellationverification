package com.bbva.pzic.cards.dao.model.mpwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPWT</code>
 *
 * @see PeticionTransaccionMpwt
 * @see RespuestaTransaccionMpwt
 */
@Component
public class TransaccionMpwt implements InvocadorTransaccion<PeticionTransaccionMpwt,RespuestaTransaccionMpwt> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpwt invocar(PeticionTransaccionMpwt transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpwt.class, RespuestaTransaccionMpwt.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpwt invocarCache(PeticionTransaccionMpwt transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpwt.class, RespuestaTransaccionMpwt.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
