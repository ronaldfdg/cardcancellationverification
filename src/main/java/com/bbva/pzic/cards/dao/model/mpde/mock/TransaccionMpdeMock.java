package com.bbva.pzic.cards.dao.model.mpde.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpde.*;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Invocador de la transacci&oacute;n <code>MPDE</code>
 *
 * @see PeticionTransaccionMpde
 * @see RespuestaTransaccionMpde
 */
@Component("transaccionMpde")
public class TransaccionMpdeMock implements InvocadorTransaccion<PeticionTransaccionMpde,RespuestaTransaccionMpde> {

	public static final String TEST_NOT_RESPONSE = "666666";
	public static final String TEST_RATES_EMPTY = "666664";

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpde invocar(PeticionTransaccionMpde peticion) throws ExcepcionTransaccion {

		final RespuestaTransaccionMpde response = new RespuestaTransaccionMpde();
		response.setCodigoRetorno("OK_COMMIT");
		response.setCodigoControl("OK");

		FormatoMPMENDE format = peticion.getCuerpo().getParte(FormatoMPMENDE.class);

		if(TEST_NOT_RESPONSE.equalsIgnoreCase(format.getNrotarj()))
			return response;

		try{

			List<CopySalida> formatosMPMS1DE = builDataCopyFormatoMPMS1DE();
			List<CopySalida> formatosMPMS2DE = builDataCopyFormatoMPMS2DE();

			response.getCuerpo().getPartes().add(formatosMPMS1DE.get(0));
			response.getCuerpo().getPartes().add(formatosMPMS2DE.get(0));
			response.getCuerpo().getPartes().add(formatosMPMS2DE.get(1));

			response.getCuerpo().getPartes().add(formatosMPMS1DE.get(1));
			response.getCuerpo().getPartes().add(formatosMPMS2DE.get(2));
			response.getCuerpo().getPartes().add(formatosMPMS2DE.get(3));

			if (TEST_RATES_EMPTY.equalsIgnoreCase(format.getNrotarj())) {
				response.getCuerpo().getPartes().remove(5);
				response.getCuerpo().getPartes().remove(4);
				response.getCuerpo().getPartes().remove(2);
				response.getCuerpo().getPartes().remove(1);

				return response;

			}
			return response;



		} catch (IOException e) {
			throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
		}


	}

	private List<CopySalida> builDataCopyFormatoMPMS1DE() throws IOException {
		List<FormatoMPMS1DE> formats = FormatsMpdeMock.getInstance().getFormatoMPMS1DE();

		List<CopySalida> copies = new ArrayList<>();
		for (FormatoMPMS1DE format: formats) {
			copies.add(buildCopySalida(format));
		}
		return copies;
	}

	private List<CopySalida> builDataCopyFormatoMPMS2DE() throws IOException {
		List<FormatoMPMS2DE> formats = FormatsMpdeMock.getInstance().getFormatoMPMS2DE();

		List<CopySalida> copies = new ArrayList<>();
		for (FormatoMPMS2DE format: formats) {
			copies.add(buildCopySalida(format));
		}
		return copies;
	}

	private CopySalida buildCopySalida(Object format) {
		CopySalida copy = new CopySalida();
		copy.setCopy(format);
		return copy;
	}

	@Override
	public RespuestaTransaccionMpde invocarCache(PeticionTransaccionMpde transaccion) throws ExcepcionTransaccion {
		return null;
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
