package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelDatoContacto {

    private ModelTipoDatoContacto tipo;

    private String valor;

    public ModelTipoDatoContacto getTipo() {
        return tipo;
    }

    public void setTipo(ModelTipoDatoContacto tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
