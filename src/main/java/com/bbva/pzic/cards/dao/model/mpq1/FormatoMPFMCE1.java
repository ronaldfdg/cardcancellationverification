package com.bbva.pzic.cards.dao.model.mpq1;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPFMCE1</code> de la transacci&oacute;n <code>MPQ1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPFMCE1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPFMCE1 {

	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String idetarj;

}