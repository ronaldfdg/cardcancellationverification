package com.bbva.pzic.cards.dao.model.mpe2;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS1E2</code> de la transacci&oacute;n <code>MPE2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1E2")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1E2 {

	/**
	 * <p>Campo <code>IDDOCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDDOCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
	private String iddocta;

	/**
	 * <p>Campo <code>FECORTE</code>, &iacute;ndice: <code>2</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 2, nombre = "FECORTE", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecorte;

	/**
	 * <p>Campo <code>INDACTI</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "INDACTI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indacti;

}