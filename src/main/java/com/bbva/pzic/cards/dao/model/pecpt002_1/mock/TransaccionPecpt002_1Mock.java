package com.bbva.pzic.cards.dao.model.pecpt002_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.pecpt002_1.PeticionTransaccionPecpt002_1;
import com.bbva.pzic.cards.dao.model.pecpt002_1.RespuestaTransaccionPecpt002_1;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>PECPT001</code>
 *
 * @see PeticionTransaccionPecpt002_1
 * @see RespuestaTransaccionPecpt002_1
 */
@Component("transaccionPecpt002_1")
public class TransaccionPecpt002_1Mock implements InvocadorTransaccion<PeticionTransaccionPecpt002_1, RespuestaTransaccionPecpt002_1> {

    public static final String TEST_EMPTY = "999";
    public static final String TEST_STORED_ADDRESS = "111";

    @Override
    public RespuestaTransaccionPecpt002_1 invocar(final PeticionTransaccionPecpt002_1 transaccion) {
        try {
            if (TEST_EMPTY.equalsIgnoreCase(transaccion.getShipmentid())) {
                return new RespuestaTransaccionPecpt002_1();
            } else if (TEST_STORED_ADDRESS.equalsIgnoreCase(transaccion.getShipmentid())) {
                return Pecpt002_1Stubs.getInstance().buildStoredShipmentAddressResponse();
            }
            return Pecpt002_1Stubs.getInstance().buildSpecificShipmentAddressResponse();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPecpt002_1 invocarCache(PeticionTransaccionPecpt002_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}
