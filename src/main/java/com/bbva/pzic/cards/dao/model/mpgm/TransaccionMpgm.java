package com.bbva.pzic.cards.dao.model.mpgm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPGM</code>
 *
 * @see PeticionTransaccionMpgm
 * @see RespuestaTransaccionMpgm
 */
@Component("transaccionMpgm")
public class TransaccionMpgm implements InvocadorTransaccion<PeticionTransaccionMpgm,RespuestaTransaccionMpgm> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpgm invocar(PeticionTransaccionMpgm transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpgm.class, RespuestaTransaccionMpgm.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpgm invocarCache(PeticionTransaccionMpgm transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpgm.class, RespuestaTransaccionMpgm.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
