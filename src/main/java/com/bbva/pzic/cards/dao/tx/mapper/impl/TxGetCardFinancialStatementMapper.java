package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPME1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS2GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS3GH;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created on 8/02/2018.
 *
 * @author Entelgy
 */

@Component("txGetCardFinancialStatementMapper")
public class TxGetCardFinancialStatementMapper implements ITxGetCardFinancialStatementMapper {

    @Override
    public FormatoMPME1GH mapIn(final InputGetCardFinancialStatement dtoIn) {
        FormatoMPME1GH formatoMPME1GH = new FormatoMPME1GH();
        formatoMPME1GH.setIdetarj(dtoIn.getCardId());
        formatoMPME1GH.setIddocta(dtoIn.getFinancialStatementId());
        return formatoMPME1GH;
    }

    @Override
    public DTOIntCardStatement mapOut(final FormatoMPMS1GH formatOutput) {
        final DTOIntCardStatement dto = new DTOIntCardStatement();
        dto.setDetail(mapOutDetail(formatOutput));
        return dto;
    }

    private DTOIntCardStatementDetail mapOutDetail(final FormatoMPMS1GH formatOutput) {
        DTOIntCardStatementDetail dtoIntCardStatementDetail = new DTOIntCardStatementDetail();
        dtoIntCardStatementDetail.setCardId(formatOutput.getNumcont());
        dtoIntCardStatementDetail.setFormatsPan(formatOutput.getNumtarj());
        dtoIntCardStatementDetail.setProductName(formatOutput.getTiptarj());
        dtoIntCardStatementDetail.setProductCode(formatOutput.getCtiptar());
        dtoIntCardStatementDetail.setBranchName(formatOutput.getOficina());
        dtoIntCardStatementDetail.setParticipantNameFirst(formatOutput.getTitptar());
        dtoIntCardStatementDetail.setParticipantNameSecond(formatOutput.getTitstar());
        dtoIntCardStatementDetail.setParticipantType(formatOutput.getIndtper());
        dtoIntCardStatementDetail.setAccountNumberPEN(formatOutput.getNumcuep());
        dtoIntCardStatementDetail.setAccountNumberUSD(formatOutput.getNumcueu());
        dtoIntCardStatementDetail.setCurrency(formatOutput.getDivcont());
        dtoIntCardStatementDetail.setProgram(mapOutProgram(formatOutput));
        dtoIntCardStatementDetail.setEndDate(formatOutput.getFeccier());
        dtoIntCardStatementDetail.setPayType(formatOutput.getPagcarg());
        dtoIntCardStatementDetail.setAddress(mapOutAddress(formatOutput));
        dtoIntCardStatementDetail.setCreditLimit(formatOutput.getLincred());
        dtoIntCardStatementDetail.setLimits(mapOutLimits(formatOutput));
        dtoIntCardStatementDetail.setPaymentDate(formatOutput.getFeculpa());
        dtoIntCardStatementDetail.setInterest(mapOutInterest(formatOutput));
        dtoIntCardStatementDetail.setDisposedBalanceLocalCurrency(createMoneyInstance(formatOutput.getCreutip()));
        dtoIntCardStatementDetail.setDisposedBalance(createMoneyInstance(formatOutput.getCreutiu()));
        dtoIntCardStatementDetail.setTotalTransactions(formatOutput.getNummovi());
        dtoIntCardStatementDetail.setExtraPayments(mapOutExtraPayments(formatOutput));
        dtoIntCardStatementDetail.setTotalDebtLocalCurrency(createMoneyInstance(formatOutput.getImpdetp()));
        dtoIntCardStatementDetail.setTotalDebt(createMoneyInstance(formatOutput.getImpdetu()));
        dtoIntCardStatementDetail.setTotalMonthsAmortizationMinimum(formatOutput.getMapmini());
        dtoIntCardStatementDetail.setTotalMonthsAmortizationMinimumFull(formatOutput.getMapminf());
        return dtoIntCardStatementDetail;
    }

    private DTOIntCardStatementProgram mapOutProgram(final FormatoMPMS1GH formatOutput) {
        DTOIntCardStatementProgram dtoIntCardStatementProgram = new DTOIntCardStatementProgram();
        dtoIntCardStatementProgram.setLifeMilesNumber(formatOutput.getNumlife());
        dtoIntCardStatementProgram.setMonthPoints(formatOutput.getPuntmes());
        dtoIntCardStatementProgram.setTotalPoints(formatOutput.getPunacum());
        dtoIntCardStatementProgram.setBonus(formatOutput.getBonomes());
        return dtoIntCardStatementProgram;
    }

    private DTOIntAddress mapOutAddress(final FormatoMPMS1GH formatOutput) {
        DTOIntAddress dtoIntAddress = new DTOIntAddress();
        dtoIntAddress.setName(formatOutput.getDirptit());
        dtoIntAddress.setStreetType(formatOutput.getDirctit());
        dtoIntAddress.setState(formatOutput.getDirppro());
        dtoIntAddress.setUbigeo(formatOutput.getDirpubi());
        return dtoIntAddress;
    }

    private DTOIntCardStatementLimits mapOutLimits(final FormatoMPMS1GH formatOutput) {
        DTOIntCardStatementLimits dtoIntCardStatementLimits = new DTOIntCardStatementLimits();
        dtoIntCardStatementLimits.setDisposedBalance(formatOutput.getCreutil());
        dtoIntCardStatementLimits.setAvailableBalance(formatOutput.getCredisp());
        dtoIntCardStatementLimits.setFinancingDisposedBalance(formatOutput.getImpcuot());
        return dtoIntCardStatementLimits;
    }

    private DTOIntCardStatementInterest mapOutInterest(final FormatoMPMS1GH formatOutput) {
        DTOIntCardStatementInterest dtoIntCardStatementInterest = new DTOIntCardStatementInterest();
        dtoIntCardStatementInterest.setPurchasePEN(formatOutput.getTeacomp());
        dtoIntCardStatementInterest.setPurchaseUSD(formatOutput.getTeacomu());
        dtoIntCardStatementInterest.setAdvancedPEN(formatOutput.getTeaavap());
        dtoIntCardStatementInterest.setAdvancedUSD(formatOutput.getTeaavau());
        dtoIntCardStatementInterest.setCountervailingPEN(formatOutput.getTeacmpp());
        dtoIntCardStatementInterest.setCountervailingUSD(formatOutput.getTeacmpu());
        dtoIntCardStatementInterest.setArrearsPEN(formatOutput.getTeamorp());
        dtoIntCardStatementInterest.setArrearsUSD(formatOutput.getTeamoru());
        return dtoIntCardStatementInterest;
    }

    private DTOIntExtraPayments mapOutExtraPayments(final FormatoMPMS1GH formatOutput) {
        DTOIntExtraPayments dtoIntExtraPayments = new DTOIntExtraPayments();
        dtoIntExtraPayments.setOutstandingBalanceLocalCurrency(createMoneyInstance(formatOutput.getImpatpe()));
        dtoIntExtraPayments.setMinimumCapitalLocalCurrency(createMoneyInstance(formatOutput.getMonmipe()));
        dtoIntExtraPayments.setInterestsBalanceLocalCurrency(createMoneyInstance(formatOutput.getImpinpe()));
        dtoIntExtraPayments.setFeesBalanceLocalCurrency(createMoneyInstance(formatOutput.getImpcope()));
        dtoIntExtraPayments.setFinancingTransactionLocalBalance(createMoneyInstance(formatOutput.getCuopape()));
        dtoIntExtraPayments.setInvoiceMinimumAmountLocalCurrency(createMoneyInstance(formatOutput.getPagmipe()));
        dtoIntExtraPayments.setInvoiceAmountLocalCurrency(createMoneyInstance(formatOutput.getPagtope()));
        dtoIntExtraPayments.setOutstandingBalance(createMoneyInstance(formatOutput.getImpatus()));
        dtoIntExtraPayments.setMinimumCapitalCurrency(createMoneyInstance(formatOutput.getMonmius()));
        dtoIntExtraPayments.setInterestsBalance(createMoneyInstance(formatOutput.getImpinus()));
        dtoIntExtraPayments.setFeesBalance(createMoneyInstance(formatOutput.getImpcous()));
        dtoIntExtraPayments.setFinancingTransactionBalance(createMoneyInstance(formatOutput.getCuopaus()));
        dtoIntExtraPayments.setInvoiceMinimumAmountCurrency(createMoneyInstance(formatOutput.getPagmius()));
        dtoIntExtraPayments.setInvoiceAmount(createMoneyInstance(formatOutput.getPagtous()));
        return dtoIntExtraPayments;
    }

    private DTOMoney createMoneyInstance(final BigDecimal amount) {
        if (amount == null) {
            return null;
        }
        DTOMoney dtoMoney = new DTOMoney();
        dtoMoney.setAmount(amount);
        return dtoMoney;
    }

    @Override
    public DTOIntCardStatement mapOut2(final FormatoMPMS2GH formatOutput,
                                       final DTOIntCardStatement dtoOut) {
        DTOIntCardStatement dto = dtoOut;
        if (dto == null) {
            dto = new DTOIntCardStatement();
        }
        if (dto.getRowCardStatement() == null) {
            dto.setRowCardStatement(new ArrayList<String>());
        }
        dto.getRowCardStatement().add(formatOutput.getLinescu());
        return dto;
    }

    @Override
    public DTOIntCardStatement mapOut3(final FormatoMPMS3GH formatOutput,
                                       final DTOIntCardStatement dtoOut) {
        DTOIntCardStatement dto = dtoOut;
        if (dto == null) {
            dto = new DTOIntCardStatement();
        }
        if (dto.getMessages() == null) {
            dto.setMessages(new ArrayList<String>());
        }
        dto.getMessages().add(
                concatenateIfNotNull(
                        formatOutput.getLinme01(), formatOutput.getLinme02(),
                        formatOutput.getLinme03(), formatOutput.getLinme04()));
        return dto;
    }

    private String concatenateIfNotNull(String... str) {
        String result = "";
        for (String s : str) {
            if (s != null) {
                result = result.concat(s).concat(" ");
            }
        }
        return result.isEmpty() ? null : result.trim();
    }
}
