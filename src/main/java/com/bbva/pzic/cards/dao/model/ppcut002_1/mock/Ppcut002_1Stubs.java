package com.bbva.pzic.cards.dao.model.ppcut002_1.mock;

import com.bbva.pzic.cards.dao.model.ppcut002_1.RespuestaTransaccionPpcut002_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 03/12/2019.
 *
 * @author Entelgy
 */
public final class Ppcut002_1Stubs {

    private static final Ppcut002_1Stubs INSTANCE = new Ppcut002_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Ppcut002_1Stubs() {
    }

    public static Ppcut002_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPpcut002_1 getProposal() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/ppcut002_1/mock/respuestaTransaccionPpcut002_1.json"), RespuestaTransaccionPpcut002_1.class);
    }
}
