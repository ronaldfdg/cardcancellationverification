// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpde;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.cards.dao.model.mpde.RespuestaTransaccionMpde;

privileged aspect RespuestaTransaccionMpde_Roo_JavaBean {
    
    /**
     * Gets codigoRetorno value
     * 
     * @return String
     */
    public String RespuestaTransaccionMpde.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    /**
     * Sets codigoRetorno value
     * 
     * @param codigoRetorno
     * @return RespuestaTransaccionMpde
     */
    public RespuestaTransaccionMpde RespuestaTransaccionMpde.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }
    
    /**
     * Gets codigoControl value
     * 
     * @return String
     */
    public String RespuestaTransaccionMpde.getCodigoControl() {
        return this.codigoControl;
    }
    
    /**
     * Sets codigoControl value
     * 
     * @param codigoControl
     * @return RespuestaTransaccionMpde
     */
    public RespuestaTransaccionMpde RespuestaTransaccionMpde.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
        return this;
    }
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return RespuestaTransaccionMpde
     */
    public RespuestaTransaccionMpde RespuestaTransaccionMpde.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String RespuestaTransaccionMpde.toString() {
        return "RespuestaTransaccionMpde {" + 
                "codigoRetorno='" + codigoRetorno + '\'' + 
                ", codigoControl='" + codigoControl + '\'' + "}" + super.toString();
    }
    
}
