package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PF;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PS;
import com.bbva.pzic.cards.dao.model.mppf.PeticionTransaccionMppf;
import com.bbva.pzic.cards.dao.model.mppf.RespuestaTransaccionMppf;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardMembershipMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
@Component("txGetCardMembership")
public class TxGetCardMembership
        extends SingleOutputFormat<DTOIntSearchCriteria, FormatoMPM0PF, DTOIntMembershipServiceResponse, FormatoMPM0PS> {

    @Resource(name = "txGetCardMembershipMapper")
    private ITxGetCardMembershipMapper mapper;

    @Autowired
    public TxGetCardMembership(@Qualifier("transaccionMppf") InvocadorTransaccion<PeticionTransaccionMppf, RespuestaTransaccionMppf> transaction) {
        super(transaction, PeticionTransaccionMppf::new, DTOIntMembershipServiceResponse::new, FormatoMPM0PS.class);
    }

    @Override
    protected FormatoMPM0PF mapInput(DTOIntSearchCriteria dtoIntSearchCriteria) {
        return mapper.mapInput(dtoIntSearchCriteria);
    }

    @Override
    protected DTOIntMembershipServiceResponse mapFirstOutputFormat(FormatoMPM0PS formatoMPM0PS, DTOIntSearchCriteria dtoIntSearchCriteria, DTOIntMembershipServiceResponse dtoIntMembershipServiceResponse) {
        return mapper.mapOutput(formatoMPM0PS, dtoIntSearchCriteria);
    }
}
