package com.bbva.pzic.cards.dao.model.mpb1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPB1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpb1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpb1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPB1.D1170707.txt
 * MPB1ACTIVACION DE TARJETAS             MP        MP2CMPB1PBDMPPO MPM0BX              MPB1  NS3000CNNNNN    SSTN    C   NNNNSNNN  NN                2016-12-12XP86425 2017-07-0712.36.24XP92512 2016-12-12-13.28.56.876705XP86425 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0BX.D1170707.txt
 * MPM0BX  �ENTRADA ACTIVACION DE TARJETA �F�03�00023�01�00001�IDETARJ�NRO.TARJETA/CONTRATO�A�020�0�R�        �
 * MPM0BX  �ENTRADA ACTIVACION DE TARJETA �F�03�00023�02�00021�CODACTV�CODIGO DE ACTIVACION�A�002�0�R�        �
 * MPM0BX  �ENTRADA ACTIVACION DE TARJETA �F�03�00023�03�00023�INDACTV�INDICADOR ACTIVACION�A�001�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPB1.D1170707.txt
</pre></code>
 *
 * @see RespuestaTransaccionMpb1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPB1",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpb1.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0BX.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpb1 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}