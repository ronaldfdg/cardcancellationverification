package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpb1.FormatoMPM0BX;
import com.bbva.pzic.cards.dao.model.mpb1.PeticionTransaccionMpb1;
import com.bbva.pzic.cards.dao.model.mpb1.RespuestaTransaccionMpb1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardActivationMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.NoneOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author Entelgy
 */
@Component("txModifyCardActivation")
public class TxModifyCardActivation
        extends NoneOutputFormat<DTOIntActivation, FormatoMPM0BX> {

    @Autowired
    private ITxModifyCardActivationMapper txModifyCardActivationMapper;

    @Autowired
    public TxModifyCardActivation(@Qualifier("transaccionMpb1") InvocadorTransaccion<PeticionTransaccionMpb1, RespuestaTransaccionMpb1> transaction) {
        super(transaction, PeticionTransaccionMpb1::new);
    }

    @Override
    protected FormatoMPM0BX mapInput(DTOIntActivation dtoIntActivation) {
        return txModifyCardActivationMapper.mapInput(dtoIntActivation);
    }
}
