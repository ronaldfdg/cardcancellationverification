package com.bbva.pzic.cards.dao.model.mpb2;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPB2</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpb2</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpb2</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPB2.D1171128.txt
 * MPB2BLOQUEO DE TARJETAS                MP        MP2CMPB2PBDMPPO MPM0B2              MPB2  NS0000CNNNNN    SSTN    C   NNNNSNNN  NN                2016-12-15XP88366 2017-08-2410.32.39XP85517 2016-12-15-16.54.07.808799XP88366 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0B2.D1171127.TXT
 * MPM0B2  �ENT. BLOQUEO DE TARJETAS      �F�06�00051�01�00001�IDETARJ�IDENTIF.TARJETA     �A�016�0�R�        �
 * MPM0B2  �ENT. BLOQUEO DE TARJETAS      �F�06�00051�02�00017�IDEBLOQ�INDENTIF.DEL BLOQUEO�A�001�0�R�        �
 * MPM0B2  �ENT. BLOQUEO DE TARJETAS      �F�06�00051�03�00018�IDERAZO�ID.RAZON DE BLOQUEO �A�002�0�R�        �
 * MPM0B2  �ENT. BLOQUEO DE TARJETAS      �F�06�00051�04�00020�IDACTBL�IND.BLOQ/DESBLOQUEO �A�001�0�R�        �
 * MPM0B2  �ENT. BLOQUEO DE TARJETAS      �F�06�00051�05�00021�IDREPOS�IND.REPOSICION      �A�001�0�O�        �
 * MPM0B2  �ENT. BLOQUEO DE TARJETAS      �F�06�00051�06�00022�DESCRIP�REFERENCIA BLOQUEO  �A�030�0�O�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0B2S.D1171127.TXT
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�01�00001�IDEBLOQ�INDENTIF.DEL BLOQUEO�A�001�0�S�        �
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�02�00002�DESBLOQ�DESC.ID.DE BLOQUEO  �A�020�0�S�        �
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�03�00022�IDERAZO�ID.RAZON DE BLOQUEO �A�002�0�S�        �
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�04�00024�DESRAZO�DESC RAZON DE BLOQ  �A�030�0�S�        �
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�05�00054�MCNBLOQ�NUMERO BLOQUEO      �N�006�0�S�        �
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�06�00060�FECBLOQ�FECHA BLOQUEO       �A�010�0�S�        �
 * MPM0B2S �BLOQUEO DE TARJETAS           �X�07�00077�07�00070�HORABLQ�HORA BLOQUEO        �A�008�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPB2.D1171127.TXT
 * MPB2MPM0B2S MEC0B2S MP2CMPB21S                             XP88366 2016-12-15-18.12.35.051706XP88366 2016-12-15-18.12.35.051720
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpb2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPB2",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpb2.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0B2.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpb2 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}