package com.bbva.pzic.cards.dao.model.pecpt005_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PECPT005</code>
 * 
 * @see PeticionTransaccionPecpt005_1
 * @see RespuestaTransaccionPecpt005_1
 */
@Component
public class TransaccionPecpt005_1 implements InvocadorTransaccion<PeticionTransaccionPecpt005_1,RespuestaTransaccionPecpt005_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPecpt005_1 invocar(PeticionTransaccionPecpt005_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt005_1.class, RespuestaTransaccionPecpt005_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPecpt005_1 invocarCache(PeticionTransaccionPecpt005_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPecpt005_1.class, RespuestaTransaccionPecpt005_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}