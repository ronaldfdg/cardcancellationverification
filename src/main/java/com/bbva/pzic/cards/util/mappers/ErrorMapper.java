package com.bbva.pzic.cards.util.mappers;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.util.Errors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 30/07/2018.
 *
 * @author Entelgy
 */
@Component
public class ErrorMapper {

    private static final Log LOG = LogFactory.getLog(ErrorMapper.class);

    @Autowired
    private ConfigurationManager configurationManager;

    public String getAlias(String smcRegistryId, int statusCode) {
        if (smcRegistryId == null) {
            LOG.error("smcRegistryId is not defined");
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }

        return getProperty("servicing.smc.configuration." + smcRegistryId + ".backend.error.code." + statusCode);
    }

    private String getProperty(final String key) {
        String propertyValue = configurationManager.getProperty(key);
        if (propertyValue == null) {
            LOG.error(String.format("Property key '%s' is not defined", key));
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }
        LOG.debug(String.format("Loaded property '%s = %s'", key, propertyValue));
        return propertyValue;
    }
}
