package com.bbva.pzic.cards.util.orika.converter.builtin;

import com.bbva.pzic.cards.util.orika.converter.BidirectionalConverter;
import com.bbva.pzic.cards.util.orika.metadata.Type;

/**
 * Created on 06/12/2016.
 *
 * @author Entelgy
 */
public class LongToIntegerConverter extends BidirectionalConverter<Long, Integer> {


    @Override
    public Integer convertTo(Long source, Type<Integer> destinationType) {
        return Integer.parseInt(source.toString());
    }

    @Override
    public Long convertFrom(Integer source, Type<Long> destinationType) {
        return source.longValue();
    }
}
