package com.bbva.pzic.cards.util.mappers;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 25/06/2020.
 *
 * @author Entelgy.
 */
public final class DateMapper {

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String TIME_PATTERN = "HH:mm:ss";

    private DateMapper() {
    }

    public static String mapDateToStringHour(final Calendar date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(TIME_PATTERN).format(date.getTime());
    }

    public static String mapDateToStringDateTime(final Calendar date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(DATE_TIME_PATTERN).format(date.getTime());
    }

    public static Calendar buildCalendar(final Date date) {
        if (date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static Calendar buildCalendar(final String time) {
        if (StringUtils.isEmpty(time)) {
            return null;
        }

        try {
            return DateUtils.toDateTime(new SimpleDateFormat(DATE_PATTERN).format(new Date()), time);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }
}
