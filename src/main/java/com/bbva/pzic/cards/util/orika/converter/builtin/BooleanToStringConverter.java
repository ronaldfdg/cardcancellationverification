package com.bbva.pzic.cards.util.orika.converter.builtin;


import com.bbva.pzic.cards.util.orika.converter.BidirectionalConverter;
import com.bbva.pzic.cards.util.orika.metadata.Type;

/**
 * Created on 31/07/2015.
 *
 * @author Entelgy
 */
public class BooleanToStringConverter extends BidirectionalConverter<Boolean, String> {

    private String vTrue;
    private String vFalse;

    public BooleanToStringConverter() {
        vTrue = "S";
        vFalse = "N";
    }

    public BooleanToStringConverter(final String vTrue, final String vFalse) {
        this.vTrue = vTrue;
        this.vFalse = vFalse;
    }

    @Override
    public String convertTo(Boolean source, Type<String> destinationType) {
        return source == null ? null : (source ? vTrue : vFalse);
    }

    @Override
    public Boolean convertFrom(String source, Type<Boolean> destinationType) {
        return source == null ? null : (vTrue.equalsIgnoreCase(source) ? Boolean.TRUE : Boolean.FALSE);
    }
}
