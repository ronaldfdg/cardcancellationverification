package com.bbva.pzic.cards.util.orika.converter.builtin;

import com.bbva.pzic.cards.util.orika.converter.BidirectionalConverter;
import com.bbva.pzic.cards.util.orika.metadata.Type;

/**
 * Created on 11/10/2017.
 *
 * @author Entelgy
 */
public class BooleanToStringNumberConverter extends BidirectionalConverter<Boolean, String> {

    private static final String TRUE = "1";
    private static final String FALSE = "0";

    @Override
    public String convertTo(Boolean source, Type<String> destinationType) {
        return source == null ? null : (source ? TRUE : FALSE);
    }

    @Override
    public Boolean convertFrom(String source, Type<Boolean> destinationType) {
        return source == null ? null : (TRUE.equalsIgnoreCase(source) ? Boolean.TRUE : Boolean.FALSE);
    }
}