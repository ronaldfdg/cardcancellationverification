package com.bbva.pzic.cards.util.encrypt;

/**
 * @author Entelgy
 */
public enum Action {

    ENCRYPT,
    MASK;

    @Override
    public String toString() {
        return this.name();
    }
}
