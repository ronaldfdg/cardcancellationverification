package com.bbva.pzic.cards.util;

/**
 * Created on 15/04/2020.
 *
 * @author Entelgy
 */
public final class Converter {

    private static final String TRUE = "S";
    private static final String FALSE = "N";

    public static Boolean convertFrom(String source) {
        return TRUE.equalsIgnoreCase(source) ? Boolean.TRUE : (FALSE.equalsIgnoreCase(source) ? Boolean.FALSE : null);
    }

    public static Integer booleanToInteger(Boolean source) {
        return source == null ? null : (source ? 1 : 0);
    }
}