package com.bbva.pzic.cards.util.generate;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.util.Errors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Entelgy
 * @see <a href='https://docs.aws.amazon.com/es_es/general/latest/gr/sigv4_signing.html'>Firma de solicitudes de AWS con Signature Version 4</a>
 */
@Component
public class AWSV4AuthGenerator {

    static final String ACCESS_KEY = "accessKey";
    static final String SECRET_KEY = "secretKey";
    static final String REGION = "region";
    static final String SERVICE_NAME = "serviceName";

    private static final Log LOG = LogFactory.getLog(AWSV4AuthGenerator.class);
    /* Other variables */
    private static final String HMAC_ALGORITHM = "AWS4-HMAC-SHA256";
    private static final String AWS_4_REQUEST = "aws4_request";
    private static final String OBLIQUE_BAR = "/";
    private static final String LINE_BREAK = "\n";
    private static final String AMPERSAND = "&";
    private static final String EQUALS = "=";
    private static final String TWO_POINTS = ":";
    private static final String SEMICOLON = ";";
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    private String strSignedHeader;

    private ConfigurationManager configurationManager;

    @Autowired
    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    public Map<String, String> createAuthorization(final String httpMethodName, final String url) {
        return createAuthorization(httpMethodName, url, null, null, null);
    }

    public Map<String, String> createAuthorization(final String httpMethodName, final String url,
                                                   final SortedMap<String, List<String>> queryParameters) {
        return createAuthorization(httpMethodName, url, queryParameters, null, null);
    }

    public Map<String, String> createAuthorization(final String httpMethodName, final String url,
                                                   final SortedMap<String, List<String>> queryParameters,
                                                   final SortedMap<String, String> headers) {
        return createAuthorization(httpMethodName, url, queryParameters, headers, null);
    }

    public Map<String, String> createAuthorization(final String httpMethodName, final String url,
                                                   final SortedMap<String, List<String>> queryParameters,
                                                   final SortedMap<String, String> headers,
                                                   final String payload) {

        SortedMap<String, String> awsHeaders = new TreeMap<>();
        if (headers != null) {
            for (Map.Entry<String, String> entrySet : headers.entrySet()) {
                String key = entrySet.getKey().toLowerCase();
                String value = entrySet.getValue().toLowerCase();

                awsHeaders.put(key, value);
            }
        }

        final String xAmzDate = getTimeStamp();
        final String currentDate = getDate();
        String[] valuePropertyUrl = searchURLCanonicalAndHOST(url);
        String canonicalURI = valuePropertyUrl[1];
        awsHeaders.put("host", valuePropertyUrl[0]);
        awsHeaders.put("x-amz-date", xAmzDate);
        //paso 1
        LOG.info("....step 1 generate url canonico....");
        String canonicalURL = prepareCanonicalRequest(httpMethodName, canonicalURI, queryParameters, awsHeaders, payload);

        LOG.info(String.format("canonicalURL:%n%s", canonicalURL));
        // llamado al los enums regionName y serviceName
        String regionName = getAWSValue(REGION);
        String serviceName = getAWSValue(SERVICE_NAME);

        //paso 2
        LOG.info("....step 2 generate String to Sign....");
        String stringToSign = prepareStringToSign(canonicalURL, xAmzDate, currentDate, regionName, serviceName);

        LOG.info(String.format("stringToSign:%n%s", stringToSign));
        // llamada a enum's secretAccessKey y accessKeyID
        String secretAccessKey = getAWSValue(SECRET_KEY);
        String accessKeyID = getAWSValue(ACCESS_KEY);

        //paso 3
        LOG.info("....step 3 generate signature....");
        String signature = calculateSignature(stringToSign, secretAccessKey, currentDate, regionName, serviceName);

        LOG.info(String.format("signature: %s", signature));
        Map<String, String> awsHeaderAuthorization = new HashMap<>();
        awsHeaderAuthorization.put("x-amz-date", xAmzDate);
        awsHeaderAuthorization.put("Authorization", buildAuthorizationString(accessKeyID, regionName, serviceName, signature));

        return awsHeaderAuthorization;
    }

    public SortedMap<String, List<String>> convertTo(final Map<String, String> map) {
        SortedMap<String, List<String>> treeMap = new TreeMap<>();

        for (Map.Entry<String, String> entrySet : map.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue();

            treeMap.put(key, Collections.singletonList(value));
        }

        return treeMap;
    }

    private String buildAuthorizationString(final String accessKeyID, final String regionName, final String serviceName, final String strSignature) {
        return HMAC_ALGORITHM + " "
                + "Credential=" + accessKeyID + OBLIQUE_BAR + getDate() + OBLIQUE_BAR + regionName + OBLIQUE_BAR + serviceName + OBLIQUE_BAR + AWS_4_REQUEST + ","
                + "SignedHeaders=" + strSignedHeader + ","
                + "Signature=" + strSignature;
    }

    private String calculateSignature(final String stringToSign, final String secretAccessKey, final String currentDate, final String regionName, final String serviceName) {
        try {
            byte[] signatureKey = getSignatureKey(secretAccessKey, currentDate, regionName, serviceName);

            byte[] signature = hmacSHA256(signatureKey, stringToSign);

            return bytesToHex(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            LOG.error("error when encrypting under the algorithm Hmac", ex);
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, ex);
        }
    }

    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars).toLowerCase();
    }

    private byte[] hmacSHA256(byte[] key, String data) throws NoSuchAlgorithmException, InvalidKeyException {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
    }

    private byte[] getSignatureKey(String key, String date, String regionName, String serviceName) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] kSecret = ("AWS4" + key).getBytes(StandardCharsets.UTF_8);
        byte[] kDate = hmacSHA256(kSecret, date);
        byte[] kRegion = hmacSHA256(kDate, regionName);
        byte[] kService = hmacSHA256(kRegion, serviceName);
        return hmacSHA256(kService, AWS_4_REQUEST);
    }

    private String prepareStringToSign(String canonicalURL, String xAmzDate, String currentDate, String regionName, String serviceName) {
        return HMAC_ALGORITHM + LINE_BREAK + xAmzDate + LINE_BREAK + currentDate + OBLIQUE_BAR + regionName + OBLIQUE_BAR + serviceName + OBLIQUE_BAR + AWS_4_REQUEST + LINE_BREAK + generateHex(canonicalURL);
    }

    /**
     * @param httpMethod      HTTP method.
     * @param canonicalURI    base URI.
     * @param queryParameters query params.
     * @param awsHeaders      headers.
     * @param payload         payload.
     * @return canonical request
     * @see <a href='https://docs.aws.amazon.com/es_es/general/latest/gr/sigv4-create-canonical-request.html'>Tarea 1: Creación de una solicitud canónica para Signature Version 4</a>
     */
    private String prepareCanonicalRequest(final String httpMethod, final String canonicalURI, final SortedMap<String, List<String>> queryParameters, final SortedMap<String, String> awsHeaders, final String payload) {
        LOG.info("....prepareCanonicalRequest....");
        final StringBuilder canonicalURL = new StringBuilder();
        LOG.debug(String.format("......... HttpMethod %s .........", httpMethod));
        canonicalURL.append(httpMethod).append(LINE_BREAK);
        LOG.debug(String.format("......... Canonical URI %s .........", canonicalURI));
        String canonicalUriAux = canonicalURI == null || canonicalURI.trim().isEmpty() ? OBLIQUE_BAR : canonicalURI;
        canonicalURL.append(canonicalUriAux).append(LINE_BREAK);

        LOG.info("....processing query params....");
        StringBuilder queryString = new StringBuilder();
        if (queryParameters != null && !queryParameters.isEmpty()) {
            for (Map.Entry<String, List<String>> entrySet : queryParameters.entrySet()) {
                String key = entrySet.getKey();
                List<String> values = entrySet.getValue();

                if (values == null || values.isEmpty()) {
                    continue;
                }
                List<String> cleanedList = values.stream().filter(Objects::nonNull).collect(Collectors.toList());

                LOG.debug(String.format("......... Query Param %s value %s .........", key, cleanedList));

                if (cleanedList.size() != 1) {
                    Collections.sort(cleanedList);
                    LOG.debug(String.format("......... Sorted query Params %s", cleanedList));
                }

                cleanedList.forEach(value -> queryString.append(key).append(EQUALS).append(encodeParameter(value)).append(AMPERSAND));
            }
            queryString.deleteCharAt(queryString.lastIndexOf(AMPERSAND));
        }
        LOG.info(String.format("Query Param String %s", queryString));
        canonicalURL.append(queryString).append(LINE_BREAK);

        LOG.info("....processing headers....");
        StringBuilder signedHeaders = new StringBuilder();
        if (awsHeaders != null && !awsHeaders.isEmpty()) {
            for (Map.Entry<String, String> entrySet : awsHeaders.entrySet()) {
                String key = entrySet.getKey().toLowerCase();
                String value = entrySet.getValue().trim();
                LOG.debug(String.format("......... header %s value %s .........", key, value));
                canonicalURL.append(key).append(TWO_POINTS).append(value).append(LINE_BREAK);
                signedHeaders.append(key).append(SEMICOLON);
            }
            canonicalURL.append(LINE_BREAK);
        }
        LOG.info(String.format("headers String %s", signedHeaders));
        this.strSignedHeader = signedHeaders.substring(0, signedHeaders.length() - 1);
        canonicalURL.append(strSignedHeader).append(LINE_BREAK);

        LOG.info("....processing payload....");
        LOG.info(String.format("......... Payload String %s .........", payload));
        String payloadAux = payload == null ? "" : payload;
        canonicalURL.append(generateHex(payloadAux));

        return canonicalURL.toString();
    }

    private String generateHex(String data) {
        try {
            // Generating Hash
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(data.getBytes(StandardCharsets.UTF_8));
            byte[] digest = messageDigest.digest();
            // Generating HexEncode
            return String.format("%064x", new java.math.BigInteger(1, digest)).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            LOG.error("error the generate string to hex UTF-8", e);
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    private String getTimeStamp() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }

    private String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }

    private String encodeParameter(final String param) {
        try {
            return URLEncoder.encode(param, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            LOG.error("Error in encode UTF-8", e);
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    private String[] searchURLCanonicalAndHOST(final String url) {
        if (url == null) {
            LOG.error("AWS backend URL is undefined");
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING, "the url can not be null");
        }
        LOG.info(String.format("URL %s", url));
        String[] propertyValues = url.split("com");
        propertyValues[0] = url.split("/")[2];
        return propertyValues;
    }

    private String getAWSValue(final String value) {
        String key = "servicing.SNPE1710030.aws.authorization".concat(".").concat(value);
        String propertyValue = configurationManager.getProperty(key);
        if (propertyValue == null) {
            LOG.error(String.format("Property key '%s' is not defined", key));

            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }
        LOG.debug(String.format("Loaded property '%s = %s'", key, propertyValue));
        return propertyValue;
    }
}
