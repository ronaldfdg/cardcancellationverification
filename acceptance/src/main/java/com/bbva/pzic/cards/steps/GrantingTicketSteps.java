package com.bbva.pzic.cards.steps;

public class GrantingTicketSteps {

    private static final String PAYLOAD_FORMAT = "{\"authentication\": \"%s\", \"backendUserRequest\": \"%s\"}";

    private TestingScenario scenario;

    @Inject
    public GrantingTicketSteps(TestingScenario scenario) { this.scenario = scenario; }

    @When("validating granting ticket service responses well with user data: {string} y {string}")
    public void consultingGrantingTicketService(String authentication, String backendUserRequest) {
        scenario.http("gt").post("/TechArchitecture/pe/grantingTicket/V02")
                .body()
                .json().content(String.format(PAYLOAD_FORMAT, authentication, backendUserRequest)).send("gt");
    }

}
