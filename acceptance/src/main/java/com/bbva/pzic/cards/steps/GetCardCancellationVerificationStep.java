package com.bbva.pzic.cards.steps;

public class GetCardCancellationVerificationStep {

    @Inject
    private TestingScenario scenario;

    @When("make a request to getCardCancellationVerification service with card-id {string}")
    public void getCardCancellationVerificationWithCardID(String cardID) {
        scenario.http("gt")
                .get("/cards/v1/cards/".concat(cardID).concat("/cancellation-verification"))
                .send();
    }

    @Then("I check that AMITYP1 property is equal to {string}")
    public void checkingAMITYP1PropertyIsEqualTo(String exceptedValue) {
        scenario.assertThat("data.debtDetail.*.currentDebt.rates.itemizeRates.*.itemizeRatesUnit[?(@.unitRateType ==" + exceptedValue + ")].unitRateType[0]").is(exceptedValue);
    }

    @Then("I check that AMCTYP1 property is equal to {string}")
    public void checkingAMCTYP1PropertyIsEqualTo(String exceptedValue) {
        scenario.assertThat("data.debtDetail.*.currentDebt.conditions.*.format[?(@.formatType ==" + exceptedValue + ")].formatType[0]").is(exceptedValue);
    }

    @Then("I check that AMCTYP2 property is equal to {string}")
    public void checkingAMCTYP2PropertyIsEqualTo(String exceptedValue) {
        scenario.assertThat("data.debtDetail.*.overdueDebt.conditions.*.format[?(@.formatType ==" + exceptedValue + ")].formatType[0]").is(exceptedValue);
    }

    @Then("I check that AMITYP2 property is equal to {string}")
    public void checkingAMITYP2PropertyIsEqualTo(String exceptedValue) {
        scenario.assertThat("data.debtDetail.*.overdueDebt.rates.itemizeRates.*.itemizeRatesUnit[?(@.unitRateType ==" + exceptedValue + ")].unitRateType[0]").is(exceptedValue);
    }

}
