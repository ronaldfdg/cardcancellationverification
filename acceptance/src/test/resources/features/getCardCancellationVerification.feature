# Created by Entelgy
@getCardCancellationVerification
Feature: getCardCancellationVerification feature

  Background: Granting ticket access
    Given client "user" with tags "data"
    #And user uses channel "$user.consumerId" to access by user "$user.userID" and iv-ticket "$user.ivticket"
    Then validating granting ticket service responses well with user data "$data.users[0].authentication" y "$data.users[0].backendUserRequest"

  @firstStep
  Scenario: Output Enum - AMITYP1 "AMOUNT"
    Given client "client" with tags "SMGG20200506"
    When make a request to getCardCancellationVerification service with card-id "$client.cards[0].cardID"
    Then I check that AMITYP1 property is equal to "AMOUNT"
    And http status is equal to 200

  @secondStep
  Scenario: Output Enum - AMCTYP1 "AMOUNT"
    Given client "client" with tags "SMGG20200506"
    When make a request to getCardCancellationVerification service with card-id "$client.cards[1].cardID"
    Then I check that AMCTYP1 property is equal to "AMOUNT"
    And http status is equal to 200

  @thirdStep
  Scenario: Output Enum - AMCTYP2 "AMOUNT"
    Given client "client" with tags "SMGG20200506"
    When make a request to getCardCancellationVerification service with card-id "$client.cards[2].cardID"
    Then I check that AMCTYP2 property is equal to "AMOUNT"
    And http status is equal to 200

  @fourthStep
  Scenario: Output Enum - AMITYP2 "AMOUNT"
    Given client "client" with tags "SMGG20200506"
    When make a request to getCardCancellationVerification service with card-id "$client.cards[3].cardID"
    Then I check that AMITYP2 property is equal to "AMOUNT"
    And http status is equal to 200